//echo php_sapi_name();die;

//require_once 'class/common/import.php'; //Class to manage the importations
//require_once 'class/Batchjob.php';
//require_once 'class/datatypes/Package.php';


function batchImport_Import($packageId, $containerId, $file)
{
	echo 'Try to import package '.$packageId . ' to container ' . $containerId . PHP_EOL;
	$Container = new mockup($containerId);
	$errorCount = 0;

	$Package = new Rb_Datatype_Package('mockup');
	$Package->load($packageId);
	$ok = $Package->import($Container, false);
	$file = $Package->getProperty('file');

	echo PHP_EOL;
	if(!$ok){
		echo 'Import failed for package ' . $packageId . PHP_EOL;
		$errorCount++;
	}
	else{
		echo "File $file of package $packageId is successfully imported" . PHP_EOL;
		$ok = unlink($file);
		if($ok){
			echo "Imported package file $file is suppressed" . PHP_EOL;
		}
		else{
			echo "Unable to suppress package file $file" . PHP_EOL;
		}
	}
	return $errorCount;
}

/*
$logFile = '/var/log/rbmockup/batchImport.log';
$logger =& Log::singleton('composite');
$filelogger =& Log::singleton('file', $logFile);
$logger->addChild($filelogger);
*/

$manager = container::_factory('mockup'); //Create new manager
$space =& $manager->space;

$import = new import($manager->SPACE_NAME , $manager);

/* get package to run */
$batchJob = new Rbplm_Batchjob();
$ctime = time();
$runner = Rbplm_Batchjob::RUNNER_FILEIMPORT;

$sql = "SELECT * FROM batchjob WHERE planned < $ctime AND state='init' AND runner='$runner'";
$dbranchbe = Ranchbe::getDb();
if(!$rs = $dbranchbe->SelectLimit( $sql , 1000 , 0)){
	echo $dbranchbe->ErrorMsg() . PHP_EOL;
	die;
}

foreach ($rs as $row) {
	$job = new Rbplm_Batchjob();
	$job->loadFromArray($row);
	$job_start = time();
	$job->setStart($job_start);
	$jname = 'batchFileImportInMockup';
	$job->setName( $jname );
	$job->setState( Rbplm_Batchjob::STATE_RUNNING );
	$ok = $job->update();
	if(!$ok ){
		echo 'error during saving' . PHP_EOL;
		die;
	}

	$datas = $job->getProperty('data');
	$errorCount = 0;
	$cleanDatas = array();

	$jId = $job->getProperty('id');

	echo 'Run job ' . $jId . PHP_EOL;

	foreach($datas as $i=>$dataItem){
		$packageId = $dataItem[0];
		$containerId = $dataItem[1];

		//Unvalid data def
		if( !$containerId || !$packageId ){
			continue;
		}

		$PackInfos = $import->GetPackageInfos( $packageId );
		$file = $PackInfos['package_file_path'].'/'.$PackInfos['package_file_name'];
		$ext = substr( $file, strrpos($file, '.') );

		if( !is_file($file) ){
			echo $file . ' is not a file' . PHP_EOL;
			$errorCount++;
			continue;
		}

		//uncompress Z files
		if($ext == '.Z'){
			$exec_return = 1;
			system( UNZIPCMD . ' "' . $file . '"' , $exec_return );
			if ( $exec_return == 0 ){
				echo $file . ' is successfully uncompress' . PHP_EOL;
				$file = rtrim($file , '.Z');
				$infos = $import->RecordImportPackage($file , false);
				$packageId = $infos['import_order'];
				$c = batchImport_Import($packageId, $containerId, $file);
				$errorCount = $errorCount + $c;
			}
			else{
				echo 'unable to uncompress file ' . $file . PHP_EOL;
				echo '------> file size:' . filesize($file) . PHP_EOL;
				echo '------> available disk space:' . disk_free_space(dirname($file)) . PHP_EOL;
				$errorCount++;
				continue;
			}
		}
		else{
			batchImport_Import($packageId, $containerId, $file);
		}

		//reaffect new package id to job definition
		$dataItem[0] = $packageId;

		//List of data without unvalid defs
		$cleanDatas[] = $dataItem;
		$datas[$i] = $dataItem;
	}

	$job->setData($datas);
	$job->update();

	$job->setEnd(time());

	if( $errorCount == 0 ){
		$job->setState(Rbplm_Batchjob::STATE_SUCCESS);
		echo 'Success ending of job ' . $jId . PHP_EOL;
	}
	else if($errorCount >= count($datas) ){
		$job->setState(Rbplm_Batchjob::STATE_FAILED);
		echo 'Fails on job ' . $jId . PHP_EOL;
	}
	else if($errorCount > 0 ){
		$job->setState(Rbplm_Batchjob::STATE_UNCOMPLETE);
		echo 'Uncomplete job ' . $jId . PHP_EOL;
	}

	$job->update();
}

//Clean old not imported package
$olderThan = time() - (2 * 7 * 24 * 3600); //Older than 2 weeks
$import->CleanHistory($olderThan);

//Clean olds jobs
$sql = "DELETE FROM batchjob WHERE submit < $olderThan AND runner='$runner' ";
if( !$result = $dbranchbe->Execute($sql) ){
	echo 'Error during cleaning of Jobs' . PHP_EOL;
	echo $dbranchbe->ErrorMsg() . PHP_EOL;
}
