# README #

### What is this repository for? ###
Ranchbe is a PLM/PDM/GED for little ingineering organisation wich use 
CAO system.
The purpose of this software is to manage all documents of the engineering 
department. A clear distinction is operate between documents used for 
works and documents to delivers to customers or to manufacturers.

### Fonctionalities ###
+ Ged
+ Pdm
	(@todo: list of OMG implementation)
+ Vault
+ Workflow manager and designer
+ User access and and ACL
+ Modularity
+ 3D viewer

### Technologie ###
+ Language = PHP5.6
+ Framework php = Zend framework 2
+ Framework Javascripts = Jquery, threeJs, babyloneJs
+ Framework Css = Bootstrap

### Version identification ###

Ranchbe 1.1 is a beta version. Its not for production.

Productions versions are identified by a even minor version.
Odd minor version is a dev version, for tests only.

Example :
+ 1.1 = dev version
+ 1.2 = production version
+ 1.3 = dev version

### Summary of set up for Ubuntu 14.04 ###

#### Setup your proxy connection if necessary
	export http_proxy=http://$user:$passwd@$proxyhost:$proxyport
	export https_proxy=http://$user:$passwd@$proxyhost:$proxyport
	export HTTP_PROXY=http://$user:$passwd@$proxyhost:$proxyport
	export HTTPS_PROXY=http://$user:$passwd@$proxyhost:$proxyport

#### Clone and install some dependencies :
	sudo apt install git
	git clone https://ocyssau@bitbucket.org/ocyssau/rbsier.git
	cd rbsier
	sudo apt install curl apache2 php php-ldap php-mysql php-mbstring php-xml php-zip
	#pour ubuntu 16.04
	sudo apt install php-ssh2 libapache2-mod-php
	sudo a2enmod rewrite
	sudo phpenmod ldap
	sudo a2enmod rewrite
	
	php installer
	php composer.phar self-update
	php composer.phar config --global repo.packagist composer https://packagist.org
	php composer.phar install --prefer-dist

#### For developmenent only
	git config --global credential.helper 'cache --timeout=28800'
	sudo apt install php-xdebug

#### Configuration d'Apache :
	sudo cp config/dist/apache.conf /etc/apache2/sites-available/rbsier.conf
	sudo nano /etc/apache2/sites-available/rbsier.conf
	sudo service apache2 restart
	
#### Configuration htaccess :
	sudo cp config/dist/htaccess.dist public/.htaccess
	#Edit the BASEURL env var
	sudo nano public/.htaccess

#### Create the basic application config file
	sudo cp config/dist/application.config.php.dist config/application.config.php
	sudo nano config/application.config.php

### Enable the site
	sudo a2ensite rbsier

#### Init the owner of ranchbe file :
Ensure that the run user of apache is matching with the owner of the writable files.
Apache user is defined in /etc/apache2/envvars, find follow lines :

	export APACHE_RUN_USER=www-data
	export APACHE_RUN_GROUP=www-data

And set the owner of the writables ranchbe files :

	chown -R root:www-data *
	chown -R www-data:www-data data
	chmod -R 755 *
	chmod -R 775 data
	
For a production site you must set your own user/group rights stategy.

#### Short open tags
Set short_open_tag directive to On or unset it in php.ini
nano /etc/php/[php version]/apache2/php.ini

#### Restart apache to take account configuration
	sudo service apache2 restart

#### Database installation :
	mysql -u root -p [password] 
		> CREATE DATABASE rbsier CHARACTER SET utf8 COLLATE utf8_general_ci;
		> CREATE USER 'rbsier'@'%' IDENTIFIED BY 'rbsier';
		> GRANT USAGE ON * . * TO 'rbsier'@'%' IDENTIFIED BY 'rbsier' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
		> GRANT ALL PRIVILEGES ON `rbsier` . * TO 'rbsier'@'%';
		> exit
	mysql -u root -p rbsier < config/install/db/1-0-7-12.sql
	mysql -u root -p rbsier < config/install/db/initSeq.sql
	mysql -u root -p rbsier < config/install/db/inituser.sql
	mysql -u root -p rbsier < config/install/db/proc.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.0.7/1.0.7.13.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.0.7/1.0.7.14.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.0.7/1.0.7.15.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.0.7/1.0.7.16.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.0.7/1.0.7.17.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.0.7/1.0.7.18.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.0.7/1.0.7.19.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.0.7/1.0.7.20.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.1/1.1.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.1/1.1.1.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.1/1.1.2.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.1/1.1.3.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.1/1.1.4.sql
	mysql -u root -p rbsier < config/install/db/1.0.6to1.1/1.1.5.sql
	
#### Configuration :
Setup your db parameters in :

	nano config/autoload/local.php

Other parameters may be copied from config/autoload/global.php. 
Adapt only the local.php file, global.php will be replaced at each update of ranchbe.

#### Create reposits :
	sudo php install.php

#### Restart the web server :
	sudo service apache2 restart


#### Additionals settings

##### Set session directory
usefull in Ubuntu to prevent the cron session cleanup scripts to delete session every 30mn.

Put this in htaccess :

	php_value session.gc_maxlifetime 43200
	php_value session.gc_probability 1
	php_value session.gc_divisor 100
	php_value session.save_path [ranchbe application path]/data/cache/sessions

Set permission on session directory

	mkdir [ranchbe application path]/data/cache/sessions
	sudo chown root:root [ranchbe application path]/data/cache/sessions
	sudo chmod 733 [ranchbe application path]/data/cache/sessions

#### How to run tests :
@todo

#### Deployment instructions :
@todo

### Contribution guidelines ###
* Writing tests
@todo
* Code review
@todo
* Other guidelines
@todo

### Who do I talk to? ###
* Repo owner or admin
Olivier CYSSAU : ocyssau@free.fr
