<?php
namespace Rbs\Notification;

use Ranchbe;
use Rbplm\People;
use Rbplm\Dao\Filter\Op;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Rbs\Ged\Document\Event;

//use Rbs\EventDispatcher\EventException;

/**
 * 
 *
 */
class Listener
{

	/**
	 *
	 * @param Event $event
	 * @return Event
	 */
	public static function onEvent(Event $event): Event
	{
		$eventName = $event->getName();
		$factory = $event->getFactory();
		$document = $event->getEmitter();
		$ranchbe = Ranchbe::get();

		/* @var \Rbplm\Dao\DaoListAbstract $list */
		$list = $factory->getList(Notification::$classId);
		$dao = $factory->getDao(Notification::$classId);

		/* @var \Rbplm\Dao\FilterAbstract $filter */
		$filter = $factory->getFilter(Notification::$classId);
		$filter->orfind($document->parentUid, $dao->toSys('referenceUid'), Op::EQUAL);
		$filter->orfind($document->getUid(), $dao->toSys('referenceUid'), Op::EQUAL);
		$filter->andfind($eventName, $dao->toSys('events'), Op::CONTAINS);
		$filter->select(array(
			$dao->toSys('ownerUid') . ' AS ' . 'ownerUid'
		));
		$list->load($filter);

		if ( $list->count() == 0 ) {
			return $event;
		}

		/* Send message to next users */
		$from = People\CurrentUser::get();
		/* @var \Ranchbe\Service\Mail $mailService */
		$mailService = $ranchbe->getServiceManager()->getMailService();
		$message = $mailService->messageFactory();
		$message->setFrom($from->getMail(), $from->getLogin());

		switch ($eventName) {
			case (Event::POST_CREATE):
				$subject = sprintf(tra('New document %s by %s'), $document->getNumber(), $from->getLogin());
				break;
			case (Event::POST_CHECKIN):
				$subject = sprintf(tra('Document %s has been updated by %s'), $document->getNumber(), $from->getLogin());
				break;
			case (Event::POST_CHECKOUT):
				$subject = sprintf(tra('Document %s is check-out by %s'), $document->getNumber(), $from->getLogin());
				break;
			case (Event::POST_MOVE):
				$subject = sprintf(tra('Document %s has been moved by %s'), $document->getNumber(), $from->getLogin());
				break;
			case (Event::POST_DELETE):
				$subject = sprintf(tra('Document %s has been suppressed by %s'), $document->getNumber(), $from->getLogin());
				break;
			default:
				$shortEventName = explode('.', $eventName)[0];
				$subject = sprintf(tra('Event %s on document %s by %s'), $shortEventName, $document->getNumber(), $from->getLogin());
				break;
		}

		$baseurl = $ranchbe->getMvcApplication()
			->getRequest()
			->getBasePath();
		$url = $baseurl . '/ged/document/detail/%s/%s';
		$docDetailUrl = sprintf($url, $document->getId(), strtolower($factory->getName()));

		$queryDatas = [
			'checked[]' => $document->getId(),
			'spacename' => strtolower($factory->getName())
		];
		$unsubscribeUrl = $baseurl . '/ged/document/notification/edit' . '?' . http_build_query($queryDatas);

		$htmlBody = '<p>';
		$htmlBody .= $subject;
		$htmlBody .= '</p><p>';
		$htmlBody .= '<a class="rb-popup btn btn-default" href="' . $docDetailUrl . '">Get Details of document' . $document->getNumber() . '</a>';
		$htmlBody .= '</p><p>';
		$htmlBody .= sprintf('You receive this message because you subscribed notifications on %s ', $document->parentUid);
		$htmlBody .= sprintf('To unsubscribe goto <a class="rb-popup btn btn-default" href="%s">here</a>', $unsubscribeUrl);
		$htmlBody .= '</p>';

		$htmlPart = new MimePart($htmlBody);
		$htmlPart->type = "text/html";
		$body = new MimeMessage();
		$body->addPart($htmlPart);
		$message->setBody($body);
		$message->setSubject($subject);
		$userDao = $factory->getDao(\Rbplm\People\User::$classId);

		$toUid = null;
		$toMail = null;
		$toLogin = null;
		foreach( $list as $notification ) {
			$toUid = $notification['ownerUid'];
			$stmt = $userDao->query([
				'mail',
				'login'
			], 'login=:uid', [
				':uid' => $toUid
			]);
			$u = $stmt->fetch(\PDO::FETCH_ASSOC);
			if ( $u ) {
				$toMail = $u['mail'];
				$toLogin = $u['login'];
				$message->addTo($toMail, $toLogin);
			}
		}

		/* */
		if ( $toMail ) {
			try{
				$mailService->send($message);
			}
			catch( \Exception $e ) {
				$ranchbe->log(sprintf('Error Code : %s: %s', $e->getCode(), $e->getMessage() ));
			}
		}

		//$event->notificationMessage = $message;
		return $event;
	}
}
