<?php
namespace Rbs\Notification;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `notifications` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(32) NOT NULL,
 `name` VARCHAR(128) NOT NULL,
 `owner_uid` VARCHAR (128) NOT NULL,
 `owner_id` VARCHAR (128) NOT NULL,
 `reference_uid` VARCHAR (128) NOT NULL,
 `reference_cid` VARCHAR (128) NULL,
 `reference_id` VARCHAR (128) NULL,
 `spacename` VARCHAR (64) NOT NULL,
 `events` VARCHAR (256) NULL,
 `condition` BLOB,
 PRIMARY KEY (`id`),
 UNIQUE KEY `U_notifications_1` (`uid`),
 UNIQUE KEY `U_notifications_2` (`owner_id`,`reference_uid`,`events`),
 KEY `K_notifications_owner_id` (`owner_id`),
 KEY `K_notifications_reference_uid` (`reference_uid`),
 KEY `K_notifications_events` (`events`),
 KEY `K_notifications_name` (`name`)
) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE `notifications`;
 <<*/

class NotificationDao extends \Rbs\Dao\Sier
{

	/**
	 * @var string
	 */
	public static $table='notifications';

	/**
	 * @var string
	 */
	public static $vtable='notifications';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'name'=>'name',
		'owner_uid'=>'ownerUid',
		'owner_id'=>'ownerId',
		'reference_uid'=>'referenceUid',
		'reference_cid'=>'referenceCid',
		'reference_id'=>'referenceId',
		'spacename'=>'spacename',
		'events'=>'events',
		'condition'=>'condition'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'condition'=>'json',
		'events'=>'json'
	);

	/**
	 * 
	 * @param \Rbs\Notification\Notification $mapped
	 * @param string $uid
	 * @param string $ownerUid
	 * @return \Rbplm\Dao\MappedInterface
	 */
	public function loadFromReferenceUidAndOwnerUid($mapped, $uid, $ownerUid)
	{
		$filter = 'obj.reference_uid=:uid AND obj.owner_uid=:ownerUid';
		$bind = array(':uid'=>$uid, ':ownerUid'=>$ownerUid);
		return $this->load($mapped, $filter, $bind );
	}
}
