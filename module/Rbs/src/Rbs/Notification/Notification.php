<?php
namespace Rbs\Notification;

use Rbplm\People;

/**
 * 
 *
 */
class Notification extends \Rbplm\Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Mapped, \Rbplm\Owned;

	/**
	 *
	 * @var string
	 */
	static $classId = '479v169a42a96';

	/**
	 *
	 * @var \Rbs\Notification\Condition
	 */
	protected $condition = false;

	/**
	 *
	 * @var array
	 */
	protected $events;

	/**
	 *
	 * @var \Rbplm\Any
	 */
	protected $reference;

	/**
	 *
	 * @var string
	 */
	public $referenceUid;

	/**
	 *
	 * @var string
	 */
	public $referenceCid;

	/**
	 *
	 * @var integer
	 */
	public $referenceId;

	/**
	 *
	 * @var string
	 */
	public $spacename;

	/**
	 *
	 * @param array $properties
	 */
	public function __construct($properties = null)
	{
		if ( $properties ) {
			$this->hydrate($properties);
		}
		$this->cid = static::$classId;
	}

	/**
	 *
	 * @param string $name
	 * @return \Rbplm\Any
	 */
	public static function init($name = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		if ( !$name ) {
			$name = $obj->getUid();
		}
		$obj->setName($name);
		$obj->setOwner(People\CurrentUser::get());
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return \Rbs\Notification\Notification
	 */
	public function hydrate(array $properties)
	{
		// ANY
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		// MAPPED
		$this->mappedHydrate($properties);
		// OWNED
		$this->ownedHydrate($properties);
		// NOTIFICATION
		(isset($properties['events'])) ? $this->events = $properties['events'] : null;
		(isset($properties['reference'])) ? $this->reference = $properties['reference'] : null;
		(isset($properties['referenceId'])) ? $this->referenceId = $properties['referenceId'] : null;
		(isset($properties['referenceUid'])) ? $this->referenceUid = $properties['referenceUid'] : null;
		(isset($properties['referenceCid'])) ? $this->referenceCid = $properties['referenceCid'] : null;
		(isset($properties['spacename'])) ? $this->spacename = $properties['spacename'] : null;

		if ( isset($properties['condition']) ) {
			$this->condition = new Condition($properties['condition']);
		}
		return $this;
	}

	/**
	 *
	 * @return \Rbs\Notification\Condition
	 */
	public function getCondition()
	{
		return $this->condition;
	}

	/**
	 *
	 * @param \Rbs\Notification\Condition $condition
	 * @return \Rbs\Notification\Notification
	 */
	public function setCondition(Condition $condition)
	{
		$this->condition = $condition;
		return $this;
	}

	/**
	 *
	 * @return \Rbs\Notification\Notification
	 */
	public function unsetCondition()
	{
		$this->condition = false;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getEvents()
	{
		return $this->events;
	}

	/**
	 *
	 * @return array
	 */
	public function setEvents($array)
	{
		$this->events = $array;
		return $this;
	}

	/**
	 *
	 * @param string $event
	 * @return \Rbs\Notification\Notification
	 */
	public function addEvent($event)
	{
		$this->events[] = $event;
		return $this;
	}

	/**
	 *
	 * @param [OPTIONAL] boolean $asId
	 * @return \Rbplm\Any
	 */
	public function getReference($asId = false)
	{
		if ( $asId ) {
			return $this->referenceUid;
		}
		return $this->reference;
	}

	/**
	 *
	 * @param \Rbplm\Any $reference
	 * @return \Rbs\Notification\Notification
	 */
	public function setReference($reference)
	{
		$this->reference = $reference;
		$this->referenceId = $reference->getId();
		$this->referenceUid = $reference->getUid();
		return $this;
	}
}

