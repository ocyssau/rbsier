<?php
//%LICENCE_HEADER%
namespace Rbs\EventDispatcher;

use Application\Console\Output as Cli;

//use Application\Model\DataTestFactory as DataFactory;
//use Rbs\Space\Factory as DaoFactory;
//use Rbs\Ged\Document\ListenerProviderFactory;
//use Rbs\EventDispatcher\Dispatcher;
//use Rbs\EventDispatcher\EventManager;
//use Rbs\Ged\Document\Event;

/**
 * Writer for Powerpoint files
 *
 * @see \Application\Model\AbstractTest
 * 
 * How to use :
 * 			
 * 			php public/index.php test /Rbs/EventDispatcher/EventManager run
 * 
 */
class EventManagerTest extends \Application\Model\AbstractTest
{

	/**
	 *
	 */
	public function runTest()
	{
		$listenerProvider = new ListenerProvider();
		$listenerProvider->addListener(function (Event $event) {
			Cli::blue('Run listener for event : ' . $event->getName());
			return $event;
		});
		$listenerProvider->addListener(function (Event $event) {
			Cli::blue('Run 2nd listener for event : ' . $event->getName());
			return $event;
		});

		$eventManager = new EventManager(new Dispatcher([
			$listenerProvider
		]));

		$event = new Event('For Test', $this);
		foreach( $listenerProvider->getListenersForEvent($event) as $validListener ) {
			Cli::yellow(var_export($validListener, true));
		}

		$eventManager->trigger($event);

		$listenerProvider2 = new ListenerProvider();
		$listenerProvider2->addListener(function (Event $event) {
			Cli::blue('Run listener2 for event : ' . $event->getName());
			return $event;
		});

		$eventManager->getDispatcher()->addListenerProvider($listenerProvider2);
		$eventManager->trigger($event);
	}
} /* End of class*/
