<?php
namespace Rbs\EventDispatcher;

//use Rbs\EventDispatcher\EventManager;

/**
 * 
 * @author ocyssau
 *
 */
interface EventManagerAwareInterface
{
	/**
	 *
	 * @param EventManager $eventManager
	 */
	public function setEventManager(EventManager $eventManager);
	
	/**
	 *
	 * @return EventManager
	 */
	public function getEventManager();
	
}
