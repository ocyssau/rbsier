<?php
namespace Rbs\EventDispatcher;

/**
 * Compose Dispatcher and instances of Event
 *
 */
class EventManager
{

	/**
	 *
	 * @var EventInterface
	 */
	protected $event;

	/**
	 *
	 * @var Dispatcher
	 */
	protected $dispatcher;

	/**
	 *
	 * @param Dispatcher $dispatcher
	 */
	public function __construct(Dispatcher $dispatcher = null)
	{
		$this->dispatcher = $dispatcher;
	}
	
	/**
	 *
	 * @param Event $event
	 * @return Event
	 */
	public function trigger(EventInterface $event)
	{
		if($this->dispatcher){
			return $this->dispatcher->dispatch($event);
		}
		else{
			return $event;
		}
	}

	/**
	 *
	 * @param Dispatcher $dispatcher
	 */
	public function setDispatcher($dispatcher)
	{
		$this->dispatcher = $dispatcher;
		return $this;
	}

	/**
	 *
	 * @return Dispatcher
	 */
	public function getDispatcher()
	{
		if(!isset($this->dispatcher)){
			$this->dispatcher = new Dispatcher();
		}
		return $this->dispatcher;
	}
}
