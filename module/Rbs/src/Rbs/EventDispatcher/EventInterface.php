<?php
namespace Rbs\EventDispatcher;

interface EventInterface
{
	/**
	 *
	 * @return object
	 */
	public function getEmitter();
	
	/**
	 *
	 * @return string
	 */
	public function getName(): String;
	
	/**
	 *
	 * @param string $name
	 * @return EventInterface
	 */
	public function setName(string $name);
	
}
