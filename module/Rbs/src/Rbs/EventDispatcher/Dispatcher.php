<?php
namespace Rbs\EventDispatcher;


/**
 * The main dispatcher, handles everything
 */
class Dispatcher implements EventDispatcherInterface
{

	/**
	 * @var ListenerProviderInterface[]
	 */
	private $listenerProviders;

	/**
	 * @param array|null $listenerProviders
	 */
	public function __construct(array $listenerProviders = null)
	{
		$this->listenerProviders = $listenerProviders ?? [];
	}

	/**
	 * @return array|null
	 */
	public function getListenerProviders()
	{
		return $this->listenerProviders;
	}
	
	/**
	 * @return Dispatcher
	 */
	public function addListenerProvider(ListenerProviderInterface $listenerProvider)
	{
		$this->listenerProviders[] = $listenerProvider;
		return $this;
	}
	
	/**
	 * @inheritdoc
	 */
	public function dispatch(EventInterface $event)
	{
		foreach( $this->listenerProviders as $listenerProvider ) {
			foreach( $listenerProvider->getListenersForEvent($event) as $listener ) {
				if ( $event instanceof StoppableEventInterface && $event->isPropagationStopped() ) {
					break;
				}
				$resultingEvent = call_user_func_array($listener, [&$event]);
				
				if ( $resultingEvent instanceof \Rbs\EventDispatcher\Event ) {
					$event = $resultingEvent;
				}
			}
		}
		return $event;
	}
}