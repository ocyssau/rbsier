<?php
namespace Rbs\EventDispatcher;

/**
 * Easy listener provider to add custom listeners at any time.
 * No prioritization / sorting is taken care of.
 */
class ListenerProvider implements ListenerProviderInterface
{
	protected $listeners = [];
	
	/**
	 * @param $listener
	 */
	public function addListener($listener)
	{
		$this->listeners[] = $listener;
		return $this;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getListenersForEvent(EventInterface $event): \Generator
	{
		foreach ($this->listeners as $listener) {
			$eventType = null;
			if (is_string($listener) || is_callable($listener)) {
				$r = new \ReflectionFunction($listener);
				$eventType = $r->getParameters()[0]->getClass();
			} elseif (is_array($listener)) {
				if (is_string($listener[0])) {
					$r = new \ReflectionClass($listener[0]);
					$eventType = $r->getMethod($listener[1])->getParameters()[0]->getClass();
				}
			}
			if ($eventType === null) {
				throw new UnacceptableListenerException(
					'The listener ' . (string)$listener . ' is not suited for being an event listener.',
					1534140615
					);
			}
			if ($event instanceof $eventType->name) {
				yield $listener;
			}
		}
	}
}
