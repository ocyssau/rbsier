<?php
namespace Rbs\EventDispatcher;

/**
 * 
 * @author ocyssau
 *
 */
class StoppableEvent extends Event implements StoppableEventInterface
{
	use StoppableEventTrait;
}
