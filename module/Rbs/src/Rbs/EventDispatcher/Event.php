<?php
namespace Rbs\EventDispatcher;

/**
 * 
 * @author ocyssau
 *
 */
class Event implements EventInterface
{

	/**
	 *
	 * @var string
	 */
	protected $name;

	/**
	 *
	 * @var object
	 */
	protected $emitter;

	/**
	 *
	 * @param object $emitter
	 * @param string $name
	 */
	public function __construct(string $name, $emitter)
	{
		$this->name = $name;
		$this->emitter = $emitter;
	}

	/**
	 *
	 * @return object
	 */
	public function getEmitter()
	{
		return $this->emitter;
	}

	/**
	 *
	 * @return string
	 */
	public function getName(): String
	{
		return $this->name;
	}

	/**
	 *
	 * @param string $name
	 * @return StoppableEvent
	 */
	public function setName(string $name)
	{
		$this->name = $name;
		return $this;
	}
}

