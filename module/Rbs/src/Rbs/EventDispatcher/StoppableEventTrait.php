<?php
namespace Rbs\EventDispatcher;

/**
 * Can be added to any event which a listener can intercept
 */
trait StoppableEventTrait
{

	/**
	 * @var bool
	 */
	private $isPropagationStopped = false;

	/**
	 * @inheritdoc
	 */
	public function stopPropagation(): StoppableEventInterface
	{
		$this->isPropagationStopped = true;
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function isPropagationStopped(): bool
	{
		return $this->isPropagationStopped;
	}
}
