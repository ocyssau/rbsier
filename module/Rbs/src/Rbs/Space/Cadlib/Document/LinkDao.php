<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Cadlib\Document;

use Rbs\Ged\Document\LinkDao as BaseDao;


/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class LinkDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'cadlib_doc_rel';
	
	/**
	 *
	 * @var string
	 */
	public static $vtable = 'cadlib_doc_rel';
	
	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'document_rel_seq';
	
	/**
	 *
	 * @var string
	 */
	public static $childTable = 'cadlib_documents';
	
	/**
	 *
	 * @var string
	 */
	public static $childForeignKey = 'id';
	
	/**
	 *
	 * @var string
	 */
	public static $parentTable = 'cadlib_documents';
	
	/**
	 *
	 * @var string
	 */
	public static $parentForeignKey = 'id';
}
