<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Cadlib\Document;

use Rbs\Ged\Document\VersionDao as BaseDao;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'cadlib_documents';

	protected $_table = 'cadlib_documents';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'cadlib_documents';

	protected $_vtable = 'cadlib_documents';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'cadlib_documents_seq';
}
