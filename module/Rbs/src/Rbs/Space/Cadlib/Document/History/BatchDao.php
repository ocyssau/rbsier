<?php
// %LICENCE_HEADER%
namespace Rbs\Space\Cadlib\Document\History;

/** SQL_SCRIPT>>
<< */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/** SQL_VIEW>>
<< */

/**
 * SQL_DROP>>
 * <<
 */

/**
 * 
 * @author olivier
 */
class BatchDao extends \Rbs\Space\Workitem\Document\History\BatchDao
{

	/** @var string */
	public static $table = 'cadlib_batches';

	/** @var string */
	public static $vtable = 'cadlib_batches';
}
