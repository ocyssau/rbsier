<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Cadlib\Document;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 */
class HistoryDao extends \Rbs\Space\Workitem\Document\HistoryDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'cadlib_documents_history';

	public static $vtable = 'cadlib_documents_history';

	public static $sequenceName = 'cadlib_documents_history_seq';

	public static $sequenceKey = 'id';
}
