<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Cadlib\Container\Link;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 *
 * @author olivier
 *
 */
class PropertyDao extends \Rbs\Org\Project\Link\PropertyDao
{
	/** @var string */
	public static $table = 'cadlib_property_rel';

	/** @var string */
	public static $vtable = 'cadlib_property_rel';

	/** @var string */
	public static $parentTable = 'cadlibs';

	/** @var string */
	public static $childTable = 'cadlib_metadata';
}
