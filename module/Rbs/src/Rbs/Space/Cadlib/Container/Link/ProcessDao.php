<?php
// %LICENCE_HEADER%
namespace Rbs\Space\Cadlib\Container\Link;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 */
class ProcessDao extends \Rbs\Org\Project\Link\ProcessDao
{

	/** @var string */
	public static $table = 'cadlib_process_rel';

	/** @var string */
	public static $vtable = 'cadlib_process_rel';

	/** @var string */
	public static $parentTable = 'cadlibs';

	/** @var string */
	public static $childTable = 'wf_process';

	/** @var string */
	public static $inheritTable = 'project_process_rel';
}
