<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Cadlib\Container\Link;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
<<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 *
 */
class DoctypeDao extends \Rbs\Org\Project\Link\DoctypeDao
{
	/** @var string */
	public static $table = 'cadlib_doctype_rel';

	/** @var string */
	public static $vtable = 'cadlib_doctype_rel';

	/** @var string */
	public static $parentTable = 'cadlibs';

	/** @var string */
	public static $childTable = 'doctypes';
}
