<?php
// %LICENCE_HEADER%
namespace Rbs\Space\Cadlib\Container\Link;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 << */

/** SQL_ALTER>>
 << */

/** SQL_FKEY>>
 <<*/

/**SQL_TRIGGER>>

 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 */
class CategoryDao extends \Rbs\Org\Project\Link\CategoryDao
{

	/** @var string */
	public static $table = 'cadlib_category_rel';

	/** @var string */
	public static $vtable = 'cadlib_category_rel';

	/** @var string */
	public static $parentTable = 'cadlibs';

	/** @var string */
	public static $childTable = 'categories';

	/** @var string */
	public static $inheritTable = 'project_category_rel';
}
