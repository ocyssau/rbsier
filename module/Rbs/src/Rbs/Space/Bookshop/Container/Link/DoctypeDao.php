<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Bookshop\Container\Link;

/**
 *
 *
 */
class DoctypeDao extends \Rbs\Org\Project\Link\DoctypeDao
{
	/** @var string */
	public static $table = 'bookshop_doctype_rel';

	/** @var string */
	public static $vtable = 'view_bookshop_doctype_rel';

	/** @var string */
	public static $parentTable = 'bookshops';

	/** @var string */
	public static $childTable = 'doctypes';
}
