<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Bookshop\Container\Link;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 *
 * @author olivier
 *
 */
class PropertyDao extends \Rbs\Org\Project\Link\PropertyDao
{
	/** @var string */
	public static $table = 'bookshop_property_rel';

	/** @var string */
	public static $vtable = 'bookshop_property_rel';

	/** @var string */
	public static $parentTable = 'bookshops';

	/** @var string */
	public static $childTable = 'bookshop_metadata';
}

