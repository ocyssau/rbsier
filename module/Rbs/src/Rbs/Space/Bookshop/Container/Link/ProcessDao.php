<?php
// %LICENCE_HEADER%
namespace Rbs\Space\Bookshop\Container\Link;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * 
 *
 */
class ProcessDao extends \Rbs\Org\Project\Link\ProcessDao
{

	/** @var string */
	public static $table = 'bookshop_process_rel';

	/** @var string */
	public static $vtable = 'bookshop_process_rel';

	/** @var string */
	public static $parentTable = 'bookshops';

	/** @var string */
	public static $childTable = 'wf_process';

	/** @var string */
	public static $inheritTable = 'project_process_rel';
}
