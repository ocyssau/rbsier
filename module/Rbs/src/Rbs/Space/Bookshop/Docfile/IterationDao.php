<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Bookshop\Docfile;

/** SQL_SCRIPT>>
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class IterationDao extends \Rbs\Ged\Docfile\IterationDao
{
	/**
	 * @var string
	 */
	public static $table='bookshop_doc_files_versions';
	public static $vtable='bookshop_doc_files_versions';
	public static $sequenceName = 'bookshop_doc_files_versions_seq';
}
