<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Bookshop\Document;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 */
class HistoryDao extends \Rbs\Space\Workitem\Document\HistoryDao
{

	/** @var string */
	public static $table = 'bookshop_documents_history';

	/** @var string */
	public static $vtable = 'bookshop_documents_history';

	/** @var string */
	public static $sequenceName = 'bookshop_documents_history_seq';

	/** @var string */
	public static $sequenceKey = 'id';
}
