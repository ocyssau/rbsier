<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Bookshop\Document;

use Rbs\Ged\Document\VersionDao as BaseDao;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'bookshop_documents';

	protected $_table = 'bookshop_documents';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'bookshop_documents';

	protected $_vtable = 'bookshop_documents';

	/**
	 * @var string
	 */
	public static $sequenceName = 'bookshop_documents_seq';
}
