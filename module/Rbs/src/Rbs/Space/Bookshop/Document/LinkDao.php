<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Bookshop\Document;

use Rbs\Ged\Document\LinkDao as BaseDao;


/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class LinkDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'bookshop_doc_rel';
	
	/**
	 *
	 * @var string
	 */
	public static $vtable = 'bookshop_doc_rel';
	
	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'document_rel_seq';
	
	/**
	 *
	 * @var string
	 */
	public static $childTable = 'bookshop_documents';
	
	/**
	 *
	 * @var string
	 */
	public static $childForeignKey = 'id';
	
	/**
	 *
	 * @var string
	 */
	public static $parentTable = 'bookshop_documents';
	
	/**
	 *
	 * @var string
	 */
	public static $parentForeignKey = 'id';
	
}
