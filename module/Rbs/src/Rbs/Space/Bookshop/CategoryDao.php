<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Bookshop;

use Rbs\Ged\CategoryDao as BaseDao;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class CategoryDao extends BaseDao
{

	/** @var string */
	public static $table = 'bookshop_categories';

	/** @var string */
	public static $vtable = 'bookshop_categories';
}
