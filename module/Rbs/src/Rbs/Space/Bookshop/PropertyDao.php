<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Bookshop;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Entity of a property definition.
 */
class PropertyDao extends \Rbs\Extended\PropertyDao
{

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'bookshop_metadata';

	public static $sequenceName = 'bookshop_metadata_seq';

	public static $sequenceKey = 'id';
}
