<?php
namespace Rbs\Space;

use Rbs\Dao\Loader;
use Rbs\Dao\Sier\MetamodelTranslator;

/**
 * Factory to build and init Dao and others objects dependants of spaces
 * 
 * - Contains map to define class to construct for each model classes
 * - Container $services array to keep reference to space dependents services
 * - Manage spacename and spaceid associations
 * - Keep $connexion, a reference to the current PDO connexion
 * - Implement multi-singleton pattern. Use Factory::get($name) to get valid instance for space name $name.
 * - Use the constructor directly only if you want rebuild a new instance.
 *
 */
class Factory
{

	/**
	 * The name of the last used factory
	 * @var string
	 */
	public static $last;

	/**
	 * Registry for services specifics to one space
	 * @var array
	 */
	public $services;

	/**
	 *
	 * @var string SpaceId as define in Space
	 */
	protected $id;

	/**
	 * SpaceName
	 * @var string
	 */
	protected $name;

	/**
	 * Registry of Dao instances. Key is classId
	 * @var array
	 */
	protected $registry;

	/**
	 *
	 * @var Loader
	 */
	protected $loader;

	/**
	 *
	 * @var \PDO
	 */
	protected static $connexion;

	/**
	 * Array of map buizness object to Dao class to instanciate and connexion to use.
	 * map key is the class id
	 * value is array where
	 * - key 0 is dao class name
	 * - key 1 is table name
	 * - key 2 is model class
	 *
	 * @var array
	 */
	protected static $map = array();

	/**
	 * 
	 * @var array
	 */
	protected $spacemap = array();

	/**
	 * Multi-Singleton
	 * @var Factory
	 */
	protected static $instances;

	/**
	 *
	 * Array of map buizness object to Dao class to instanciate and connexion to use.
	 * @var array
	 */
	protected static $options = array(
		'listclass' => '\Rbs\Dao\Sier\DaoList',
		'filterclass' => '\Rbs\Dao\Sier\Filter',
		'translatorclass' => '\Rbs\Dao\Sier\MetamodelTranslator'
	);

	/**
	 *
	 * @param string $name
	 */
	public function __construct($name)
	{
		if ( !$name ) {
			throw new \BadMethodCallException('$name is not setted');
		}
		$this->name = strtolower($name);
		$this->id = Space::getIdFromName($this->name);
		$this->spacemap = array_merge(static::$map['all'], static::$map[$this->name]);
		$this->registry = [];
		$this->services = [];
		self::$instances[$this->name] = $this;
		self::$last = $this->name;
	}
	
	/**
	 *
	 * Multi-singleton
	 *
	 * @param string $name
	 */
	public static function get($name = 'default')
	{
		($name) ? null : $name = 'default';
		$name = strtolower($name);
		if ( !isset(self::$instances[$name]) ) {
			new self($name);
		}
		self::$last = $name;
		return self::$instances[$name];
	}
	
	/**
	 *
	 * @param $conn \PDO
	 */
	public static function setConnexion(\PDO $conn)
	{
		static::$connexion = $conn;
	}

	/**
	 *
	 * @return \PDO
	 */
	public static function getConnexion()
	{
		return static::$connexion;
	}

	/**
	 * Set an option staticaly = valid for all instances of Factory
	 * 
	 * @param string $key
	 * @param string $value
	 */
	public static function setOption($key, $value)
	{
		$key = strtolower($key);
		static::$options[$key] = $value;
	}

	/**
	 *
	 * @param array $config		Is array of map buizness object to Dao class to instanciate and connexion to use.
	 */
	public static function setMap(array $map)
	{
		static::$map = $map;
	}

	/**
	 *
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 *
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $classId
	 * @return \Rbs\Dao\Sier
	 */
	public function getDao($classId)
	{
		if ( !isset($this->registry[$classId]) ) {
			$map = $this->spacemap;

			$daoClass = $map[$classId][0];
			$dao = new $daoClass(static::$connexion);

			/*keep a reference to the factory*/
			$dao->setFactory($this);
			$this->registry[$classId] = $dao;
		}

		return $this->registry[$classId];
	}

	/**
	 * @param \Rbplm\Any $any
	 * @return \Rbs\Dao\Sier
	 * @deprecated
	 */
	public function getDaoFromObject(\Rbplm\Any $any)
	{
		$classId = $any->cid;
		return $this->getDao($classId);
	}

	/**
	 * Return a instance of class specified by id.
	 *
	 * @param integer $classId
	 * @return \Rbplm\Any
	 */
	public function getModel($classId)
	{
		$class = $this->spacemap[$classId][2];
		return new $class();
	}

	/**
	 * Return a initialized instance of class specified by id.
	 *
	 * @return \Rbplm\AnyPermanent
	 */
	public function getNewModel($classId)
	{
		$class = $this->spacemap[$classId][2];
		return $class::init();
	}

	/**
	 * @return \Rbs\Dao\Sier\DaoList
	 */
	public function getList($classId)
	{
		$dao = $this->getDao($classId);
		$table = $this->spacemap[$classId][1];
		$listClass = static::$options['listclass'];
		$list = new $listClass($table, static::$connexion);
		$list->setOption('dao', $dao);
		$list->dao = $dao;
		/*keep a reference to the factory*/
		$list->factory = $this;
		return $list;
	}

	/**
	 * @return \Rbs\Dao\Sier\Filter
	 */
	public function getFilter($classId)
	{
		$filterClass = static::$options['filterclass'];
		$filter = new $filterClass();
		$filter->setOption('translator', self::$options['translatorclass']);
		return $filter;
	}

	/**
	 * @param \Rbs\Dao\Sier $dao
	 * @return MetamodelTranslator
	 */
	public function getTranslator(\Rbs\Dao\Sier $dao)
	{
		return $dao->getTranslator();
	}

	/**
	 * @return string
	 */
	public function getDaoClass($classId)
	{
		return $this->spacemap[$classId][0];
	}

	/**
	 * @return string
	 */
	public function getModelClass($classId)
	{
		return $this->spacemap[$classId][2];
	}

	/**
	 * @return string
	 */
	public function getTable($classId)
	{
		return $this->spacemap[$classId][1];
	}

	/**
	 *
	 * @param integer $cid
	 */
	public function getMap($cid = null)
	{
		if ( $cid ) {
			return $this->spacemap[$cid];
		}
		else {
			return $this->spacemap;
		}
	}

	/**
	 * @return \Rbs\Dao\Loader
	 */
	public function getLoader()
	{
		if ( !isset($this->loader) ) {
			$this->loader = new Loader($this);
		}
		return $this->loader;
	}

	/**
	 * Erase all objects recorded in registry, or just the $object.
	 *
	 * @param \Rbplm\Dao\DaoInterface $object
	 * @return void
	 */
	public function reset($object = null)
	{
		if ( $object ) {
			foreach( static::$registry as $key => $in ) {
				if ( $in == $object ) {
					unset(static::$registry[$key]);
					break;
				}
			}
		}
		else {
			static::$registry = array();
		}
	}
}
