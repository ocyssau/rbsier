<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Mockup\Docfile;

/** SQL_SCRIPT>>
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class IterationDao extends \Rbs\Ged\Docfile\IterationDao
{
	/**
	 * @var string
	 */
	public static $table='mockup_doc_files_versions';
	public static $vtable='mockup_doc_files_versions';
	public static $sequenceName = 'mockup_doc_files_versions_seq';
	
}
