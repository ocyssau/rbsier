<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Mockup\Docfile;

use Rbs\Ged\Docfile\VersionDao as BaseDao;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'mockup_doc_files';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'mockup_doc_files';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'mockup_doc_files_seq';
}
