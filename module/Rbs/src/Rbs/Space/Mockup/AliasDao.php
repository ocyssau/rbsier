<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Mockup;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class AliasDao extends \Rbs\Org\Container\AliasDao
{

	/**
	 * @var string
	 */
	public static $table = 'mockup_alias';

	public static $vtable = 'mockup_alias';
}
