<?php
// %LICENCE_HEADER%
namespace Rbs\Space\Mockup\Container\Link;

/**
 * 
 *
 */
class CategoryDao extends \Rbs\Org\Project\Link\CategoryDao
{

	/** @var string */
	public static $table = 'mockup_category_rel';

	/** @var string */
	public static $vtable = 'mockup_category_rel';

	/** @var string */
	public static $parentTable = 'mockups';

	/** @var string */
	public static $childTable = 'categories';
	
	/** @var string */
	public static $inheritTable = 'project_category_rel';
	
}
