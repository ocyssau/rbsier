<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Mockup\Container\Link;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 *
 */
class DoctypeDao extends \Rbs\Org\Project\Link\DoctypeDao
{
	/** @var string */
	public static $table = 'mockup_doctype_rel';

	/** @var string */
	public static $vtable = 'mockup_doctype_rel';

	/** @var string */
	public static $parentTable = 'mockups';

	/** @var string */
	public static $childTable = 'doctypes';
}
