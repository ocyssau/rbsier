<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Mockup\Container\Link;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 *
 */
class PropertyDao extends \Rbs\Org\Project\Link\PropertyDao
{

	/** @var string */
	public static $table = 'mockup_metadata_rel';

	/** @var string */
	public static $vtable = 'mockup_metadata_rel';

	/** @var string */
	public static $parentTable = 'mockups';

	/** @var string */
	public static $childTable = 'mockup_metadata';
}
