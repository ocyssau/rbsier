<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Mockup\Document;

use Rbs\Ged\Document\LinkDao as BaseDao;

/** SQL_SCRIPT>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class LinkDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'mockup_doc_rel';
	
	/**
	 *
	 * @var string
	 */
	public static $vtable = 'mockup_doc_rel';
	
	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'document_rel_seq';
	
	/**
	 *
	 * @var string
	 */
	public static $childTable = 'mockup_documents';
	
	/**
	 *
	 * @var string
	 */
	public static $childForeignKey = 'id';
	
	/**
	 *
	 * @var string
	 */
	public static $parentTable = 'mockup_documents';
	
	/**
	 *
	 * @var string
	 */
	public static $parentForeignKey = 'id';
}
