<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Mockup\Document;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 */
class HistoryDao extends \Rbs\Space\Workitem\Document\HistoryDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'mockup_documents_history';

	public static $vtable = 'mockup_documents_history';

	public static $sequenceName = 'mockup_documents_history_seq';

	public static $sequenceKey = 'id';
}
