<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Mockup\Document;

use Rbs\Ged\Document\VersionDao as BaseDao;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>

 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'mockup_documents';

	protected $_table = 'mockup_documents';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'mockup_documents';

	protected $_vtable = 'mockup_documents';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'mockup_documents_seq';
}
