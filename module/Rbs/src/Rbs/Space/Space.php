<?php

namespace Rbs\Space;

/**
 * This class manage the space.
 *
 */
class Space
{
	
	static $classId = '64bdd156af045';
	
	const NAME_DEFAULT = 'default';
	const NAME_PRODUCT = 'product';
	const NAME_WORKITEM = 'workitem';
	const NAME_MOCKUP = 'mockup';
	const NAME_BOOKSHOP = 'bookshop';
	const NAME_CADLIB = 'cadlib';
	
	const ID_DEFAULT = 0;
	const ID_PRODUCT = 1;
	const ID_WORKITEM = 10;
	const ID_MOCKUP = 15;
	const ID_BOOKSHOP = 20;
	const ID_CADLIB = 25;
	
	static protected $idToName = [
		1=>self::NAME_PRODUCT,
		10=>self::NAME_WORKITEM,
		15=>self::NAME_MOCKUP,
		20=>self::NAME_BOOKSHOP,
		25=>self::NAME_CADLIB,
		0=>self::NAME_DEFAULT
	];
	
	static protected $nameToId = [
		self::NAME_PRODUCT=>1,
		self::NAME_WORKITEM=>10,
		self::NAME_MOCKUP=>15,
		self::NAME_BOOKSHOP=>20,
		self::NAME_CADLIB=>25,
		self::NAME_DEFAULT=>0
	];
	
	/**
	 * @var string
	 */
	public $cid = '64bdd156af045';

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $id;

	/**
	 *
	 * @param string $objectType
	 */
	function __construct($name)
	{
		$this->name = $name;
		$this->id = self::getIdFromName($name);
	}
	
	/**
	 *
	 */
	public static function getNames()
	{
		return self::$idToName;
	}
	
	/**
	 *
	 */
	public static function getIds()
	{
		return self::$nameToId;
	}
	

	/**
	 *
	 * @param integer $id
	 */
	public static function getNameFromId($id)
	{
		return self::$idToName[$id];
	}

	/**
	 *
	 * @param string $name
	 */
	public static function getIdFromName($name)
	{
		return self::$nameToId[($name)];
	}

	/**
	 *
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 *
	 */
	public function getId()
	{
		return $this->id;
	}

}

