<?php
//%LICENCE_HEADER%

namespace Rbs\Space;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT >>

CREATE TABLE IF NOT EXISTS `spaces` (
  `id` int(11) NOT NULL,
  `uid` VARCHAR(64) NOT NULL DEFAULT '',
  `cid` VARCHAR(64) NOT NULL DEFAULT '64bdd156af045',
  `name` VARCHAR(255) NOT NULL DEFAULT '',
  `designation` VARCHAR(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_spaces_uid` (`uid`),
  KEY `K_spaces_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 <<*/

/** SQL_INSERT>>
-- DELETE FROM `spaces`;
INSERT INTO `spaces`
(`id`,
`uid`,
`name`,
`designation`)
VALUES
(0,
'default156f045space',
'default',
'default'),
(1,
'product56af045space',
'product',
'products'),
(10,
'wibdd156af045space',
'workitem',
'workitems'),
(15,
'mockupd156f045space',
'mockup',
'mockup'),
(20,
'bookshop156f045space',
'bookshop',
'bookshop'),
(25,
'cadlib156f045space',
'cadlib',
'cadlib');
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class SpaceDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='spaces';

	/**
	 *
	 * @var string
	 */
	public static $vtable='spaces';

	/**
	 * @var string
	 */
	public static $sequenceName='';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'name'=>'name',
		'cid'=>'cid',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
	);
}
