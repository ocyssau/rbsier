<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Workitem;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `workitem_alias` (
 `alias_id` INT(11) NOT NULL ,
 `uid` VARCHAR(128)  NOT NULL,
 `container_id` INT(11) NOT NULL,
 `number` VARCHAR(128)  NOT NULL,
 `name` VARCHAR(128) NOT NULL,
 `designation` VARCHAR(128)  default NULL,
 `cid` VARCHAR(16)  NOT NULL default '45c84a5zalias',
 `created` DATETIME DEFAULT NULL,
 `create_by_id` INT(11) DEFAULT NULL,
 `create_by_uid` VARCHAR(128) DEFAULT NULL,
 PRIMARY KEY  (`alias_id`),
 KEY (`uid`),
 KEY (`container_id`),
 KEY (`number`),
 KEY (`cid`)
 ) ENGINE=InnoDB ;

 CREATE TABLE IF NOT EXISTS `bookshop_alias` LIKE `workitem_alias`;
 CREATE TABLE IF NOT EXISTS `cadlib_alias` LIKE `workitem_alias`;
 CREATE TABLE IF NOT EXISTS `mockup_alias` LIKE `workitem_alias`;

 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>

 ALTER TABLE `workitem_alias` 
 ADD CONSTRAINT `FK_workitem_alias_1`
 FOREIGN KEY (`container_id`)
 REFERENCES `workitems` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE;
 
 ALTER TABLE `bookshop_alias` 
 ADD CONSTRAINT `FK_bookshop_alias_1`
 FOREIGN KEY (`container_id`)
 REFERENCES `bookshops` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE;

 ALTER TABLE `cadlib_alias` 
 ADD CONSTRAINT `FK_cadlib_alias_1`
 FOREIGN KEY (`container_id`)
 REFERENCES `cadlibs` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE;

 ALTER TABLE `mockup_alias` 
 ADD CONSTRAINT `FK_mockup_alias_1`
 FOREIGN KEY (`container_id`)
 REFERENCES `mockups` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE;


 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class AliasDao extends \Rbs\Org\Container\AliasDao
{

	/**
	 * @var string
	 */
	public static $table = 'workitem_alias';

	public static $vtable = 'workitem_alias';
}
