<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Docfile;

use Rbs\Ged\Docfile\VersionDao as BaseDao;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `workitem_doc_files` (
 `id` int(11) NOT NULL,
 `uid` VARCHAR(140) NOT NULL UNIQUE,
 `cid` VARCHAR(16) NOT NULL DEFAULT '569e92b86d248',
 `document_id` INT(11) NOT NULL,
 `document_uid` VARCHAR(64) NULL,
 `name` VARCHAR(255) NOT NULL,
 `file_used_name` VARCHAR(128) NULL DEFAULT NULL,
 `path` MEDIUMTEXT NOT NULL,
 `iteration` INT(11) NOT NULL DEFAULT '1',
 `acode` INT(11) NOT NULL DEFAULT '0',
 `root_name` VARCHAR(255) NOT NULL,
 `extension` VARCHAR(16) DEFAULT NULL,
 `life_stage` VARCHAR(64) NOT NULL DEFAULT 'init',
 `type` VARCHAR(128) NOT NULL,
 `size` INT(11) DEFAULT NULL,
 `mtime` DATETIME DEFAULT NULL,
 `md5` VARCHAR(128) DEFAULT NULL,
 `locked` DATETIME DEFAULT NULL,
 `lock_by_id` INT(11) DEFAULT NULL,
 `lock_by_uid` VARCHAR(64) DEFAULT NULL,
 `created` DATETIME NOT NULL,
 `create_by_id` INT(11) NOT NULL,
 `create_by_uid` VARCHAR(64) DEFAULT NULL,
 `updated` DATETIME DEFAULT NULL,
 `update_by_id` INT(11) DEFAULT NULL,
 `update_by_uid` VARCHAR(64) DEFAULT NULL,
 `mainrole` INT(2) NOT NULL DEFAULT 1,
 `roles` TEXT DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY (`name`,`path`(512)),
 UNIQUE KEY (`name`,`path`(128)),
 KEY (`document_id`),
 KEY (`name`),
 KEY (`path`(128)),
 KEY (`iteration`,`document_id`),
 KEY (`mainrole`),
 KEY (`roles`(32)),
 KEY (`create_by_uid`),
 KEY (`lock_by_uid`),
 KEY (`update_by_uid`),
 FULLTEXT INDEX (`name` ASC)
 ) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bookshop_doc_files` LIKE `workitem_doc_files`;
CREATE TABLE IF NOT EXISTS `cadlib_doc_files` LIKE `workitem_doc_files`;
CREATE TABLE IF NOT EXISTS `mockup_doc_files` LIKE `workitem_doc_files`;

CREATE TABLE IF NOT EXISTS `workitem_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `workitem_doc_files_seq` (`id`) VALUES ('10');

CREATE TABLE IF NOT EXISTS `bookshop_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `bookshop_doc_files_seq` (`id`) VALUES ('10');

CREATE TABLE IF NOT EXISTS `cadlib_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `cadlib_doc_files_seq` (`id`) VALUES ('10');

CREATE TABLE IF NOT EXISTS `mockup_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `mockup_doc_files_seq` (`id`) VALUES ('10');

<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE `workitem_doc_files` 
ADD CONSTRAINT `FK_workitem_doc_files_1`
  FOREIGN KEY (`document_id`)
  REFERENCES `workitem_documents` (`id`)
  ON UPDATE CASCADE;

ALTER TABLE `bookshop_doc_files` 
ADD CONSTRAINT `FK_bookshop_doc_files_1`
  FOREIGN KEY (`document_id`)
  REFERENCES `bookshop_documents` (`id`)
  ON UPDATE CASCADE;

ALTER TABLE `cadlib_doc_files` 
ADD CONSTRAINT `FK_cadlib_doc_files_1`
  FOREIGN KEY (`document_id`)
  REFERENCES `cadlib_documents` (`id`)
  ON UPDATE CASCADE;

ALTER TABLE `mockup_doc_files` 
ADD CONSTRAINT `FK_mockup_doc_files_1`
  FOREIGN KEY (`document_id`)
  REFERENCES `mockup_documents` (`id`)
  ON UPDATE CASCADE;

 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>

-- ON DOCFILE DELETE --
DROP TRIGGER IF EXISTS `onWIDocfileDelete`; 
DELIMITER $$
CREATE TRIGGER `onWIDocfileDelete` AFTER DELETE ON `workitem_doc_files` FOR EACH ROW 
BEGIN
	DELETE FROM `checkout_index` WHERE `file_id`=OLD.`id` AND `spacename`='workitem';
END;$$
DELIMITER ;

-- ON DOCFILE UPDATE --
DROP TRIGGER IF EXISTS onWIDocfileUpdate; 
DELIMITER $$
CREATE TRIGGER onWIDocfileUpdate AFTER UPDATE ON workitem_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `workitem_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'workitem',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END$$
DELIMITER ;

-- ON DOCFILE INSERT --
DROP TRIGGER IF EXISTS onWIDocfileInsert;
DELIMITER $$
CREATE TRIGGER onWIDocfileInsert AFTER INSERT ON workitem_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF NEW.acode = 1 THEN
		SELECT 
			`container_id`,`container_uid`,`id`,`designation` 
            INTO 
            containerId, containerNumber, documentId, designation
			FROM `workitem_documents`
			WHERE `id`=NEW.document_id;
		
		INSERT INTO `checkout_index`
		(
		`file_name`,
		`lock_by_id`,
		`lock_by_uid`,
		`file_id`,
		`file_uid`,
		`designation`,
		`spacename`,
		`container_id`,
		`container_number`,
		`document_id`
		)
		VALUES
        (
			NEW.name,
			NEW.lock_by_id,
			NEW.lock_by_uid,
			NEW.id,
			NEW.uid,
			`designation`,
			'workitem',
			`containerId`,
			`containerNumber`,
			`documentId`
		);        
	END IF;
END$$
DELIMITER ;

-- ************BOOKSHOPS *******************************

DROP TRIGGER IF EXISTS onBookshopDocfileDelete; 
delimiter $$
CREATE TRIGGER onBookshopDocfileDelete AFTER DELETE ON bookshop_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='bookshop';
END;$$
delimiter ;


-- ON DOCFILE UPDATE --
DROP TRIGGER IF EXISTS onBookshopDocfileUpdate; 
DELIMITER $$
CREATE TRIGGER onBookshopDocfileUpdate AFTER UPDATE ON bookshop_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `bookshop_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'bookshop',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END$$
DELIMITER ;


-- ON DOCFILE INSERT --
DROP TRIGGER IF EXISTS onBookshopDocfileInsert;
DELIMITER $$
CREATE TRIGGER onBookshopDocfileInsert AFTER INSERT ON bookshop_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF NEW.acode = 1 THEN
		SELECT 
			`container_id`,`container_uid`,`id`,`designation` 
            INTO 
            containerId, containerNumber, documentId, designation
			FROM `bookshop_documents`
			WHERE `id`=NEW.document_id;
		
		INSERT INTO `checkout_index`
		(
		`file_name`,
		`lock_by_id`,
		`lock_by_uid`,
		`file_id`,
		`file_uid`,
		`designation`,
		`spacename`,
		`container_id`,
		`container_number`,
		`document_id`
		)
		VALUES
        (
			NEW.name,
			NEW.lock_by_id,
			NEW.lock_by_uid,
			NEW.id,
			NEW.uid,
			`designation`,
			'bookshop',
			`containerId`,
			`containerNumber`,
			`documentId`
		);        
	END IF;
END$$
DELIMITER ;

-- ************CADLIB *******************************

DROP TRIGGER IF EXISTS onCadlibDocfileDelete; 
delimiter $$
CREATE TRIGGER onCadlibDocfileDelete AFTER DELETE ON cadlib_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='cadlib';
END;$$
delimiter ;

-- ON DOCFILE UPDATE --
DROP TRIGGER IF EXISTS onCadlibDocfileUpdate; 
DELIMITER $$
CREATE TRIGGER onCadlibDocfileUpdate AFTER UPDATE ON cadlib_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `cadlib_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'cadlib',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END$$
DELIMITER ;


-- ON DOCFILE INSERT --
DROP TRIGGER IF EXISTS onCadlibDocfileInsert;
DELIMITER $$
CREATE TRIGGER onCadlibDocfileInsert AFTER INSERT ON cadlib_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF NEW.acode = 1 THEN
		SELECT 
			`container_id`,`container_uid`,`id`,`designation` 
            INTO 
            containerId, containerNumber, documentId, designation
			FROM `cadlib_documents`
			WHERE `id`=NEW.document_id;
		
		INSERT INTO `checkout_index`
		(
		`file_name`,
		`lock_by_id`,
		`lock_by_uid`,
		`file_id`,
		`file_uid`,
		`designation`,
		`spacename`,
		`container_id`,
		`container_number`,
		`document_id`
		)
		VALUES
        (
			NEW.name,
			NEW.lock_by_id,
			NEW.lock_by_uid,
			NEW.id,
			NEW.uid,
			`designation`,
			'cadlib',
			`containerId`,
			`containerNumber`,
			`documentId`
		);        
	END IF;
END$$
DELIMITER ;

-- ************ MOCKUP  *******************************
DROP TRIGGER IF EXISTS onMockupDocfileDelete; 
delimiter $$
CREATE TRIGGER onMockupDocfileDelete AFTER DELETE ON mockup_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='mockup';
END;$$
delimiter ;

<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{
	/**
	 *
	 * @var string
	 */
	public static $table='workitem_doc_files';

	/**
	 *
	 * @var string
	 */
	public static $vtable='workitem_doc_files';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'workitem_doc_files_seq';
}
