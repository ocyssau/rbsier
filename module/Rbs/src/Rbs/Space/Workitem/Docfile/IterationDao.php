<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Docfile;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `workitem_doc_files_versions` LIKE `workitem_doc_files`;
CREATE TABLE IF NOT EXISTS `bookshop_doc_files_versions` LIKE `bookshop_doc_files`;
CREATE TABLE IF NOT EXISTS `cadlib_doc_files_versions` LIKE `cadlib_doc_files`;
CREATE TABLE IF NOT EXISTS `mockup_doc_files_versions` LIKE `mockup_doc_files`;
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
ALTER TABLE `workitem_doc_files_versions`
	ADD COLUMN `of_file_id` INT(11) NULL AFTER `id`,
	ADD CONSTRAINT `FK_workitem_doc_files_versions_1` FOREIGN KEY (`of_file_id`) REFERENCES `workitem_doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD KEY (`of_file_id`);

ALTER TABLE `bookshop_doc_files_versions`
	ADD COLUMN `of_file_id` INT(11) NULL AFTER `id`,
	ADD CONSTRAINT `FK_bookshop_doc_files_versions_1` FOREIGN KEY (`of_file_id`) REFERENCES `bookshop_doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD KEY (`of_file_id`);

ALTER TABLE `cadlib_doc_files_versions`
	ADD COLUMN `of_file_id` INT(11) NULL AFTER `id`,
	ADD CONSTRAINT `FK_cadlib_doc_files_versions_1` FOREIGN KEY (`of_file_id`) REFERENCES `cadlib_doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD KEY (`of_file_id`);

ALTER TABLE `mockup_doc_files_versions`
	ADD COLUMN `of_file_id` INT(11) NULL AFTER `id`,
	ADD CONSTRAINT `FK_mockup_doc_files_versions_1` FOREIGN KEY (`of_file_id`) REFERENCES `mockup_doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD KEY (`of_file_id`);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class IterationDao extends \Rbs\Ged\Docfile\IterationDao
{
	/**
	 * @var string
	 */
	public static $table='workitem_doc_files_versions';
	public static $vtable='workitem_doc_files_versions';
	public static $sequenceName = '`workitem_doc_files_versions_seq`';
}
