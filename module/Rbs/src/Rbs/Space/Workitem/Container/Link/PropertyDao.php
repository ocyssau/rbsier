<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Container\Link;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `workitem_property_rel` LIKE `project_property_rel`;
 CREATE TABLE IF NOT EXISTS `bookshop_property_rel` LIKE `project_property_rel`;
 CREATE TABLE IF NOT EXISTS `cadlib_property_rel` LIKE `project_property_rel`;
 CREATE TABLE IF NOT EXISTS `mockup_property_rel` LIKE `project_property_rel`;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
ALTER TABLE `workitem_property_rel`
	ADD CONSTRAINT `FK_workitem_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `workitem_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `bookshop_property_rel`
	ADD CONSTRAINT `FK_bookshop_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `bookshop_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `cadlib_property_rel`
	ADD CONSTRAINT `FK_cadlib_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `cadlib_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `mockup_property_rel`
	ADD CONSTRAINT `FK_mockup_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `mockup_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS `onWorkitemPropertyRelInsert`;
DELIMITER $$
CREATE TRIGGER `onWorkitemPropertyRelInsert` BEFORE INSERT ON `workitem_property_rel` FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS `onBookshopPropertyRelInsert`;
DELIMITER $$
CREATE TRIGGER `onBookshopPropertyRelInsert` BEFORE INSERT ON `bookshop_property_rel` FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS `onCadlibPropertyRelInsert`;
DELIMITER $$
CREATE TRIGGER `onCadlibPropertyRelInsert` BEFORE INSERT ON `cadlib_property_rel` FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS `onMockupPropertyRelInsert`;
DELIMITER $$
CREATE TRIGGER `onMockupPropertyRelInsert` BEFORE INSERT ON `mockup_property_rel` FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 *
 * @author olivier
 *
 */
class PropertyDao extends \Rbs\Org\Project\Link\PropertyDao
{
	/** @var string */
	public static $table = 'workitem_property_rel';

	/** @var string */
	public static $vtable = 'workitem_property_rel';

	/** @var string */
	public static $parentTable = 'workitems';

	/** @var string */
	public static $childTable = 'workitem_metadata';
}
