<?php
// %LICENCE_HEADER%
namespace Rbs\Space\Workitem\Container\Link;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `workitem_category_rel` LIKE `project_category_rel`;
 CREATE TABLE IF NOT EXISTS `bookshop_category_rel` LIKE `project_category_rel`;
 CREATE TABLE IF NOT EXISTS `cadlib_category_rel` LIKE `project_category_rel`;
 CREATE TABLE IF NOT EXISTS `mockup_category_rel` LIKE `project_category_rel`;
 <<*/

/** SQL_ALTER>>
 ALTER TABLE `workitem_category_rel` 
	 ADD CONSTRAINT `FK_workitem_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_workitem_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_workitem_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

 ALTER TABLE `bookshop_category_rel` 
	ADD CONSTRAINT `FK_bookshop_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

ALTER TABLE `cadlib_category_rel` 
	ADD CONSTRAINT `FK_cadlib_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
ALTER TABLE `mockup_category_rel`
	ADD CONSTRAINT `FK_mockup_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
 <<*/


/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 
 -- TRIGGER, Update rdn from parentDn when insert
 -- Note: when update, rdn is updated by constraints
 DROP TRIGGER IF EXISTS onWorkitemCategoryRelInsert;
 DELIMITER $$
 CREATE TRIGGER onWorkitemCategoryRelInsert BEFORE INSERT ON workitem_category_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn VARCHAR(128);
 SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END;$$
 DELIMITER ;
 
 -- TRIGGER, Update rdn from parentDn when insert
 -- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onBookshopCategoryRelInsert;
DELIMITER $$
CREATE TRIGGER onBookshopCategoryRelInsert BEFORE INSERT ON bookshop_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END $$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onCadlibCategoryRelInsert;
DELIMITER $$
CREATE TRIGGER onCadlibCategoryRelInsert BEFORE INSERT ON cadlib_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onMockupCategoryRelInsert;
DELIMITER $$
CREATE TRIGGER onMockupCategoryRelInsert BEFORE INSERT ON mockup_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

 <<*/


/** SQL_VIEW>>
 
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * 
 *
 */
class CategoryDao extends \Rbs\Org\Project\Link\CategoryDao
{

	/** @var \PDOStatement */
	protected $getInheritsFromParentIdStmt;
	
	/** @var string */
	public static $table = 'workitem_category_rel';

	/** @var string */
	public static $vtable = 'workitem_category_rel';

	/** @var string */
	public static $parentTable = 'workitems';

	/** @var string */
	public static $childTable = 'categories';

	/** @var string */
	public static $inheritTable = 'project_category_rel';
}
