<?php
// %LICENCE_HEADER%
namespace Rbs\Space\Workitem\Container\Link;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `workitem_process_rel` LIKE `project_process_rel`;
 CREATE TABLE IF NOT EXISTS `bookshop_process_rel` LIKE `project_process_rel`;
 CREATE TABLE IF NOT EXISTS `cadlib_process_rel` LIKE `project_process_rel`;
 CREATE TABLE IF NOT EXISTS `mockup_process_rel` LIKE `project_process_rel`;

 <<*/
	

/** SQL_ALTER>>
 ALTER TABLE `workitem_process_rel` 
	 ADD CONSTRAINT `FK_workitem_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_workitem_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_workitem_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

 ALTER TABLE `bookshop_process_rel`
	 ADD CONSTRAINT `FK_bookshop_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_bookshop_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_bookshop_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE;

 ALTER TABLE `cadlib_process_rel` 
	 ADD CONSTRAINT `FK_cadlib_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_cadlib_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_cadlib_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;


 ALTER TABLE `mockup_process_rel` 
	 ADD CONSTRAINT `FK_mockup_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_mockup_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_mockup_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 
 -- TRIGGER, Update rdn from parentDn when insert
 -- Note: when update, rdn is updated by constraints
 DROP TRIGGER IF EXISTS onWorkitemProcessRelInsert;
 DELIMITER $$
 CREATE TRIGGER onWorkitemProcessRelInsert BEFORE INSERT ON workitem_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn VARCHAR(128);
 SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END;$$
 DELIMITER ;

 -- TRIGGER, Update rdn from parentDn when insert
 -- Note: when update, rdn is updated by constraints
 DROP TRIGGER IF EXISTS onBookshopProcessRelInsert;
 DELIMITER $$
 CREATE TRIGGER onBookshopProcessRelInsert BEFORE INSERT ON bookshop_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn VARCHAR(128);
 SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END;$$
 DELIMITER ;

 -- TRIGGER, Update rdn from parentDn when insert
 -- Note: when update, rdn is updated by constraints
 DROP TRIGGER IF EXISTS onCadlibProcessRelInsert;
 DELIMITER $$
 CREATE TRIGGER onCadlibProcessRelInsert BEFORE INSERT ON cadlib_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn VARCHAR(128);
 SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END;$$
 DELIMITER ;

 
 -- TRIGGER, Update rdn from parentDn when insert
 -- Note: when update, rdn is updated by constraints
 DROP TRIGGER IF EXISTS onMockupProcessRelInsert;
 DELIMITER $$
 CREATE TRIGGER onMockupProcessRelInsert BEFORE INSERT ON mockup_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn VARCHAR(128);
 SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END;$$
 DELIMITER ;
 
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 */
class ProcessDao extends \Rbs\Org\Project\Link\ProcessDao
{

	/** @var string */
	public static $table = 'workitem_process_rel';

	/** @var string */
	public static $vtable = 'workitem_process_rel';

	/** @var string */
	public static $parentTable = 'workitems';

	/** @var string */
	public static $childTable = 'wf_process';

	/** @var string */
	public static $inheritTable = 'project_process_rel';
}
