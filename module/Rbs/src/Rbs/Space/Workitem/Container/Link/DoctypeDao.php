<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Container\Link;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `workitem_doctype_rel` LIKE `project_doctype_rel`;
CREATE TABLE IF NOT EXISTS `bookshop_doctype_rel` LIKE `project_doctype_rel`;
CREATE TABLE IF NOT EXISTS `cadlib_doctype_rel` LIKE `project_doctype_rel`;
CREATE TABLE IF NOT EXISTS `mockup_doctype_rel` LIKE `project_doctype_rel`;
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>

ALTER TABLE `workitem_doctype_rel`
	ADD CONSTRAINT `FK_workitem_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `bookshop_doctype_rel`
	ADD CONSTRAINT `FK_bookshop_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cadlib_doctype_rel`
	ADD CONSTRAINT `FK_cadlib_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `mockup_doctype_rel`
	ADD CONSTRAINT `FK_mockup_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
 
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER >>

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onWorkitemDoctypeRelInsert;
DELIMITER $$
CREATE TRIGGER onWorkitemDoctypeRelInsert BEFORE INSERT ON workitem_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onBookshopDoctypeRelInsert;
DELIMITER $$
CREATE TRIGGER onBookshopDoctypeRelInsert BEFORE INSERT ON bookshop_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onCadlibDoctypeRelInsert;
DELIMITER $$
CREATE TRIGGER onCadlibDoctypeRelInsert BEFORE INSERT ON cadlib_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onMockupDoctypeRelInsert;
DELIMITER $$
CREATE TRIGGER onMockupDoctypeRelInsert BEFORE INSERT ON mockup_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
DELIMITER ;
<<*/

/** SQL_VIEW>>
CREATE VIEW view_workitem_doctype_rel AS 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM workitem_doctype_rel 
UNION 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;

DROP VIEW IF EXISTS `view_bookshop_doctype_rel`;
CREATE VIEW view_bookshop_doctype_rel AS 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM bookshop_doctype_rel 
UNION 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;

DROP VIEW IF EXISTS `view_cadlib_doctype_rel`;
CREATE VIEW view_cadlib_doctype_rel AS 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM cadlib_doctype_rel 
UNION 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;

DROP VIEW IF EXISTS `view_mockup_doctype_rel`;
CREATE VIEW view_mockup_doctype_rel AS 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM mockup_doctype_rel 
UNION 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;

<<*/

/** SQL_DROP>>
<<*/

/**
 *
 *
 */
class DoctypeDao extends \Rbs\Org\Project\Link\DoctypeDao
{
	/** @var string */
	public static $table = 'workitem_doctype_rel';

	/** @var string */
	public static $vtable = 'view_workitem_doctype_rel';

	/** @var string */
	public static $parentTable = 'workitems';

	/** @var string */
	public static $childTable = 'doctypes';
}
