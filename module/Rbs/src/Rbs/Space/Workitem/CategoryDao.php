<?php
// %LICENCE_HEADER%
namespace Rbs\Space\Workitem;

use Rbs\Ged\CategoryDao as BaseDao;

/**
 * SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `workitem_categories` (
	`id` int(11) NOT NULL DEFAULT '0',
	`uid` VARCHAR(32) NOT NULL,
	`cid` CHAR(13) NOT NULL DEFAULT '569e918a134ca',
	`dn` VARCHAR(128) NULL,
	`number` VARCHAR(128) DEFAULT NULL,
	`designation` text,
	`parent_id` int(11) NULL,
	`parent_uid` VARCHAR(32) NULL,
	`icon` VARCHAR(32) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY (`uid`),
	UNIQUE KEY (`number`),
	UNIQUE KEY (`dn`),
	KEY (`parent_id`),
	KEY (`parent_uid`)
) ENGINE=InnoDB;

<<
 */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */

/**
 */
class CategoryDao extends BaseDao
{
	
	/** @var string */
	public static $table='workitem_categories';
	
	/** @var string */
	public static $vtable='workitem_categories';
	
}
