<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Document;

use Rbs\Ged\Document\LinkDao as BaseDao;


/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class LinkDao extends BaseDao
{
	
	/**
	 *
	 * @var string
	 */
	public static $table = 'workitem_doc_rel';
	
	/**
	 *
	 * @var string
	 */
	public static $vtable = 'workitem_doc_rel';
	
	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'document_rel_seq';
	
	/**
	 *
	 * @var string
	 */
	public static $childTable = 'workitem_documents';
	
	/**
	 *
	 * @var string
	 */
	public static $childForeignKey = 'id';
	
	/**
	 *
	 * @var string
	 */
	public static $parentTable = 'workitem_documents';
	
	/**
	 *
	 * @var string
	 */
	public static $parentForeignKey = 'id';

}
