<?php
// %LICENCE_HEADER%
namespace Rbs\Space\Workitem\Document\History;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
<< */

/** SQL_INSERT>>
<< */

/** SQL_ALTER>>
<< */

/** SQL_FKEY>>
<< */

/** SQL_TRIGGER>>
<< */

/** SQL_VIEW>>

DROP VIEW IF EXISTS `workitem_batches`;
CREATE VIEW `workitem_batches` AS
SELECT DISTINCT
	`action_batch_uid` AS `uid`,
 	`action_name` AS `callback_method`,
	`action_by` AS `owner_uid`,
	`action_started` AS `created`,
	`action_comment` AS `comment`,
	'workitem' AS `spacename`
FROM
	`workitem_documents_history`
WHERE 
	`action_batch_uid` IS NOT NULL;

DROP VIEW IF EXISTS `bookshop_batches`;
CREATE VIEW `bookshop_batches` AS
SELECT 
	`action_batch_uid` AS `uid`,
 	`action_name` AS `callback_method`,
	`action_by` AS `owner_uid`,
	`action_started` AS `created`,
	`action_comment` AS `comment`,
	'bookshop' AS `spacename`
FROM
	`bookshop_documents_history`
WHERE 
	`action_batch_uid` IS NOT NULL;

DROP VIEW IF EXISTS `cadlib_batches`;
CREATE VIEW `cadlib_batches` AS
SELECT 
	`action_batch_uid` AS `uid`,
 	`action_name` AS `callback_method`,
	`action_by` AS `owner_uid`,
	`action_started` AS `created`,
	`action_comment` AS `comment`,
	'cadlib' AS `spacename`
FROM
	`cadlib_documents_history`
WHERE 
	`action_batch_uid` IS NOT NULL;

DROP VIEW IF EXISTS `mockup_batches`;
CREATE VIEW `mockup_batches` AS
SELECT 
	`action_batch_uid` AS `uid`,
 	`action_name` AS `callback_method`,
	`action_by` AS `owner_uid`,
	`action_started` AS `created`,
	`action_comment` AS `comment`,
	'mockup' AS `spacename`
FROM
	`mockup_documents_history`
WHERE 
	`action_batch_uid` IS NOT NULL;

<< */

/**SQL_DROP>>
<< */

/**
 *
 * @author olivier
 */
class BatchDao extends DaoSier
{

	/** @var string */
	public static $table = 'workitem_batches';

	/** @var string */
	public static $vtable = 'workitem_batches';

	/** @var string */
	public static $sequenceName = null;

	/** @var string */
	public static $sequenceKey = null;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'uid' => 'uid',
		'callback_method' => 'callbackMethod',
		'owner_uid' => 'ownerUid',
		'created' => 'created',
		'comment' => 'comment'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created' => 'datetime',
	);
}
