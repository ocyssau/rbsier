<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Document;

use Rbs\Ged\Document\VersionDao as BaseDao;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `workitem_documents` (
 `id` INT(11) NOT NULL DEFAULT '0',
 `uid` VARCHAR(140) NOT NULL UNIQUE,
 `cid` VARCHAR(64) DEFAULT '569e924be82e7',
 `number` VARCHAR(128)  NOT NULL DEFAULT '',
 `name` VARCHAR(255)  DEFAULT NULL,
 `designation` VARCHAR(512)  DEFAULT NULL,
 `label` VARCHAR(255)  DEFAULT NULL,
 `life_stage` VARCHAR(32)  NOT NULL DEFAULT 'init',
 `acode` INT(11) NOT NULL DEFAULT '0',
 `branch_id` INT(1) DEFAULT NULL,
 `iteration` INT(11) NOT NULL DEFAULT '1',
 `version` INT(5) DEFAULT NULL,
 `container_id` INT(11) NOT NULL DEFAULT '0',
 `container_uid` VARCHAR(128) NULL,
 `spacename` VARCHAR(8)  NOT NULL DEFAULT 'workitem',
 `doctype_id` INT(11) NOT NULL DEFAULT '0',
 `default_process_id` INT(11) DEFAULT NULL,
 `category_id` INT(11) DEFAULT NULL,
 `tags` VARCHAR(256) DEFAULT NULL,
 `from_document_id` INT(11) DEFAULT NULL,
 `as_template` BOOLEAN DEFAULT FALSE,
 `lock_by_id` INT(11) DEFAULT NULL,
 `lock_by_uid` VARCHAR(64) DEFAULT NULL,
 `locked` DATETIME DEFAULT NULL,
 `updated` DATETIME DEFAULT NULL,
 `update_by_id` INT(11) DEFAULT NULL,
 `update_by_uid` VARCHAR(64) DEFAULT NULL,
 `created` DATETIME DEFAULT NULL,
 `create_by_id` INT(11) DEFAULT NULL,
 `create_by_uid` VARCHAR(64) DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY (`number`,`version`),
 KEY (`uid`),
 KEY (`number`),
 KEY (`name`),
 KEY (`doctype_id`),
 KEY (`version`),
 KEY (`container_id`),
 KEY (`container_uid`),
 KEY (`category_id`),
 KEY (`create_by_uid`),
 KEY (`lock_by_uid`),
 KEY (`update_by_uid`),
 KEY (`as_template`),
 KEY (`tags`),
 KEY (`branch_id`),
 KEY (`label`),
 KEY (`spacename`),
 FULLTEXT (name, number, designation, label, tags)
 );

CREATE TABLE IF NOT EXISTS `bookshop_documents` LIKE `workitem_documents`;
CREATE TABLE IF NOT EXISTS `cadlib_documents` LIKE `workitem_documents`;
CREATE TABLE IF NOT EXISTS `mockup_documents` LIKE `workitem_documents`;

CREATE TABLE IF NOT EXISTS `workitem_documents_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `workitem_documents_seq` (id) VALUES(500);

 CREATE TABLE IF NOT EXISTS `bookshop_documents_seq`(
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=MyISAM AUTO_INCREMENT=500;
 INSERT INTO `bookshop_documents_seq` (id) VALUES(500);

CREATE TABLE IF NOT EXISTS `cadlib_documents_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `cadlib_documents_seq` (id) VALUES(500);

CREATE TABLE IF NOT EXISTS `mockup_documents_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `mockup_documents_seq` (id) VALUES(500);

<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
ALTER TABLE `workitem_documents`
	 ADD CONSTRAINT `FK_workitem_documents_doctype_id` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`),
	 ADD CONSTRAINT `FK_workitem_documents_version` FOREIGN KEY (`version`) REFERENCES `document_indice` (`indice_id`),
	 ADD CONSTRAINT `FK_workitem_documents_container_id` FOREIGN KEY (`container_id`) REFERENCES `workitems` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_workitem_documents_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
 
 ALTER TABLE `bookshop_documents`
	 CHANGE COLUMN `spacename` `spacename` VARCHAR(8) NOT NULL DEFAULT 'bookshop',
	 ADD CONSTRAINT `FK_bookshop_documents_doctype_id` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_bookshop_documents_version` FOREIGN KEY (`version`) REFERENCES `document_indice` (`indice_id`),
	 ADD CONSTRAINT `FK_bookshop_documents_container_id` FOREIGN KEY (`container_id`) REFERENCES `bookshops` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_bookshop_documents_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

  ALTER TABLE `cadlib_documents`
	 CHANGE COLUMN `spacename` `spacename` VARCHAR(8) NOT NULL DEFAULT 'cadlib',
	 ADD CONSTRAINT `FK_cadlib_documents_doctype_id` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_cadlib_documents_version` FOREIGN KEY (`version`) REFERENCES `document_indice` (`indice_id`),
	 ADD CONSTRAINT `FK_cadlib_documents_container_id` FOREIGN KEY (`container_id`) REFERENCES `cadlibs` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_cadlib_documents_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

 ALTER TABLE `mockup_documents`
	 CHANGE COLUMN `spacename` `spacename` VARCHAR(8) NOT NULL DEFAULT 'mockup',
	 ADD CONSTRAINT `FK_mockup_documents_doctype_id` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_mockup_documents_version` FOREIGN KEY (`version`) REFERENCES `document_indice` (`indice_id`),
	 ADD CONSTRAINT `FK_mockup_documents_container_id` FOREIGN KEY (`container_id`) REFERENCES `mockups` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
	 ADD CONSTRAINT `FK_mockup_documents_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>

-- ON DOCUMENT DELETE --
DROP TRIGGER IF EXISTS `onWIDocumentDelete`;
DELIMITER $$
CREATE TRIGGER `onWIDocumentDelete` AFTER DELETE ON `workitem_documents` FOR EACH ROW
BEGIN
 DELETE FROM `docbasket` WHERE `document_id`=OLD.`id` AND `space_id`=20;
 DELETE FROM `postit` WHERE `parent_uid`=OLD.`uid`;
 DELETE FROM `discussion_comment` WHERE `parent_uid`=OLD.`uid`;
 DELETE FROM `notifications` WHERE `reference_uid`=OLD.`uid`;
 UPDATE `pdm_product_version` SET `document_id`=NULL, `document_uid`=NULL WHERE `document_uid`=OLD.`uid`;
 DELETE FROM `wf_instance` WHERE `id` IN (SELECT `id` FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`);
 DELETE FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`;
 DELETE FROM `acl_resource` WHERE `referToUid`=OLD.`uid`;
END;$$
DELIMITER ;

-- ***************** BOOKSHOP ********************
DROP TRIGGER IF EXISTS `onBookshopDocumentDelete`;
DELIMITER $$
CREATE TRIGGER `onBookshopDocumentDelete` AFTER DELETE ON `bookshop_documents` FOR EACH ROW
 BEGIN
 DELETE FROM `docbasket` WHERE `document_id`=OLD.`id` AND `space_id`=20;
 DELETE FROM `postit` WHERE `parent_uid`=OLD.`uid`;
 DELETE FROM `discussion_comment` WHERE `parent_uid`=OLD.`uid`;
 DELETE FROM `notifications` WHERE `reference_uid`=OLD.`uid`;
 UPDATE `pdm_product_version` SET `document_id`=NULL, `document_uid`=NULL WHERE `document_uid`=OLD.`uid`;
 DELETE FROM `wf_instance` WHERE `id` IN (SELECT `id` FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`);
 DELETE FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`;
 DELETE FROM `acl_resource` WHERE `referToUid`=OLD.`uid`;
END;$$
DELIMITER ;

-- ***************** CADLIB ********************
DROP TRIGGER IF EXISTS onCadlibDocumentDelete;
DELIMITER $$
CREATE TRIGGER onCadlibDocumentDelete AFTER DELETE ON cadlib_documents FOR EACH ROW
BEGIN
 DELETE FROM `docbasket` WHERE `document_id`=OLD.`id` AND `space_id`=20;
 DELETE FROM `postit` WHERE `parent_uid`=OLD.`uid`;
 DELETE FROM `discussion_comment` WHERE `parent_uid`=OLD.`uid`;
 DELETE FROM `notifications` WHERE `reference_uid`=OLD.`uid`;
 UPDATE `pdm_product_version` SET `document_id`=NULL, `document_uid`=NULL WHERE `document_uid`=OLD.`uid`;
 DELETE FROM `wf_instance` WHERE `id` IN (SELECT `id` FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`);
 DELETE FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`;
 DELETE FROM `acl_resource` WHERE `referToUid`=OLD.`uid`;
END;$$
DELIMITER ;

-- ***************** MOCKUP ********************
DROP TRIGGER IF EXISTS onMockupDocumentDelete;
DELIMITER $$
CREATE TRIGGER onMockupDocumentDelete AFTER DELETE ON mockup_documents FOR EACH ROW
BEGIN
 DELETE FROM `docbasket` WHERE `document_id`=OLD.`id` AND `space_id`=20;
 DELETE FROM `postit` WHERE `parent_uid`=OLD.`uid`;
 DELETE FROM `discussion_comment` WHERE `parent_uid`=OLD.`uid`;
 DELETE FROM `notifications` WHERE `reference_uid`=OLD.`uid`;
 UPDATE `pdm_product_version` SET `document_id`=NULL, `document_uid`=NULL WHERE `document_uid`=OLD.`uid`;
 DELETE FROM `wf_instance` WHERE `id` IN (SELECT `id` FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`);
 DELETE FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`;
 DELETE FROM `acl_resource` WHERE `referToUid`=OLD.`uid`;
END;$$
DELIMITER ;


<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table='workitem_documents';
	protected $_table='workitem_documents';

	/**
	 *
	 * @var string
	 */
	public static $vtable='workitem_documents';
	protected $_vtable='workitem_documents';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'workitem_documents_seq';
}
