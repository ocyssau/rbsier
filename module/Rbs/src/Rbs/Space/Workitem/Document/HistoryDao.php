<?php
// %LICENCE_HEADER%
namespace Rbs\Space\Workitem\Document;

/** SQL_SCRIPT>>

CREATE TABLE IF NOT EXISTS `workitem_documents_history` (
`histo_order` INT(11) NOT NULL,
`action_name` VARCHAR(32) NOT NULL,
`action_by` VARCHAR(64) NOT NULL,
`action_started` DATETIME NOT NULL,
`action_batch_uid` VARCHAR(128),
`action_comment` MEDIUMTEXT,
`document_id` INT(11) DEFAULT NULL,
`number` VARCHAR(128) DEFAULT NULL,
`life_stage` VARCHAR(32) DEFAULT NULL,
`iteration` INT(11) DEFAULT NULL,
`version` INT(11) DEFAULT NULL,
`designation` VARCHAR(128) DEFAULT NULL,
`data` MEDIUMTEXT,
PRIMARY KEY (`histo_order`),
INDEX (`document_id` ASC),
INDEX (`action_batch_uid` ASC)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bookshop_documents_history` LIKE `workitem_documents_history`;
CREATE TABLE IF NOT EXISTS `cadlib_documents_history` LIKE `workitem_documents_history`;
CREATE TABLE IF NOT EXISTS `mockup_documents_history` LIKE `workitem_documents_history`;

CREATE TABLE IF NOT EXISTS `workitem_documents_history_seq` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
INSERT INTO `workitem_documents_history_seq` (`id`) VALUES(100);

CREATE TABLE IF NOT EXISTS `bookshop_documents_history_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
INSERT INTO `bookshop_documents_history_seq` (`id`) VALUES(100);

CREATE TABLE IF NOT EXISTS `cadlib_documents_history_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
INSERT INTO `cadlib_documents_history_seq` (`id`) VALUES(100);

CREATE TABLE IF NOT EXISTS `mockup_documents_history_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB ;
INSERT INTO `mockup_documents_history_seq` (`id`) VALUES (10);

<< */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */

/**
 *
 * @author olivier
 */
class HistoryDao extends \Rbs\History\HistoryDao
{

	/** @var string */
	public static $table = 'workitem_documents_history';

	/** @var string */
	public static $vtable = 'workitem_documents_history';

	/** @var string */
	public static $sequenceName = 'workitem_documents_history_seq';

	/** @var string */
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'histo_order' => 'action_id',
		'action_name' => 'action_name',
		'action_by' => 'action_ownerUid',
		'action_started' => 'action_created',
		'action_comment' => 'action_comment',
		'action_batch_uid' => 'action_batchUid',
		'data' => 'data',
		'document_id' => 'data_id',
		'number' => 'data_number',
		'designation' => 'data_description',
		'life_stage' => 'data_lifeStage',
		'iteration' => 'data_iteration',
		'version' => 'data_version',
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'action_started' => 'datetime',
		'closed' => 'datetime'
	);

	/**
	 * Unset some data properties to dont save in history
	 *
	 * (non-PHPdoc)
	 * 
	 * @see Rbs\History.HistoryDao::_bind()
	 */
	protected function _bind($mapped)
	{
		/* Unset some properties to dont save in history */
		$data = $mapped->getData();
		unset($data['dao'], $data['docfiles'], $data['factory']);
		$mapped->setData($data);
		
		return parent::_bind($mapped);
	}
}
