<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem;

use Rbs\History\HistoryDao as BaseDao;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `workitem_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` VARCHAR(32)  DEFAULT NULL,
  `action_by` VARCHAR(32)  DEFAULT NULL,
  `action_started` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL DEFAULT '0',
  `number` VARCHAR(128)  DEFAULT NULL,
  `life_stage` VARCHAR(16)  DEFAULT NULL,
  `description` VARCHAR(256)  DEFAULT NULL,
  `indice_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `default_file_path` text,
  `created` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `planned_closure` int(11) DEFAULT NULL,
  `closed` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
);

CREATE TABLE IF NOT EXISTS `workitem_history_seq` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
INSERT INTO `workitem_history_seq` (`id`) VALUES(100);
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 */
class HistoryDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table='workitem_history';
	public static $vtable='workitem_history';

	public static $sequenceName = 'workitem_history_seq';
	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'histo_order'=>'action_id',
		'action_name'=>'action_name',
		'action_by'=>'action_ownerId',
		'action_started'=>'action_created',
		'container_id'=>'data_id',
		'number'=>'data_name',
		'description'=>'data_description',
		'life_stage'=>'data_status',
		'indice_id'=>'data_versionId',
		'project_id'=>'data_projectId',
		'open_by'=>'data_createBy',
		'created'=>'data_created',
		'planned_closure'=>'data_forseenCloseDate',
		'close_by'=>'data_closeBy',
		'closed'=>'data_closed',
		'default_file_path'=>'data_defaultFilePath',
	);

	public static $sysToAppFilter = array(
		'action_started'=>'datetime',
		'created'=>'datetime',
		'closed'=>'datetime',
		'planned_closure'=>'datetime',
	);
}
