<?php
//%LICENCE_HEADER%
namespace Rbs\Space\Workitem;

/** SQL_SCRIPT>>


 CREATE TABLE `workitem_metadata` (
 `id` int(11) NOT NULL,
 `uid` varchar(64) DEFAULT NULL,
 `field_name` varchar(32) NOT NULL DEFAULT '',
 `extendedCid` varchar(32) DEFAULT NULL,
 `field_description` varchar(128) NOT NULL DEFAULT '',
 `field_type` varchar(32) NOT NULL DEFAULT '',
 `field_regex` varchar(255) DEFAULT NULL,
 `field_required` int(1) NOT NULL DEFAULT '0',
 `field_multiple` int(1) NOT NULL DEFAULT '0',
 `maybenull` int(1) NOT NULL DEFAULT '0',
 `field_size` int(11) DEFAULT NULL,
 `return_name` int(1) NOT NULL DEFAULT '0',
 `field_list` varchar(255) DEFAULT NULL,
 `field_where` varchar(255) DEFAULT NULL,
 `is_hide` int(1) NOT NULL DEFAULT '0',
 `table_name` varchar(32) DEFAULT NULL,
 `field_for_value` varchar(32) DEFAULT NULL,
 `field_for_display` varchar(32) DEFAULT NULL,
 `date_format` varchar(32) DEFAULT NULL,
 `adv_select` tinyint(1) NOT NULL DEFAULT '0',
 `appname` varchar(128) DEFAULT NULL,
 `getter` varchar(128) DEFAULT NULL,
 `setter` varchar(128) DEFAULT NULL,
 `label` varchar(255) DEFAULT NULL,
 `min` decimal(10,0) DEFAULT NULL,
 `max` decimal(10,0) DEFAULT NULL,
 `step` decimal(10,0) DEFAULT NULL,
 `dbfilter` mediumtext,
 `dbquery` mediumtext,
 `attributes` mediumtext,
 PRIMARY KEY (`id`),
 UNIQUE KEY (`field_name`,`extendedCid`),
 KEY  (`extendedCid`),
 KEY  (`field_name`),
 KEY  (`appname`),
 KEY  (`label`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


 CREATE TABLE IF NOT EXISTS `bookshop_metadata` LIKE `workitem_metadata`;
 CREATE TABLE IF NOT EXISTS `cadlib_metadata` LIKE `workitem_metadata`;
 CREATE TABLE IF NOT EXISTS `mockup_metadata` LIKE `workitem_metadata`;

 CREATE TABLE IF NOT EXISTS `workitem_metadata_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=MyISAM AUTO_INCREMENT=10;
 INSERT INTO `workitem_metadata_seq` (`id`) VALUES (10);

 CREATE TABLE IF NOT EXISTS `bookshop_metadata_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=MyISAM AUTO_INCREMENT=10;
 INSERT INTO `bookshop_metadata_seq` (`id`) VALUES (10);

 CREATE TABLE IF NOT EXISTS `cadlib_metadata_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=MyISAM AUTO_INCREMENT=10;
 INSERT INTO `cadlib_metadata_seq` (`id`) VALUES (10);

 CREATE TABLE IF NOT EXISTS `mockup_metadata_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=MyISAM AUTO_INCREMENT=10;
 INSERT INTO `mockup_metadata_seq` (`id`) VALUES (10);

 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Entity of a property definition.
 */
class PropertyDao extends \Rbs\Extended\PropertyDao
{

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'workitem_metadata';

	public static $sequenceName = 'workitem_metadata_seq';

	public static $sequenceKey = 'id';
}
