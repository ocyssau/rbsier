<?php

namespace Rbs\Viewer\Driver;

/**
 *
*/
class Cortonavrmlclient extends Raw
{
	/**
	 *
	 * @param string $file
	 * @return string
	 */
	public static function embededViewer($file){
		$url = './getFile.php?file='.$file;
		return ('<OBJECT DATA="'.$url.'"
			TYPE="x-world/x-vrml"
			TITLE="'.basename($file).'"
			WIDTH=300
			HEIGHT=300></OBJECT>');
	}
}

