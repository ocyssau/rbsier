<?php

namespace Rbs\Viewer\Driver;

/**
 *
 */
class Gdtxt extends Raw
{

	/**
	 * @param string $file
	 * @return string
	 */
	public static function embededViewer($file)
	{
		$html  = '<i>'.tra('File content:').'</i>';
		$html .= '<pre>';
		$html .= file_get_contents($file,0,null,-1,300);
		$html .= '</pre>';
		return $html;
	}//End of method

} //End of class
