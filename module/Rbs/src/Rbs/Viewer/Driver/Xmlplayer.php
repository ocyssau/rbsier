<?php

namespace Rbs\Viewer\Driver;

/**
 *
 */
class RXmlplayer extends Raw
{

	/**
	 *
	 * @param string $file
	 * @return string
	 */
	public static function embededViewer($file)
	{
		return ('
			<object type="application/x-3dxmlplugin" id="Xmlplayer" width="500" height="300" style="MARGIN: 2px" border="0">
			<param name="DocumentFile" value="'.$file.'"">
			</object>
			');
	}
}
