<?php

namespace Rbs\Viewer\Driver;

/**
 *
 */
class Raw
{
	/**
	 *
	 * @var \Rbs\Viewer\Viewer
	 */
	protected $_viewer;

	/**
	 * @param \Rbs\Viewer\Viewer $viewer
	 */
	public function __construct()
	{
	}

	/**
	 *
	 * @param string $file
	 */
	public static function embededViewer($file)
	{
	}

	/**
	 *
	 * @param string $file
	 */
	public function viewFile($file)
	{
		return $this->viewRawFile($file);
	}

	/**
	 * View the document file.
	 *
	 * This method send the content of the $file to the client browser. It send too the mimeType to help the browser to choose the right application to use for open the file.
	 * If the mimetype = no_read, display a error.
	 *
	 * @param string $file path of the file to display on the client computer
	 */
	public function viewRawFile($file)
	{
		$fileName = basename($file);

		header("Content-disposition: attachment; filename=$fileName");
		header("Content-Type: " . $this->_viewer->mimetype);
		header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
		header("Content-Length: ".filesize($file));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		readfile($file);
		die;
	}

}
