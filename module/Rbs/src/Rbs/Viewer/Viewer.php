<?php
namespace Rbs\Viewer;

use Rbplm\Sys\Fsdata;

/**
 * Class to view file or attachment file
 */
class Viewer
{

	protected $fsdata;

	// fsdata Object
	protected $filePath;

	// Path to file
	protected $extension;

	protected $mimetype;

	protected $driverName;

	// The name of the driver to use
	protected $driver;

	// The driver to use
	protected $noread;

	/**
	 *
	 * @param string $name
	 */
	public function __get($name)
	{
		return $this->$name;
	}

	/**
	 * Load the code for a specific Viewer driver.
	 * protected function.
	 */
	protected function _initDriver()
	{
		if ( empty($this->driverName) ) {
			$this->driverName = 'Raw';
		}
		$driverClass = __NAMESPACE__ . '\\Driver\\' . $this->driverName;
		$this->driver = new $driverClass($this);
		return true;
	}

	/**
	 * Get the properties from fsdata object
	 */
	public function initFromFsdata($fsdata)
	{
		$this->filePath = $fsdata->getFullpath();
		$this->extension = $fsdata->getExtension();
		$this->mimetype = $fsdata->getMimetype();
		$this->noread = false;
		$this->driverName = null;
		$this->fsdata = $fsdata;
		return $this;
	}

	/**
	 * Get the properties from docfile object
	 *
	 * @param
	 *        	DocfileVersion|DocfileIteration
	 */
	public function initFromDocfile($docfile)
	{
		$fsdata = $docfile->getdata()->getFsdata();
		$this->initFromFsdata($fsdata);
		return $this;
	}

	/**
	 * Init the properties of the current object from the file
	 */
	public function initFromFile($file)
	{
		if ( is_file($file) || is_dir($file) ) {
			$fsdata = new Fsdata($file);
		}
		else {
			return false;
		}
		$this->initFromFsdata($fsdata);
		return $this;
	}

	/**
	 * Generate a embeded Viewer for the file
	 * Return html code of the embeded Viewer
	 *
	 * @param
	 *
	 */
	protected function _embeded()
	{
		if ( isset($this->driver) && isset($this->filePath) ) return $this->driver->embededViewer($this->filePath);
		else {
			return false;
		}
	}

	/**
	 * Push the file to the client
	 *
	 * This method send the content of the $file to the client browser. It send too the mimeType to help the browser to choose the right application to use for open the file.
	 * If the mimetype = no_read, display a error.
	 * Return true or false.
	 *
	 * @param string $file path of the file to display on the client computer
	 */
	protected function _pushfile()
	{
		if ( $this->noread == 'no_read' ) {
			throw new \Exception("you cant view this file by this way");
		}
		$this->fsdata->download();
	}

	/**
	 * Push main file to download it
	 */
	public function push()
	{
		return $this->_pushfile();
	}

	/**
	 * Show the visualisation file of the current document in a html page (embeded)
	 *
	 * @param
	 *
	 */
	public function display()
	{
		$this->_initDriver(); // init the driver for the file
		return $this->_embeded(); // Return the embeded Viewer
	}
} /* End of class */
