<?php
namespace Rbs\Converter;

use Rbs\Converter\AbstractConverter as AbstractConverter;

/**
 * Require poppler-utils
 * sudo apt install poppler-utils
 */
class PdfToTxt extends AbstractConverter
{

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Rbs\Converter\AbstractConverter::convert()
	 */
	public function convert($file, $toFile)
	{
		if ( !is_file($file) ) {
			throw new \Exception("file $file is not existing");
		}
		
		$return = null;
		$outputs = null;
		
		$execcmd = sprintf("pdftotext '%s' '%s'", $file, $toFile);
		exec($execcmd, $outputs, $return);
		if ( $return != 0 ) {
			$this->getResult()->error($outputs);
			throw new \RuntimeException("Error during run $execcmd - may be you need to install poppler-utils");
		}
		
		return $this;
	}

} /* End of class */
