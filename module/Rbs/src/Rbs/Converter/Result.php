<?php
namespace Rbs\Converter;

/**
 */
class Result
{

	/** array */
	protected $datas;

	/** array */
	protected $feedbacks;

	/** array */
	protected $errors;

	/**
	 *
	 * @param string $name
	 * @param mixed $datas
	 */
	public function setData($name, $datas)
	{
		$this->datas[$name] = $datas;
	}

	/**
	 *
	 * @param string $msg
	 */
	public function error($msg)
	{
		if(is_array($msg)){
			foreach($msg as $m){
				$this->errors[] = (string)$m;
			}
		}
		else{
			$this->errors[] = (string)$msg;
		}
	}

	/**
	 *
	 * @param string $msg
	 */
	public function feedback($msg)
	{
		if(is_array($msg)){
			foreach($msg as $m){
				$this->feedbacks[] = (string)$m;
			}
		}
		$this->feedbacks[] = (string)$msg;
	}

	/**
	 *
	 * @return array
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 *
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 *
	 * @return array
	 */
	public function getFeedback()
	{
		return $this->feedbacks;
	}
}
