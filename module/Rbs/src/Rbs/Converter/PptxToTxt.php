<?php
namespace Rbs\Converter;

use Rbs\Converter\AbstractConverter as AbstractConverter;

/**
 * abiword is require
 * apt install abiword
 *
 */
class PptxToTxt extends AbstractConverter
{

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Rbs\Converter\AbstractConverter::convert()
	 */
	public function convert($file, $toFile)
	{
		if ( !is_file($file) ) {
			throw new \Exception("file $file is not existing");
		}
		
		$return = null;
		$outputs = null;
		
		$execcmd = sprintf("unzip -qc '%s' ppt/slides/slide*.xml | grep -oP '(?<=\<a:t\>).*?(?=\</a:t\>)' > %s", $file, $toFile);
		exec($execcmd, $outputs, $return);
		if ( $return != 0 ) {
			$this->getResult()->error($outputs);
			throw new \RuntimeException("Error during run $execcmd");
		}
		
		return $this;
	}
} /* End of class */
