<?php
namespace Rbs\Converter;

/**
 *
 *
 */
class Factory
{

	/**
	 *
	 * @var Factory
	 */
	protected static $instance;

	/**
	 *
	 * @var Factory
	 */
	protected static $registry;

	/**
	 *
	 * @var array
	 */
	protected $map;

	/**
	 *
	 * @param array $map
	 */
	public function __construct($map)
	{
		$this->map = $map;
	}

	/**
	 * Singleton
	 *
	 * @return Factory
	 */
	public static function get()
	{
		if ( !self::$instance ) {
			$map = \Ranchbe::get()->getConfig('converter.map');
			self::$instance = new self($map);
		}
		return self::$instance;
	}

	/**
	 * @param string $fromMtype
	 * @param string $toMtype
	 * @return ConverterInterface
	 * @throws NotMappedException \Exception
	 */
	public function getConverter($fromType, $toType)
	{
		$k = trim($fromType, '.') . '-' . trim($toType, '.');
		$k = strtolower($k);
		$map = $this->map;
		
		if ( isset($map[$k]) ) {
			$callback = $map[$k];
			if ( is_callable($callback) ) {
				$class = $callback[0];
				$converter = new $class();
				return $converter;
			}
			else{
				throw new \Exception("Converter $k is not existing");
			}
		}
		else{
			throw new NotMappedException("Converter $k is not mapped");
		}
	}

	/**
	 * @param string $name
	 * @return ConverterInterface
	 */
	public function getConverterFromName($name)
	{
		$map = $this->map;
		if ( isset($map[$name]) ) {
			$callback = $map[$name];
			if ( is_callable($callback) ) {
				$class = $callback[0];
				$converter = new $class();
				return $converter;
			}
		}
		throw new \Exception('Converter is not existing');
	}
}
