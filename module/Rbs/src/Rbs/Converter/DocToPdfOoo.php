<?php
namespace Rbs\Converter;

use Rbs\Converter\AbstractConverter as AbstractConverter;
use \Rbplm\Sys\Datatype\File as FileTool;

/**
 * Libreoffice is require
 * >apt install libreoffice
 * 
 * And you must create home for apache2 run user
 * >mkhomedir_helper www-data
 *
 */
class DocToPdfOoo extends AbstractConverter
{

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Rbs\Converter\AbstractConverter::convert()
	 */
	public function convert($file, $toFile)
	{
		if ( !is_file($file) ) {
			throw new \Exception("file $file is not existing");
		}
		
		$return = null;
		$outputs = null;
		
		/* Capture pdf */
		$execcmd = sprintf('libreoffice --headless --convert-to pdf --outdir /tmp %s', $file);
		exec($execcmd, $outputs, $return);
		if ( $return != 0 ) {
			$this->getResult()->error($outputs);
			throw new \RuntimeException("Error during run $execcmd");
		}
		else {
			$tf = '/tmp/' . basename($file);
			$tf = FileTool::sGetRoot($tf).'.pdf';
			if ( is_file($tf) ) {
				rename($tf, $toFile);
			}
			else{
				throw new \RuntimeException("Output file $tf is not founded");
			}
		}
		return $this;
	}
} /* End of class */
