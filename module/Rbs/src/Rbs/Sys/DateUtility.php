<?php
namespace Rbs\Sys;

class DateUtility
{
	
	static $longDateFormat = '';

	/**
	 *
	 */
	public static function getNowdate()
	{
		//Take current date en return the date in lisible format
		$now['TimeStamp'] = time();
		$format = self::$longDateFormat;
		$now['formated'] = strftime($format, $now['TimeStamp']);
		return $now;
	}
	
	/**
	 *
	 * @param \DateTime $timestamp
	 * @return boolean
	 */
	public static function formatDate($timestamp)
	{
		//Take current date en return the date in lisible format
		if ( !is_null($timestamp) ) {
			$format = self::$longDateFormat;
			return strftime($format, $timestamp);
		}
		else
			return false;
	}
	
	/**
	 *
	 */
	public static function getDateFromTS($timeStamp)
	{
		$date['d'] = date(d, $timeStamp);
		$date['M'] = date(M, $timeStamp);
		$date['Y'] = date(Y, $timeStamp);
		return $date;
	}
	
	/**
	 *
	 * @param string $timeStamp
	 */
	public static function getDateFromMysql($timeStamp)
	{
		//mktime ( [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]] )
		mktime($timeStamp);
		$date['d'] = date(d, $timeStamp);
		$date['M'] = date(M, $timeStamp);
		$date['Y'] = date(Y, $timeStamp);
		return $date;
	}
	
	/**
	 *
	 * @param \DateTime $date
	 */
	public static function getTSFromDate($date)
	{
		//  [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]]
		$timeStamp = mktime(0, 0, 0, $date['M'], $date['d'], $date['Y']);
		return $timeStamp;
	}
	
	/**
	 *
	 * @param string $string
	 * @return number
	 */
	public static function makeTimestamp($string)
	{
		if ( empty($string) ) {
			$time = time(); // Use now
		}
		elseif ( preg_match('/^\d{14}$/', $string) ) {
			// it is mysql timestamp format of YYYYMMDDHHMMSS?
			$time = mktime(substr($string, 8, 2), substr($string, 10, 2), substr($string, 12, 2), substr($string, 4, 2), substr($string, 6, 2), substr($string, 0, 4));
		}
		elseif ( is_numeric($string) ) {
			$time = (int)$string; // it is a numeric string, we handle it as timestamp
		}
		else {
			// strtotime should handle it
			$time = strtotime($string);
			if ( $time == -1 || $time === false ) {
				// strtotime() was not able to parse $string, use "now":
				$time = time();
			}
		}
		return $time;
	}
}
