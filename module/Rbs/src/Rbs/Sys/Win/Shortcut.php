<?php
namespace Rbs\Sys\Win;

use Rbs\Sys\Win\ShortcutException as Exception;

/**
 * 
 * @author ocyssau
 *
 */
class Shortcut
{

	/**
	 * 
	 * @var string
	 */
	protected $path;

	/**
	 * 
	 * @var string
	 */
	protected $target;

	/**
	 *
	 * @var boolean
	 */
	protected $isWrite = false;

	/**
	 * 
	 * @var array
	 */
	public $outputs = [];

	/**
	 * 
	 * @var boolean
	 */
	public $return;

	/**
	 * @param string $link
	 * @param string $toPath
	 */
	public static function new($path, $target)
	{
		$link = new Shortcut($path, $target);
		$link->write();
		return $link;
	}

	/**
	 * @param string $link
	 * @param string $toPath
	 */
	public function __construct($path, $target)
	{
		$this->path = $path;
		$this->target = $target;
		$this->isWrite = false;
	}

	/**
	 * 
	 * @throws Exception
	 * @return \Rbs\Sys\Win\Shortcut
	 */
	public function write()
	{
		if ( $this->isWrite == false ) {
			/* path is not existing */
			if ( is_file($this->path) ) {
				throw new Exception(sprintf('The link file %s is existing', $this->path));
			}

			$return = null;
			$outputs = [];
			$shellScript = getcwd() . '/module/Rbs/src/Rbs/Sys/Win/shortcut.sh';

			if ( !is_executable($shellScript) ) {
				throw new Exception(sprintf('The shell scripts %s is not excecutable. You must set permission with : chmod 555 %s', $shellScript, $shellScript));
			}

			$cmd = sprintf('%s -l "%s" -o "%s"', $shellScript, $this->target, $this->path);
			exec($cmd, $outputs, $return);

			$this->outputs = $outputs;
			$this->return = $return;

			if ( $return != 0 ) {
				$logger = \Ranchbe::get()->getLogger();
				foreach( $outputs as $msg ) {
					$logger->log($msg);
				}
				throw new Exception(sprintf('unable to create shortcut %s for path %s. Consult the log to know more.', $this->path, $this->target), 10040);
			}
			$this->isWrite = true;
		}
		return $this;
	}

	/**
	 * 
	 * @return boolean
	 */
	public function isWrite()
	{
		return $this->isWrite;
	}

	/**
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 *
	 * @return string
	 */
	public function getTarget()
	{
		return $this->target;
	}
}
