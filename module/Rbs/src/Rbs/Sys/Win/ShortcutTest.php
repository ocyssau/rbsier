<?php
namespace Rbs\Sys\Win;

use Application\Console\Output as Cli;
//use Application\Model\DataTestFactory as DataFactory;

/**
 * 
 * @author ocyssau
 *
 */
class ShortcutTest extends \Application\Model\AbstractTest
 {

	/**
	 * @return Shortcut
	 */
	public function runTest()
	{
		try{
			$tmpname = tempnam('/tmp/rbtest', 'LinkTest_');
			$path = $tmpname.'.lnk';
			$target = '/nothing';
			$shortcut = Shortcut::new($path, $target);
			$shortcut->write();
			assert($shortcut->isWrite() == true);
		}
		catch(\Throwable $e){
			Cli::blue($e->getMessage());
		}
		
		Cli::blue('Outputs log :');
		Cli::blue(var_export($shortcut->outputs, true));
		Cli::red('return code '.$shortcut->return);
		Cli::green('See result file in '.$path);
	}
	
	/**
	 * @return Shortcut
	 */
	public function fakeTest()
	{
		try{
			$path = '/tmp/fake/link.lnk';
			$target = '/nothing';
			$shortcut = Shortcut::new($path, $target);
			$shortcut->write();
			assert($shortcut->isWrite() == true);
		}
		catch(\Throwable $e){
			Cli::blue($e->getMessage());
		}
	}
}
