<?php
// %LICENCE_HEADER%
namespace Rbs\Sys;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\Exception;
use Rbplm\Dao\NotExistingException;

/**
 * SQL_SCRIPT>>
 * <<
 */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */

/**
 * @brief Dao for message.
 * This dao may be use as replacement of \Zend_Mail_Transport object to use a postgresql db
 * as mail transfert system.
 *
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 * Example and tests: Rbplm/Sys/MessageTest.php
 */
abstract class MessageDao extends DaoSier
{

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'hash' => 'uid',
		'ownerUid' => 'ownerUid',
		'created' => 'created',
		'from' => 'from',
		'to' => 'to',
		'bcc' => 'bcc',
		'cc' => 'cc',
		'subject' => 'subject',
		'body' => 'body',
		'isRead' => 'isRead',
		'isReplied' => 'isReplied',
		'isFlagged' => 'isFlagged',
		'priority' => 'priority',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'to' => 'json',
		'bcc' => 'json',
		'cc' => 'json',
		'created' => 'datetime',
		'isRead' => 'boolean',
		'isReplied' => 'boolean',
		'isFlagged' => 'boolean',
	);

	/**
	 * Move Messages to archive
	 *
	 * @param string $filter
	 *        	sql filter
	 */
	public function archive($filter, $bind = null)
	{
		$connexion = $this->getConnexion();

		/* @var \PDO $connexion */
		if(!$this->archiveStmt || $this->archiveFilter != $filter){
			$table = $this->_table;
			$archiveTable = static::$archiveTable;

			$sql = "INSERT INTO $archiveTable SELECT * FROM $table WHERE $filter";
			$this->archiveStmt = $connexion->prepare($sql);

			$sql = "DELETE FROM $table WHERE $filter";
			$this->archiveDeleteStmt = $connexion->prepare($sql);

			$this->archiveFilter = $filter;
		}

		try {
			$connexion->beginTransaction();
			$this->archiveStmt->execute($bind);
			$this->archiveDeleteStmt->execute($bind);
			$connexion->commit();
		}
		catch (\Exception $e) {
			$connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 * Move forward to the next Message and get it from the database
	 *
	 * @param \Rbplm\Sys\Message $mapped
	 * @param integer $uid
	 *        	Uid of current position
	 * @param string $filter
	 *        	OPTIONAL Sql filter string
	 *
	 */
	public function loadNextMessage($mapped, $uid, $filter = '')
	{
		if ( !$uid ) {
			throw new Exception( sprintf('BAD_PARAMETER_OR_EMPTY %s', 'uid') );
		}

		$table = $this->_table;

		if ( $filter ) {
			$filter = ' AND ' . $filter;
		}

		$sql = "SELECT * FROM $table WHERE id > (SELECT id FROM $table WHERE uid=:uid LIMIT 1)" . $filter . ' ORDER BY id ASC LIMIT 1';
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute(array(
			':uid' => $uid
		));
		$row = $stmt->fetch();

		if ( $row ) {
			$this->loadFromArray($mapped, $row);
		}
		else {
			new NotExistingException(sprintf('CAN_NOT_BE_LOAD_FROM_%s', $sql));
		}
		return $this;
	}

	/**
	 * Move backward to the next Message and get it from the database
	 *
	 * @param \Rbplm\Sys\Message $mapped
	 * @param integer $uid
	 *        	Uid of current position
	 * @param string $filter
	 *        	OPTIONAL Sql filter string
	 */
	public function loadPrevMessage($mapped, $uid, $filter = '')
	{
		if ( !$uid ) {
			throw new Exception( sprintf('BAD_PARAMETER_OR_EMPTY %s', 'uid') );
		}

		$table = $this->_table;

		if ( $filter ) {
			$filter = ' AND ' . $filter;
		}

		$sql = "SELECT * FROM $table WHERE id < (SELECT id FROM $table WHERE uid=:uid)" . $filter . ' ORDER BY id ASC';
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute(array(
			':uid' => $uid
		));
		$row = $stmt->fetch();

		if ( $row ) {
			$this->loadFromArray($mapped, $row);
		}
		else {
			throw new NotExistingException(sprintf('CAN_NOT_BE_LOAD_FROM_%s', $sql));
		}
		return $this;
	}
} /* End of class */
