<?php
//%LICENCE_HEADER%

namespace Rbs\Sys\Message;

use Rbs\Sys\MessageDao;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `message_mailbox` (
`id` int(14) NOT NULL,
`ownerUid` VARCHAR(128) NOT NULL,
`created` DATETIME DEFAULT NULL,
`from` VARCHAR(200) NOT NULL,
`to` VARCHAR(255),
`cc` VARCHAR(255),
`bcc` VARCHAR(255),
`subject` VARCHAR(255)  DEFAULT NULL,
`body` text,
`hash` VARCHAR(32)  DEFAULT NULL,
`replyto_hash` VARCHAR(32)  DEFAULT NULL,
`isRead` tinyint(1)  DEFAULT 0,
`isReplied` tinyint(1)  DEFAULT 0,
`isFlagged` tinyint(1)  DEFAULT 0,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `ownerUid` ),
INDEX ( `isRead` ),
INDEX ( `isReplied` ),
INDEX ( `isFlagged` ),
INDEX ( `subject` ),
FULLTEXT ( `body`,`subject` )
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `message_seq`(
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO message_seq(id) VALUES(10);
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

class MailboxDao extends MessageDao
{

	/**
	 * @var string
	 */
	public static $table = 'message_mailbox';
	public static $vtable = 'message_mailbox';
	public static $archiveTable = 'message_archive';
	public static $sequenceName = 'message_seq';
	public static $sequenceKey = 'id';
}
