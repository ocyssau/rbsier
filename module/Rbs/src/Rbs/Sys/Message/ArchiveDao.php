<?php
//%LICENCE_HEADER%

namespace Rbs\Sys\Message;

use Rbs\Sys\MessageDao;
use Exception;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `message_archive` (
`id` int(14) NOT NULL,
`ownerUid` VARCHAR(128) NOT NULL,
`created` DATETIME DEFAULT NULL,
`from` VARCHAR(200) NOT NULL,
`to` VARCHAR(255),
`cc` VARCHAR(255),
`bcc` VARCHAR(255),
`subject` VARCHAR(255)  DEFAULT NULL,
`body` text,
`hash` VARCHAR(32)  DEFAULT NULL,
`replyto_hash` VARCHAR(32)  DEFAULT NULL,
`isRead` tinyint(1)  DEFAULT 0,
`isReplied` tinyint(1)  DEFAULT 0,
`isFlagged` tinyint(1)  DEFAULT 0,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `ownerUid` ),
INDEX ( `isRead` ),
INDEX ( `isReplied` ),
INDEX ( `isFlagged` ),
INDEX ( `subject` ),
FULLTEXT ( `body`,`subject` )
) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

class ArchiveDao extends MessageDao
{
	public static $table = 'message_archive';
	public static $vtable = 'message_archive';
	public static $archiveTable = '';
	public static $sequenceName = 'message_seq';
	public static $sequenceKey = 'id';

	/**
	 * (non-PHPdoc)
	 * @see Rbs\Sys.MessageDao::archive()
	 */
	public function archive($filter, $bind = null)
	{
		throw new Exception('Not authorized here');
	}
}
