<?php 
return array(
	'Rbplm\Org\Unit'=>array('connex'=>'', 'class'=>'\Rbs\Org\UnitDao'),
	'Rbplm\Org\Workitem'=>array('connex'=>'', 'class'=>'\Rbs\Org\WorkitemDao'),
    'Rbplm\Org\Project'=>array('connex'=>'', 'class'=>'\Rbs\Org\ProjectDao'),
	'Rbplm\Wf\Process'=>array('connex'=>'', 'class'=>'\Rbs\Wf\ProcessDao'),
	'Rbplm\Ged\Document'=>array('connex'=>'', 'class'=>'\Rbs\Ged\DocumentDao'),
	'Rbplm\Ged\Document\Version'=>array('connex'=>'', 'class'=>'\Rbs\Ged\Document\VersionDao'),
	'Rbplm\Ged\Docfile'=>array('connex'=>'', 'class'=>'\Rbs\Ged\DocfileDao'),
	'Rbplm\Ged\Docfile\Version'=>array('connex'=>'', 'class'=>'\Rbs\Ged\Docfile\VersionDao'),
	'Rbplm\Ged\Doctype'=>array('connex'=>'', 'class'=>'\Rbs\Ged\DoctypeDao'),
    'Rbplm\Ged\Category'=>array('connex'=>'', 'class'=>'\Rbs\Ged\CategoryDao'),
	'Rbplm\People\User'=>array('connex'=>'', 'class'=>'\Rbs\People\UserDao'),
	'Rbplm\People\Group'=>array('connex'=>'', 'class'=>'\Rbs\People\GroupDao'),
	'Rbs\Lu\Area'=>array('connex'=>'', 'class'=>'\Rbs\Lu\AreaDao'),
	'Rbs\Lu\Right'=>array('connex'=>'', 'class'=>'\Rbs\Lu\RightDao'),
	'Rbplm\Ged\Document\Link'=>array('connex'=>'', 'class'=>'\Rbs\Ged\Document\LinkDao'),
	'Rbplm\Vault\Record'=>array('connex'=>'', 'class'=>'\Rbs\Vault\RecordDao'),
);
