<?php
// %LICENCE_HEADER%
namespace Rbs\Dao;

use Rbplm\Dao\Mysql;
use Rbplm\Dao\Connexion;
use Rbplm\Sys\Exception as Exception;
use Rbplm\Dao\NotExistingException;
use Rbs\Dao\Sier\MetamodelTranslator;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 */
class Sier extends Mysql
{

	/**
	 * 
	 * @var array
	 */
	public $lastBind;

	/**
	 * 
	 * @var \PDOStatement
	 */
	public $lastStmt;

	/**
	 *
	 * @var array
	 */
	public $extendedModel = [];

	/**
	 *
	 * @var array
	 */
	public $extended = [];

	/**
	 *
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 *
	 * @var MetamodelTranslator
	 */
	protected $translator;

	/**
	 *
	 * @var string
	 */
	protected $_lvtable;

	/** @var array */
	protected $updateSelect;

	/**
	 * Constructor
	 *
	 * @param \PDO $conn
	 */
	public function __construct($conn = null)
	{
		if ( $conn ) {
			$this->connexion = $conn;
		}
		else {
			$this->connexion = Connexion::get();
		}

		$this->metaModel = static::$sysToApp;
		$this->metaModelFilters = static::$sysToAppFilter;

		$this->_sequenceName = static::$sequenceName;
		$this->_sequenceKey = static::$sequenceKey;

		$this->_table = static::$table;
		$this->_vtable = static::$vtable;
		$this->_ltable = static::$ltable;
		$this->_lvtable = static::$lvtable;
	}

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function setFactory($factory)
	{
		$this->factory = $factory;
		return $this;
	}

	/**
	 *
	 * @param \Rbplm\AnyObject $mapped
	 * @param array $bind
	 *        	Bind definition to add to generic bind construct from $sysToApp
	 * @param array $select
	 *        	Array of system properties define in $sysToApp array than must be save. If empty, save all.
	 * @return void
	 *
	 */
	protected function _update($mapped, $select = null)
	{
		$sysToApp = $this->metaModel;
		$bind = [];
		$table = $this->_table;
		$set = [];

		if ( !$this->updateStmt || $this->updateSelect != $select ) {
			if ( is_array($select) ) {
				$sysToApp = array_intersect($sysToApp, $select);
			}
			foreach( $sysToApp as $asSys => $asApp ) {
				$set[] = '`' . $asSys . '`' . '=:' . $asApp;
			}
			$sql = "UPDATE $table SET " . implode(',', $set) . ' WHERE ' . $this->getTranslator()->toSys('id') . '=:id;';
			$this->updateStmt = $this->connexion->prepare($sql);
			$this->updateSelect = $select;
		}
		$bind = $this->_bind($mapped);
		if ( is_array($select) ) {
			array_walk($select, function (&$val, $key) {
				$val = ':' . $val;
			});
			$bind = array_intersect_key($bind, array_flip($select));
			$bind[':id'] = $mapped->getId();
		}

		$this->updateStmt->execute($bind);
	}

	/**
	 *
	 * @param \Rbplm\AnyObject $mapped
	 * @param array $bind
	 *        	Bind definition to add to generic bind construct from $sysToApp
	 * @param array $select
	 *        	Array of system properties define in $sysToApp array than must be save. If empty, save all.
	 * @return void
	 *
	 */
	protected function _insert($mapped, $select = null)
	{
		$sysToApp = $this->metaModel;
		$table = $this->_table;
		$sysSet = [];
		$appSet = [];

		/* if autoincrement key, not set the pkey */
		if ( !$this->_sequenceName ) {
			$pKey = array_search('id', $sysToApp);
			unset($sysToApp[$pKey]);
		}

		/* Init statement */
		if ( !$this->insertStmt ) {
			foreach( $sysToApp as $asSys => $asApp ) {
				$sysSet[] = '`' . $asSys . '`';
				$appSet[] = ':' . $asApp;
			}

			if ( $this->_sequenceName ) {
				$sql = 'UPDATE ' . $this->_sequenceName . ' SET ' . $this->_sequenceKey . '=LAST_INSERT_ID(' . $this->_sequenceKey . ' + 1) LIMIT 1;';
				$this->seqStmt = $this->connexion->prepare($sql);
			}

			$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
			$this->insertStmt = $this->connexion->prepare($sql);
		}

		$this->lastStmt = $this->insertStmt;
		/* a named ssequence is required */
		if ( $this->_sequenceName ) {
			$this->seqStmt->execute();
			$mapped->setId($this->connexion->lastInsertId($this->_sequenceName));
			$bind = $this->_bind($mapped);
			$this->insertStmt->execute($bind);
		}
		/* none sequence */
		else if ( $this->_sequenceName === false ) {
			$bind = $this->_bind($mapped);
			$this->insertStmt->execute($bind);
			$mapped->setId($this->connexion->lastInsertId($table));
		}
		/* use autonum on primary key */
		else {
			$bind = $this->_bind($mapped);
			unset($bind[':id']);
			$this->insertStmt->execute($bind);
			$mapped->setId($this->connexion->lastInsertId($table));
		}
	}

	/**
	 * @param array $mapped
	 * @return array
	 */
	protected function _bind($mapped)
	{
		$bind = [];
		$properties = $mapped->getArrayCopy();

		$sysToApp = $this->metaModel;
		$sysToAppFilter = $this->metaModelFilters;

		/* @var Rbs\Dao\Sier\MetamodelTranslator */
		$translator = $this->getTranslator();

		/* Transform, convert values */
		foreach( $sysToApp as $sysName => $appName ) {
			if ( isset($sysToAppFilter[$sysName]) && array_key_exists($appName, $properties) ) {
				if ( isset($properties[$appName]) ) {
					$value = $properties[$appName];
					$filterMethod = $sysToAppFilter[$sysName] . 'ToSys';
					$bind[':' . $appName] = $translator->$filterMethod($value);
				}
				else {
					$bind[':' . $appName] = null;
				}
			}
			else {
				if ( isset($properties[$appName]) ) {
					$value = $properties[$appName];
					if ( is_array($value) ) {
						$value = $translator->jsonToSys($value);
					}
					$bind[':' . $appName] = $value;
				}
				else {
					$bind[':' . $appName] = null;
				}
			}
		}
		$this->lastBind = $bind;
		return $bind;
	}

	/**
	 *
	 * @return MetamodelTranslator
	 */
	public function getTranslator()
	{
		if ( !isset($this->translator) ) {
			$this->translator = new MetamodelTranslator($this->metaModel, $this->metaModelFilters);
		}
		return $this->translator;
	}

	/**
	 *
	 * @param MetamodelTranslator $translator
	 * @return Sier
	 */
	public function setTranslator(MetamodelTranslator $translator)
	{
		$this->translator = $translator;
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::hydrate() Hydrator.
	 *      Load the properties in the mapped object.
	 *
	 * @param \Rbplm\AnyObject $mapped
	 * @param array $row
	 *        	\PDO fetch result to load
	 * @param boolean $fromApp
	 *        	If true, assume that keys $row are name of properties as set in model, else are set as in persitence system.
	 * @return \Rbplm\AnyObject
	 */
	public function hydrate($mapped, array $row, $fromApp = false)
	{
		$properties = array();

		if ( $fromApp ) {
			foreach( $this->metaModel as $asSys => $asApp ) {
				$properties[$asApp] = $row[$asApp];
			}
		}
		else {
			/* @var Rbs\Dao\Sier\MetamodelTranslator */
			$translator = $this->getTranslator();
			$sysToAppFilter = $this->metaModelFilters;
			foreach( $this->metaModel as $asSys => $asApp ) {
				if ( isset($sysToAppFilter[$asSys]) ) {
					$value = $row[$asSys];
					$filterMethod = $sysToAppFilter[$asSys] . 'ToApp';
					$properties[$asApp] = $translator->$filterMethod($value);
				}
				else {
					$value = $row[$asSys];
					if ( $value[0] == '[' ) {
						$value = $translator->jsonToApp($value);
					}
					$properties[$asApp] = $value;
				}
			}
		}

		/* Hydrate standard model */
		$mapped->hydrate($properties);

		/* Load the extended model */
		if ( $this->extendedModel ) {
			foreach( $this->extendedModel as $asSys => $asApp ) {
				$mapped->$asApp = $properties[$asApp];
			}
		}

		return $mapped;
	}

	/**
	 *
	 * @param string $filter
	 * @param array $bind
	 * @param boolean $withChildren
	 * @param boolean $withTrans
	 * @throws Exception
	 * @return Sier
	 */
	protected function _deleteFromFilter($filter, $bind, $withChildren = true, $withTrans = null)
	{
		($withTrans === null) ? $withTrans = $this->options['withtrans'] : null;
		($this->connexion->inTransaction() == true) ? $withTrans = false : null;

		if ( $withTrans ) $this->connexion->beginTransaction();

		if ( !$this->deleteStmt || $this->deleteFilter != $filter ) {
			$table = $this->_table;

			$sql = 'DELETE FROM ' . $table . ' WHERE ' . $filter;
			$this->deleteStmt = $this->connexion->prepare($sql);

			$this->deleteFilter == $filter;
		}

		try {
			$this->deleteStmt->execute($bind);

			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 * Alias for hydrate.
	 */
	public function loadFromArray($mapped, array $row, $fromApp = false)
	{
		return $this->hydrate($mapped, $row, $fromApp);
	}

	/**
	 *
	 * @param array $select as Sys
	 * @param string $filter as Sys
	 * @param array $bind
	 * @return \PDOStatement
	 * @throws NotExistingException, \PDOException
	 */
	public function query($select, $filter = null, $bind = null)
	{
		$table = $this->_table;

		$select = implode($select, ',');
		$sql = "SELECT $select FROM $table AS obj";

		if ( is_string($filter) ) {
			$sql .= " WHERE $filter";
		}

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		if ( !$stmt ) {
			throw new NotExistingException(sprintf('CAN NOT BE LOAD FROM %s', $sql));
		}
		return $stmt;
	}

	/**
	 *
	 * @param integer $id
	 * @return string
	 * @throws NotExistingException, \PDOException
	 */
	public function getUidFromId($id)
	{
		$trans = $this->getTranslator();
		$sql = "SELECT " . $trans->toSys('uid') . " FROM " . $this->_table . " WHERE " . $trans->toSys('id') . "=:id";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':id' => $id
		));
		if ( !$stmt ) {
			throw new NotExistingException(sprintf('CAN NOT BE LOAD FROM %s', $sql));
		}
		return $stmt->fetchColumn(0);
	}

	/**
	 *
	 * @param integer $id
	 * @return string
	 * @throws NotExistingException, \PDOException
	 */
	public function getNumberFromId($id)
	{
		$trans = $this->getTranslator();
		$sql = "SELECT " . $trans->toSys('number') . " FROM " . $this->_table . " WHERE " . $trans->toSys('id') . "=:id";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':id' => $id
		));
		if ( !$stmt ) {
			throw new NotExistingException(sprintf('CAN NOT BE LOAD FROM %s', $sql));
		}
		return $stmt->fetchColumn(0);
	}

	/**
	 *
	 * @param string $uid
	 * @return string
	 * @throws NotExistingException, \PDOException
	 */
	public function getIdFromUid($uid)
	{
		$trans = $this->getTranslator();
		$sql = "SELECT " . $trans->toSys('id') . " FROM " . $this->_table . " WHERE " . $trans->toSys('uid') . "=:uid";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':uid' => $uid
		));
		if ( !$stmt ) {
			throw new NotExistingException(sprintf('CAN NOT BE LOAD FROM %s', $sql));
		}

		return $stmt->fetchColumn(0);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::delete() Suppress current record in database
	 *
	 * @param string $uid
	 * @param boolean $withChildren
	 *        	If true, suppress all childs and childs of childs
	 * @param boolean $withTrans
	 *        	If true, open a new transaction
	 * @throws \PDOException
	 * @return Sier
	 */
	public function delete($uid, $withChildren = false, $withTrans = null)
	{
		$trans = $this->getTranslator();
		$toSysId = $trans->toSys('uid');
		$filter = "$toSysId=:uid";
		$bind = array(
			':uid' => $uid
		);
		$this->_deleteFromFilter($filter, $bind, $withChildren, $withTrans);
		return $this;
	}

	/**
	 *
	 * @param integer $id
	 * @param boolean $withChildren
	 *        	If true, suppress all childs and childs of childs
	 * @param boolean $withTrans
	 *        	If true, open a new transaction
	 * @return Sier
	 */
	public function deleteFromId($id, $withChildren = true, $withTrans = null)
	{
		$trans = $this->getTranslator();
		$toSysId = $trans->toSys('id');
		$filter = "$toSysId=:id";
		$bind = array(
			':id' => $id
		);
		$this->_deleteFromFilter($filter, $bind, $withChildren, $withTrans);
		return $this;
	}

	/**
	 *
	 * @param string $in
	 * @return \DateTime
	 */
	public static function dateToApp($timestamp)
	{
		if ( $timestamp == 0 ) {
			return new \Rbplm\Sys\Date();
		}
		else {
			return new \Rbplm\Sys\Date((int)$timestamp);
		}
	}

	/**
	 * @deprecated
	 * Use Translator class
	 * @param \DateTime $in
	 */
	public static function dateToSys($in)
	{
		if ( $in instanceof \DateTime ) {
			return $in->getTimestamp();
		}
		elseif ( $in == 0 ) {
			$date = new \DateTime();
			return $date->getTimestamp();
		}
		elseif ( is_string($in) ) {
			$date = new \DateTime($in);
			return $date->getTimestamp();
		}
	}

	/**
	 * @deprecated
	 * Use Translator class
	 * @param string $in
	 * @return \DateTime
	 */
	public static function datetimeToApp($string)
	{
		if ( $string == '' ) {
			return null;
		}
		else {
			return new \Rbplm\Sys\Date($string);
		}
	}

	/**
	 * @deprecated
	 * Use Translator class
	 * @param \DateTime $in
	 */
	public static function datetimeToSys($in)
	{
		if ( $in instanceof \DateTime ) {
			return $in->format(self::DATE_FORMAT);
		}
		elseif ( $in == 0 ) {
			return null;
		}
		elseif ( is_string($in) ) {
			$date = new \Rbplm\Sys\Date($in);
			return $date->format(self::DATE_FORMAT);
		}
	}

	/**
	 *
	 * @deprecated
	 * Use Translator class
	 * @param string $in
	 * @return array
	 */
	public static function jsonToApp($json)
	{
		if ( $json[0] == '{' || $json[0] == '[' ) {
			return json_decode($json, true);
		}
		else {
			return array(
				$json
			);
		}
	}

	/**
	 *
	 * @deprecated
	 * Use Translator class
	 * @param string $in
	 * @return array
	 */
	public static function jsonToSys($array)
	{
		if ( is_array($array) ) {
			$ret = json_encode($array, true);
		}
		else {
			$ret = $array;
		}
		return $ret;
	}

	/**
	 * @deprecated
	 * Use Translator class
	 *
	 * @param boolean $yesOrNo
	 */
	protected static function yesOrNoToApp($yesOrNo)
	{
		return ($yesOrNo == 'y') ? true : false;
	}

	/**
	 *
	 * @deprecated
	 * Use Translator class
	 * @param boolean $yesOrNo
	 */
	protected static function yesOrNoToSys($yesOrNo)
	{
		return ($yesOrNo == true) ? 'y' : 'n';
	}

	/**
	 * @deprecated
	 * Use Translator class
	 *
	 * @param integer $bool
	 */
	protected static function booleanToApp($bool)
	{
		return (bool)$bool;
	}

	/**
	 * @deprecated
	 * Use Translator class
	 *
	 * @param boolean $bool
	 */
	protected static function booleanToSys($bool)
	{
		return (int)$bool;
	}
} /* End of class */
