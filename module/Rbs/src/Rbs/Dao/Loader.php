<?php

namespace Rbs\Dao;

use Rbplm\Dao\Registry;
use Rbplm\Dao\LoaderInterface;
use Rbs\Space\Factory;

/**
 * 
 *
 */
class Loader implements LoaderInterface
{
	/**
	 * Singleton instance
	 * @var Loader
	 */
	protected static $instance;

	/**
	 * @var array
	 */
	protected $options = [
		'force'=>false,
		'locks'=>false,
		'useregistry'=>true,
		'uidkeyname'=>'uid',
		'idkeyname'=>'id',
		'dao'=>null,
	];

	/**
	 * @var Factory
	 */
	protected $factory;

	/**
	 * @var Registry
	 */
	protected $registry;

	/**
	 *
	 */
	public function __construct(Factory $factory)
	{
		$this->registry = new Registry();
		$this->factory = $factory;
		self::$instance = $this;
	}

	/**
	 * Singleton getter
	 * @return Loader
	 */
	public static function get()
	{
		if(!isset(self::$instance)){
			throw new \Exception('Loader must be construct before use get()');
		}
		return self::$instance;
	}

	/**
	 * @param Factory
	 * @return Loader
	 */
	public function setFactory(Factory $factory)
	{
		$this->factory = $factory;
		return $this;
	}

	/**
	 * @return Factory
	 */
	public function getFactory()
	{
		return $this->factory;
	}

	/**
	 * Set a option
	 * Option key may be :
	 * 		force
	 * 		lock
	 * 		useregistry
	 * 		uidkeyname
	 * 		idkeyname
	 * 		parentidkeyname
	 * 		childidkeyname
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return Loader
	 */
	public function setOptions( $key, $value )
	{
		$key = strtolower($key);
		$this->options[$key] = $value;
		return $this;
	}

	/**
	 * @param string $id	Id of the object to load.
	 * @param string $classId	Id of the object class to load.
	 * @return \Rbplm\Any
	 * @throws \Exception
	 */
	public function loadFromId($id, $classId)
	{
		$useRegistry = $this->options['useregistry'];
		if($useRegistry){
			$object = $this->registry->getFromId($id, $classId);
			if($object){
				return $object;
			}
		}

		$object = $this->factory->getModel($classId);
		$dao = $this->factory->getDao($classId);
		$dao->loadFromId($object, $id);

		if($useRegistry){
			$this->registry->add($object);
		}
		return $object;
	}


	/**
	 * @param string $uid	Uuid of the object to load.
	 * @param string $classId	Id of the object class to load.
	 *
	 * @return \Rbplm\Any
	 * @throws \Exception
	 */
	public function loadFromUid($uid, $classId)
	{
		$useRegistry = $this->options['useregistry'];
		if($useRegistry){
			$object = $this->registry->getFromUid($uid);
			if($object){
				return $object;
			}
		}

		$object = $this->factory->getModel($classId);
		$dao = $this->factory->getDao($classId);
		$dao->loadFromUid($object, $uid);

		if($useRegistry){
			$this->registry->add($object);
		}
		return $object;
	}

	/**
	 * Recursive function
	 * Load from Uid
	 * @param array
	 * @param string
	 */
	public function loadChildrenFromUid(&$treeList, $parentUid)
	{
		$list = $this->factory->getList(Factory::GENERIC);
		$parentKeyName = $this->factory->getDao()->getTranslator()->toSys('parentUid');
		$filter = "$parentKeyName='$parentUid'";
		$list->load($filter);

		foreach($list as $item){
			$treeList[] = $item;
			$uid = $item['uid'];
			self::loadChildrenFromUid($treeList, $uid);
		}

		return $treeList;
	}

	/**
	 * 
	 * Recursive function
	 * Load from Uid
	 * @param parent
	 * @param string
	 */
	public function loadChildren($parent, $filter=null)
	{
		$list = $this->factory->getList($parent->cid);
		$parentUid = $parent->getUid();
		$parentKeyName = $this->factory->getDao($parent->cid)->getTranslator()->toSys('parentUid');

		if($filter){
			$filter = "$filter AND $parentKeyName='$parentUid' ORDER BY `index` ASC";
		}
		else{
			$filter = "$parentKeyName='$parentUid' ORDER BY `index` ASC";
		}

		$list->load($filter);

		foreach($list as $item){
			$uid = $item['uid'];
			$child = $this->factory->getModel($item['cid']);
			$this->factory->getDao($item['cid'])->loadFromUid($child, $uid);
			$parent->addChild($child);
			self::loadChildren($child);
		}

		return $parent;
	}

}
