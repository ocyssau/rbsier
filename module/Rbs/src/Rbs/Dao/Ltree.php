<?php
// %LICENCE_HEADER%
namespace Rbs\Dao;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 */
class Ltree
{

	/**
	 * @var \PDO
	 */
	public $connexion;

	/**
	 * @var \Rbs\Dao\Sier
	 */
	public $dao;

	/**
	 * @var string
	 */
	public $_vtable;

	/**
	 * @var string
	 */
	public $_table;

	/**
	 * @var array
	 */
	public $metaModel;


	/**
	 * @var array
	 */
	public $metaModelFilters;

	/**
	 * @var array
	 */
	static $sysToApp = array(
		'cn'=>'canonicalName',
	);

	/**
	 * @var array
	 */
	static $sysToAppFilter = array();

	/**
	 * Constructor
	 *
	 * @param \Rbs\Dao\Sier $dao
	 */
	public function __construct($dao)
	{
		$this->dao = $dao;
		$this->connexion = $dao->getConnexion();
		$this->_vtable = $dao::$vtable;
		$this->_table = $dao::$table;

		$this->metaModel = static::$sysToApp;
		$this->metaModelFilters = static::$sysToAppFilter;
	}

	/**
	 * Get children from the canonical name $cn.
	 * $dn as 'root/node1/node2...'
	 * Return only the first level of nodes after node2
	 *
	 * @param string $dn
	 * @return \PDOStatement
	 */
	public function getChildren($dn, $filter='1=1')
	{
		$table = $this->_vtable;
		$toSysCn = array_search('canonicalName', $this->metaModel);
		$sql = "SELECT * FROM $table AS node WHERE" .
				" $toSysCn LIKE :dn1 AND $toSysCn NOT LIKE :dn2 AND $filter";

		$bind = array(
			':dn1'=>$dn.'/%',
			':dn2'=>$dn.'/%/%',
		);

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}

	/**
	 * Get children from the canonical name $cn.
	 * $cn as 'root/node1/node2...'
	 * Return only the first level of nodes after node2
	 * $cn node is not included in return
	 *
	 * @param string $cn
	 * @return \PDOStatement
	 */
	public function getParents($dn, $filter='1=1')
	{
		$splitted = explode('/', $dn);
		$dn = implode($splitted, '/');
		return $this->getNodes($dn, $filter);
	}

	/**
	 * Get nodes with canonicalName $dn.
	 * $dn as 'root/node1/node2...'
	 *
	 * @param string $dn
	 * @return \PDOStatement
	 */
	public function getNodes($dn, $filter='1=1')
	{
		$table = $this->_vtable;
		$toSysCn = array_search('canonicalName', $this->metaModel);
		$sql = "SELECT * FROM $table AS node WHERE" .
			   " $toSysCn = :dn1 AND $filter";

		$bind = array(
			':dn1'=>$dn,
		);
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}

	/**
	 * Get node from canonicalName $dn up to last leaf of the branch.
	 * The node $dn is inclued in return
	 *
	 * @param string $dn
	 * @return \PDOStatement
	 */
	public function getDescendants($dn, $filter='1=1')
	{
		$table = $this->_vtable;
		$toSysCn = array_search('canonicalName', $this->metaModel);
		$sql = "SELECT * FROM $table AS node WHERE" .
		" $toSysCn LIKE :dn1 AND $filter";

		$bind = array(
			':dn1'=>$dn.'/%',
		);

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}

	/**
	 * Get nodes from the canonicalName $dn up to root node
	 *
	 * @param string $dn
	 * @return \PDOStatement
	 */
	public function getAncestors($dn, $filter='1=1', $select='*', $bind=array())
	{
		$table = $this->_vtable;
		$toSysCn = array_search('canonicalName', $this->metaModel);
		$dn = $dn.'/%';
		$sql = "SELECT $select FROM $table AS node WHERE" .
				" :dn1 LIKE CONCAT($toSysCn, '%') AND $filter";

		$bind[':dn1'] = $dn;
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}

} /* End of class */
