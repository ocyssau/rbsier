<?php
//%LICENCE_HEADER%

namespace Rbs\Dao;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
class History
{
	
	function __construct($mapped, $dao)
	{
		$factory = $dao->daoFactory;
		$this->table = $factory->getTable($mapped->cid);
	}//End of method
	
	
	public static function factory($dao)
	{
		return new self();
	}
	
	public function onSavePost($mapped)
	{
	}
	
	public function onInsertPost($mapped)
	{
	}
	
	public function onUpdatePost($mapped)
	{
	}
	
	
	public function onLoadPost($mapped)
	{
	}
	
	public function onDeletePost($mapped)
	{
	}
	
	
	
	
}
