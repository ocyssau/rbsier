<?php
//%LICENCE_HEADER%
namespace Rbs\Dao\Sier;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Filter\Op;
use Application\Console\Output as Cli;

//use Application\Model\DataTestFactory as DataFactory;

/**
 * @brief Test class for \Rbplm\Dao\Pg\Filter
 *
 */
class FilterTest extends \Application\Model\AbstractTest
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	public function setUp()
	{
		$connexion = DaoFactory::get()->getConnexion();
		if ( is_null($connexion) ) {
			\Ranchbe::get()->getServiceManager()->getDaoService();
		}
	}

	/**
	 *
	 */
	function runTest()
	{
		$factory = DaoFactory::get();
		
		/* @var \Rbs\Dao\Sier\Filter $filter */
		$filter = $factory->getFilter(\Rbplm\Ged\Document\Version::$classId);

		/* use values directly :*/
		$filter->andfind('utilisation', 'designation', Op::CONTAINS);
		Cli::yellow($filter);
		
		/* USE BIND PARAMETERS, passthroughs characters %% must be set in search string */
		$filter = $factory->getFilter(\Rbplm\Ged\Document\Version::$classId);
		$filter->andfind(':what', 'designation', Op::LIKE);
		$filter->orFind(':what', 'indexer.content', Op::MATCH);
		$filter->orFind('directwhat', 'other.content', Op::MATCH);
		$bind = [':what', '%utilisation%'];
		
		Cli::blue($filter);
		Cli::blue(var_export($bind));

		/* with sub filter */
		$subFilter = $factory->getFilter(\Rbplm\Org\Unit::$classId);
		$subFilter->andfind('qqueschose', 'name', Op::CONTAINS);
		$subFilter->orfind('autrechose', 'name', Op::CONTAINS);
		$subFilter->orfind('encoreautrechose', 'name', Op::CONTAINS);
		$filter->subor($subFilter);
		Cli::yellow($filter);
	}
}
