<?php
//%LICENCE_HEADER%

namespace Rbs\Dao\Sier;

use \Rbplm\Sys\Exception;
use \Rbplm\Sys\Error;


/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS index (
	uid uuid NOT NULL,
	dao varchar(256) DEFAULT NULL,
	connexion varchar(256) DEFAULT NULL
);
ALTER TABLE index ADD PRIMARY KEY (uid);
<<*/


/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_anyobject_index AS
    SELECT
	    anyobject.id,
	    anyobject.uid,
	    anyobject.name,
	    anyobject.cid,
	    anyobject.label,
	    anyobject.parent,
	    anyobject.path,
	    classes.name AS class_name,
		index.dao,
		index.connexion
    FROM
    	anyobject
    JOIN classes ON (anyobject.cid = classes.id)
    LEFT OUTER JOIN index ON (anyobject.uid = index.uid);

CREATE OR REPLACE VIEW view_anyobject_class AS
    SELECT
	    anyobject.id,
	    anyobject.uid,
	    anyobject.name,
	    anyobject.label,
	    anyobject.parent,
	    anyobject.path,
	    classes.id AS cid,
	    classes.name AS class_name
    FROM
    	anyobject
    JOIN classes ON (anyobject.cid = classes.id);

<<*/



/**
 * @brief Instanciate a anyobject and load properties in from the given Uuid.
 *
 * It is a builder class to instanciate the Rbplm objects from a Uuid. instanciate the corresponding class
 * and load properties from the pg database.
 *
 * @see \Rbplm\Dao\LoaderInterface
 *
 * @example
 * @code
 * $myanyobject = Loader::load($uid);
 * @endcode
 *
 *
 *
 */
class Loader implements \Rbplm\Dao\LoaderInterface
{

	/**
	 * The table to query for get the class name from uid.
	 * @var string
	 */
	static $_master_index_table = 'view_anyobject_index';

	/**
	 * Connexion to pgsql server where is the master index table
	 * @var \PDO
	 */
	private static $_connexion = null;

    /**
     * Set the connexion to Postgresql db where are store the index.
     *
     * @param \PDO
     * @return void
     */
    public static function setConnexion( \PDO $connexion )
    {
    	self::$_connexion = $connexion;
    }

	/**
	 * @see \Rbplm\Dao\LoaderInterface::load()
	 *
	 * @param string				$uid	Uuid of the object to load.
	 * @param array		$options = array(
	 * 						boolean	'force'	[OPTIONAL] If true, force to query db to reload links.
	 * 						boolean	'locks'  [OPTIONAL] If true, lock the db records in accordance to the use db system.
	 * 						boolean	'useRegistry' [OPTIONAL] If true, get object from registry, default is true
	 * 						string	'classname'	  [OPTIONAL]	Name of class of object to load
	 * 						\Rbplm\Dao\Pg	'dao' [OPTIONAL]	The dao used to load
	 * )
	 * @return \Rbplm\AnyObject
	 *
	 * @throws \Rbplm\Sys\Exception
	 */
	public static function load( $uid, $options=null )
	{
		if(!$uid){
			throw new Exception( sprintf('BAD_PARAMETER_OR_EMPTY %s', 'uid'), Error::WARNING );
		}

		if($uid == \Rbplm\Org\Root::UUID){
			return \Rbplm\Org\Root::singleton();
		}

		$className = $options['classname'];
		$dao = $options['dao'];

		if(isset($options['force'])) $force = $options['force'];
		else $force = false;

		if(isset($options['locks'])) $locks = $options['locks'];
		else $locks = false;

		if(isset($options['useRegistry'])) $useRegistry = $options['useRegistry'];
		else $useRegistry = true;

		if($useRegistry){
			$Object = \Rbplm\Dao\Registry::singleton()->get($uid);
			if($Object){
				return $Object;
			}
		}

		if( $className == null && self::$_connexion){
			//Get class from master index
			$sql = "SELECT class_name FROM " . self::$_master_index_table . ' WHERE uid=:uid';
			$stmt = self::$_connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$stmt->execute( array(':uid'=>$uid) );
			$row = $stmt->fetch();
			//$className = $row['class_name'];
			$className = str_replace('_', '\\', $row['class_name']);
			//$daoClassname = $row['dao'];
			//$connexionName = $row['connexion'];
		}

		if( !$className ){
			throw new Exception( sprintf('UNABLE_TO_FIND_OBJECT_UID_%s_IN_%s', $uid, self::$_master_index_table), Error::WARNING);
		}

		//use the factory for get instance of dao from registry:
		$Object = new $className();

		if( !is_a($dao, '\Rbplm\Dao\Pg') ){
			$dao = \Rbplm\Dao\Factory::get()->getDao( $className );
		}

		$options = array('locks'=>$locks, 'force'=>$force);
		$dao->loadFromUid( $Object, $uid, $options );

		if($useRegistry){
			\Rbplm\Dao\Registry::singleton()->add($Object);
		}

        return $Object;
	}


	/**
	 *
	 * @param \Rbplm\AnyObject	$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParent( \Rbplm\AnyObject $mapped, $expectedProperties = array(), $force = false, $locks = true )
	{
		$mapped->setParent( self::load( $mapped->parentId, $expectedProperties, $force, $locks ) );
	}


	/**
	 *
	 * @param \Rbplm\AnyObject	$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParentRecursively( \Rbplm\AnyObject $mapped, $force = false, $locks = true )
	{
		if($mapped->parentId){
			$parent = Loader::load( $mapped->parentId, array(), $force, $locks );
			if($parent){
				self::loadParentRecursively( $parent, $force, $locks );
				$mapped->setParent( $parent );
			}
		}
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\LoaderInterface::loadFromUid()
	 */
	public function loadFromUid($uid, $classId)
    {}

    /**
     * 
     * {@inheritDoc}
     * @see \Rbplm\Dao\LoaderInterface::loadFromId()
     */
	public function loadFromId($id, $classId)
    {}
	
    /**
     * 
     * {@inheritDoc}
     * @see \Rbplm\Dao\LoaderInterface::setOptions()
     */
    public function setOptions($key, $value)
    {}
    
} /* End of class */
