<?php
//%LICENCE_HEADER%

namespace Rbs\Dao\Sier;

use Rbplm\Dao\DaoListAbstract;

/** SQL_VIEW>>
 <<*/

/**
 * A list of records in db.
 * This class implements a iterator.
 *
 */
class DaoList extends DaoListAbstract
{
	
	/**
	 *
	 * @var \Rbplm\Dao\DaoInterface
	 */
	public $dao;
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::getTranslator()
	 * @return \Rbs\Dao\Sier\MetamodelTranslator
	 */
	public function getTranslator()
	{
		if ( !isset($this->translator) ) {
			$this->translator = new MetamodelTranslator($this->metaModel, $this->metaModelFilters);
		}
		return $this->translator;
	}
} /* End of class */
