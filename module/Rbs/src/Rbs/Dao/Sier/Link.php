<?php
// %LICENCE_HEADER%
namespace Rbs\Dao\Sier;

use Rbplm\Dao\Connexion;
use Rbplm\Sys\Exception as Exception;
use Rbplm\Sys\Error as Error;

/**
 */
class Link
{

	/**
	 *
	 * @var \PDO
	 */
	protected $connexion;

	/**
	 *
	 * @var string
	 */
	public $_table;

	/**
	 *
	 * @var string
	 */
	public $_vtable;

	/**
	 *
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 *
	 * @var MetamodelTranslator
	 */
	protected $translator;

	/**
	 *
	 * @var array
	 */
	public $metaModel;

	/**
	 *
	 * @var array
	 */
	protected $metaModelFilters;

	/** @var \PDOStatement */
	protected $insertStmt;

	/** @var \PDOStatement */
	protected $updateStmt;

	/** @var array */
	protected $updateSelect;

	/** @var \PDOStatement */
	protected $deleteStmt;

	/** @var \PDOStatement */
	protected $deleteUsingParentStmt;

	/** @var \PDOStatement */
	protected $deleteUsingChildrenStmt;

	/** @var \PDOStatement */
	protected $getChildrenFromIdStmt;

	/** @var \PDOStatement */
	protected $getChildrenFromUidStmt;

	/** @var \PDOStatement */
	protected $getParentStmt;

	/**
	 *
	 * @var array
	 */
	protected $options = array(
		'withtrans' => true
	);

	/**
	 *
	 * @var string
	 */
	public static $table = '';

	/** @var string */
	public static $vtable = '';

	/** @var string */
	public static $childTable = null;

	/** @var string */
	public static $childForeignKey = '';

	/** @var string */
	public static $parentTable = null;

	/** @var string */
	public static $parentForeignKey = '';

	/** @var string */
	public static $sequenceName = '';

	/** @var string */
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'uid' => 'uid',
		'parentId' => 'parentId',
		'parentUid' => 'parentUid',
		'parentCid' => 'parentCid',
		'childId' => 'childId',
		'childUid' => 'childUid',
		'childCid' => 'childCid',
		'dn' => 'dn',
		'name' => 'name',
		'index' => 'index',
		'attributes' => 'attributes'
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
		'attributes' => 'json'
	);

	/**
	 *
	 * @param \PDO $conn
	 */
	public function __construct($conn = null)
	{
		if ( $conn ) {
			$this->connexion = $conn;
		}
		else {
			$this->connexion = Connexion::get();
		}

		$this->metaModel = static::$sysToApp;
		$this->metaModelFilters = static::$sysToAppFilter;
		$this->_table = static::$table;
		$this->_vtable = static::$vtable;
		$this->_sequenceName = static::$sequenceName;
		$this->_sequenceKey = static::$sequenceKey;

		$this->_parentTable = static::$parentTable;
		$this->_parentForeignKey = static::$parentForeignKey;
		$this->_childTable = static::$childTable;
		$this->_childForeignKey = static::$childForeignKey;
	}

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function setFactory($factory)
	{
		$this->factory = $factory;
		return $this;
	}

	/**
	 *
	 * @return MetamodelTranslator
	 */
	public function getTranslator()
	{
		if ( !isset($this->translator) ) {
			$this->translator = new MetamodelTranslator($this->metaModel, $this->metaModelFilters);
		}
		return $this->translator;
	}

	/**
	 *
	 * @param \Rbplm\Any $mapped
	 * @return multitype:NULL unknown
	 */
	public function bind($mapped)
	{
		$bind = array();
		$properties = $mapped->getArrayCopy();

		$sysToApp = $this->metaModel;
		$sysToAppFilter = $this->metaModelFilters;

		/* @var \Rbs\Dao\Sier\MetamodelTranslator */
		$translator = $this->getTranslator();

		foreach( $sysToApp as $sysName => $appName ) {
			if ( isset($sysToAppFilter[$sysName]) && array_key_exists($appName, $properties) ) {
				$value = $properties[$appName];
				$filterMethod = $sysToAppFilter[$sysName] . 'ToSys';
				$bind[':' . $appName] = $translator->$filterMethod($value);
			}
			else {
				$bind[':' . $appName] = $properties[$appName];
			}
		}
		return $bind;
	}

	/**
	 *
	 * @return \PDO
	 */
	public function getConnexion()
	{
		if ( !$this->connexion ) {
			throw new Exception('CONNEXION_IS_NOT_SET', Error::ERROR);
		}
		return $this->connexion;
	}

	/**
	 *
	 * @todo Escape sql injection on select
	 *       $mapped \Rbh\Link
	 */
	public function insert($mapped, array $select = null)
	{
		$table = $this->_table;
		$sysToApp = $this->metaModel;

		$withTrans = $this->options['withtrans'];
		($this->connexion->inTransaction() == true) ? $withTrans = false : null;

		/* Init statement */
		if ( !$this->insertStmt ) {
			foreach( $sysToApp as $asSys => $asApp ) {
				$sysSet[] = '`' . $asSys . '`';
				$appSet[] = ':' . $asApp;
			}

			if ( $this->_sequenceName ) {
				$sql = 'UPDATE ' . $this->_sequenceName . ' SET ' . $this->_sequenceKey . '=LAST_INSERT_ID(' . $this->_sequenceKey . ' + 1) LIMIT 1;';
				$this->seqStmt = $this->connexion->prepare($sql);
			}

			$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
			$this->insertStmt = $this->connexion->prepare($sql);
		}

		try {
			if ( $withTrans ) $this->connexion->beginTransaction();
			if ( $this->_sequenceName ) {
				$this->seqStmt->execute();
				$id = $this->connexion->lastInsertId($this->_sequenceName);
				$mapped->setId($id);
				$bind = $this->bind($mapped);
				$this->insertStmt->execute($bind);
			}
			else {
				$bind = $this->bind($mapped);
				$this->insertStmt->execute($bind);
				$id = $this->connexion->lastInsertId($table);
				$mapped->setId($id);
			}
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 * @todo Escape sql injection on select
	 *
	 * @param \Rbplm\Any $mapped
	 * @param array $select
	 * @throws Exception
	 * @return Link
	 */
	public function update($mapped, $select = null)
	{
		$table = $this->_table;
		$sysToApp = $this->metaModel;

		$withTrans = $this->options['withtrans'];
		($this->connexion->inTransaction() == true) ? $withTrans = false : null;

		/* Init statement */
		if ( !$this->updateStmt and $this->updateSelect = $select ) {
			if ( is_array($select) ) {
				$sysToApp = array_intersect($sysToApp, $select);
			}
			foreach( $sysToApp as $asSys => $asApp ) {
				$set[] = $asSys . '=:' . $asApp;
			}
			$set = implode(',', $set);
			$idKey = array_search('id', $sysToApp);
			$sql = "UPDATE $table SET $set WHERE $idKey=:id;";
			$this->updateStmt = $this->connexion->prepare($sql);
			$this->updateSelect = $select;
		}

		$bind = $this->_bind($mapped);

		if ( is_array($select) ) {
			foreach( $select as &$val ) {
				$val = ':' . $val;
			}
			$bind = array_intersect_key($bind, array_flip($select));
			$bind[':id'] = $mapped->getId();
		}

		try {
			if ( $withTrans ) $this->connexion->beginTransaction();
			$this->updateStmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 * To do a partial update.
	 * Properties must be selected in array $properties as key=property name, value=property value.
	 *
	 * @todo Escape sql injection
	 *
	 * @param string $filter
	 * @param array $properties
	 * @return Link
	 */
	public function partialUpdate($filter, $properties)
	{
		$table = $this->_table;

		$set = '';
		foreach( $properties as $pname => $pvalue ) {
			$set .= "$pname=:$pname,";
			$bind[":$pname"] = $pvalue;
		}
		$set = trim($set, ',');

		$sql = 'UPDATE ' . $table . ' SET ' . $set . ' WHERE ' . $filter;
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		return $this;
	}

	/**
	 * Delete using children table.
	 *
	 * In filter may used children table field to select rows to delete.
	 * children table records will deleted with.
	 *
	 * @todo Escape sql injection
	 * @todo To test
	 */
	public function deleteUsingChildren($filter, $bind = null)
	{
		if ( !$this->deleteUsingChildrenStmt || $filter != $this->deleteUsingChildrenFilter ) {
			$linkTable = $this->_table;
			$childTable = $this->_childTable;
			$toSysChildId = 'childId';
			$childForeignKey = $this->_childForeignKey;

			$sql = "DELETE link, child FROM $linkTable AS link" . " JOIN $childTable AS child ON (link.$toSysChildId = child.$childForeignKey) WHERE $filter";

			$this->deleteUsingChildrenStmt = $this->connexion->prepare($sql);
			$this->deleteUsingChildrenFilter = $filter;
		}

		($this->connexion->inTransaction() == true) ? $withTrans = false : $withTrans = true;
		try {
			if ( $withTrans ) $this->connexion->beginTransaction();
			$this->deleteUsingChildrenStmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			var_dump($sql, $bind);
			die();
			throw $e;
		}

		return $this;
	}

	/**
	 * Delete using parent table.
	 *
	 * In filter may used parent table field to select rows to delete.
	 * parent table records are not deleted,
	 * So the parent table must have a foreign key constraint with ON DELETE CASCADE option.
	 *
	 * @todo Escape sql injection
	 * @todo To test
	 */
	public function deleteUsingParent($filter, $bind = null)
	{
		if ( !$this->deleteUsingParentStmt || $filter != $this->deleteUsingParentFilter ) {
			$linkTable = $this->_table;
			$parentTable = $this->_parentTable;
			$toSysParentId = 'parentId';
			$parentForeignKey = $this->_parentForeignKey;

			$sql = "DELETE link, parent FROM $linkTable AS link" . " JOIN $parentTable AS parent ON (link.$toSysParentId = parent.$parentForeignKey) WHERE $filter";

			$this->deleteUsingParentStmt = $this->connexion->prepare($sql);
			$this->deleteUsingParentFilter = $filter;
		}

		($this->connexion->inTransaction() == true) ? $withTrans = false : $withTrans = true;
		try {
			if ( $withTrans ) $this->connexion->beginTransaction();
			$this->deleteUsingParentStmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 *
	 * @todo Escape sql injection
	 */
	public function delete($filter, $bind = null)
	{
		$withTrans = $this->options['withtrans'];
		($this->connexion->inTransaction() == true) ? $withTrans = false : null;

		if ( !$this->deleteStmt || $filter != $this->deleteFilter ) {
			$table = $this->_table;
			$sql = "DELETE FROM $table WHERE $filter";
			$this->deleteStmt = $this->connexion->prepare($sql);
			$this->deleteFilter = $filter;
		}

		try {
			if ( $withTrans ) $this->connexion->beginTransaction();
			$this->deleteStmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 *
	 * @todo Escape sql injection
	 *
	 * @param int $parentId
	 * @param int $childId
	 * @param string $parentCid
	 */
	public function deleteFromParentAndChild($parentId, $childId, $parentCid = null, $childCid = null)
	{
		$toSysParentId = array_search('parentId', $this->metaModel);
		$toSysChildId = array_search('childId', $this->metaModel);
		$filter = "$toSysParentId=:parentId AND $toSysChildId=:childId";
		$bind = array(
			':parentId' => $parentId,
			':childId' => $childId
		);

		if ( $parentCid ) {
			$toSysParentCid = array_search('parentCid', $this->metaModel);
			$filter .= " AND $toSysParentCid=:parentCid";
			$bind[':parentCid'] = $parentCid;
		}
		if ( $childCid ) {
			$toSysChildCid = array_search('childCid', $this->metaModel);
			$filter .= " AND $toSysChildCid=:childCid";
			$bind[':childCid'] = $childCid;
		}

		$this->delete($filter, $bind);
		return $this;
	}

	/**
	 * Suppress all links without child or parent
	 */
	public function cleanOrpheanLinks()
	{
		throw new Exception('Not implemented method');

		$withTrans = $this->options['withtrans'];
		($this->connexion->inTransaction() == true) ? $withTrans = false : null;

		$table = $this->_table;
		$sql = "DELETE FROM $table as l WHERE l.childId NOT IN(SELECT id FROM anyobject) OR l.parentId NOT IN (SELECT id FROM anyobject)";
		$stmt = $this->connexion->prepare($sql);

		try {
			if ( $withTrans ) $this->connexion->beginTransaction();
			$stmt->execute();
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 * Return the PDOStatement populate with the full defintion of the children.
	 * If select is set return only the result of the select.
	 *
	 * Tables are Aliased with child. and link. prefix, so you can refer to this alias in your select and filters.
	 *
	 * @param array $select
	 * @param string $filter
	 * @return \PDOStatement
	 */
	public function getChildrenStmt($select = null, $filter = null)
	{
		if ( !$select ) {
			$select = array(
				'child.*'
			);
		}

		$table = $this->_table;

		$rightTable = $this->_childTable;
		$rightKey = $this->_childForeignKey;

		$sysToApp = $this->metaModel;
		$childIdKey = array_search('childId', $sysToApp);

		/* ADD LINK PROPERTIES TO SELECT */
		foreach( $sysToApp as $asSys => $asApp ) {
			$select[] = "link.$asSys AS link_$asApp";
		}
		$select = implode($select, ', ');

		$sql = "SELECT $select FROM $table AS link";

		/* Joins */
		if ( $rightTable && $rightKey ) {
			$sql .= " JOIN $rightTable AS child ON link.$childIdKey=child.$rightKey";
		}

		/* Filters */
		if ( $filter ) {
			$sql .= ' WHERE ' . $filter;
		}

		/* Prepare */
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		return $stmt;
	}

	/**
	 * Get children objects linked from dn, for each nodes of the branch.
	 * Link table must have a 'rdn' column
	 *
	 * @param string $dn
	 * @param 	array 				$select
	 * @param 	string 				$filter
	 * @param 	array 				$bind
	 * @return \PDOStatement
	 */
	public function getInherits($dn, $select = [
		'child.*'
	], $filter = '1=1', $bind = array())
	{
		$lnkTable = $this->_table;
		$childTable = $this->_childTable;
		$childKey = $this->_childForeignKey;
		$toSysChildId = array_search('childId', $this->metaModel);
		$selectStr = implode(',', $select);

		/* WITH PARENT :
		 $parentTable = $this->_parentTable;
		 $parentKey = $this->_parentForeignKey;
		 $toSysParentId = array_search('parentId', $this->metaModel);
		 $sql = "SELECT $select FROM $parentTable as parent";
		 $sql .= " JOIN $lnkTable AS lnk ON child.$parentKey=lnk.$toSysParentId AND :dn LIKE CONCAT(parent.$toSysDn,'%')";
		 $sql .= " JOIN $childTable AS child ON child.$childKey=lnk.$toSysChildId";
		 */

		$sql = "SELECT $selectStr FROM $lnkTable as lnk";
		$sql .= " JOIN $childTable AS child ON child.$childKey=lnk.$toSysChildId";
		$sql .= " WHERE :dn LIKE CONCAT(lnk.rdn,'%')";
		$sql .= " AND $filter";

		$bind[':dn'] = $dn;
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}

	/**
	 * Return the PDOStatement populate with the full defintion of the children of the parentId.
	 * If select is set, return only the result of the select.
	 * Note that at the first excution, the statment is build, so at second execution the previous select is reused.
	 * To recreate the statment you must explicitly reset the public property $loadparentStmt as :
	 * @code $link->getChildrenFromIdStmt = null;
	 *
	 * Tables are Aliased with child. and link. prefix, so you can refer to this alias in your select and filters
	 *
	 * @param integer $parentId
	 * @param array $select
	 * @param string $filter
	 * @return \PDOStatement
	 */
	public function getChildrenFromId($parentId, $select = null, $filter = '1=1', $bind = array())
	{
		if ( !$this->getChildrenFromIdStmt ) {
			$sysToApp = $this->metaModel;
			$parentIdKey = array_search('parentId', $sysToApp);

			/* Filters */
			$filter = "link.$parentIdKey=:parentId AND " . $filter;

			/* Prepare */
			$this->getChildrenFromIdStmt = $this->getChildrenStmt($select, $filter);
		}

		$bind[':parentId'] = $parentId;
		$this->getChildrenFromIdStmt->execute($bind);
		return $this->getChildrenFromIdStmt;
	}

	/**
	 * Return the PDOStatement populate with the full defintion of the children of the parentUid.
	 * If select is set, return only the result of the select.
	 * Note that at the first excution, the statment is build, so at second execution the previous select is reused.
	 * To recreate the statment you must explicitly reset the public property $loadparentStmt as :
	 * @code $link->getChildrenFromIdStmt = null;
	 *
	 * Tables are Aliased with child. and link. prefix, so you can refer to this alias in your select and filters
	 *
	 * @param integer $parentUid
	 * @param array $select
	 * @param string $filter
	 * @return \PDOStatement
	 */
	public function getChildrenFromUid($parentUid, $select = null, $filter = '1=1', $bind = array())
	{
		if ( !$this->getChildrenFromUidStmt ) {
			$sysToApp = $this->metaModel;
			$parentUidKey = array_search('parentUid', $sysToApp);

			/* Filters */
			$filter = "link.$parentUidKey=:parentUid AND " . $filter;

			/* Prepare */
			$this->getChildrenFromUidStmt = $this->getChildrenStmt($select, $filter);
		}

		$bind[':parentUid'] = $parentUid;
		$this->getChildrenFromUidStmt->execute($bind);
		return $this->getChildrenFromUidStmt;
	}

	/**
	 * Return the PDOStatement populate with the full defintion of the parent of the childId.
	 * If select is set return only the result of the select.
	 * Note that at the first excution, the statment is build, so at second excution the select is ignored.
	 * To recreate the statment you must explicitly reset the public property $loadparentStmt as :
	 * @code $link->loadparentStmt = null;
	 *
	 * @param integer $childId
	 * @param array $select
	 * @param string $filter
	 * @return \PDOStatement
	 */
	public function getParent($childId, $select = null, $filter = null)
	{
		if ( !$this->getParentStmt ) {
			if ( !$select ) {
				$select = array(
					'parent.*'
				);
			}

			$table = $this->_table;
			$leftKey = 'parentId';
			$joinTable = $this->_parentTable;
			$rightKey = $this->_parentForeignKey;
			$sysToApp = $this->metaModel;
			$childIdKey = array_search('childId', $sysToApp);

			$select = array();
			foreach( $sysToApp as $asSys => $asApp ) {
				$select[] = "link.$asSys AS link_$asApp";
			}
			$select = implode($select, ', ');

			$sql = "SELECT $select FROM $table AS link";

			if ( $joinTable && $rightKey ) {
				$sql .= " JOIN $joinTable AS parent ON link.$leftKey=parent.$rightKey";
			}
			$sql .= " WHERE link.$childIdKey=:childId";
			if ( $filter ) {
				$sql .= ' AND ' . $filter;
			}

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->getParentStmt = $stmt;
		}

		$this->getParentStmt->execute(array(
			':childId' => $childId
		));
		return $this->getParentStmt;
	}
} /* End of class */
