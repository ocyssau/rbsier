<?php
//%LICENCE_HEADER%
namespace Rbs\Dao\Sier;

use Rbplm\Dao\FilterAbstract;

/**
 * Create params to search in database.
 *
 */
class Filter extends FilterAbstract
{

	/**
	 * Binded parameters to send to PDO preapred statement
	 * @var array
	 */
	protected $bind = [];

	/**
	 * @return array
	 */
	public function getBind()
	{
		return $this->bind;
	}

	/**
	 * @return Filter
	 */
	public function setBind(array $bind)
	{
		$this->bind = $bind;
		return $this;
	}
}
