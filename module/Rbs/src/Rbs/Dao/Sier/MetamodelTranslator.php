<?php
namespace Rbs\Dao\Sier;

/**
 * 
 *
 */
class MetamodelTranslator implements \Rbplm\Dao\MetamodelTranslatorInterface
{

	/**
	 * Date format use by db
	 * @var string
	 */
	const DATE_FORMAT = 'Y-m-d H:i:s';

	/**
	 *
	 * @param array $metaModel As defined by Dao::$sysToApp
	 * @param array $metaModelFilters As defined by Dao::$sysToAppFilter
	 */
	public function __construct($metaModel, $metaModelFilters)
	{
		$this->sysToAppMetaModel = $metaModel;
		$this->metamodelFilters = $metaModelFilters;
		$this->appToSysMetaModel = array_flip($metaModel);
	}

	/**
	 * @see Rbplm\Dao.DaoInterface::toApp()
	 *
	 * Translate property name and value to model semantic
	 *
	 * @param array|string		$in		Db properties name or array (key as db column name => value)
	 * @return array			Model properties
	 */
	public function toApp($in)
	{
		$sysToApp = $this->sysToAppMetaModel;
		$sysToAppFilter = $this->metamodelFilters;

		if ( is_array($in) ) {
			$out = array();
			foreach( $in as $asSys => $value ) {
				if ( isset($sysToApp[$asSys]) ) {
					$appName = $sysToApp[$asSys];
					if ( isset($sysToAppFilter[$asSys]) ) {
						$filterMethod = $sysToAppFilter[$asSys] . 'ToApp';
						$value = static::$filterMethod($value);
					}
					$out[$appName] = $value;
				}
				else {
					$out[$asSys] = $value;
				}
			}
		}
		else {
			if ( isset($sysToApp[$in]) ) {
				$out = $sysToApp[$in];
			}
			else {
				$out = $in;
			}
		}
		return $out;
	}

	/**
	 * @see Rbplm\Dao.DaoInterface::toSys()
	 *
	 * Translate current row data from model to db semantic
	 *
	 * @param array|string		 $in	Application property name or array (key as model property name => value)
	 * @return array					System attributs
	 */
	public function toSys($in)
	{
		$out = [];

		$appToSys = $this->appToSysMetaModel;
		$sysToAppFilter = $this->metamodelFilters;

		if ( is_array($in) ) {
			foreach( $in as $appName => $value ) {
				if ( isset($appToSys[$appName]) ) {
					$sysName = $appToSys[$appName];
					if ( isset($sysToAppFilter[$sysName]) ) {
						$filterMethod = $sysToAppFilter[$sysName] . 'ToSys';
						$value = static::$filterMethod($value);
					}
					$out[$sysName] = $value;
				}
				else {
					$out[$appName] = $value;
				}
			}
		}
		else {
			if ( isset($appToSys[$in]) ) {
				$out = $appToSys[$in];
			}
			else {
				$out = $in;
			}
		}

		return $out;
	}

	/**
	 * Get select with AS applicationName
	 * @param string $alias Alias table name added on each select
	 * @param string $select if is set as array, return a select limited by this selection. Each entry must be a valid application property name
	 * @return array
	 */
	public function getSelectAsApp($alias = '', $select = null)
	{
		$out = [];
		$translator = $this->sysToAppMetaModel;

		if ( !$select ) {
			$select = array_keys($translator);
		}

		foreach( $select as $asSys ) {
			$asApp = $translator[$asSys];
			if ( $alias ) {
				$out[] = "`$alias`.`$asSys` AS `$asApp`";
			}
			else {
				$out[] = "`$asSys` AS `$asApp`";
			}
		}

		return $out;
	}

	/**
	 * @param string $in
	 * @return array
	 */
	public static function jsonToApp($json)
	{
		if ( $json[0] == '{' || $json[0] == '[' ) {
			return json_decode($json, true);
		}
		else {
			return $json;
		}
	}

	/**
	 * @param string $in
	 * @return array
	 */
	public static function jsonToSys($array)
	{
		if ( is_array($array) ) {
			$ret = json_encode($array, true);
		}
		elseif ( $array instanceof \SplFixedArray ) {
			$ret = json_encode($array->toArray(), true);
		}
		elseif ( $array instanceof \JsonSerializable ) {
			$ret = json_encode($array, true);
		}
		else {
			$ret = $array;
		}
		return $ret;
	}

	/**
	 *
	 * @param string $yesOrNo
	 */
	public static function yesOrNoToApp($yesOrNo)
	{
		return ($yesOrNo == 'y') ? true : false;
	}

	/**
	 * @param string $yesOrNo
	 */
	public static function yesOrNoToSys($yesOrNo)
	{
		return ($yesOrNo == true) ? 'y' : 'n';
	}

	/**
	 *
	 * @param string|integer|boolean $bool
	 * @return int
	 */
	public static function booleanToApp($bool)
	{
		return ($bool) ? true : false;
	}

	/**
	 * @param string|integer|boolean $bool
	 * @return int
	 */
	public static function booleanToSys($bool)
	{
		return ($bool) ? 1 : 0;
	}

	/**
	 * @param string $in
	 * @return \DateTime
	 */
	public static function dateToApp($timestamp)
	{
		if ( $timestamp == 0 ) {
			return new \Rbplm\Sys\Date();
		}
		else {
			return new \Rbplm\Sys\Date((int)$timestamp);
		}
	}

	/**
	 * @param \DateTime $in
	 */
	public static function dateToSys($in)
	{
		if ( $in instanceof \DateTime ) {
			return $in->getTimestamp();
		}
		elseif ( $in == 0 ) {
			$date = new \DateTime();
			return $date->getTimestamp();
		}
		elseif ( is_string($in) ) {
			$date = new \DateTime($in);
			return $date->getTimestamp();
		}
	}

	/**
	 * @param string $in
	 * @return \DateTime
	 */
	public static function datetimeToApp($string)
	{
		if ( $string == '' ) {
			return null;
		}
		else {
			return new \Rbplm\Sys\Date($string);
		}
	}

	/**
	 * @param \DateTime $in
	 */
	public static function datetimeToSys($in)
	{
		if ( $in instanceof \DateTime ) {
			return $in->format(self::DATE_FORMAT);
		}
		elseif ( $in == 0 ) {
			return null;
		}
		elseif ( is_string($in) ) {
			$date = new \Rbplm\Sys\Date($in);
			return $date->format(self::DATE_FORMAT);
		}
	}

	/**
	 * @param string $in
	 * @return \DateInterval
	 */
	public static function dateintervalToApp($string)
	{
		if ( $string == '' ) {
			return null;
		}
		else {
			return new \DateInterval($string);
		}
	}

	/**
	 * @param \DateInterval $in
	 */
	public static function dateintervalToSys($in)
	{
		if ( $in instanceof \DateInterval ) {
			return $in->format(self::DATE_FORMAT);
		}
		if ( $in == 0 ) {
			return null;
		}
	}

	/**
	 * @param string $in
	 * @return \DateInterval
	 */
	public static function cryptToApp($in)
	{
		if ( $in ) {
			return \Rbs\Sys\Crypt::decrypt($in);
		}
		else {
			return null;
		}
	}

	/**
	 * @param \DateInterval $in
	 */
	public static function cryptToSys($in)
	{
		if ( $in ) {
			return \Rbs\Sys\Crypt::crypt($in);
		}
		else {
			return null;
		}
	}
}
