<?php
// %LICENCE_HEADER%
namespace Rbs\Postit;

use Rbplm\Any;

class Postit extends Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Owned;
	use \Rbplm\Mapped;

	static $classId = '56c4876b1e741';

	/**
	 *
	 * @var string
	 */
	protected $body;

	/**
	 * The object concerned by this postit
	 *
	 * @var string
	 */
	protected $parent = null;

	/** @var integer */
	public $parentId = null;

	/** @var string */
	public $parentUid = null;

	/** @var string */
	public $parentCid = null;

	/** @var integer */
	public $spacename = null;

	/**
	 *
	 * @param array $properties
	 * @return Postit
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['parentId'])) ? $this->parentId = $properties['parentId'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid = $properties['parentUid'] : null;
		(isset($properties['parentCid'])) ? $this->parentCid = $properties['parentCid'] : null;
		(isset($properties['ownerId'])) ? $this->ownerId = $properties['ownerId'] : null;
		(isset($properties['body'])) ? $this->body = $properties['body'] : null;
		(isset($properties['spacename'])) ? $this->spacename = $properties['spacename'] : null;
		return $this;
	}

	/**
	 *
	 * @param string $string
	 * @return Postit
	 */
	public function setBody($string)
	{
		$this->body = $string;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * Set the owner parent document
	 *
	 * @param \Rbplm\AnyPermanent $parent
	 * @return Postit
	 */
	public function setParent($parent)
	{
		$this->parentId = $parent->getId();
		$this->parentUid = $parent->getUid();
		$this->parentCid = $parent->cid;
		return $this;
	}

	/**
	 * Get the discussion id.
	 *
	 * @return \Rbplm\AnyPermanent
	 */
	public function getParent($asId = false)
	{
		if ( $asId ) {
			return $this->parentId;
		}
		return $this->parent;
	}
}
