<?php
//%LICENCE_HEADER%

namespace Rbs\Postit;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
-- ************************
CREATE TABLE IF NOT EXISTS `postit` (
  `id` int(11) NOT NULL,
  `uid` VARCHAR(32) NOT NULL,
  `cid` VARCHAR(32) NOT NULL,
  `name` VARCHAR(128) DEFAULT NULL,
  `parent_id` INT(11) NOT NULL,
  `parent_uid` VARCHAR(32) DEFAULT NULL,
  `parent_cid` VARCHAR(32) NOT NULL,
  `owner_id` VARCHAR(32) DEFAULT NULL,
  `spacename` VARCHAR(32) NOT NULL,
  `body` MEDIUMTEXT DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_uid` (`uid`),
  KEY `K_parentId` (`parent_id`),
  KEY `K_parentUid` (`parent_uid`),
  KEY `K_parentCid` (`parent_cid`),
  KEY `K_spaceName` (`spacename`),
  KEY `K_ownerId` (`owner_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `postit_seq`(
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO postit_seq(id) VALUES(10);

 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class PostitDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='postit';
	public static $vtable='postit';
	public static $sequenceName='postit_seq';

	/**
	 * @var \PDOStatement
	 */
	protected $getFromParentStmt;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'cid'=>'cid',
		'name'=>'name',
		'parent_id'=>'parentId',
		'parent_uid'=>'parentUid',
		'parent_cid'=>'parentCid',
		'spacename'=>'spacename',
		'owner_id'=>'ownerId',
		'body'=>'body',
	);

	/**
	 * @param integer $parentId
	 * @return \PDOStatement
	 */
	public function getFromParent($parentId)
	{
		$spacename = $this->factory->getName();

		if(!$this->getFromParentStmt){
			$table = $this->_table;
			$select = $this->getSelectAsApp();
			$selectStr = implode(',', $select);
			$sql = "SELECT $selectStr FROM $table WHERE parent_id=:parentId AND spacename=:spacename";
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->getFromParentStmt = $stmt;
		}

		/**/
		$bind = array(
			':parentId'=>$parentId,
			':spacename'=>$spacename
		);

		/**/
		$this->getFromParentStmt->execute($bind);
		return $this->getFromParentStmt;
	}

}
