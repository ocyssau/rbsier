<?php
namespace Rbs;

use Zend\Filter\FilterInterface;

/**
 * 
 *
 */
class Number implements FilterInterface
{
	/**
	 * preg_replace search chars to replace by $replace
	 * @var string
	 */
	static public $search = '[^A-Za-z0-9\-_.#]';
	
	/**
	 * preg_replace replace term for $search chars
	 * @var string
	 */
	static public $replace = '';
	
	/**
	 * Remove spaces and specials characters from the $string
	 *
	 * @param string $string
	 * @return string
	 */
	static function noaccent($string)
	{
		/* Replaces all spaces with hyphens. */
		//setlocale(LC_ALL, 'fr_FR.utf8');
		$string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
		return $string;
	}

	/**
	 * Remove spaces and specials characters from the $string
	 *
	 * @param string $string
	 * @return string
	 */
	static function normalize($string)
	{
		/* Replaces all spaces with hyphens. */
		$string = str_replace(' ', '-', $string);
		$string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
		$string = preg_replace('/'.self::$search.'/', self::$replace, $string);
		$string = strtolower($string);
		return $string;
	}

	/**
	 * Returns the result of filtering $value
	 *
	 * @param  mixed $value
	 * @return mixed
	 */
	public function filter($value)
	{
		return self::normalize($value);
	}
}
