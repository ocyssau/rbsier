<?php
//%LICENCE_HEADER%
namespace Rbs\Batch;

/**
 * CallbackInterface interface
 * 
 * Must be implemented by callbock for execution of cronTasks
 * 
 */
interface CallbackInterface 
{
	/**
	 * @param array $params
	 * @param \Rbs\Batch\Job $job
	 */
	public function __construct($params, \Rbs\Batch\Job $job);
	
	/**
	 * 
	 */
	public function run();
}
