<?php
// %LICENCE_HEADER%
namespace Rbs\Batch;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `batch` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(258) NULL,
 `owner_id` INT(11) NULL,
 `owner_uid` VARCHAR(128) NULL,
 `callback_class` TEXT NULL,
 `callback_method` VARCHAR(64) NULL,
 `callback_params` TEXT NULL,
 `comment` TEXT NULL,
 PRIMARY KEY (`id`),
 UNIQUE (`uid`),
 INDEX (`owner_id`),
 INDEX (`owner_uid`)
 ) ENGINE=MyIsam;
 <<*/

/** SQL_INSERT>>
 << */

/** SQL_ALTER>>
 << */

/** SQL_FKEY>>
 << */

/** SQL_TRIGGER>>
 << */

/** SQL_VIEW>>
 << */

/** SQL_DROP>>
 << */

/**
 * Job to execute in batch mode.
 */
class BatchDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'batch';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'batch';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = null;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'callback_class' => 'callbackClass',
		'callback_method' => 'callbackMethod',
		'callback_params' => 'callbackParams',
		'owner_id' => 'ownerId',
		'owner_uid' => 'ownerUid',
		'comment' => 'comment'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'callback_params' => 'json'
	);

	/**
	 *
	 * @param integer $id
	 */
	public function getCommentAsText($id)
	{
		$table = $this->_table;
		$sql = "SELECT CONVERT(log USING utf8) FROM $table WHERE id=:id";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':id' => $id
		));
		return $stmt->fetchColumn(1);
	}
} /* End of class */
