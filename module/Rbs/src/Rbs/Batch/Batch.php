<?php
namespace Rbs\Batch;

/**
 * 
 */
class Batch extends \Rbplm\Any implements \Rbplm\Dao\MappedInterface
{
	/**
	 * 
	 */
	use \Rbplm\Mapped;
	
	/**
	 * 
	 */
	use \Rbplm\Owned;

	/**
	 * @var string
	 */
	static $classId = 'batch86g598qd';

	/** @var string */
	protected $callbackClass;

	/** @var string */
	protected $callbackMethod;

	/** @var array */
	protected $callbackParams;

	/** @var string */
	protected $comment;

	/**
	 *
	 * @param array $properties
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
	}

	/**
	 * Init properties.
	 *
	 * @param string $name
	 * @return Batch
	 */
	public static function init()
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		return $obj;
	}

	/**
	 * @return Batch
	 */
	public function hydrate(array $properties)
	{
		$this->ownedHydrate($properties);
		$this->mappedHydrate($properties);
		(isset($properties['callbackClass'])) ? $this->callbackClass = $properties['callbackClass'] : null;
		(isset($properties['callbackMethod'])) ? $this->callbackMethod = $properties['callbackMethod'] : null;
		(isset($properties['callbackParams'])) ? $this->callbackParams = $properties['callbackParams'] : null;
		(isset($properties['comment'])) ? $this->comment = $properties['comment'] : null;
		return $this;
	}

	/**
	 * 
	 * @param string $comment
	 * @return Batch
	 */
	public function setComment($comment)
	{
		$this->comment = trim($comment);
		return $this;
	}

	/**
	 * 
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}
}
