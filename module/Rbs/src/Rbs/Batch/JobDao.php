<?php
// %LICENCE_HEADER%
namespace Rbs\Batch;

use Rbs\Dao\Sier as DaoSier;
use Rbs\Dao\Sier\MetamodelTranslator;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `batchjob` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(258) NULL,
 `callback_class` TEXT NULL,
 `callback_method` VARCHAR(64) NULL,
 `callback_params` TEXT NULL,
 `owner_id` INT(11) NULL,
 `owner_uid` VARCHAR(128) NULL,
 `status` VARCHAR(32) NULL,
 `planned` DATETIME NULL,
 `created` DATETIME NULL,
 `started` DOUBLE NULL,
 `ended` DOUBLE NULL,
 `duration` FLOAT NULL,
 `hashkey` VARCHAR(40) NOT NULL,
 `pid` VARCHAR(32) NULL,
 `sapi` VARCHAR(32) NULL,
 `log` LONGBLOB NULL,
 PRIMARY KEY (`id`),
 INDEX `K_batchjob_1` (`planned`),
 INDEX `K_batchjob_2` (`status`),
 INDEX `K_batchjob_3` (`name`),
 UNIQUE `U_batchjob_4` (`hashkey`)
 ) ENGINE=MyIsam;
 <<*/

/** SQL_INSERT>>
 << */

/** SQL_ALTER>>
 << */

/** SQL_FKEY>>
 << */

/** SQL_TRIGGER>>
 << */

/** SQL_VIEW>>
 << */

/** SQL_DROP>>
 << */

/**
 * Job to execute in batch mode.
 */
class JobDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'batchjob';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'batchjob';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = null;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'name' => 'name',
		'callback_class' => 'callbackClass',
		'callback_method' => 'callbackMethod',
		'callback_params' => 'callbackParams',
		'created' => 'created',
		'owner_id' => 'ownerId',
		'owner_uid' => 'ownerUid',
		'planned' => 'planned',
		'status' => 'status',
		'started' => 'started',
		'ended' => 'ended',
		'duration' => 'duration',
		'hashkey' => 'hashkey',
		'pid' => 'pid',
		'sapi' => 'sapi',
		'log' => 'log'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created' => 'datetime',
		'planned' => 'datetime',
		'callback_params' => 'json'
	);

	/**
	 * @var \PDOStatement
	 */
	protected $endStmt;

	/**
	 * read the first task which need to be run by cron
	 *
	 * @return \PDOStatement
	 **/
	public function getNeedToRun()
	{
		$currentTime = date(self::DATE_FORMAT);
		$table = $this->_table;
		
		$sql = "SELECT * FROM $table AS job";
		$sql .= " WHERE job.status = :initStatus AND (planned <= :currentTime )";
		
		$bind = array(
			':initStatus' => Job::STATE_INIT,
			':currentTime' => $currentTime
		);
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}

	/**
	 *
	 * @param integer $id
	 */
	public function getLogAsText($id)
	{
		$table = $this->_table;
		$sql = "SELECT CONVERT(log USING utf8) FROM $table WHERE id=:id";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':id' => $id
		));
		return $stmt->fetchColumn(1);
	}

	/**
	 * @return JobDao
	 */
	public function end($id, $status)
	{
		if ( !$this->endStmt ) {
			$table = $this->_table;
			$sql = "UPDATE $table SET status=:status, ended=:ended WHERE id=:id";
			$this->endStmt = $this->connexion->prepare($sql);
		}
		
		$ended = new \DateTime();
		$this->endStmt->execute(array(
			':ended' => MetamodelTranslator::datetimeToSys($ended),
			':status' => $status,
			':id' => $id
		));
		return $this;
	}

	/**
	 * @param \DateTime $older
	 * @throws \Exception
	 * @return JobDao
	 */
	public function cleanup($older = null)
	{
		$connexion = $this->connexion;
		$table = $this->_table;
		
		if ( !$older ) {
			$older = new \DateTime();
			$older->modify('-1 month');
		}
		
		/* clean log */
		try {
			$sql = "DELETE FROM $table WHERE ended IS NOT NULL AND ended < :older";
			$cleanLogstmt = $connexion->prepare($sql);
			$bind = array(
				':older' => $older->getTimestamp()
			);
			$this->lastStmt = $cleanLogstmt;
			$this->lastBind = $bind;
			$cleanLogstmt->execute($bind);
			return $this;
		}
		catch( \Exception $e ) {
			throw $e;
		}
	}

	/**
	 * @param \DateTime $older
	 * @throws \Exception
	 * @return JobDao
	 */
	public function closeLongtime($older = null)
	{
		$connexion = $this->connexion;
		$table = $this->_table;
		
		if ( !$older ) {
			$older = new \DateTime();
			$older->modify('-1 day');
		}
		
		/* clean log */
		try {
			$sql = "UPDATE $table SET status='failed', ended=NOW() WHERE ended IS NULL AND started < :older";
			$cleanupLongtimeStmt = $connexion->prepare($sql);
			$bind = array(
				':older' => $older->getTimestamp()
			);
			$this->lastStmt = $cleanupLongtimeStmt;
			$this->lastBind = $bind;
			$cleanupLongtimeStmt->execute($bind);
			return $this;
		}
		catch( \Exception $e ) {
			throw $e;
		}
	}
	

} /* End of class */
