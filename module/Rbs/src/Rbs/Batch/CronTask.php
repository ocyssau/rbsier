<?php
//%LICENCE_HEADER%
namespace Rbs\Batch;

use Rbplm\Dao\MappedInterface;
use Rbplm\AnyObject;
use Rbplm\Mapped;
use DateTime;

/**
 * CronTask class
 */
class CronTask extends AnyObject implements MappedInterface
{
	use Mapped;

	/**
	 * @var string
	 */
	static $classId = 'crontask35f98';

	/** @var integer */
	const STATE_INIT = 0;

	/** @var integer */
	const STATE_WAITING = 1;

	/** @var integer */
	const STATE_RUNNING = 2;

	/** @var integer */
	const MODE_INTERNAL = 1;

	/** @var integer */
	const MODE_EXTERNAL = 2;
	
	/**
	 * Reference to the CronService
	 * @var \Batch\Service\CronService
	 */
	public $service;
	

	/** @var string */
	public $description;

	/** @var string */
	protected $callbackClass;

	/** @var string */
	protected $callbackMethod;

	/** @var array */
	protected $callbackParams;

	/** @var string */
	protected $status;

	/** @var DateTime */
	protected $frequency;

	/** @var DateTime */
	protected $scheduleStart;

	/** @var DateTime */
	protected $scheduleEnd;

	/** @var DateTime */
	protected $lastExec;

	/** @var boolean */
	protected $actif = true;

	/**
	 * @param string $name
	 * @return \Rbplm\Any
	 */
	public static function init($name = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		$obj->created = new DateTime();
		$obj->setStatus($class::STATE_INIT);
		$obj->setName($name);
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return \Rbplm\Any
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		$this->mappedHydrate($properties);
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['callbackClass'])) ? $this->callbackClass = $properties['callbackClass'] : null;
		(isset($properties['callbackMethod'])) ? $this->callbackMethod = $properties['callbackMethod'] : null;
		(is_array($properties['callbackParams'])) ? $this->callbackParams = $properties['callbackParams'] : null;
		(isset($properties['created'])) ? $this->created = $properties['created'] : null;
		(isset($properties['ownerId'])) ? $this->ownerId = $properties['ownerId'] : null;
		(isset($properties['ownerUid'])) ? $this->ownerUid = $properties['ownerUid'] : null;
		(isset($properties['status'])) ? $this->status = $properties['status'] : null;
		(isset($properties['frequency'])) ? $this->frequency = $properties['frequency'] : null;
		(isset($properties['scheduleStart'])) ? $this->scheduleStart = $properties['scheduleStart'] : null;
		(isset($properties['scheduleEnd'])) ? $this->scheduleEnd = $properties['scheduleEnd'] : null;
		(isset($properties['lastExec'])) ? $this->lastExec = $properties['lastExec'] : null;
		(isset($properties['actif'])) ? $this->actif = (boolean)$properties['actif'] : false;
		return $this;
	}

	/**
	 * Factory for jobs
	 * @return Job
	 */
	public function newJob()
	{
		$job = Job::init('cron.' . $this->name . '.' . time())->hash();
		$job->cron = $this;
		$job->hydrate(array(
			'ownerId' => $this->ownerId,
			'ownerUid' => $this->ownerUid,
			'callbackClass' => $this->callbackClass,
			'callbackMethod' => $this->callbackMethod,
			'status' => Job::STATE_INIT
		));
		return $job;
	}

	/**
	 * @param Integer $time in seconds
	 */
	public function setScheduleStart($time)
	{
		$this->scheduleStart = (int)$time;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getScheduleStart()
	{
		return $this->scheduleStart;
	}

	/**
	 * @param Integer $time in seconds
	 */
	public function setScheduleEnd($time)
	{
		$this->scheduleEnd = (int)$time;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getScheduleEnd()
	{
		return $this->scheduleEnd;
	}

	/**
	 * @param Integer $time in seconds
	 */
	public function setFrequency($time)
	{
		$this->frequency = (int)$time;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getFrequency()
	{
		return $this->frequency;
	}

	/**
	 * @param DateTime $dateTime
	 */
	public function setLastExec(\DateTime $dateTime)
	{
		$this->lastExec = $dateTime;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getLastExec()
	{
		return $this->lastExec;
	}

	/**
	 * @param string $string
	 */
	public function setStatus($string)
	{
		$this->status = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param array $array key 0: class name, 1: method name
	 */
	public function setCallback($array)
	{
		$this->callbackClass = $array[0];
		$this->callbackMethod = $array[1];
		
		if ( is_array($array[2]) ) {
			$this->callbackParams = $array[2];
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function getCallback()
	{
		return array(
			$this->callbackClass,
			$this->callbackMethod
		);
	}

	/**
	 * @param array|string $mixed
	 */
	public function setCallbackParams($mixed)
	{
		if ( is_array($mixed) ) {
			$this->callbackParams = $mixed;
		}
		else if ( is_string($mixed) && ($mixed[0] == '[' || $mixed[0] == '{') ) {
			$this->callbackParams = json_decode($mixed, true);
		}
		else {
			throw new \UnexpectedValueException('$mixed must be a array or a json string');
		}
		$this->hashkey = md5($this->data . $this->ownerId);
		return $this;
	}

	/**
	 * @return array
	 */
	public function getCallbackParams()
	{
		return $this->callbackParams;
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isActif($bool = null)
	{
		if ( is_bool($bool) ) {
			if ( $bool == false ) {
				$this->status = self::STATE_INIT;
			}
			else {
				$this->status = self::STATE_WAITING;
			}
			return $this->actif = $bool;
		}
		else {
			return $this->actif;
		}
	}

	/**
	 * Translate status to string
	 *
	 * @param $status integer
	 *
	 * @return string
	 **/
	static public function getStatusName($status)
	{
		switch ($status) {
			case self::STATE_RUNNING:
				return 'Running';
			case self::STATE_WAITING:
				return 'Scheduled';
			case self::STATE_INIT:
				return 'Init';
		}
		return '???';
	}
}
