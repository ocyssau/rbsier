<?php
//%LICENCE_HEADER%
namespace Rbs\Batch;

use Rbplm\Dao\MappedInterface;
use Rbplm\AnyObject;
use Rbplm\Mapped;
use Rbplm\People;
use DateTime;

/**
 * Job to execute in batch mode.
 *
 */
class Job extends AnyObject implements MappedInterface
{
	use Mapped;

	/**
	 * @var string
	 */
	static $classId = 'batchjob35c98';

	/**
	 *
	 * @var string
	 */
	const STATE_INIT = 'init';

	/**
	 *
	 * @var string
	 */
	const STATE_SUCCESS = 'success';

	/**
	 *
	 * @var string
	 */
	const STATE_FAILED = 'failed';

	/**
	 *
	 * @var string
	 */
	const STATE_WARNING = 'warning';

	/**
	 *
	 * @var string
	 */
	const STATE_RUNNING = 'running';

	/**
	 *
	 * @var string
	 */
	const STATE_UNCOMPLETE = 'uncomplete';

	/** @var string */
	protected $callbackClass;

	/** @var string */
	protected $callbackMethod;

	/** @var array */
	protected $callbackParams;

	/**
	 *
	 * @var People\User
	 */
	protected $runner;

	/**
	 *
	 * @var string
	 */
	protected $log;

	/**
	 *
	 * @var string
	 */
	protected $pid;

	/**
	 *
	 * @var string
	 */
	protected $sapi;

	/**
	 *
	 * @var string
	 */
	protected $status;

	/**
	 *
	 * @var \DateTime
	 */
	protected $planned;

	/**
	 *
	 * @var \DateTime
	 */
	protected $created;

	/**
	 * microtime
	 * @var integer
	 */
	protected $started = null;

	/**
	 * microtime
	 * @var integer
	 */
	protected $ended = null;

	/**
	 * microtime
	 * @var integer
	 */
	protected $duration = 0;

	/**
	 *
	 * @var string
	 */
	protected $hashkey = null;

	/**
	 * Reference to root CronTask
	 * 
	 * @var \Rbs\Batch\CronTask
	 */
	public $cron;

	/**
	 *
	 * @param string $name
	 * @return Job
	 */
	public static function init($name = null, $parent = null)
	{
		$obj = parent::init($name);
		$obj->created = new DateTime();
		$obj->setStatus('init');
		$obj->setPid(getmypid());
		$obj->setSapi(php_sapi_name());
		return $obj;
	}

	/**
	 *
	 */
	public function hash()
	{
		$this->hashkey = md5($this->getName() . $this->ownerUid);
		return $this;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Job
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		$this->mappedHydrate($properties);
		(isset($properties['callbackClass'])) ? $this->callbackClass = $properties['callbackClass'] : null;
		(isset($properties['callbackMethod'])) ? $this->callbackMethod = $properties['callbackMethod'] : null;
		(isset($properties['callbackParams']) && is_array($properties['callbackParams'])) ? $this->callbackParams = $properties['callbackParams'] : null;
		(isset($properties['ownerId'])) ? $this->ownerId = $properties['ownerId'] : null;
		(isset($properties['ownerUid'])) ? $this->ownerUid = $properties['ownerUid'] : null;
		(isset($properties['created'])) ? $this->created = $properties['created'] : null;
		(isset($properties['planned'])) ? $this->planned = $properties['planned'] : null;
		(isset($properties['started'])) ? $this->started = $properties['started'] : null;
		(isset($properties['ended'])) ? $this->ended = $properties['ended'] : null;
		(isset($properties['duration'])) ? $this->duration = $properties['duration'] : null;
		(isset($properties['status'])) ? $this->status = $properties['status'] : null;
		(isset($properties['hashkey'])) ? $this->hashkey = $properties['hashkey'] : null;
		(isset($properties['log'])) ? $this->log = $properties['log'] : null;
		(isset($properties['pid'])) ? $this->pid = $properties['pid'] : null;
		(isset($properties['sapi'])) ? $this->sapi = $properties['sapi'] : null;
		return $this;
	}

	/**
	 * @param array $array key 0: class name, 1: method name
	 */
	public function setCallback($array)
	{
		$this->callbackClass = $array[0];
		$this->callbackMethod = $array[1];
		
		if ( is_array($array[2]) ) {
			$this->callbackParams = $array[2];
		}
		
		return $this;
	}

	/**
	 * @return array
	 */
	public function getCallback()
	{
		return array(
			$this->callbackClass,
			$this->callbackMethod,
			$this->callbackParams
		);
	}

	/**
	 * @param array|string $mixed
	 */
	public function setCallbackParams($mixed)
	{
		if ( is_array($mixed) ) {
			$this->callbackParams = $mixed;
		}
		else if ( is_string($mixed) && ($mixed[0] == '[' || $mixed[0] == '{') ) {
			$this->callbackParams = json_decode($mixed, true);
		}
		else if ( is_null($mixed) ) {
			$this->callbackParams = null;
		}
		else {
			throw new \UnexpectedValueException('$mixed must be a array or a json string');
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function getCallbackParams()
	{
		return $this->callbackParams;
	}

	/**
	 * @param string $string
	 */
	public function setLog($string)
	{
		$this->log = $string;
		return $this;
	}

	/**
	 * @return array|string
	 */
	public function getLog()
	{
		return $this->log;
	}

	/**
	 * @param string $str
	 */
	public function setStatus($str)
	{
		$this->status = $str;
		return $this;
	}

	/**
	 * Set the php process id
	 *
	 * @param string $pid
	 */
	public function setPid($pid)
	{
		$this->pid = $pid;
		return $this;
	}

	/**
	 * Get the php process id
	 *
	 * @return string
	 */
	public function getPid()
	{
		return $this->pid;
	}

	/**
	 * Set the php Sapi name
	 *
	 * @see http://php.net/manual/function.php-sapi-name.php
	 * @param string $string
	 */
	public function setSapi($string)
	{
		$this->sapi = $string;
		return $this;
	}

	/**
	 * Get the php Sapi name
	 *
	 * @see http://php.net/manual/function.php-sapi-name.php
	 * @return string
	 */
	public function getSapi()
	{
		return $this->sapi;
	}

	/**
	 * @param DateTime $date
	 */
	public function setPlanned($date)
	{
		$this->planned = $date;
		return $this;
	}

	/**
	 *
	 * @return \Rbs\Batch\Job
	 */
	public function start()
	{
		$this->started = microtime(true);
		return $this;
	}

	/**
	 *
	 * @return \Rbs\Batch\Job
	 */
	public function end()
	{
		$this->ended = microtime(true);
		$this->duration = $this->ended - $this->started;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getDuration()
	{
		return $this->duration;
	}
} /* End of class */
