<?php
// %LICENCE_HEADER%
namespace Rbs\Batch;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `cron_task` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(258) NULL,
 `description` VARCHAR(258) NULL,
 `callback_class` TEXT NULL,
 `callback_method` VARCHAR(64) NULL,
 `callback_params` TEXT NULL,
 `owner_id` INT(11) NULL,
 `owner_uid` VARCHAR(128) NULL,
 `status` VARCHAR(32) NULL,
 `created` DATETIME NULL,
 `exec_frequency` DECIMAL NULL,
 `exec_schedule_start` DECIMAL NULL,
 `exec_schedule_end` DECIMAL NULL,
 `last_exec` DATETIME NULL,
 `is_actif` tinyint DEFAULT 1,
 PRIMARY KEY (`id`),
 INDEX `K_cron_task_1` (`name`),
 INDEX `K_cron_task_2` (`owner_id`),
 INDEX `K_cron_task_3` (`owner_uid`),
 INDEX `K_cron_task_4` (`status`),
 INDEX `K_cron_task_5` (`created`),
 INDEX `K_cron_task_6` (`exec_schedule_start`),
 INDEX `K_cron_task_7` (`exec_schedule_end`),
 INDEX `K_cron_task_8` (`exec_frequency`),
 INDEX `K_cron_task_9` (`is_actif`)
 ) ENGINE=MyIsam;
 <<*/

/** SQL_INSERT>>
 << */

/** SQL_ALTER>>
 << */

/** SQL_FKEY>>
 << */

/** SQL_TRIGGER>>
 << */

/** SQL_VIEW>>
 << */

/** SQL_DROP>>
 << */

/**
 * Job to execute in batch mode.
 */
class CronTaskDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'cron_task';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'cron_task';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = null;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'name' => 'name',
		'description' => 'description',
		'callback_class' => 'callbackClass',
		'callback_method' => 'callbackMethod',
		'callback_params' => 'callbackParams',
		'created' => 'created',
		'owner_id' => 'ownerId',
		'owner_uid' => 'ownerUid',
		'status' => 'status',
		'exec_frequency' => 'frequency',
		'exec_schedule_start' => 'scheduleStart',
		'exec_schedule_end' => 'scheduleEnd',
		'last_exec' => 'lastExec',
		'is_actif' => 'actif'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created' => 'datetime',
		'last_exec' => 'datetime',
		'is_actif' => 'boolean',
		'callback_params' => 'json'
	);

	/**
	 * read the first task which need to be run by cron
	 * 
	 * @return \PDOStatement
	 **/
	public function getNeedToRun()
	{
		$hour = date('H');
		$table = $this->_table;

		$sql = "SELECT * FROM $table AS cron";
		$sql .= " WHERE cron.is_actif = 1";
		$sql .= ' AND cron.name NOT IN (SELECT cron.name FROM batchjob WHERE batchjob.name LIKE CONCAT("cron.",cron.name,".%") AND batchjob.status="running")';
		$sql .= ' AND (';
		$sql .= " (exec_schedule_start < exec_schedule_end AND :currentHour >= exec_schedule_start AND :currentHour < exec_schedule_end)";
		$sql .= " OR (exec_schedule_start > exec_schedule_end AND (:currentHour >= exec_schedule_start OR :currentHour < exec_schedule_end))";
		$sql .= ')';
		$sql .= ' AND (last_exec is NULL OR DATE_ADD(`last_exec`,INTERVAL `exec_frequency` SECOND) <= NOW())';
		
		$bind = array(
			':runningStatus' => CronTask::STATE_RUNNING,
			':waitingStatus' => CronTask::STATE_WAITING,
			':currentHour' => $hour,
		);
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}
} /* End of class */
