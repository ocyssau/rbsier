<?php
namespace Rbs\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use Rbplm\People\User;

/**
 *
 *
 */
class PassallAdapter implements AdapterInterface
{

	/**
	 * Sets username and password for authentication
	 *
	 * @return void
	 */
	public function __construct($username = null, $password = null)
	{
		$this->identity = $username;
		$this->credential = $password;
	}

	/**
	 *
	 * @param string $string
	 */
	public function setIdentity($string)
	{
		$this->identity = $string;
		return $this;
	}

	/**
	 *
	 * @param string $string
	 */
	public function setCredential($string)
	{
		$this->credential = $string;
		return $this;
	}

	/**
	 * Performs an authentication attempt
	 *
	 * @return \Zend\Authentication\Result
	 * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
	 */
	public function authenticate()
	{
		$identity = array(
			'id' => User::SUPER_USER_ID,
			'uid' => User::SUPER_USER_UUID,
			'firstname' => 'ranchbe',
			'lastname' => 'ranchbe',
			'mail' => '',
			'login' => $this->identity,
			'identity' => $this->identity
		);

		$result = new Result(Result::SUCCESS, $identity);
		return $result;
	}
}
