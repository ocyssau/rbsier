<?php
namespace Rbs\Auth;

use Rbplm\Dao\Connexion;

class Factory
{

	/**
	 * @return \Zend\Authentication\Adapter\AdapterInterface
	 */
	public static function getAdapter($conf)
	{
		if ( $conf['adapter'] == 'ldap' ) {
			$ldapOptions = $conf['options'];
			$username = "";
			$password = "";
			$authAdapter = new Ldap($ldapOptions, $username, $password);
			$authAdapter->setUserMap($conf['usermap']);
			$authAdapter->setRoleMap($conf['rolemap']);
		}
		elseif ( $conf['adapter'] == 'passall' ) {
			$authAdapter = new PassallAdapter();
		}
		elseif ( $conf['adapter'] == 'db' ) {
			$authAdapter = new DbAdapter();
			$authAdapter->setConnexion(Connexion::get());
		}
		return $authAdapter;
	}
}
