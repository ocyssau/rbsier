<?php
namespace Rbs\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

/**
 *
 */
class DbAdapter implements AdapterInterface
{

	/**
	 * Sets username and password for authentication
	 *
	 * @return void
	 */
	public function __construct($username = null, $password = null)
	{
		$this->identity = $username;
		$this->credential = $password;
	}

	/**
	 *
	 * @param string $string
	 */
	public function setIdentity($string)
	{
		$this->identity = $string;
		return $this;
	}

	/**
	 *
	 * @param string $string
	 */
	public function setCredential($string)
	{
		$this->credential = $string;
		return $this;
	}

	/**
	 *
	 * @param
	 *        	Connexion
	 */
	public function setConnexion($conn)
	{
		$this->connexion = $conn;
		return $this;
	}

	/**
	 * Performs an authentication attempt
	 *
	 * @return \Zend\Authentication\Result
	 * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
	 */
	public function authenticate()
	{
		$sql = "SELECT u.id, u.uid, u.lastlogin,
			u.firstname, u.lastname,
			u.mail, u.login, u.is_active,
			p.lang, p.css_sheet, p.rbgateserver_url
			FROM acl_user u
			LEFT OUTER JOIN user_prefs p ON u.id=p.owner_id
			WHERE login=:login AND password=:password AND auth_from='db'";
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);

		$credential = md5($this->credential);

		$stmt->execute(array(
			':login' => $this->identity,
			':password' => $credential
		));
		$row = $stmt->fetch();

		if ( $row ) {
			$identity = array(
				'id' => $row['id'],
				'uid' => $row['uid'],
				'lastLogin' => $row['lastlogin'],
				'firstname' => $row['firstname'],
				'lastname' => $row['lastname'],
				'mail' => $row['mail'],
				'login' => $row['login'],
				'isActive' => $row['is_active'],
				'authFrom' => 'db',
				'loaded' => true,
				'preferences' => array(
					'lang'=>$row['lang'],
					'cssSheet'=>$row['css_sheet'],
					'rbconverterUrl'=>$row['rbgateserver_url']
				)
			);
			$result = new Result(Result::SUCCESS, $identity, array('Authentication from db successfull'));
		}
		else {
			$identity = array();
			$result = new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $identity, array('Bad password or user dont existing'));
		}
		return $result;
	}
}
