<?php
//%LICENCE_HEADER%
namespace Rbs\People;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `partners` (
 `id` INT(11) NOT NULL,
 `number` VARCHAR(128) NOT NULL DEFAULT '',
 `type` enum('customer','supplier','staff') DEFAULT NULL,
 `first_name` VARCHAR(64) DEFAULT NULL,
 `last_name` VARCHAR(64) DEFAULT NULL,
 `adress` VARCHAR(64) DEFAULT NULL,
 `city` VARCHAR(64) DEFAULT NULL,
 `zip_code` int(11) DEFAULT NULL,
 `phone` VARCHAR(64) DEFAULT NULL,
 `cell_phone` VARCHAR(64) DEFAULT NULL,
 `mail` VARCHAR(64) DEFAULT NULL,
 `web_site` VARCHAR(64) DEFAULT NULL,
 `activity` VARCHAR(64) DEFAULT NULL,
 `company` VARCHAR(64) DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE (`number`),
 INDEX (`type`)
 ) ENGINE=InnoDB;
 
CREATE TABLE IF NOT EXISTS `partners_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
)ENGINE=MyIsam AUTO_INCREMENT=10;
INSERT INTO partners_seq(id) VALUES(10);
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class SomebodyDao extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table = 'partners';

	/**
	 * @var string
	 */
	public static $vtable = 'partners';

	/**
	 * @var string
	 */
	public static $sequenceName = 'partners_seq';

	/**
	 * @var string
	 */
	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'number' => 'uid',
		'first_name' => 'firstname',
		'last_name' => 'lastname',
		'adress' => 'adress',
		'mail' => 'mail',
		'city' => 'city',
		'zip_code' => 'zipcode',
		'web_site' => 'website',
		'phone' => 'phone',
		'cell_phone' => 'cellphone',
		'activity' => 'activity',
		'company' => 'company',
		'type' => 'type'
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array();

	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
	}

	/**
	 * Compose the partner number from the first and last name.
	 * return number, else return false.
	 *
	 * @param string $firstName
	 * @param string $lastName
	 */
	static function composeNumber($firstName, $lastName)
	{
		if ( !empty($firstName) && !empty($lastName) ) {
			$separator = '_';
		}
		return self::noAccent($firstName . $separator . $lastName);
	}

	/**
	 * Suppress accent of the input string.
	 * return the input without accents, else return false.
	 *
	 * @param string $in input string.
	 */
	static function noAccent($in)
	{
		//thank to 'http://www.wikistuce.info/doku.php/php/supprimer_tous_les_caracteres_speciaux_d-une_chaine'
		$search = array(
			'@[����]@',
			'@[���]@',
			'@[��]@',
			'@[���]@',
			'@[��]@',
			'@[����]@',
			'@[���]@',
			'@[��]@',
			'@[��]@',
			'@[��]@',
			'@[�]@i',
			'@[�]@i',
			'@[ ]@i',
			'@[^a-zA-Z0-9_]@'
		);
		$replace = array(
			'e',
			'a',
			'i',
			'u',
			'o',
			'E',
			'A',
			'I',
			'U',
			'O',
			'c',
			'C',
			'_',
			''
		);
		return preg_replace($search, $replace, $in);
	}
}
