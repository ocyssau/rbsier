<?php
//%LICENCE_HEADER%

namespace Rbs\People;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `acl_group` (
	`id` int(11) NOT NULL,
	`uid` VARCHAR(30) NOT NULL,
	`name` VARCHAR(128) DEFAULT NULL,
	`description` VARCHAR(64) DEFAULT NULL,
	`is_active` tinyint(1) DEFAULT '1',
	`owner_id` int(11) DEFAULT NULL,
	`owner_uid`  VARCHAR(64) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY (`uid`),
	UNIQUE KEY (`name`),
	KEY (`owner_id` ASC)
);
<<*/

/** SQL_INSERT>>
-- Builtin protected users id < 50
-- Builtin protected roles id < 50
-- Builtin others users id < 100 > 50
-- Builtin others roles id < 100 > 50
-- None conflicts of id withs users ids is permits
INSERT INTO `acl_group` (`id`,`name`,`uid`,`description`,`is_active`) VALUES (80,"concepteurs","concepteurs","Concepteurs CAO",1);
INSERT INTO `acl_group` (`id`,`name`,`uid`,`description`,`is_active`) VALUES (81,"groupLeader","groupleader","chef de groupe de conception",1);
INSERT INTO `acl_group` (`id`,`name`,`uid`,`description`,`is_active`) VALUES (82,"Leader","leader","Chargé d'affaire",1);
INSERT INTO `acl_group` (`id`,`name`,`uid`,`description`,`is_active`) VALUES (83,"checkers","checkers","verificateurs des projets CAO",1);
 <<*/

/** SQL_ALTER>>

ALTER TABLE `acl_group` 
ADD CONSTRAINT `FK_acl_group_acl_user1`
  FOREIGN KEY (`owner_id`)
  REFERENCES `acl_user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
<<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/**
 * @brief Dao class for \Rbplm\People\Group
 *
 * See the examples: Rbplm/People/GroupTest.php
 *
 * @see \Rbplm\Dao\Pg
 * @see \Rbplm\People\GroupTest
 *
 */
class GroupDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'acl_group';
	public static $sequenceName='acl_seq';
	public static $sequenceKey='id';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'name'=>'name',
		'description'=>'description',
		'is_active'=>'active',
	);

} /* End of class */
