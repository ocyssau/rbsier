<?php
//%LICENCE_HEADER%

namespace Rbs\People;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `acl_user` (
`id` int(11) NOT NULL,
`uid` VARCHAR(128) NOT NULL,
`login` VARCHAR(128) NOT NULL,
`lastname` VARCHAR(128) DEFAULT NULL,
`firstname` VARCHAR(128) DEFAULT NULL,
`password` VARCHAR(128) DEFAULT NULL,
`mail` VARCHAR(128) DEFAULT NULL,
`owner_id` int(11) DEFAULT NULL,
`owner_uid` VARCHAR(64) DEFAULT NULL,
`primary_role_id` int(11) DEFAULT NULL,
`lastlogin` DATETIME DEFAULT NULL,
`is_active` tinyint(1) DEFAULT '1',
`auth_from` VARCHAR(16) DEFAULT 'db',
`conn_from` VARCHAR(16) DEFAULT 'auto',
`extends` JSON,
PRIMARY KEY (`id`),
UNIQUE KEY `login_idx` (`login`),
UNIQUE KEY `uid_idx` (`uid`)
);
 <<*/

/** SQL_INSERT>>
-- Builtin protected users id < 50
-- Builtin protected roles id < 50
-- Builtin others users id < 100 > 50
-- Builtin others roles id < 100 > 50
-- None conflicts of id withs users ids is permits

INSERT INTO `acl_user`
	(`id`,`uid`,`login`,`lastname`,`firstname`,`password`,`mail`,`owner_id`,`owner_uid`,`primary_role_id`,`lastlogin`,`is_active`)
	VALUES
	(1,'admin','admin','admin','admin',md5('admin00'),'admin@ranchbe.fr',2,'admin',1,NULL,1),
	(5,'nobody','nobody','nobody','nobody',md5('nobody00'),'nobody@ranchbe.fr',20,'admin',1,NULL,1),
	(70,'user','user','user','user',md5('user00'),'user@ranchbe.fr',50,'admin',1,NULL,1);
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class UserDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='acl_user';
	public static $vtable='acl_user';

	/**
	 * @var string
	 */
	public static $sequenceName='acl_seq';
	public static $sequenceKey='id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'login'=>'login',
		'lastname'=>'lastname',
		'firstname'=>'firstname',
		'mail'=>'mail',
		'owner_id'=>'ownerId',
		'owner_uid'=>'ownerUid',
		'primary_role_id'=>'primaryRoleId',
		'lastlogin'=>'lastLogin',
		'is_active'=>'active',
		'auth_from'=>'authFrom',
		'conn_from'=>'connFrom',
		'extends'=>'extends',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'lastlogin'=>'datetime',
		'is_active'=>'boolean',
		'extends'=>'json',
	);

	/**
	 * 
	 * @param MappedInterface $mapped
	 * @param string $name
	 * @return \Rbplm\Dao\MappedInterface
	 */
	public function loadFromLogin(MappedInterface $mapped, $name)
	{
		$filter = 'obj.'.$this->toSys('login').'=:name';
		$bind = array(':name'=>$name);
		return $this->load($mapped, $filter, $bind );
	}
}
