<?php
//%LICENCE_HEADER%

namespace Rbs\People;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class PasswordDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='acl_user';
	public static $vtable='acl_user';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'password'=>'password',
	);
}
