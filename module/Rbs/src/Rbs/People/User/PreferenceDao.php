<?php
namespace Rbs\People\User;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `user_prefs` (
	`id` int(11) AUTO_INCREMENT,
	`owner_id` int(11) NOT NULL,
	`css_sheet` VARCHAR(32) default NULL,
	`lang` VARCHAR(32) default NULL,
	`long_date_format` VARCHAR(32) default NULL,
	`short_date_format` VARCHAR(32) default NULL,
	`hour_format` VARCHAR(32) default NULL,
	`time_zone` VARCHAR(32) default NULL,
	`rbgateserver_url` VARCHAR(256) default NULL,
	PRIMARY KEY (`id`),
	UNIQUE (`owner_id`)
) ENGINE=InnoDB;
<<*/

/** SQL_ALTER>>
ALTER TABLE `user_prefs`
	ADD CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`owner_id`) REFERENCES `acl_user` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
<<*/


/**
 * manage user preferences
 */
class PreferenceDao extends \Rbs\Dao\Sier
{
	/** @var string */
	public static $table = 'user_prefs';

	/** @var string */
	public static $vtable = 'user_prefs';

	/** @var string */
	public static $sequenceName = null;

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'owner_id' => 'ownerId',
		'css_sheet' => 'cssSheet',
		'lang' => 'lang',
		'long_date_format' => 'longDateFormat',
		'short_date_format' => 'shortDateFormat',
		'hour_format' => 'hourFormat',
		'time_zone' => 'timeZone',
		'rbgateserver_url' => 'rbgateServerUrl',
	);

	/**
	 * Load the preferences from the owner
	 *
	 * @param
	 *        	$mapped
	 * @param
	 *        	$ownerUid
	 * @return Preference
	 */
	public function loadFromOwnerId($mapped, $ownerId)
	{
		$filter = 'obj.owner_id=:owner';
		$bind = array(
			':owner' => $ownerId
		);
		return $this->load($mapped, $filter, $bind);
	}
} /* End of class */
