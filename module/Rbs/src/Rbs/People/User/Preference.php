<?php
// %LICENCE_HEADER%
namespace Rbs\People\User;

use Rbplm\Any;
use Rbplm\Owned;
use Rbplm\Mapped;
use Rbplm\Dao\MappedInterface;
use Rbplm\People;

/**
 * @brief Manage user Preferences.
 *
 * Example and tests: Rbplm/People/UserTest.php
 */
class Preference extends Any implements MappedInterface
{
	use Owned;
	use Mapped;

	/**
	 *
	 * @var integer
	 */
	static $classId = '569b63eb7cce4';

	/**
	 * Array of default Preferences.
	 *
	 * @var array
	 */
	protected $defaults = array();

	/**
	 * Array result Preferences to apply
	 *
	 * @var array
	 */
	protected $preferences = array();

	/**
	 * True to get prefs from user profile, else set always to default values.
	 *
	 * @var boolean
	 */
	protected $isEnable = true;

	/**
	 * Multi singleton registry
	 *
	 * @var array
	 */
	protected static $instances;

	/**
	 *
	 * @param People\User $user
	 */
	public function __construct(People\User $user)
	{
		parent::__construct();
		$this->setOwner($user);
		self::$instances[$user->getId()] = $this;
	}

	/**
	 * Get the preferences of the the user
	 *
	 * @param \Rbplm\People\User $user
	 * @return Preference
	 */
	public static function get(People\User $user)
	{
		$id = $user->getId();
		if ( !isset(self::$instances[$id]) ) {
			new self($user);
		}
		return self::$instances[$id];
	}

	/**
	 * Set the default value
	 *
	 * @param array $config
	 * @return Preference
	 */
	public function setDefault($defaults)
	{
		$this->defaults = $defaults;
		return $this;
	}

	/**
	 * Magic method.
	 * Set the preference value from his name.
	 *
	 * @param
	 *        	string
	 * @param
	 *        	mixed
	 * @return void
	 */
	public function __set($name, $value)
	{
		$this->preferences[$name] = $value;
	}

	/**
	 * Magic method.
	 * Get the preference value from his name.
	 *
	 * @param
	 *        	string
	 * @return string
	 */
	public function __get($name)
	{
		if ( !$this->isEnable ) {
			return $this->defaults[$name];
		}
		else {
			return $this->preferences[$name];
		}
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $config
	 *        	\PDO fetch result to load
	 * @return Any
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['ownerId'])) ? $this->ownerId = $properties['ownerId'] : null;
		(isset($properties['cssSheet']) && !is_null($properties['cssSheet'])) ? $this->preferences['cssSheet'] = $properties['cssSheet'] : null;
		(isset($properties['lang']) && !is_null($properties['lang'])) ? $this->preferences['lang'] = $properties['lang'] : null;
		(isset($properties['longDateFormat']) && !is_null($properties['longDateFormat'])) ? $this->preferences['longDateFormat'] = $properties['longDateFormat'] : null;
		(isset($properties['shortDateFormat']) && !is_null($properties['shortDateFormat'])) ? $this->preferences['shortDateFormat'] = $properties['shortDateFormat'] : null;
		(isset($properties['dateInputMethod']) && !is_null($properties['dateInputMethod'])) ? $this->preferences['dateInputMethod'] = $properties['dateInputMethod'] : null;
		(isset($properties['hourFormat']) && !is_null($properties['hourFormat'])) ? $this->preferences['hourFormat'] = $properties['hourFormat'] : null;
		(isset($properties['timeZone']) && !is_null($properties['timeZone'])) ? $this->preferences['timeZone'] = $properties['timeZone'] : null;
		(isset($properties['maxRecord']) && !is_null($properties['maxRecord'])) ? $this->preferences['maxRecord'] = $properties['maxRecord'] : null;
		(isset($properties['rbgateServerUrl']) && !is_null($properties['rbgateServerUrl'])) ? $this->preferences['rbgateServerUrl'] = $properties['rbgateServerUrl'] : null;
		return $this;
	}

	/**
	 * Alias for __serialize; implement arrayObject interface
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		$return = array_merge($this->getPreferences(), array(
			'id' => $this->id,
			'uid' => $this->uid,
			'cid' => $this->cid,
			'name' => $this->name,
			'ownerId' => $this->ownerId
		));
		return $return;
	}

	/**
	 * Enable the load of the user preferences.
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isEnable($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->isEnable = $bool;
		}
		else {
			return $this->isEnable;
		}
	}

	/**
	 * Get all Preferences of current user.
	 *
	 * @return array
	 */
	public function getPreferences()
	{
		if ( !$this->isEnable ) {
			return $this->defaults;
		}
		else {
			return array_merge($this->defaults, $this->preferences);
		}
	}

	/**
	 */
	public function setPreference($name, $value)
	{
		$this->preferences[$name] = $value;
		return $this;
	}

	/**
	 */
	public function setPreferences(array $prefs)
	{
		$this->preferences = $prefs;
		return $this;
	}
} /* End of class */
