<?php
// %LICENCE_HEADER%
namespace Rbs\People\User;

/**
 * Only for define sql schema.
 * Required by Sql Extractor command.
 */

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `user_context` (
 `user_id` int(11) NOT NULL,
 `datas` LONGTEXT,
 PRIMARY KEY (`user_id`)
 ) ENGINE=MyIsam;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP IF EXISTS TABLE `user_sessions`;
 <<*/

/**
 * 
 *
 */
class ContextDao extends \Rbs\Dao\Sier
{

	/**
	 * @var string
	 */
	public static $table = 'user_context';

	/** @var string */
	public static $vtable = 'user_context';

	/** @var string */
	public static $sequenceName = false;

	/**
	 * @var array
	 */
	public static $sysToApp = [
		'user_id' => 'userId',
		'datas' => 'datas'
	];

	/**
	 * @var array
	 */
	public static $sysToAppFilter = [
		'datas' => 'json'
	];

	/**
	 * 
	 * @param Context $mapped
	 * @param int $userId
	 * @return Context
	 */
	public function loadFromUserId($mapped, $userId)
	{
		$connexion = $this->getConnexion();

		if ( !$userId ) {
			throw new \Exception('userId is not set');
		}
		$table = $this->_table;
		$sql = 'SELECT datas FROM ' . $table . ' WHERE user_id =' . $userId;
		$stmt = $connexion->query($sql);
		$dataStr = $stmt->fetchColumn(0);

		if ( !$dataStr ) {
			#throw new \Rbplm\Dao\NotExistingException('Session datas is not found');
		}
		else {
			$datas = $this->getTranslator()->jsonToApp($dataStr);
			$mapped->setDatas($datas);
		}

		$mapped->setUserId($userId);
		return $mapped;
	}

	/**
	 */
	public function save(\Rbplm\Dao\MappedInterface $context)
	{
		$sysToApp = $this->metaModel;
		$table = $this->_table;
		
		foreach( $sysToApp as $asSys => $asApp ) {
			$sysSet[] = '`' . $asSys . '`';
			$appSet[] = ':' . $asApp;
			$uset[] = '`' . $asSys . '`' . '=:' . $asApp;
		}

		$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
		$this->insertStmt = $this->connexion->prepare($sql);

		$sql = "UPDATE $table SET " . implode(',', $uset) . ' WHERE user_id=:userId;';
		$this->updateStmt = $this->connexion->prepare($sql);

		$bind = $this->_bind($context);
		
		try {
			#var_dump($this->insertStmt->queryString, $bind);die;
			$this->insertStmt->execute($bind);
		}
		catch( \PDOException $e ) {
			if(strstr($e->getMessage(), '1062 Duplicate entry')){
				$this->updateStmt->execute($bind);
			}
			else{
				throw $e;
			}
		}
		
		$context->isLoaded(true);
		$context->dao = $this;
		return $context;
	}
}
