<?php
// %LICENCE_HEADER%
namespace Rbs\People\User;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\Dao\MappedInterface;

/**
 * this class is used to store all properties defined by project and container like :
 * containers links
 * doctypes
 * current space
 * current container
 * current project
 */
class Context extends \Rbplm\Any implements MappedInterface
{
	use \Rbplm\Mapped;

	/**
	 * 
	 * @var string
	 */
	public static $classId = 'usercontext';

	/**
	 * 
	 * @var string
	 */
	const SESSION_KEY = 'context';

	/**
	 *
	 * @var \Zend\Session\Storage\StorageInterface
	 */
	private $sessionStorage;

	/**
	 *
	 * @var int
	 */
	protected $userId;

	/**
	 * 
	 * @var array
	 */
	protected $datas = [];

	/**
	 * 
	 */
	public function __construct()
	{
		$this->cid = self::$classId;
		$this->datas = [
			'spacename' => null,
			'containerId' => null,
			'containerNumber' => null,
			'containerName' => null,
			'containerUid' => null,
			'projectId' => null,
			'projectUid' => null,
			'projectNumber' => null,
			'projectName' => null,
			'projectInfos' => null,
			'isLoaded' => false
		];
	}
	
	/**
	 * Magic method
	 * Getter for properties
	 *
	 * @param string $name
	 */
	public function __get($name)
	{
		return $this->getData($name);
	}
	
	/**
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['userId'])) ? $this->setUserId($properties['userId']) : null;
		(isset($properties['datas'])) ? $this->setDatas($properties['datas']) : null;
		return $this;
	}

	/**
	 * 
	 * @param \Zend\Session\Storage\StorageInterface $sessionStorage
	 */
	public function setSessionStorage(\Zend\Session\Storage\StorageInterface $sessionStorage)
	{
		$this->sessionStorage = $sessionStorage;
	}

	/**
	 *
	 * @param ContextDao $dao
	 * @return Context
	 */
	public function setDao(ContextDao $dao)
	{
		$this->dao = $dao;
		return $this;
	}

	/**
	 * @return ContextDao
	 */
	public function getDao()
	{
		return $this->dao;
	}

	/**
	 *
	 * @param int $userId
	 * @return Context
	 */
	public function setUserId($userId)
	{
		$this->userId = $userId;
		return $this;
	}

	/**
	 *
	 * @return int
	 */
	public function getUserId()
	{
		return $this->userId;
	}

	/**
	 *
	 * @param array $datas
	 * @return Context
	 */
	public function setDatas(array $datas)
	{
		$this->datas = $datas;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return Context
	 */
	public function setData($name, $value)
	{
		$this->datas[$name] = $value;
		return $this;
	}

	/**
	 *
	 * @return mixed
	 */
	public function getData($name)
	{
		if ( isset($this->datas[$name]) ) {
			return $this->datas[$name];
		}
		else {
			return null;
		}
	}

	/**
	 * 
	 * @return \Rbs\People\User\Context
	 */
	public function loadFromSession()
	{
		if ( $this->sessionStorage->offsetExists(self::SESSION_KEY) ) {
			$this->setDatas($this->sessionStorage->offsetGet(self::SESSION_KEY));
		}
		return $this;
	}

	/**
	 *
	 * @return \Rbs\People\User\Context
	 */
	public function saveInSession()
	{
		$this->sessionStorage->offsetSet(self::SESSION_KEY, $this->getDatas());
		return $this;
	}

	/**
	 * Record selected container id/number/type and eventualy his project id/number in session.<br>
	 *
	 * Create var in session :<br>
	 * SelectedContainer : id of the current container<br>
	 * SelectedContainerNum : Number of the current container<br>
	 * SelectedContainerType : Type of the current container(workitem, bookshop, cadlib, mockup)<br>
	 * SelectedProject : id of the father project if current container is a workitem<br>
	 * SelectedProjectNum : number of the father project if current container is a workitem<br>
	 * projectInfos : Infos about current project(only if selected container is type workitem)<br>
	 *
	 * @param integer $containerId        	
	 * @param string $spacename        	
	 * @return \Rbplm\Org\Workitem
	 */
	public function activate($containerId, $spacename)
	{
		$factory = DaoFactory::get($spacename);
		$container = $factory->getModel(Workitem::$classId);
		$container->dao = $factory->getDao(Workitem::$classId);
		$container->dao->loadFromId($container, $containerId);
		$projDao = $factory->getDao(Project::$classId);

		if ( $spacename != $this->getData('spacename') || $containerId != $this->getData('containerId') ) {
			/* Reinit filter record in session */
			$this->sessionStorage->offsetSet(\Application\Form\AbstractFilterForm::SESSION_KEY, []);

			$projectId = $container->getParent(true);
			if ( $projectId ) {
				$project = new Project();
				$projDao->loadFromId($project, $projectId);
				$container->setParent($project);
				$this->setData('projectId', $project->getId());
				$this->setData('projectUid', $project->getUid());
				$this->setData('projectNumber', $project->getNumber());
				$this->setData('projectName', $project->getName());
				$this->setData('projectInfos', $project->getArrayCopy());
			}

			$this->setData('containerId', $containerId);
			$this->setData('containerNumber', $container->getNumber());
			$this->setData('containerName', $container->getName());
			$this->setData('containerUid', $container->getUid());
			$this->setData('spacename', $spacename);
			$this->saveInSession();
			$this->isSaved(false);
		}
		
		return $container;
	}

	/**
	 * Clean all content of session
	 *
	 * @return Context
	 */
	public function clean()
	{
		$this->setDatas([
			'spacename' => '',
			'containerId' => '',
			'containerNumber' => '',
			'containerName' => '',
			'containerUid' => '',
			'projectId' => '',
			'projectUid' => '',
			'projectNumber' => '',
			'projectName' => '',
			'projectInfos' => '',
			'isLoaded' => true
		]);
		$this->saveInSession();
		return $this;
	}
}
