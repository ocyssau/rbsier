<?php
//%LICENCE_HEADER%

namespace Rbs\People;

use Rbplm\AnyPermanent;

/**
 * @brief user definition.
 *
 * Example and tests: Rbplm/People/UserTest.php
 *
 */
class Somebody extends AnyPermanent
{
	/**
	 *
	 * @var string
	 */
	static $classId = 'somebody5lwg7';

	/**
	 * @Annotation\Type("Zend\Form\Element\Text")
	 * @Annotation\Required({"required":"true" })
	 * @Annotation\Filter({"name":"StripTags"})
	 * @Annotation\Options({"label":"Username:"})
	 *
	 * @var string
	 */
	protected $firstname = null;

	/**
	 * @var
	 */
	protected $lastname = null;

	/**
	 * @var
	 */
	protected $mail = null;

	/**
	 * @param array			$properties
	 * @param \Rbplm\Any $parent
	 * @return void
	 */
	public function __construct(array $properties = null, $parent = null)
	{
		parent::__construct( $properties, $parent );
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Somebody
	 */
	public function hydrate( array $properties )
	{
		/* Any */
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['cid'])) ? $this->cid=$properties['cid'] : null;
		/* People */
		(isset($properties['firstname'])) ? $this->firstname=$properties['firstname'] : null;
		(isset($properties['lastname'])) ? $this->lastname=$properties['lastname'] : null;
		(isset($properties['adress'])) ? $this->adress=$properties['adress'] : null;
		(isset($properties['city'])) ? $this->city=$properties['city'] : null;
		(isset($properties['zipcode'])) ? $this->zipcode=$properties['zipcode'] : null;
		(isset($properties['phone'])) ? $this->phone=$properties['phone'] : null;
		(isset($properties['cellphone'])) ? $this->cellphone=$properties['cellphone'] : null;
		(isset($properties['mail'])) ? $this->mail=$properties['mail'] : null;
		(isset($properties['website'])) ? $this->website=$properties['website'] : null;
		(isset($properties['activity'])) ? $this->activity=$properties['activity'] : null;
		(isset($properties['company'])) ? $this->company=$properties['company'] : null;
		(isset($properties['type'])) ? $this->type=$properties['type'] : null;
		return $this;
	}

} /* End of class */
