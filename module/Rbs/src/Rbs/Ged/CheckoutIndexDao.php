<?php
namespace Rbs\Ged;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\NotExistingException;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Document;
use Rbplm\Event;

/** SQL_SCRIPT>>

 CREATE TABLE IF NOT EXISTS `checkout_index` (
 `file_name` VARCHAR(128) NOT NULL,
 `lock_by_id` int(11) NULL,
 `lock_by_uid` VARCHAR(64) NULL,
 `file_id` int(11) NULL,
 `file_uid` VARCHAR(140) NULL,
 `designation` VARCHAR(128) NULL,
 `spacename` VARCHAR(128) NULL,
 `container_id` int(11) NULL,
 `container_number` VARCHAR(128) NULL,
 `document_id` int(11) NULL,
 PRIMARY KEY (`file_name`),
 KEY `IK_checkout_index_1` (`lock_by_id`),
 KEY `IK_checkout_index_2` (`lock_by_uid`),
 KEY `IK_checkout_index_3` (`file_id`),
 KEY `IK_checkout_index_4` (`file_uid`),
 KEY `IK_checkout_index_5` (`spacename`),
 KEY `IK_checkout_index_6` (`container_id`),
 KEY `IK_checkout_index_7` (`container_number`),
 KEY `IK_checkout_index_8` (`document_id`)
 ) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
<<*/

/** SQL_TRIGGER>>
 DROP PROCEDURE IF EXISTS `updateCheckoutIndexFromUserId`;
 DELIMITER $$
 CREATE PROCEDURE `updateCheckoutIndexFromUserId`(_userId INT(11))
 BEGIN
	 DECLARE userId INT(11);
	 SET @userId=_userId;
	 DELETE FROM `checkout_index` WHERE `lock_by_id`=@userId;
	 INSERT INTO checkout_index(
	 SELECT * FROM(
	 SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
	 FROM workitem_doc_files as file
	 JOIN workitem_documents as doc ON file.document_id=doc.id
	 where file.acode=1 and file.lock_by_id=@userId
	 UNION(
	 SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
	 FROM bookshop_doc_files as file
	 JOIN bookshop_documents as doc ON file.document_id=doc.id
	 where file.acode=1 and file.lock_by_id=@userId
	 )
	 UNION(
	 SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
	 FROM cadlib_doc_files as file
	 JOIN cadlib_documents as doc ON file.document_id=doc.id
	 where file.acode=1 and file.lock_by_id=@userId
	 )
	 UNION(
	 SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
	 FROM mockup_doc_files as file
	 JOIN mockup_documents as doc ON file.document_id=doc.id
	 where file.acode=1 and file.lock_by_id=@userId
	 )
	 ) as checkout_index
	 );
 END $$
 DELIMITER ;
 
 -- CALL updateCheckoutIndexFromUserId(13);


DROP PROCEDURE IF EXISTS `updateCheckoutIndex`;
DELIMITER $$
CREATE PROCEDURE `updateCheckoutIndex`()
BEGIN
	DELETE FROM `checkout_index`;
	INSERT INTO checkout_index(
		SELECT * FROM(
			SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
			FROM workitem_doc_files as file
			JOIN workitem_documents as doc ON file.document_id=doc.id
			WHERE file.acode=1
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM bookshop_doc_files as file
				JOIN bookshop_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM cadlib_doc_files as file
				JOIN cadlib_documents as doc ON file.document_id=doc.id
				WHERE file.acode=1
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM mockup_doc_files as file
				JOIN mockup_documents as doc ON file.document_id=doc.id
				WHERE file.acode=1
			)
		) as checkout_index
	);
END $$
DELIMITER ;



<< */

/** SQL_VIEW>>
<<*/

/**  SQL_DROP>>
 DROP TABLE `checkout_index`;
 <<*/

/**
 * This class is used to retrieve informations about checkout document from the file in the wildspace.
 */
class CheckoutIndexDao extends DaoSier
{

	/** @var \PDOStatement */
	protected $getFromFileNameStmt;

	/** @var \PDOStatement */
	protected $addStmt;

	/**
	 *
	 * @var string
	 */
	public static $table = 'checkout_index';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'file_name' => 'name',
		'file_id' => 'id',
		'file_uid' => 'uid',
		'designation' => 'designation',
		'spacename' => 'spacename',
		'container_id' => 'containerId',
		'container_number' => 'containerNumber',
		'lock_by_id' => 'lockById',
		'lock_by_uid' => 'lockByUid',
		'document_id' => 'parentId'
	);

	/**
	 * Create entry in index table for retrieve container of a checkout file.
	 *
	 * @param Docfile\Version $docfile
	 * @return CheckoutIndexDao
	 */
	public function add(Docfile\Version $docfile)
	{
		($this->connexion->inTransaction() == true) ? $withTrans = false : $withTrans = true;
		if ( $withTrans ) $this->connexion->beginTransaction();
		try {
			if ( !$this->addStmt ) {
				$sysToApp = $this->metaModel;
				$table = $this->_table;

				foreach( $sysToApp as $asSys => $asApp ) {
					$sysSet[] = '`' . $asSys . '`';
					$appSet[] = ':' . $asApp;
				}

				$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
				$this->addStmt = $this->connexion->prepare($sql);
			}

			$lockBy = $docfile->getLockBy();
			if ( $lockBy ) {
				$bind = array(
					':name' => $docfile->getName(),
					':id' => $docfile->getId(),
					':uid' => $docfile->getUid(),
					':designation' => $docfile->getParent()->designation,
					':spacename' => '',
					':containerId' => $docfile->getParent()
						->getParent()
						->getId(),
					':containerNumber' => $docfile->getParent()
						->getParent()
						->getNumber(),
					':lockById' => $lockBy->getId(),
					':parentId' => $docfile->getParent(true)
				);
				$this->addStmt->execute($bind);
			}
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 * Suppress entry in checkout index table.
	 *
	 * @param string $fileName
	 * @param string $spacename
	 * @return CheckoutIndexDao
	 */
	public function deleteFromFileName($fileName, $spacename)
	{
		$toSysName = $this->toSys('name');
		$toSysSpacename = $this->toSys('spacename');
		$filter = "$toSysName=:name AND $toSysSpacename=:spacename";
		$bind = array(
			':name' => $fileName,
			':spacename' => $spacename
		);
		$this->_deleteFromFilter($filter, $bind, false, true);
		return $this;
	}

	/**
	 * Get infos on stored document linked to "$file".
	 *
	 * @param string $fileName
	 * @return array
	 */
	public function getFromFileName($fileName)
	{
		if ( !$this->getFromFileNameStmt ) {
			$table = $this->_table;
			foreach( $this::$sysToApp as $asSys => $asApp ) {
				$select[] = $asSys . ' as ' . $asApp;
			}
			$select = implode(',', $select);
			$sql = "SELECT $select FROM $table WHERE file_name=:fileName";
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->getFromFileNameStmt = $stmt;
		}

		$bind = array(
			':fileName' => $fileName
		);
		$this->getFromFileNameStmt->execute($bind);
		$row = $this->getFromFileNameStmt->fetch();
		if ( $row ) {
			return $row;
		}
		else {
			throw new NotExistingException(sprintf('CAN_NOT_BE_LOAD_FROM_%s', $this->getFromFileNameStmt->queryString));
		}
	}

	/**
	 * @param Event $event
	 * @param Document\Version $document
	 * @param string $actionName
	 * @return CheckoutIndexDao
	 */
	public function onEvent(Event $event, Document\Version $document, $actionName = null)
	{
		if ( !$actionName ) {
			$explode = explode('.', $event->name);
			if ( $explode[1] == 'pre' ) {
				return;
			}
			$actionName = $explode[0];
		}

		switch ($event->name) {
			case ($document::SIGNAL_POST_CHECKIN):
				foreach( $document->getDocfiles() as $docfile ) {
					$this->add($docfile);
				}
				break;
			case ($document::SIGNAL_POST_CHECKOUT):
				foreach( $document->getDocfiles() as $docfile ) {
					$this->deleteFromFileName($docfile->getName(), $document->spacename);
				}
				break;
		}
		return $this;
	}
} /* End of class */
