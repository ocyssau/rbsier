<?php
// %LICENCE_HEADER%
namespace Rbs\Ged;

use Rbs\Dao\Sier as DaoSier;

/**
 * SQL_SCRIPT>>
 * <<
 */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/** SQL_TRIGGER>>

DROP PROCEDURE IF EXISTS `updateDocfileDocumentUidRelation`;
DELIMITER $$
CREATE PROCEDURE `updateDocfileDocumentUidRelation`()
BEGIN

#### WORKITEM
DROP TEMPORARY TABLE IF EXISTS tmptableworkitem;
CREATE TEMPORARY TABLE tmptableworkitem AS (
	SELECT df.* FROM workitem_doc_files as df
	LEFT OUTER JOIN workitem_documents as doc ON df.document_uid=doc.uid
	WHERE doc.id IS NULL
);
DROP TABLE IF EXISTS workitem_doc_files_backup;
CREATE TABLE workitem_doc_files_backup AS(
	SELECT * FROM workitem_doc_files
);
UPDATE workitem_doc_files as df 
SET document_uid=(SELECT uid FROM workitem_documents WHERE id=df.document_id)
WHERE df.id IN (SELECT id FROM tmptableworkitem);

#### BOOKSHOP
DROP TEMPORARY TABLE IF EXISTS tmptablebookshop;
CREATE TEMPORARY TABLE tmptablebookshop AS (
	SELECT df.* FROM bookshop_doc_files as df
	LEFT OUTER JOIN bookshop_documents as doc ON df.document_uid=doc.uid
	WHERE doc.id IS NULL
);
DROP TABLE IF EXISTS bookshop_doc_files_backup;
CREATE TABLE bookshop_doc_files_backup AS(
	SELECT * FROM bookshop_doc_files
);
UPDATE bookshop_doc_files as df 
SET document_uid=(SELECT uid FROM bookshop_documents WHERE id=df.document_id)
WHERE df.id IN (SELECT id FROM tmptablebookshop);

END $$
DELIMITER ;
<< */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */

/**
 * @brief Dao class for \Rbplm\Ged\Docfile
 *
 * See the examples: Rbplm/Ged/DocfileTest.php
 *
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Ged\DocfileTest
 *
 */
class DocfileDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = '';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array();

	/**
	 * Getter for versions.
	 * Return a list.
	 *
	 * @param \Rbplm\Dao\MappedInterface $mapped
	 * @return \Rbs\Dao\Sier\DaoList
	 */
	public function getVersions($mapped)
	{}
}

