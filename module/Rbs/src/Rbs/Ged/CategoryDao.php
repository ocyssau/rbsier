<?php
//%LICENCE_HEADER%
namespace Rbs\Ged;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `categories` (
 `id` int(11) NOT NULL,
 `uid` VARCHAR(32) NOT NULL,
 `cid` CHAR(13) NOT NULL DEFAULT '569e918a134ca',
 `dn` VARCHAR(128) NULL,
 `name` VARCHAR(512) DEFAULT NULL,
 `nodelabel` VARCHAR(64) DEFAULT NULL,
 `designation` text,
 `parent_id` int(11) NULL,
 `parent_uid` VARCHAR(32) NULL,
 `icon` VARCHAR(32) DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `U_categories_uid` (`uid`),
 UNIQUE KEY `U_categories_dn` (`dn`),
 KEY `I_categories_nodelabel` (`nodelabel`),
 KEY `I_categories_name` (`name`),
 KEY `I_categories_parent_id` (`parent_id`),
 KEY `I_categories_parent_uid` (`parent_uid`)
 ) ENGINE=InnoDB;

 CREATE TABLE IF NOT EXISTS `categories_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
 INSERT INTO `categories_seq` (`id`) VALUES(100);
 <<*/

/** SQL_INSERT>>
-- UPDATE `categories` SET name=CONCAT('/', nodelabel, '/');
-- UPDATE `categories` SET dn=CONCAT('/', id, '/');
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>

 -- CATEGORIES INSERT
 DROP TRIGGER IF EXISTS onCategoriesInsert;
 delimiter $$
 CREATE TRIGGER onCategoriesInsert BEFORE INSERT ON categories FOR EACH ROW 
 BEGIN
 DECLARE parentDn VARCHAR(128);
 DECLARE parentName VARCHAR(128);

 IF(NEW.parent_id IS NULL) THEN
 SET NEW.dn = CONCAT('/',NEW.id,'/');
 SET NEW.name = CONCAT('/',NEW.nodelabel,'/');
 ELSE
 SELECT dn, name INTO parentDn, parentName from categories where id=NEW.parent_id;
 SET NEW.dn = CONCAT(parentDn, NEW.id,'/');
 SET NEW.name = CONCAT(parentName, NEW.nodelabel,'/');
 END IF;

 END;$$
 delimiter ;

 -- CATEGORIES UPDATE
 DROP TRIGGER IF EXISTS onCategoriesUpdate;
 delimiter $$
 CREATE TRIGGER onCategoriesUpdate BEFORE UPDATE ON categories FOR EACH ROW 
 BEGIN
 DECLARE parentDn VARCHAR(128);
 DECLARE parentName VARCHAR(128);

 IF(NEW.parent_id IS NULL) THEN
 SET NEW.dn = CONCAT('/',NEW.id,'/');
 SET NEW.name = CONCAT('/',NEW.nodelabel,'/');
 ELSEIF(IFNULL(NEW.parent_id=OLD.parent_id, TRUE) OR IFNULL(NEW.nodelabel=OLD.nodelabel, TRUE) OR NEW.parent_id != OLD.parent_id OR NEW.nodelabel != OLD.nodelabel) THEN
 -- get parent dn
 SELECT dn, name INTO parentDn, parentName from categories where id=NEW.parent_id;
 SET NEW.dn = CONCAT(parentDn, NEW.id,'/');
 SET NEW.name = CONCAT(parentName, NEW.nodelabel,'/');
 END IF;
 END;$$
 delimiter ;

 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE IF EXISTS `categories_seq`;
 DROP TABLE IF EXISTS `categories`;
 <<*/

/**
 *
 */
class CategoryDao extends DaoSier
{

	/** @var string */
	public static $table = 'categories';

	/** @var string */
	public static $vtable = 'categories';

	/** @var string */
	public static $sequenceName = 'categories_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = [
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'dn' => 'cn',
		'name' => 'name',
		'nodelabel' => 'nodelabel',
		'designation' => 'description',
		'parent_id' => 'parentId',
		'parent_uid' => 'parentUid',
		'icon' => 'icon'
	];

	/**
	 * Get categories used by documents in container
	 * In select you may use alias doc for documents table and cat for categories table.
	 *
	 * @param int $containerId
	 * @param string $spacename
	 * @param array $select
	 * @param string $filter
	 * @param array $bind
	 * @return \PDOStatement
	 */
	public function getUsedByContainer($containerId, $spacename, $select = null, $filter = null, $bind = [])
	{
		$documentTable = $this->factory->getTable(\Rbplm\Ged\Document\Version::$classId);
		$table = $this->_table;
		if ( is_array($select) ) {
			$selectStr = implode(',', $select);
		}
		else {
			$selectStr = 'cat.*';
		}

		if ( !$filter ) {
			$filter = 'doc.container_id=:containerId ORDER BY cat.name ASC';
		}
		else {
			$filter = 'doc.container_id=:containerId AND ' . $filter;
		}

		$bind[':containerId'] = $containerId;

		$sql = "SELECT DISTINCT $selectStr FROM $documentTable AS doc JOIN $table AS cat ON doc.category_id=cat.id WHERE $filter";
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}
}
