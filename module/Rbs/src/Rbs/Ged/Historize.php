<?php
//%LICENCE_HEADER%

namespace Rbs\Ged;

use Rbs\Vault\Vault;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\AccessCode;

/**
 * @brief Helper class for create history copy of ged components.
 *
 *
 */
class Historize
{

	/**
	 *
	 * @param Vault $vault
	 * @param DaoFactory $factory
	 */
	public function __construct($vault, DaoFactory $factory)
	{
		$this->vault = $vault;
		$this->factory = $factory;
	}

	/**
	 * Create a new docfile as a history copy of $headDocfile.
	 * Return new docfile.
	 *
	 * @param DocfileVersion $headDocfile
	 */
	public function depriveDocfileToIteration(DocfileVersion $headDocfile)
	{
		$docfile = clone($headDocfile);
		$docfile->newUid();
		$docfile->lock(AccessCode::ITERATION);
		$docfile->setLifeStage('ITERATION');
		$docfile->setOfDocfile($headDocfile);

		$record = clone($docfile->getData());
		/* copy file to __iterations reposit and update links of $record */
		$this->vault->depriveToIteration($record, $docfile->iteration);
		$docfile->setData($record);

		return $docfile;
	}

} /* End of class */

