<?php
// %LICENCE_HEADER%
namespace Rbs\Ged\Document;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 */
abstract class VersionDao extends DaoSier
{

	/** @var \PDOStatement */
	protected $getlastversionStmt;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'number' => 'number',
		'name' => 'name',
		'label' => 'label',
		'life_stage' => 'lifeStage',
		'acode' => 'accessCode',
		'iteration' => 'iteration',
		'version' => 'version',
		'branch_id' => 'branchId',
		'container_id' => 'parentId',
		'container_uid' => 'parentUid',
		'doctype_id' => 'doctypeId',
		'category_id' => 'categoryId',
		'tags' => 'tags',
		'designation' => 'description',
		'from_document_id' => 'fromId',
		'lock_by_id' => 'lockById',
		'lock_by_uid' => 'lockByUid',
		'locked' => 'locked',
		'updated' => 'updated',
		'update_by_id' => 'updateById',
		'update_by_uid' => 'updateByUid',
		'created' => 'created',
		'create_by_id' => 'createById',
		'create_by_uid' => 'createByUid',
		'spacename' => 'spacename',
		'as_template' => 'asTemplate'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created' => 'datetime',
		'locked' => 'datetime',
		'updated' => 'datetime',
		'as_template' => 'boolean'
	);

	/**
	 * @param \Rbplm\Ged\Document\Version $mapped
	 */
	public function save(MappedInterface $mapped)
	{
		$parent = $mapped->getParent();
		if ( $parent ) {
			$mapped->parentId = $parent->id;
			$mapped->parentUid = $parent->uid;
		}

		$updateBy = $mapped->getUpdateBy();
		if ( $updateBy ) {
			$mapped->updateById = $updateBy->id;
			$mapped->updateByUid = $updateBy->getUid();
		}

		$createBy = $mapped->getCreateBy();
		if ( $createBy ) {
			$mapped->createById = $createBy->id;
			$mapped->createByUid = $createBy->getUid();
		}

		$lockBy = $mapped->getLockBy();
		if ( $lockBy ) {
			$mapped->lockById = $lockBy->id;
			$mapped->lockByUid = $lockBy->getUid();
		}

		$doctype = $mapped->getDoctype();
		if ( $doctype ) {
			$mapped->doctypeId = $doctype->id;
		}

		$category = $mapped->getCategory();
		if ( $category ) {
			$mapped->categoryId = $category->id;
		}

		$spacename = explode('_', $this->_table);
		$mapped->spacename = $spacename[0];

		return parent::save($mapped);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbs\Dao.Sier::hydrate()
	 */
	public function hydrate($mapped, array $row, $fromApp = false)
	{
		parent::hydrate($mapped, $row, $fromApp);
		$mapped->spacename = $row['spacename'];
		return $mapped;
	}

	/**
	 *
	 * @see \Rbplm\Dao\DaoInterface::loadFromName() Load the last version of this named document
	 */
	public function loadFromName(MappedInterface $mapped, $name, $versionId = null)
	{
		if ( $versionId ) {
			$filter = 'name=:name AND version=:versionId LIMIT 1';
			$bind = array(
				':name' => $name,
				':versionId' => $versionId
			);
		}
		else {
			$filter = "name=:name ORDER BY version DESC";
			$bind = array(
				':name' => $name
			);
		}
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 * If $versionId is null, load the last version
	 *
	 * @param MappedInterface $mapped
	 * @param string $number
	 * @param integer $versionId
	 * @return \Rbplm\Dao\MappedInterface
	 */
	public function loadFromNumber(MappedInterface $mapped, $number, $versionId = null)
	{
		if ( $versionId ) {
			$filter = 'number=:number AND version=:versionId LIMIT 1';
			$bind = array(
				':number' => $number,
				':versionId' => $versionId
			);
		}
		else {
			$filter = "number=:number ORDER BY version DESC";
			$bind = array(
				':number' => $number
			);
		}
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 *
	 * @param integer $documentId
	 */
	public function loadPreviousVersion($previous, $documentId)
	{
		$table = $this->_table;
		$filter = "number=(SELECT number FROM $table WHERE id=:id)
					AND version<(SELECT version FROM $table WHERE id=:id)
					ORDER BY version DESC";

		/* Get the previous versionId */
		$this->load($previous, $filter, array(
			':id' => $documentId
		));
	}

	/**
	 *
	 * @param string $number
	 * @return integer
	 */
	public function getLastVersion($number)
	{
		if ( !$this->getlastversionStmt ) {
			$table = $this->_table;
			$sql = "SELECT MAX(version) FROM $table WHERE number=:number GROUP BY number";
			$this->getlastversionStmt = $this->connexion->prepare($sql);
		}
		$this->getlastversionStmt->execute(array(
			':number' => $number
		));
		$lastVersionId = $this->getlastversionStmt->fetchColumn(0);
		return $lastVersionId;
	}
}
