<?php
namespace Rbs\Ged\Document;

use Rbplm\Ged\Document\Version as Emitter;
use Rbs\Space\Factory as DaoFactory;
use Rbs\EventDispatcher\StoppableEvent;
use Rbs\History\HistoryEventInterface;

/**
 * VERY generic to see if a task can be stopped
 */
class Event extends StoppableEvent implements HistoryEventInterface
{

	use \Rbs\History\EventTrait;

	const POST_SETNUMBER = 'setnumber.post';

	const POST_SETSTATE = 'setstate.post';

	const PRE_CREATE = 'create.pre';

	const POST_CREATE = 'create.post';

	const PRE_EDIT = 'edit.pre';

	const POST_EDIT = 'edit.post';

	const PRE_SAVE = 'save.pre';

	const POST_SAVE = 'save.post';

	const PRE_CHECKOUT = 'checkout.pre';

	const POST_CHECKOUT = 'checkout.post';

	const PRE_CHECKIN = 'checkin.pre';

	const POST_CHECKIN = 'checkin.post';

	const PRE_UPDATE = 'update.pre';

	const POST_UPDATE = 'update.post';

	const PRE_CANCELCHECKOUT = 'cancelcheckout.pre';

	const POST_CANCELCHECKOUT = 'cancelcheckin.post';

	const PRE_DELETE = 'delete.pre';

	const POST_DELETE = 'delete.post';

	const PRE_COPY = 'copy.pre';

	const POST_COPY = 'copy.post';

	const PRE_MOVE = 'move.pre';

	const POST_MOVE = 'move.post';

	const PRE_LOCK = 'lock.pre';

	const POST_LOCK = 'lock.post';

	const PRE_UNLOCK = 'unlock.pre';

	const POST_UNLOCK = 'unlock.post';

	const PRE_NEWVERSION = 'newversion.pre';

	const POST_NEWVERSION = 'newversion.post';

	const PRE_ADDFILE = 'addfile.pre';

	const POST_ADDFILE = 'addfile.post';
	
	const PRE_SETDOCTYPE = 'setdoctype.pre';

	const POST_SETDOCTYPE = 'setdoctype.post';
	
	/**
	 *
	 * @var DaoFactory
	 */
	protected $factory;

	/**
	 * 
	 * @param string $name
	 * @param Emitter $document
	 * @param DaoFactory $factory
	 */
	public function __construct(string $name, Emitter $document, DaoFactory $factory)
	{
		$this->setName($name);
		$this->emitter = $document;
		$this->factory = $factory;
	}

	/**
	 *
	 * @return DaoFactory
	 */
	public function getFactory(): DaoFactory
	{
		return $this->factory;
	}
}
