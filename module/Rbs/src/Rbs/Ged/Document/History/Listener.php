<?php
namespace Rbs\Ged\Document\History;

use Rbs\Ged\Document\Event;
use Rbs\Ged\Document\History;
use Rbs\History\Exception;
use Rbplm\Ged\Document;

/**
 * 
 *
 */
class Listener
{

	/**
	 * @param Event $event
	 * @return Event
	 */
	public static function onEvent(Event $event): Event
	{
		$emitter = $event->getEmitter();
		$history = History::init();

		if ( $emitter instanceof Document\Version ) {
			$document = $emitter;
		}
		elseif ( isset($emitter->document) ) {
			$document = $emitter->document;
		}
		else {
			$document = null;
		}

		/* run history only on post events */
		$eventName = $event->getName();
		/* remove .pre from event name */
		$explode = explode('.', $eventName);
		if ( $explode[1] == 'pre' ) {
			return $event;
		}
		$actionName = $explode[0];

		/* set a action name user friendly */
		if ( isset($event->actionName) ) {
			$actionName = $event->actionName;
		}

		/* transform event to history action entity */
		$batch = $event->getBatch();
		if ( $batch ) {
			$history->getAction()->setBatch($batch);
			$event->setComment($batch->getComment());
		}

		$history->getAction()
			->setName($actionName)
			->setComment($event->getComment());

		if ( $document ) {
			$history->setData($document->getArrayCopy());
		}

		/* init dao */
		try {
			$factory = $event->getFactory();
			$history->dao = $factory->getDao(History::$classId);
		}
		catch( \Throwable $e ) {
			$historyException = new Exception($e->getMessage());
			throw $historyException;
		}

		/* save history */
		try {
			$history->dao->save($history);
		}
		catch( \PDOException $e ) {
			$historyException = new Exception($e->getMessage());
			$historyException->lastQueryString = $history->dao->lastStmt->queryString;
			$historyException->lastQueryBind = $history->dao->lastBind;
			throw $historyException;
		}

		return $event;
	}
}

