<?php
//%LICENCE_HEADER%
namespace Rbs\Ged\Document\History;

use Rbplm\Any;
use Rbplm\Mapped;
use Rbplm\Dao\MappedInterface;

/**
 *
 */
class Batch extends Any implements MappedInterface
{
	use Mapped;

	/** @var string */
	static $classId = '600b50cfhisto';
	
	/**
	 * 
	 * @var string
	 */
	protected $owner;
	
	/**
	 * 
	 * @var string
	 */
	protected $created;
	
	/**
	 * 
	 * @var string
	 */
	protected $comment;

	/**
	 * @param string $name
	 * @return Any
	 */
	public static function init()
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		$name = uniqid();
		$obj->setName($name);
		return $obj;
	}
	
	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Any
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['owner'])) ? $this->owner = $properties['owner'] : null;
		(isset($properties['created'])) ? $this->created = $properties['created'] : null;
		(isset($properties['comment'])) ? $this->comment = $properties['comment'] : null;
		return $this;
	}
	
} /* End of class */
