<?php
namespace Rbs\Ged\Document;

use Rbplm\Ged\Document\Version as DocumentVersion;
use Exception;
use Ranchbe;

/** 
 * Class to view file or attachment file
 */
class Viewer extends \Rbs\Viewer\Viewer
{

	/** @var DocumentVersion */
	private $document;

	/**
	 * Filename and path to the visu file
	 * @var string
	 */
	private $visuFile;

	/**
	 * Filename and path to the thumbnail file
	 * @var string
	 */
	private $thumbFile;

	/**
	 * Filename and path to the picture file
	 * @var string
	 */
	private $pictureFile;

	/**
	 * @param DocumentVersion $document
	 */
	function __construct()
	{}

	/**
	 *
	 * @param string $property
	 */
	public function __get($name)
	{
		switch ($name) {
			case 'document':
				return $this->document;
			case 'thumbFile':
				return $this->thumbFile;
			case 'visuFile':
				return $this->_getVisuFile();
			case 'pictureFile':
				return $this->_getPictureFile();
			default:
				return parent::__get($name);
		}
	}

	/**
	 * Get the properties from fsdata object
	 */
	public function initFromDocument(DocumentVersion $document)
	{
		if ( $document->getId() < 1 ) {
			throw new Exception('document is not set', E_USER_WARNING);
		}
		
		$this->document = $document;
		return $this;
	}

	/**
	 * Show the picture associate to current document in a html page (embeded)
	 *
	 */
	public function displayPicture()
	{
		if ( !$this->_getPictureFile() ) {
			return false;
		}
		
		$encHeader = 'data:' . $this->_mimetype . ';base64,';
		$imgdata = $encHeader . base64_encode(file_get_contents($this->pictureFile));
		return '<img border=0 align="center" src="' . $imgdata . '" height="300" />';
	}

	/**
	 * Show the visualisation file of the current document in a html page (embeded)
	 *
	 */
	public function displayVisu()
	{
		if ( !$this->_getVisuFile() ) {
			return false;
		}
		
		$this->_initDriver();
		return $this->_embeded();
	}

	/**
	 * Show the visualisation file of the current document in a html page (embeded)
	 */
	public function display()
	{
		if ( !$this->_getMainFile() ) {
			return false;
		}
		
		$this->_initDriver();
		return $this->_embeded();
	}

	/**
	 * Push picture file to download it
	 *
	 */
	public function pushPicture()
	{
		if ( !$this->_getPictureFile() ) {
			return false;
		}
		
		return $this->_pushfile();
	}

	/**
	 * Push visu file to download it
	 */
	public function pushVisu()
	{
		if ( !$this->_getVisuFile() ) {
			return false;
		}
		
		return $this->_pushfile();
	}

	/**
	 * Get the visualisation file name and path
	 */
	public function push()
	{
		if ( !$this->_getMainFile() ) {
			return false;
		}
		
		return $this->_pushfile();
	}

	/**
	 * Get the picture file name and path
	 */
	private function _getPictureFile()
	{
		if ( !$this->pictureFile ) {
			if ( !isset($this->document) ) {
				throw new Exception('this->document is not set');
			}
			$docfile = $this->document->getDocfile('mainpicture');
			if ( $docfile ) {
				$this->_initFsdata($docfile->getFsdata());
				$this->pictureFile = $this->_file_path;
			}
			else {
				return false;
			}
		}
		return $this->pictureFile;
	}

	/**
	 * Get the visualisation file name and path
	 */
	private function _getVisuFile()
	{
		if ( !$this->visuFile ) {
			if ( !isset($this->document) ) {
				throw new Exception('this->document is not set');
			}
			$docfile = false;
			$search_order = Ranchbe::get()->getConfig('visu.roles.searchorder');
			if ( !$search_order ) {
				$search_order = array(
					'main'
				);
			}
			foreach( $search_order as $role ) {
				$docfile = $this->document->getDocfile($role);
				if ( $docfile ) break;
			}
			if ( $docfile ) {
				$this->_initFsdata($docfile->getFsdata());
				$this->visuFile = $this->_file_path;
			}
			else {
				return false;
			}
		}
		return $this->visuFile;
	}

	/**
	 * Get the visualisation file name and path
	 *
	 */
	private function _getMainFile()
	{
		if ( !isset($this->document) ) {
			throw new Exception('this->document is not set');
		}
		
		$factory = $this->document->dao->factory;
		$dao = $factory->getDao(\Rbplm\Ged\Docfile\Version::$classId);
		$dao->loadDocfiles($this->document);
		
		$docfile = array_shift($this->document->getDocfiles());
		if ( $docfile ) {
			$this->initFromFsdata($docfile->getData()
				->getFsdata());
		}
		else {
			return false;
		}
		return $docfile;
	}
} /* End of class */
