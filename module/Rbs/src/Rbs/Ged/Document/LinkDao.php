<?php
// %LICENCE_HEADER%
namespace Rbs\Ged\Document;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;
use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;

/** SQL_SCRIPT>>

CREATE TABLE IF NOT EXISTS `workitem_doc_rel` (
 `link_id` INT(11) NOT NULL,
 `name` VARCHAR(256) default NULL,
 `parent_id` INT(11) default NULL,
 `parent_uid` VARCHAR(64) default NULL,
 `parent_space_id` INT(11) default NULL,
 `child_id` INT(11) default NULL,
 `child_uid` VARCHAR(64) default NULL,
 `child_space_id` INT(11) default NULL,
 `acode` INT(11) default NULL,
 `lindex` INT(11) default 0,
 hash CHAR(32) default NULL,
 data TEXT default NULL,
 PRIMARY KEY (`link_id`),
 UNIQUE KEY (`parent_id`,`child_id`),
 UNIQUE KEY (`parent_id`,`name`),
 INDEX (`lindex`),
 INDEX (`parent_uid`),
 INDEX (`child_uid`),
 INDEX (`parent_id`, `parent_space_id`),
 INDEX (`child_id`, `child_space_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `document_rel_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `document_rel_seq` (`id`) VALUES (10);

CREATE TABLE IF NOT EXISTS `bookshop_doc_rel` LIKE `workitem_doc_rel`;
CREATE TABLE IF NOT EXISTS `cadlib_doc_rel` LIKE `workitem_doc_rel`;
CREATE TABLE IF NOT EXISTS `mockup_doc_rel` LIKE `workitem_doc_rel`;

<< */

/**
 * SQL_INSERT>>
 * <<
 */

/**SQL_ALTER>>

 ALTER TABLE `workitem_doc_rel`
 ADD CONSTRAINT `FK_workitem_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_workitem_doc_rel_11` FOREIGN KEY (`child_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

 ALTER TABLE `bookshop_doc_rel`
 ADD CONSTRAINT `FK_bookshop_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_bookshop_doc_rel_11` FOREIGN KEY (`child_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

 ALTER TABLE `cadlib_doc_rel`
 ADD CONSTRAINT `FK_cadlib_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_cadlib_doc_rel_11` FOREIGN KEY (`child_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

 ALTER TABLE `mockup_doc_rel`
 ADD CONSTRAINT `FK_mockup_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_mockup_doc_rel_11` FOREIGN KEY (`child_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
 
<<*/

/**
 * SQL_FKEY>>
 * <<
 */

/** SQL_TRIGGER>>
<<*/

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */

/**
 */
class LinkDao extends DaoSier
{

	/** @var \PDOStatement */
	protected $loadChildrenStmt;

	/** @var \PDOStatement */
	protected $loadParentStmt;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'link_id' => 'id',
		'name' => 'name',
		'parent_id' => 'parentId',
		'parent_uid' => 'parentUid',
		'parent_space_id'=>'parentSpaceId',
		'child_id' => 'childId',
		'child_uid' => 'childUid',
		'child_space_id' => 'childSpaceId',
		'acode' => 'accessCode',
		'lindex' => 'lindex',
		'hash' => 'hash',
		'data' => 'data'
	);

	/**
	 * 
	 * @param \PDO $conn
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);

		$this->_childTable = static::$childTable;
		$this->_childForeignKey = static::$childForeignKey;
		$this->_parentTable = static::$parentTable;
		$this->_parentForeignKey = static::$parentForeignKey;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbs\Dao.Sier::save()
	 */
	public function save(MappedInterface $mapped)
	{
		$parentId = $mapped->getParent(true);
		if ( $parentId ) {
			$mapped->parentId = $parentId;
		}

		$childId = $mapped->getChild(true);
		if ( $childId ) {
			$mapped->childId = $childId;
		}

		$name = $mapped->getName();
		if ( !$name && !$childId ) {
			throw new Exception('REFERENCE NOT SAVED', Error::BAD_UID);
		}
		
		if ( !$parentId ) {
			throw new Exception('PARENT NOT SAVED', Error::BAD_UID);
		}

		$mapped->hash = md5($parentId . $childId);
		
		parent::save($mapped);
	}

	/**
	 *
	 * @throws Exception
	 */
	public function bind($mapped)
	{
		return [
			':name' => $mapped->getName(),
			':uid' => $mapped->getUid(),
			':parentId' => (int)$mapped->parentId,
			':parentUid' => $mapped->parentUid,
			':parentSpaceId' => $mapped->parentSpaceId,
			':childId' => (int)$mapped->childId,
			':childUid' => $mapped->childUid,
			':childSpaceId' => $mapped->childSpaceId,
			':lindex' => (int)$mapped->lindex,
			':data' => \json_encode($mapped->data)
		];
	}

	/**
	 * Update a link between document
	 * Return true or false
	 *
	 * This method search in the _doc_rel table the document linked with id $l_document_id and replace it
	 * by the the $_new_l_document_id
	 * This function is used when the document is indice upgraded for always keeps the links.
	 * The link is replace only if is not explicitly locked. A link is locked if the value of dr_access_code is > to 14.
	 *
	 * @param integer $childId
	 *        	Id of the linked document.
	 * @param integer $newChildId(
	 *        	New id of the linked document.
	 */
	function replace($childId, $newChildId, $newChildUid)
	{
		throw new \Exception("NOT IMPLEMENTED");
		
		$query = "UPDATE $this->_table
					SET `child_id` = REPLACE(`child_id`, $childId, $newChildId)
					WHERE `child_id` = '$childId'
					AND (`dr_access_code` < '15' OR `dr_access_code` IS NULL)";
		
		return $query;
	}

	/**
	 * Get list of children links
	 * 
	 * Some alias are defined to be used in filters :
	 * <ul>
	 * * <li>.lt alias to refer to link table ([spacename]_doc_rel) </li>
	 * * <li>.doc to refer to children documents </li>
	 * </ul>
	 * 
	 * @example
	 *  Return all documents children of document identified by $parentDocumentId :
	 *  $filter->select(['doc.*']);
	 * 	$filter->andfind($parentDocumentId, 'lt.parent_id', Op::EQUAL)
	 * 
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return \PDOStatement
	 */
	public function getChildren($filter = null, $bind = array())
	{
		$leftTable = $this->_table;
		$leftKey = $this->toSys('childId');
		$rightTable = $this->_childTable;
		$rightKey = $this->_childForeignKey;
		$select = array();

		if ( !$this->loadChildrenStmt ) {
			if ( $filter instanceof \Rbplm\Dao\FilterAbstract ) {
				$filterStr = $filter->__toString();
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
			}
			else {
				$filterStr = '1=1';
				$selectStr = '*';
			}

			$sql = "SELECT $selectStr FROM $leftTable AS lt";
			$sql .= " JOIN $rightTable AS doc ON lt.$leftKey=doc.$rightKey";
			$sql .= " WHERE $filterStr";

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadChildrenStmt = $stmt;
		}
		
		$this->loadChildrenStmt->execute($bind);
		return $this->loadChildrenStmt;
	}

	/**
	 * Get documents father linked to document.
	 * 
	 * Some alias are defined to be used in filters :
	 * <ul>
	 * * <li>.lt alias to refer to link table  ([spacename]_doc_rel) </li>
	 * * <li>.doc to refer to parent documents </li>
	 * </ul>
	 * 
	 * @example
	 *  Return all documents parents of document identified by $childDocumentId :
	 *  $filter->select(['doc.*']);
	 * 	$filter->andfind($childDocumentId, 'lt.child_id', Op::EQUAL)
	 * 
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return \PDOStatement
	 *
	 */
	function getParents($filter = null, $bind = array())
	{
		$leftTable = $this->_table;
		$leftKey = $this->toSys('parentId');
		$rightTable = $this->_parentTable;
		$rightKey = $this->_parentForeignKey;
		$select = array();

		if ( !$this->loadParentStmt ) {
			if ( $filter ) {
				$filterStr = $filter->__toString();
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
			}
			else {
				$filterStr = '1=1';
				$selectStr = '*';
			}

			$sql = "SELECT $selectStr FROM $leftTable AS lt";
			$sql .= " JOIN $rightTable AS doc ON lt.$leftKey=doc.$rightKey";
			$sql .= " WHERE $filterStr";

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadParentStmt = $stmt;
		}

		$this->loadParentStmt->execute($bind);
		return $this->loadParentStmt;
	}
	
	/**
	 * @param integer $parentId 	The id of parent document of links to delete
	 * @param integer $spaceId 		The space id as return by Space\Factory::getId()
	 * @param string $linkType 		[OPTIONAL] Limit the delete to type of link, or let null to delete all types
	 * 
	 * @return LinkDao
	 */
	public function deleteFromParentId($parentId, $spaceId, $linkType=null)
	{
		$toSysId = $this->toSys('parentId');
		$toSysSpaceId = $this->toSys('parentSpaceId');
		$filter = "$toSysId=:parentId AND $toSysSpaceId=:spaceId";
		$bind = array(
			':parentId' => $parentId,
			':spaceId' => $spaceId
		);
		
		$this->_deleteFromFilter($filter, $bind);
		return $this;
	}
	
}
