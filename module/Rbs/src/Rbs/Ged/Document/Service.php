<?php
namespace Rbs\Ged\Document;

use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\AccessCode;
use Rbplm\Ged\Doctype;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Ged\Document\Service\Respons as ServiceRepons;
use Rbs\Ged\Document\Service\Exception;
use Rbplm\Dao\NotExistingException;
use Rbplm\Dao\ExistingException;
use Rbplm\Ged\AccessCodeException;
use Rbplm\Org\Workitem;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Vault\Vault;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;
use Rbplm\Sys\Date as DateTime;
use Rbplm\Sys\Fsdata;
use Rbs\Org\Container\Link\Doctype as DoctypeLink;
use Rbs\Ged\Docfile\Service\WsCleanupFailed;
use Rbs\EventDispatcher\EventManager;

/**
 *
 * High level service to manipulate document.
 *
 * This class work with controller.
 * It get out Controller some code to be reused in all  controllers of the application.
 * This class is dependents of Models and Daos classes.
 *
 */
class Service implements \Rbs\EventDispatcher\EventManagerAwareInterface
{

	/**
	 * 
	 * @var EventManager
	 */
	protected $eventManager;

	/**
	 *
	 * @var ServiceRepons
	 */
	public $respons;

	/**
	 *
	 * @var DaoFactory
	 */
	public $factory;

	/**
	 * 
	 * @var array
	 */
	public $inputPdmData;

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function __construct(DaoFactory $factory)
	{
		$this->factory = $factory;
		$factory->documentService = $this;
		$this->respons = new ServiceRepons();
	}

	/**
	 *
	 * @param EventManager $eventManager
	 */
	public function setEventManager(EventManager $eventManager)
	{
		$this->eventManager = $eventManager;
		return $this;
	}

	/**
	 *
	 * @return EventManager
	 */
	public function getEventManager()
	{
		if ( !isset($this->eventManager) ) {
			/* construct a fake EventManager to prevent Exceptions */
			$this->eventManager = new EventManager();
		}
		return $this->eventManager;
	}

	/**
	 * Lock documents and associated files.
	 * if $withFiles is true, lock too all docfiles return by Document\Version::getDocfiles().
	 * The return Docfiles must be have property dao with the dao to use for save docfile.
	 * The Document\Version $document must have dao property set with the dao used to save object.
	 * if $accessCode == 0, document is unlocked
	 *
	 * @param Document\Version $document
	 * @param integer $accessCode
	 *        	One of class constant AccessCode::*
	 * @param boolean $withFiles
	 * @throws Exception
	 * @return Service
	 */
	public function lock($document, $accessCode, $withFiles = true)
	{
		$factory = $this->factory;

		if ( !isset($document->dao) ) {
			throw new Exception(sprintf('dao is not set to %s', $document->getName()));
		}

		$conn = $factory->getConnexion();
		$conn->beginTransaction();
		
		if($accessCode > 0){
			$preEventName = Event::PRE_LOCK;
			$postEventName = Event::POST_LOCK;
		}
		else{
			$preEventName = Event::PRE_UNLOCK;
			$postEventName = Event::POST_UNLOCK;
		}

		/* Signal */
		$event = new Event($preEventName, $document, $factory);
		(isset($this->batch)) ? $event->setBatch($this->batch) : null;
		(isset($document->comment)) ? $event->setComment($document->comment) : null;
		$this->getEventManager()->trigger($event);
		
		if ( $accessCode == 0 ) {
			$document->unlock();
		}
		else {
			$document->lock($accessCode, CurrentUser::get());
		}

		/* get list of associated files and checkout */
		try {
			if ( $withFiles ) {
				/* lock and save docfiles */
				$docfiles = $document->getDocfiles();
				if ( $docfiles ) {
					foreach( $docfiles as $docfile ) {
						if ( !$docfile->dao ) {
							throw new Exception(sprintf('dao is not set to %s', $docfile->getName()));
						}
						/** @var \Rbplm\Ged\Docfile\Version $docfile */
						if ( $accessCode == 0 ) {
							$docfile->unlock();
						}
						else {
							$docfile->lock($accessCode, CurrentUser::get());
						}
						$docfile->dao->save($docfile);
					}
				}
			}
			/* save document */
			$document->dao->save($document);
		}
		catch( \Exception $e ) {
			$conn->rollBack();
			throw $e;
		}
		$conn->commit();

		/* Notiy observers on event */
		$event->setName($postEventName);
		$event->actionName = AccessCode::getName($accessCode);
		$this->getEventManager()->trigger($event);

		return $this;
	}

	/**
	 * Put files of a Document from the vault reposit directory to user wildspace and lock the Document.
	 *
	 * CheckOut is use for modify a Document. The files are put in a directory where user has write access
	 * and the Document storing on the protected directory (the vault) is lock. Thus it is impossible that a other user modify it in the same time.
	 *
	 * If option $checkoutFile is false, options $ifExistMode, $getFiles and $withFiles are ignored
	 * If option $getFiles is false, options $ifExistMode is ignored
	 *
	 * @param Document\Version $document
	 * @param boolean $withFiles
	 *        	if true, checkin all associated docfiles loaded in document
	 * @param boolean $replace
	 *        	if true replace file in ws if existing
	 * @param boolean $getFiles
	 *        	if true copy file in wildspace
	 * @throws \Exception
	 * @return Service
	 *
	 */
	public function checkout($document, $withFiles = true, $replace = true, $getFiles = true)
	{
		$factory = $this->factory;
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		/* check if access is free, valide codes : 0-1 */
		$aCode = $document->checkAccess();
		if ( $aCode > AccessCode::FREE ) {
			throw new AccessCodeException(sprintf('LOCKED_%s_TO_PREVENT_THIS_ACTION', $aCode));
		}

		$preEventName = Event::PRE_CHECKOUT;
		$postEventName = Event::POST_CHECKOUT;

		/* Notiy observers on event */
		$event = new Event($preEventName, $document, $factory);
		(isset($this->batch)) ? $event->setBatch($this->batch) : null;
		$this->getEventManager()->trigger($event);

		$document->lock(AccessCode::CHECKOUT, CurrentUser::get());

		/* get list of associated files and checkout */
		if ( $withFiles ) {
			$docfiles = $document->getDocfiles();
			if ( $docfiles ) {
				foreach( $docfiles as $docfile ) {
					try {
						$dfService->checkout($docfile, $replace, $getFiles);
					}
					catch( AccessCodeException $e ) {
						continue;
					}
				}
			}
		}
		$document->dao->save($document);

		/* Notiy observers on event */
		$event->setName($postEventName);
		$this->getEventManager()->trigger($event);

		return $this;
	}

	/**
	 * Replace a file checkout in the vault and unlock it.
	 * The checkIn copy file from the wildspace to vault reposit dir
	 * If the file has been changed (check by md5 code comparaison), create a new iteration
	 *
	 * The document must be populate with Docfiles.
	 * The parent container must be set if updateData is true
	 *
	 * @param Document\Version $document
	 * @param bool $releasing
	 *        	if true, release the docfile after replace
	 * @param bool $updateData
	 *        	if true, update vaulted files data
	 * @param bool $deleteFileInWs
	 *        	if true, delete files in widlspace if checkin is successed
	 * @param bool $fromWildspace
	 *        	if true, update vaulted files from files founded in wildspace. updateData must be true to take effect.
	 * @param bool $checkAccess
	 *        	if true, check if the document and docfile accessCode and checkoutBy properties are in the right value.
	 *        	Set to false only for administration tasks
	 * @throws Exception
	 * @return Service
	 *
	 */
	public function checkin(Document\Version $document, $releasing = true, $updateData = true, $deleteFileInWs = true, $fromWildspace = true, $checkAccess = true)
	{
		$factory = $this->factory;
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		/* Action Name definition is used to display event in history */
		$actionName = null;

		if ( $releasing == true && $updateData == true ) {
			$actionName = 'checkinAndRelease';
			$preEventName = Event::PRE_CHECKIN;
			$postEventName = Event::POST_CHECKIN;
		}
		elseif ( $releasing == true && $updateData == false ) {
			$actionName = 'cancelCheckout';
			$preEventName = Event::PRE_CANCELCHECKOUT;
			$postEventName = Event::POST_CANCELCHECKOUT;
		}
		elseif ( $releasing == false && $updateData == true ) {
			$actionName = 'checkinAndKeep';
			$preEventName = Event::PRE_UPDATE;
			$postEventName = Event::POST_UPDATE;
		}

		$aCode = $document->checkAccess();
		$coBy = $document->lockById;
		if ( ($aCode != AccessCode::CHECKOUT or $coBy != CurrentUser::get()->getId()) and $checkAccess ) {
			throw new AccessCodeException(sprintf('You have not access to this document locked By %s', $coBy));
		}

		/* Notiy observers on event */
		$event = new Event($preEventName, $document, $factory);
		$event->actionName = $actionName;
		(isset($this->batch)) ? $event->setBatch($this->batch) : null;
		(isset($document->comment)) ? $event->setComment($document->comment) : null;
		$this->getEventManager()->trigger($event);
		
		/** @var Docfile\Version $mainDf Main Docfile */
		$mainDf = $document->getMainDocfile();

		if ( $actionName != 'cancelCheckout' ) {
			/* Load vault */
			$container = $document->getParent();
			$reposit = Reposit::init($container->path);
			$vault = new Vault($reposit, $factory->getDao(Record::$classId));

			/* Set pdmDatas from mainDocfile name as key */
			/* @Move in CALLBACK :
			 if ( $this->inputPdmData ) {
			 $inputPdmData = $this->inputPdmData;

			 if ( !isset($this->wildspace) ) {
			 $wildspace = Wildspace::get(CurrentUser::get());
			 }
			 else {
			 $wildspace = $this->wildspace;
			 }

			 $rbconverterWorkingDir = $wildspace->getPath() . '/rbconverter/';

			 if ( $mainDf ) {
			 $pdmdataKey = $mainDf->getName();
			 if ( $inputPdmData->offsetExists($pdmdataKey) ) {
			 $inputElmt = $inputPdmData->offsetGet($pdmdataKey);
			 $inputElmt->setWorkingDir($rbconverterWorkingDir);
			 $setter = new \Pdm\Input\PdmData\Setter($factory);
			 $setter->setDocument($document, $inputElmt);
			 }
			 }
			 }
			 */

			/* Remove previous VISU only except for reset action */
			/*
			 $tempdoc = new Document\Version();
			 $tempdoc->setId($document->getId());
			 $dfdao = $factory->getDao(Docfile\Version::$classId);
			 $dfdao->loadDocfiles($tempdoc, $dfdao->toSys('mainrole') . ' >= ' . Docfile\Role::VISU);
			 foreach( $tempdoc->getDocfiles() as $df ) {
			 $dfService->delete($df, $withfiles = true, $withiterations = false);
			 }
			 unset($tempdoc);
			 */
		}

		/** @var array $docfiles list of checkouted  associated files */
		$docfiles = $document->getDocfiles();

		/* Checkin associated files */
		$iterate = false;
		if ( $docfiles && $actionName != 'cancelCheckout' ) {
			foreach( $docfiles as $docfile ) {
				$preDfIteration = 0;
				try {
					/* If docfile is existing in db */
					if ( $docfile->getId() > 0 ) {
						$preDfIteration = $docfile->iteration;
						if ( $updateData && $fromWildspace ) {
							$dfService->checkinFromWs($docfile, $releasing, $updateData, $deleteFileInWs, $checkAccess);
						}
						else {
							$dfService->checkin($docfile, $releasing, $updateData, $checkAccess);
						}
					}
					/* If not yet saved */
					else {
						$dfService->create($docfile, $vault);
					}
				}
				catch( WsCleanupFailed $e ) {
					$this->respons->addFeedback('file %s may not be deleted from wildspace. May be is open?', $docfile->getNumber());
				}
				catch( AccessCodeException $e ) {
					continue;
				}
				catch( \Rbs\Ged\Docfile\Service\NothingToUpdate $e ) {
					$this->respons->addFeedback('docfile %s has none updated datas', $docfile->getNumber());
					continue;
				}
				catch( \PDOException $e ) {
					throw $e;
				}
				catch( \Exception $e ) {
					throw $e;
				}

				/* check if docfile is changed */
				if ( $docfile->iteration > $preDfIteration ) {
					$iterate = true;
				}
			} /* endforeach */

			/* Iterate document if docfiles are changed */
			if ( $iterate ) {
				$document->iteration = $document->iteration + 1;
				$document->setUpdated(new \DateTime());
				$document->setUpdateBy(CurrentUser::get());
			}
		}
		elseif ( $docfiles && $actionName == 'cancelCheckout' ) {
			foreach( $docfiles as $docfile ) {
				try {
					$dfService->checkin($docfile, true, false, $checkAccess);
				}
				catch( AccessCodeException $e ) {
					continue;
				}
				catch( \Exception $e ) {
					throw $e;
				}
			}
		}

		/* Remove checkout access restriction */
		if ( $releasing == true ) {
			$document->unLock();
		}

		/* Checkin and save product */
		if ( isset($document->product) ) {
			$product = $document->product;
			$product->setDocument($document);
			$pdmservice = new \Rbs\Pdm\Product\Service($factory);
			$pdmservice->checkin($product);
			unset($pdmservice);
		}

		/* Save Document */
		$document->dao->save($document);

		/* Notiy observers on event */
		$this->getEventManager()->trigger($event->setName($postEventName));
		
		return $this;
	}

	/**
	 * Update vault data from wildspace or from data attached to $document.
	 * Docfiles must be loaded before.
	 * 
	 * @param Document\Version $document
	 * @param bool $deleteFileInWs
	 *        	if true, delete files in widlspace if checkin is successed
	 * @param bool $fromWildspace
	 *        	if true, update vaulted files from files founded in wildspace. updateData must be true to take effect.
	 * @param bool $checkAccess
	 *        	if true, check if the document and docfile accessCode and checkoutBy properties are in the right value.
	 *        	Set to false only for administration tasks
	 * @throws Exception
	 * @return Service
	 *
	 */
	public function update(Document\Version $document, $deleteFileInWs = true, $fromWildspace = true, $checkAccess = true)
	{
		$factory = $this->factory;
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		$aCode = $document->checkAccess();
		$coBy = $document->lockById;
		if ( $aCode == AccessCode::CHECKOUT ) {
			if ( ($coBy != CurrentUser::get()->getId()) and $checkAccess ) {
				throw new AccessCodeException(sprintf('You have not access to this document locked By %s', $coBy));
			}
		}

		/* Action Name definition */
		$preEventName = Event::PRE_UPDATE;
		$postEventName = Event::PRE_UPDATE;

		/* Notiy observers on event */
		$event = new Event($preEventName, $document, $factory);
		(isset($this->batch)) ? $event->setBatch($this->batch) : null;
		(isset($document->comment)) ? $event->setComment($document->comment) : null;
		$this->getEventManager()->trigger($event);

		/* Load vault */
		$container = $document->getParent();
		$reposit = Reposit::init($container->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		/** @var Docfile\Version $mainDf Main Docfile */
		$mainDf = $document->getMainDocfile();
		/** @var array $docfiles list of checkouted  associated files */
		$docfiles = $document->getDocfiles();

		/* Checkin associated files */
		$iterate = false;
		if ( $docfiles ) {
			foreach( $docfiles as $docfile ) {
				$preDfIteration = 0;
				try {
					/* If docfile is existing in db */
					if ( $docfile->getId() > 0 ) {
						$preDfIteration = $docfile->iteration;
						if ( $fromWildspace ) {
							$dfService->updateFromWs($docfile, $deleteFileInWs, $checkAccess);
						}
						else {
							$dfService->update($docfile, $checkAccess);
						}
					}
					/* If not yet saved */
					else {
						$dfService->create($docfile, $vault);
					}
				}
				catch( WsCleanupFailed $e ) {
					$this->respons->addFeedback('file %s may not be deleted from wildspace. May be is open?', $docfile->getNumber());
				}
				catch( AccessCodeException $e ) {
					continue;
				}
				catch( \Rbs\Ged\Docfile\Service\NothingToUpdate $e ) {
					$this->respons->addFeedback('docfile %s has none updated datas', $docfile->getNumber());
					continue;
				}
				catch( \PDOException $e ) {
					throw $e;
				}
				catch( \Exception $e ) {
					throw $e;
				}

				/* check if docfile is changed */
				if ( $docfile->iteration > $preDfIteration ) {
					$iterate = true;
				}
			} /* endforeach */

			/* Iterate document if docfiles are changed */
			if ( $iterate ) {
				$document->iteration = $document->iteration + 1;
				$document->setUpdated(new \DateTime());
				$document->setUpdateBy(CurrentUser::get());
			}
		}

		/* Save Document */
		$document->dao->save($document);

		/* Notiy observers on event */
		$this->getEventManager()->trigger($event->setName($postEventName));

		return $this;
	}

	/**
	 * This method can be used to remove a document from a container
	 * Return true or false.
	 *
	 * This method suppress the record of the document and associated files
	 * and suppress files and version files from the container directory.
	 * If a history indice exist, suppress the current indice and restore the previous indice.
	 *
	 * @param Document\Version $document
	 * @return Service
	 * @throws Exception
	 */
	function delete(Document\Version $document)
	{
		$factory = $this->factory;
		$dao = $factory->getDao(Document\Version::$classId);
		$dfdao = $factory->getDao(Docfile\Version::$classId);

		/* Notiy observers on event */
		$preEventName = Event::PRE_DELETE;
		$postEventName = Event::POST_DELETE;

		/* Signal */
		$event = new Event($preEventName, $document, $factory);
		(isset($this->batch)) ? $event->setBatch($this->batch) : null;
		$this->getEventManager()->trigger($event);

		/* get the container */
		$container = $document->getParent();
		if ( !$container ) {
			$containerId = $document->getParent(true);
			$container = $factory->getModel(Workitem::$classId);
			$container->dao = $factory->getDao(Workitem::$classId);
			$container->dao->loadFromId($container, $containerId);
			$document->setParent($container);
		}

		/* check if access is free */
		$access = $document->checkAccess();
		if ( !($access == AccessCode::FREE || $access == AccessCode::SUPPRESS || $access >= 100) ) {
			throw new AccessCodeException(sprintf('Document %s is locked with code %s, suppress is not permit', $document->getUid(), $access));
		}

		/* get docfiles */
		$dfdao->loadDocfiles($document);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		/* Delete files of current version and old iteration */
		foreach( $document->getDocfiles() as $docfile ) {
			try {
				$withfile = true;
				$withiterations = true;
				$dfService->delete($docfile, $withfile, $withiterations);
			}
			catch( \Exception $e ) {
				throw $e;
			}
		}

		/* Get the previous version of document, IMPORTANT: Before delete*/
		try {
			$previous = new Document\Version();
			$dao->loadPreviousVersion($previous, $document->getId());
		}
		catch( NotExistingException $e ) {
			$previous = null;
		}

		/* Delete record of document */
		$dao->deleteFromId($document->getId());

		/* restore the previous document */
		if ( $previous ) {
			$container = $previous->getParent();
			if ( !$container ) {
				$containerId = $previous->getParent(true);
				$container = $factory->getModel(Workitem::$classId);
				$container->dao = $factory->getDao(Workitem::$classId);
				$container->dao->loadFromId($container, $containerId);
				$previous->setParent($container);
			}

			/* Restore file of previous version */
			$previous->lock(AccessCode::VALIDATE);
			//$previous->setLifeStage('getback');

			$dfdao->loadDocfiles($previous);
			foreach( $previous->getDocfiles() as $docfile ) {
				try {
					$docfile->setParent($previous);
					$dfService->setVersionToHead($docfile);
				}
				catch( \Exception $e ) {
					throw $e;
				}
			}
			$dao->save($previous);
		}

		/* @todo: Delete record of all process instance */
		/* @todo: Delete or archive history */
		/* @todo remove visu file */
		/* @todo remove thumbnail file */

		/* Notiy observers on event */
		$event->setName($postEventName);
		$this->getEventManager()->trigger($event);

		return $this;
	}

	/**
	 * The document must be populate with parent container before
	 * The document must be populate with docfiles
	 * Each docfile must have a property fsdata set with the fsdata to store in vault
	 *
	 * @param boolean $keepCo If true, Document is saved with checkouted status
	 *
	 */
	public function createDocument($document, $keepCo = false)
	{
		/* set some helpers */
		$factory = $this->factory;

		/* load some objects */
		$container = $document->getParent();
		$docfiles = $document->getDocfiles();
		$reposit = Reposit::init($container->path);

		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		$preEventName = Event::PRE_CREATE;
		$postEventName = Event::POST_CREATE;

		/* Signal */
		$event = new Event($preEventName, $document, $factory);
		(isset($this->batch)) ? $event->setBatch($this->batch) : null;
		$this->getEventManager()->trigger($event);

		/* OPEN TRANSACTION */
		$connexion = $factory->getConnexion();
		$connexion->beginTransaction();

		/* SET DOCTYPE, ASSUME THAT FIRST DOCFILE IS THE MAIN DOCFILE */
		$doctype = new Doctype();
		$mainDocfile = reset($docfiles); /* First element */

		$dn = $container->getDn();
		$factory->getDao(DoctypeLink::$classId)->loadFromDocument($doctype, $dn, $document->getNumber(), $mainDocfile->fsdata->getExtension(), $mainDocfile->fsdata->getType());
		$document->setDoctype($doctype);
		$document->newUid();

		if ( $keepCo ) {
			$document->lock(AccessCode::CHECKOUT, CurrentUser::get());
		}

		/* SAVE DOCUMENT */
		if ( !$document->getId() ) {
			$this->respons->addFeedback('create_doc : %s', $document->getNumber());
			try {
				$document->dao->save($document);
				$this->respons->addFeedback('The doc %s has been created', $document->getNumber());
			}
			catch( \Exception $e ) {
				$connexion->rollBack();
				throw new Exception(sprintf(tra('Error during document creation of %s: %s'), $document->getNumber(), $e->getMessage()), null, $e);
			}
		}

		/* PUT FILE IN VAULT */
		$dfService = new \Rbs\Ged\Docfile\Service($factory);
		foreach( $docfiles as $docfile ) {
			/* reset parentUid and parentId */
			$docfile->setParent($document);
			try {
				$dfService->create($docfile, $vault, $keepCo);
				$this->respons->addFeedback('the file %s has been associated to document %s', $docfile->getName(), $document->getUid());
			}
			catch( \Exception $e ) {
				$this->respons->addError('The file %s can not be associated to the document %s. Message : %s', $docfile->getName(), $document->getUid(), $e->getMessage());
				continue;
			}
		}

		$connexion->commit();

		/* Notiy observers on event */
		$event->setName($postEventName);
		$this->getEventManager()->trigger($event);

		return $this;
	}

	/**
	 * Clone and save the $document as new document version.
	 * Load, clone and save associated files as new version.
	 * Lock all previous versions.
	 * 
	 * If associated docfile has a "newData" property setted, use this newData as file asociated to docfile.
	 * newData must be \Rbplm\Vault\Record instance.
	 *
	 * @return \Rbplm\Ged\Document\Version 	New version of document
	 */
	public function newVersion($document, $newContainer = null, $newVersionId = null)
	{
		/* ignore docfiles marked as deleted */
		if ( $document->checkAccess() == AccessCode::SUPPRESS ) {
			throw \Exception('This document is marked as deleted and can not be upgraded');
		}

		$factory = $this->factory;
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$dfDao->loadDocfiles($document);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		$container = $document->getParent();
		$docfiles = $document->getDocfiles();

		$fromvault = new Vault(Reposit::init($container->path), $factory->getDao(Record::$classId));
		if ( $newContainer ) {
			$tovault = new Vault(Reposit::init($newContainer->path), $factory->getDao(Record::$classId));
		}
		else {
			$tovault = $fromvault;
		}

		/* Check access */
		$aCode = $document->checkAccess();
		if ( $aCode != AccessCode::FREE && $aCode <= AccessCode::LOCKEDLEVEL1 ) {
			throw new AccessCodeException(sprintf('LOCKED_%s_TO_PREVENT_THIS_ACTION', $aCode));
		}

		/* Notiy observers on event */
		$preEventName = Event::PRE_NEWVERSION;
		$postEventName = Event::POST_NEWVERSION;

		/* Set the version */
		$lastVersionId = (int)$document->dao->getLastVersion($document->getNumber());
		if ( $newVersionId && $newVersionId > $lastVersionId ) {
			$newVersionId = (int)$newVersionId;
		}
		else {
			$newVersionId = (int)$lastVersionId + 1;
		}

		/* OPEN TRANSACTION */
		$connexion = $factory->getConnexion();
		$connexion->beginTransaction();

		try {
			/* CLONE DOCUMENT */
			/* @var Document\Version $newDocument */
			$newDocument = clone ($document);
			$newDocument->lock(AccessCode::FREE);
			$newDocument->version = $newVersionId;
			//$newDocument->setUid($newDocument->getNumber() . '.' . $newVersionId);
			$newDocument->newUid();
			$newDocument->setFrom($document);
			$newDocument->lifeStage = 'init';
			if ( $newContainer ) {
				$newDocument->setContainer($newContainer);
			}

			/* Signal */
			$event = new Event($preEventName, $document, $factory);
			$event->newDocument = $newDocument;
			(isset($this->batch)) ? $event->setBatch($this->batch) : null;
			(isset($document->comment)) ? $event->setComment($document->comment) : null;
			$this->getEventManager()->trigger($event);

			$document->dao->save($newDocument);
			/* Lock the current indice */
			$document->lock(AccessCode::HISTORY);
			$document->dao->save($document);

			/* CLONE DOCFILES */
			foreach( $docfiles as $docfile ) {
				$accessCode = $docfile->checkAccess();
				/* ignore docfiles marked as deleted */
				if ( $accessCode == AccessCode::SUPPRESS ) {
					continue;
				}

				$newDocfile = clone ($docfile);
				$newDocfile->lock(AccessCode::FREE);
				$newDocfile->version = $newVersionId;
				//$newDocfile->setUid($newDocfile->getName() . '.' . $newVersionId);
				$newDocfile->newUid();
				$newDocfile->setFrom($docfile);
				$newDocument->addDocfile($newDocfile);

				/* CLONE DATAS */
				if ( isset($docfile->newData) ) {
					$newRecord = $docfile->newData;
					$newDocfile->setData($newRecord);
				}
				else {
					$record = $docfile->getData();
					$newRecord = clone ($record);
					$newDocfile->setData($newRecord);
					$newDocfile->getData()->setFsdata(clone ($record->getFsdata()));
				}

				/* Deprive to version only if docfile is not a version, iteration, history, archive */
				if ( $accessCode < AccessCode::ITERATION ) {
					$record = $tovault->depriveToVersion($record, $document->version);
					/* Lock access to files too */
					$docfile->lock(AccessCode::HISTORY);
				}

				/* Re-set record to docfile after move to version reposit */
				$docfile->setData($record);

				/* Save old docfile first to prevent unicity violation */
				$docfile->dao->save($docfile);
				$docfile->dao->save($newDocfile);

				/* Delete iterations of old docfile */
				/* Errors during iterations deletion must not do a rollback */
				try {
					$dfService->deleteIterations($docfile);
				}
				catch( \Exception $e ) {
					/* @todo: log errors on iterations deletions */
					continue;
				}
			} /* endforeach */
		}
		catch( \Exception $e ) {
			$connexion->rollBack();
			throw new Exception($e->getMessage(), $e->getCode(), $e);
		}
		/* Commit changes in db */
		$connexion->commit();
		
		/* Notiy observers on event */
		$this->getEventManager()->trigger($event->setName($postEventName));
		
		$this->respons->addFeedback('the document %s has been locked.', $document->getUid());
		return $newDocument;
	}

	/**
	 * Put file in wildspace
	 */
	public function putinws($document, $prefix = '', $suffix = '')
	{
		if ( !isset($this->wildspace) ) {
			$this->wildspace = Wildspace::get(CurrentUser::get());
		}
		$toPath = $this->wildspace->getPath();
		if ( !$suffix ) {
			$suffix = '.v' . $document->version . '.' . $document->iteration;
		}

		foreach( $document->getDocfiles() as $docfile ) {
			$data = $docfile->getData();
			$data->getFsData()->copy($toPath . '/' . $prefix . $data->rootname . $suffix . $data->extension, 0777, false);
		}
	}

	/**
	 * This method can be used to move a document from container to another.
	 * Return true or false.
	 *
	 * A document can be move only between container of the same space.
	 * You can not move a document of a bookshop container to a cadlib container.
	 * This method modify the document record and move the associated files from the
	 * directory of the original container to the directory of the tager container.
	 * Please, consider that this method dont move version files and the previous indice document.
	 *
	 * @param Document\Version $document
	 * @param Workitem $toContainer the target container.
	 */
	public function moveDocument($document, $toContainer)
	{
		$aCode = $document->checkAccess();
		if ( $aCode >= 100 ) {
			throw new AccessCodeException(sprintf('LOCKED_%s_TO_PREVENT_THIS_ACTION', $aCode));
		}

		// TODO : revoir la transaction en utilisant rollback pour autoriser les validations intermediaires
		$factory = $this->factory;
		$dao = $factory->getDao($document->cid);
		$dfDao = $factory->getDao(Docfile\Version::$classId);

		$reposit = Reposit::init($toContainer->path);

		$conn = $factory->getConnexion();
		$conn->beginTransaction();

		/* SET PARENT */
		$document->setParent($toContainer);

		/* Notiy observers on event */
		$preEventName = Event::PRE_MOVE;
		$postEventName = Event::POST_MOVE;

		/* Signal */
		$event = new Event($preEventName, $document, $factory);
		$event->toContainer = $toContainer;
		(isset($this->batch)) ? $event->setBatch($this->batch) : null;
		$this->getEventManager()->trigger($event);

		/* MOVE FSDATA AND RESET TO DOCFILES */
		foreach( $document->getDocfiles() as $docfile ) {
			/* MOVE FSDATA IN VAULT */
			/** @var \Rbplm\Vault\Record $data */
			$data = $docfile->getData();
			/** @var Fsdata $fsdata */
			$fsdata = $data->getFsdata();
			$fsdata->rename($reposit->getPath() . '/' . $fsdata->getName());

			/* RESET DATA TO DOCFILE */
			$data->setFsdata($fsdata);
			$docfile->setData($data);

			/* TRY TO SAVE DOCFILE */
			try {
				$dfDao->save($docfile);
				$this->respons->addFeedback('File %s has been moved to %s', $fsdata->getName(), $reposit->getPath());
			}
			catch( \Exception $e ) {
				$conn->rollBack();
				throw $e;
			}
		}

		/* TRY TO SAVE DOCUMENT */
		try {
			$dao->save($document);
		}
		catch( \Exception $e ) {
			$conn->rollBack();
			throw $e;
		}

		$conn->commit();

		/* Notiy observers on event */
		$event->setName($postEventName);
		$this->getEventManager()->trigger($event);

		// @todo Copy picture file
		// @todo Copy the doclinks
		// @todo Copy the relationship

		return $document;
	}

	/**
	 * This method can be used to copy a document from container to another.
	 *
	 * A document can be copy only between container of the same space.
	 *
	 * @param Document\Version $document
	 * @param \Rbplm\Org\Workitem $toContainer the target container.
	 * @param string $toNumber number of the new document to create.
	 * @param integer $toVersion version for new document.
	 */
	public function copyDocument($document, $toContainer, $toNumber, $toVersion = 1)
	{
		// TODO : revoir la transaction en utilisant rollback pour autoriser les validations intermediaires
		$fromNumber = $document->getNumber();
		$toPath = $toContainer->path;

		$factory = $this->factory;
		$dao = $factory->getDao($document->cid);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$reposit = Reposit::init($toContainer->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		$preEventName = Event::PRE_COPY;
		$postEventName = Event::POST_COPY;

		/* Signal */
		$event = new Event($preEventName, $document, $factory);
		(isset($this->batch)) ? $event->setBatch($this->batch) : null;
		$this->getEventManager()->trigger($event);

		$conn = $factory->getConnexion();
		$conn->beginTransaction();
		$vault->beginTransaction();

		$toDocument = clone ($document);
		$toDocument->setName($toNumber)->setNumber(\Rbs\Number::normalize($toNumber));
		$toDocument->version = $toVersion;
		$toDocument->iteration = 1;
		$toDocument->lifeStage = 'init';
		$toDocument->setCreated(new DateTime());
		$toDocument->setUpdated(new DateTime());
		$toDocument->setCreateBy(CurrentUser::get());
		$toDocument->setUpdateBy(CurrentUser::get());
		$toDocument->setOwner(CurrentUser::get());
		$toDocument->unlock();
		$toDocument->setFrom($document);
		$toDocument->newUid();

		/* TRY TO SAVE DOCUMENT */
		try {
			$dao->save($toDocument);
		}
		catch( \Exception $e ) {
			$conn->rollBack();
			throw $e;
		}

		foreach( $document->getDocfiles() as $docfile ) {
			$todocfile = clone ($docfile);
			$todocfile->setParent($toDocument);
			$todocfile->version = 1;
			$todocfile->iteration = 1;
			$todocfile->lifeStage = 'init';
			$todocfile->setCreated(new DateTime());
			$todocfile->setUpdated(new DateTime());
			$todocfile->setCreateBy(CurrentUser::get());
			$todocfile->setUpdateBy(CurrentUser::get());
			$todocfile->setOwner(CurrentUser::get());
			$todocfile->unlock();
			$todocfile->setFrom($docfile);
			$toDocument->addDocfile($todocfile);

			/* CLONE RECORD DATA */
			$todata = clone ($docfile->getData());

			/* rename docfile by replacing fromDocument Name by toDocument Name in docfile name, else rename with md5 */
			$fromDfName = $docfile->getName();
			$toDfName = str_replace($fromNumber, $toNumber, $fromDfName);
			if ( $toDfName == $fromDfName ) {
				$toDfName = uniqid() . $todata->extension;
			}

			if ( $vault->exist($toDfName) ) {
				$conn->rollBack();
				$vault->rollBack();
				throw new \Rbplm\Vault\ExistingDataException(sprintf('Data %s is yet existing in vault %s', $toDfName, $toPath));
			}

			/* COPY FSDATA IN VAULT */
			$fromfsdata = $todata->getFsdata();
			$md5 = null;
			$vault->record($todata, $fromfsdata, $toDfName, $md5);

			/* SET DATA TO DOCFILE */
			$todocfile->setData($todata);

			/* TRY TO SAVE DOCFILE */
			try {
				$todocfile->setName($toDfName)
					->
				//->setUid($toDfName)
				newUid()
					->setNumber($toDfName);
				$dfDao->save($todocfile);
			}
			catch( \Exception $e ) {
				$conn->rollBack();
				$vault->rollBack();
				throw $e;
			}
		}

		$conn->commit();

		/* Notiy observers on event */
		$event->setName($postEventName);
		$event->toDocument = $toDocument;
		$this->getEventManager()->trigger($event);

		// @todo Copy picture file
		// @todo Copy the doclinks
		// @todo Copy the relationship

		return $toDocument;
	}

	/**
	 * Before use as inputs :
	 * 		- a container must be associate to document
	 * 		- a dao must be associate to document and docfile
	 * 
	 *
	 * @param Document\Version $document
	 * @param Docfile\Version $docfile
	 * @param Fsdata $fsdata
	 * @throws AccessCodeException
	 * @return Service
	 */
	public function addFileToDocument(Document\Version $document, Docfile\Version $docfile, Fsdata $fsdata)
	{
		$factory = $this->factory;
		$container = $document->getParent();
		$reposit = Reposit::init($container->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		/* Check access on document */
		$aCode = $document->checkAccess();
		$coBy = $document->lockById;
		if ( !($aCode == AccessCode::FREE or ($aCode == AccessCode::CHECKOUT and $coBy == CurrentUser::get()->getId())) ) {
			throw new AccessCodeException(sprintf('LOCKED_%s_TO_PREVENT_THIS_ACTION', $aCode));
		}

		$preEventName = Event::PRE_ADDFILE;
		$postEventName = Event::POST_ADDFILE;

		/* Signal */
		$event = new Event($preEventName, $document, $factory);
		$this->getEventManager()->trigger($event);

		/* save document if is not */
		if ( !($document->getId() > 0) ) {
			if ( isset($document->dao) ) {
				$document->dao->save($document);
			}
			else {
				$factory->getDao($document->cid)->save($document);
			}
		}

		$docfile->setParent($document);
		$document->addDocfile($docfile);

		/* SAVE RECORDS IN VAULT */
		try {
			$record = $vault->record(Record::init(), $fsdata, $fsdata->getName());
		}
		catch( ExistingException $e ) {
			throw new ExistingException('File is existing in vault', E_USER_ERROR, $e);
		}

		/* set data to docfile */
		$docfile->setData($record);

		/* save docfile */
		if ( isset($docfile->dao) ) {
			$docfile->dao->save($docfile);
		}
		else {
			$factory->getDao($docfile->cid)->save($docfile);
		}

		/* Signal */
		$event->setName($postEventName);
		$this->getEventManager()->trigger($event);

		return $this;
	}

	/**
	 *
	 * @param Document\Version $document
	 * @param string $file
	 *        	Full path to data file to associate
	 * @param string $filename
	 *        	Name of file as must be recorded in vault
	 * @param integer $role
	 *        	Role id of the new docfile
	 *
	 */
	public function associateFile(Document\Version $document, $file, $filename, $role = 8)
	{
		$factory = $this->factory;
		$container = $document->getParent();
		$reposit = Reposit::init($container->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));
		$aCode = $document->checkAccess();
		if ( $aCode >= 100 ) {
			throw new AccessCodeException(sprintf('LOCKED_%s_TO_PREVENT_THIS_ACTION', $aCode));
		}

		$preEventName = Event::PRE_ADDFILE;
		$postEventName = Event::POST_ADDFILE;

		/* Signal */
		$event = new Event($preEventName, $document, $factory);
		$this->getEventManager()->trigger($event);

		try {
			/* init df */
			$fsdata = new Fsdata($file);
			$docfile = Docfile\Version::init();
			$docfile->setName($filename);
			$docfile->dao = $factory->getDao($docfile->cid);

			/* put file in vault */
			$record = $vault->record(Record::init(), $fsdata, $filename);
			$docfile->setParent($document);
			$docfile->setData($record);
			$docfile->setMainrole($role);
			/* save df */
			$docfile->dao->save($docfile);
			/* feedbacks */
			$this->respons->addFeedback('the file %s has been associated to document %s', $record->getName(), $document->getUid());
			$document->addDocfile($docfile);
		}
		catch( \Exception $e ) {
			$msg = sprintf('The file %s can not be associated to the document. Reason: %s', $fsdata->getName(), $e->getMessage());
			$this->respons->addError($msg);
			throw new Exception($msg, 65000, $e);
		}

		/* delete widlspace file */
		$fsdata->delete();

		/* Signal */
		$event->setName($postEventName);
		$this->getEventManager()->trigger($event);

		return $this;
	}
}
