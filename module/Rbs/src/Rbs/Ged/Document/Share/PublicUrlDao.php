<?php
// %LICENCE_HEADER%
namespace Rbs\Ged\Document\Share;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `share_public_url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` VARCHAR(255) NOT NULL,
  `document_id` int(11) DEFAULT NULL,
  `spacename` VARCHAR(32) DEFAULT NULL,
  `document_uid` VARCHAR(255) DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_share_public_url_uid` (`uid`),
  KEY `K_share_public_url_document_id` (`document_id`),
  KEY `K_share_public_url_spacename` (`spacename`),
  KEY `K_share_public_url_document_uid` (`document_uid`),
  KEY `K_share_public_url_expiration_date` (`expiration_date`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE IF EXISTS `share_public_url`;
 <<*/

/**
 */
class PublicUrlDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'share_public_url';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'share_public_url';

	/**
	 * @var string
	 */
	public static $sequenceName = null;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'document_id' => 'documentId',
		'spacename' => 'spacename',
		'document_uid' => 'documentUid',
		'expiration_date' => 'expirationDate',
		'comment' => 'comment'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'expiration_date' => 'datetime'
	);

	/**
	 * @param string $uid
	 * @return PublicUrlDao
	 */
	public function deleteFromUid($uid)
	{
		$toSysId = $this->toSys('uid');
		$filter = "$toSysId=:uid";
		$bind = array(
			':uid' => $uid
		);
		$this->_deleteFromFilter($filter, $bind);
		return $this;
	}

	/**
	 * @param integer $documentId
	 * @param string $spacename
	 * @param \Rbs\Ged\Document\Share\PublicUrl $mapped
	 * @return PublicUrlDao
	 */
	public function loadFromDocumentIdAndSpacename($documentId, $spacename, $mapped)
	{
		$toSysDocumentId = $this->toSys('documentId');
		$toSysSpacename = $this->toSys('spacename');
		$filter = "$toSysDocumentId=:documentId AND $toSysSpacename=:spacename";
		$bind = array(
			':documentId' => $documentId,
			':spacename' => $spacename
		);
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 * @return PublicUrlDao
	 */
	public function cleanup()
	{
		$connexion = $this->connexion;
		$table = $this->_table;
		
		$now = new \DateTime();
		
		/* clean log */
		try {
			$sql = "DELETE FROM $table WHERE expiration_date IS NOT NULL AND expiration_date < :now";
			$cleanupStmt = $connexion->prepare($sql);
			$bind = array(
				':now' => $now->format(self::DATE_FORMAT)
			);
			$this->lastStmt = $cleanupStmt;
			$this->lastBind = $bind;
			$cleanupStmt->execute($bind);
		}
		catch( \Exception $e ) {
			throw $e;
		}
		
		return $this;
	}
}
