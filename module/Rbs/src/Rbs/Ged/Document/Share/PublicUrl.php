<?php
// %LICENCE_HEADER%
namespace Rbs\Ged\Document\Share;

use Rbplm\Any;
use Rbplm\Ged\Document;

/**
 * @brief Job to execute in batch mode.
 */
class PublicUrl extends Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Mapped;
	
	/** @var string */
	static $classId = 'sharepublicurl';

	/** @var string */
	protected $url;

	/** @var Document\Version */
	protected $document;

	/** @var integer */
	public $documentId;

	/** @var string */
	public $spacename;
	
	/** @var string */
	public $documentUid;

	/** @var \DateTime */
	protected $expirationDate;

	/** @var string */
	protected $comment;

	/**
	 * @var string
	 */
	public static $baseUrl = 'document/share/public/';

	/**
	 * 
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * @return PublicUrl
	 */
	public static function init()
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		return $obj;
	}

	/**
	 *
	 * @return string
	 */
	public function newUid()
	{
		$this->uid = bin2hex(openssl_random_pseudo_bytes(20));
		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\MappedInterface::hydrate()
	 * @return PublicUrl
	 */
	public function hydrate(array $properties)
	{
		isset($properties['id']) ? $this->id = $properties['id'] : null;
		isset($properties['uid']) ? $this->uid = $properties['uid'] : null;
		isset($properties['ownerId']) ? $this->ownerId = $properties['ownerId'] : null;
		isset($properties['ownerUid']) ? $this->ownerUid = $properties['ownerUid'] : null;
		isset($properties['documentId']) ? $this->documentId = $properties['documentId'] : null;
		isset($properties['spacename']) ? $this->spacename = $properties['spacename'] : null;
		isset($properties['documentUid']) ? $this->documentUid = $properties['documentUid'] : null;
		isset($properties['url']) ? $this->url = $properties['url'] : null;
		isset($properties['comment']) ? $this->comment = $properties['comment'] : null;
		
		if(isset($properties['expirationDate'])){
			$date = new \DateTime($properties['expirationDate']);
			$this->setExpirationDate($date);
		}
		
		return $this;
	}

	/**
	 *
	 * @param Document\Version $param
	 */
	public function setDocument(Document\Version $document)
	{
		$this->document = $document;
		$this->documentId = $document->getId();
		$this->documentUid = $document->getUid();
		$this->spacename = $document->spacename;
		return $this;
	}

	/**
	 *
	 * @return Document\Version
	 */
	public function getDocument()
	{
		return $this->document;
	}

	/**
	 * 
	 * @param string $url
	 * @return PublicUrl
	 */
	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	/**
	 * 
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 *
	 * @param \DateTime $date
	 * @return PublicUrl
	 */
	public function setExpirationDate(\DateTime $date = null)
	{
		$this->expirationDate = $date;
		return $this;
	}

	/**
	 *
	 * @return \DateTime
	 */
	public function getExpirationDate()
	{
		return $this->expirationDate;
	}

	/**
	 *
	 * @param string $string
	 * @return PublicUrl
	 */
	public function setComment($string)
	{
		$this->comment = trim($string);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}
}
