<?php
namespace Rbs\Ged\Document;

use Rbs\EventDispatcher\ListenerProvider;
use Rbs\Ged\Document\Event as DocumentEvent;
use Rbs\Notification\Listener as NotificationListener;
use Rbs\Ged\Document\History\Listener as HistoryListener;
use Rbs\Observers\CallbackListener;
use Rbs\Observers\DoctypeCallbackListener;

/**
 */
class ListenerProviderFactory
{

	/**
	 * @return ListenerProvider
	 */
	public static function get()
	{
		$listenerProvider = new ListenerProvider();

		$listenerProvider->addListener(function (DocumentEvent $event) {
			$event = NotificationListener::onEvent($event);
			return $event;
		});

		$listenerProvider->addListener(function (DocumentEvent $event) {
			$event = HistoryListener::onEvent($event);
			return $event;
		});

		$listenerProvider->addListener(function (DocumentEvent $event) {
			$event = CallbackListener::onEvent($event);
			return $event;
		});

		$listenerProvider->addListener(function (DocumentEvent $event) {
			$event = DoctypeCallbackListener::onEvent($event);
			return $event;
		});

		$listenerProvider->addListener(function (\Rbs\EventDispatcher\Event $event) {
			\Ranchbe::get()->log(sprintf('Event %s type %s is emitted by object class %s', $event->getName(), get_class($event), get_class($event->getEmitter())));
			return $event;
		});

		/*
		 $listenerProvider->addListener(function (DocumentEvent $event) {
		 $checkoutIndexer = $event->getFactory()->getDao('checkoutindex');
		 $checkoutIndexer->onEvent($event);
		 return $event;
		 });
		 */

		return $listenerProvider;
	}
}
