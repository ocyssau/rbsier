<?php
//%LICENCE_HEADER%
namespace Rbs\Ged\Document;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * Read Only DAO
 */
class PdmLinkDao extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table = 'workitem_documents';

	/**
	 * @var string
	 */
	public static $vtable = 'workitem_documents';

	/**
	 * @var string
	 */
	public static $sequenceName = '';

	/**
	 * @var string
	 */
	public static $parentTable = 'pdm_product_version';

	/**
	 * @var string
	 */
	public static $parentForeignKey = 'document_id';

	/**
	 * @var string
	 */
	public static $childTable = 'pdm_product_instance';

	/**
	 * @var string
	 */
	public static $childForeignKey = '';

	/**
	 * @var array
	 */
	public static $sysToApp = array();

	/**
	 * @var \PDOStatement
	 */
	protected $loadChildrenStmt;

	/**
	 * @var \PDOStatement
	 */
	protected $loadParentStmt;

	/**
	 *
	 * @param \Rbplm\Dao\Connexion $conn
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		
		$this->_childTable = static::$childTable;
		$this->_childForeignKey = static::$childForeignKey;
		$this->_parentTable = static::$parentTable;
		$this->_parentForeignKey = static::$parentForeignKey;
	}

	/**
	 * Documents join to instances children of product.
	 * 
	 * Some alias are defined to be used in filters :
	 * <ul>
	 * * <li>.parent alias for parent product version</li>
	 * * <li>.inst alias for children products instances</li>
	 * * <li>.prod alias for children products versions</li>
	 * * <li>.doc alias children documents</li>
	 * </ul>
	 * 
	 * @example
	 *  Return all documents children of document identified by $parentDocumentId :
	 * 	$filter->andfind($parentDocumentId, 'parent.document_id', Op::EQUAL)
	 * 
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return \PDOStatement
	 */
	public function getChildren($filter = null, $bind = array())
	{
		$instanceTable = $this->_childTable;
		$productTable = $this->_parentTable;
		$documentTable = $this->_table;
		
		if ( !$this->loadChildrenStmt ) {
			if ( $filter ) {
				$filterStr = $filter->__toString();
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
			}
			else {
				$filterStr = '1=1';
				$selectStr = 'doc.*';
			}
			
			/* get children instance */
			//$sql = "SELECT * FROM pdm_product_instance";
			//$sql = "WHERE parent_id = (SELECT id FROM pdm_product_version WHERE document_id=:parentId)";
			
			$sql = "
			SELECT $selectStr FROM $productTable as parent
			LEFT OUTER JOIN $instanceTable as inst ON inst.parent_id=parent.id
			LEFT OUTER JOIN $productTable as prod ON inst.of_product_id=prod.id
			LEFT OUTER JOIN $documentTable as doc ON prod.document_id=doc.id
			WHERE $filterStr";
			
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadChildrenStmt = $stmt;
		}
		
		$this->loadChildrenStmt->execute($bind);
		return $this->loadChildrenStmt;
	}

	/**
	 * Documents join to product children of product.
	 * 
	 * Some alias are defined to be used in filters :
	 * <ul>
	 * * <li>.child alias for child ProductVersions</li>
	 * * <li>.inst alias for Instances of child ProductVersions</li>
	 * * <li>.prod alias for parents ProductsVersions of the children Instances</li>
	 * * <li>.doc alias children Documents</li>
	 * </ul>
	 * 
	 * @example
	 *  Return all document that use the document identified by $documentId :
	 * 	$filter->andfind($documentId, 'child.document_id', Op::EQUAL)  
	 * 
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return \PDOStatement
	 */
	function getParents($filter = null, $bind = array())
	{
		$instanceTable = $this->_childTable;
		$productTable = $this->_parentTable;
		$documentTable = $this->_table;
		
		if ( !$this->loadParentStmt ) {
			if ( $filter ) {
				$filterStr = $filter->__toString();
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
			}
			else {
				$filterStr = '1=1';
				$selectStr = 'doc.*';
			}
			
			$sql = "
			SELECT $selectStr FROM $productTable as child
			LEFT OUTER JOIN $instanceTable as inst ON inst.of_product_id=child.id
			LEFT OUTER JOIN $productTable as prod ON inst.parent_id=prod.id
			LEFT OUTER JOIN $documentTable as doc ON prod.document_id=doc.id
			WHERE $filterStr";
			
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadParentStmt = $stmt;
		}
		
		$this->loadParentStmt->execute($bind);
		return $this->loadParentStmt;
	}
}
