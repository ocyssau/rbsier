<?php
// %LICENCE_HEADER%
namespace Rbs\Ged\Document;

use Rbplm\Dao\Connexion;
use Rbplm\Dao\NotExistingException;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Dao\FilterInterface;

/** SQL_SCRIPT >>
 CREATE TABLE IF NOT EXISTS `docbasket` (
 `user_id` INT(11) NOT NULL,
 `document_id` INT(11) NOT NULL,
 `space_id` INT(11) NOT NULL,
 PRIMARY KEY (`user_id`, `document_id`, `space_id`)
 ) ENGINE = MyISAM DEFAULT CHARACTER SET = utf8;
 << */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */

/**
 * 
 *
 */
class BasketDao
{
	
	/**
	 *
	 * @var \PDO
	 */
	protected $connexion;
	
	/**
	 *
	 * @var string
	 */
	protected $usrId;
	
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 *
	 * @var string
	 */
	public static $table = 'docbasket';
	
	/**
	 *
	 * @var string
	 */
	protected $_table;
	
	/**
	 * @var \PDOStatement
	 */
	protected $insertStmt;
	
	/**
	 * @var \PDOStatement
	 */
	protected $deleteStmt;
	
	/**
	 * @var \PDOStatement
	 */
	protected $cleanStmt;
	
	/**
	 *
	 * @param \Rbplm\People\User $user
	 */
	public function __construct($user, $factory)
	{
		$this->connexion = Connexion::get();
		$this->_table = self::$table;
		$this->usrId = $user->getId();
		$this->factory = $factory;
	}
	
	/**
	 * Return list of document ids of user box
	 *
	 * @param string|\Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return array
	 */
	public function get($filter = null, $bind = array())
	{
		$dao = $this->factory->getDao(DocumentVersion::$classId);
		
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = "doc.`$asSys` AS `$asApp`";
		}
		$select[] = 'ubox.user_id as userId';
		$select = implode($select, ', ');
		
		$sql = "SELECT * FROM (";
		$sql .= "(SELECT $select FROM docbasket AS ubox";
		$sql .= " JOIN workitem_documents AS doc ON doc.id = ubox.document_id AND ubox.space_id=10)";
		$sql .= " UNION";
		$sql .= "(SELECT $select FROM docbasket AS ubox";
		$sql .= " JOIN bookshop_documents AS doc ON doc.id = ubox.document_id AND ubox.space_id=20)";
		$sql .= " UNION";
		$sql .= "(SELECT $select FROM docbasket AS ubox";
		$sql .= " JOIN cadlib_documents AS doc ON doc.id = ubox.document_id AND ubox.space_id=25)";
		$sql .= ") as basket_documents";
		$sql .= " WHERE userId =:userId";
		
		$filterAsStr = '1=1';
		if ( $filter instanceof FilterInterface ) {
			$filterAsStr = $filter->__toString();
		}
		elseif ( $filter && is_string($filter) ) {
			$filterAsStr = $filter;
		}
		
		$sql .= ' AND '.$filterAsStr;
		
		$bind[':userId'] = $this->usrId;
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		$ret = $stmt->fetchAll();
		if ( $ret ) {
			return $ret;
		}
		else {
			throw new NotExistingException(sprintf('CAN_NOT_BE_LOAD_FROM_%s', $sql));
		}
	}
	
	/**
	 *
	 * @param string|\Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return integer
	 */
	public function countAll($filter = null, $bind = array())
	{
		$table = $this->_table;
		$filterAsStr = '1=1';
		
		if ( $filter instanceof FilterInterface ) {
			$filterAsStr = $filter->__toString();
		}
		elseif ( $filter && is_string($filter) ) {
			$filterAsStr = $filter;
		}
		
		$sql = "SELECT COUNT(*) FROM $table WHERE user_id =:userId AND $filterAsStr";
		$stmt = $this->connexion->prepare($sql);
		$bind[':userId'] = $this->usrId;
		$stmt->execute($bind);
		$count = $stmt->fetchColumn(0);
		return $count;
	}
	
	/**
	 *
	 * Add a index to user box
	 *
	 * @param integer $documentId
	 * @param integer $spaceId
	 * @return boolean
	 */
	public function add($documentId, $spaceId)
	{
		($this->connexion->inTransaction() == true) ? $withTrans = false : $withTrans=true;
		
		$table = $this->_table;
		$data = array(
			'user_id' => ':userId',
			'document_id' => ':documentId',
			'space_id' => ':spaceId'
		);
		
		$bind = array(
			':userId' => $this->usrId,
			':documentId' => $documentId,
			':spaceId' => $spaceId
		);
		
		if ( $withTrans ) $this->connexion->beginTransaction();
		try {
			// Init statement
			if ( !$this->insertStmt ) {
				foreach( $data as $asSys => $asApp ) {
					$sysSet[] = $asSys;
					$appSet[] = $asApp;
				}
				
				$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
				$this->insertStmt = $this->connexion->prepare($sql);
			}
			
			$this->insertStmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
	}
	
	/**
	 *
	 * Remove a document from user box
	 *
	 * @param integer $documentId
	 * @param integer $spaceId
	 * @return boolean
	 */
	public function remove($documentId, $spaceId)
	{
		$table = $this->_table;
		$userId = $this->usrId;
		
		($this->connexion->inTransaction() == true) ? $withTrans = false : $withTrans = true;
		if ( $withTrans ) $this->connexion->beginTransaction();
		
		if ( !$this->deleteStmt ) {
			$sql = "DELETE FROM $table WHERE user_id=:userId AND document_id=:documentId AND space_id=:spaceId";
			$this->deleteStmt = $this->connexion->prepare($sql);
		}
		
		$bind = array(
			':userId' => $userId,
			':documentId' => $documentId,
			':spaceId' => $spaceId
		);
		
		try {
			$this->deleteStmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		
		return $this;
	}
	
	/**
	 *
	 * Clean all content of user box
	 *
	 * @param integer $documentId
	 * @param integer $spaceId
	 * @return boolean
	 */
	public function clean()
	{
		$table = $this->_table;
		$userId = $this->usrId;
		
		($this->connexion->inTransaction() == true) ? $withTrans = false : $withTrans = true;
		if ( $withTrans ) $this->connexion->beginTransaction();
		
		if ( !$this->cleanStmt ) {
			$sql = "DELETE FROM $table WHERE user_id=:userId";
			$this->cleanStmt = $this->connexion->prepare($sql);
		}
		
		$bind = array(
			':userId' => $userId
		);
		
		try {
			$this->cleanStmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		
		return $this;
	}
}
