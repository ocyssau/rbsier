<?php
namespace Rbs\Ged\Document;

use Rbplm\Dao\Connexion;

/**
 * SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `document_indice` (
	`indice_id` int(11) NOT NULL DEFAULT '0',
	`indice_value` VARCHAR(8) NOT NULL DEFAULT '',
	PRIMARY KEY (`indice_id`),
	UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB;
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**SQL_TRIGGER>>
DROP PROCEDURE IF EXISTS `populateIndices`;
DELIMITER $$
CREATE PROCEDURE populateIndices()
BEGIN
	DECLARE i INT DEFAULT 1;
	DECLARE label VARCHAR(12);
    
   	TRUNCATE TABLE `document_indice`;

	WHILE i < 100 DO
        SET label = CONCAT('SI',LPAD(i, 2, '0'));
        INSERT INTO `document_indice` (`indice_id`, `indice_value`) VALUES (i, label);
   		SET i = i + 1;
	END WHILE;
END$$
DELIMITER ;
<< */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */
class IndiceDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'document_indice';

	public static $vtable = 'document_indice';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'indice_id' => 'id',
		'indice_value' => 'name'
	);

	/**
	 * Constructor
	 *
	 * @param
	 *        	\PDO
	 */
	public function __construct($conn = null)
	{
		if ( $conn ) {
			$this->connexion = $conn;
		}
		else {
			$this->connexion = Connexion::get();
		}

		$this->metaModel = static::$sysToApp;
		$this->_table = static::$table;
	}

	/**
	 * Return an array indice_id=>indice_number if success, false else.
	 *
	 * @param
	 *        	$filters
	 * @return \PDOStatement
	 */
	function getIndices($filters = null)
	{
		$sql = 'SELECT indice_id AS id, indice_value AS name FROM ' . $this->_table;
		if ( $filters ) {
			$sql .= ' WHERE ' . $filters;
		}

		$stmt = $this->connexion->query($sql);
		return $stmt;
	}

	/**
	 *
	 * @see Rbplm\Dao.DaoInterface::toSys() Translate current row data from model to db semantic
	 *
	 * @param array|string $in
	 *        	Application property name or array (key as model property name => value)
	 * @return array System attributs
	 */
	public static function toSys($in)
	{
		$out = array();
		$translator = array_flip(static::$sysToApp);
		if ( is_array($in) ) {
			foreach( $in as $appName => $value ) {
				if ( isset($translator[$appName]) ) {
					$out[$translator[$appName]] = $value;
				}
				else {
					$out[$appName] = $value;
				}
			}
		}
		else {
			if ( isset($translator[$in]) ) {
				$out = $translator[$in];
			}
			else {
				$out = $in;
			}
		}
		return $out;
	}

	/**
	 *
	 * @see Rbplm\Dao.DaoInterface::toApp() Translate property name and value to model semantic
	 *
	 * @param array|string $in
	 *        	Db properties name or array (key as db column name => value)
	 * @return array Model properties
	 */
	public static function toApp($in)
	{
		$translator = static::$sysToApp;
		if ( is_array($in) ) {
			$out = array();
			foreach( $in as $asSys => $value ) {
				if ( isset($translator[$asSys]) ) {
					$out[$translator[$asSys]] = $value;
				}
				else {
					$out[$asSys] = $value;
				}
			}
		}
		else {
			if ( isset($translator[$in]) ) {
				$out = $translator[$in];
			}
			else {
				$out = $in;
			}
		}
		return $out;
	}
}