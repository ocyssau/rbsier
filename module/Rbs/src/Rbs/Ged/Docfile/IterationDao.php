<?php
//%LICENCE_HEADER%

namespace Rbs\Ged\Docfile;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class IterationDao extends VersionDao
{
	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'document_id'=>'parentId',
		'name'=>'name',
		'iteration'=>'iteration',
		'acode'=>'accessCode',
		'locked'=>'locked',
		'lock_by_id'=>'lockById',
		'created'=>'created',
		'create_by_id'=>'createById',
		'updated'=>'updated',
		'update_by_id'=>'updateById',
		'life_stage'=>'lifeStage',
		'path'=>'path',
		'root_name'=>'rootname',
		'extension'=>'extension',
		'type'=>'type',
		'size'=>'size',
		'mtime'=>'mtime',
		'md5'=>'md5',
		'mainrole'=>'mainrole',
		'roles'=>'roles',
		'of_file_id'=>'ofDocfileId',
	);
}
