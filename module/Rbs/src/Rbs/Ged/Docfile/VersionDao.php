<?php
//%LICENCE_HEADER%
namespace Rbs\Ged\Docfile;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends DaoSier
{

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'document_id' => 'parentId',
		'document_uid' => 'parentUid',
		'name' => 'name',
		'iteration' => 'iteration',
		'acode' => 'accessCode',
		'locked' => 'locked',
		'lock_by_id' => 'lockById',
		'lock_by_uid' => 'lockByUid',
		'created' => 'created',
		'create_by_id' => 'createById',
		'create_by_uid' => 'createByUid',
		'updated' => 'updated',
		'update_by_id' => 'updateById',
		'update_by_uid' => 'updateByUid',
		'life_stage' => 'lifeStage',
		'path' => 'path',
		'root_name' => 'rootname',
		'extension' => 'extension',
		'type' => 'type',
		'size' => 'size',
		'mtime' => 'mtime',
		'md5' => 'md5',
		'mainrole' => 'mainrole',
		'roles' => 'roles'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created' => 'datetime',
		'locked' => 'datetime',
		'updated' => 'datetime',
		'mtime' => 'datetime',
		'roles' => 'json'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function save(MappedInterface $mapped)
	{
		$parent = $mapped->getParent();
		if ( $parent ) {
			$mapped->parentId = $parent->getId();
		}
		
		$updateBy = $mapped->getUpdateBy();
		if ( $updateBy ) {
			$mapped->updateById = $updateBy->getId();
			$mapped->updateByUid = $updateBy->getUid();
		}
		
		$createBy = $mapped->getCreateBy();
		if ( $createBy ) {
			$mapped->createById = $createBy->getId();
			$mapped->createByUid = $createBy->getUid();
		}
		
		$lockBy = $mapped->getLockBy();
		if ( $lockBy ) {
			$mapped->lockById = $lockBy->getId();
			$mapped->lockByUid = $lockBy->getUid();
		}
		
		$record = $mapped->getData();
		$mapped->name = $record->name;
		$mapped->extension = $record->extension;
		$mapped->fullpath = $record->fullpath;
		$mapped->md5 = $record->md5;
		$mapped->mtime = $record->mtime;
		$mapped->path = $record->path;
		$mapped->rootname = $record->rootname;
		$mapped->size = $record->size;
		$mapped->type = $record->type;
		
		parent::save($mapped);
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbplm\Ged\Docfile\Version    	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function hydrate($mapped, array $row, $fromApp = false)
	{
		$properties = array();
		
		/* @var Rbs\Dao\Sier\MetamodelTranslator */
		$translator = $this->getTranslator();
		
		if ( $fromApp ) {
			$mapped->hydrate($row);
			$mapped->getData()->hydrate($row);
		}
		else {
			$sysToAppFilter = $this->metaModelFilters;
			foreach( $this->metaModel as $asSys => $asApp ) {
				if ( isset($sysToAppFilter[$asSys]) ) {
					$value = $row[$asSys];
					$filterMethod = $sysToAppFilter[$asSys] . 'ToApp';
					$properties[$asApp] = $translator->$filterMethod($value);
				}
				else {
					$properties[$asApp] = $row[$asSys];
				}
			}
			$properties['fullpath'] = $properties['path'] . '/' . $properties['rootname'] . $properties['extension'];
			$mapped->hydrate($properties);
			$mapped->getData()->hydrate($properties);
		}
		
		/*
		 $record->setId($row['file_id']);
		 $record->setUid($row['file_id']);
		 $record->setName($row['file_root_name'].$row['file_extension']);
		 $record->created = $row['created'];
		 $record->extension 	= $row['file_extension'];
		 $record->path 		= $row['file_path'];
		 $record->rootname 	= $row['file_root_name'];
		 $record->fullpath 	= $row['file_path'].'/'.$row['file_root_name'].$row['file_extension'];
		 $record->size 		= $row['file_size'];
		 $record->mtime 		= $row['file_mtime'];
		 $record->type 		= $row['file_type'];
		 $record->md5 		= $row['file_md5'];
		 $mapped->uid = $mapped->id;
		 */
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\Mysql::loadFromName()
	 * 
	 * @param MappedInterface $mapped
	 * @param string $name
	 * @param int $iterationId [OPTIONAL] If not set, return the last
	 * @param int $parentId [OPTIONAL] return docfile of document id=$parentId
	 * 
	 */
	public function loadFromName(MappedInterface $mapped, $name, $iterationId = null, $parentId = null)
	{
		$filter = 'name=:name';
		$bind = array(
			':name' => $name
		);
		if ( $iterationId ) {
			$filter = $filter . ' AND iteration=:iterationId';
			$bind[':iterationId'] = $iterationId;
		}
		if ( $parentId ) {
			$filter = $filter . ' AND document_id=:documentId';
			$bind[':documentId'] = $parentId;
		}
		$filter = $filter . ' ORDER BY iteration DESC LIMIT 1';
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 *
	 * @param integer $documentId
	 * @return \PDOStatement
	 */
	public function getFromParent($parentId)
	{
		$table = $this->_table;
		$sql = "SELECT * FROM $table WHERE document_id=:documentId";
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		
		$bind = array(
			':documentId' => $parentId
		);
		$stmt->execute($bind);
		return $stmt;
	}
	
	/**
	 * @param \Rbplm\Ged\Document\Version
	 */
	public function loadDocfiles($document, $filter = '1=1', $bind = array())
	{
		$table = $this->_table;
		$sql = "SELECT * FROM $table WHERE $filter AND document_id=:documentId ORDER BY mainrole,document_id ASC";
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		
		$bind[':documentId'] = $document->getId();
		$stmt->execute($bind);
		
		while( $entry = $stmt->fetch() ) {
			$docfile = new \Rbplm\Ged\Docfile\Version();
			$this->hydrate($docfile, $entry);
			$docfile->dao = $this;
			$document->addDocfile($docfile);
		}
		return $this;
	}
}
