<?php
namespace Rbs\Ged\Docfile;

use Rbs\EventDispatcher\ListenerProvider;
//use Rbs\Ged\Docfile\Event as DocfileEvent;

/**
 */
class ListenerProviderFactory
{

	/**
	 * @return ListenerProvider
	 */
	public static function get()
	{
		$listenerProvider = new ListenerProvider();
		return $listenerProvider;
	}
}
