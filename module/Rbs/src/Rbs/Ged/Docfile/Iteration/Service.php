<?php
namespace Rbs\Ged\Docfile\Iteration;

use Rbplm\Ged\Docfile;
use Rbplm\Sys\Exception;
use Rbs\Space\Factory as DaoFactory;

/**
 *
 *
 */
class Service
{
	/**
	 * 
	 * @var DaoFactory
	 */
	protected $factory;

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function __construct(DaoFactory $factory)
	{
		$this->factory = $factory;
	}

	/**
	 * @param Docfile\Version $docfile
	 * @throws Exception
	 * @return Service
	 */
	public function delete(Docfile\Iteration $docfile, $withfiles=false)
	{
		$factory = $this->factory;
		$dao = $factory->getDao($docfile->cid);

		/* Init Record and Fsdata */
		$record = $docfile->getData();
		$fsdata = $record->getFsdata();

		/* Delete record of associated files */
		$dao->deleteFromId($docfile->getId());

		if($withfiles === true){ /* Delete the file if $withfiles = true */
			if( $fsdata->isExisting() ){ /* Delete the file if $withfiles = true */
				if( !$fsdata->putInTrash() ){
					throw new Exception("Can not delete file ".$fsdata->getFullpath());
				}
			}
		}

		return $this;
	}
} /* End of class */
