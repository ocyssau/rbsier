<?php
namespace Rbs\Ged\Docfile;

use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbs\Space\Factory as DaoFactory;
use Rbs\EventDispatcher\StoppableEvent;
use Rbs\History\HistoryEventInterface;

/**
 * VERY generic to see if a task can be stopped
 */
class Event extends StoppableEvent implements HistoryEventInterface
{
	use \Rbs\History\EventTrait;

	const PRE_CREATE = 'create.pre';

	const POST_CREATE = 'create.post';
	
	const PRE_CHECKOUT = 'checkout.pre';
	
	const POST_CHECKOUT = 'checkout.post';
	
	const PRE_CHECKIN = 'checkin.pre';
	
	const POST_CHECKIN = 'checkin.post';
	
	const PRE_UPDATE = 'update.pre';
	
	const POST_UPDATE = 'update.post';
	
	const PRE_DELETE = 'delete.pre';
	
	const POST_DELETE = 'delete.post';
	
	const PRE_DELETE_ITERATION = 'delete-iteration.pre';
	
	const POST_DELETE_ITERATION = 'delete-iteration.post';
	
	const PRE_PROMUTE = 'promute.pre';
	
	const POST_PROMUTE = 'promute.post';
	
	/* check if are used */
	const PRE_CANCELCHECKOUT = 'cancelcheckout.pre';

	const POST_CANCELCHECKOUT = 'cancelcheckin.post';

	const PRE_COPY = 'copy.pre';

	const POST_COPY = 'copy.post';

	const PRE_MOVE = 'move.pre';

	const POST_MOVE = 'move.post';

	const PRE_LOCK = 'lock.pre';

	const POST_LOCK = 'lock.post';

	const PRE_UNLOCK = 'unlock.pre';

	const POST_UNLOCK = 'unlock.post';

	const PRE_NEWVERSION = 'newversion.pre';

	const POST_NEWVERSION = 'newversion.post';

	const PRE_REPLACEFILE = 'replacefile.pre';

	const POST_REPLACEFILE = 'replacefile.post';

	/**
	 *
	 * @var DaoFactory
	 */
	protected $factory;

	/**
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * 
	 * @param string $name
	 * @param DocfileVersion $docfile
	 * @param DaoFactory $factory
	 */
	public function __construct(string $name, DocfileVersion $docfile, DaoFactory $factory)
	{
		$this->setName($name);
		$this->emitter = $docfile;
		$this->factory = $factory;
	}

	/**
	 *
	 * @return DaoFactory
	 */
	public function getFactory(): DaoFactory
	{
		return $this->factory;
	}
}
