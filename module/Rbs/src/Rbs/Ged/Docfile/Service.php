<?php
namespace Rbs\Ged\Docfile;

use Rbs\Ged\Docfile\Service\Exception;
use Rbplm\Ged\AccessCodeException;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\AccessCode;
use Rbs\Ged\Historize;
use Rbplm\Sys\Error;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;
use Rbs\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;
use Rbs\Dao\Sier\Filter;
use Rbplm\Dao\Filter\Op;
use Rbs\Ged\Docfile\Service\WsCleanupFailed;
use Rbs\EventDispatcher\EventManager;

/**
 *
 *
 */
class Service
{

	protected $factory;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
		$factory->docfileService = $this;
	}

	/**
	 *
	 * @param EventManager $eventManager
	 */
	public function setEventManager(EventManager $eventManager)
	{
		$this->eventManager = $eventManager;
		return $this;
	}

	/**
	 *
	 * @return EventManager
	 */
	public function getEventManager()
	{
		if ( !isset($this->eventManager) ) {
			/* construct a fake EventManager to prevent Exceptions */
			$this->eventManager = new EventManager();
		}
		return $this->eventManager;
	}

	/**
	 * @param Docfile\Version $docfile
	 * @param Vault $vault
	 * @param boolean $keepCo If true, Docfile is saved with checkouted status
	 *
	 * @throws Exception
	 * @return \Rbs\Ged\Docfile\Service
	 *
	 */
	public function create($docfile, $vault, $keepCo = false)
	{
		$factory = $this->factory;
		$currentUser = CurrentUser::get();

		if ( !isset($docfile->fsdata) ) {
			throw new Exception('$docfile->fsdata is not set; Set fsdata with the fsdata to store in vault');
		}

		$fsdata = $docfile->fsdata;
		if ( !isset($docfile->fsdata) ) {
			throw new \Exception('$docfile->fsdata is not set');
		}

		$docfile->setName($fsdata->getName());
		$docfile->newUid();

		/* Signal */
		$event = new Event(Event::PRE_CREATE, $docfile, $factory);
		$this->getEventManager()->trigger($event);

		$record = $vault->record(Record::init(), $fsdata, $docfile->getName());
		$docfile->setData($record);
		$dfDao = $factory->getDao($docfile->cid);
		if ( $keepCo ) {
			$docfile->lock(AccessCode::CHECKOUT, $currentUser);
		}
		$dfDao->save($docfile);

		if ( $keepCo == false ) {
			try {
				$fsdata->delete();
			}
			catch( \Exception $e ) {
				$this->errors[] = $e->getMessage();
				null;
			}
		}

		/* Signal */
		$event = new Event(Event::POST_CREATE, $docfile, $factory);
		$this->getEventManager()->trigger($event);

		return $this;
	}

	/**
	 *
	 * @param Docfile\Version $docfile
	 * @param bool $replace			replace file in ws
	 * @param bool $getFiles		copy file in ws
	 * @throws Exception
	 * @return \Rbs\Ged\Docfile\Service
	 */
	public function checkout($docfile, $replace = true, $getFiles = true)
	{
		$currentUser = CurrentUser::get();
		$wilspace = Wildspace::get($currentUser);

		/* check if access is free, valide codes : 0-1 */
		$aCode = $docfile->checkAccess();
		if ( $aCode != AccessCode::FREE ) {
			throw new AccessCodeException(sprintf('LOCKED_%s_TO_PREVENT_THIS_ACTION', $aCode));
		}
		else {
			$docfile->lock(AccessCode::CHECKOUT, $currentUser);
		}

		/* Signal */
		$event = new Event(Event::PRE_CHECKOUT, $docfile, $this->factory);
		$this->getEventManager()->trigger($event);

		/* get list of associated files and checkout */
		if ( $getFiles ) {
			$data = $docfile->getData();
			$wsFile = $wilspace->getPath() . '/' . $data->getName();
			/* COPY TO WS */
			if ( is_file($wsFile) || is_dir($wsFile) ) {
				if ( $replace ) {
					$data->getFsdata()->copy($wsFile, 0766, true);
				}
			}
			else {
				$data->getFsdata()->copy($wsFile, 0766, false);
			}
		}

		$docfile->dao->save($docfile);

		/* Signal */
		$event->setName(Event::POST_CHECKOUT);
		$this->getEventManager()->trigger($event);

		return $this;
	}

	/**
	 * Replace a file checkout in the vault and unlock it.
	 *
	 * The checkIn copy file from the wildspace to vault reposit dir
	 * If the file has been changed(check by md5 code comparaison), create a new iteration
	 * $docfile->fsdata must be set for update data in vault from this fsdata.
	 *
	 * @param Record $docfile			The docfile to update. Must have a fsdata property to update data.
	 * @param boolean $releasing		if true, release the docfile after replace
	 * @param boolean $updateData		if true, the file in vault will be replaced by new data
	 * @param boolean $checkAccess		if true, check if access code is right
	 * @throws Exception
	 * @return Service
	 */
	public function checkin($docfile, $releasing = true, $updateData = true, $checkAccess = true)
	{
		$factory = $this->factory;

		$aCode = $docfile->checkAccess();
		$coBy = $docfile->lockById;
		$currentUser = CurrentUser::get();
		if ( ($aCode != 1 or $coBy != $currentUser->getId()) and $checkAccess ) {
			throw new AccessCodeException(sprintf('LOCKED_%s_TO_PREVENT_THIS_ACTION', $aCode . ' by ' . $coBy));
		}

		/* Signal */
		$event = new Event(Event::PRE_CHECKIN, $docfile, $factory);
		$this->getEventManager()->trigger($event);

		/* Update data vault */
		if ( $updateData ) {
			/* If no fsdata setted, reset docfile and throw exception */
			if ( !isset($docfile->fsdata) ) {
				if ( $releasing ) {
					$docfile->unLock();
				}
				$docfile->dao->save($docfile);
				throw new Service\NothingToUpdate('$docfile->fsdata is not set. Is must be set with the new fsdata');
			}

			$container = $docfile->getParent()->getParent();
			$reposit = Reposit::init($container->path);
			$vault = new Vault($reposit, $factory->getDao(Record::$classId));
			$historize = new Historize($vault, $factory);

			/* the docfile.fsdata property must ne set in the caller. fsdata is the data to save in vault */
			$fsdata = $docfile->fsdata;

			/* If md5 are equals, dont update files, dont increment iteration */
			if ( $fsdata->getMd5() == $docfile->getData()->md5 ) {
				if ( $releasing ) {
					$docfile->unLock();
					$docfile->dao->save($docfile);
				}
			}
			else {
				/* Copy old data as iteration */
				$iteration = $historize->depriveDocfileToIteration($docfile, $docfile->iteration);
				$iteration->dao = $factory->getDao(\Rbplm\Ged\Docfile\Iteration::$classId);
				$iteration->dao->save($iteration);

				/* Put new data in vault and attach to docfile */
				$record = $vault->record($docfile->getData(), $fsdata, $docfile->getName());
				$docfile->setData($record);
				$docfile->setUpdateBy($currentUser);
				$docfile->setUpdated(new \DateTime());
				$docfile->iteration = $docfile->iteration + 1;

				if ( $releasing ) {
					$docfile->unLock();
				}

				$docfile->dao->save($docfile);
			}
		}
		/* Reset docfile */
		else {
			if ( $releasing ) {
				$docfile->unLock();
				$docfile->dao->save($docfile);
			}
		}

		/* Signal */
		$event->setName(Event::POST_CHECKIN);
		$this->getEventManager()->trigger($event);

		return $this;
	}

	/**
	 *
	 * @param Docfile\Version $docfile
	 * @param boolean $releasing
	 * @param boolean $deleteFileInWs
	 * @param boolean $checkAccess
	 */
	public function checkinFromWs($docfile, $releasing = true, $updateData = true, $deleteFileInWs = true, $checkAccess = false)
	{
		/* Get fsdata in wildspace */
		$wildspace = Wildspace::get(CurrentUser::get());
		$fileName = $docfile->getData()->getName();
		$wsFile = $wildspace->getPath() . '/' . $fileName;

		if ( is_file($wsFile) || is_dir($wsFile) ) {
			$fsdata = new Fsdata($wsFile);
			$docfile->fsdata = $fsdata;
			$this->checkin($docfile, $releasing, $updateData, $checkAccess);

			if ( $releasing == true && $deleteFileInWs == true ) {
				try {
					$fsdata->delete();
				}
				catch( \Exception $e ) {
					throw new WsCleanupFailed($e->getMessage(), 1010, $e);
				}
			}
		}
		else {
			throw new Exception(sprintf('Unable to find file %s', $wsFile), Error::ERROR);
		}
	}

	/**
	 * Update vault data.
	 *
	 * The checkIn copy file from the wildspace to vault reposit dir
	 * If the file has been changed(check by md5 code comparaison), create a new iteration
	 * $docfile->fsdata must be set for update data in vault from this fsdata.
	 *
	 * @param Record $docfile			The docfile to update. Must have a fsdata property to update data.
	 * @param boolean $checkAccess		if true, check if access code is right
	 * @throws Exception
	 * @return Service
	 */
	public function update($docfile, $checkAccess = true)
	{
		$factory = $this->factory;

		$aCode = $docfile->checkAccess();
		$coBy = $docfile->lockById;
		$currentUser = CurrentUser::get();
		if ( $aCode == AccessCode::CHECKOUT ) {
			if ( ($coBy != CurrentUser::get()->getId()) and $checkAccess ) {
				throw new AccessCodeException(sprintf('You have not access to this docfile locked By %s', $coBy));
			}
		}

		/* Signal */
		$event = new Event(Event::PRE_UPDATE, $docfile, $factory);
		$this->getEventManager()->trigger($event);

		/* Update data vault */
		/* If no fsdata setted, reset docfile and throw exception */
		if ( !isset($docfile->fsdata) ) {
			throw new Service\NothingToUpdate('$docfile->fsdata is not set. Is must be set with the new fsdata');
		}

		/* The docfile.fsdata property must ne set in the caller. fsdata is the data to save in vault */
		$fsdata = $docfile->fsdata;
		$container = $docfile->getParent()->getParent();
		$reposit = Reposit::init($container->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));
		$historize = new Historize($vault, $factory);

		/* If md5 are equals, dont update files, dont increment iteration */
		if ( $fsdata->getMd5() != $docfile->getData()->md5 ) {
			/* Copy old data as iteration */
			$historize->depriveDocfileToIteration($docfile, $docfile->iteration);

			/* Put new data in vault and attach to docfile */
			$record = $vault->record($docfile->getData(), $fsdata, $docfile->getName());
			$docfile->setData($record);
			$docfile->setUpdateBy($currentUser);
			$docfile->setUpdated(new \DateTime());
			$docfile->iteration = $docfile->iteration + 1;
			$docfile->dao->save($docfile);
		}

		$this->getEventManager()->trigger($event->setName(Event::POST_UPDATE));

		return $this;
	}

	/**
	 *
	 * @param Docfile\Version $docfile
	 * @param boolean $deleteFileInWs
	 * @param boolean $checkAccess
	 */
	public function updateFromWs($docfile, $deleteFileInWs = false, $checkAccess = true)
	{
		/* Get fsdata in wildspace */
		$wildspace = Wildspace::get(CurrentUser::get());
		$fileName = $docfile->getData()->getName();
		$wsFile = $wildspace->getPath() . '/' . $fileName;

		if ( is_file($wsFile) || is_dir($wsFile) ) {
			$fsdata = new Fsdata($wsFile);
			$docfile->fsdata = $fsdata;
			$this->update($docfile, $checkAccess);

			if ( $deleteFileInWs == true ) {
				try {
					$fsdata->delete();
				}
				catch( \Exception $e ) {
					throw new WsCleanupFailed($e->getMessage(), 1010, $e);
				}
			}
		}
		else {
			throw new Exception(sprintf('Unable to find file %s', $wsFile), Error::ERROR);
		}
	}

	/**
	 * @param Docfile\Version $docfile
	 * @throws Exception
	 * @return Service
	 */
	public function delete(Docfile\Version $docfile, $withfiles = false, $withiterations = false)
	{
		$factory = $this->factory;
		$dao = $factory->getDao($docfile->cid);

		/* Init Record and Fsdata */
		$record = $docfile->getData();
		$fsdata = $record->getFsdata();

		/* Check if access is free */
		$access = $docfile->checkAccess();
		if ( !($access == AccessCode::FREE || $access == AccessCode::SUPPRESS || $access >= 100) || $access == AccessCode::HISTORY ) {
			throw new AccessCodeException(sprintf('Docfile %s is locked with code %s, suppress is not permit', $docfile->getUid(), $access));
		}

		/* Signal */
		$event = new Event(Event::PRE_DELETE, $docfile, $factory);
		$this->getEventManager()->trigger($event);

		/* Delete record of associated files */
		$dao->deleteFromId($docfile->getId());

		/* Delete the file if $withfiles = true */
		if ( $withfiles === true ) {
			if ( $fsdata->isExisting() ) {
				if ( !$fsdata->putInTrash() ) {
					throw new Exception("Can not delete file " . $fsdata->getFullpath());
				}
			}
		}

		/* Delete the iterations of file */
		if ( $withiterations ) {
			$this->deleteIterations($docfile);
		}

		/* Signal */
		$event->setName(Event::POST_DELETE);
		$this->getEventManager()->trigger($event);

		return $this;
	}

	/**
	 * @param Docfile\Version $docfile
	 * @throws Exception
	 * @return Service
	 */
	public function deleteIterations(Docfile\Version $docfile, $toKeep = 0)
	{
		$factory = $this->factory;

		/* Signal */
		$event = new Event(Event::PRE_DELETE_ITERATION, $docfile, $factory);
		$this->getEventManager()->trigger($event);

		try {
			/* Get list of associated files and check access */
			$iterationList = $factory->getList(Docfile\Iteration::$classId);
			$iDao = $factory->getDao(Docfile\Iteration::$classId);
			//$filter = $iDao->toSys('ofDocfileId').'='.$docfile->getId();
			$filter = new Filter($iDao);
			$filter->andfind($docfile->getId(), $iDao->toSys('ofDocfileId'), Op::EQUAL);
			$filter->sort($iDao->toSys('id'), 'DESC');
			$iterationList->load($filter);

			/* Delete each iteration record and file */
			$i = 0;
			foreach( $iterationList as $entry ) {
				if ( $i < $toKeep ) {
					$i++;
					continue;
				}

				$iteration = new Docfile\Iteration();
				$iDao->hydrate($iteration, $entry);

				/* Delete record of associated files */
				$iDao->deleteFromId($iteration->getId());

				$fsdata = $iteration->getData()->getFsdata();
				if ( $fsdata->isExisting() ) {
					if ( !$fsdata->putInTrash() ) {
						throw new Exception("Can not delete file " . $fsdata->getFullpath());
					}
				}
				$i++;
			}
		}
		catch( \Exception $e ) {
			throw new Exception(sprintf('Can not delete iterations of docfile %s: %s', $docfile->getName(), $e->getMessage()));
		}

		/* Signal */
		$event->setName(Event::POST_DELETE_ITERATION);
		$this->getEventManager()->trigger($event);

		return $this;
	}

	/**
	 * @param Docfile\Version $docfile
	 * @throws Exception
	 * @return Service
	 */
	public function setVersionToHead(Docfile\Version $docfile)
	{
		/* get helpers */
		$factory = $this->factory;
		$container = $docfile->getParent()->getParent();
		$reposit = Reposit::init($container->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));
		$dao = $factory->getDao($docfile->cid);

		/* Signal */
		$event = new Event(Event::PRE_PROMUTE, $docfile, $factory);
		$this->getEventManager()->trigger($event);

		/* init Record and Fsdata */
		$record = $docfile->getData();
		$previousFsdata = $record->getFsdata();

		$vault->restoreRecordToHead($record);
		$previousFsdata->delete();

		$docfile->lock(AccessCode::VALIDATE);
		$docfile->setLifeStage('getback');
		$dao->save($docfile);

		/* Signal */
		$event->setName(Event::POST_PROMUTE);
		$this->getEventManager()->trigger($event);

		return $this;
	}
}

