<?php
// %LICENCE_HEADER%
namespace Rbs\Ged;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
<< */

/**SQL_INSERT>>
<< */

/** SQL_ALTER>>
<< */

/** SQL_FKEY>>
<< */

/** SQL_TRIGGER>>
<< */

/** SQL_VIEW>>
<< */

/** SQL_DROP>>
<< */

/**
 *
 *
 */
class DocumentDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'workitems_documents';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'workitems_documents';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'workitems_documents_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'number' => 'number',
		'life_stage' => 'lifeStage',
		'acode' => 'accessCode',
		'iteration' => 'iteration',
		'container_id' => 'parentId',
		'version' => 'version',
		'doctype_id' => 'doctypeId',
		'designation' => 'designation',
		'from_document_id' => 'fromId',
		'updated' => 'updated',
		'update_by_id' => 'updateById',
		'created' => 'created',
		'create_by_id' => 'createById'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created' => 'datetime',
		'locked' => 'datetime',
		'updated' => 'datetime'
	);

	/**
	 * Constructor
	 *
	 * @param \Rbplm\Ged\Document $mapped
	 * @param array $options
	 */
	public function save(MappedInterface $mapped, $options = array())
	{
		$parent = $mapped->getParent();
		if ( $parent ) {
			$mapped->parentId = $parent->id;
		}

		$process = $mapped->getProcess();
		if ( $process ) {
			$mapped->processId = $process->id;
		}

		$updateBy = $mapped->getUpdateBy();
		if ( $updateBy ) {
			$mapped->updateById = $updateBy->id;
		}

		$createBy = $mapped->getCreateBy();
		if ( $createBy ) {
			$mapped->createById = $createBy->id;
		}

		$lockBy = $mapped->getLockBy();
		if ( $lockBy ) {
			$mapped->lockById = $lockBy->id;
		}

		$doctype = $mapped->getDoctype();
		if ( $doctype ) {
			$mapped->doctypeId = $doctype->id;
		}

		$category = $mapped->getCategory();
		if ( $category ) {
			$mapped->categoryId = $category->id;
		}

		parent::save($mapped, $options);
	}
}
