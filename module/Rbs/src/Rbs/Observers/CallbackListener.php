<?php
namespace Rbs\Observers;

use Rbs\EventDispatcher\Event;
use Rbplm\Dao\Connexion as Connexion;
use Ranchbe;

/**
 * 
 *
 */
class CallbackListener
{
	
	/**
	 * @var \PDOStatement
	 */
	static $onEventStmt;

	/**
	 *
	 * @param Event $event
	 * @return Event
	 */
	public static function onEvent(Event $event) : Event
	{
		$eventName = $event->getName();
		$any = $event->getEmitter();
		$ranchbe = Ranchbe::get();

		/* build a stmt and set in static var*/
		if ( !isset(self::$onEventStmt) ) {
			$select = array(
				'id as id',
				'reference_uid as referenceUid',
				'reference_cid as referenceCid',
				'reference_id as referenceId',
				'spacename as spacename',
				'events as events',
				'callback_class as callbackClass',
				'callback_method as callbackMethod'
			);
			$filter = '(reference_uid=:referenceUid OR (reference_cid=:referenceCid AND reference_uid IS NULL)) AND events LIKE :event AND is_actif = 1';
			$selectStr = implode(',', $select);
			$sql = "SELECT $selectStr FROM callbacks WHERE $filter";
			$stmt = Connexion::get()->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			self::$onEventStmt = $stmt;
		}

		$stmt = self::$onEventStmt;
		$bind = [
			':referenceUid' => $any->getUid(),
			':referenceCid' => $any->cid,
			':event' => '%' . $eventName . '%'
		];

		$stmt->execute($bind);

		if ( $stmt->rowCount() == 0 ) {
			return $event;
		}

		/* */
		
		while( $caller = $stmt->fetch(\PDO::FETCH_ASSOC) ) {
			try {
				$callBackClass = $caller['callbackClass'];
				$callBackMethod = $caller['callbackMethod'];
				$ranchbe->log(sprintf('Run callback %s::%s', $callBackClass, $callBackMethod));
				$callBack = new $callBackClass();
				$callBack->$callBackMethod($event);
			}
			catch( CallbackException $e ) {
				$ranchbe->log(sprintf('Callback Exception, Code %s, Message : %s', $e->getCode(), $e->getMessage()));
				$ranchbe->log($e);
			}
			catch( InformationException $e ) {
				$ranchbe->log(sprintf('Information Exception : %s', $e->getMessage()));
				$ranchbe->log($e);
			}
			catch( \Throwable $e ) {
				$ranchbe->log(sprintf('Unattend Callback Exception : %s', $e->getMessage()));
				$ranchbe->log($e);
			}
		}

		return $event;
	}
}
