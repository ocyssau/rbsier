<?php
namespace Rbs\Observers;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `callbacks` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR (128) NOT NULL,
 `cid` VARCHAR (32) NOT NULL,
 `name` VARCHAR (128) NULL,
 `description` VARCHAR (512) NULL,
 `is_actif` INT(1) DEFAULT 1,
 `reference_id` VARCHAR (128) NULL,
 `reference_uid` VARCHAR (128) NULL,
 `reference_cid` VARCHAR (128) NULL,
 `spacename` VARCHAR (64) NULL,
 `events` VARCHAR (256) NULL,
 `callback_class` VARCHAR (256) NULL,
 `callback_method` VARCHAR (256) NULL,
 PRIMARY KEY (`id`),
 KEY `INDEX_callbacks_1` (`uid`),
 KEY `INDEX_callbacks_2` (`cid`),
 KEY `INDEX_callbacks_3` (`reference_id`),
 KEY `INDEX_callbacks_4` (`reference_uid`),
 KEY `INDEX_callbacks_5` (`reference_cid`),
 KEY `INDEX_callbacks_8` (`events`),
 UNIQUE KEY `UNIQ_callbacks_1` (`uid`)
) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP TABLE IF EXISTS `callbacks`;
 <<*/

/**
 *
 *
 */
class CallbackFactoryDao extends \Rbs\Dao\Sier
{

	/**
	 * @var string
	 */
	public static $table='callbacks';

	/**
	 * @var string
	 */
	public static $vtable='callbacks';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'cid'=>'cid',
		'name'=>'name',
		'description'=>'description',
		'is_actif'=>'isActif',
		'reference_id'=>'referenceId',
		'reference_uid'=>'referenceUid',
		'reference_cid'=>'referenceCid',
		'spacename'=>'spacename',
		'events'=>'events',
		'callback_class'=>'callbackClass',
		'callback_method'=>'callbackMethod'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'events'=>'json'
	);

	/**
	 * 
	 * @param \Rbplm\Any $mapped
	 * @param string $cid
	 * @param string $eventName
	 * @return \Rbplm\Dao\MappedInterface
	 */
	public function loadFromReferenceCidAndEvent($mapped, $cid, $eventName)
	{
		$filter = 'obj.reference_cid=:cid AND obj.events LIKE :event';
		$bind = array(':cid'=>$cid, ':event'=>'%'.$eventName.'%');
		return $this->load($mapped, $filter, $bind );
	}
}
