<?php
namespace Rbs\Observers;


/**
 * Instanciate and call the user callbacks define on doctypes
 */
class DoctypeCallbackFactory extends CallbackFactory
{

	/** @var string */
	static $classId = 'dtcallbackjh9';


	/** @var integer */
	public $doctypeId;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties        	
	 * @return Callback
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['doctypeId'])) ? $this->doctypeId = $properties['doctypeId'] : null;
		return $this;
	}

}
