<?php
namespace Rbs\Observers;

/**
 * Exception for callback.
 * Throw for informe about Callback execution
 * 
 * @author ocyssau
 *
 */
class InformationException extends \Exception
{
}