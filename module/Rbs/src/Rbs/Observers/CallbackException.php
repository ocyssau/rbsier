<?php
namespace Rbs\Observers;

/**
 * Exception for callback.
 * Throw when a callback failed.
 * 
 * @author ocyssau
 *
 */
class CallbackException extends \Rbs\EventDispatcher\EventException
{
}
