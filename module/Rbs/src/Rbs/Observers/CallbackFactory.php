<?php
namespace Rbs\Observers;

use Rbplm\Any;

/**
 * Instanciate and call the user callbacks
 */
class CallbackFactory extends Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Mapped;

	/** @var string */
	static $classId = 'callback9u687';

	/**
	 *
	 * @var array
	 */
	protected $events = [];

	/**
	 * @var Any
	 */
	protected $reference;

	/**
	 * @var bool
	 */
	protected $isActif = true;

	/** @var string */
	public $referenceUid;

	/** @var string */
	public $referenceCid;

	/** @var integer */
	public $referenceId;

	/** @var string */
	public $spacename;

	/** @var string */
	public $callbackClass;

	/** @var string */
	public $callbackMethod;

	/**
	 *
	 * @param array $properties        	
	 */
	public function __construct($properties = null)
	{
		if ( $properties ) {
			$this->hydrate($properties);
		}

		$this->cid = static::$classId;
	}

	/**
	 *
	 * @param string $name        	
	 * @return Callback
	 */
	public static function init($name = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid()->setName($name);
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties        	
	 * @return Callback
	 */
	public function hydrate(array $properties)
	{
		$this->mappedHydrate($properties);
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['isActif'])) ? $this->isActif = $properties['isActif'] : null;

		/* Notification */
		(isset($properties['referenceId'])) ? $this->referenceId = $properties['referenceId'] : null;
		(isset($properties['referenceUid'])) ? $this->referenceUid = $properties['referenceUid'] : null;
		(isset($properties['referenceCid'])) ? $this->referenceCid = $properties['referenceCid'] : null;
		(isset($properties['spacename'])) ? $this->spacename = $properties['spacename'] : null;

		/* enable null values : */
		(array_key_exists('events', $properties)) ? $this->events = $properties['events'] : null;
		(array_key_exists('callbackClass', $properties)) ? $this->callbackClass = $properties['callbackClass'] : null;
		(array_key_exists('callbackMethod', $properties)) ? $this->callbackMethod = $properties['callbackMethod'] : null;

		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getEvents()
	{
		return $this->events;
	}

	/**
	 *
	 * @return array
	 */
	public function setEvents($array)
	{
		return $this->events = $array;
	}

	/**
	 *
	 * @param string $event        	
	 * @return Callback
	 */
	public function addEvent($event)
	{
		$this->events[] = $event;
		return $this;
	}

	/**
	 * 
	 * @param bool $bool
	 * @return bool
	 */
	public function isActif(bool $bool = null)
	{
		(is_bool($bool)) ? $this->isActif = $bool : null;
		return $this->isActif;
	}

	/**
	 *
	 * @param boolean [OPTIONAL] $asId        	
	 * @return Any
	 */
	public function getReference($asId = false)
	{
		if ( $asId ) {
			return $this->referenceUid;
		}
		return $this->reference;
	}

	/**
	 *
	 * @param Any $reference        	
	 * @return Callback
	 */
	public function setReference($reference)
	{
		$this->reference = $reference;
		$this->referenceId = $reference->getId();
		$this->referenceUid = $reference->getUid();
		return $this;
	}
}
