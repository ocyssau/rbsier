<?php
namespace Rbs\Observers;

use Rbs\Ged\Document\Event;
use Rbplm\Dao\Connexion as Connexion;

/**
 * Instanciate and call the user callbacks define on doctypes
 */
class DoctypeCallbackListener
{

	/**
	 *
	 * @var \PDOStatement
	 */
	static $onEventStmt;

	/**
	 *
	 * @param Event $event
	 * @return Event
	 */
	public static function onEvent(Event $event) : Event
	{
		$eventName = $event->getName();
		$any = $event->getEmitter();

		/* build a stmt and set in static var */
		if ( !isset(self::$onEventStmt) ) {
			$select = [
				'pre_store_class as preStoreClass',
				'pre_store_method as preStoreMethod',
				'post_store_class as postStoreClass',
				'post_store_method as postStoreMethod',
				'pre_update_class as preUpdateClass',
				'pre_update_method as preUpdateMethod',
				'post_update_class as postUpdateClass',
				'post_update_method as postUpdateMethod'
			];
			$selectStr = implode(',', $select);
			$sql = "SELECT $selectStr FROM doctypes WHERE id=:doctypeId";
			$stmt = Connexion::get()->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			self::$onEventStmt = $stmt;
		}

		$stmt = self::$onEventStmt;
		$bind = [
			':doctypeId' => $any->doctypeId
		];
		$stmt->execute($bind);
		$dt = $stmt->fetch();

		/* */
		switch ($eventName) {
			case Event::PRE_CREATE:
				$callBackClass = $dt['preStoreClass'];
				$callBackMethod = $dt['preStoreMethod'];
				break;
			case Event::POST_CREATE:
				$callBackClass = $dt['postStoreClass'];
				$callBackMethod = $dt['postStoreMethod'];
				break;
			case Event::PRE_CHECKIN:
				$callBackClass = $dt['preUpdateClass'];
				$callBackMethod = $dt['preUpdateMethod'];
				break;
			case Event::POST_CHECKIN:
				$callBackClass = $dt['postUpdateClass'];
				$callBackMethod = $dt['postUpdateMethod'];
				break;
			default:
				return $event;
		}

		/* */
		if ( $callBackClass && $callBackMethod ) {
			$callBack = new $callBackClass();
			$callBack->$callBackMethod($event);
		}

		return $event;
	}
}

