<?php
namespace Rbs\Observers;

use Rbs\EventDispatcher\Event;

/**
 * Template for Callback to run on Docfile
 * 
 * $emitter = $event->getEmitter();
 * $emitter->factory = $event->getFactory();
 * 
 */
abstract class Callback
{

	/**
	 *
	 * @param Event $event        	
	 */
	public function onEvent(Event $event)
	{
		$methodName = 'on' . ucfirst($event->getName());
		if ( method_exists($this, $methodName) ) {
			call_user_func([
				$this,
				$methodName
			], $event);
		}
	}
}
