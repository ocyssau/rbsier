<?php
namespace Rbs\Pdm\Service;

/**
 *
 */
class Result
{

	/**
	 * 
	 * @var array
	 */
	protected $datas = array();

	/**
	 * 
	 * @var array
	 */
	protected $feedbacks = array();

	/**
	 * 
	 * @var array
	 */
	protected $errors = array();

	/**
	 *
	 * @param string $name
	 * @param mixed $datas
	 * @return Result
	 */
	public function setData($name, $datas)
	{
		$this->datas[$name] = $datas;
		return $this;
	}

	/**
	 *
	 * @param string $msg
	 * @return Result
	 */
	public function error($msg)
	{
		$this->errors[] = $msg;
		return $this;
	}

	/**
	 *
	 * @param string $msg
	 * @return Result
	 */
	public function feedback($msg)
	{
		$this->feedbacks[] = $msg;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @return array
	 */
	public function getFeedbacks()
	{
		return $this->feedbacks;
	}
}
