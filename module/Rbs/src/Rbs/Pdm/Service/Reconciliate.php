<?php
//%LICENCE_HEADER%
namespace Rbs\Pdm\Service;

use Rbplm\Dao\Connexion;

/**
 * 
 * Reconciliate Product to Document and 
 * populate document properties from Product properties if $this->sychroniseProperties = 'productToDocument'
 * or populate product properties from document properties if $this->sychroniseProperties = 'documentToProduct'
 */
class Reconciliate
{

	/** None synchronization */
	const SYNC_NONE = 0;

	/** Syncronise document from product properties */
	const SYNC_DOCUMENT = 1;

	/** Syncronise product from document properties */
	const SYNC_PRODUCT = 2;

	/**
	 * @var \PDO
	 */
	protected $connexion;

	/** @var string */
	protected $spacename = 'workitem';

	/** @var integer */
	protected $limit = 1000000;

	/**
	 * 
	 * @param array $params
	 */
	public function __construct(array $params)
	{
		$this->connexion = Connexion::get();
		isset($params['spacename']) ? $this->spacename = $params['spacename'] : null;
		isset($params['limit']) ? $this->limit = (int)$params['limit'] : null;
	}

	/**
	 * Reconcile Product\Version with Ged\Document\Version when product.number = document.number
	 * 
	 * @param string $filter Sql filter
	 * @param array $bind PDO bind array for parameter in filter
	 * @return \PDOStatement
	 */
	public function productToDocument($filter = '1=1', $bind = array())
	{
		//$limit = $this->limit;
		
		$sql = 'UPDATE pdm_product_version AS product, reconciliate_documents AS document SET';
		$sql .= ' product.document_id = document.id,';
		$sql .= ' product.document_uid = document.uid';
		$sql .= ' WHERE product.number = document.number';
		$sql .= ' AND product.document_id IS NULL';
		$sql .= " AND $filter";
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->bind = $bind;
		$stmt->execute($bind);
		return $stmt;
	}

	/**
	 * Return list of product to reconciliate with documents
	 *
	 * @param string $filter Sql filter
	 * @param array $bind PDO bind array for parameter in filter
	 * @return \PDOStatement
	 */
	public function productToReconcilateWithDocument($filter = '1=1', $bind = array())
	{
		$limit = $this->limit;
		
		$sql = 'SELECT product.id, product.uid, document.id AS document_id, document.uid AS document_uid';
		$sql .= ' FROM reconciliate_documents AS document, pdm_product_version AS product';
		$sql .= ' WHERE document.number = product.number';
		$sql .= " AND product.document_id IS NULL AND product.number IS NOT NULL";
		$sql .= " AND $filter LIMIT $limit";
		
		$selectStmt = $this->connexion->prepare($sql);
		$selectStmt->bind = $bind;
		$selectStmt->execute($bind);
		
		return $selectStmt;
	}

	/**
	 * Reconcile Product\Instance with Product\Version when instance.of_product_number = product.number
	 * 
	 * @param string $filter Sql filter
	 * @param array $bind PDO bind array for parameter in filter
	 * @return \PDOStatement
	 */
	public function instanceToProduct($filter = '1=1', $bind = array())
	{
		//$limit = $this->limit;
		
		$sql = 'UPDATE pdm_product_instance AS instance, pdm_product_version AS product SET';
		$sql .= ' instance.of_product_id = product.id,';
		$sql .= ' instance.of_product_uid = product.uid,';
		$sql .= ' instance.type = product.type';
		$sql .= " WHERE instance.of_product_number = product.number";
		$sql .= " AND instance.of_product_id IS NULL AND instance.of_product_number IS NOT NULL";
		$sql .= " AND instance.id > 1 AND $filter";
		$updateStmt = $this->connexion->prepare($sql);
		
		$updateStmt->bind = $bind;
		$updateStmt->execute($bind);
		
		return $updateStmt;
	}

	/**
	 * Return list of instance to reconciliate with product
	 * 
	 * @param string $filter Sql filter
	 * @param array $bind PDO bind array for parameter in filter
	 * @return \PDOStatement
	 */
	public function instanceToReconcilateWithProduct($filter = '1=1', $bind = array())
	{
		$limit = $this->limit;
		
		$sql = 'SELECT instance.id, instance.uid, product.id AS of_product_id, product.uid AS of_product_uid';
		$sql .= ' FROM pdm_product_instance AS instance, pdm_product_version AS product';
		$sql .= ' WHERE instance.of_product_number = product.number';
		$sql .= " AND instance.of_product_id IS NULL AND instance.of_product_number IS NOT NULL";
		$sql .= " AND instance.id > 1 AND $filter LIMIT $limit";
		
		$selectStmt = $this->connexion->prepare($sql);
		$selectStmt->bind = $bind;
		$selectStmt->execute($bind);
		
		return $selectStmt;
	}

	/**
	 * Populate designation of document from associated product designation
	 * 
	 * @param string $filter Sql filter
	 * @param array $bind PDO bind array for parameter in filter
	 * @return \PDOStatement
	 */
	public function sychroniseDocument($filter = '1=1', $bind = array())
	{
		$spacename = $this->spacename;
		if ( !$spacename ) {
			throw new \InvalidArgumentException('spacename must be set as parameters in constructor');
		}
		$table = $spacename . '_documents';
		$bind[':spacename'] = $spacename;
		
		$sql = "UPDATE $table AS doc";
		$sql .= " JOIN pdm_product_version AS product ON doc.id = product.document_id AND product.spacename = :spacename";
		$sql .= ' SET';
		$sql .= ' doc.designation = IF(product.description <> "", product.description, doc.designation)';
		$sql .= " WHERE $filter";
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->bind = $bind;
		$stmt->execute($bind);
		return $stmt;
	}

	/**
	 * Populate designation of product from associated document designation
	 * 
	 * @param string $filter Sql filter
	 * @param array $bind PDO bind array for parameter in filter
	 * @return \PDOStatement
	 */
	public function sychroniseProduct($filter = '1=1', $bind = array())
	{
		$spacename = $this->spacename;
		if ( !$spacename ) {
			throw new \InvalidArgumentException('spacename must be set as parameters in constructor');
		}
		$table = $spacename . '_documents';
		$bind[':spacename'] = $spacename;
		
		$sql = "UPDATE pdm_product_version AS product";
		$sql .= " JOIN $table AS doc ON product.document_id=doc.id AND product.spacename = :spacename";
		$sql .= ' SET';
		$sql .= ' product.description = IF(doc.designation<>"" AND doc.designation<>"none", doc.designation, product.description)';
		//$sql .= ' ,product.version = doc.version';
		$sql .= "WHERE $filter";
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->bind = $bind;
		$stmt->execute($bind);
		return $stmt;
	}
	
	
}
