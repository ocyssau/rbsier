<?php
namespace Rbs\Pdm\Product;

use Rbplm\Pdm\Product;

/**
 * 
 * @author ocyssau
 *
 */
class Service
{

	/**
	 *
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 *
	 * @var \Rbs\Pdm\Service\Result
	 */
	public $result;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
		$this->result = new \Rbs\Pdm\Service\Result();
	}

	/**
	 *
	 * @param \Rbplm\Pdm\Product\Version $product
	 * @return Service
	 */
	public function checkout($product)
	{
		return $this;
	}

	/**
	 *
	 * @param \Rbplm\Pdm\Product\Version $product
	 * @throws \Exception
	 * @return Service
	 */
	public function checkin($product)
	{
		$factory = $this->factory;

		/** @var \Rbs\Pdm\Product\VersionDao $productDao */
		$productDao = $factory->getDao($product->cid);

		/** @var \Rbs\Pdm\Product\InstanceDao $instanceDao */
		$instanceDao = $factory->getDao(Product\Instance::$classId);

		/* OPEN TRANSACTION */
		/** @var \PDO $connexion */
		$connexion = $factory->getConnexion();
		$inTrans = $connexion->inTransaction();
		if ( $inTrans == false ) {
			$connexion->beginTransaction();
		}

		/* DELETE PREVIOUS INSTANCES FOR THE PRODUCT */
		try {
			$instanceDao->deleteFromParentId($product->getId());
		}
		catch( \Exception $e ) {
			throw $e;
		}

		/* SAVE PRODUCT VERSION */
		if ( !$product->getId() ) {
			$this->result->feedback(sprintf(tra('create product %s'), $product->getNumber()));
		}
		else {
			$this->result->feedback(sprintf(tra('save product %s'), $product->getNumber()));
		}

		try {
			$productDao->save($product);
			$this->result->feedback(sprintf(tra('The product %s has been saved'), $product->getNumber()));
		}
		catch( \Exception $e ) {
			$this->result->error(sprintf(tra('Can not save the product %s : %s'), $product->getNumber(), $e->getMessage()));
			$connexion->rollBack();
			throw $e;
		}

		/* SAVE CHILDREN INSTANCES */
		if ( $product->getChildren() ) {
			/** @var \Rbplm\Pdm\Product\Instance $child */
			foreach( $product->getChildren() as $child ) {
				$child->setParent($product);
				$ofProduct = $child->getOfProduct();
				$productDao->save($ofProduct);

				$child->setOfProduct($ofProduct);
				$instanceDao->save($child);

				if ( $child->getChildren() ) {
					$this->checkin($ofProduct);
				}
			}
		}

		if ( $inTrans == false ) {
			$connexion->commit();
		}

		return $this;
	}

	/**
	 *
	 * @param \Rbplm\Pdm\Product\Version $product
	 * @return Service
	 */
	function delete($product)
	{
		return $this;
	}

	/**
	 *
	 * @param \Rbplm\Pdm\Product\Version $product
	 * @param boolean $lock
	 * @return Service
	 */
	public function create($product, $lock = false)
	{
		$factory = $this->factory;
		$productDao = $factory->getDao($product->cid);

		/* OPEN TRANSACTION */
		$connexion = $factory->getConnexion();
		$inTrans = $connexion->inTransaction();
		if ( $inTrans == false ) {
			$connexion->beginTransaction();
		}

		/* SAVE PRODUCT VERSION */
		if ( !$product->getId() ) {
			$this->result->feedback(sprintf(tra('create product %s'), $product->getNumber()));
			try {
				$productDao->save($product);
				$this->result->feedback(sprintf(tra('The product %s has been created'), $product->getNumber()));
			}
			catch( \Exception $e ) {
				$this->result->error(sprintf(tra('Can not create the product %s'), $product->getNumber()));
				$connexion->rollBack();
				return $this;
			}
		}

		/* SAVE CHILDREN INSTANCES */
		if ( $product->getChildren() ) {
			$instanceDao = $factory->getDao(Product\Instance::$classId);
			foreach( $product->getChildren() as $child ) {
				$child->setParent($product);
				$instanceDao->save($child);
			}
		}

		if ( $inTrans == false ) {
			$connexion->commit();
		}

		return $this;
	}

	/**
	 *
	 * @param \Rbplm\Pdm\Product\Version $product
	 * @return Service
	 */
	public function newVersion($product)
	{
		return $this;
	}
} /* End of class */
