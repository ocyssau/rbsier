<?php
// %LICENCE_HEADER%
namespace Rbs\Pdm\Product;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\Exception;
use Rbplm\Dao\MappedInterface;

/**
 * SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `pdm_product_version` (
	`id` int(11) NOT NULL,
	`uid` VARCHAR(32) NOT NULL,
	`name` VARCHAR(128) NOT NULL,
	`number` VARCHAR(128) NOT NULL,
	`description` VARCHAR(512) NULL,
	`version` int(11) NOT NULL DEFAULT 1,
	`of_product_id` int(11) NULL,
	`of_product_uid` VARCHAR(32) NULL,
	`document_id` int(11) NULL,
	`document_uid` VARCHAR(32) NULL,
	`lock_by_id` int(11) DEFAULT NULL,
	`lock_by_uid` VARCHAR(64) DEFAULT NULL,
	`locked` DATETIME DEFAULT NULL,
	`updated` DATETIME DEFAULT NULL,
	`update_by_id` int(11) DEFAULT NULL,
	`update_by_uid` VARCHAR(64) DEFAULT NULL,
	`created` DATETIME DEFAULT NULL,
	`create_by_id` int(11) DEFAULT NULL,
	`create_by_uid` VARCHAR(64) DEFAULT NULL,
	`spacename` VARCHAR(32) NULL,
	`type` VARCHAR(64) NULL,
	`materials` VARCHAR(512) NULL,
	`weight` float NULL,
	`volume` float NULL,
	`wetsurface` float NULL,
	`density` float NULL,
	`gravitycenter` VARCHAR(512) NULL,
	`inertiacenter` VARCHAR(512) NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `U_pdm_product_version_1` (`uid`),
	UNIQUE KEY `U_pdm_product_version_2` (`number`),
	UNIQUE KEY `U_pdm_product_version_3` (`of_product_uid`,`version`),
	KEY `INDEX_pdm_product_version_1` (`of_product_uid`),
	KEY `INDEX_pdm_product_version_6` (`of_product_id`),
	KEY `INDEX_pdm_product_version_2` (`version`),
	KEY `INDEX_pdm_product_version_3` (`description`),
	KEY `INDEX_pdm_product_version_4` (`uid`),
	KEY `INDEX_pdm_product_version_5` (`name`),
	KEY `INDEX_pdm_product_version_7` (`document_id`),
	KEY `INDEX_pdm_product_version_8` (`document_uid`),
	KEY `INDEX_pdm_product_version_9` (`spacename`),
	KEY `INDEX_pdm_product_version_10` (`type`),
	KEY `INDEX_pdm_product_version_11` (`create_by_uid`),
	KEY `INDEX_pdm_product_version_12` (`lock_by_uid`),
	KEY `INDEX_pdm_product_version_13` (`update_by_uid`),
	KEY `INDEX_pdm_product_version_14` (`created`),
	KEY `INDEX_pdm_product_version_15` (`locked`),
	KEY `INDEX_pdm_product_version_16` (`updated`),
	FULLTEXT INDEX `FT_name` (`name` ASC, `number` ASC, `description` ASC)
);

CREATE TABLE IF NOT EXISTS `pdm_seq`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO pdm_seq(id) VALUES(100);

<<
 */

/** SQL_INSERT>>

INSERT INTO `pdm_product_version`
(`id`,
`uid`,
`name`,
`number`,
`description`,
`type`,
`locked`,
`lock_by_id`,
`lock_by_uid`,
`updated`,
`update_by_id`,
`update_by_uid`,
`created`,
`create_by_id`,
`create_by_uid`
)
VALUES
('1', 'ranchbe', 'ranchbe', 'ranchbe', 'Root Product on top of Ranchbe', 'root', now(), 1, 'admin', now(), 1, 'admin', now(), 1, 'admin' );

<<*/

/** SQL_ALTER>>
<< */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * DROP TABLE pdm_product_version;
 * <<
 */

/**
 * @brief Dao class
 *
 * @see \Rbs\Dao\Sier
 *
 */
class VersionDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'pdm_product_version';

	/* @var string */
	public static $vtable = 'pdm_product_version';

	/* @var string */
	public static $sequenceName = 'pdm_seq';

	/* @var string */
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'name' => 'name',
		'number' => 'number',
		'description' => 'description',
		'of_product_uid' => 'ofProductUid',
		'of_product_id' => 'ofProductId',
		'document_uid' => 'documentUid',
		'document_id' => 'documentId',
		'spacename' => 'spacename',
		'lock_by_id' => 'lockById',
		'lock_by_uid' => 'lockByUid',
		'locked' => 'locked',
		'updated' => 'updated',
		'update_by_id' => 'updateById',
		'update_by_uid' => 'updateByUid',
		'created' => 'created',
		'create_by_id' => 'createById',
		'create_by_uid' => 'createByUid',
		'type' => 'type',
		'version' => 'version',
		'materials' => 'materials',
		'weight' => 'weight',
		'volume' => 'volume',
		'wetsurface' => 'wetSurface',
		'density' => 'density',
		'gravitycenter' => 'gravityCenter',
		'inertiacenter' => 'inertiaCenter'
		
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'materials' => 'json',
		'gravitycenter' => 'json',
		'inertiacenter' => 'json',
		'created' => 'datetime',
		'locked' => 'datetime',
		'updated' => 'datetime',
	);

	/**
	 * If $versionId is null, load the last version
	 *
	 * @param MappedInterface $mapped
	 * @param string $number
	 * @param integer $versionId
	 * @return \Rbplm\Dao\MappedInterface
	 */
	public function loadFromNumber(MappedInterface $mapped, $number, $versionId = null)
	{
		if ( $versionId ) {
			$filter = 'obj.number=:number AND obj.version=:versionId LIMIT 1';
			$bind = array(
				':number' => $number,
				':versionId' => $versionId
			);
		}
		else {
			$filter = 'obj.number=:number ORDER BY version DESC LIMIT 1';
			$bind = array(
				':number' => $number
			);
		}
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::save()
	 */
	public function save(MappedInterface $mapped)
	{
		if ( !isset($mapped->documentId) ) {
			try {
				$document = $mapped->getDocument();
				if ( $document ) {
					$mapped->setDocument($document);
				}
			}
			catch( \Exception $e ) {
				throw new Exception('documentId is not set');
			}
		}

		if ( !isset($mapped->ofProductId) ) {
			try {
				$product = $mapped->getOfProduct();
				if ( $product ) {
					$mapped->setOfProduct($product);
				}
			}
			catch( \Exception $e ) {
				throw new Exception('productId is not set');
			}
		}

		return parent::save($mapped);
	}
} /* End of class */
