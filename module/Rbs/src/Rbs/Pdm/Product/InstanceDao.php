<?php
// %LICENCE_HEADER%
namespace Rbs\Pdm\Product;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;
use Exception;

/**SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `pdm_product_instance` (
  `id` int(11) NOT NULL,
  `uid` VARCHAR(32) NOT NULL,
  `cid` VARCHAR(32) NOT NULL DEFAULT '569e971bb57c0',
  `name` VARCHAR(128) NOT NULL,
  `number` VARCHAR(128) NOT NULL,
  `nomenclature` VARCHAR(128) NULL,
  `description` VARCHAR(512) NULL,
  `position` VARCHAR(512) NULL,
  `quantity` int(11) NULL,
  `parent_id` int(11) NULL,
  `path` VARCHAR(256) NULL,
  `of_product_id` int(11) NULL,
  `of_product_uid` VARCHAR(32) NULL,
  `of_product_number` VARCHAR(128) NULL,
  `context_id` VARCHAR(32) NULL,
  `type` VARCHAR(64) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_pdm_product_instance_1` (`uid`),
  UNIQUE KEY `U_pdm_product_instance_2` (`number`, `parent_id`),
  KEY `INDEX_pdm_product_instance_1` (`cid`),
  KEY `INDEX_pdm_product_instance_2` (`number`),
  KEY `INDEX_pdm_product_instance_3` (`description`),
  KEY `INDEX_pdm_product_instance_4` (`nomenclature`),
  KEY `INDEX_pdm_product_instance_5` (`uid`),
  KEY `INDEX_pdm_product_instance_6` (`name`),
  KEY `INDEX_pdm_product_instance_7` (`type`),
  KEY `INDEX_pdm_product_instance_8` (`of_product_uid`),
  KEY `INDEX_pdm_product_instance_9` (`of_product_id`),
  KEY `INDEX_pdm_product_instance_10` (`path`),
  KEY `INDEX_pdm_product_version_12` (`of_product_number` ASC),
  CONSTRAINT `FK_pdm_product_instance_1` 
  		FOREIGN KEY (`of_product_id`) 
  		REFERENCES `pdm_product_version` (`id`) 
  		ON DELETE CASCADE 
  		ON UPDATE CASCADE,
  CONSTRAINT `FK_pdm_product_instance_2`
		FOREIGN KEY (`parent_id`)
		REFERENCES `pdm_product_version` (`id`)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);
  
  CREATE TABLE IF NOT EXISTS `pdm_instance_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
  )ENGINE=MyISAM AUTO_INCREMENT=100;
  INSERT INTO pdm_instance_seq(id) VALUES(100);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
<<*/

/**SQL_FKEY>>
<<*/

/**SQL_TRIGGER>>
<<*/

/**SQL_VIEW>>
<< */

/**SQL_DROP>>
DROP TABLE pdm_product_instance;
<<*/

/**
 * @brief Dao class
 *
 * @see \Rbs\Dao\Sier
 *
 */
class InstanceDao extends DaoSier
{

	/**
	 * @var \PDOStatement
	 */
	protected $loadChildrenStmt;
	
	/**
	 *
	 * @var string
	 */
	public static $table = 'pdm_product_instance';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'pdm_product_instance';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'pdm_instance_seq';

	/**
	 *
	 * @var string
	 */
	public static $sequenceKey = 'id';

	/**
	 * Instance has no children
	 * @var string
	 */
	public static $childTable = null;

	/**
	 * @var string
	 */
	public static $childForeignKey = null;

	/**
	 *
	 * @var string
	 */
	public static $parentTable = 'pdm_product_version';

	/**
	 *
	 * @var string
	 */
	public static $parentForeignKey = 'id';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'name' => 'name',
		'number' => 'number',
		'description' => 'description',
		'nomenclature' => 'nomenclature',
		'quantity' => 'quantity',
		'position' => 'position',
		'of_product_id' => 'ofProductId',
		'of_product_uid' => 'ofProductUid',
		'of_product_number' => 'ofProductNumber',
		'parent_id' => 'parentId',
		'path' => 'path',
		'context_id' => 'contextId',
		'type' => 'type',
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'position' => 'json',
	);

	/**
	 *
	 * @param \PDO $conn
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);

		$this->_childTable = static::$childTable;
		$this->_childForeignKey = static::$childForeignKey;
		$this->_parentTable = static::$parentTable;
		$this->_parentForeignKey = static::$parentForeignKey;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::save()
	 */
	public function save(MappedInterface $mapped)
	{
		if ( !isset($mapped->ofProductId) && !isset($mapped->ofProductNumber) && !isset($mapped->ofProductUid) ) {
			try {
				$ofProduct = $mapped->getOfProduct();
				if ( $ofProduct ) {
					$mapped->setOfProduct($mapped->getOfProduct());
				}
			}
			catch( \Exception $e ) {
				throw new Exception('ofProduct is not set');
			}
		}

		if ( !isset($mapped->parentId) ) {
			try {
				$parent = $mapped->getParent();
				if ( $parent ) {
					$mapped->setParent($mapped->getParent());
				}
			}
			catch( \Exception $e ) {
				throw new Exception('parentId is not set');
			}
		}

		return parent::save($mapped);
	}
	
	/**
	 *
	 * @param MappedInterface $mapped
	 * @param string $instanceNumber
	 * @param string $parentNumber
	 */
	public function loadFromNumberAndParentNumber($mapped, $instanceNumber, $parentNumber)
	{
		$bind = array(
			':number' => $instanceNumber,
			':parentNumber' => $parentNumber
		);
		$table = $this->_table;
		$parentTable = $this->_parentTable;
		
		$sql = "SELECT child.* FROM $table as child
		JOIN $parentTable as parent ON parent.id=child.parent_id
		WHERE child.number=:number AND parent.number=:parentNumber";
		
		return $this->loadFromSql($mapped, $sql, $bind);
	}

	/**
	 * Get children instances of a product from a user defined filter
	 *
	 * In filter You may use alias instance for select instance table :
	 * You may use alias "parent." for select parent product table
	 * You may use alias "child." for select child product table (product of the instance)
	 * 
	 * Return a PDO statement with properties name in application semantic.
	 * BE CAREFUL to fact that value are not converted to application attended value,
	 * so the position value must be converted from json to array.
	 * 
	 * This method must be changed in futur.
	 * 
	 * @param array $select
	 * @param string [OPTIONAL] $filter
	 * @param array [OPTIONAL] $bind
	 * 
	 * @return \PDOStatement
	 */
	public function getChildrenFromFilter($select = null, $filter = null, $bind = array())
	{
		$instanceTable = $this->_table;
		$productTable = $this->_parentTable;

		if ( !$this->loadChildrenStmt || $this->loadChildrenFilter != $filter ) {
			if ( !$select ) {
				foreach( $this->metaModel as $asSys => $asApp ) {
					$select[] = 'instance.' . $asSys . ' AS ' . $asApp;
				}
				
				$productMetaModel = array(
					'id' => 'id',
					'uid' => 'uid',
					'name' => 'name',
					'number' => 'number',
					'description' => 'description',
					'document_uid' => 'documentUid',
					'document_id' => 'documentId',
					'spacename' => 'spacename',
					'version' => 'version',
				);
				foreach( $productMetaModel as $asSys => $asApp ) {
					$select[] = 'child.' . $asSys . ' AS child_' . $asApp;
				}
			}

			$selectStr = implode($select, ',');

			$sql = "SELECT $selectStr
			FROM $instanceTable as instance
			LEFT OUTER JOIN $productTable as parent ON instance.parent_id=parent.id
			LEFT OUTER JOIN $productTable as child ON instance.of_product_id=child.id";

			if ( $filter ) {
				$sql .= ' WHERE ' . $filter;
			}
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadChildrenStmt = $stmt;
			$this->loadChildrenFilter = $filter;
		}

		$this->loadChildrenStmt->execute($bind);
		return $this->loadChildrenStmt;
	}

	/**
	 * Get children instances of a product
	 *
	 * You may use alias instance.
	 * for select instance table
	 * You may use alias parent. for select parent product table
	 * You may use alias child. for select child product table (product of the instance)
	 *
	 * @param int $parentId
	 *        	product version id
	 * @param array $select
	 * @param string [OPTIONAL] $filter
	 * @param array [OPTIONAL] $bind
	 * @return \PDOStatement
	 */
	public function getChildrenFromParentId($parentId, $select = null, $filter = null, $bind = array())
	{
		if ( $filter ) {
			$filter = 'parent_id=:parentId AND ' . $filter;
		}
		else {
			$filter = 'parent_id=:parentId';
		}

		$bind[':parentId'] = $parentId;
		return $this->getChildrenFromFilter($select, $filter, $bind);
	}

	/**
	 * Get all children instance of a product including the ou children
	 *
	 * You may use alias instance.
	 * for select instance table
	 * You may use alias parent. for select parent product table
	 * You may use alias child. for select child product table (product of the instance)
	 *
	 * @param int $parentId
	 *        	product version id OR product instance id
	 * @param array $select
	 * @param string [OPTIONAL] $filter
	 * @param array [OPTIONAL] $bind
	 * @return \PDOStatement
	 */
	public function getFullChildren($parentId, $select = null, $filter = null, $bind = array())
	{
		if ( $filter ) {
			$filter = 'instance.path LIKE :path AND ' . $filter;
		}
		else {
			$filter = 'instance.path LIKE :path';
		}
		$bind[':path'] = $parentId . '.%';
		return $this->getChildrenFromFilter($select, $filter, $bind);
	}

	/**
	 *
	 * @param int $childId
	 * @param array $select
	 * @param string [OPTIONAL] $filter
	 * @param array [OPTIONAL] $bind
	 * @return \PDOStatement
	 */
	public function getParents($childId, $select = null, $filter = null, $bind = array())
	{
		$instanceTable = $this->_table;
		$productTable = $this->_parentTable;

		if ( !$this->loadParentsStmt ) {
			if ( !$select ) {
				$productMetaModel = array(
					'description' => 'description',
					'document_uid' => 'documentUid',
					'document_id' => 'documentId',
					'spacename' => 'spacename',
					'version' => 'version',
					'number' => 'productNumber'
				);
				foreach( $this->metaModel as $asSys => $asApp ) {
					$select[] = 'instance.' . $asSys . ' AS ' . $asApp;
				}
				foreach( $productMetaModel as $asSys => $asApp ) {
					$select[] = 'child.' . $asSys . ' AS ' . $asApp;
				}
			}

			$selectStr = implode($select, ',');

			$sql = "SELECT $selectStr
			FROM $instanceTable as instance
			JOIN $productTable as product ON product.id=instance.of_product_id
			JOIN $productTable as parent ON parent.id=instance.parent_id
			WHERE product.id=:childId";

			if ( $filter ) {
				$sql .= ' AND ' . $filter;
			}

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadParentsStmt = $stmt;
		}

		$this->loadParentsStmt->execute(array(
			':childId' => $childId
		));
		return $this->loadParentsStmt;
	}

	/**
	 *
	 * @return InstanceDao
	 */
	public function deleteFromParentId($parentId, $withChildren = false, $withTrans = null)
	{
		$toSysId = $this->toSys('parentId');
		$filter = "$toSysId=:parentId";
		$bind = array(
			':parentId' => $parentId
		);

		$this->_deleteFromFilter($filter, $bind, $withChildren, $withTrans);
		return $this;
	}
} /* End of class */
