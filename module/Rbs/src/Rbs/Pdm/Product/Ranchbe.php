<?php
// %LICENCE_HEADER%
namespace Rbs\Pdm\Product;

use \Rbplm\Pdm\Product\Version as ProductVersion;

/**
 *
 *
 */
class Ranchbe extends ProductVersion
{
	const ID = 1;
	const UID = 'ranchbe';
	const NUMBER = 'ranchbe';

	/**
	 * @var Ranchbe
	 */
	static $singleton;

	/**
	 */
	public function __construct()
	{
		$this->id = self::ID;
		$this->uid = self::UID;
		$this->number = self::NUMBER;
		$this->name = 'Root';
		$this->cid = ProductVersion::$classId;
	}

	/**
	 * Singleton caller
	 * @return Ranchbe
	 */
	public static function get()
	{
		if(!self::$singleton){
			self::$singleton = new self();
		}
		return self::$singleton;
	}
} /* End of class */
