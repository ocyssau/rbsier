<?php

namespace Rbs\Pdm\Product\Instance;

/**
 *
 *
 */
class Service
{

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
	}

	/**
	 * @throw Exception
	 * @return Service
	 *
	 */
	public function checkout($product)
	{
		return $this;
	}

	/**
	 * @throw Exception
	 * @return Service
	 */
	public function checkin($product, $releasing=true, $updateData=true, $checkAccess=true)
	{
		return $this;
	}

	/**
	 * @throw Exception
	 * @return Service
	 */
	function delete($product)
	{
		return $this;
	}

	/**
	 */
	public function create($product, $lock=false)
	{
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function newVersion($product)
	{
		return $this;
	}
} /* End of class*/
