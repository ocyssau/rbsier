<?php
//%LICENCE_HEADER%

namespace Rbs\Pdm\Product;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Dao class
 *
 * @see \Rbs\Dao\Sier
 *
 */
class ConceptDao extends DaoSier
{
	/**
	 *
	 * @var string
	 */
	public static $table='';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(self::$sysToApp, DaoSier::$sysToApp);
	}
}

