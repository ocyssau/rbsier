<?php
//%LICENCE_HEADER%
namespace Rbs\Pdm;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @see \Rbs\Dao\Sier
 *
 */
class UsageDao extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table = 'pdm_product_instance';
	
	/**
	 * @var string
	 */
	public static $vtable = 'pdm_product_instance';
	
	/**
	 * @var string
	 */
	public static $sequenceName = '';
	
	/**
	 * @var string
	 */
	public static $parentTable = 'pdm_product_version';
	
	/**
	 * @var string
	 */
	public static $parentForeignKey = 'of_product_id';
	
	/**
	 * @var string
	 */
	public static $childTable = 'pdm_product_instance';
	
	/**
	 * @var string
	 */
	public static $childForeignKey = '';
	
	/**
	 * @var array
	 */
	public static $sysToApp = array();
	
	/**
	 * @var \PDOStatement
	 */
	protected $getUsedbyStmt;
	
	/**
	 * @var \PDOStatement
	 */
	protected $getChildrenStmt;
	
	/**
	 *
	 * @param \Rbplm\Dao\Connexion $conn
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		
		$this->_childTable = static::$childTable;
		$this->_childForeignKey = static::$childForeignKey;
		$this->_parentTable = static::$parentTable;
		$this->_parentForeignKey = static::$parentForeignKey;
	}
	
	/**
	 * Get list of products wich used product version $id
	 * 
	 * Alias used by SQL request :
	 * "instance" for children instance
	 * "product" for parents products
	 * 
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return \PDOStatement
	 */
	function getUsedBy($id, $filter = null, $bind = array())
	{
		$instanceTable = $this->_table;
		$productTable = $this->_parentTable;
		
		if ( !$this->getUsedbyStmt ) {
			if ( $filter ) {
				$filterStr = $filter->__toString();
				$filterStr = 'instance.of_product_id=:id AND '.$filterStr;
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
				if(!$selectStr){
					$selectStr = 'product.*';
				}
			}
			else {
				$filterStr = 'instance.of_product_id=:id';
				$selectStr = 'product.*';
			}
			
			$sql = 
			"SELECT $selectStr FROM $instanceTable as instance
			JOIN $productTable as product on product.id=instance.parent_id
			WHERE $filterStr";
			
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadParentStmt = $stmt;
		}
		
		$bind[':id'] = $id;
		$this->loadParentStmt->execute($bind);
		return $this->loadParentStmt;
	}
	
	/**
	 * Get list of children product of product version $id
	 *
	 * Alias used by SQL request :
	 * "instance" for children instances
	 * "product" for children products
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return \PDOStatement
	 */
	function getChildren($id, $filter = null, $bind = array())
	{
		$instanceTable = $this->_table;
		$productTable = $this->_parentTable;
		
		if ( !$this->getChildrenStmt ) {
			if ( $filter ) {
				$filterStr = $filter->__toString();
				$filterStr = 'instance.parent_id=:id AND '.$filterStr;
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
				if(!$selectStr){
					$selectStr = 'instance.id as instanceId, instance.name as instanceName, product.*';
				}
			}
			else {
				$filterStr = 'instance.parent_id=:id';
				$selectStr = 'instance.id as instanceId, instance.name as instanceName, product.*';
			}
			
			$sql =
			"SELECT $selectStr FROM $instanceTable as instance
			JOIN $productTable as product on product.id=instance.of_product_id
			WHERE $filterStr";
			
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadParentStmt = $stmt;
		}
		
		$bind[':id'] = $id;
		$this->loadParentStmt->execute($bind);
		return $this->loadParentStmt;
	}
	
}
