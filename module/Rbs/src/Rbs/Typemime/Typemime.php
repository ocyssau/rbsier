<?php
//%LICENCE_HEADER%
namespace Rbs\Typemime;

use Rbplm\Any;

/**
 * 
 *
 */
class Typemime extends Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Mapped;

	static $classId = 'typemime';

	/**
	 *
	 * @var string
	 */
	protected $extensions;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Typemime
	 */
	public function hydrate(array $properties)
	{
		$this->mappedHydrate($properties);
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['extensions'])) ? $this->extensions = $properties['extensions'] : null;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getExtensions()
	{
		return $this->extensions;
	}

	/**
	 *
	 * @return Typemime
	 */
	public function setExtensions($string)
	{
		$this->extensions = $string;
		return $this;
	}
} /* End of class */
