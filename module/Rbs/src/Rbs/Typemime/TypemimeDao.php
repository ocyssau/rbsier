<?php
//%LICENCE_HEADER%

namespace Rbs\Typemime;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `typemimes` (
	 `id` int(11) NOT NULL AUTO_INCREMENT,
	 `uid` VARCHAR(255) NOT NULL DEFAULT '',
	 `name` VARCHAR(255) NOT NULL DEFAULT '',
	 `extensions` VARCHAR(255) DEFAULT NULL,
	 PRIMARY KEY (`id`),
	 UNIQUE (`uid`),
	 INDEX (`name`),
	 INDEX (`extensions`)
 ) ENGINE=MyIsam;
 <<*/

/** SQL_INSERT>>
INSERT INTO `typemimes` 
	(`id`,`uid`,`name`,`extensions`) 
VALUES 
	(1,"application/argouml","Argo UML",".zargo"),
	(2,"x-world/x-3dmf","3Fichiers DMF",".3dmf .3dm .qd3d .qd3"),
	(3,"x-world/x-vrml","Model 3D Vrml",".wrl"),
	(4,"application/x-3dxmlplugin","3D XML",".3dxml"),
	(5,"application/acad","Fichiers AutoCAD (d\'après NCSA)",".dwg"),
	(6,"application/CATIA.Analysis","Fichier analyse catia",".CATAnalysis"),
	(7,"application/CATIA.Catalog","Fichier catalog catia",".catalog"),
	(8,"application/CATIA.Drawing","Fichier drawing catia",".CATDrawing"),
	(9,"application/CATIA.Material","Fichier material catia",".CATMaterial"),
	(10,"application/CATIA.Model","Fichier model catia",".model"),
	(11,"application/CATIA.Part","Fichier part catia",".CATPart"),
	(12,"application/CATIA.Process","Fichier process catia",".CATProcess"),
	(13,"application/CATIA.Product","Fichier product catia",".CATProduct"),
	(14,"application/CATIA.Script","Fichier script catia",".CATScript"),
	(15,"application/CATIA.Setting","Fichier setting catia",".CATSetting"),
	(16,"application/CATIA.Cgr","Cgr catia",".cgr"),
	(17,"application/SOLIDWORK.Assembly","Assemblage solidwork",".sldasm"),
	(18,"application/SOLIDWORK.Part","Part solidwork",".sldprt"),
	(19,"application/SOLIDWORK.Drawing","Drawing solidwork",".slddrw"),
	(20,"application/dsptype","Fichiers TSP",".tsp"),
	(21,"application/dxf","Fichiers AutoCAD",".dxf"),
	(22,"application/futuresplash","Fichiers Flash Futuresplash",".spl"),
	(23,"application/gzip","Fichiers GNU Zip",".gz"),
	(24,"application/listenup","Fichiers Listenup",".ptlk"),
	(25,"application/mac-binhex40","Fichiers binaires Macintosh ",".hqx"),
	(26,"application/mbedlet","Fichiers Mbedlet",".mbd"),
	(27,"application/mif","Fichiers FrameMaker Interchange Format",".mif"),
	(28,"application/msexcel","Fichiers Microsoft Excel",".xls .xla .xlsx .xlsm .xlam .xlt .xltx .xltm"),
	(29,"application/mshelp","Fichiers d\'aide Microsoft Windows",".hlp .chm"),
	(30,"application/mspowerpoint","Fichiers Microsoft Powerpoint",".ppt .ppz .pps .pot .potx .potm .pptx .pptm .ppsm .ppsx .ppam .ppa"),
	(31,"application/msword","Fichiers Microsoft Word",".doc .dot .dotx .dotm .docx .docm"),
	(32,"application/octet-stream","Fichiers executables",".bin .exe .com .dll .class"),
	(33,"application/oda","Fichiers Oda",".oda"),
	(34,"application/ooimpress","Fichiers oo impress",".odp"),
	(35,"application/pdf","Fichiers Adobe PDF",".pdf"),
	(36,"application/postscript","Fichiers Adobe Postscript",".ai .eps .ps"),
	(37,"application/rtc","Fichiers RTC",".rtc"),
	(38,"application/rtf","Fichiers Microsoft RTF",".rtf"),
	(39,"application/studiom","Fichiers Studiom",".smp"),
	(40,"application/toolbook","Fichiers Toolbook",".tbk"),
	(41,"application/vnd.wap.wmlc","Fichiers WMLC (WAP)",".wmlc"),
	(42,"application/vnd.wap.wmlscriptc","Fichiers script C WML (WAP)",".wmlsc"),
	(43,"application/vocaltec-media-desc","Fichiers Vocaltec Mediadesc",".vmd"),
	(44,"application/vocaltec-media-file","Fichiers Vocaltec Media",".vmf"),
	(45,"application/x-bcpio","Fichiers BCPIO",".bcpio"),
	(46,"application/x-compress","Fichiers -",".z"),
	(47,"application/x-cpio","Fichiers CPIO",".cpio"),
	(48,"application/x-csh","Fichiers C-Shellscript",".csh"),
	(49,"application/x-director","Fichiers -",".dcr .dir .dxr"),
	(50,"application/x-dvi","Fichiers DVI",".dvi"),
	(51,"application/x-envoy","Fichiers Envoy",".evy"),
	(52,"application/x-gtar","Fichiers archives GNU tar",".gtar"),
	(53,"application/x-hdf","Fichiers HDF",".hdf"),
	(54,"application/x-httpd-php","Fichiers PHP",".php .phtml"),
	(55,"application/x-javascript","Fichiers JavaScript cote serveur",".js"),
	(56,"application/x-latex","Fichiers source Latex",".latex"),
	(57,"application/x-mif","Fichiers FrameMaker Interchange Format",".mif"),
	(58,"application/x-netcdf","Fichiers Unidata CDF",".nc .cdf"),
	(59,"application/x-nschat","Fichiers NS Chat",".nsc"),
	(60,"application/x-sh","Fichiers Bourne Shellscript",".sh"),
	(61,"application/x-shar","Fichiers atchives Shell",".shar"),
	(62,"application/x-shockwave-flash","Fichiers Flash Shockwave",".swf .cab"),
	(63,"application/x-sprite","Fichiers Sprite",".spr .sprite"),
	(64,"application/x-stuffit","Fichiers Stuffit",".sit"),
	(65,"application/x-supercard","Fichiers Supercard",".sca"),
	(66,"application/x-sv4cpio","Fichiers CPIO",".sv4cpio"),
	(67,"application/x-sv4crc","Fichiers CPIO avec CRC",".sv4crc"),
	(68,"application/x-tar","Fichiers archives tar",".tar"),
	(69,"application/x-tcl","Fichiers script TCL",".tcl"),
	(70,"application/x-tex","Fichiers TEX",".tex"),
	(71,"application/x-texinfo","Fichiers TEXinfo",".texinfo .texi"),
	(72,"application/x-troff","Fichiers TROFF (Unix)",".t .tr .roff"),
	(73,"application/x-troff-man","Fichiers TROFF avec macros MAN (Unix)",".man .troff"),
	(74,"application/x-troff-me","Fichiers TROFF avec macros ME (Unix)",".me .troff"),
	(75,"application/x-troff-ms","Fichiers TROFF avec macros MS (Unix)",".me .troff"),
	(76,"application/x-ustar","Fichiers archives tar (Posix)",".ustar"),
	(77,"application/x-wais-source","Fichiers source WAIS",".src"),
	(78,"application/zip","Fichiers archives ZIP",".zip .Z"),
	(79,"application/tar","Fichiers archives TAR",".tar"),
	(80,"audio/basic","Fichiers son",".au .snd"),
	(81,"audio/echospeech","Fichiers Echospeed",".es"),
	(82,"audio/tsplayer","Fichiers TS-Player",".tsi"),
	(83,"audio/voxware","Fichiers Vox",".vox"),
	(84,"audio/x-aiff","Fichiers son AIFF",".aif .aiff .aifc"),
	(85,"audio/x-dspeeh","Fichiers parole",".dus .cht"),
	(86,"audio/x-midi","Fichiers MIDI",".mid .midi"),
	(87,"audio/x-mpeg","Fichiers MPEG",".mp2"),
	(88,"audio/x-pn-realaudio","Fichiers RealAudio",".ram .ra"),
	(89,"audio/x-pn-realaudio-plugin","Fichiers plugin RealAudio",".rpm"),
	(90,"audio/x-qt-stream","Fichiers -",".stream"),
	(91,"audio/x-wav","Fichiers Wav",".wav"),
	(92,"application/i-deas","Fichiers SDRC I-deas",".unv"),
	(93,"application/cao__iges","Format d\'echange CAO IGES",".igs"),
	(94,"application/proeng","Fichiers ProEngineer",".prt"),
	(95,"application/pstree","product ps","_ps"),
	(96,"application/set","Fichiers CAO SET",".set"),
	(97,"application/sla","Fichiers stereolithographie",".stl"),
	(98,"application/step","Fichiers de donnees STEP",".step .stp"),
	(99,"application/vda","Fichiers de surface",".vda"),
	(100,"drawing/x-dwf","Fichiers Drawing",".dwf"),
	(101,"image/cis-cod","Fichiers CIS-Cod",".cod"),
	(102,"image/cmu-raster","Fichiers CMU-Raster",".ras"),
	(103,"image/fif","Fichiers FIF",".fif"),
	(104,"image/gif","Fichiers GIF",".gif"),
	(105,"image/ief","Fichiers IEF",".ief"),
	(106,"image/jpeg","Fichiers JPEG",".jpeg .jpg .jpe"),
	(107,"image/tiff","Fichiers TIFF",".tiff .tif"),
	(108,"image/vasa","Fichiers Vasa",".mcf"),
	(109,"image/vnd.wap.wbmp","Fichiers Bitmap (WAP)",".wbmp"),
	(110,"image/x-freehand","Fichiers Freehand",".fh4 .fh5 .fhc"),
	(111,"image/x-portable-anymap","Fichiers PBM Anymap",".pnm"),
	(112,"image/x-portable-bitmap","Fichiers Bitmap PBM",".pbm"),
	(113,"image/x-portable-graymap","Fichiers PBM Graymap",".pgm"),
	(114,"image/x-portable-pixmap","Fichiers PBM Pixmap",".ppm"),
	(115,"image/x-rgb","Fichiers RGB",".rgb"),
	(116,"image/x-windowdump","X-Windows Dump",".xwd"),
	(117,"image/x-xbitmap","Fichiers XBM",".xbm"),
	(118,"image/x-xpixmap","Fichiers XPM",".xpm"),
	(119,"image/svg+xml","Scalable vector graphic",".svg"),
	(120,"text/comma-separated-values","Fichiers de donnees separees par des virgules",".csv"),
	(121,"text/css","Fichiers de feuilles de style CSS",".css"),
	(122,"text/html","Fichiers -",".htm .html .shtml"),
	(123,"text/javascript","Fichiers JavaScript",".js"),
	(124,"text/plain","Fichiers pur texte",".txt"),
	(125,"text/richtext","Fichiers texte enrichi (Richtext)",".rtx"),
	(126,"text/dat","Fichiers dat",".dat"),
	(127,"text/rtf","Fichiers Microsoft RTF",".rtf"),
	(128,"text/tab-separated-values","Fichiers de donnees separees par des tabulations",".tsv"),
	(129,"text/vnd.wap.wml","Fichiers WML (WAP)",".wml"),
	(130,"text/vnd.wap.wmlscript","Fichiers script WML (WAP)",".wmls"),
	(131,"text/x-setext","Fichiers SeText",".etx"),
	(132,"text/x-sgml","Fichiers SGML",".sgm .sgml"),
	(133,"text/x-speech","Fichiers Speech",".talk .spc"),
	(134,"text/xml","Fichiers xml",".xml"),
	(135,"video/mpeg","Fichiers MPEG",".mpeg .mpg .mpe"),
	(136,"video/quicktime","Fichiers Quicktime",".qt .mov"),
	(137,"video/vnd.vivo","Fichiers Vivo","viv .vivo"),
	(138,"video/x-msvideo","Fichiers Microsoft AVI",".avi"),
	(139,"video/x-sgi-movie","Fichiers Movie",".movie"),
	(140,"workbook/formulaone","Fichiers FormulaOne",".vts .vtts"),
	(141,"application/vnd.oasis.opendocument.text","Open office text",".odt"),
	(142,"application/vnd.oasis.opendocument.text-template","Open office text template",".ott"),
	(143,"application/vnd.oasis.opendocument.text-web","Open office text web",".oth"),
	(144,"application/vnd.oasis.opendocument.text-master","Open office text master",".odm"),
	(145,"application/vnd.oasis.opendocument.graphics","Open office",".odg"),
	(146,"application/vnd.oasis.opendocument.graphics-template","Open office",".otg"),
	(147,"application/vnd.oasis.opendocument.presentation","Open office",".odp"),
	(148,"application/vnd.oasis.opendocument.presentation-template","Open office",".otp"),
	(149,"application/vnd.oasis.opendocument.spreadsheet","Open office",".ods"),
	(150,"application/vnd.oasis.opendocument.spreadsheet-template","Open office",".ots"),
	(151,"application/vnd.oasis.opendocument.chart","Open office",".odc"),
	(152,"application/vnd.oasis.opendocument.formula","Open office",".odf"),
	(153,"application/vnd.oasis.opendocument.database","Open office",".odb"),
	(154,"application/vnd.oasis.opendocument.image","Open office",".odi");
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class TypemimeDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='typemimes';

	/**
	 *
	 * @var string
	 */
	public static $vtable='typemimes';

	/**
	 * @var string
	 */
	public static $sequenceName=null;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'name'=>'name',
		'extensions'=>'extensions',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
	);
	
	
	/**
	 * 
	 * @return \Rbs\Typemime\TypemimeDao
	 */
	public function getTypes()
	{
		$conn = $this->connexion;
		$sql = "SELECT * FROM typemimes ORDER BY uid ASC";
		$stmt = $conn->query($sql);
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
}
