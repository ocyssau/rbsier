<?php
//%LICENCE_HEADER%
namespace Rbs\History;

use Rbplm\Any;
use Rbplm\People;
use Rbplm\Mapped;
use Rbplm\Dao\MappedInterface;

/**
 *
 */
class AbstractHistory extends Any implements MappedInterface
{
	use Mapped;

	static $classId = '598a45cf2hist';

	/**
	 *
	 * @var Action
	 */
	protected $action;

	/**
	 *
	 * @var array
	 */
	protected $data;

	/**
	 *
	 * @var \Rbs\Dao\Sier
	 */
	public $dao;

	/**
	 *
	 * @param array $properties
	 * @param Any $parent
	 */
	public function __construct($properties = null, $parent = null)
	{
		parent::__construct($properties, $parent);
		$this->action = new Action();
	}

	/**
	 * @param string $name
	 * @return Any
	 */
	public static function init()
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		$action = $obj->getAction();
		$action->newUid();
		$action->setCreated(new \DateTime());
		$action->setOwner(People\CurrentUser::get());
		$name = uniqid();
		$obj->setName($name);
		$action->setName($name);
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Any
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['action'])) ? $this->action->hydrate($properties['action']) : null;
		(isset($properties['data'])) ? $this->data = $properties['data'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getArrayCopy()
	{
		$ret = $this->__serialize();
		$ret['action'] = $this->action->__serialize();
		return $ret;
	}

	/**
	 * @param Action $action
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 *
	 * @param array $data
	 */
	public function setData($data)
	{
		/*Clean references*/
		$return = array();

		foreach( $data as $name => $value ) {
			if ( $value instanceof Any ) {
				$return[$name] = $value->getUid();
			}
			elseif ( is_scalar($value) ) {
				$return[$name] = $value;
			}
			elseif ( !$value ) {
				$return[$name] = null;
			}
		}

		$this->data = $return;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 *
	 * @param \Rbs\Dao\Sier $dao
	 */
	public function setDao($dao)
	{
		$this->dao = $dao;
		return $this;
	}
} /* End of class */
