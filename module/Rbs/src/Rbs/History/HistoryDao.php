<?php
//%LICENCE_HEADER%
namespace Rbs\History;

use Rbs\Dao\Sier as DaoSier;
use Rbs\Dao\Sier\MetamodelTranslator;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 */
abstract class HistoryDao extends DaoSier
{

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'histo_order' => 'action_id',
		'action_name' => 'action_name',
		'action_by' => 'action_ownerUid',
		'action_started' => 'action_created',
		'action_comment' => 'action_comment',
		'data' => 'data'
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array();

	/**
	 * @return HistoryDao
	 */
	public function deleteFromId($id, $withChildren = true, $withTrans = null)
	{
		$toSysId = $this->toSys('action_id');
		$filter = "$toSysId=:id";
		$bind = array(
			':id' => $id
		);
		
		$this->_deleteFromFilter($filter, $bind, $withChildren, $withTrans);
		return $this;
	}

	/**
	 * @return HistoryDao
	 */
	public function deleteFromDocumentId($id, $withChildren = true, $withTrans = null)
	{
		$toSysId = $this->toSys('data_id');
		$filter = "$toSysId=:id";
		$bind = array(
			':id' => $id
		);
		$this->_deleteFromFilter($filter, $bind, $withChildren, $withTrans);
		return $this;
	}

	/**
	 *
	 * @param \Rbs\History\AbstractHistory $mapped
	 */
	public function bind($mapped)
	{
		return $this->_bind($mapped);
	}

	/**
	 * Action property name is mapped to action_$name in Dao
	 * Object property name is mapped to data_$name in Dao
	 * History property name is mapped to $name in Dao
	 * 
	 * @param \Rbs\History\AbstractHistory $mapped
	 * @return array
	 */
	protected function _bind($mapped)
	{
		/* bind to PDO */
		$bind = array();
		/* temp array to consolidate data_ and action_ */
		$properties = array();
		
		foreach( $mapped->getAction()->getArrayCopy() as $k => $v ) {
			$properties['action_' . $k] = $v;
		}
		
		$data = $mapped->getData();
		foreach( $data as $k => $v ) {
			$properties['data_' . $k] = $v;
		}
		
		$properties['data'] = json_encode($data, true);
		$properties['action_id'] = $mapped->getId();
		
		$sysToApp = $this->metaModel;
		$sysToAppFilter = $this->metaModelFilters;
		
		foreach( $sysToApp as $sysName => $appName ) {
			if ( array_key_exists($appName, $properties) ) {
				if ( isset($sysToAppFilter[$sysName]) && array_key_exists($appName, $properties) ) {
					$filterMethod = $sysToAppFilter[$sysName] . 'ToSys';
					$bind[':' . $appName] = MetamodelTranslator::$filterMethod($properties[$appName]);
				}
				else {
					$bind[':' . $appName] = $properties[$appName];
				}
			}
		}
		
		$this->lastBind = $bind;
		return $bind;
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.DaoInterface::hydrate()
	 *
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbs\History\AbstractHistory $mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean $fromApp		If true, assume that keys $row are name of properties as set in  model, else are set as in persitence system.
	 * @return \Rbs\History\AbstractHistory
	 */
	public function hydrate($mapped, array $row, $fromApp = false)
	{
		$properties = array();
		
		if ( $fromApp ) {
			foreach( $this->metaModel as $asSys => $asApp ) {
				$properties[$asApp] = $row[$asApp];
			}
		}
		else {
			$sysToAppFilter = $this->metaModelFilters;
			foreach( $this->metaModel as $asSys => $asApp ) {
				$asApp = str_replace(array(
					'data_',
					'action_'
				), '', $asApp);
				if ( isset($sysToAppFilter[$asSys]) ) {
					$filterMethod = $sysToAppFilter[$asSys] . 'ToApp';
					$properties[$asApp] = static::$filterMethod($row[$asSys]);
				}
				else {
					$properties[$asApp] = $row[$asSys];
				}
			}
		}
		
		$mapped->hydrate($properties);
		return $mapped;
	}
}
