<?php

// %LICENCE_HEADER%
namespace Rbs\History;

use Rbplm\Any;
use Rbplm\Owned;
use DateTime;

/**
 */
class Action extends Any
{

	use Owned;

	static $classId = '598a45cf2acti';

	/**
	 *
	 * @var DateTime
	 */
	protected $created = null;

	/**
	 *
	 * @var string
	 */
	protected $comment = '';

	/**
	 *
	 * @var \Rbs\Batch\Batch
	 */
	protected $batch = null;

	/**
	 * 
	 * @var integer
	 */
	public $batchId = null;

	/**
	 * 
	 * @var string
	 */
	public $batchUid = null;

	/**
	 *
	 * @param DateTime $date        	
	 * @return Action
	 */
	public function setCreated(\DateTime $date)
	{
		$this->created = $date;
		return $this;
	}

	/**
	 *
	 * @return DateTime
	 */
	public function getCreated($format = null)
	{
		if ( !$this->created instanceof DateTime ) {
			$this->created = new DateTime();
		}

		if ( $format ) {
			return $this->created->format($format);
		}
		else {
			return $this->created;
		}
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Action
	 */
	public function hydrate(array $properties)
	{
		$this->ownedHydrate($properties);
		(isset($properties['created'])) ? $this->setCreated(new DateTime($properties['created'])) : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['batchId'])) ? $this->batchId = $properties['batchId'] : null;
		(isset($properties['batchUid'])) ? $this->batchUid = $properties['batchUid'] : null;
		(isset($properties['comment'])) ? $this->comment = $properties['comment'] : null;
		return $this;
	}

	/**
	 *
	 * @param array $datas        	
	 * @return Action
	 */
	public function setDatas($datas)
	{
		$this->datas = $datas;
		return $this;
	}

	/**
	 *
	 * @return array $datas
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 *
	 * @param string $string
	 * @return Action
	 */
	public function setComment($string)
	{
		$this->comment = (string)$string;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 *
	 * @param \Rbs\Batch\Batch $batch
	 * @return Action
	 */
	public function setBatch(\Rbs\Batch\Batch $batch)
	{
		$this->batch = $batch;
		$this->batchId = $batch->getId();
		$this->batchUid = $batch->getUid();
		return $this;
	}

	/**
	 *
	 * @return \Rbs\Batch\Batch
	 */
	public function getBatch()
	{
		return $this->batch;
	}
}/* End of class */
