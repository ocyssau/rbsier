<?php
namespace Rbs\History;

use Rbs\Batch\Batch;
use Rbs\Space\Factory as DaoFactory;

/**
 * 
 *
 */
interface HistoryEventInterface
{

	/**
	 * @param Batch $batch
	 * @return EventTrait
	 */
	public function setBatch(Batch $batch);

	/**
	 * @return Batch
	 */
	public function getBatch();

	/**
	 * @param string $string
	 * @return EventTrait
	 */
	public function setComment(string $string);

	/**
	 * @return string
	 */
	public function getComment();

	/**
	 *
	 * @return DaoFactory
	 */
	public function getFactory();
}
