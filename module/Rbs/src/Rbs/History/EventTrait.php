<?php
namespace Rbs\History;

use Rbs\Batch\Batch;

/**
 * Event emit when a history trace is required
 * Implement HistoryEventInterface
 *
 */
trait EventTrait
{

	/**
	 *
	 * @var string
	 */
	protected $comment;

	/**
	 *
	 * @var string
	 */
	protected $batch;

	/**
	 * @param Batch $batch
	 * @return EventTrait
	 */
	public function setBatch(Batch $batch)
	{
		$this->batch = $batch;
		$this->comment = $batch->getComment();
		return $this;
	}

	/**
	 * @return Batch
	 */
	public function getBatch()
	{
		return $this->batch;
	}

	/**
	 * @param string $string
	 * @return EventTrait
	 */
	public function setComment(string $string)
	{
		$this->comment = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}
}
