<?php
//%LICENCE_HEADER%

namespace Rbs\Wf;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `wf_activity`(
	`id` int(11) NOT NULL,
	`uid` VARCHAR(64)  NOT NULL,
	`cid` VARCHAR(64) NOT NULL DEFAULT '56acc299ed0e4',
	`name` VARCHAR(128) DEFAULT NULL,
	`title` TEXT DEFAULT NULL,
	`ownerId` VARCHAR(255) DEFAULT NULL,
	`updated` DATETIME DEFAULT NULL,
	`updateById` VARCHAR(64) DEFAULT NULL,
	`parentId` INT(11) DEFAULT NULL,
	`parentUid` VARCHAR(255) DEFAULT NULL,
	`type` ENUM('start','end','split','switch','join','activity','standalone') DEFAULT NULL,
	`normalizedName` VARCHAR(256) NULL,
	`isInteractive` TINYINT(1) default 0,
	`isAutorouted` TINYINT(1) default 0,
	`isAutomatic` TINYINT(1) default 0,
	`isComment` TINYINT(1) default 0,
	`processId` INT(11) NULL,
	`progression` float NULL,
	`expirationTime` DATETIME NULL,
	`roles` MEDIUMTEXT NULL,
	`attributes` MEDIUMTEXT NULL,
	PRIMARY KEY (`id`),
	UNIQUE (uid),
	UNIQUE (`name` ,`processId`),
	INDEX (`normalizedName` ASC),
	INDEX (`uid` ASC),
	INDEX (`name` ASC),
	INDEX (`parentUid` ASC),
	INDEX (`parentId` ASC),
	INDEX (`cid` ASC),
	INDEX (`ownerId` ASC),
	INDEX (`processId` ASC),
	INDEX (roles(100) ASC)
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE `wf_activity` 
ADD CONSTRAINT `FK_wf_activity_1`
  FOREIGN KEY (`processId`)
  REFERENCES `wf_process` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class ActivityDao extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table = 'wf_activity';

	/**
	 * @var string
	 */
	public static $vtable='wf_activity';

	/**
	 * @var string
	 */
	public static $sequenceName='wf_seq';
	
	/**
	 * 
	 * @var \PDOStatement
	 */
	protected $deleteFromProcessStmt;

	/**
	 * @var array
	 */
	static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'cid'=>'cid',
		'name'=>'name',
		'title'=>'title',
		'ownerId'=>'ownerId',
		'updated'=>'updated',
		'updateById'=>'updateById',
		'parentId'=>'parentId',
		'parentUid'=>'parentUid',
		'processId'=>'processId',
		'normalizedName'=>'normalizedName',
		'type'=>'type',
		'roles'=>'roles',
		'isInteractive'=>'isInteractive',
		'isAutorouted'=>'isAutorouted',
		'isAutomatic'=>'isAutomatic',
		'isComment'=>'isComment',
		'expirationTime'=>'expirationTime',
		'progression'=>'progression',
		'attributes'=>'attributes',
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'updated'=>'datetime',
		'expirationTime'=>'datetime',
		'attributes'=>'json',
		'roles'=>'json',
		'isInteractive'=>'boolean',
		'isAutorouted'=>'boolean',
		'isAutomatic'=>'boolean',
		'isComment'=>'boolean',
	);

	/**
	 * Delete all activities of a process
	 * @param integer $processId
	 */
	public function deleteFromProcess($processId)
	{
		if(!$this->deleteFromProcessStmt){
			$table = $this->_table;
			$sql = "DELETE FROM $table WHERE processId = :processId";
			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}
		$this->deleteFromProcessStmt->execute(array(':processId'=>$processId));
		return $this;
	}

} /* End of class */
