<?php
// %LICENCE_HEADER%
namespace Rbs\Wf;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;
use Workflow\Model\Wf;
use Exception;
use Ranchbe;

/**SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `wf_instance`(
 `id` INT(11) NOT NULL,
 `uid` VARCHAR(255) NOT NULL,
 `cid` VARCHAR(32) NOT NULL DEFAULT '56acc299ed1bd',
 `name` VARCHAR(255) NULL,
 `title` VARCHAR(255) NULL,
 `ownerId` VARCHAR(255) NULL,
 `parentId` INT(11) NULL,
 `parentUid` VARCHAR(255) NULL,
 `updateById` VARCHAR(255) NULL,
 `updated` DATETIME NULL,
 `processId` INT(11) NULL,
 `status` VARCHAR(64) NULL,
 `nextActivities` MEDIUMTEXT NULL,
 `nextUsers` MEDIUMTEXT NULL,
 `attributes` MEDIUMTEXT NULL,
 `started` DATETIME NULL,
 `ended` DATETIME NULL,
 PRIMARY KEY (`id`),
 UNIQUE (uid),
 INDEX (`name` ASC),
 INDEX (`parentUid` ASC),
 INDEX (`parentId` ASC),
 INDEX (`cid` ASC),
 INDEX (`ownerId` ASC),
 INDEX (`processId` ASC),
 INDEX (nextActivities(100) ASC),
 INDEX (nextUsers(100) ASC),
 INDEX (`started` ASC),
 INDEX (`ended` ASC)
 );

 CREATE TABLE IF NOT EXISTS `wf_instance_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=MyISAM AUTO_INCREMENT=100;
 INSERT INTO `wf_instance_seq` (`id`) VALUES (100);

 << */

/**SQL_INSERT>>
 << */

/**SQL_ALTER>>
 ALTER TABLE `wf_instance` 
 ADD CONSTRAINT `FK_wf_instance_1`
 FOREIGN KEY (`processId`)
 REFERENCES `wf_process` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE;
 << */

/**SQL_FKEY>>
 << */

/**SQL_TRIGGER>>
 << */

/**SQL_VIEW>>
 << */

/**SQL_DROP>>
 << */

/**
 * 
 *
 */
class InstanceDao extends DaoSier
{

	/** @var string */
	public static $table = 'wf_instance';

	/** @var string */
	public static $vtable = 'wf_instance';

	/** @var string */
	public static $sequenceName = 'wf_instance_seq';

	/** @var string */
	public static $sequenceKey = 'id';

	/**
	 * 
	 * @var \PDOStatement
	 */
	protected $deleteFromProcessStmt;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'name' => 'name',
		'title' => 'title',
		'ownerId' => 'ownerId',
		'updated' => 'updated',
		'updateById' => 'updateById',
		'parentId' => 'parentId',
		'parentUid' => 'parentUid',
		'processId' => 'processId',
		'status' => 'status',
		'nextActivities' => 'nextActivities',
		'nextUsers' => 'nextUsers',
		'attributes' => 'attributes',
		'started' => 'started',
		'ended' => 'ended'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'updated' => 'datetime',
		'started' => 'datetime',
		'ended' => 'datetime',
		'properties' => 'json',
		'nextActivities' => 'json',
		'nextUsers' => 'json'
	);

	/**
	 * Constructor
	 *
	 * @param
	 *        	\PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
		$this->metaModelFilters = self::$sysToAppFilter;
	}

	/**
	 * Load a instance from Instance JOIN with Activity
	 *
	 * @todo Escape sql injection
	 *      
	 * @param \Rbplm\Any $mapped
	 * @param string $filter
	 * @return \Rbplm\Any
	 * @throws Exception
	 */
	public function load(MappedInterface $mapped, $filter = null, $bind = null)
	{
		if ( $mapped->isLoaded() == true ) {
			Ranchbe::get()->log('Object ' . $mapped->getUid() . ' is yet loaded');
			return;
		}

		/* select field of activity */
		$select = implode(',', array(
			'obj.*',
			'proc.isActive',
			'proc.isValid',
			'proc.normalizedName',
			'proc.version'
		));
		$sql = "SELECT $select FROM wf_instance AS obj JOIN wf_process AS proc ON obj.processId=proc.id";

		if ( is_a($filter, '\Rbplm\Dao\Filter\FilterInterface') ) {
			$filter = $filter->__toString();
		}
		if ( $filter ) {
			$sql .= " WHERE $filter";
		}

		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		$row = $stmt->fetch();

		if ( $row ) {
			$this->hydrate($mapped, $row);
		}
		else {
			throw new Exception(sprintf('CAN_NOT_BE_LOAD_FROM_%s', $sql));
		}
		$mapped->isLoaded(true);
		return $mapped;
	}

	/**
	 * Delete all transitions of a process
	 *
	 * @param integer $processId        	
	 */
	public function deleteFromProcess($processId)
	{
		if ( !$this->deleteFromProcessStmt ) {
			$table = static::$table;
			$sql = "DELETE FROM $table WHERE processId=:processId";
			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}
		$this->deleteFromProcessStmt->execute(array(
			':processId' => $processId
		));
		return $this;
	}

	/**
	 * Getter for runningActivities.
	 * Return also properties of activity object as :
	 * type, isComment, isInteractive, isAutomatic
	 *
	 * @param integer $processInstanceId        	
	 * @return array
	 */
	public function getRunningActivities($processInstanceId)
	{
		$status = Wf\Instance\Activity::STATUS_RUNNING;

		$sql = "SELECT actinst.*, act.type, act.isComment, act.isInteractive, act.isAutomatic
		FROM wf_instance_activity AS actinst
		JOIN wf_activity AS act ON actinst.activityId=act.id
		WHERE actinst.status=:status AND actinst.instanceId = :instanceId";

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':status' => $status,
			':instanceId' => $processInstanceId
		));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Get Activities children of Activity linked to Instance Activity identified by $actInstId
	 *
	 * Return all properties of Wf\Activity object in App sementic
	 * with nextStatus attach to transition
	 * and process instance id as instanceId
	 *
	 * $selectActivities may contain a list of id of Activity to load
	 * $selectTransition may contain a list of name of transitions
	 *
	 * @param integer $activityId        	
	 * @param array $selectActivities        	
	 * @param array $selectTransition        	
	 * @return array
	 */
	public function getNextCandidates($actInstId, $selectActivities = null, $selectTransition = null)
	{
		$filter = '';
		$bind = array(
			':actinstId' => $actInstId
		);

		if ( $selectActivities ) {
			if ( is_string($selectActivities) ) {
				$filter = ' AND act.id=' . $selectActivities;
			}
			elseif ( is_array($selectActivities) ) {
				$filter = ' AND act.id IN (' . implode(',', $selectActivities) . ')';
			}
		}

		if ( $selectTransition ) {
			if ( is_string($selectTransition) ) {
				$bind[':transition'] = $selectTransition;
				$filter = ' AND trans.name=:transition';
			}
			elseif ( is_array($selectTransition) ) {
				$s = [];
				$i = 0;
				foreach( $selectTransition as $t ) {
					$bind[':transition' . $i] = $t;
					$s[] = ':transition' . $i;
					$i++;
				}
				$filter = ' AND trans.name IN (' . implode(',', $s) . ')';
			}
		}

		$sql = "SELECT act.*, trans.name AS nextStatus, actinst.instanceId AS instanceId
				FROM wf_activity AS act
				JOIN wf_transition AS trans ON act.id = trans.childId
				JOIN (SELECT activityId, instanceId FROM wf_instance_activity WHERE id=:actinstId) AS actinst
				WHERE trans.parentId = actinst.activityId" . $filter;

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);

		/* Apply filters to data and convert to app sementic */
		$asApp = array();
		foreach( $stmt->fetchAll(\PDO::FETCH_ASSOC) as $asSys ) {
			$asApp[] = $asSys;
		}

		return $asApp;
	}

	/**
	 *
	 * Return all properties of Wf\Activity object in App sementic
	 * with nextStatus attach to transition
	 * and process instance id as instanceId
	 *
	 * $selectActivities may contain a list of id of Activity to load
	 * $selectTransition may contain a list of name of transitions
	 *
	 * @param integer $actId
	 * @param integer $processInstanceId
	 * @param array $selectActivities
	 * @param array $selectTransition
	 * @return array
	 */
	public function getNextCandidatesFromActivityAndProcessinst($actId, $processInstanceId, $selectActivities = null, $selectTransition = null)
	{
		$filter = '';
		$bind = array(
			':actId' => $actId,
			':processInstanceId' => $processInstanceId
		);

		if ( $selectActivities ) {
			if ( is_string($selectActivities) ) {
				$filter = ' AND act.id=' . $selectActivities;
			}
			elseif ( is_array($selectActivities) ) {
				$filter = ' AND act.id IN (' . implode(',', $selectActivities) . ')';
			}
		}

		if ( $selectTransition ) {
			if ( is_string($selectTransition) ) {
				$bind[':transition'] = $selectTransition;
				$filter = ' AND trans.name=:transition';
			}
			elseif ( is_array($selectTransition) ) {
				$s = [];
				$i = 0;
				foreach( $selectTransition as $t ) {
					$bind[':transition' . $i] = $t;
					$s[] = ':transition' . $i;
					$i++;
				}
				$filter = ' AND trans.name IN (' . implode(',', $s) . ')';
			}
		}

		$sql = "SELECT act.*, trans.name AS nextStatus";
		$sql .= " FROM wf_instance AS inst";
		$sql .= " JOIN wf_process AS proc ON inst.processId=proc.id";
		$sql .= " JOIN wf_activity AS act ON act.processId=proc.id";
		$sql .= " JOIN wf_transition AS trans ON trans.childId=act.id";
		$sql .= " WHERE trans.parentId = :actId AND inst.id=:processInstanceId";
		$sql .= $filter;

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);

		/* Apply filters to data and convert to app sementic */
		$asApp = array();
		foreach( $stmt->fetchAll(\PDO::FETCH_ASSOC) as $asSys ) {
			$asApp[] = $asSys;
		}

		return $asApp;
	}

	/**
	 * Get Transition children of Activity linked to Instance Activity identified by $actInstId
	 *
	 * @param integer $activityId        	
	 * @return array
	 */
	public function getNextTransitions($actInstId)
	{
		$sql = "SELECT trans.*, actinst.instanceId AS instanceId, act.name as actName
		FROM wf_activity AS act
		JOIN wf_activity AS trans ON act.activityId = trans.actToId
		JOIN (SELECT activityId, instanceId FROM wf_instance_activities WHERE activityId=:activityId) AS actinst
		WHERE trans.parentId = actinst.activityId";

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':activityId' => $actInstId
		));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
}
