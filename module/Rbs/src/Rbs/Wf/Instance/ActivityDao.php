<?php
//%LICENCE_HEADER%
namespace Rbs\Wf\Instance;

use Rbs\Dao\Sier as DaoSier;
use Rbs\Wf;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS wf_instance_activity(
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(64)  NOT NULL,
 `cid` VARCHAR(64) NOT NULL DEFAULT '56acc299ed22a',
 `name` VARCHAR(128) DEFAULT NULL,
 `title` VARCHAR(256) DEFAULT NULL,
 `ownerId` VARCHAR(64) DEFAULT NULL,
 `updated` datetime DEFAULT NULL,
 `updateById` VARCHAR(64) DEFAULT NULL,
 `parentId` INT(11) DEFAULT NULL,
 `parentUid` VARCHAR(128) DEFAULT NULL,
 `instanceId` INT(11) NOT NULL,
 `activityId` INT(11) NOT NULL,
 `status` ENUM('running','completed') NULL,
 `type` ENUM('start','end','split','switch','join','activity','standalone') DEFAULT NULL,
 `started` datetime NULL,
 `ended` datetime NULL,
 `attributes` MEDIUMTEXT NULL,
 `comment` MEDIUMTEXT NULL,
 PRIMARY KEY (`id`),
 UNIQUE (uid),
 INDEX (`uid` ASC),
 INDEX (`name` ASC),
 INDEX (`parentUid` ASC),
 INDEX (`parentId` ASC),
 INDEX (`cid` ASC),
 INDEX (`ownerId` ASC),
 INDEX (`instanceId` ASC),
 INDEX (`activityId` ASC),
 INDEX (`type` ASC),
 INDEX (`started` ASC),
 INDEX (`ended` ASC),
 CONSTRAINT `FK_wf_instance_activity_1` FOREIGN KEY (`activityId`) REFERENCES `wf_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `FK_wf_instance_activity_2` FOREIGN KEY (`instanceId`) REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
 );
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class ActivityDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'wf_instance_activity';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'wf_instance_activity';

	/**
	 * @var string
	 */
	public static $sequenceName = 'wf_instance_seq';

	/**
	 * @var array
	 */
	static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'name' => 'name',
		'title' => 'title',
		'ownerId' => 'ownerId',
		'updated' => 'updated',
		'updateById' => 'updateById',
		'parentId' => 'parentId',
		'parentUid' => 'parentUid',
		'instanceId' => 'instanceId',
		'activityId' => 'activityId',
		'status' => 'status',
		'type' => 'type',
		'started' => 'started',
		'ended' => 'ended',
		'attributes' => 'attributes',
		'comment' => 'comment'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'updated' => 'datetime',
		'started' => 'datetime',
		'ended' => 'datetime',
		'expirationTime' => 'datetime',
		'attributes' => 'json',
		'roles' => 'json'
	);

	/** @var \PDOStatement */
	protected $deleteFromProcessStmt;

	/**
	 */
	public function loadWithActivity($mapped, $filter = null, $bind = null)
	{
		$table = $this->_table;

		/* select field of activity */
		$select = implode(',', array(
			'obj.*',
			'act.processId',
			'act.type',
			'act.isInteractive',
			'act.isAutorouted',
			'act.isAutomatic',
			'act.isComment',
			'act.expirationTime',
			'act.roles',
			'act.attributes AS actattributes',
			'act.name as actname'
		));

		$sql = "SELECT $select FROM $table AS obj JOIN wf_activity AS act ON obj.activityId=act.id";
		if ( $filter ) {
			$sql .= " WHERE $filter";
		}
		return $this->loadFromSql($mapped, $sql, $bind);
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * Rewrite the Dao hydrator to specify properties different when load and when save
	 * ActivityInstance is populate with instance and activity properties
	 *
	 * @param \Rbplm\Any    				$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return \Rbplm\AnyObject
	 */
	public function hydrate($mapped, array $row, $fromApp = false)
	{
		$natifMetaModel = $this->metaModel;
		$metaModel = array_merge($natifMetaModel, array(
			'processId' => 'processId',
			'isInteractive' => 'isInteractive',
			'isAutorouted' => 'isAutorouted',
			'isAutomatic' => 'isAutomatic',
			'isComment' => 'isComment',
			'expirationTime' => 'expirationTime',
			'roles' => 'roles',
			'actattributes' => 'attributes',
			'actname' => 'name',
			'type' => 'type'
		));

		/* temporary extends metamodel */
		$this->metaModel = $metaModel;
		parent::hydrate($mapped, $row, $fromApp);
		$this->metaModel = $natifMetaModel;

		return $mapped;
	}

	/**
	 * Getter for previous. Return a list.
	 *
	 * @param Any
	 * @return array
	 */
	public function getPrevious($mapped)
	{}

	/**
	 * Getter for comments. Return a list.
	 *
	 * @param Any
	 * @return array
	 */
	public function getComments($mapped)
	{}

	/**
	 * Delete all transitions of a process
	 * @param integer $processId
	 */
	public function deleteFromProcess($processId)
	{
		if ( !$this->deleteFromProcessStmt ) {
			$table = static::$table;
			$activityTable = Wf\ActivityDao::$table;
			$sql = "DELETE FROM actInst USING $table AS actInst";
			$sql .= " LEFT JOIN $activityTable AS act ON (actInst.activityId = act.id)";
			$sql .= " WHERE act.processId = :processId";

			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}

		$this->deleteFromProcessStmt->execute(array(
			':processId' => $processId
		));
		return $this;
	}

	/**
	 * Delete all transitions of a process
	 * @param integer $processId
	 */
	public function deleteFromInstance($instanceId, $withChilds = false, $withTrans = true)
	{
		$filter = "instanceId=:instanceId";
		$bind = array(
			':instanceId' => $instanceId
		);
		$this->_deleteFromFilter($filter, $bind, $withChilds, $withTrans);
		return $this;
	}

	/**
	 * Return a statement with parents activities and acitivitiesInstance
	 * of the activity $activityId for process instance $instanceId.
	 *
	 * Used to get join activity instance parents.
	 *
	 * Defined Alias :
	 *    act = wf_activity, The child activity to request
	 * 	  link = wf_transition, The transitions of the process
	 *    parentsAct = wf_activity, The parents activities
	 *    parentsActInst = wf_instance_activity , The parents instance of activities
	 *
	 * @param integer $instanceId
	 * @param integer $activityId
	 * @return \PDOStatement
	 */
	public function getParentsInstanceActivity($instanceId, $filter, $select, $bind = array())
	{
		//$filter = "(parentsActInst.status NOT LIKE :status OR parentsActInst.status IS NULL) AND act.id=:activityId";
		//$select = 'parentsAct.id, parentsActInst.status';
		$bind[':instanceId'] = $instanceId;

		$sql = "SELECT $select FROM wf_activity as act" . " JOIN wf_transition as link ON link.childId = act.id" . " JOIN wf_activity as parentsAct ON link.parentId = parentsAct.id" . " LEFT OUTER JOIN wf_instance_activity as parentsActInst ON parentsActInst.activityId = parentsAct.id AND parentsActInst.instanceId=:instanceId" . " WHERE $filter";

		/* @var \PDOStatement $stmt */
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		return $stmt;
	}
} /* End of class */

