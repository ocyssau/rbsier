<?php
// %LICENCE_HEADER%
namespace Rbs\Wf\Instance;

use Rbs\Dao\Sier\Link;
use Exception;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `wf_document_link` (
 `uid` VARCHAR(64) NOT NULL,
 `parentId` int(11) NOT NULL,
 `childId` int(11) NOT NULL,
 `parentUid` VARCHAR(255) DEFAULT NULL,
 `childUid` VARCHAR(255) DEFAULT NULL,
 `name` VARCHAR(255) DEFAULT NULL,
 `lindex` int(11) DEFAULT '0',
 `attributes` mediumtext,
 `spacename` VARCHAR(64) NOT NULL,
 PRIMARY KEY (`parentId`,`childId`),
 UNIQUE KEY `uid` (`uid`),
 KEY `name` (`name`),
 KEY `lindex` (`lindex`),
 KEY `parentUid` (`parentUid`),
 KEY `childUid` (`childUid`),
 KEY `parentId` (`parentId`),
 KEY `childId` (`childId`),
 CONSTRAINT `FK_wf_document_link_1` FOREIGN KEY (`childId`) REFERENCES `wf_instance` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 << */

/**
 SQL_INSERT>>
 <<
 */

/** SQL_ALTER>>
 <<*/

/**
 SQL_FKEY>>
 <<
 */

/**
 SQL_TRIGGER>>
 <<
 */

/**
 SQL_VIEW>>
 <<
 */

/** SQL_DROP>>
 DROP TABLE wf_document_link CASCADE;
 << */

/**
 *
 *
 */
class DocumentLinkDao extends Link
{

	/**
	 * @var string
	 */
	public static $table = 'wf_document_link';

	/**
	 * @var string
	 */
	public static $parentTable = '%s_documents';

	/**
	 * @var string
	 */
	public static $childTable = 'wf_instance';

	/**
	 * @var string
	 */
	public static $parentForeignKey = 'id';

	/**
	 * @var string
	 */
	public static $childForeignKey = 'id';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'uid' => 'uid',
		'parentId' => 'documentId',
		'childId' => 'instanceId',
		'parentUid' => 'documentUid',
		'childUid' => 'instanceUid',
		'spacename' => 'spacename',
		'attributes' => 'attributes'
	);

	/**
	 *
	 * @param \PDO $conn
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);

		$spacename = strtolower($this->spacename);

		$this->_childTable = self::$childTable;
		$this->_parentTable = sprintf(self::$parentTable, $spacename);
		$this->spacename = $spacename;
	}

	/**
	 * @todo Escape sql injection on select
	 * $mapped \Rbh\Link
	 */
	public function insertFromArray($input)
	{
		$table = static::$table;
		$sysToApp = $this->metaModel;

		($this->connexion->inTransaction() == true) ? $withTrans = false : $withTrans = true;

		/* Init statement */
		if ( !$this->insertStmt ) {
			$sysSet = [];
			$appSet = [];
			foreach( $sysToApp as $asSys => $asApp ) {
				$sysSet[] = '`' . $asSys . '`';
				$appSet[] = ':' . $asApp;
			}

			$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
			$this->insertStmt = $this->connexion->prepare($sql);
		}

		$bind = [];
		foreach( $sysToApp as $asSys => $asApp ) {
			if ( isset($input[$asApp]) ) {
				$bind[':' . $asApp] = $input[$asApp];
			}
			else {
				$bind[':' . $asApp] = null;
			}
		}
		$bind[':uid'] = uniqid();
		$bind[':spacename'] = $this->spacename;

		try {
			if ( $withTrans ) $this->connexion->beginTransaction();
			$this->insertStmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 *
	 * @param int $documentId
	 * @return \Rbs\Dao\Sier\DaoList
	 */
	public function getNextActivitiesFromDocumentId($documentId)
	{
		$table = $this->_table;
		$childTable = $this->_childTable;
		$spacename = $this->spacename;

		$sql = "SELECT inst.id as instanceId,act.*
				FROM $table AS doclink
				JOIN $childTable AS inst ON inst.id = doclink.childId
				JOIN wf_instance_activity AS actInst ON actInst.instanceId = inst.id
				JOIN wf_activity AS act ON act.id = actInst.activityId";
		$sql .= " WHERE doclink.parentId = :documentId AND actInst.status='running' AND inst.status='running'";
		$sql .= " AND doclink.spacename = :spacename";
		$sql .= " ORDER BY inst.id ASC";

		$list = new \Rbs\Dao\Sier\DaoList(null);
		$list->loadFromSql($sql, array(
			':documentId' => $documentId,
			':spacename' => $spacename
		));

		return $list;
	}

	/**
	 * Getter for runningActivities.
	 * Return also properties of activity object as :
	 * type, isComment, isInteractive, isAutomatic
	 *
	 * @param int $id
	 * @param string $status
	 * @return array
	 */
	public function getInstanceFromDocumentId($id, $status = '%', $select = null)
	{
		if ( !$select ) {
			$select = 'inst.*, lnk.name as lname, lnk.lindex, lnk.attributes as lattributes';
		}

		$table = $this->_table;
		$childTable = $this->_childTable;

		$sql = "SELECT $select
		FROM $table AS lnk
		JOIN $childTable AS inst ON inst.id=lnk.childId
		WHERE lnk.parentId=:documentId AND inst.status=:status ORDER BY inst.id ASC";

		$list = new \Rbs\Dao\Sier\DaoList(null);
		$list->loadFromSql($sql, array(
			':documentId' => $id,
			':status' => $status
		));
		return $list;
	}

	/**
	 * Delete all links of the documents and with status $status
	 *
	 * @param int $documentId
	 * @param string $status
	 * @return DocumentLinkDao
	 */
	public function deleteFromDocumentId($documentId, $status = '%')
	{
		$filter = "link.parentId=:parentId AND child.status LIKE :status";
		$bind = array(
			':parentId' => $documentId,
			':status' => $status
		);
		$this->deleteUsingChildren($filter, $bind);
		return $this;
	}
}
