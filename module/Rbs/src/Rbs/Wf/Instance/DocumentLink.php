<?php
// %LICENCE_HEADER%
namespace Rbs\Wf\Instance;

use Workflow\Model\Any;


class DocumentLink extends Any
{
	/**
	 * @var integer
	 */
	public static $classId = '56acc299ab315';

	/**
	 * @var \Rbplm\Ged\Document\Version
	 */
	protected $document;
	public $documentId;
	public $documentUid;

	/**
	 * @var \Workflow\Model\Wf\Instance
	 */
	protected $instance;
	public $instanceId;
	public $instanceUid;

	/**
	 *
	 * @var string
	 */
	public $spacename;

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return DocumentLink
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['documentId'])) ? $this->documentId = $properties['documentId'] : null;
		(isset($properties['documentUid'])) ? $this->documentUid = $properties['documentUid'] : null;
		(isset($properties['instanceId'])) ? $this->instanceId = $properties['instanceId'] : null;
		(isset($properties['instanceUid'])) ? $this->instanceUid = $properties['instanceUid'] : null;
		(isset($properties['spacename'])) ? $this->spacename = $properties['spacename'] : null;
		(isset($properties['attributes'])) ? $this->attributes = $properties['attributes'] : null;
		return $this;
	}

	/**
	 *
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function getDocument($asId=false)
	{
		if($asId){
			return $this->documentId;
		}
		return $this->document;
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return DocumentLink
	 */
	public function setDocument($document)
	{
		$this->document = $document;
		$this->documentId = $document->getId();
		$this->documentUid = $document->getUid();
		$this->spacename = $document->spacename;
		return $this;
	}

	/**
	 *
	 * @return \Workflow\Model\Wf\Instance
	 */
	public function getInstance($asId=false)
	{
		if($asId){
			return $this->instanceId;
		}
		return $this->instance;
	}

	/**
	 *
	 * @param \Workflow\Model\Wf\Instance $instance
	 * @return DocumentLink
	 */
	public function setInstance($instance)
	{
		$this->instance = $instance;
		$this->instanceId = $instance->getId();
		$this->instanceUid = $instance->getUid();
		return $this;
	}

} /* End of class */