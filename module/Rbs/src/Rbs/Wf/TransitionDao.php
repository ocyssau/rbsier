<?php
// %LICENCE_HEADER%
namespace Rbs\Wf;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `wf_transition` (
 `uid` VARCHAR(128) NOT NULL,
 `cid` VARCHAR(32) NOT NULL DEFAULT '56acc299ed150',
 `processId` INT(11) NOT NULL DEFAULT '0',
 `parentId` INT(11) NOT NULL DEFAULT '0',
 `parentUid` VARCHAR(32) NOT NULL,
 `childId` INT(11) NOT NULL DEFAULT '0',
 `childUid` VARCHAR(32) NOT NULL,
 `name` VARCHAR(128) DEFAULT NULL,
 `attributes` MEDIUMTEXT,
 `lindex` INT(7) DEFAULT '0',
 PRIMARY KEY (`parentId`,`childId`),
 UNIQUE KEY `UC_uid` (`uid`),
 KEY `K_parentUid` (`parentUid`),
 KEY `K_childUid` (`childUid`)
 ) ENGINE=InnoDB;
 << */

/**SQL_INSERT>>
 << */

/**SQL_ALTER>>
 ALTER TABLE `wf_transition` 
 ADD CONSTRAINT `FK_wf_transition_1`
 FOREIGN KEY (`parentId`)
 REFERENCES `wf_activity` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_wf_transition_2`
 FOREIGN KEY (`childId`)
 REFERENCES `wf_activity` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE;
 <<*/

/**SQL_FKEY>>
 << */

/**SQL_TRIGGER>>
 << */

/**SQL_VIEW>>
 << */

/**SQL_DROP>>
 << */

/**
 */
class TransitionDao extends \Rbs\Dao\Sier\Link
{

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'uid' => 'uid',
		'cid' => 'cid',
		'parentId' => 'parentId',
		'childId' => 'childId',
		'parentUid' => 'parentUid',
		'childUid' => 'childUid',
		'name' => 'name',
		'lindex' => 'index',
		'attributes' => 'attributes',
		'processId' => 'processId'
	);

	/** @var string */
	public static $table = 'wf_transition';

	/** @var string */
	public static $vtable = 'wf_transition';

	/** @var string */
	public static $childTable = 'wf_activity';

	/** @var string */
	public static $childForeignKey = 'id';

	/** @var string */
	public static $parentTable = 'wf_activity';

	/** @var string */
	public static $parentForeignKey = 'id';

	/** @var string */
	public static $sequenceName = null;

	/** @var \PDOStatement */
	protected $deleteFromProcessStmt;

	/** @var \PDOStatement */
	protected $loadChildrenStmt;

	/**
	 * Delete all transitions of a process
	 *
	 * @param integer $processId        	
	 */
	public function deleteFromProcess($processId)
	{
		if ( !$this->deleteFromProcessStmt ) {
			$table = static::$table;
			$activityTable = ActivityDao::$table;
			$sql = "DELETE FROM trans USING $table AS trans LEFT JOIN $activityTable AS act ON (trans.parentId = act.id) WHERE trans.processId = :processId";
			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}
		$this->deleteFromProcessStmt->execute(array(
			':processId' => $processId
		));
		return $this;
	}

	/**
	 * Getter for activities.
	 * Return a list.
	 *
	 * @param integer $processId
	 * @return array
	 */
	public function getFromProcess($processId)
	{
		$table = static::$table;
		$parentTable = static::$parentTable;
		$parentForeignKey = static::$parentForeignKey;

		$sql = "SELECT trans.* FROM $table AS trans";
		$sql .= " JOIN $parentTable AS parent ON parent.$parentForeignKey = trans.parentId";
		$sql .= " WHERE parent.pId=:processId";

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(
			':processId' => $processId
		));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
} /* End of class */
