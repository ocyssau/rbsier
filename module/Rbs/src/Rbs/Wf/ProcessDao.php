<?php
//%LICENCE_HEADER%

namespace Rbs\Wf;

use Rbs\Dao\Sier as DaoSier;
use Workflow\Model\Wf;
use Rbs\Space\Factory as DaoFactory;




/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `wf_process` (
	`id` INT(11) NOT NULL,
	`uid` VARCHAR(128)  NOT NULL,
	`cid` VARCHAR(64) NOT NULL DEFAULT '56acc299ecd6d',
	`name` VARCHAR(128) DEFAULT NULL,
	`title` VARCHAR(256) DEFAULT NULL,
	`ownerId` VARCHAR(255) DEFAULT NULL,
	`updated` DATETIME DEFAULT NULL,
	`updateById` VARCHAR(128) DEFAULT NULL,
	`parentId` INT(11) DEFAULT NULL,
	`parentUid` VARCHAR(128) DEFAULT NULL,
	`version` VARCHAR(32) DEFAULT NULL,
	`normalizedName` VARCHAR(256) DEFAULT NULL,
	`isValid` TINYINT(1) DEFAULT 0,
	`isActive` TINYINT(1) DEFAULT 0,
	PRIMARY KEY (`id`),
	UNIQUE (uid),
	UNIQUE (normalizedName),
	INDEX (`uid` ASC),
	INDEX (`cid` ASC),
	INDEX (`name` ASC),
	INDEX (`parentUid` ASC),
	INDEX (`parentId` ASC),
	INDEX (`ownerId` ASC),
	INDEX (`isValid` ASC),
	INDEX (`isActive` ASC)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `wf_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO `wf_seq` (`id`) VALUES (100);

<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class ProcessDao extends DaoSier
{

	/** @var \PDOStatement */
	protected $getInheritsFromParentIdStmt;
	
	/** @var \PDOStatement */
	protected $getTransitionsStmt;
	
	/**
	 *
	 * @var string
	 */
	public static $table='wf_process';

	/**
	 *
	 * @var string
	 */
	public static $vtable='wf_process';

	/**
	 * @var string
	 */
	public static $sequenceName='wf_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'cid'=>'cid',
		'name'=>'name',
		'title'=>'title',
		'ownerId'=>'ownerId',
		'updated'=>'updated',
		'updateById'=>'updateById',
		'parentId'=>'parentId',
		'parentUid'=>'parentUid',
		'version'=>'version',
		'normalizedName'=>'normalizedName',
		'isValid'=>'isValid',
		'isActive'=>'isActive',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'updated'=>'datetime',
		'isActive'=>'boolean',
		'isValid'=>'boolean',
	);

	/**
	 * @param int $documentId
	 * @return \Rbs\Dao\Sier\DaoList
	 */
	public function getDefaultProcessFromDocumentId($documentId)
	{
		$contTable = $this->factory->getTable(\Rbplm\Org\Workitem::$classId);
		$docTable = $this->factory->getTable(\Rbplm\Ged\Document\Version::$classId);

		$sql = "SELECT proc.* FROM $docTable AS doc
				JOIN $contTable AS cont ON doc.container_id = cont.id
				JOIN wf_process AS proc ON proc.id = cont.default_process_id";
		$sql .= " WHERE doc.id = :documentId";

		$list = new \Rbs\Dao\Sier\DaoList(null);
		$list->loadFromSql($sql, array(':documentId'=>$documentId));

		return $list;
	}
	
	/**
	 * Getter for process activities
	 *
	 * @param int $processId
	 * @param string $processId
	 * @return \Rbs\Dao\$type\DaoList
	 */
	public function getActivities($processId, $type)
	{
		$sql = "SELECT act.* FROM wf_activity AS act";
		$sql .= " JOIN wf_process AS proc ON proc.id = act.processId";
		$sql .= " WHERE proc.id = :processId AND act.type=:type";

		$list = new \Rbs\Dao\Sier\DaoList(null);
		$list->loadFromSql($sql, array(':processId'=>$processId, ':type'=>$type));

		return $list;
	}

	/**
	 * Getter for transitions
	 *
	 * @param Model\Any
	 * @return array
	 */
	public function getTransitions($processId)
	{
		if(!$this->getTransitionsStmt){
			$sql = 'SELECT * FROM wf_transition where processId=:processId';
			$this->getTransitionsStmt = $this->connexion->prepare($sql);
		}
		$this->getTransitionsStmt->execute(array(':processId'=>$processId));
		return $this->getTransitionsStmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Getter for versions
	 *
	 * @param Model\Any
	 * @return array
	 */
	public function getVersions($processName)
	{
		$table = self::$table;

		$sql  = "SELECT version FROM $table WHERE name=:processName ORDER BY version DESC";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(':processName'=>$processName));
		return $stmt->fetchAll(\PDO::FETCH_COLUMN);
	}

	/**
	 * @param integer $processId
	 * @param DaoFactory $factory
	 * @return array
	 */
	public function getGraphAsArray($processId, DaoFactory $factory)
	{
		/* load activitities */
		$activities = $factory->getList(Wf\Activity\Activity::$classId);
		$activities->load('processId=:processId', array(':processId'=>$processId));
		$activities->dao = $factory->getDao(Wf\Activity\Activity::$classId);
		$return = array();

		$sql = "SELECT trans.parentId, trans.childId, trans.name, trans.attributes
				FROM wf_transition AS trans
				JOIN wf_activity AS act ON act.id = trans.parentId";
		$sql .= " WHERE act.processId=:processId";

		$transitions = $factory->getList(Wf\Transition::$classId);
		$transitions->loadFromSql(trim($sql), array(':processId'=>$processId));

		foreach($transitions as $transition){
			$attributes = json_decode($transition['attributes'], true);

			$return['transitions'][] = array(
				'name'=>$transition['name'],
				'fromid'=>$transition['parentId'],
				'toid'=>$transition['childId'],
				'attributes'=>$attributes,
			);
		}

		foreach($activities as $activity){
			$attributes = $activity['attributes'];
			$roles = $activity['roles'];

			if(!is_array($attributes)){
				$attributes = json_decode($attributes, true);
			}
			if(!is_array($roles)){
				$roles = json_decode($roles, true);
			}

			$return['activities'][] = array(
				'id'=>$activity['id'],
				'name'=>$activity['name'],
				'title'=>$activity['title'],
				'type'=>$activity['type'],
				'isAutomatic'=>(int)$activity['isAutomatic'],
				'isComment'=>(int)$activity['isComment'],
				'isInteractive'=>(int)$activity['isInteractive'],
				'progression'=>$activity['progression'],
				'attributes'=>$attributes,
				'roles'=>$roles
			);
		}

		return $return;
	}


	/**
	 * Return the activities setted with parentId and parentUid from transitions
	 * @param integer
	 * @return \Rbplm\Dao\ListInterface
	 */
	public function getGraphWithTransitions($filter, $bind)
	{
		$factory = $this->factory;
		if(!$factory){
			throw new \Exception('the factory public property must be set before');
		}

		/* populate activities with parentId property */
		$sql = "SELECT
		trans.parentId,
		trans.parentUid,
		trans.name as status,
		act.id,
		act.uid,
		act.cid,
		act.name,
		act.normalizedName,
		act.title,
		act.ownerId,
		act.updateById,
		act.updated,
		act.isInteractive,
		act.isAutorouted,
		act.isAutomatic,
		act.isComment,
		act.type,
		act.processId,
		act.progression,
		act.expirationTime,
		act.roles,
		act.attributes
		FROM wf_activity AS act
		LEFT OUTER JOIN wf_transition as trans ON act.id=trans.childId
		WHERE $filter";
		$list = $factory->getList(Wf\Activity\Activity::$classId)->loadFromSql($sql, $bind);
		return $list;
	}
}
