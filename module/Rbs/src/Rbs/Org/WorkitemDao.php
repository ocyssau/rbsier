<?php
//%LICENCE_HEADER%
namespace Rbs\Org;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `workitems` (
	`id` INT(11) NOT NULL DEFAULT '0',
	`uid` VARCHAR( 128 ) NOT NULL DEFAULT '',
	`cid` VARCHAR(16) NOT NULL DEFAULT '569e94192201a',
	`alias_id` INT(11) NULL DEFAULT NULL,
	`object_class` VARCHAR(16) NOT NULL DEFAULT 'workitem',
	`dn` VARCHAR(128) NULL,
	`name` VARCHAR(128) NOT NULL DEFAULT '',
	`number` VARCHAR(128) NOT NULL DEFAULT '',
	`designation` TEXT DEFAULT NULL,
	`file_only` TINYINT(1) NOT NULL DEFAULT '0',
	`life_stage` VARCHAR(16) NOT NULL DEFAULT 'init',
	`acode` INT(11) DEFAULT NULL,
	`parent_id` INT(11) DEFAULT NULL,
	`parent_uid` VARCHAR(128) DEFAULT NULL,
	`default_process_id` INT(11) DEFAULT NULL,
	`created` DATETIME DEFAULT NULL,
	`create_by_id` INT(11) DEFAULT NULL,
	`create_by_uid` VARCHAR(64) DEFAULT NULL,
	`closed` DATETIME DEFAULT NULL,
	`close_by_id` INT(11) DEFAULT NULL,
	`close_by_uid` VARCHAR(64) DEFAULT NULL,
	`planned_closure` DATETIME DEFAULT NULL,
	`spacename` VARCHAR(16) NOT NULL DEFAULT 'workitem',
	`default_file_path` MEDIUMTEXT NOT NULL,
	`reposit_id` INT(11) DEFAULT NULL,
	`reposit_uid` VARCHAR(128) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY (`number`),
	UNIQUE KEY (`uid`),
	KEY (`create_by_uid`),
	KEY (`close_by_uid`),
	KEY (`parent_id`),
	KEY (`default_process_id`),
	KEY (`name`),
	KEY (`dn`),
	KEY (`reposit_uid`),
	KEY (`reposit_id`),
	FULLTEXT KEY `FT_1` (`number`,`name`,`designation`),
	CONSTRAINT `FK_workitems_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT `FK_workitems_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `FK_workitems_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>

-- ******************************************************************************
 DROP TRIGGER IF EXISTS onWorkitemInsert;
 DELIMITER $$
 CREATE TRIGGER onWorkitemInsert BEFORE INSERT ON workitems FOR EACH ROW 
 BEGIN
	 DECLARE parentDn VARCHAR(128);
	 DECLARE resourceDn VARCHAR(128);
	 DECLARE parentResourceDn VARCHAR(128);
	
	 SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	 SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	 SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	 set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	 -- add resource
	 INSERT INTO `acl_resource`(
	 	`id`,
		`uid`,
		`cn`,
		`referToUid`,
		`referToId`,
		`referToCid`)
		VALUES (
			aclSequence(),
			substr(uuid(),1,8),
			resourceDn,
			NEW.uid,
			NEW.id,
			NEW.cid
	 	);
 END $$
 DELIMITER ;
	
-- ******************************************************************************
 -- WORKITEM UPDATE
 DROP TRIGGER IF EXISTS onWorkitemUpdate;
 DELIMITER $$
 CREATE TRIGGER onWorkitemUpdate BEFORE UPDATE ON workitems FOR EACH ROW 
 BEGIN
 DECLARE parentDn VARCHAR(128);
 DECLARE resourceDn VARCHAR(128);
 DECLARE parentResourceDn VARCHAR(128);

 IF(NEW.parent_id != OLD.parent_id) THEN
 -- get parent dn
 SELECT dn INTO parentDn from projects where id=NEW.parent_id;
 SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');

 SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
 set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
 UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
 END IF;
 END;$$
 DELIMITER ;

-- ******************************************************************************
DROP PROCEDURE IF EXISTS `updateParentUid`;
DELIMITER $$
CREATE PROCEDURE `updateParentUid`()
BEGIN
	UPDATE workitems SET parent_uid = (SELECT uid FROM projects WHERE id=workitems.parent_id) WHERE workitems.parent_uid IS NULL;
	UPDATE cadlibs SET parent_uid = (SELECT uid FROM projects WHERE id=cadlibs.parent_id) WHERE cadlibs.parent_uid IS NULL;
	UPDATE bookshops SET parent_uid = (SELECT uid FROM projects WHERE id=bookshops.parent_id) WHERE bookshops.parent_uid IS NULL;
	UPDATE mockups SET parent_uid = (SELECT uid FROM projects WHERE id=mockups.parent_id) WHERE mockups.parent_uid IS NULL;
END $$
DELIMITER ;

-- ******************************************************************************
DROP PROCEDURE IF EXISTS `updateWorkitemDn`;
DELIMITER $$
CREATE PROCEDURE `updateWorkitemDn`()
BEGIN
	UPDATE workitems SET dn = CONCAT((SELECT dn FROM projects WHERE id=workitems.parent_id), uid,'/') WHERE workitems.dn IS NULL;
	UPDATE cadlibs SET dn = CONCAT((SELECT dn FROM projects WHERE id=cadlibs.parent_id), uid,'/') WHERE cadlibs.dn IS NULL;
	UPDATE bookshops SET dn = CONCAT((SELECT dn FROM projects WHERE id=bookshops.parent_id), uid,'/') WHERE bookshops.dn IS NULL;
	UPDATE mockups SET dn = CONCAT((SELECT dn FROM projects WHERE id=mockups.parent_id), uid,'/') WHERE mockups.dn IS NULL;
END $$
DELIMITER ;

 <<*/

/** SQL_VIEW>>

DROP VIEW IF EXISTS `view_containers`;
CREATE VIEW `view_containers` AS 
	SELECT 
    `workitems`.`id`,
    `workitems`.`number`,
    `workitems`.`name`,
    `workitems`.`uid`,
    `workitems`.`cid`,
    `workitems`.`dn`,
    `workitems`.`life_stage`,
    `workitems`.`designation`,
    `workitems`.`file_only`,
    `workitems`.`acode`,
    `workitems`.`version`,
    `workitems`.`parent_id`,
    `workitems`.`parent_uid`,
    `workitems`.`default_process_id`,
    `workitems`.`default_file_path`,
    `workitems`.`reposit_id`,
    `workitems`.`reposit_uid`,
    `workitems`.`created`,
    `workitems`.`create_by_id`,
    `workitems`.`create_by_uid`,
    `workitems`.`planned_closure`,
    `workitems`.`closed`,
    `workitems`.`close_by_id`,
    `workitems`.`close_by_uid`,
    `workitems`.`spacename`
FROM `workitems`
UNION 
SELECT
    `bookshops`.`id`,
    `bookshops`.`number`,
    `bookshops`.`name`,
    `bookshops`.`uid`,
    `bookshops`.`cid`,
    `bookshops`.`dn`,
    `bookshops`.`life_stage`,
    `bookshops`.`designation`,
    `bookshops`.`file_only`,
    `bookshops`.`acode`,
    `bookshops`.`version`,
    `bookshops`.`parent_id`,
    `bookshops`.`parent_uid`,
    `bookshops`.`default_process_id`,
    `bookshops`.`default_file_path`,
    `bookshops`.`reposit_id`,
    `bookshops`.`reposit_uid`,
    `bookshops`.`created`,
    `bookshops`.`create_by_id`,
    `bookshops`.`create_by_uid`,
    `bookshops`.`planned_closure`,
    `bookshops`.`closed`,
    `bookshops`.`close_by_id`,
    `bookshops`.`close_by_uid`,
    `bookshops`.`spacename`
FROM `bookshops`
UNION
SELECT
    `cadlibs`.`id`,
    `cadlibs`.`number`,
    `cadlibs`.`name`,
    `cadlibs`.`uid`,
    `cadlibs`.`cid`,
    `cadlibs`.`dn`,
    `cadlibs`.`life_stage`,
    `cadlibs`.`designation`,
    `cadlibs`.`file_only`,
    `cadlibs`.`acode`,
    `cadlibs`.`version`,
    `cadlibs`.`parent_id`,
    `cadlibs`.`parent_uid`,
    `cadlibs`.`default_process_id`,
    `cadlibs`.`default_file_path`,
    `cadlibs`.`reposit_id`,
    `cadlibs`.`reposit_uid`,
    `cadlibs`.`created`,
    `cadlibs`.`create_by_id`,
    `cadlibs`.`create_by_uid`,
    `cadlibs`.`planned_closure`,
    `cadlibs`.`closed`,
    `cadlibs`.`close_by_id`,
    `cadlibs`.`close_by_uid`,
    `cadlibs`.`spacename`
FROM `cadlibs`
UNION
SELECT 
    `mockups`.`id`,
    `mockups`.`number`,
    `mockups`.`name`,
    `mockups`.`uid`,
    `mockups`.`cid`,
    `mockups`.`dn`,
    `mockups`.`life_stage`,
    `mockups`.`designation`,
    `mockups`.`file_only`,
    `mockups`.`acode`,
    `mockups`.`version`,
    `mockups`.`parent_id`,
    `mockups`.`parent_uid`,
    `mockups`.`default_process_id`,
    `mockups`.`default_file_path`,
    `mockups`.`reposit_id`,
    `mockups`.`reposit_uid`,
    `mockups`.`created`,
    `mockups`.`create_by_id`,
    `mockups`.`create_by_uid`,
    `mockups`.`planned_closure`,
    `mockups`.`closed`,
    `mockups`.`close_by_id`,
    `mockups`.`close_by_uid`,
    `mockups`.`spacename`
FROM `mockups`;
<<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class WorkitemDao extends DaoSier
{

	/** @var string */
	public static $table = 'workitems';

	/** @var string */
	public static $vtable = 'workitems';

	/** @var string */
	public static $sequenceName = 'org_seq';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'dn' => 'dn',
		'name' => 'name',
		'number' => 'number',
		'designation' => 'description',
		'parent_id' => 'parentId',
		'parent_uid' => 'parentUid',
		'life_stage' => 'lifeStage',
		'acode' => 'accessCode',
		'default_process_id' => 'processId',
		'created' => 'created',
		'create_by_id' => 'createById',
		'create_by_uid' => 'createByUid',
		'closed' => 'closed',
		'close_by_id' => 'closeById',
		'close_by_uid' => 'closeByUid',
		'planned_closure' => 'forseenCloseDate',
		'spacename' => 'spacename',
		'default_file_path' => 'path',
		'reposit_id' => 'repositId',
		'reposit_uid' => 'repositUid'
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'planned_closure' => 'datetime',
		'created' => 'datetime',
		'closed' => 'datetime'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function save(MappedInterface $mapped, $options = array())
	{
		$parent = $mapped->getParent();
		if ( $parent ) {
			$mapped->parentId = $parent->getId();
		}

		$process = $mapped->getProcess();
		if ( $process ) {
			$mapped->processId = $process->getId();
		}

		$createBy = $mapped->getCreateBy();
		if ( $createBy ) {
			$mapped->createById = $createBy->getId();
			$mapped->createByUid = $createBy->getUid();
		}

		$closeBy = $mapped->getCloseBy();
		if ( $closeBy ) {
			$mapped->closeById = $closeBy->getId();
			$mapped->closeByUid = $closeBy->getUid();
		}

		$alias = $mapped->getAlias();
		if ( $alias ) {
			$mapped->aliasId = $alias->getId();
		}

		(isset($mapped->repositPath)) ? $mapped->path = $mapped->repositPath : null;

		parent::save($mapped, $options);
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbs\Dao.Sier::hydrate()
	 */
	public function hydrate($mapped, array $row, $fromApp = false)
	{
		parent::hydrate($mapped, $row, $fromApp);
		(isset($row['default_file_path'])) ? $mapped->path = $row['default_file_path'] : null;
		(isset($row['spacename'])) ? $mapped->spacename = $row['spacename'] : null;
		return $mapped;
	}
}
