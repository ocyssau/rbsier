<?php
//%LICENCE_HEADER%

namespace Rbs\Org;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
CREATE TABLE IF NOT EXISTS `cadlibs` LIKE `workitems`;
ALTER TABLE `cadlibs` 
  CHANGE COLUMN `spacename` `spacename` VARCHAR(16) NOT NULL DEFAULT 'cadlib',
  ADD CONSTRAINT `FK_cadlibs_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
  ADD CONSTRAINT `FK_cadlibs_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
  ADD CONSTRAINT `FK_cadlibs_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT;
 <<*/


/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
DROP TRIGGER IF EXISTS onCadlibInsert;
delimiter $$
CREATE TRIGGER onCadlibInsert BEFORE INSERT ON cadlibs FOR EACH ROW 
BEGIN
	DECLARE parentDn VARCHAR(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END;$$
delimiter ;

-- CADLIB UPDATE
DROP TRIGGER IF EXISTS onCadlibUpdate;
delimiter $$
CREATE TRIGGER onCadlibUpdate BEFORE UPDATE ON cadlibs FOR EACH ROW 
BEGIN
	DECLARE parentDn VARCHAR(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END;$$
delimiter ;
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Postgresql Dao class for \Rbplm\Org\Project.
 *
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 * Example and tests: Rbplm/Org/UnitTest.php
 *
 */
class CadlibDao extends WorkitemDao
{

	/** @var string */
	public static $table = 'cadlibs';
	
	/** @var string */
	public static $vtable = 'cadlibs';
	
	/** @var string */
	public static $sequenceName = 'org_seq';
}
