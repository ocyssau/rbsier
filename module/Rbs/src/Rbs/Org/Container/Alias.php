<?php
namespace Rbs\Org\Container;

use Rbplm\Any;
use Rbplm\Dao\MappedInterface;

class Alias extends Any implements MappedInterface
{
	use \Rbplm\Mapped;
	use \Rbplm\LifeControl;

	/**
	 *
	 * @var string
	 */
	public static $classId = '45c84a5zalias';

	/**
	 * 
	 * @var integer
	 */
	public $aliasOfId;

	/**
	 * 
	 * @var string
	 */
	public $description;

	/**
	 * 
	 * @var string
	 */
	protected $number;

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.MappedInterface::hydrate()
	 */
	public function hydrate(array $properties)
	{
		$this->lifeControlHydrate($properties);
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['aliasOfId'])) ? $this->aliasOfId = $properties['aliasOfId'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
	}

	/**
	 *
	 * @param string $number
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

} /* end of class */
