<?php
//%LICENCE_HEADER%

namespace Rbs\Org\Container;

use Rbs\History\HistoryDao as BaseDao;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 */
class HistoryDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table='project_history';
	public static $vtable='project_history';

	public static $sequenceName = 'project_history_seq';
	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'histo_order'=>'id',
		'action_name'=>'action_name',
		'action_by'=>'action_ownerId',
		'action_started'=>'action_created',
		'id'=>'data_id',
		'number'=>'data_name',
		'description'=>'data_description',
		'life_stage'=>'data_status',
		'default_process_id'=>'data_processId',
		'open_by'=>'data_createBy',
		'created'=>'data_created',
		'planned_closure'=>'data_forseenCloseDate',
		'close_by'=>'data_closeBy',
		'closed'=>'data_closed',
	);

	public static $sysToAppFilter = array(
		'action_started'=>'datetime',
		'created'=>'datetime',
		'closed'=>'datetime',
	);
}
