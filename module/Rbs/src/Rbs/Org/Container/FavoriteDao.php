<?php
//%LICENCE_HEADER%
namespace Rbs\Org\Container;

use Rbplm\Dao\NotExistingException;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `container_favorite` (
 `user_id` int(11) NOT NULL,
 `container_id` int(11) NOT NULL,
 `container_number` VARCHAR(512) NOT NULL,
 `container_description` MEDIUMTEXT NULL,
 `space_id` int(11) NOT NULL,
 `spacename` VARCHAR(16) NOT NULL,
 PRIMARY KEY  (`user_id`, `container_id`, `space_id`)
 ) ENGINE=MyIsam;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class FavoriteDao extends \Rbs\Dao\Sier
{

	/**
	 * @var string
	 */
	public static $table = 'container_favorite';

	/**
	 * @var string
	 */
	public static $vtable = 'container_favorite';

	/** @var \PDOStatement */
	protected $addStmt;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = [
		'user_id' => 'ownerId',
		'container_id' => 'containerId',
		'container_number' => 'containerNumber',
		'container_description' => 'containerDescription',
		'space_id' => 'spaceId',
		'spacename' => 'spacename'
	];

	/**
	 * Return list of containers
	 * @param integer $userId
	 * @param array $select
	 * @return array key=>container_id
	 */
	public function getFromUserId($userId, array $select=['*'])
	{
		try {
			$filter = 'user_id=:userId';
			$bind = [
				':userId' => $userId
			];
			$stmt = $this->query($select, $filter, $bind);
			return $stmt->fetchAll(\PDO::FETCH_ASSOC);
		}
		catch( NotExistingException $e ) {}
	}

	/**
	 * Add a index to user box
	 * @return boolean
	 */
	public function add($bind)
	{
		$withTrans = $this->options['withtrans'];
		if ( $withTrans ) $this->connexion->beginTransaction();

		if ( !$this->addStmt ) {
			$table = static::$table;
			$sysToApp = $this->metaModel;

			foreach( $sysToApp as $asSys => $asApp ) {
				$sysSet[] = $asSys;
				$appSet[] = ':' . $asApp;
			}
			$sql = 'INSERT INTO ' . $table . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
			$this->addStmt = $this->connexion->prepare($sql);
		}

		try {
			$this->addStmt->execute($bind);
			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function remove($userId, $contId, $spaceId)
	{
		$filter = 'user_id=:userId AND container_id=:containerId AND space_id=:spaceId';
		$bind = [
			':userId' => $userId,
			':containerId' => $contId,
			':spaceId' => $spaceId
		];
		return $this->_deleteFromFilter($filter, $bind, false, true);
	}

	/**
	 * @return boolean
	 */
	public function clean($userId)
	{
		$filter = 'user_id=:userId';
		$bind = [
			':userId' => $userId
		];

		return $this->_deleteFromFilter($filter, $bind, false, true);
	}
}
