<?php
//%LICENCE_HEADER%

namespace Rbs\Org\Container;

use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\AccessCode;

/**
 *
 *
 */
class Archiver
{

	/**
	 *
	 * @var \Rbs\Space\Factory
	 */
	protected $factory;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
	}

	/**
	 * Run archiving on document.
	 * All document of container are archived.
	 *
	 * @var \Rbplm\Org\Workitem $container
	 * @var \Rbplm\People\User $user
	 */
	public function archive($container, $user)
	{
		$factory = $this->factory;

		$documentDao = $factory->getDao(Document\Version::$classId);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$list = $factory->getList(Document\Version::$classId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		$select = array();
		foreach($documentDao->metaModel as $asSys=>$asApp){
			$select[] = $asSys.' as '. $asApp;
		}
		$filter->select($select);

		$filter->andfind($container->getId(), $documentDao->toSys('parentId'), Op::EQUAL);
		$filter->andfind(AccessCode::ARCHIVE, $documentDao->toSys('accessCode'), Op::INF);
		$list->load($filter);

		$documentArchiver = $this->_getDocumentArchiver();
		$reposit = $documentArchiver->getReposit($container);

		try {
			foreach( $list as $item ) {
				$document = new Document\Version();
				$document->hydrate($item);
				$document->setParent($container);
				$document->dao = $documentDao;
				$dfDao->loadDocfiles($document, $dfDao->toSys('accessCode').'<'.$documentArchiver::$accessCode);
				$documentArchiver->archive($document, $reposit, true);
			}
		}
		catch (\Exception $e) {
			return $this;
		}

		$container->status = 'ARCHIVED';
		$container->accessCode = AccessCode::ARCHIVE;
		$container->setCloseBy($user);
		$container->setClosed(new \Rbplm\Sys\Date());
		$container->path = $reposit->getPath();

		$factory->getDao($container::$classId)->save($container);

		return $this;
	}

	/**
	 *
	 */
	protected function _getDocumentArchiver()
	{
		if(isset($this->documentArchiver)){
			$this->documentArchiver = new \Ged\Archiver\Archiver($this->factory);
		}
		return $this->documentArchiver;
	}

}
