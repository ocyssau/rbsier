<?php
//%LICENCE_HEADER%

namespace Rbs\Org\Container;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class AliasDao extends \Rbs\Dao\Sier
{

	/** @var string */
	public static $sequenceName = 'org_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'alias_id'=>'id',
		'uid'=>'uid',
		'cid'=>'cid',
		'name'=>'name',
		'number'=>'number',
		'container_id'=>'aliasOfId',
		'designation'=>'description',
		'created' => 'created',
		'create_by_id' => 'createById',
		'create_by_uid'=>'createByUid'
	);
	
	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created' => 'datetime'
	);
}
