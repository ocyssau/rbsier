<?php
namespace Rbs\Org\Project;

use Rbs\EventDispatcher\ListenerProvider;
use Rbs\Org\Event as OrgEvent;

/**
 */
class Listener
{

	/**
	 * @return ListenerProvider
	 */
	public static function get()
	{
		$listenerProvider = new ListenerProvider();

		$listenerProvider->addListener(function (OrgEvent $e) {
			$history = History::init();
			$history->onEvent($e);
			return $e;
		});

		return $listenerProvider;
	}
}
