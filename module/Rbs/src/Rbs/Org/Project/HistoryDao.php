<?php
//%LICENCE_HEADER%

namespace Rbs\Org\Project;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `project_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` VARCHAR(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` VARCHAR(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_started` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `project_number` VARCHAR(16) COLLATE latin1_general_ci DEFAULT NULL,
  `project_description` VARCHAR(128) COLLATE latin1_general_ci DEFAULT NULL,
  `project_state` VARCHAR(16) COLLATE latin1_general_ci DEFAULT NULL,
  `project_indice_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `planned_closure` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `closed` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
);
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 */
class HistoryDao extends \Rbs\History\HistoryDao
{

	/**
	 *
	 * @var string
	 */
	public static $table='project_history';
	public static $vtable='project_history';

	public static $sequenceName = 'project_history_seq';
	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'histo_order'=>'action_id',
		'action_name'=>'action_name',
		'action_by'=>'action_ownerId',
		'action_started'=>'action_created',
		'comment'=>'comment',
		'data'=>'data',

		'project_id'=>'data_id',
		'project_number'=>'data_name',
		'project_description'=>'data_description',
		'project_state'=>'data_status',
		'project_indice_id'=>'data_indice',
		'default_process_id'=>'data_processId',
		'open_by'=>'data_createBy',
		'created'=>'data_created',
		'planned_closure'=>'data_forseenCloseDate',
		'close_by'=>'data_closeBy',
		'closed'=>'data_closed',
		'area_id'=>'data_areaId',
	);

	public static $sysToAppFilter = array(
		'action_started'=>'datetime',
		'created'=>'datetime',
		'closed'=>'datetime',
	);
}
