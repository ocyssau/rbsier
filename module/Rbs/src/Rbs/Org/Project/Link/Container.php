<?php
//%LICENCE_HEADER%

namespace Rbs\Org\Project\Link;

/**
 * @brief Link Org\Container->Extended\Property
 *
 */
class Container extends \Rbplm\Link
{
	public static $classId = '569elnkcontain';
	
	/**
	 * Spacename of the linked container
	 * 
	 * @var string
	 */
	public $spacename;
}
