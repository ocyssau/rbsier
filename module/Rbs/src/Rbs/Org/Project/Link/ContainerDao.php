<?php
//%LICENCE_HEADER%

namespace Rbs\Org\Project\Link;

/** SQL_SCRIPT>>

-- CONTAINER
CREATE TABLE IF NOT EXISTS `project_container_rel` (
	`parent_id` int(11) NOT NULL,
	`parent_uid` VARCHAR(64) NOT NULL,
	`parent_cid` VARCHAR(64) NOT NULL,
	`child_id` int(11) DEFAULT NULL,
	`child_uid` VARCHAR(64) DEFAULT NULL,
	`child_cid` VARCHAR(64) DEFAULT NULL,
	`spacename` VARCHAR(64) NOT NULL,
	`rdn` VARCHAR(128) DEFAULT NULL,
	KEY `K_project_container_rel_1` (`parent_id`),
	KEY `K_project_container_rel_2` (`parent_uid`),
	KEY `K_project_container_rel_3` (`parent_cid`),
	KEY `K_project_container_rel_4` (`child_id`),
	KEY `K_project_container_rel_5` (`child_uid`),
	KEY `K_project_container_rel_6` (`child_cid`),
	KEY `K_project_container_rel_7` (`rdn`),
	UNIQUE KEY `U_project_container_rel_1` (`parent_uid`,`child_uid`),
	CONSTRAINT `FK_project_container_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
	CONSTRAINT `FK_project_container_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;


 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>

-- TRIGGER --
DROP TRIGGER IF EXISTS onOrgContainerRelInsert;
delimiter $$
CREATE TRIGGER onOrgContainerRelInsert BEFORE INSERT ON project_container_rel FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	DECLARE parentUid VARCHAR(128);
	DECLARE childUid VARCHAR(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	CASE NEW.spacename
		WHEN 'bookshop' THEN
			SELECT uid INTO childUid FROM bookshops WHERE id=NEW.child_id;
		WHEN 'cadlib' THEN
			SELECT uid INTO childUid FROM cadlibs WHERE id=NEW.child_id;
		WHEN 'mockup' THEN
			SELECT uid INTO childUid FROM mockups WHERE id=NEW.child_id;
		WHEN 'workitem' THEN
			SELECT uid INTO childUid FROM workitems WHERE id=NEW.child_id;
	END CASE;
			
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END;$$
delimiter ;
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP TABLE IF EXISTS `project_container_rel`;
 <<*/


class ContainerDao extends \Rbs\Dao\Sier\Link
{
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'project_container_rel';
	public static $vtable = 'project_container_rel';

	public static $parentTable = 'projects';
	public static $parentForeignKey = 'id';

	public static $childTable = '';
	public static $childForeignKey = 'id';

	public static $sequenceName = '';
	public static $sequenceKey = '';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'parent_id'=>'parentId',
		'parent_uid'=>'parentUid',
		'parent_cid'=>'parentCid',
		'child_id'=>'childId',
		'child_uid'=>'childUid',
		'child_cid'=>'childCid',
		'spacename'=>'spacename',
		'rdn'=>'rdn',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
	);

	/**
	 * @param \Rbs\Space\Factory $factory
	 */
	public function setFactory($factory)
	{
		parent::setFactory($factory);
		$spacename = strtolower($factory->getName()).'s';
		if($spacename=='defaults'){
			$this->_childTable = 'projects';
		}
		else{
			$this->_childTable = strtolower($factory->getName()).'s';
		}
		return $this;
	}
} /* End of class */
