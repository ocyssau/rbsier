<?php
//%LICENCE_HEADER%
namespace Rbs\Org\Project\Link;

/** SQL_SCRIPT>>
 
CREATE TABLE IF NOT EXISTS `project_category_rel` (
`parent_id` int(11) NOT NULL,
`parent_uid` VARCHAR(64) NOT NULL,
`parent_cid` VARCHAR(64) NOT NULL,
`child_id` int(11) DEFAULT NULL,
`child_uid` VARCHAR(64) DEFAULT NULL,
`child_cid` VARCHAR(64) DEFAULT NULL,
`rdn` VARCHAR(128) DEFAULT NULL,
KEY `K_project_category_rel_1` (`parent_id`),
KEY `K_project_category_rel_2` (`parent_uid`),
KEY `K_project_category_rel_3` (`parent_cid`),
KEY `K_project_category_rel_4` (`child_id`),
KEY `K_project_category_rel_5` (`child_uid`),
KEY `K_project_category_rel_6` (`child_cid`),
KEY `K_project_category_rel_7` (`rdn`),
UNIQUE KEY `U_project_category_rel_1` (`parent_id`,`parent_cid`,`child_id`),
CONSTRAINT `FK_project_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
CONSTRAINT `FK_project_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `FK_project_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 
 DROP TRIGGER IF EXISTS onOrgCategoryRelInsert;
 delimiter $$
 CREATE TRIGGER onProjectCategoryRelInsert BEFORE INSERT ON project_category_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn VARCHAR(128);
 DECLARE parentUid VARCHAR(128);
 DECLARE childUid VARCHAR(128);
 SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SELECT uid INTO childUid FROM categories WHERE id=NEW.child_id;
 SET NEW.rdn = parentDn;
 SET NEW.parent_uid = parentUid;
 SET NEW.child_uid = childUid;
 END;$$
 delimiter ;
 
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * 
 * @author ocyssau
 *
 */
class CategoryDao extends \Rbs\Dao\Sier\Link
{

	/** @var \PDOStatement */
	protected $getInheritsFromParentIdStmt;

	/** @var string */
	public static $table = 'project_category_rel';

	/** @var string */
	public static $vtable = 'project_category_rel';

	/** @var string */
	public static $parentTable = 'projects';

	/** @var string */
	public static $parentForeignKey = 'id';

	/** @var string */
	public static $childTable = 'categories';

	/** @var string */
	public static $childForeignKey = 'id';

	/** @var string */
	public static $sequenceName = null;

	/** @var string */
	public static $sequenceKey = null;

	/** @var string */
	public static $inheritTable = 'project_category_rel';

	/** @var string */
	public $_inheritTable;

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'parent_id' => 'parentId',
		'parent_uid' => 'parentUid',
		'parent_cid' => 'parentCid',
		'child_id' => 'childId',
		'child_uid' => 'childUid',
		'child_cid' => 'childCid',
		'rdn' => 'rdn'
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array();

	/**
	 *
	 * @param \PDO $conn
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->_inheritTable = static::$inheritTable;
	}

	/**
	 * Get categories inherited from current container and his parent project
	 * In $select, you must use 'child' and 'lnk' alias to refer to children table and link table.
	 *
	 * @param 	integer 			$parentId
	 * @param 	array 				$select
	 * @param 	string 				$filter
	 * @param 	array 				$bind
	 * @return \PDOStatement
	 */
	public function getInheritsFromParentId($parentId, $select = null, $filter = '1=1', $bind = array())
	{
		/* build statement */
		if ( !$this->getInheritsFromParentIdStmt ) {
			$lnkTable = $this->_vtable;
			$childTable = $this->_childTable;
			$childKey = $this->_childForeignKey;
			$toSysChildId = array_search('childId', $this->metaModel);
			$parentTable = $this->_parentTable;
			$inheritTable = $this->_inheritTable;
			
			if ( !$select ) {
				$select = array(
					'DISTINCT child.id',
					'child.uid',
					'child.cid',
					'child.number',
					'child.designation AS description',
					'child.parent_id AS parentId',
					'child.parent_uid AS parentUid',
					'child.icon'
				);
			}
			$selectStr = implode(',', $select);
			
			/* */
			$sql = "SELECT $selectStr FROM(";
			$sql .= " (SELECT parent_id, child_id, rdn, rdn as contRdn, '' as projRdn FROM $lnkTable)";
			$sql .= " UNION";
			$sql .= " (SELECT parent_id, child_id, rdn, '' as contRdn, rdn as projRdn FROM $inheritTable)";
			$sql .= " ) as lnk";
			$sql .= " JOIN $childTable AS child ON child.$childKey=lnk.$toSysChildId";
			$sql .= " WHERE";
			$sql .= " (SELECT dn FROM $parentTable WHERE id=:parentId LIMIT 1) LIKE CONCAT(lnk.rdn,'%')";
			$sql .= " AND $filter";
			
			/* */
			$stmt = $this->getConnexion()->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->getInheritsFromParentIdStmt = $stmt;
		}
		else {
			$stmt = $this->getInheritsFromParentIdStmt;
		}
		
		/* */
		$bind[':parentId'] = $parentId;
		$stmt->execute($bind);
		return $stmt;
	}
}
