<?php
// %LICENCE_HEADER%
namespace Rbs\Org\Project\Link;

/**SQL_SCRIPT>>

-- ############################################################################"
-- PROPERTY
-- ############################################################################"
-- PROPERTY
CREATE TABLE IF NOT EXISTS `project_property_rel` (
	`parent_id` int(11) NOT NULL,
	`parent_uid` VARCHAR(64) NOT NULL,
	`parent_cid` VARCHAR(64) NOT NULL,
	`child_id` int(11) DEFAULT NULL,
	`child_uid` VARCHAR(64) DEFAULT NULL,
	`child_cid` VARCHAR(64) DEFAULT NULL,
	`rdn` VARCHAR(128) DEFAULT NULL,
	KEY `K_project_property_rel_1` (`parent_id`),
	KEY `K_project_property_rel_2` (`parent_uid`),
	KEY `K_project_property_rel_3` (`parent_cid`),
	KEY `K_project_property_rel_4` (`child_id`),
	KEY `K_project_property_rel_5` (`child_uid`),
	KEY `K_project_property_rel_6` (`child_cid`),
	KEY `K_project_property_rel_7` (`rdn`),
	UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
	CONSTRAINT `FK_project_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
	CONSTRAINT `FK_project_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;


<<*/

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>

-- TRIGGER --
DROP TRIGGER IF EXISTS onOrgPropertyRelInsert;
delimiter $$
CREATE TRIGGER onOrgPropertyRelInsert BEFORE INSERT ON project_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	DECLARE parentUid VARCHAR(128);
	DECLARE childUid VARCHAR(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM doctypes WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END;$$
delimiter ;


 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */
class PropertyDao extends \Rbs\Dao\Sier\Link
{

	/** @var string */
	public static $table = 'project_property_rel';

	/** @var string */
	public static $vtable = 'project_property_rel';

	/** @var string */
	public static $parentTable = '';

	/** @var string */
	public static $parentForeignKey = 'id';

	/** @var string */
	public static $childTable = '';

	/** @var string */
	public static $childForeignKey = 'id';

	/** @var string */
	public static $sequenceName = null;

	/** @var string */
	public static $sequenceKey = null;

	/**
	 * @var array
	 */
	static $sysToApp = array(
		'parent_id'=>'parentId',
		'parent_uid'=>'parentUid',
		'parent_cid'=>'parentCid',
		'child_id'=>'childId',
		'child_uid'=>'childUid',
		'child_cid'=>'childCid',
		'rdn'=>'rdn',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array();

	/**
	 * @param \Rbs\Space\Factory $factory
	 */
	public function setFactory($factory)
	{
		parent::setFactory($factory);
		$spacename = strtolower($factory->getName());
		if($spacename=='default'){
			$this->_parentTable = 'projects';
			$this->_childTable = 'project_metadata';
		}
		else{
			$this->_parentTable = $spacename.'s';
			$this->_childTable = $spacename.'_metadata';
		}
		return $this;
	}

}
