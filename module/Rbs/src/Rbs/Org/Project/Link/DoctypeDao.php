<?php
//%LICENCE_HEADER%

namespace Rbs\Org\Project\Link;

use Rbs\Ged\Doctype\NotFoundException;

/** SQL_SCRIPT>>

CREATE TABLE IF NOT EXISTS `project_doctype_rel` (
	`parent_id` int(11) NOT NULL,
	`parent_uid` VARCHAR(64) NOT NULL,
	`parent_cid` VARCHAR(64) NOT NULL,
	`child_id` int(11) DEFAULT NULL,
	`child_uid` VARCHAR(64) DEFAULT NULL,
	`child_cid` VARCHAR(64) DEFAULT NULL,
	`rdn` VARCHAR(128) DEFAULT NULL,
	KEY `K_project_doctype_rel_1` (`parent_id`),
	KEY `K_project_doctype_rel_2` (`parent_uid`),
	KEY `K_project_doctype_rel_3` (`parent_cid`),
	KEY `K_project_doctype_rel_4` (`child_id`),
	KEY `K_project_doctype_rel_5` (`child_uid`),
	KEY `K_project_doctype_rel_6` (`child_cid`),
	KEY `K_project_doctype_rel_7` (`rdn`),
	UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
	CONSTRAINT `U_project_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
	CONSTRAINT `U_project_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `U_project_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>

DROP TRIGGER IF EXISTS onProjectDoctypeRelInsert;
delimiter $$
CREATE TRIGGER onProjectDoctypeRelInsert BEFORE INSERT ON project_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn VARCHAR(128);
	DECLARE parentUid VARCHAR(128);
	DECLARE childUid VARCHAR(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM doctypes WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END;$$
delimiter ;

 <<*/

/** SQL_VIEW>>
<<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 *
 */
class DoctypeDao extends \Rbs\Dao\Sier\Link
{
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'project_doctype_rel';
	public static $vtable = 'project_doctype_rel';

	public static $parentTable = 'project';
	public static $parentForeignKey = 'id';

	public static $childTable = 'doctypes';
	public static $childForeignKey = 'id';

	public static $sequenceName = null;
	public static $sequenceKey = null;

	/** @var \PDOStatement */
	protected $loadFromDocumentStmt;

	/** @var \PDOStatement */
	protected $loadFromDocumentCountStmt;

	/** @var \PDOStatement */
	protected $loadFromDocumentAllStmt;

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'parent_id'=>'parentId',
		'parent_uid'=>'parentUid',
		'parent_cid'=>'parentCid',
		'child_id'=>'childId',
		'child_uid'=>'childUid',
		'child_cid'=>'childCid',
		'rdn'=>'rdn',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
	);

	/**
	 * Select the Doctype from the document parameters and match Regex with $documentNumber.
	 * Doctypes are get from Current Project And Parent Project linked doctypes.
	 *
	 * @param 	\Rbplm\Ged\Doctype	$mapped
	 * @param 	string 				$documentNumber
	 * @param 	string 				$fileExtension
	 * @param 	string 				$fileType
	 */
	public function loadFromDocument( $mapped, $dn, $documentNumber, $fileExtension=null, $fileType=null )
	{
		$bind = [];

		if(!$this->loadFromDocumentStmt){
			$lnkTable = $this->_vtable;
			$childTable = $this->_childTable;
			$childKey = $this->_childForeignKey;
			$toSysChildId = array_search('childId', $this->metaModel);

			/* Count request*/
			$countRequest = "SELECT COUNT(*) FROM $lnkTable AS lnk";
			$countRequest.= " JOIN $childTable AS child ON child.$childKey=lnk.$toSysChildId";
			$countRequest.= " WHERE :dn LIKE CONCAT(lnk.rdn,'%')";
			$this->loadFromDocumentCountStmt = $this->getConnexion()->prepare($countRequest);

			/* */
			$sql = "SELECT child.* FROM $lnkTable AS lnk";
			$sql .= " JOIN $childTable AS child ON child.$childKey=lnk.$toSysChildId";
			$sql .= " WHERE :dn LIKE CONCAT(lnk.rdn,'%')";
			$sql .= " AND child.file_extensions LIKE :fileExtension";
			$sql .= " AND child.file_type LIKE :fileType";

			/* */
			$stmt = $this->getConnexion()->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadFromDocumentStmt = $stmt;

			/* get from sys stmt */
			$sql = "SELECT * FROM $childTable";
			$sql .= " WHERE file_extensions LIKE :fileExtension";
			$sql .= " AND file_type LIKE :fileType";
			$stmt = $this->getConnexion()->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadFromDocumentAllStmt = $stmt;
		}

		/* add passThrough */
		($fileExtension) ? $fileExtension = '%'.$fileExtension.'%' : $fileExtension='%';
		($fileType) ? $fileType= '%'.$fileType.'%' : $fileType='%';

		/* count doctypes found in org tree */
		$this->loadFromDocumentCountStmt->execute(array(':dn'=>$dn));
		$count = $this->loadFromDocumentCountStmt->fetchColumn(0);

		/* If found, load from org tree */
		if($count){
			$stmt = $this->loadFromDocumentStmt;
			$bind = array(
				':dn'=>$dn,
				':fileExtension'=>$fileExtension,
				':fileType'=>$fileType,
			);
		}
		else{
			$stmt = $this->loadFromDocumentAllStmt;
			$bind = array(
				':fileExtension'=>$fileExtension,
				':fileType'=>$fileType,
			);
		}

		/* */
		$stmt->execute($bind);
		if( $stmt->rowCount() == 0){
			throw new NotFoundException('NONE_VALID_DOCTYPE');
		}

		/* For each doctype test if current Document corresponding to regex */
		$doctypeProps = array();
		$genericDoctype = array();
		while( $row = $stmt->fetch() ){
			if( !empty($row['recognition_regexp']) ){
				if(preg_match('/'.$row['recognition_regexp'].'/', $documentNumber ) ){
					/* assign doctype and exit loop */
					$doctypeProps = $row;
					break;
				}
			}
			else{
				/* if recognition_regexp field is empty, its a generic doctype */
				$genericDoctype = $row;
			}
		}

		/* If no match doctypes, then assign the generic doctype, else return false */
		if(!$doctypeProps && $genericDoctype){
			$doctypeProps = $genericDoctype;
		}

		/**/
		if( !$doctypeProps ){
			throw new NotFoundException('NONE_VALID_DOCTYPE');
		}

		$this->factory->getDao($mapped->cid)->loadFromArray($mapped, $doctypeProps);
	}
}
