<?php
// %LICENCE_HEADER%
namespace Rbs\Org;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `projects` (
	`id` int(11) NOT NULL,
	`uid` VARCHAR(140) NOT NULL,
	`cid` VARCHAR(64) NOT NULL DEFAULT '569e93c6ee156',
	`dn` VARCHAR(128) NULL,
	`number` VARCHAR(128) NOT NULL,
	`name` VARCHAR(128) NOT NULL DEFAULT '',
	`designation` TEXT DEFAULT NULL,
	`parent_id` int(11) NULL,
	`parent_uid` VARCHAR(64) NULL,
	`life_stage` VARCHAR(32) NOT NULL DEFAULT 'init',
	`acode` int(11) DEFAULT NULL,
	`default_process_id` int(11) DEFAULT NULL,
	`created` DATETIME DEFAULT NULL,
	`create_by_id` int(11) DEFAULT NULL,
	`create_by_uid` VARCHAR(64) DEFAULT NULL,
	`closed` DATETIME DEFAULT NULL,
	`close_by_id` int(11) DEFAULT NULL,
	`close_by_uid` VARCHAR(64) DEFAULT NULL,
	`planned_closure` DATETIME DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY (`uid`),
	UNIQUE KEY (`number`),
	UNIQUE KEY (`dn`),
	KEY (`name`),
	KEY (`parent_id`),
	KEY (`parent_uid`),
	KEY (`life_stage`),
	KEY (`acode`),
	KEY (`create_by_uid`),
	KEY (`close_by_uid`),
	FULLTEXT KEY (`number`,`name`,`designation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
<<*/

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
<<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>

DROP TRIGGER IF EXISTS `onProjectInsert`;
DELIMITER $$
CREATE TRIGGER `onProjectInsert` BEFORE INSERT ON `projects` FOR EACH ROW 
BEGIN
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentDn VARCHAR(128);

	IF(NEW.parent_id IS NULL) THEN
		SET NEW.dn = CONCAT('/',NEW.uid,'/');
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
	ELSE
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
	END IF;
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END;$$
DELIMITER ;

DROP TRIGGER IF EXISTS `onProjectUpdate`;
DELIMITER $$
CREATE TRIGGER `onProjectUpdate` BEFORE UPDATE ON `projects` FOR EACH ROW
BEGIN
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentDn VARCHAR(128);
	
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- update parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		-- update resource
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE referToId=NEW.id AND `referToCid`='569e93c6ee156';
	END IF;
END;$$
DELIMITER ;


DROP TRIGGER IF EXISTS `onProjectDelete`;
DELIMITER $$
CREATE TRIGGER `onProjectDelete` AFTER DELETE ON `projects` FOR EACH ROW
BEGIN
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
DELIMITER ;

<< */

/** SQL_VIEW>>
<< */

/** SQL_DROP>>
<< */

/**
 * @brief Postgresql Dao class for \Rbplm\Org\Project.
 *
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 * Example and tests: Rbplm/Org/UnitTest.php
 */
class ProjectDao extends DaoSier
{

	/** @var string */
	public static $table = 'projects';

	/** @var string */
	public static $vtable = 'projects';

	/** @var string */
	public static $sequenceName = 'org_seq';

	/**
	 * 
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'cid'=>'cid',
		'dn' => 'dn',
		'name'=>'name',
		'number'=>'number',
		'designation'=>'description',
		'parent_id' => 'parentId',
		'parent_uid' => 'parentUid',
		'life_stage' => 'lifeStage',
		'acode'=>'accessCode',
		'default_process_id' => 'processId',
		'created' => 'created',
		'create_by_id' => 'createById',
		'create_by_uid'=>'createByUid',
		'closed'=>'closed',
		'close_by_id' => 'closeById',
		'close_by_uid' => 'closeByUid',
		'planned_closure'=>'forseenCloseDate',
	);
	
	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created' => 'datetime',
		'planned_closure' => 'datetime',
		'closed' => 'datetime'
	);

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\Mysql::save()
	 */
	public function save(MappedInterface $mapped, $options = array())
	{
		$parent = $mapped->getParent();
		if ( $parent ) {
			$mapped->parentId = $parent->id;
		}

		$process = $mapped->getProcess();
		if ( $process ) {
			$mapped->processId = $process->id;
		}

		$createBy = $mapped->getCreateBy();
		if ($createBy){
			$mapped->createById = $createBy->getId();
			$mapped->createByUid = $createBy->getUid();
		}
		
		$closeBy = $mapped->getCloseBy();
		if ($closeBy){
			$mapped->closeById = $closeBy->getId();
			$mapped->closeByUid = $closeBy->getUid();
		}
		parent::save($mapped, $options);
	}
}
