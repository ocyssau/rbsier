<?php
namespace Rbs\Org;

use Rbplm\Org\Unit as OrgUnit;
use Rbs\Space\Factory as DaoFactory;
use Rbs\EventDispatcher\StoppableEvent;

/**
 * VERY generic to see if a task can be stopped
 */
class Event extends StoppableEvent
{
	
	const PRE_CREATE = 'create.pre';
	
	const POST_CREATE = 'create.post';
	
	const PRE_EDIT = 'edit.pre';
	
	const POST_EDIT = 'edit.post';
	
	const PRE_SAVE = 'save.pre';
	
	const POST_SAVE = 'save.post';
	
	const PRE_DELETE = 'delete.pre';
	
	const POST_DELETE = 'delete.post';

	/**
	 *
	 * @var DaoFactory
	 */
	protected $factory;

	/**
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * 
	 * @param string $name
	 * @param OrgUnit $org
	 * @param DaoFactory $factory
	 */
	public function __construct(string $name, OrgUnit $org, DaoFactory $factory)
	{
		$this->setName($name);
		$this->emitter = $org;
		$this->factory = $factory;
	}

	/**
	 *
	 * @return DaoFactory
	 */
	public function getFactory(): DaoFactory
	{
		return $this->factory;
	}
}
