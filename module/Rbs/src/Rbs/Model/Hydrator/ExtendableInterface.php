<?php
namespace Rbs\Model\Hydrator;

/**
 */
interface ExtendableInterface
{

	/**
	 * $extendProperties is array where value is name of additional property
	 *
	 * @param array $extendProperties
	 *        	In App sementic
	 * @return Extendable
	 */
	public function setExtended(array $extendProperties);

	/**
	 *
	 * @return array
	 */
	public function getExtendProperties();
}
