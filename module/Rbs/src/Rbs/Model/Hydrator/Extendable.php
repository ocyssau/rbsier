<?php
namespace Rbs\Model\Hydrator;

use Zend\Hydrator\HydratorInterface;

/**
 */
class Extendable implements HydratorInterface, ExtendableInterface
{

	/**
	 *
	 * @var array
	 */
	protected $extendProperties;

	/**
	 *
	 * @param array $extendProperties
	 */
	public function __construct($extendProperties = [])
	{
		$this->extendProperties = $extendProperties;
	}

	/**
	 * $extendProperties is array where value is name of additional property
	 *
	 * @param array $extendProperties
	 *        	In App sementic
	 * @return Extendable
	 */
	public function setExtended(array $extendProperties)
	{
		$this->extendProperties = $extendProperties;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getExtendProperties()
	{
		return $this->extendProperties;
	}

	/**
	 * Extract values from the provided object
	 *
	 * Extracts values via the object's getArrayCopy() method.
	 *
	 * @param object $object
	 * @return array
	 * @throws \BadMethodCallException for an $object not implementing getArrayCopy()
	 */
	public function extract($object)
	{
		if ( !is_callable([$object,'getArrayCopy']) ) {
			throw new \BadMethodCallException(sprintf('%s expects the provided object to implement getArrayCopy()', __METHOD__));
		}

		$data = $object->getArrayCopy();
		return $data;
	}

	/**
	 * Hydrate an object
	 *
	 * Hydrates an object by passing $data to either its exchangeArray() or
	 * populate() method.
	 *
	 * @param array $data
	 * @param object $object
	 * @return object
	 * @throws \BadMethodCallException for an $object not implementing hydrate()
	 */
	public function hydrate(array $data, $object)
	{
		if ( is_callable([$object,'hydrate']) ) {
			$object->hydrate($data);
		}
		else {
			throw new \BadMethodCallException(sprintf('%s expects the provided object to implement hydrate()', __METHOD__));
		}

		if($this->extendProperties){
			foreach($this->extendProperties as $property){
				$name = $property['appName'];
				(isset($data[$name])) ? $object->$name = $data[$name] : null;
			}
		}

		return $object;
	}
}
