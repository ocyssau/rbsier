<?php
//%LICENCE_HEADER%
namespace Rbs\Extended;

use Rbplm\Any;
use Rbplm\Dao\MappedInterface;

/**
 * @brief Entity of a property definition.
 *
 */
class Property extends Any implements MappedInterface
{
	use \Rbplm\Mapped;

	/**
	 * Must be define in children classes
	 * @var integer
	 */
	static $classId = '56b269f80prop';

	static $nameSuffix = 'ext56b26_';

	/**
	 * Commons
	 * @var
	 */
	protected $extendedCid = '';

	protected $appName = '';

	protected $sysType = '';

	protected $type = null;

	public $description = '';

	public $label = '';

	public $required = false;

	public $multiple = false;

	public $returnName = false;

	public $maybenull = false;

	public $hide = false;

	public $getter = '';

	public $setter = '';

	public $size = 1;

	public $regex = '';

	public $attributes = array();

	public $dbtable = '';

	public $forDisplay = '';

	public $forValue = '';

	public $dbfilter = '';

	public $dbquery = '';

	public $list = '';

	public $dateFormat;

	public $min = -1;

	public $max = -1;

	public $step = 1;

	/**
	 * @param Any $parent
	 */
	public function __construct($properties = [])
	{
		foreach( $properties as $pname => $pvalue ) {
			$this->$pname = $pvalue;
		}
		$this->cid = static::$classId;
	}

	/**
	 * @param string $name
	 * @return Any
	 */
	public static function init($name = "")
	{
		$class = get_called_class();
		$obj = new $class();
		if ( !$name ) {
			$name = uniqid(get_class($obj));
		}
		$obj->name = $name;
		return $obj;
	}

	/**
	 * Alias for hydrate; implement arrayObject interface
	 * @param array $properties
	 */
	public function __serialize()
	{
		$return = array();
		foreach( $this as $name => $value ) {
			$return[$name] = $value;
		}
		return $return;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Any
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['extendedCid'])) ? $this->extendedCid = $properties['extendedCid'] : null;
		(isset($properties['type'])) ? $this->type = $properties['type'] : null;
		(isset($properties['sysType'])) ? $this->sysType = $properties['sysType'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['appName'])) ? $this->appName = $properties['appName'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;

		/* To put in attributes ?: */
		(isset($properties['getter'])) ? $this->getter = $properties['getter'] : null;
		(isset($properties['setter'])) ? $this->setter = $properties['setter'] : null;
		(isset($properties['label'])) ? $this->label = $properties['label'] : null;
		(isset($properties['size'])) ? $this->size = $properties['size'] : null;
		(isset($properties['regex'])) ? $this->regex = $properties['regex'] : null;
		(isset($properties['required'])) ? $this->required = $properties['required'] : null;

		(isset($properties['multiple'])) ? $this->multiple = $properties['multiple'] : null;
		(isset($properties['returnName'])) ? $this->returnName = $properties['returnName'] : null;
		(isset($properties['hide'])) ? $this->hide = $properties['hide'] : null;
		(isset($properties['maybenull'])) ? $this->maybenull = $properties['maybenull'] : null;

		(isset($properties['attributes'])) ? $this->attributes = $properties['attributes'] : null;

		(isset($properties['dbtable'])) ? $this->dbtable = $properties['dbtable'] : null;
		(isset($properties['dbfilter'])) ? $this->dbfilter = $properties['dbfilter'] : null;
		(isset($properties['dbquery'])) ? $this->dbquery = $properties['dbquery'] : null;
		(isset($properties['forDisplay'])) ? $this->forDisplay = $properties['forDisplay'] : null;
		(isset($properties['forValue'])) ? $this->forValue = $properties['forValue'] : null;
		(isset($properties['list'])) ? $this->list = $properties['list'] : null;

		(isset($properties['dateFormat'])) ? $this->dateFormat = $properties['dateFormat'] : null;

		(isset($properties['min'])) ? $this->min = $properties['min'] : null;
		(isset($properties['max'])) ? $this->max = $properties['max'] : null;
		(isset($properties['step'])) ? $this->step = $properties['step'] : null;

		return $this;
	}

	/**
	 *
	 * @param string $type
	 * @return Property
	 */
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}

	/**
	 *
	 * @param string $type
	 */
	public function getSysType()
	{
		return $this->sysType;
	}

	/**
	 *
	 * @param string $type
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 *
	 * @param string $name
	 * @return Property
	 */
	public function setAppName($name)
	{
		$this->appName = $name;
		$this->name = self::$nameSuffix . $name;
		return $this;
	}

	/**
	 *
	 * @param string $name
	 */
	public function getAppName()
	{
		return $this->appName;
	}

	/**
	 *
	 * @param string $cid
	 * @return Property
	 */
	public function setExtended($cid)
	{
		$this->extendedCid = $cid;
		return $this;
	}

	/**
	 *
	 * @param string $cid
	 */
	public function getExtended()
	{
		return $this->extendedCid;
	}
}
