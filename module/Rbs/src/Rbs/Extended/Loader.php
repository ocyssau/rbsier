<?php
namespace Rbs\Extended;

/**
 * @brief Load the extended properties and configure the Dao.
 *
 * Complete Dao metamodel
 * Set the plugin to Dao
 *
 * @author olivier
 *
 */
class Loader
{

	/** @var string */
	public $table = 'workitem_metadata';

	/** @var \PDO */
	public $connexion;

	/** @var \PDOStatement */
	public $loadFromDocumentIdStmt;

	/** @var \PDOStatement */
	public $loadStmt;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
		$this->connexion = $factory->getConnexion();
		$spacename = strtolower($factory->getName());
		$this->table = $spacename . '_metadata';
		$this->spacename = $spacename;
	}

	/**
	 * @throws \Exception
	 */
	public function load($cid, $dao)
	{
		if ( !$this->loadStmt ) {
			$connexion = $this->connexion;
			
			$table = $this->table;
			
			$allSelect = \Rbs\Extended\PropertyDao::$sysToApp;
			foreach( $allSelect as $asSys => $asApp ) {
				$select[] = "`$asSys` AS `$asApp`";
			}
			$select = implode(',', $select);
			$sql = "SELECT $select FROM $table as prop WHERE extendedCid=:cid";
			$stmt = $connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadStmt = $stmt;
		}
		
		$this->loadStmt->execute(array(
			':cid' => $cid
		));
		$fetch = $this->loadStmt->fetchAll();
		
		if ( $fetch ) {
			$this->extendDao($fetch, $dao);
		}
		
		return $fetch;
	}

	/**
	 * @param integer $containerId
	 * @param array $select 	Array of asApp property name to put in select
	 * @return array 	The extended property entity in App semantic
	 */
	public function loadFromContainerId($containerId, $dao, $select = null)
	{
		$allSelect = \Rbs\Extended\PropertyDao::$sysToApp;
		if ( $select ) {
			$allSelect = array_intersect($allSelect, $select);
		}
		$select = array();
		foreach( $allSelect as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}
		
		$lnkDao = $this->factory->getDao(\Rbs\Org\Container\Link\Property::$classId);
		$extended = $lnkDao->getChildrenFromId($containerId, $select)->fetchAll();
		$this->extendDao($extended, $dao);
	}

	/**
	 * @var integer $documentId
	 * @var \Rbs\Dao\Sier $dao
	 * @var array $select
	 */
	public function loadFromDocumentId($documentId, $dao, $select = null)
	{
		$allSelect = \Rbs\Extended\PropertyDao::$sysToApp;
		if ( $select ) {
			$allSelect = array_intersect($allSelect, $select);
		}
		
		$select = array();
		foreach( $allSelect as $asSys => $asApp ) {
			$select[] = "`child`.`$asSys` AS `$asApp`";
		}
		$select = implode(',', $select);
		
		if ( !$this->loadFromDocumentIdStmt ) {
			$connexion = $dao->getConnexion();
			$spacename = $this->spacename;
			$table = $this->table;
			$relTable = $spacename . '_property_rel';
			$docTable = $spacename . '_documents';
			
			$sql = "
				SELECT $select
				FROM $docTable as parent
				JOIN $relTable as link ON link.parent_id=parent.container_id
				JOIN $table as child ON link.child_id=child.id
				WHERE parent.id=:documentId";
			$stmt = $connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadFromDocumentIdStmt = $stmt;
		}
		$this->loadFromDocumentIdStmt->execute(array(
			':documentId' => $documentId
		));
		$fetch = $this->loadFromDocumentIdStmt->fetchAll();
		
		if ( $fetch ) {
			$this->extendDao($fetch, $dao);
		}
		
		return $fetch;
	}

	/**
	 * @param array $extended 	In App sementic
	 * @param \Rbs\Dao\Sier $dao
	 */
	public function extendDao(array $extended, $dao)
	{
		$metaModel = array();
		$metaModelFilter = array();
		$extendedModel = array();
		
		foreach( $extended as $i ) {
			$sysName = $i['name'];
			$appName = $i['appName'];
			$extendedModel[$sysName] = $appName;
			$metaModel[$sysName] = $appName;
			if ( $i['type'] == 'date' ) {
				$metaModelFilter[$sysName] = 'datetime';
			}
		}
		$dao->metaModel = array_merge($dao::$sysToApp, $metaModel);
		$dao->metaModelFilters = array_merge($dao::$sysToAppFilter, $metaModelFilter);
		$dao->extendedModel = $extendedModel;
		$dao->extended = $extended;
	}
}
