<?php
//%LICENCE_HEADER%
namespace Rbs\Extended\Property;

/**
 * 
 *
 */
class Text extends \Rbs\Extended\Property
{

	/**
	 * @var integer
	 */
	static $classId = 201;

	/**
	 * @var string
	 */
	public $type = 'text';
}
