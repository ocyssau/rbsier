<?php
//%LICENCE_HEADER%
namespace Rbs\Extended\Property;

class Date extends \Rbs\Extended\Property
{

	/**
	 * @var integer
	 */
	static $classId = 206;

	/**
	 * @var string
	 */
	public $type = 'date';

	/**
	 * @var \DateTime
	 */
	public $min = null;

	/**
	 * @var \DateTime
	 */
	public $max = null;

	/**
	 * @var integer
	 */
	public $step = null;
}
