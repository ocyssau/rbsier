<?php
//%LICENCE_HEADER%

namespace Rbs\Extended\Property;

use Zend\Db\Sql\Ddl\Column\Decimal;

class Number extends \Rbs\Extended\Property
{
	/**
	 * @var integer
	 */
	static $classId = 202;
	
	/**
	 * @var string
	 */
	public $type = 'number';
	
	/**
	 * @var decimal
	 */
	public $min=null;
	
	/**
	 * @var decimal
	 */
	public $max=null;
	
	/**
	 * @var decimal
	 */
	public $step=null;
}
