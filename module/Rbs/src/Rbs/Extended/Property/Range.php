<?php
//%LICENCE_HEADER%
namespace Rbs\Extended\Property;

/**
 * 
 *
 */
class Range extends \Rbs\Extended\Property
{
	/**
	 * @var integer
	 */
	static $classId = 203;
	
	/**
	 * @var string
	 */
	public $type = 'range';
	
	/**
	 * @var double
	 */
	public $min = null;
	
	/**
	 * @var double
	 */
	public $max = null;
	
	/**
	 * @var double
	 */
	public $step = null;
}
