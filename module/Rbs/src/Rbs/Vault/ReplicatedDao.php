<?php
// %LICENCE_HEADER%
namespace Rbs\Vault;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `vault_replicated` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(32) NOT NULL,
	`cid` CHAR(13) NOT NULL DEFAULT 'repositsynchr',
	`name` VARCHAR(512) DEFAULT NULL,
	`description` text,
	`reposit_id` int(11) NULL,
	`reposit_uid` VARCHAR(32) NULL,
	`distantsite_id` int(11) NULL,
	`distantsite_uid` VARCHAR(32) NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `UC_vault_replicated_uid` (`uid`),
	KEY `K_vault_replicated_name_1` (`reposit_id`),
	KEY `K_vault_replicated_name_2` (`reposit_uid`),
	KEY `K_vault_replicated_name_3` (`distantsite_id`),
	KEY `K_vault_replicated_name_4` (`distantsite_uid`),
	INDEX `K_vault_replicated_1_idx` (`distantsite_id` ASC, `distantsite_uid` ASC),
	INDEX `K_vault_replicated_2_idx` (`reposit_id` ASC, `reposit_uid` ASC)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
ALTER TABLE `vault_replicated` 
ADD CONSTRAINT `fk_vault_replicated_1`
  FOREIGN KEY (`distantsite_id`)
  REFERENCES `vault_distant_site` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_vault_replicated_2`
  FOREIGN KEY (`reposit_id`)
  REFERENCES `vault_reposit` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE IF EXISTS `vault_replicated`;
 <<*/

/**
 * @package 
 */
class ReplicatedDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'vault_replicated';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'vault_replicated';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = null;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'name' => 'name',
		'description' => 'description',
		'reposit_id' => 'repositId',
		'reposit_uid' => 'repositUid',
		'distantsite_id' => 'distantSiteId',
		'distantsite_uid' => 'distantSiteUid',
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array();
	
	/**
	 *
	 * @param array $select
	 * @param string $filter
	 * @param array $bind
	 * @return \PDOStatement
	 */
	public function getWithRepositAndSite($select=null, $filter='1=1', $bind=[])
	{
		$table = $this->_table;
		$repositTable = 'vault_reposit';
		$siteTable = 'vault_distant_site';
		
		if(is_array($select)){
			$selectStr = implode(',', $select);
		}
		else{
			$selectStr = '*';
		}
		$sql = "SELECT $selectStr FROM $table AS replicated".
			" JOIN $repositTable AS reposit ON replicated.reposit_id=reposit.id".
			" JOIN $siteTable AS site ON replicated.distantsite_id=site.id".
			" WHERE $filter";
		
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}
}
