<?php
//%LICENCE_HEADER%

namespace Rbs\Vault;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;
use Rbplm\Sys\Exception;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Sier Dao class for \Rbplm\Vault\Record.
 *
 */
class RecordDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='';

	/**
	 *
	 * @var string
	 */
	public static $vtable='';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName='';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array();

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array();

	/**
	 */
	public function save(MappedInterface $mapped, $options = array())
	{
		throw new Exception('Save Is Not Permit');
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.Mysql::load()
	 */
	public function load( MappedInterface $mapped, $filter=null, $bind=null )
	{
		throw new Exception('Load Is Not Permit');
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbs\Dao.Sier::loadFromArray()
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		throw new Exception('Load Is Not Permit');
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbs\Dao\Sier::_deleteFromFilter()
	 */
	protected function _deleteFromFilter($filter, $bind, $withChildren = true, $withTrans = null)
	{
		throw new Exception('Delete Is Not Permit');
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.Mysql::updateFromArray()
	 */
	public function updateFromArray($id, $data)
	{
		throw new Exception('Save Is Not Permit');
	}

} /* End of class */

