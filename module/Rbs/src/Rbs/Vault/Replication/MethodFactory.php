<?php
// %LICENCE_HEADER%
namespace Rbs\Vault\Replication;

/**
 * 
 * @author ocyssau
 *
 */
class MethodFactory
{
	/**
	 * 
	 * @var MethodFactory
	 */
	static $singleton;
	
	/**
	 *
	 */
	private function __construct()
	{
	}
	
	/**
	 * Singleton method
	 */
	public static function get()
	{
		if(!self::$singleton){
			self::$singleton = new self();
		}
		return self::$singleton;
	}
	
	/**
	 * @param string $methodName
	 * @return \Rbs\Vault\Replication\Method\AbstractMethod
	 */
	public function fromMethod($methodName)
	{
		switch($methodName){
			case 'rsync':
				$method = new \Rbs\Vault\Replication\Method\Rsync();
				return $method;
				break;
		}
	}
}
