<?php
// %LICENCE_HEADER%
namespace Rbs\Vault\Replication\Method;

/**
 * 
 * @author ocyssau
 *
 */
abstract class AbstractMethod
{

	/**
	 * 
	 * @var array
	 */
	protected $params;

	/**
	 *
	 * @var array
	 */
	protected $credential;
	
	/**
	 *
	 * @var array
	 */
	protected $log;
	
	/**
	 *
	 */
	public function __construct()
	{
		$this->log = [
			'cmd'=>'',
			'return'=>''
		];
	}

	/**
	 * 
	 * @param array $params
	 * @return AbstractMethod
	 */
	public function setParams($params)
	{
		$this->params = array_merge($this->params, $params);
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getParams($params)
	{
		return $this->params;
	}

	/**
	 * 
	 * @param string $authUser
	 * @param string $authPassword
	 * @return AbstractMethod
	 */
	public function setCredential($authUser, $authPassword)
	{
		$this->credential = [
			$authUser,
			$authPassword
		];
		return $this;
	}
	
	/**
	 *
	 * @return array
	 */
	public function getLog()
	{
		return $this->log;
	}

	/**
	 *
	 */
	abstract public function run();
}
