<?php
// %LICENCE_HEADER%
namespace Rbs\Vault\Replication\Method;

/**
 * @author ocyssau
 * 
 * 
 * setParams(array) accept a array with keys :
 * 
 * 		hostname : the hostname of the target rsync server to replicated datas.
 * 		source : the hostname of the target rsync server to replicated datas.
 * 		target : the directory path on the target hostanme where put replicated data. The first root / must be omit.
 * 		params : rsync parameters. Default is   -rvzh --inplace --no-whole-file --exclude=__*
 * 
 */
class Rsync extends AbstractMethod
{

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->params = [
			'hostname'=>'',
			'source'=>'',
			'target'=>'',
			'params'=>'-rvzh --inplace --no-whole-file --exclude=__*',
		];
	}

	/**
	 *
	 */
	public function run()
	{
		$username = $this->credential[0];
		$password = $this->credential[1];
		$hostname = $this->params['hostname'];
		$targetDirectory = $this->params['target'];
		$source = $this->params['source'];
		$params = $this->params['params'];
		
		/* command for rsync */
		$cmd = 'export RSYNC_PASSWORD="%s" ; ' . sprintf('rsync %s %s %s@%s::%s', $params, $source, $username, $hostname, $targetDirectory);
		
		/* log without clear password */
		$this->log['cmd'] = sprintf($cmd, '**********');
		
		/* build full command line */
		$cmd = sprintf($cmd, $password);
		
		/* init out vars and run command */
		$returnVar = null;
		$outputs = [];
		exec($cmd, $outputs, $returnVar);
		
		/* log outputs */
		$this->log['return'] = $returnVar;
		$this->log['outputs'] = $outputs;
		
		return $returnVar;
	}
}
