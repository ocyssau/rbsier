<?php
// %LICENCE_HEADER%
namespace Rbs\Vault;

use Rbplm\Mapped;
use Rbplm\Any;
use Rbplm\Uuid;
use Rbplm\Dao\MappedInterface;

/**
 * @package 
 */
class DistantSite extends Any implements MappedInterface
{
	use Mapped;

	/**
	 *
	 * @var string
	 */
	public static $classId = 'distantsite36';

	/**
	 *
	 * @var string
	 */
	protected $name = '';
	
	/**
	 *
	 * @var string
	 */
	protected $method = '';
	
	/**
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 *
	 * @var string JSON
	 */
	protected $parameters;

	/**
	 *
	 * @var string
	 */
	protected $authUser = '';

	/**
	 *
	 * @var string
	 */
	protected $authPassword = '';

	/**
	 * @return DistantSite
	 */
	public static function init()
	{
		$class = get_called_class();
		/* @var \Rbs\Vault\DistantSite $obj */
		$obj = new $class();
		$obj->setUid(Uuid::newUid());
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return DistantSite
	 */
	public function hydrate(array $properties)
	{
		/* Any */
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		/* This */
		(isset($properties['method'])) ? $this->method = $properties['method'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['parameters'])) ? $this->parameters = $properties['parameters'] : null;
		(isset($properties['authUser'])) ? $this->authUser = $properties['authUser'] : null;
		(isset($properties['authPassword']) && $properties['authPassword']) ? $this->authPassword = $properties['authPassword'] : null;

		$this->mappedHydrate($properties);
		return $this;
	}

	/**
	 * 
	 * @return string JSON
	 */
	public function getParameters()
	{
		return $this->parameters;
	}
	
	/**
	 *
	 * @param string JSON
	 * @return DistantSite
	 */
	public function setParameters(array $params)
	{
		$this->parameters = $params;
		return $this;
	}
}
