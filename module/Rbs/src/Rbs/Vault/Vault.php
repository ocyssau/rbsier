<?php
//%LICENCE_HEADER%
namespace Rbs\Vault;

use Rbplm\Sys\Fsdata;
use Rbplm\Vault\Record;
use Exception;

/**
 * @brief Helper to record datas in vault.
 *
 * Example and tests: Rbplm/Vault/RecordTest.php
 *
 */
class Vault extends \Rbplm\Vault\Vault
{
	
	/**
	 * @param Record $record
	 * @throws Exception
	 * @return Record
	 */
	public function putInTrash(Record $record)
	{
		if ( $this->reposit == false ) {
			throw new Exception(sprintf('BAD_PARAMETER_OR_EMPTY %s', 'reposit'));
		}

		$reposit = $this->reposit;
		$path = $reposit->getTrashPath();
		$toPath = $path . '/' . $record->getName();

		$record->getFsdata()->rename($toPath, true);
		$fsdata = new Fsdata($toPath);

		$record->setFsdata($fsdata);
		$this->lastRecords[] = $record;
		return $record;
	}

	/**
	 *
	 * @param Record $record
	 * @param integer $iterationId
	 * @throws Exception
	 * @return Record
	 */
	public function unTrash(Record $record)
	{
		if ( $this->reposit == false ) {
			throw new Exception(sprintf('BAD_PARAMETER_OR_EMPTY %s', 'reposit'));
		}

		/* */
		$reposit = $this->reposit;
		$path = $reposit->getPath();
		$toPath = $path . '/' . $record->getName();

		$record->getFsdata()->rename($toPath, true);
		$fsdata = new Fsdata($toPath);

		$record->setFsdata($fsdata);
		$this->lastRecords[] = $record;
		return $record;
	}
}
