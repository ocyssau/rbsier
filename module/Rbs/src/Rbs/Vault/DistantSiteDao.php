<?php
// %LICENCE_HEADER%
namespace Rbs\Vault;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `vault_distant_site` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(32) NOT NULL,
 `cid` CHAR(13) NOT NULL DEFAULT 'distantsite36',
 `name` VARCHAR(512) DEFAULT NULL,
 `description` text,
 `method` VARCHAR(32),
 `parameters` text NULL,
 `auth_user` VARCHAR(128),
 `auth_password` VARCHAR(64),
 PRIMARY KEY (`id`),
 UNIQUE KEY `UC_vault_distant_site_uid` (`uid`)
 ) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE IF EXISTS `vault_distant_site`;
 <<*/

/**
 * @package 
 */
class DistantSiteDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'vault_distant_site';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'vault_distant_site';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = null;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'name' => 'name',
		'method' => 'method',
		'description' => 'description',
		'parameters' => 'parameters',
		'auth_user' => 'authUser',
		'auth_password' => 'authPassword'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'auth_password' => 'crypt'
	);
}

