<?php
// %LICENCE_HEADER%
namespace Rbs\Vault;

use Rbs\Dao\Sier as DaoSier;


/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `tools_missing_files` (
 `id` int(11) NOT NULL,
 `uid` VARCHAR(140) NOT NULL UNIQUE,
 `document_id` int(11) NOT NULL,
 `path` text NOT NULL,
 `name` VARCHAR(128) NOT NULL,
 `spacename` VARCHAR(64) NULL,
 UNIQUE KEY `U_tools_missing_files_1` (`name`,`path`(128)),
 KEY `IK_tools_missing_files_2` (`name`),
 KEY `IK_tools_missing_files_3` (`path`(128)),
 KEY `IK_tools_missing_files_4` (`spacename`)
 ) ENGINE=MyIsam;
 <<*/


/**
 *
 * @author olivier
 *
 */
class MissingFileDao extends DaoSier
{
	/**
	 *
	 * @var string
	 */
	public static $table = 'tools_missing_files';
	
	/**
	 * Get files without documents
	 */
	public function getMissingFiles($select, $filter)
	{
		$table = $this->_table;
		
		$strSelect = implode(',', $select);
		$sql = "SELECT $strSelect FROM $table";
		$sql .= ' WHERE' . $filter->__toString();
		$list = new \Rbs\Dao\Sier\DaoList($table);
		$list->loadFromSql($sql);
		return $list;
	}

	/**
	 * Update index of missing files. This index is saved in table tools_missing_files
	 */
	public function updateMissingFiles()
	{
		$connexion = $this->connexion;
		$table = $this->_table;
		
		$sql = "SELECT id,uid,document_id,name,path,'workitem' as spacename FROM workitem_doc_files";
		$sql .= " UNION ";
		$sql .= "SELECT id,uid,document_id,name,path,'bookshop' as spacename FROM bookshop_doc_files";
		$sql .= " UNION ";
		$sql .= "SELECT id,uid,document_id,name,path,'cadlib' as spacename FROM cadlib_doc_files";
		$sql = "SELECT df.*, t.id AS recorded FROM ($sql) AS df LEFT OUTER JOIN $table AS t ON t.uid=df.uid WHERE t.id IS NULL";
		$stmt = $connexion->prepare($sql);
		$stmt->execute();
		
		$sql = "INSERT INTO $table (id,uid,document_id,name,path,spacename) VALUES(:id,:uid,:documentId,:name,:path,:spacename)";
		$insertStmt = $connexion->prepare($sql);
		$connexion->setAttribute(\PDO::ATTR_AUTOCOMMIT, true);
		$connexion->query("TRUNCATE TABLE $table");
		
		/**/
		$c = 0;
		while( $entry = $stmt->fetch(\PDO::FETCH_ASSOC) ) {
			$c++;
			$file = $entry['path'] . '/' . $entry['name'];
			if ( !is_file($file) ) {
				$bind = array(
					'id' => $entry['id'],
					':uid' => $entry['uid'],
					':documentId' => $entry['document_id'],
					':name' => $entry['name'],
					':path' => $entry['path'],
					':spacename' => $entry['spacename']
				);
				$insertStmt->execute($bind);
			}
		}
		
		return $c;
	}
}
