<?php
// %LICENCE_HEADER%
namespace Rbs\Vault;

use Rbplm\Mapped;
use Rbplm\Any;
use Rbplm\Uuid;
use Rbplm\Rbplm;
use Rbplm\Dao\MappedInterface;

/**
 * @package 
 */
class Replicated extends Any implements MappedInterface
{
	use Mapped;

	/**
	 *
	 * @var string
	 */
	public static $classId = 'reporeplicate';

	/**
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 *
	 * @var \Rbplm\Vault\Reposit
	 */
	protected $reposit;

	/**
	 *
	 * @var integer
	 */
	public $repositId = null;

	/**
	 *
	 * @var string
	 */
	public $repositUid = "";
	
	/**
	 *
	 * @var \Rbs\Vault\DistantSite
	 */
	protected $distantSite;
	
	/**
	 *
	 * @var integer
	 */
	public $distantSiteId = null;
	
	/**
	 *
	 * @var string
	 */
	public $distantSiteUid = "";

	/**
	 * @return \Rbplm\Vault\Reposit
	 */
	public static function init()
	{
		$class = get_called_class();
		/* @var \Rbs\Vault\Replicated $obj */
		$obj = new $class();
		$obj->setUid(Uuid::newUid());
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Replicated
	 */
	public function hydrate(array $properties)
	{
		/* Any */
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		/* This */
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['repositId'])) ? $this->repositId = $properties['repositId'] : null;
		(isset($properties['repositUid'])) ? $this->repositUid = $properties['repositUid'] : null;
		(isset($properties['reposit'])) ? $this->setReposit($properties['reposit']) : null;
		(isset($properties['distantSiteId'])) ? $this->distantSiteId = $properties['distantSiteId'] : null;
		(isset($properties['distantSiteUid'])) ? $this->distantSiteUid = $properties['distantSiteUid'] : null;
		(isset($properties['distantSite'])) ? $this->setDistantSite($properties['distantSite']) : null;
		
		$this->mappedHydrate($properties);
		return $this;
	}

	/**
	 * 
	 * @param \Rbplm\Vault\Reposit $reposit
	 * @return \Rbs\Vault\Replicated
	 */
	public function setReposit(\Rbplm\Vault\Reposit $reposit)
	{
		$this->reposit = $reposit;
		$this->repositId = $reposit->getId();
		$this->repositUid = $reposit->getUid();
		return $this;
	}

	/**
	 * 
	 * @param boolean $asId
	 * @return number|\Rbplm\Vault\Reposit
	 */
	public function getReposit($asId = false)
	{
		if ( $asId ) {
			return $this->repositId;
		}
		else {
			return $this->reposit;
		}
	}
	
	/**
	 *
	 * @param \Rbs\Vault\DistantSite $distantSite
	 * @return \Rbs\Vault\Replicated
	 */
	public function setDistantSite(\Rbs\Vault\DistantSite $distantSite)
	{
		$this->distantSite = $distantSite;
		$this->distantSiteId = $distantSite->getId();
		$this->distantSiteUid = $distantSite->getUid();
		return $this;
	}
	
	/**
	 *
	 * @param boolean $asId
	 * @return number|\Rbs\Vault\DistantSite
	 */
	public function getDistantSite($asId = false)
	{
		if ( $asId ) {
			return $this->distantSiteId;
		}
		else {
			return $this->distantSite;
		}
	}
}
