<?php
//%LICENCE_HEADER%

namespace Rbs\Vault;

use Rbs\Dao\Sier as DaoSier;


/** SQL_SCRIPT>>

CREATE TABLE IF NOT EXISTS `vault_reposit` (
 `id` int(11) NOT NULL,
 `uid` VARCHAR(64) NOT NULL,
 `number` VARCHAR(255) NOT NULL,
 `name` VARCHAR(255) NOT NULL,
 `description` VARCHAR(256) DEFAULT NULL,
 `default_file_path` VARCHAR(512) NOT NULL,
 `spacename` VARCHAR(64) NOT NULL DEFAULT 'default',
 `type` int(2) NOT NULL,
 `mode` int(2) NOT NULL,
 `actif` int(2) NOT NULL DEFAULT 1,
 `priority` int(2) NOT NULL DEFAULT 1,
 `maxsize` int(11) NOT NULL DEFAULT 50000000,
 `maxcount` int(11) NOT NULL DEFAULT 50000,
 `created` DATETIME DEFAULT NULL,
 `create_by_id` int(11) DEFAULT NULL,
 `create_by_uid` VARCHAR(64) DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `U_vault_reposit_1` (`uid`),
 UNIQUE KEY `U_vault_reposit_2` (`number`),
 UNIQUE KEY `U_vault_reposit_3` (`default_file_path`),
 KEY `FK_vault_reposit_8` (`spacename`),
 KEY `FK_vault_reposit_1` (`name`),
 KEY `FK_vault_reposit_2` (`description`),
 KEY `FK_vault_reposit_4` (`type`),
 KEY `FK_vault_reposit_5` (`mode`),
 KEY `FK_vault_reposit_6` (`actif`),
 KEY `FK_vault_reposit_7` (`priority`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `vault_reposit_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `vault_reposit_seq` (id) VALUES(500);

<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 * @brief Dao class for \Rbplm\Vault\Reposit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 * @see \Rbplm\Vault\RepositTest
 *
 */
class RepositDao extends DaoSier
{
	/**
	 *
	 * @var string
	 */
	public static $table='vault_reposit';

	/**
	 *
	 * @var string
	 */
	public static $vtable='vault_reposit';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName='vault_reposit_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'description'=>'description',
		'name'=>'name',
		'number'=>'number',
		'default_file_path'=>'path',
		'spacename'=>'spacename',
		'type'=>'type',
		'mode'=>'mode',
		'actif'=>'active',
		'priority'=>'priority',
		'maxsize'=>'maxsize',
		'maxcount'=>'maxcount',
		'created'=>'created',
		'create_by_id'=>'createById',
		'create_by_uid'=>'createByUid',
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created'=>'datetime',
	);

	/**
	 *
	 * @param \Rbplm\Vault\Reposit $mapped
	 * @param integer Type of reposit, one of constants \Rbplm\Vault\Reposit::TYPE_* or let blank to get type from $mapped object.
	 * @return void
	 */
	public function loadActive($mapped, $type=null)
	{
		if(is_null($type)){
			$type = $mapped->getType();
		}
		$filter = "state=1 AND type= $type ORDER BY priority ASC";
		return $this->load( $mapped, $filter );
	}

	/**
	 *
	 * @param \Rbplm\Vault\Reposit $mapped
	 * @param string	$number
	 * @return void
	 */
	public function loadFromNumber($mapped, $number)
	{
		$filter = "number='$number'";
		return $this->load( $mapped, $filter );
	}

} /* End of class */
