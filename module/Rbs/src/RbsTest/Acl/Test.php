<?php
//%LICENCE_HEADER%

namespace RbsTest\Acl;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\NotExistingException;
use Acl\Model as Acl;
use Acl\Model\Resource;
use Rbplm\People;

/**
 * @brief Test class for \Rbplm\Org\Unit.
 *
 * @include Rbplm/Acl/Test.php
 */
class Test extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$factory = DaoFactory::get();

		echo '-- LOAD admin AS CURRENT USER' . CRLF;
		$user = new People\User();
		$factory->getDao($user->cid)->loadFromLogin($user, 'admin');
		People\CurrentUser::set($user);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	/**
	 *
	 */
	public function Test_Role()
	{
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Acl\Role::$classId);

		try{
			$role = new Acl\Role();
			$dao->loadFromUid($role, 'tester');
		}
		catch(NotExistingException $e){
			echo '-- ROLE TESTER IS NOT EXISTING'.CRLF;
			$role = Acl\Role::init('tester');
			$role->setName('Tester');
			$role->setDescription('A role for the tester');
			$dao = $factory->getDao($role->cid);
			$dao->save($role);
		}
		var_dump($role->getId());
	}

	/**
	 *
	 */
	public function Test_Right()
	{
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Acl\Right::$classId);

		try{
			$right = new Acl\Right(Acl\Right::READ, 'Read list of projects');
			$dao->save($right);
		}
		catch(\PDOException $e){
			echo '-- RIGHT IS EXISTING'.CRLF;
		}
	}

	/**
	 *
	 */
	public function Test_Rule()
	{
		$factory = DaoFactory::get();
		$ruleDao = $factory->getDao(Acl\Rule::$classId);
		$roleDao = $factory->getDao(Acl\Role::$classId);

		$role = new Acl\Role();
		$roleDao->loadFromUid($role, 'tester');

		try{
			throw new NotExistingException();
		}
		catch(NotExistingException $e){
			echo '-- SAVE RULE --'.CRLF;
			$rule = new Acl\Rule( Acl\Resource\Application::$appCn, $role->getId() );
			$rule->addRule(Acl\Right::CREATE, $rule::ALLOW);
			$rule->addRule(Acl\Right::DELETE, $rule::DENY);
			$ruleDao->save($rule);
		}

		echo '-- SAVE RULE --'.CRLF;
		$rule = new Acl\Rule( Acl\Resource\Ged::$appCn, $role->getId() );
		$rule->addRule(Acl\Right::CREATE, $rule::ALLOW);
		$rule->addRule(Acl\Right::DELETE, $rule::DENY);
		$ruleDao->save($rule);

		echo '-- SAVE RULE --'.CRLF;
		$rule = new Acl\Rule( Acl\Resource\Admin::$appCn, $role->getId() );
		$rule->addRule(Acl\Right::CREATE, $rule::ALLOW);
		$rule->addRule(Acl\Right::DELETE, $rule::DENY);
		$ruleDao->save($rule);

		echo '-- SAVE RULE --'.CRLF;
		$rule = new Acl\Rule( Acl\Resource\Workflow::$appCn, $role->getId() );
		$rule->addRule(Acl\Right::CREATE, $rule::ALLOW);
		$rule->addRule(Acl\Right::DELETE, $rule::DENY);
		$ruleDao->save($rule);

		echo '-- SAVE RULE --'.CRLF;
		$rule = new Acl\Rule( Acl\Resource\Acl::$appCn, $role->getId() );
		$rule->addRule(Acl\Right::CREATE, $rule::ALLOW);
		$rule->addRule(Acl\Right::DELETE, $rule::DENY);
		$ruleDao->save($rule);

		echo '-- SAVE RULE --'.CRLF;
		$rule = new Acl\Rule( Acl\Resource\Project::$appCn, $role->getId() );
		$rule->addRule(Acl\Right::CREATE, $rule::ALLOW);
		$rule->addRule(Acl\Right::DELETE, $rule::DENY);
		$ruleDao->save($rule);

		echo '-- SAVE RULE --'.CRLF;
		$rule = new Acl\Rule( Acl\Resource\Owner::$appCn, $role->getId() );
		$rule->addRule(Acl\Right::CREATE, $rule::ALLOW);
		$rule->addRule(Acl\Right::DELETE, $rule::DENY);
		$ruleDao->save($rule);

		echo '-- SAVE RULE --'.CRLF;
		$rule = new Acl\Rule( Acl\Resource\Wildspace::$appCn, $role->getId() );
		$rule->addRule(Acl\Right::CREATE, $rule::ALLOW);
		$rule->addRule(Acl\Right::DELETE, $rule::DENY);
		$ruleDao->save($rule);
	}

	/**
	 *
	 */
	public function Test_RuleTree()
	{
		$factory = DaoFactory::get();
		$ruleDao = $factory->getDao(Acl\Rule::$classId);
		/* @var \PDOStatement $stmt */
		$stmt = $ruleDao->getLtree()->getAncestors(Resource\Ged::$appCn, "roleId=1 AND rightId=1");
		var_dump($stmt->fetchAll());

		$stmt = $ruleDao->getLtree()->getChildren(Resource\Ged::$appCn);
		var_dump($stmt->fetchAll());

		$stmt = $ruleDao->getLtree()->getParents(Resource\Project::$appCn);
		var_dump($stmt->fetchAll());

		$stmt = $ruleDao->getLtree()->getDescendants(Resource\Ged::$appCn);
		var_dump($stmt->fetchAll());
	}

	/**
	 *
	 */
	public function Test_Resource()
	{
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Resource::$classId);
		$resource = new Resource\Acl();
		$dao->save($resource);
		$resource = new Resource\Admin();
		$dao->save($resource);
		$resource = new Resource\Application();
		$dao->save($resource);
		$resource = new Resource\Ged();
		$dao->save($resource);
		$resource = new Resource\Owner();
		$dao->save($resource);
		$resource = new Resource\Project();
		$dao->save($resource);
		$resource = new Resource\Wildspace();
		$dao->save($resource);
		$resource = new Resource\Workflow();
		$dao->save($resource);
	}

	/**
	 *
	 */
	public function Test_CheckRule()
	{
		$factory = DaoFactory::get();
		$ruleDao = $factory->getDao(Acl\Rule::$classId);

		$roleId = 14;
		$resourceCn = 'app/ged';
		$userId = 1;

		/* @var \PDOStatement $stmt */
		$stmt = $ruleDao->getFromRoleAndResource($roleId, $resourceCn);
		var_dump( $stmt->fetchAll() );

		$rightId = Acl\Right::CREATE;
		$stmt = $ruleDao->check($userId, $rightId, $resourceCn);
	}

} /* End of class */

