<?php
//%LICENCE_HEADER%

namespace RbsTest\People;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\People\User;
use Rbplm\People\Group;
use Rbplm\People\CurrentUser;

use Rbplm\Dao\ExistingException;
use Rbplm\Dao\NotExistingException;

/**
 * @brief Test class for \Rbplm\People\User
 *
 * @include Rbplm/People/UserTest.php
 *
 */
class UserTest extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	/**
	 *
	 */
	function Test_Dao()
	{
		$factory = DaoFactory::get();
		$userDao = $factory->getDao(User::$classId);

		/*Load admin user*/
		$user = new User();
		$userDao->loadFromName($user, 'admin');
		CurrentUser::set($user);

		assert($user->getId() == 1);
		assert($user->getName() == 'admin');
		assert($user->getLogin() == 'admin');

		/*new user*/
		$user = User::init(uniqid());
		$user->isActive(true);
		$userDao->save($user);

		var_dump($user->getUid());
		var_dump($user->getId());

		/*Load user*/
		$loadedUser = new User();
		$userDao->loadFromId( $loadedUser, $user->getId() );
		var_dump($loadedUser->getUid());
		var_dump($loadedUser->getId());

		assert($loadedUser->getUid() == $user->getUid());
	}
	
	/**
	 *
	 */
	function Test_GetUsers()
	{
		$factory = DaoFactory::get();
		$list = DaoFactory::get()->getList(User::$classId);
		$authUserId = 1;
		$list->loadFromSql('SELECT handle FROM '.$factory->getTable(User::$classId).' WHERE auth_user_id=:userid', array('userid'=>$authUserId));
		var_dump($list->current());
	}
	
	/**
	 *
	 */
	function Test_GetOne()
	{
	}
	
	/**
	 *
	 */
	function Test_AssignRole()
	{
		$factory = DaoFactory::get();
		$userDao = $factory->getDao(User::$classId);

		/*Load admin user*/
		$user = new User();
		$userDao->loadFromName($user, 'admin');

		$group = new Group();
		$factory->getDao($group->cid)->loadFromName($group, 'concepteurs');

		try{
			$userDao->assignRole($user->getId(), $group->getId());
		}
		catch(ExistingException $e){
		}
		
		try{
			$userDao->deleteRole($user->getId(), $group->getId());
		}
		catch(NotExistingException $e){
		}
	}


}
