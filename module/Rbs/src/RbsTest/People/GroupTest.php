<?php
//%LICENCE_HEADER%

namespace RbsTest\People;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\People\Group;
use Rbplm\Dao\NotExistingException;
use Rbplm\Dao\ExistingException;

/**
 * @brief Test class for \Rbplm\People\Group
 * 
 * @include Rbplm/People/GroupTest.php
 */
class GroupTest extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$factory = DaoFactory::get();
		
		try{
			$group = new Group();
			$factory->getDao($group->cid)->loadFromUid($group, 'GROUPTESTER');
		}
		catch(NotExistingException $e){
			$group = Group::init( 'GROUPTESTER' );
			$group->setDescription('description Of gtester');
			$group->setUid('GROUPTESTER');
			$factory->getDao($group->cid)->save($group);
		}
	}
	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	/**
	 */
	function Test_PseudoUuid()
	{
		$factory = DaoFactory::get();
		
		$myGroup = Group::init( 'GROUPTESTER' );
		$myGroup->setDescription('descriptionOfgtester');
		
		$dao = $factory->getDao($myGroup->cid);
		$dao->save( $myGroup );
		var_dump($myGroup->getUid());
		
		$loadedGroup = new Group();
		$dao->loadFromId( $loadedGroup, $myGroup->getId() );
		var_dump($loadedGroup->getUid());
		
		assert($loadedGroup->getUid() == $myGroup->getUid());
	}
	
	/**
	 */
	function Test_AssignPerm()
	{
		$factory = DaoFactory::get();
		
		$group = new Group();
		$dao = $factory->getDao($group->cid);
		$dao->loadFromUid($group, 'GROUPTESTER');
		
		$groupId=$group->getId();
		$areaId = 81;
		
		try{
			$dao->assignPerm($groupId, 'container_document_get', $areaId);
		}
		catch(ExistingException $e){
		}
		
		try{
			$dao->deletePerm($groupId, 'container_document_get', $areaId);
		}
		catch(NotExistingException $e){
		}
	}
	
	/**
	 */
	function Test_Dao()
	{
		$factory = DaoFactory::get();
		
		$name = uniqid('gtester');
		$myGroup = Group::init( $name );
		$myGroup->setDescription('descriptionOfgtester');
		
		assert( $myGroup->getName() == $name );
		assert( $myGroup->getDescription() == 'descriptionOfgtester' );
		
		$dao=$factory->getDao($myGroup->cid);
		$dao->save( $myGroup );
		
		$loadedGroup = new Group();
		$dao->loadFromId( $loadedGroup, $myGroup->getId() );
		
		assert( $loadedGroup->getName() ==  $myGroup->getName() );
		
		echo "Create some groups \n";
		$group0 = Group::init( uniqid('group0'));
		$group1 = Group::init( uniqid('group1'));
		$group2 = Group::init( uniqid('group2'));
		$group3 = Group::init( uniqid('group3'));
		
		/*
		 * Create a group tree.
		 * $myGroup
		 * 		|$group0
		 * 		|$group1
		 * 		|$group2
		 * 			|$group3
		 */
		$myGroup->addGroup($group0);
		$myGroup->addGroup($group1);
		$myGroup->addGroup($group2);
		$group2->addGroup($group3);
		
		$dao->save($group0);
		$dao->save($group1);
		$dao->save($group2);
		$dao->save($group3);
		
		echo 'Names of groups group0, group1, group2, group3' . CRLF;
		var_dump($group0->getName(), $group1->getName(), $group2->getName(), $group3->getName() );
		echo 'Uids of groups group0, group1, group2, group3' . CRLF;
		var_dump($group0->getUid(), $group1->getUid(), $group2->getUid(), $group3->getUid() );
		
		$dao->save($myGroup);
		
		$uid = $myGroup->getUid();
		
		unset($group0);
		unset($group1);
		unset($group2);
		unset($group3);
		
		$dao->loadFromUid($myGroup, $uid);
		//self::_displayGroupTree($myGroup);
		
		/*
		$GroupList = new \Rbplm\Dao\Pg\DaoList( array('table'=>'view_people_group_links'), \Rbplm\Dao\Connexion::get() );
		$GroupList->loadInCollection($myGroup->getGroups(), "lparent='$uid' ORDER BY lindex ASC");
		
		self::_displayGroupTree($myGroup);
		
		var_dump( $myGroup->getGroups()->getByIndex(0)->getName(), $group0Name, $myGroup->getUid() );
		assert( $myGroup->getGroups()->getByIndex(0)->getName() == $group0Name );
		
		//In other style with helpers methods of DAO
		$this->setUp();
		$dao->loadFromUid($myGroup, $uid);
		$dao->getGroups($myGroup, true);
		assert( $myGroup->getGroups()->getByIndex(0)->getName() == $group0Name );
		*/
		
		/*
		 * Load groups, and all sub-groups until infinite depth
		 */
		$list = $dao->loadGroupsRecursively($myGroup);
		foreach( $list as $row ){
			var_dump($row);
		}
		self::_displayGroupTree($myGroup);
	}
	
	/**
	 * 
	 */
	protected static function _displayGroupTree( $group )
	{
		/*Walk along the groups tree relation with a iterator*/
    	$it = new \RecursiveIteratorIterator( $group->getGroups(), \RecursiveIteratorIterator::SELF_FIRST );
    	$it->setMaxDepth(100);
    	echo $group->getName() . CRLF;
    	foreach($it as $node){
    		echo str_repeat('  ', $it->getDepth()+1) . $node->getName() . CRLF;
    	}
	}
}
