//%LICENCE_HEADER%

namespace RbsTest\Org;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\People\CurrentUser;

use Rbplm\Org\Workitem;
use Rbs\Org\Container\History as ContainerHistory;

use Rbplm\Org\Project;
use Rbs\Org\Project\History as ProjectHistory;

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 *
 */
class Test extends \Rbplm\Test\Test
{

	/**
	 * @var    \Rbplm\Org\Unit
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	/**
	 *
	 */
	public function Test_Alias()
	{
		$factory = DaoFactory::get('Workitem');
		$alias = new \Rbs\Org\Container\Alias();
		$aliasDao = $factory->getDao($alias->cid);

		$alias->setName('alias');
		$alias->setUid('alias');
		$alias->description = "Alias Test";
		$alias->aliasOfId = 2;

		$aliasDao->save($alias);
	}

	/**
	 *
	 */
	public function Test_ProjectHistory()
	{
		CurrentUser::get()->setId(1)->setUid(1);

		$factory = DaoFactory::get();
		$project = Project::init('TESTHISTORY');
		$project->setId(1);
		$history = ProjectHistory::init();
		$dao = $factory->getDao($history->cid);

		//getArrayCopy
		assert( $history->getAction()->getOwner() == CurrentUser::get());
		assert( $history->getAction()->getCreated() == new \DateTime());

		$history->getAction()->setName('Create');
		$history->setData($project->getArrayCopy());

		$data = $history->getArrayCopy();
		//var_dump($data);die;

		assert( $data['data']==$project->getArrayCopy() );
		assert( $data['action']==$history->getAction()->getArrayCopy() );

		//Hydrate
		$history2 = new History();
		$history2->hydrate($data);

		var_dump( $history->getAction()->getArrayCopy(),$history2->getAction()->getArrayCopy());
		var_dump( array_diff($history->getAction()->getArrayCopy(),$history2->getAction()->getArrayCopy()));
		//Failed tests because owner is set null in first case and 1 to second case :
		//assert($history->getAction()->getArrayCopy() == $history2->getAction()->getArrayCopy());
		//assert($history->getArrayCopy() == $history2->getArrayCopy());

		//var_dump($history->getAction());
		//var_dump($history->getArrayCopy());
		//var_dump($dao->bind($history));

		$dao->save($history);
	}

	/**
	 *
	 */
	public function Test_ContainerHistory()
	{
		$factory = DaoFactory::get('workitem');

		$workunit = Workitem::init('TESTHISTORY');
		$workunit->setId(1);

		$history = ContainerHistory::init();
		$dao = $factory->getDao($history->cid);

		$history->getAction()->setName('Create');
		$history->setData($workunit->getArrayCopy());
		$dao->save($history);
	}

	/**
	 *
	 */
	public function Test_ContainerLink()
	{
		$factory = DaoFactory::get('workitem');
	}


}
