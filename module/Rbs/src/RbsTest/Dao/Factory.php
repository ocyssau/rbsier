<?php
//%LICENCE_HEADER%

namespace RbsTest\Dao;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Test\Test;

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 *
 */
class Factory extends Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}

	/**
	 *
	 */
	function Test_GenerateDaoMap()
	{
		$factory = \Rbs\Space\Factory::get();
		$map = $factory->getMap();

		foreach($map as $cid=>$mapItem){
			$class = $mapItem[2];
			$cid = $class::$classId;
			echo "'".$cid."'=>array('" . $mapItem[0] . "', '" . $mapItem[1] . "', '" . $mapItem[2] . "')," . PHP_EOL;
		}
	}

	/**
	 *
	 */
	function Test_DaoFactory()
	{
		$factory = \Rbs\Space\Factory::get('mockup');
		$map = $factory->getMap();

		assert( $map['569e924be82e7'][1] == 'mockup_documents');

		foreach($map as $cid=>$mapItem){
			var_dump($cid, $mapItem);
			$modelClass = $mapItem[2];
			assert($cid == $modelClass::$classId);
			if($mapItem[0]){
				DaoFactory::get()->getDao($cid);
				DaoFactory::get()->getFilter($cid);
				DaoFactory::get()->getList($cid);
			}
		}
	}

	/**
	 *
	 */
	function Test_SpaceFactory()
	{
		$factory = \Rbs\Space\Factory::get('mockup');
		$map = $factory->getMap();

		assert( $map['569e924be82e7'][1] == 'mockup_documents');

		$document = new \Rbplm\Ged\Document\Version();

		//spaced factory
		$dao = $factory->getDao($document->cid);
		assert($dao::$table == 'mockup_documents');

		//without space
		$factory = \Rbs\Space\Factory::get();
		$dao = $factory->getDao($document->cid);

		//Cadlib
		$factory = \Rbs\Space\Factory::get('cadlib');
		$dao = $factory->getDao($document->cid);
		assert($dao::$table == 'cadlib_documents');

		//Bookshop
		$factory = \Rbs\Space\Factory::get('bookshop');
		$dao = $factory->getDao($document->cid);
		assert($dao::$table == 'bookshop_documents');
	}


	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

}
