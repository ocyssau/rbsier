<?php
namespace RbsTest\Docflow;

use Workflow\Model\Wf;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\People;


/**
 *
 *
 */
class Test extends \Rbplm\Test\Test
{
	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$factory = DaoFactory::get('Workitem');
		$this->factory = $factory;

		echo '-- LOAD admin AS CURRENT USER';
		$user = new People\User();
		$factory->getDao($user->cid)->loadFromLogin($user, 'admin');
		People\CurrentUser::set($user);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	/**
	 *
	 */
	public function Test_Process()
	{
		$factory = $this->factory;

		echo '-- INIT MODEL'.CRLF;
		$process = Wf\Process::init(uniqid('FORTESTPROC001'));
		$process->getNormalizedName(); // to init normalized name
		$process->setTitle('FOR TEST 01');
		echo '-- SAVE PROCESS'.CRLF;
		$factory->getDao($process->cid)->save($process);

		echo '-- INIT ACTIVITIES'.CRLF;
		$start = Wf\Activity\Start::init('START', $process);
		$end = Wf\Activity\End::init('END', $process);
		$act01 = Wf\Activity\Activity::init('ACT01', $process);
		echo '-- SAVE ACTIVITIES'.CRLF;
		$dao = $factory->getDao($start->cid);
		$dao->save($start);
		$dao->save($end);
		$dao->save($act01);

		/* transitions */
		$transition = Wf\Transition::factory($start, $act01)->setStatus('started');
		$transition->processId = $process->getId();
		$transition2 = Wf\Transition::factory($act01, $end)->setStatus('act01');
		$transition2->processId = $process->getId();
		echo '-- SAVE TRANSITIONS'.CRLF;
		$dao = $this->factory->getDao(Wf\Transition::$classId);
		$dao->insert($transition);
		$dao->insert($transition2);

	}

}
