// %LICENCE_HEADER%
namespace RbsTest\Ged;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People;
use Rbplm\Wf\Process;
use Rbplm\Ged\Category;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\AccessCode;
use Rbs\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;
use Rbs\Acl\Area;
use Rbs\Acl\Right;
use Rbplm\Sys;
use Rbplm\Sys\Exception;
use Rbplm\Dao\NotExistingException;
use Rbplm\Ged\AccessCodeException;
use Rbplm\Sys\Date as DateTime;
use Rbs\Ged\Historize;

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 */
class CheckOutIn extends \Rbplm\Test\Test
{

	/**
	 *
	 * @var \Rbplm\Org\Unit
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->tid = uniqid();
		Sys\Filesystem::isSecure(false);

		$this->dataPath = realpath('data/cache');
		$dataPath = $this->dataPath;
		var_dump($this->dataPath);

		$factory = DaoFactory::get('Workitem');
		$this->factory = $factory;

		echo '-- LOAD admin AS CURRENT USER';
		$user = new People\User();
		$factory->getDao($user->cid)->loadFromLogin($user, 'admin');
		People\CurrentUser::set($user);

		echo '--PREPARE FILES ON DISK--' . CRLF;
		$this->file1 = $this->dataPath . '/tmpTestfile1.txt';
		$this->file2 = $this->dataPath . '/tmpTestfile2.txt';
		touch($this->file1);
		touch($this->file2);
		\file_put_contents($this->file1, 'version001' . "\n");
		\file_put_contents($this->file2, 'version001' . "\n");

		echo '--LOAD PROJECT--' . CRLF;
		$project = Project::init('TESTPROJECT');
		try {
			$factory->getDao($project->cid)->loadFromName($project, 'TESTPROJECT');
		}
		catch( NotExistingException $e ) {
			$project->description = 'Projet de test';
			$factory->getDao($project->cid)->save($project);
		}
		$this->project = $project;

		echo '--LOAD WORKITEM--' . CRLF;
		$workitem = Workitem::init('WIFORTEST1');
		try {
			$factory->getDao($workitem->cid)->loadFromName($workitem, 'WIFORTEST1');
		}
		catch( NotExistingException $e ) {
			$reposit = Reposit::init($dataPath . '/repositTest/' . $workitem->getName());
			$workitem->path = $reposit->getPath();
			$workitem->description = 'Wi de test';
			$workitem->setName('WIFORTEST1')
				->setUid('WIFORTEST1')
				->setNumber('WIFORTEST1');

			$workitem->forseenCloseDate = new DateTime();
			$workitem->forseenCloseDate->add(new \DateInterval('P5Y')); // 5 years

			$workitem->setParent($project);
			$factory->getDao($workitem->cid)->save($workitem);
		}
		$this->workitem = $workitem;
	}

	/**
	 */
	public function Test_Store()
	{
		$factory = $this->factory;
		$dataPath = $this->dataPath;
		$file1 = $this->file1;
		$file2 = $this->file2;

		$user = new People\User();
		$factory->getDao($user->cid)->loadFromLogin($user, 'admin');
		People\CurrentUser::set($user);

		$factory->getConnexion()->beginTransaction();
		$reposit = Reposit::init($this->workitem->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		try {
			echo '--LOAD DOCUMENT v1--' . CRLF;
			$document = new Document\Version();
			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);
			$dao->loadFromName($document, 'DOCUMENTTEST_1');

			echo "-- DELETE DOCUMENT AND FILES" . CRLF;
			$service = new \Rbs\Ged\Document\Service($factory);
			$service->delete($document);
			throw new NotExistingException("none");
		}
		catch( NotExistingException $e ) {
			echo '--CREATE DOCUMENT v1--' . CRLF;
			$document = Document\Version::init('DOCUMENTTEST_1');
			$document->setUid('DOCUMENTTEST_1');
			$document->description = "Document version de test";
			$document->version = 1;
			$document->iteration = 1;
			$document->setOfDocument(Document::init('BaseDocument')->setNumber($this->tid));
			$document->setParent($this->workitem);

			$doctype = new Doctype();
			$factory->getDao($doctype->cid)->loadFromName($doctype, 'text__plain');
			$document->setDoctype($doctype);

			$category = Category::init(uniqid('CATEGORY1'));
			$category->description = 'Category de test';
			$document->setCategory($category);

			$factory->getDao($document->cid)->save($document);
		}

		echo '--CREATE DOCFILES--' . CRLF;
		$fsdata1 = new Fsdata($file1);
		$fsdata2 = new Fsdata($file2);

		try {
			$docfile1 = new Docfile\Version();
			$factory->getDao($docfile1->cid)->loadFromName($docfile1, 'DOCFILETEST_1.txt');
		}
		catch( NotExistingException $e ) {
			$docfile1 = Docfile\Version::init();
			$docfile1->setOfDocfile(Docfile::init(uniqid('baseDocfile1')));
			$docfile1->setParent($document);
		}

		try {
			$docfile2 = new Docfile\Version();
			$factory->getDao($docfile2->cid)->loadFromName($docfile2, 'DOCFILETEST_2.txt');
		}
		catch( NotExistingException $e ) {
			$docfile2 = Docfile\Version::init();
			$docfile2->setOfDocfile(Docfile::init(uniqid('baseDocfile2')));
			$docfile2->setParent($document);
		}

		echo '--SAVE RECORDS IN VAULT--' . CRLF;
		try {
			$record1 = $vault->record(Record::init(), $fsdata1, 'DOCFILETEST_1.txt', $md5 = null, $replace = true);
			$record2 = $vault->record(Record::init(), $fsdata2, 'DOCFILETEST_2.txt', $md5 = null, $replace = true);
		}
		catch( \Exception $e ) {
			echo '--FILES ARE EXISTING IN VAULT--' . CRLF;
		}

		$docfile1->setData($record1);
		$docfile2->setData($record2);

		echo '--SAVE DOCFILE v1--' . CRLF;
		$factory->getDao($docfile1->cid)->save($docfile1);
		$factory->getDao($docfile2->cid)->save($docfile2);
		$factory->getConnexion()->commit();
	}

	/**
	 */
	public function Test_Checkout()
	{
		$factory = DaoFactory::get('Workitem');

		echo '--LOAD DOCUMENT v1--' . CRLF;
		$document1 = new Document\Version();
		$factory->getDao($document1->cid)->loadFromName($document1, 'DOCUMENTTEST_1');

		$document1->lock(AccessCode::FREE, null);
		try {
			$this->_checkOutDocument($document1);
		}
		catch( AccessCodeException $e ) {
			echo "DOCUMENT IS CHECKOUT" . CRLF;
		}
	}

	/**
	 */
	public function Test_Checkin()
	{
		$factory = DaoFactory::get('Workitem');

		echo '--LOAD DOCUMENT v1--' . CRLF;
		$document1 = new Document\Version();
		$factory->getDao($document1->cid)->loadFromName($document1, 'DOCUMENTTEST_1');
		$this->_checkInDocument($document1);
	}

	/**
	 *
	 * @param unknown_type $document
	 * @throws Exception
	 */
	private function _checkOutDocument($document)
	{
		$factory = DaoFactory::get('Workitem');
		$dfDao = $factory->getDao(Docfile\Version::$classId);

		echo '--CHECKOUT DOCUMENT--' . CRLF;
		$aCode = $document->checkAccess();

		if ( ($aCode > 1) || ($aCode less than 0 || $aCode === 1) ) {
			throw new AccessCodeException(sprintf('LOCKED_%s_TO_PREVENT_THIS_ACTION', $aCode));
		}
		else {
			$document->lock(AccessCode::CHECKOUT, People\CurrentUser::get());
		}

		echo '--CHECKOUT DOCFILE--' . CRLF;
		$dfDao->loadDocfiles($document);
		foreach( $document->getDocfiles() as $docfile ) {
			if ( $docfile->checkAccess() !== 0 ) {
				continue;
			}
			else {
				$docfile->lock(AccessCode::CHECKOUT, People\CurrentUser::get());
			}
			$factory->getDao($docfile->cid)->save($docfile);
		}

		$factory->getDao($document->cid)->save($document);
	}

	/**
	 * Replace a file checkout in the vault and unlock it.
	 *
	 * The checkIn copy file from the wildspace to vault reposit dir
	 * If the file has been changed(check by md5 code comparaison), create a new iteration
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param Bool $releasing
	 *        	if true, release the docfile after replace
	 * @param Bool $checkAccess
	 *        	if true, check if access code is right
	 * @throws Exception
	 * @return Docfile\Version
	 */
	private function _checkInDocument(\Rbplm\Ged\Document\Version $document, $releasing = true, $checkAccess = true)
	{
		$dataPath = realpath('data');
		$factory = DaoFactory::get('Workitem');
		$reposit = Reposit::init($dataPath . '/repositTest/' . $this->tid);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));
		$historize = new Historize($vault, $factory);

		/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
		$dfDao = $factory->getDao(Docfile\Version::$classId);

		echo '--PREPARE FILES ON DISK--' . CRLF;
		$file1 = $dataPath . '/tmpTestfile1.txt';
		$file2 = $dataPath . '/tmpTestfile2.txt';
		file_put_contents($file1, 'version002' . "\n");
		file_put_contents($file2, 'version002' . "\n");

		echo '--CHECKIN DOCUMENT--' . CRLF;
		$aCode = $document->checkAccess();
		$coBy = $document->lockById;

		if ( ($aCode != 1 or $coBy != People\CurrentUser::get()->getId()) and $checkAccess ) {
			//throw new AccessCodeException(sprintf('LOCKED_%s_TO_PREVENT_THIS_ACTION', $aCode));
		}

		echo '--CHECKIN DOCFILES--' . CRLF;
		/* load docfiles */
		$filter = $dfDao->toSys('accessCode') . '=' . AccessCode::CHECKOUT . ' AND ' . $dfDao->toSys('lockById') . '=' . People\CurrentUser::get()->getId();
		$dfDao->loadDocfiles($document, $filter);

		foreach( $document->getDocfiles() as $docfile ) {
			$aCode = $docfile->checkAccess();
			$coBy = $docfile->lockById;

			$newFsdata = new \Rbplm\Sys\Fsdata($file1);
			$oldMd5 = $docfile->getData()->md5;
			$newMd5 = $newFsdata->getMd5();
			if ( $oldMd5 == $newMd5 ) {
				if ( $releasing ) {
					$docfile->unLock();
					$docfile->dao->save($docfile);
				}
			}
			else {
				$historize->depriveDocfileToIteration($docfile, $docfile->iteration);
				$vault->record($docfile->getData(), $newFsdata, $docfile->getName(), null, $replace=true);
				$docfile->iteration = $docfile->iteration + 1;
				// set the iteration id of document to the max iteration of docfiles
				if ( $nextDocumentIteration less than $docfile->iteration ) {
					$nextDocumentIteration = $docfile->iteration;
				}
				if ( $releasing ) {
					$docfile->unLock();
				}
				$docfile->dao->save($docfile);
			}
		}

		$document->unLock();

		if($nextDocumentIteration){
			if ( $document->iteration + 1 > $nextDocumentIteration ) {
				$nextDocumentIteration = $document->iteration + 1;
			}
			$document->iteration = $nextDocumentIteration;
		}
		$factory->getDao($document->cid)->save($document);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
		// DELETE
		echo "Delete Docfile, Document, Workitem, Project, Area, Process \n";
	}
}
