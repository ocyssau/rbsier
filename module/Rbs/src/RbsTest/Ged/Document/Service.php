// %LICENCE_HEADER%
namespace RbsTest\Ged\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People;
use Rbplm\Wf\Process;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\AccessCode;
use Rbplm\Vault\Reposit;
use Rbplm\Sys\Fsdata;
use Rbs\Lu\Area;
use Rbs\Lu\Right;
use Rbplm\Sys;
use Rbplm\Dao\NotExistingException;
use Rbplm\Sys\Date as DateTime;

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 */
class Service extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->tid = uniqid();
		Sys\Filesystem::isSecure(false);

		$this->dataPath = realpath('data/cache');
		$dataPath = $this->dataPath;
		var_dump($this->dataPath);

		$factory = DaoFactory::get('Workitem');
		$this->factory = $factory;

		$user = new People\User();
		$factory->getDao($user->cid)->loadFromLogin($user, 'admin');
		People\CurrentUser::set($user);

		echo '--LOAD PROJECT--' . CRLF;
		$project = Project::init('TESTPROJECT');
		try {
			$factory->getDao($project->cid)->loadFromName($project, 'TESTPROJECT');
		}
		catch( NotExistingException $e ) {
			$project->description = 'Projet de test';
			$area = new Area($project->getName());
			$right = new Right($area);
			$project->area = $area;
			$factory->getDao($area->cid)->save($area);
			$factory->getDao($right->cid)->save($right);
			$factory->getDao($project->cid)->save($project);
		}
		$this->project = $project;

		echo '--LOAD WORKITEM--' . CRLF;
		$workitem = Workitem::init('WIFORTEST1');
		try {
			$factory->getDao($workitem->cid)->loadFromName($workitem, 'WIFORTEST1');
		}
		catch( NotExistingException $e ) {
			$reposit = Reposit::init($dataPath . '/repositTest/' . $workitem->getName());
			$workitem->path = $reposit->getPath();
			$workitem->description = 'Wi de test';
			$workitem->setName('WIFORTEST1');
			$workitem->setUid('WIFORTEST1');

			$workitem->forseenCloseDate = new DateTime();
			$workitem->forseenCloseDate->add(new \DateInterval('P5Y')); // 5 years

			$workitem->setParent($project);
			$factory->getDao($workitem->cid)->save($workitem);
		}
		$this->workitem = $workitem;
	}

	/**
	 *
	 * @throws NotExistingException
	 */
	public function getDocument()
	{
		echo '--LOAD DOCUMENT v1--' . CRLF;
		try {
			$document = new Document\Version();
			$this->factory->getDao($document->cid)->loadFromName($document, 'DOCUMENTTEST_1');
			$document->setParent($this->workitem);

			echo '--LOAD DOCUMENT DOCFILES--' . CRLF;
			$this->factory->getDao(Docfile\Version::$classId)->loadDocfiles($document);
		}
		catch( NotExistingException $e ) {
			echo '--NONE DOCUMENT DOCUMENT v1--' . CRLF;
			$document = Document\Version::init('DOCUMENTTEST_1');
			$document->setUid('DOCUMENTTEST_1');
			$document->description = "Document version de test";
			$document->version = 1;
			$document->iteration = 1;
			$document->setParent($this->workitem);
		}

		return $document;
	}

	/**
	 */
	public function Test_Store()
	{
		$factory = $this->factory;
		$service = new \Rbs\Ged\Document\Service($factory);

		try {
			echo '--LOAD DOCUMENT v1--' . CRLF;
			$document = new Document\Version();

			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);
			$dao->loadFromName($document, 'DOCUMENTTEST_1');

			echo "-- DELETE DOCUMENT AND FILES" . CRLF;
			$service = new \Rbs\Ged\Document\Service($factory);
			$service->delete($document);
		}
		catch( NotExistingException $e ) {
			echo '--NONE DOCUMENT v1--' . CRLF;
		}

		echo '--CREATE DOCUMENT v1--' . CRLF;
		$document = Document\Version::init('DOCUMENTTEST_1');
		$document->setUid('DOCUMENTTEST_1');
		$document->setNumber('DOCUMENTTEST_1');
		$document->description = "Document version de test";
		$document->version = 1;
		$document->iteration = 1;
		$document->setParent($this->workitem);

		echo '--PREPARE FILES ON DISK--' . CRLF;
		$file1 = '/tmp/Testfile1.txt';
		$file2 = '/tmp/Testfile2.txt';
		touch($file1);
		touch($file2);
		\file_put_contents($file1, 'version001' . "\n");
		\file_put_contents($file2, 'version001' . "\n");

		echo '--CREATE DOCFILES--' . CRLF;
		$fsdata1 = new Fsdata($file1);
		$fsdata2 = new Fsdata($file2);

		$docfile1 = Docfile\Version::init()->setParent($document);
		$docfile2 = Docfile\Version::init()->setParent($document);

		/* Set fsdata property to indicate file must be vaulted */
		$docfile1->fsdata = $fsdata1;
		$docfile2->fsdata = $fsdata2;
		$document->addDocfile($docfile1)->addDocfile($docfile2);

		/* Set dao to objects */
		$docfile1->dao = $factory->getDao($docfile1->cid);
		$docfile2->dao = $factory->getDao($docfile2->cid);
		$document->dao = $factory->getDao($document->cid);

		/* Create document */
		echo '--CREATE DOCUMENT SERVICE--' . CRLF;
		$service->createDocument($document, $lock = false);
		var_dump($service->respons);
	}

	/**
	 */
	public function Test_Checkout()
	{
		$factory = $this->factory;
		$service = new \Rbs\Ged\Document\Service($factory);
		$document = $this->getDocument();

		/* Dump Ws path */
		$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
		var_dump($wildspace->getPath());

		/* Call service */
		$service->checkout($document, $withFiles = true, $replace = true, $getFiles = true);

		$wsFile1 = $wildspace->getPath() . '/Testfile1.txt';
		$wsFile2 = $wildspace->getPath() . '/Testfile2.txt';

		assert(is_file($wsFile1));
		assert(is_file($wsFile2));

		var_dump($service->respons);
	}

	/**
	 */
	public function Test_CheckinFromWs()
	{
		$factory = $this->factory;
		$service = new \Rbs\Ged\Document\Service($factory);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$document = $this->getDocument();
		$user = People\CurrentUser::get();

		/* Dump Ws path */
		$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
		$dataPath = realpath($wildspace->getPath());
		var_dump($dataPath);

		echo '--PREPARE FILES ON DISK--' . CRLF;
		$file1 = $dataPath . '/Testfile1.txt';
		$file2 = $dataPath . '/Testfile2.txt';
		file_put_contents($file1, uniqid('version') . "\n");
		file_put_contents($file2, uniqid('version') . "\n");

		/* Load docfiles to checkin */
		$filter = $dfDao->toSys('accessCode') . '=' . AccessCode::CHECKOUT . ' AND ' . $dfDao->toSys('lockById') . '=' . $user->getId();
		$dfDao->loadDocfiles($document, $filter);

		assert(count($document->getDocfiles()) > 0);

		/* Call service */
		$data1 = \file_get_contents($file1);
		$service->checkin($document, $releasing = true, $updateData = true, $deleteFileInWs = true, $fromWildspace = true, $checkAccess = true);
		var_dump($service->respons);

		/* content in vault == new data */
		$vaultedFile1 = current($document->getDocfiles())->data->fullpath;
		$data2 = \file_get_contents($vaultedFile1);
		var_dump($data1, $data2, $vaultedFile1);
		assert($data1 == $data2);

		$wsFile1 = $wildspace->getPath() . '/Testfile1.txt';
		$wsFile2 = $wildspace->getPath() . '/Testfile2.txt';
		assert(!is_file($wsFile1));
		assert(!is_file($wsFile2));

		$service->checkout($document, $withFiles = true, $replace = true, $getFiles = false);
		assert(!is_file($wsFile1));
		assert(!is_file($wsFile2));

		$service->checkin($document, $releasing = true, $updateData = false, $deleteFileInWs = false, $fromWildspace = true, $checkAccess = true);
		assert(!is_file($wsFile1));
		assert(!is_file($wsFile2));

		$service->checkout($document, $withFiles = true, $replace = true, $getFiles = true);
		assert(is_file($wsFile1));
		assert(is_file($wsFile2));

		$service->checkin($document, $releasing = true, $updateData = false, $deleteFileInWs = false, $fromWildspace = true, $checkAccess = true);
		assert(is_file($wsFile1));
		assert(is_file($wsFile2));
	}

	/**
	 */
	public function Test_Checkin()
	{
		$factory = $this->factory;
		$service = new \Rbs\Ged\Document\Service($factory);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$document = $this->getDocument();
		$user = People\CurrentUser::get();

		/* Dump Ws path */
		$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
		var_dump($wildspace->getPath());

		/* Load docfiles to checkin */
		$filter = $dfDao->toSys('accessCode') . '=' . AccessCode::CHECKOUT . ' AND ' . $dfDao->toSys('lockById') . '=' . $user->getId();
		$dfDao->loadDocfiles($document, $filter);
		assert(count($document->getDocfiles()) > 0);

		$mainDf = current($document->getDocfiles());
		$df2 = next($document->getDocfiles());

		echo '--PREPARE FILES ON DISK--' . CRLF;
		$file1 = '/tmp/Testfile1.txt';
		touch($file1);
		\file_put_contents($file1, uniqid('version') . "\n");

		$file2 = '/tmp/Testfile1.txt';
		touch($file2);
		\file_put_contents($file2, uniqid('version') . "\n");

		/* Set fsdata property with fsdata of new data to store in vault */
		$mainDf->fsdata = new \Rbplm\Sys\Fsdata($file1);
		$df2->fsdata = new \Rbplm\Sys\Fsdata($file2);

		/* Call service */
		$preIteration = $document->iteration;
		$service->checkin($document, $releasing = true, $updateData = true, $deleteFileInWs = true, $fromWildspace = false, $checkAccess = true);
		var_dump($service->respons);

		/* document must be iterated */
		assert($document->iteration == $preIteration + 1);

		/* $deleteFileInWs option is ignored when checkin out of ws */
		$wsFile1 = $wildspace->getPath() . '/Testfile1.txt';
		$wsFile2 = $wildspace->getPath() . '/Testfile2.txt';
		assert(is_file($wsFile1));
		assert(is_file($wsFile2));

		/* content in vault == new data */
		$vaultedFile1 = $mainDf->data->fullpath;
		$data1 = \file_get_contents($file1);
		$data2 = \file_get_contents($vaultedFile1);

		var_dump($data1, $data2, $vaultedFile1);
		assert($data1 == $data2);

		/* iterations */

	}

	/**
	 */
	public function Test_Reset()
	{
		$factory = $this->factory;
		$service = new \Rbs\Ged\Document\Service($factory);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$document = $this->getDocument();
		$user = People\CurrentUser::get();

		/* Load docfiles to checkin */
		$filter = $dfDao->toSys('accessCode') . '=' . AccessCode::CHECKOUT . ' AND ' . $dfDao->toSys('lockById') . '=' . $user->getId();
		$dfDao->loadDocfiles($document, $filter);
		assert(count($document->getDocfiles()) > 0);

		/*get before datas*/
		$mainDf = current($document->getDocfiles());
		$data1 = \file_get_contents($mainDf->getData()->fullpath);

		/**/
		echo '--PREPARE FILES ON DISK--' . CRLF;
		$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
		$dataPath = realpath($wildspace->getPath());
		$file1 = $dataPath . '/Testfile1.txt';
		file_put_contents($file1, uniqid('version') . "\n");

		/*Call service*/
		$service->checkin($document, $releasing = true, $updateData = false, $deleteFileInWs = true, $fromWildspace = false, $checkAccess = true);
		$data2 = \file_get_contents($mainDf->getData()->fullpath);
		var_dump($service->respons);
		assert($data1==$data2);

		/* files are not deleted from ws */
		assert(is_file($file1));
	}

	/**
	 */
	public function Test_Update()
	{
		$factory = $this->factory;
		$service = new \Rbs\Ged\Document\Service($factory);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$document = $this->getDocument();
		$user = People\CurrentUser::get();

		/* Load docfiles to checkin */
		$filter = $dfDao->toSys('accessCode') . '=' . AccessCode::CHECKOUT . ' AND ' . $dfDao->toSys('lockById') . '=' . $user->getId();
		$dfDao->loadDocfiles($document, $filter);
		assert(count($document->getDocfiles()) > 0);

		/**/
		echo '--PREPARE FILES ON DISK--' . CRLF;
		$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
		$dataPath = realpath($wildspace->getPath());
		$file1 = $dataPath . '/Testfile1.txt';
		file_put_contents($file1, uniqid('version') . "\n");

		/*Call service*/
		$service->checkin($document, $releasing = false, $updateData = true, $deleteFileInWs = false, $fromWildspace = true, $checkAccess = true);
		var_dump($service->respons);

		/* content in vault == new data */
		$mainDf = current($document->getDocfiles());
		$vaultedFile1 = $mainDf->data->fullpath;
		$data1 = \file_get_contents($file1);
		$data2 = \file_get_contents($vaultedFile1);
		assert($data1 == $data2);
	}

	/**
	 */
	public function Test_Copy()
	{
		$factory = $this->factory;
		$service = new \Rbs\Ged\Document\Service($factory);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);
		$dao = $factory->getDao(Document\Version::$classId);

		$document = $this->getDocument();
		$toDocument = $service->copyDocument($document, $this->workitem, 'COPY1', 1);

		foreach( $toDocument->getDocfiles() as $docfile ) {
			$fsdata = $docfile->getData()->getFsdata();
			var_dump($fsdata->getFullpath());
		}
	}

	/**
	 */
	public function Test_Delete()
	{
		$factory = $this->factory;
		$document = $this->getDocument();

		$service = new \Rbs\Ged\Document\Service($factory);
		$service->delete($document);
	}

	/**
	 *
	 */
	public function Test_Newversion()
	{
		$factory = $this->factory;

		$document = $this->getDocument();
		$service = new \Rbs\Ged\Document\Service($factory);
	}

	/**
	 *
	 */
	public function Test_DeleteIteration()
	{
		$factory = $this->factory;
		$document = $this->getDocument();
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		/* delete obsolete iterations */
		$toKeep = \Ranchbe::get()->getConfig('document.assocfile.iterationtokeep');
		foreach($document->getDocfiles() as $docfile){
			$dfService->deleteIterations($docfile, $toKeep);
		}
	}






	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
		// DELETE
		echo "Delete Docfile, Document, Workitem, Project, Area, Process \n";
	}
}
