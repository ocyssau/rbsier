<?php
//%LICENCE_HEADER%

namespace RbsTest\Pdm;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Pdm\Product\Version as ProductVersion;
use Rbplm\Pdm\Product\Instance as ProductInstance;
use Rbplm\Dao\NotExistingException;

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 *
 */
class Test extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	/**
	 *
	 */
	public function Test_ProductVersionDao()
	{
		$factory = DaoFactory::get('Workitem');

		try{
			$document = new DocumentVersion();
			$factory->getDao($document->cid)->loadFromName($document, 'PRODUCT001');
		}
		catch(NotExistingException $e){
			$document = DocumentVersion::init('PRODUCT001');
			$document->number='PRODUCT001';
			$document->description = "Document version de test";
			$document->version = 1;
			$document->iteration = 1;
			$document->parentId = 331;
			$document->doctypeId = 1;
			$factory->getDao($document->cid)->save($document);
		}

		try{
			$product = new ProductVersion();
			$product->dao = $factory->getDao(ProductVersion::$classId);
			$product->dao->loadFromNumber($product, 'PRODUCT001');
		}
		catch(NotExistingException $e){
			$product = ProductVersion::init('PRODUCT001');
			$productData = array(
				'name'=>'PRODUCT001',
				'number'=>'PRODUCT001',
				'nomenclature'=>1,
				'revision'=>1,
				'definition'=>'',
				'description'=>'FOR TEST ONLY',
				'spacename'=>$factory->getName(),
				'ofProductId'=>100,
			);
			$product->hydrate($productData);
		}

		$product->setDocument($document);

		$pysicals = $product->getPhysicalProperties();
		$pysicals->setWeight(100);
		$pysicals->setWetSurface(50);
		$pysicals->setVolume(20);
		$pysicals->setDensity(2750);

		$gravityCenter = new \Rbplm\Geometric\Point(array(10,10,10));
		$pysicals->setGravityCenter($gravityCenter);
		$inertiaCenter = new \Rbplm\Geometric\Point(array(100,100,500));
		$pysicals->setInertiaCenter($inertiaCenter);
		$product->setPhysicalProperties($pysicals);

		$product->addMaterial('aluminium');

		$product->dao = $factory->getDao(ProductVersion::$classId);
		$product->dao->save($product);
	}

	/**
	 *
	 */
	public function Test_ProductInstanceDao()
	{
		$factory = DaoFactory::get('Workitem');

		$document = new DocumentVersion();
		$factory->getDao($document->cid)->loadFromName($document, 'DOCUMENTTEST_1');

		$product = new ProductVersion();
		$product->dao = $factory->getDao(ProductVersion::$classId);
		$product->dao->loadFromNumber($product, 'PRODUCT001');

		$instanceDao = $factory->getDao(ProductInstance::$classId);

		try{
			$instance = new ProductInstance();
			$instanceDao->loadFromName($instance, 'PRODUCT001.1');
		}
		catch(NotExistingException $e){
			$instance = ProductInstance::init('PRODUCT001.1');
			$productData = array(
				'number'=>'PRODUCT001.1',
				'nomenclature'=>1,
			);
			$instance->hydrate($productData);
		}
		$instance->setOfProduct($product);
		$instanceDao->save($instance);

		try{
			$instance2 = new ProductInstance();
			$instanceDao->loadFromName($instance2, 'PRODUCT002.1');
		}
		catch(NotExistingException $e){
			$instance2 = ProductInstance::init('PRODUCT002.1');
			$productData = array(
				'number'=>'PRODUCT002.1',
				'nomenclature'=>1,
				'position'=>array(0,0,1,0,1,0,1,0,0,10,10,10),
			);
			$instance2->hydrate($productData);
		}
		$position =& $instance2->getPosition();
		var_dump($position);
		$position->setFromArray(array(0,0,1,0,1,0,1,0,0,5,10,20));

		$instance2->setParent($instance);
		$instance2->quantity = 10;
		$instance2->description = "FOR TEST";

		$instanceDao->save($instance2);
	}


	/**
	 *
	 */
	public function Test_Reconciliate()
	{
		$factory = DaoFactory::get('Workitem');
		$instanceDao = $factory->getDao(ProductInstance::$classId);
		$productDao = $factory->getDao(ProductVersion::$classId);

		try{
			$product = new ProductVersion();
			$productDao->loadFromNumber($product, 'PRODUCT002');
		}
		catch(NotExistingException $e){
			$product = ProductVersion::init('PRODUCT002');
			$product->number='PRODUCT002';
			$product->description = "Document version de test";
			$product->version = 1;
			$product->spacename = $factory->getName();
			$productDao->save($product);
		}

		$instance = new ProductInstance();
		$instanceDao->loadFromName($instance, 'PRODUCT002.1');

		/* Code a placer dans une classe separee et adaptable selon les regle de nommage */
		$instanceName = $instance->getName();
		$productNumber = substr($instanceName, 0, strrpos($instanceName, '.'));

		var_dump($productNumber);

		$product = new ProductVersion();
		$productDao->loadFromNumber($product, $productNumber);
		$instance->setOfProduct($product);
		$instanceDao->save($instance);

		var_dump($product->getArrayCopy());
	}
}
