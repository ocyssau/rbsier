<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'batch-jobs' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/batch/jobs',
					'defaults' => array(
						'controller' => 'Batch\Controller\Job',
						'action' => 'index',
					),
				),
			),
			'batch-job' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/batch/job/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Batch\Controller\Job',
						'action' => 'edit',
					),
				),
			),
			'batch-cron' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/batch/cron[/][:action]',
					'defaults' => array(
						'controller' => 'Batch\Controller\Cron',
						'action' => 'index',
					),
				),
			),
		),
	),
	'console' => array(
		'router'=>array(
			'routes'=>array(
				'batch-cron-run'=>array(
					'type'=>'simple',
					'options'=>array(
						'route'=>'cron run [--user=] [--pwd=] [-f] [<cronName>]',
						'defaults' => array(
							'controller'=>'Batch\Controller\Cli',
							'action'=>'cronrun'
						)
					)
				),
				'notauthorized'=>array(
					'type'=>'simple',
					'options'=>array(
						'route'=>'notauthorized',
						'defaults' => array(
							'controller'=>'Batch\Controller\Cli',
							'action'=>'notauthorized'
						)
					)
				)
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Batch\Controller\Job'=>'Batch\Controller\JobController',
			'Batch\Controller\Cron'=>'Batch\Controller\CronController',
			'Batch\Controller\Cli'=>'Batch\Controller\CliController',
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	),
	'menu' => [
		'admin' => [
			'separator' =>[
				'name' => uniqid(),
				'class' => 'separator'
			],
			'callback' => [
				'name' => 'callback',
				'route' => [
					'params' => [
						'action' => 'index'
					],
					'options' => [
						'name' => 'admin-callback'
					]
				],
				'class' => '%anchor%',
				'label' => 'Callbacks'
			],
			'cron' => [
				'name' => 'cron',
				'route' => [
					'params' => [
						'action' => 'index'
					],
					'options' => [
						'name' => 'batch-cron'
					]
				],
				'class' => '%anchor%',
				'label' => 'Crons'
			],
			'jobs' => [
				'name' => 'jobs',
				'route' => [
					'params' => [
						'action' => 'index'
					],
					'options' => [
						'name' => 'batch-jobs'
					]
				],
				'class' => '%anchor%',
				'label' => 'Jobs'
			]
		]
	]
);
