<?php
namespace Batch\Form\Cron;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Application\Form\Hydrator\ArrayToJsonStrategie;

/**
 *
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	public $template;

	public $ranchbe;

	protected $inputFilter;

	/**
	 *
	 * @param \Application\View\ViewModel $view
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($view, $factory)
	{
		parent::__construct('cronEditForm');
		$this->template = 'batch/cron/editform';
		$this->setAttribute('method', 'post')->setInputFilter(new InputFilter());

		$hydrator = new Hydrator();
		$hydrator->addStrategy('callbackParams', new ArrayToJsonStrategie());
		$this->setHydrator($hydrator);

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Set A Convenient Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Callback Class */
		$this->add(array(
			'name' => 'callbackClass',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '\Full\Class\Name',
				'class' => 'form-control',
				'size' => 48
			),
			'options' => array(
				'label' => tra('Callback Class')
			)
		));

		/* Callback Method */
		$this->add(array(
			'name' => 'callbackMethod',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Method',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Callback Method')
			)
		));

		/* Callback Params */
		$this->add(array(
			'name' => 'callbackParams',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '{"name1":"value1", "name2":"value2"}',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Callback Params')
			)
		));

		/* Frequency */
		$this->add(array(
			'name' => 'frequency',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Frequency'),
				'value_options' => $this->_getFrequency()
			)
		));

		/* Schedule Start */
		$this->add(array(
			'name' => 'scheduleStart',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Schedule Start At Hour'),
				'value_options' => $this->_getStart()
			)
		));

		/* Schedule End */
		$this->add(array(
			'name' => 'scheduleEnd',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Schedule End At Hour'),
				'value_options' => $this->_getEnd()
			)
		));

		/* Is Actif */
		$this->add(array(
			'name' => 'actif',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-check-input',
				'id' => 'isActifCb'
			),
			'options' => array(
				'label' => tra('Is Actif')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => false
			),
			'name' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z0-9-_.]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, numbers, space, and -,_.'
							)
						)
					)
				)
			),
			'callbackClass' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z\\\\]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, and \\'
							)
						)
					)
				)
			),
			'callbackMethod' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters'
							)
						)
					)
				)
			),
			'callbackParams' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => '\Application\Validator\Json',
						'options' => array(
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Is not a valid JSON'
							)
						)
					)
				)
			),
			'frequency' => array(
				'required' => false
			),
			'scheduleStart' => array(
				'required' => false
			),
			'scheduleEnd' => array(
				'required' => false
			),
			'actif' => array(
				'required' => false
			)
		);
	}

	/**
	 * 
	 */
	protected function _getFrequency()
	{
		return array(
			60 => '1 minute',
			60 * 5 => '5 minutes',
			60 * 10 => '10 minutes',
			60 * 15 => '15 minutes',
			60 * 20 => '20 minutes',
			60 * 25 => '25 minutes',
			60 * 30 => '30 minutes',
			60 * 40 => '40 minutes',
			60 * 50 => '50 minutes',
			60 * 60 => '1 hour',
			60 * 60 * 2 => '2 hours',
			60 * 60 * 3 => '3 hours',
			60 * 60 * 4 => '4 hours',
			60 * 60 * 5 => '5 hours',
			60 * 60 * 6 => '6 hours',
			60 * 60 * 7 => '7 hours',
			60 * 60 * 8 => '8 hours',
			60 * 60 * 9 => '9 hours',
			60 * 60 * 10 => '10 hours',
			60 * 60 * 11 => '11 hours',
			60 * 60 * 12 => '12 hours',
			60 * 60 * 13 => '13 hours',
			60 * 60 * 14 => '14 hours',
			60 * 60 * 15 => '15 hours',
			60 * 60 * 16 => '16 hours',
			60 * 60 * 17 => '17 hours',
			60 * 60 * 18 => '18 hours',
			60 * 60 * 19 => '19 hours',
			60 * 60 * 20 => '20 hours',
			60 * 60 * 21 => '21 hours',
			60 * 60 * 22 => '22 hours',
			60 * 60 * 23 => '23 hours',
			60 * 60 * 24 => '1 day',
			60 * 60 * 24 * 2 => '2 days',
			60 * 60 * 24 * 3 => '3 days',
			60 * 60 * 24 * 4 => '4 days',
			60 * 60 * 24 * 5 => '5 days',
			60 * 60 * 24 * 6 => '6 days',
			60 * 60 * 24 * 7 => 'Each week',
			60 * 60 * 24 * 7 * 4 => 'Each month'
		);
	}

	/**
	 *
	 */
	protected function _getStart()
	{
		return range(0, 23, 1);
	}

	/**
	 *
	 */
	protected function _getEnd()
	{
		return range(0, 23, 1);
	}
}
