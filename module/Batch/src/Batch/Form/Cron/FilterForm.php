<?php
namespace Batch\Form\Cron;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 * 
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @param \Rbs\Space\Factory $factory
	 * @param string $nameSpace
	 */
	public function __construct($factory, $nameSpace)
	{
		parent::__construct($factory, $nameSpace);

		$this->template = 'batch/cron/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Name */
		$this->add(array(
			'name' => 'find_name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for name...',
				'class' => 'form-control',
				'data-where' => 'name',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Name'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_name',
			'required' => false
		));

		/* Designation */
		$this->add(array(
			'name' => 'find_designation',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for designation...',
				'class' => 'form-control',
				'data-where' => 'description',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_designation',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(\Rbplm\Ged\Doctype::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* NAME */
		if ( $datas['find_name'] ) {
			$filter->andFind($datas['find_name'], $dao->toSys('name'), Op::OP_CONTAINS);
		}

		/* DESIGNATION */
		if ( $datas['find_designation'] ) {
			$filter->andFind($datas['find_designation'], $dao->toSys('description'), Op::OP_CONTAINS);
		}

		return $this;
	}
}
