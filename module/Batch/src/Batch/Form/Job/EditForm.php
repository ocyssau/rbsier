<?php
namespace Batch\Form\Job;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Application\Form\Hydrator\ArrayToJsonStrategie;

/**
 *
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view, $factory)
	{
		parent::__construct('jobEditorForm');
		$this->template = 'batch/job/editform';
		$this->setAttribute('method', 'post')->setInputFilter(new InputFilter());

		$hydrator = new Hydrator();
		$hydrator->addStrategy('callbackParams', new ArrayToJsonStrategie());
		$this->setHydrator($hydrator);

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Set A Convenient Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Callback Class */
		$this->add(array(
			'name' => 'callbackClass',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '\Full\Class\Name',
				'class' => 'form-control',
				'size' => 48
			),
			'options' => array(
				'label' => tra('Callback Class')
			)
		));

		/* Callback Method */
		$this->add(array(
			'name' => 'callbackMethod',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Method',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Callback Method')
			)
		));

		/* Callback Params */
		$this->add(array(
			'name' => 'callbackParams',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '{"name1":"value1", "name2":"value2"}',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Callback Params')
			)
		));

		/* Planned */
		$this->add(array(
			'name' => 'planned',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Click to select date',
				'class' => 'form-control datetimepicker',
				'id' => 'planned-date',
				'min' => date(DATE_ISO8601)
			),
			'options' => array(
				'label' => tra('Planned At'),
				'format' => DATE_ISO8601
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'name' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z0-9-_\.]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, numbers, space, and -,_,.'
							)
						)
					)
				)
			),
			'callbackClass' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z\\\\]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, and \\'
							)
						)
					)
				)
			),
			'callbackMethod' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters'
							)
						)
					)
				)
			),
			'callbackParams' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => '\Application\Validator\Json',
						'options' => array(
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters'
							)
						)
					)
				)
			),
			'planned' => array(
				'required' => false
			)
		);
	}
}
