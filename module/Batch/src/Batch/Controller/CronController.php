<?php
namespace Batch\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Batch\CronTask as Cron;
use Batch\Service\CronService;
use Rbplm\People;

/**
 *
 *
 */
class CronController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'batch_cron';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'batch/cron/index';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'admin/tools';

	/**
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('cron');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$request = $this->getRequest();
		$view = $this->view;
		$view->setTemplate('batch/cron/index/index');
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Cron::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Batch\Form\Cron\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'cronfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Cron::$classId);
		$select = [];
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->load([
			'default' => [
				'orderby' => 'name',
				'order' => 'asc',
				'limit' => 50
			]
		])
			->setMaxLimit($list->countAll)
			->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);

		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Cron Manager';
		return $view;
	}

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$cron = Cron::init();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Cron::$classId);
		$cron->setOwner(People\CurrentUser::get());
		$cron->setStatus(Cron::STATE_INIT);
		$cron->isActif(true);
		$cron->setScheduleStart(0);
		$cron->setScheduleEnd(23);
		$cron->setFrequency(3600);

		$form = new \Batch\Form\Cron\EditForm($view, $factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($cron);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($cron);
				return $this->successForward();
			}
		}

		$view->pageTitle = tra('Create Cron');
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$cronIds = $request->getQuery('checked', array());
			$cronId = $cronIds[0];
			$validate = false;
		}
		if ( $request->isPost() ) {
			$cronId = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$cron = new Cron();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Cron::$classId);
		$dao->loadFromId($cron, $cronId);

		$form = new \Batch\Form\Cron\EditForm($view, $factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($cron);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());

			if ( $form->isValid() ) {
				$dao->save($cron);
				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Edit Callback %s'), $cron->getName());
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$cronIds = $request->getQuery('checked', []);
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Cron::$classId);

		foreach( $cronIds as $cronId ) {
			try {
				$dao->deleteFromId($cronId);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $cronId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}

	/**
	 *
	 */
	public function runAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$cronIds = $request->getQuery('checked', []);
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Cron::$classId);
		$cronService = new CronService();

		foreach( $cronIds as $cronId ) {
			try {
				$cron = new Cron();
				$dao->loadFromId($cron, $cronId);
				$cronService->run($cron);
				$cron->dao->save($cron);
			}
			catch( \PDOException $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}

	/**
	 *
	 */
	public function syslogAction()
	{
		$view = $this->view;
		$config = \Ranchbe::get()->getConfig();

		$logfile = $config['cron.log.path'];
		if ( is_file($logfile) ) {
			$content = file_get_contents($logfile);
		}
		else {
			$content = '';
		}

		$view->pageTitle = 'Sys Log of Crons: ' . $logfile;
		$view->logfile = $logfile;
		$view->log = $content;
		return $view;
	}

	/**
	 * Display statistics of a task
	 *
	 * @return void
	 **/
	function showStatistics()
	{
		$query = "SELECT MIN(`date`) AS datemin,
	                          MIN(`elapsed`) AS elapsedmin,
	                          MAX(`elapsed`) AS elapsedmax,
	                          AVG(`elapsed`) AS elapsedavg,
	                          SUM(`elapsed`) AS elapsedtot,
	                          MIN(`volume`) AS volmin,
	                          MAX(`volume`) AS volmax,
	                          AVG(`volume`) AS volavg,
	                          SUM(`volume`) AS voltot
                   FROM `glpi_crontasklogs`
                   WHERE `crontasks_id` = '" . $this->fields['id'] . "'";
		return $query;
	}
}
