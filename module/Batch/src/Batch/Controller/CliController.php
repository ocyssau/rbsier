<?php
namespace Batch\Controller;

use Application\Controller\AbstractCliController as AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Batch\CronTask as Cron;
use Rbs\Batch\Job;
use Batch\Service\CronService;
use Batch\Service\JobService;

/**
 *  
 *
 */
class CliController extends AbstractController
{

	/**
	 */
	public function init()
	{
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 * cron run [--user=] [--pwd=] [-f] [<cronName>]
	 * 
	 * @throws \RuntimeException
	 */
	public function cronrunAction()
	{
		$request = $this->getRequest();
		$this->authenticate($request);

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		$force = $request->getParam('f');
		$name = $request->getParam('cronName');

		if ( $force && $name ) {
			return $this->runnow($name);
		}

		/* init some helpers */
		$factory = DaoFactory::get();
		/** @var \Rbs\Batch\CronTaskDao $dao */
		$dao = $factory->getDao(Cron::$classId);
		$jobDao = $factory->getDao(Job::$classId);

		/* Run crontasks */
		$cronService = new CronService();

		$stmt = $dao->getNeedToRun();
		$crons = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		foreach( $crons as $cronDatas ) {
			$cron = new Cron();
			$cron->dao = $dao;
			$dao->hydrate($cron, $cronDatas);
			$cronService->run($cron);
			$cron->dao->save($cron);
		}

		/* Run jobs */
		$jobService = new JobService();
		$stmt = $jobDao->getNeedToRun();
		$jobs = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		foreach( $jobs as $jobEntry ) {
			$job = new Job();
			$job->dao = $jobDao;
			$jobDao->hydrate($job, $jobEntry);
			$jobService->run($job);
		}

		return $this;
	}

	/**
	 * Force the task to run without checking schedulle
	 * @throws \RuntimeException
	 */
	protected function runnow($name, $save = false)
	{
		/* init some helpers */
		$factory = DaoFactory::get();
		/** @var \Rbs\Batch\CronTaskDao $dao */
		$dao = $factory->getDao(Cron::$classId);

		/* Run crontasks */
		$cronService = new CronService();
		$cron = new Cron();
		$dao->loadFromName($cron, $name);
		$cronService->run($cron);
		if ( $save ) $cron->dao->save($cron);

		return $this;
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	public function runjobsAction()
	{
		$request = $this->getRequest();
		$this->authenticate($request);

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* init some helpers */
		$factory = DaoFactory::get();

		/** @var \Rbs\Batch\CronTaskDao $dao */
		$jobDao = $factory->getDao(Job::$classId);

		/* Load jobs candidate to execute */
		$stmt = $jobDao->getNeedToRun();
		$jobs = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$jobService = new JobService();
		foreach( $jobs as $jobEntry ) {
			$job = new Job();
			$job->dao = $jobDao;
			$dao->hydrate($job, $jobEntry);
			$jobService->run($job);
		}
	}
}
