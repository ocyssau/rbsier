<?php
namespace Batch\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Batch\Job;
use Exception;

/**
 *
 *
 */
class JobController extends AbstractController
{

	/** @var string */
	public $pageId = 'batch_job';

	/** @var string */
	public $defaultSuccessForward = 'batch/jobs';

	/** @var string */
	public $defaultFailedForward = 'admin/tools';

	/**
	 */
	public function init()
	{
		parent::init();

		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('batch');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Job::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Batch\Form\Job\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'jobfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Job::$classId);
		$select = [];
		foreach( $dao->metaModel as $asSys => $asApp ) {
			if ( $asApp == 'log' ) continue;
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->load([
			'default' => [
				'orderby' => 'created',
				'order' => 'desc',
				'limit' => 50
			]
		])
			->setMaxLimit($list->countAll)
			->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);

		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Batch Jobs';
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$job = Job::init('job');
		$job->setPlanned(new \DateTime());
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Job::$classId);

		$form = new \Batch\Form\Job\EditForm($view, $factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($job);
		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$job->hash();
				$dao->save($job);
				return $this->successForward();
			}
		}

		$view->pageTitle = tra('Create Job');
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$jobId = $this->params()->fromRoute('id');
		if ( $request->isGet() ) {
			if ( !$jobId ) {
				$jobIds = $request->getQuery('checked', array());
				$jobId = $jobIds[0];
			}
			$validate = false;
		}
		else {
			if ( !$jobId ) {
				$jobId = $request->getPost('id', null);
			}
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		/* Init Model */
		$job = new Job();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Job::$classId);
		$dao->loadFromId($job, $jobId);

		/* Init Form */
		$form = new \Batch\Form\Job\EditForm($view, $factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($job);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($job);
				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Edit Job %s'), $job->getName());
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$jobIds = $request->getQuery('checked', array());
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Job::$classId);

		foreach( $jobIds as $jobId ) {
			try {
				$dao->deleteFromId($jobId);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $jobId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}

	/**
	 *
	 */
	public function killAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$jobIds = $request->getQuery('checked', array());
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Job::$classId);

		foreach( $jobIds as $jobId ) {
			try {
				$job = new Job();
				$dao->loadFromId($job, $jobId);
				$pid = $job->getPid();
				//$sapi = $job->getSapi();
				/* kill the unix process */
				$ret = null;
				system('kill ' . $pid, $ret);
				/* if cant, mark as to kill */
				$job->setStatus('killed');
				$dao->save($job);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $jobId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}

	/**
	 *
	 */
	public function getlogAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$jobId = $this->params()->fromRoute('id');
		if ( $request->isGet() ) {
			if ( !$jobId ) {
				$jobIds = $request->getQuery('checked', array());
				$jobId = $jobIds[0];
			}
			//$validate = false;
		}
		else {
			throw new Exception('MUST BE HTTP GET ONLY');
		}

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Job::$classId);

		$job = new Job();
		$dao->loadFromId($job, $jobId);
		$view->pageTitle = 'Log of job ' . $job->getName();
		$view->job = $job;

		return $view;
	}

	/**
	 *
	 */
	public function resetAction()
	{
		$request = $this->getRequest();
		$jobId = $this->params()->fromRoute('id');
		if ( $request->isGet() ) {
			if ( !$jobId ) {
				$jobIds = $request->getQuery('checked', array());
				$jobId = $jobIds[0];
			}
		}
		else {
			throw new Exception('MUST BE HTTP GET ONLY');
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		/* Init Model */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Job::$classId);

		foreach( $jobIds as $jobId ) {
			try {
				$job = new Job();
				$dao->loadFromId($job, $jobId);
				$job->setStatus('init');
				$job->hydrate(array(
					'started' => 0,
					'ended' => 0,
					'duration' => 0
				));
				$dao->save($job);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		return $this->successForward();
	}
}
