<?php
//%LICENCE_HEADER%
namespace Batch\Cron;

use Rbs\Batch\CallbackInterface;
use Rbplm\Dao\Connexion;

/**
 * CallbackInterface interface
 * 
 * Must be implemented by callbock for execution of cronTasks
 * 
 */
class ProcessKiller implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 * 
	 */
	public function run()
	{
		$connexion = Connexion::get();

		$deleteSql = "DELETE FROM batchjob WHERE id=:id";
		$deleteStmt = $connexion->prepare($deleteSql);

		$sql = "SELECT * FROM batchjob WHERE status='killed'";
		$stmt = $connexion->prepare($sql);
		$stmt->execute();

		foreach( $stmt->fetchAll(\PDO::FETCH_ASSOC) as $job ) {
			$pid = $job['pid'];
			#$sapi = $job['sapi'];
			$id = $job['id'];
			try {
				$ret = null;
				system('kill ' . $pid, $ret);
				$deleteStmt->execute(array(
					':id',
					$id
				));
			}
			catch( \Exception $e ) {
				throw $e;
			}
		}
	}
}
