<?php
//%LICENCE_HEADER%
namespace Batch\Cron;

use Rbs\Batch\CallbackInterface;
use Rbs\Space\Factory as DaoFactory;

/**
 * CallbackInterface interface
 *
 * Must be implemented by callbock for execution of cronTasks
 *
 */
class JobCleaner implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 * max execution time
	 * may be set as: 
	 * n year, 
	 * n month, 
	 * n day, 
	 * n hour
	 */
	protected $maxExecutionTime = '6 hour';

	/** 
	 * max age for retention of jobs 
	 * may be set as: 
	 * n year, 
	 * n month, 
	 * n day, 
	 * n hour
	 */
	protected $maxJobAge = '1 month';

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;

		isset($params['maxExecutionTime']) ? $this->maxExecutionTime = $params['maxExecutionTime'] : null;
		isset($params['maxJobAge']) ? $this->maxJobAge = $params['maxJobAge'] : null;
	}

	/**
	 *
	 */
	public function run()
	{
		echo 'Cleanup of obsolete Jobs older than ' . $this->maxJobAge . "\n";
		$dao = DaoFactory::get()->getDao(\Rbs\Batch\Job::$classId);
		$older = new \DateTime();
		$older->modify('- ' . $this->maxJobAge);
		$dao->cleanup($older);
		echo $dao->lastStmt->queryString . "\n";
		var_export($dao->lastBind) . "\n";

		echo "\n" . 'Cleanup of long jobs with running time longer than ' . $this->maxExecutionTime . "\n";
		$older = new \DateTime();
		$older->modify('- ' . $this->maxExecutionTime);
		$dao->closeLongtime($older);
		echo $dao->lastStmt->queryString . "\n";
		var_export($dao->lastBind) . "\n";

		return $this;
	}
}
