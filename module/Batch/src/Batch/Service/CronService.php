<?php
namespace Batch\Service;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Batch\CronTask as Cron;
use Rbs\Batch\Job;

/**
 * Helper class to instanciate job of cronTask to run and run job.
 *
 */
class CronService
{

	/**
	 * 
	 * @param Cron $cron
	 * @throws \Exception
	 * @return \Batch\Service\CronService
	 */
	public function run(Cron $cron)
	{
		$cron->service = $this;
		$factory = DaoFactory::get();
		$jobDao = $factory->getDao(Job::$classId);

		$job = $cron->newJob();
		$c = $job->getCallback();

		$callbackClass = $c[0];
		$callbackMethod = $c[1];
		$params = $cron->getCallbackParams();
		$job->setCallbackParams($params);

		if ( !$callbackClass || !$callbackMethod ) {
			throw new \Exception('Callback class or method is not define. You must edit this task and set callback');
		}
		if ( !is_callable(array(
			$callbackClass,
			$callbackMethod
		)) ) {
			throw new \Exception("$callbackClass::$callbackMethod class is not callable");
		}

		$job->start()->setStatus(Job::STATE_RUNNING);
		$jobDao->save($job);

		ob_start();
		$longDateFormat = \Ranchbe::get()->getConfig()['view.longdate.format'];
		echo date($longDateFormat) . "\n";
		$callback = new $callbackClass($params, $job);
		$callback->$callbackMethod();
		$log = ob_get_contents();
		ob_end_clean();

		$job->setLog($log);
		$job->end()->setStatus(Job::STATE_SUCCESS);
		$jobDao->save($job);

		$cron->job = $job;
		$cron->setLastExec(new \DateTime());
		return $this;
	}

	/**
	 * 
	 * @param string $name
	 */
	public function cronFactory($name)
	{
		/* init some helpers */
		$factory = DaoFactory::get();
		/** @var \Rbs\Batch\CronTaskDao $dao */
		$dao = $factory->getDao(Cron::$classId);

		$cron = new Cron();
		$cron->service = $this;
		$cron->dao = $dao;
		$dao->loadFromName($cron, $name);
		return $cron;
	}
}
