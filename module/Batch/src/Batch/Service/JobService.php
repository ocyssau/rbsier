<?php
namespace Batch\Service;

use Rbs\Batch\Job;

/**
 *
 *
 */
class JobService
{

	/**
	 * 
	 * @param Job $job
	 * @throws \RuntimeException
	 * @throws \Exception
	 * @return \Batch\Service\JobService
	 */
	public function run(Job $job)
	{
		if ( !isset($job->dao) ) {
			throw new \RuntimeException('dao must be set to $job before call JobService::run() method');
		}
		$jobDao = $job->dao;

		$c = $job->getCallback();
		$callbackClass = $c[0];
		$callbackMethod = $c[1];
		$callbackParams = $c[2];

		if ( !$callbackClass || !$callbackMethod ) {
			$msg = 'Callback class or method is not define. You must edit this task and set callback';
			$job->setStatus(Job::STATE_FAILED)->setLog($msg);
			$jobDao->save($job);
			throw new \Exception($msg);
		}
		if ( !is_callable($c) ) {
			$msg = "$callbackClass::$callbackMethod class is not callable";
			$job->setStatus(Job::STATE_FAILED)->setLog($msg);
			$jobDao->save($job);
			throw new \Exception($msg);
		}

		$job->start()->setStatus(Job::STATE_RUNNING);
		$jobDao->save($job);

		ob_start();
		$longDateFormat = \Ranchbe::get()->getConfig()['view.longdate.format'];
		echo date($longDateFormat) . "\n";
		$callback = new $callbackClass($callbackParams, $job);
		$callback->$callbackMethod($callbackParams);
		$log = ob_get_contents();
		ob_end_clean();

		$job->setLog($log);
		$job->end()->setStatus(Job::STATE_SUCCESS);
		$jobDao->save($job);

		return $this;
	}
}
