<?php
return array(
	'router' => array(
		'routes' => array(
			'change' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/change',
					'defaults' => array(
						'__NAMESPACE__' => 'Change\Controller',
						'controller' => 'Index',
						'action' => 'index'
					)
				)
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Change\Controller\Index' => 'Change\Controller\IndexController',

		)
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Change' => __DIR__ . '/../view'
		)
	),
	'menu' => [
		'admin' => [
			'change' => [
				'name' => 'change',
				'route' => [
					'params' => [
						'action' => 'index'
					],
					'options' => [
						'name' => 'change'
					]
				],
				'class' => '%anchor%',
				'label' => 'Change'
			]
		]
	]
);
