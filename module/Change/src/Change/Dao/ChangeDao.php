<?php
namespace Change\Dao;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `changes` (
 `id` INT(11) NOT NULL,
 `uid` VARCHAR(32) NOT NULL,
 `cid` VARCHAR(16) NOT NULL DEFAULT 'change47g548rh',
 `dn` VARCHAR(256) NULL,
 `number` VARCHAR(256) NOT NULL,
 `name` VARCHAR(256) NOT NULL,
 `label` VARCHAR(16) DEFAULT NULL,
 `designation` TEXT DEFAULT NULL,
 `reason_of_change` TEXT DEFAULT NULL,
 `parent_id` INT(11) NULL,
 `parent_uid` VARCHAR(32) NULL,
 `life_stage` VARCHAR(64) DEFAULT NULL,
 `created` DATETIME DEFAULT NULL,
 `create_by_id` INT(11) DEFAULT NULL,
 `create_by_uid` VARCHAR(64) DEFAULT NULL,
 `closed` DATETIME DEFAULT NULL,
 `close_by_id` INT(11) DEFAULT NULL,
 `close_by_uid` VARCHAR(64) DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `UK_changes_uid` (`uid`),
 UNIQUE KEY `UK_changes_number` (`number`),
 UNIQUE KEY `UK_changes_dn` (`dn`),
 KEY `K_changes_name` (`name`),
 FULLTEXT `FT_changes_reason_of_change` (`reason_of_change`),
 FULLTEXT `FT_changes_designation` (`designation`),
 KEY `K_changes_parent_id` (`parent_id`),
 KEY `K_changes_parent_uid` (`parent_uid`),
 KEY `K_changes_life_stage` (`life_stage`),
 KEY `K_changes_created` (`created`),
 KEY `K_changes_create_by_id` (`create_by_id`),
 KEY `K_changes_create_by_uid` (`create_by_uid`),
 KEY `K_changes_closed` (`closed`),
 KEY `K_changes_close_by_id` (`close_by_id`),
 KEY `K_changes_close_by_uid` (`close_by_uid`)
 ) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>> 
 << */

/** SQL_ALTER>>
 << */

/** SQL_FKEY>>
 << */

/** SQL_TRIGGER>>
 << */

/** SQL_VIEW>>
 << */

/** SQL_DROP>>
 DROP TABLE `changes`;
 << */

/**
 *
 */
class ChangeDao extends DaoSier
{

	/** @var string */
	public static $table = 'changes';

	/** @var string */
	public static $vtable = 'changes';

	/** @var string */
	public static $sequenceName = 'org_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'dn' => 'dn',
		'number' => 'number',
		'name' => 'name',
		'label' => 'label',
		'designation' => 'description',
		'reason_of_change' => 'reasonOfChange',
		'life_stage' => 'lifeStage',
		'parent_id' => 'parentId',
		'parent_uid' => 'parentUid',
		'created' => 'created',
		'create_by_id' => 'createById',
		'create_by_uid' => 'createByUid',
		'closed' => 'closed',
		'close_by_id' => 'closeById',
		'close_by_uid' => 'closeByUid'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created' => 'datetime',
		'closed' => 'datetime'
	);
}
