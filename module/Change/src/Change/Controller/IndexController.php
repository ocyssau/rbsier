<?php
namespace Change\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Change\Model\Change;

/**
 */
class IndexController extends AbstractController
{

	/** @var string */
	public $pageId = 'change_manage';

	/** @var string */
	public $defaultSuccessForward = 'change/index/display';

	/** @var string */
	public $defaultFailedForward = 'change/index/display';

	/**
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('change');
		$this->layout()->tabs = $tabs;

		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
	}

	/**
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$request = $this->getRequest();

		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$view = $this->view;
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Init some helpers*/
		$factory = DaoFactory::get();
		$trans = $factory->getDao(Change::$classId)->getTranslator();

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Change\Form\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'changefilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Change::$classId);
		$select = $trans->getSelectAsApp();
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->orderby = 'name';
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Change Manager';
		return $view;
	}

	/**
	 *
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$change = Change::init('Change');
		$factory = DaoFactory::get();
		$change->dao = $factory->getDao(Change::$classId);

		/**/
		$form = new \Change\Form\EditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($change);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$change->dao->save($change);
				return $this->successForward();
			}
		}

		/* */
		$view->pageTitle = 'Create Change';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', []);
			$id = $ids[0];
			$validate = false;
		}
		if ( $request->isPost() ) {
			$id = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$change = new Change();
		$factory = DaoFactory::get();
		$change->dao = $factory->getDao(Change::$classId);
		$change->dao->loadFromId($change, $id);

		$form = new \Change\Form\EditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($change);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$change->dao->save($change);
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Change ' . $change->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', array());
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Change::$classId);

		foreach( $ids as $id ) {
			try {
				$dao->deleteFromId($id);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $id));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Throwable $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}
} /* End of class */
