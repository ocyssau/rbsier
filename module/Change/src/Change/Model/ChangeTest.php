<?php
//%LICENCE_HEADER%
namespace Change\Model;

use Application\Console\Output as Cli;
//use Application\Model\DataTestFactory as DataFactory;
use Rbs\Space\Factory as DaoFactory;

/**
 * Writer for Powerpoint files
 * 
 * @see \Application\Model\AbstractTest
 */
class ChangeTest extends \Application\Model\AbstractTest
{

	/**
	 * 
	 */
	public function runTest()
	{
		$factory = DaoFactory::get();
		$change = Change::init('my change')->setNumber(uniqid())->setReasonOfChange('For test ony');
		$change->lifeStage = 'init';
		$change->description = 'Change for test';
		Cli::green(var_export($change, true));
		$dao = $factory->getDao($change->cid);
		$dao->save($change);

		$id = $change->getId();
		assert($change->getId() > 0);
		Cli::green(var_export($change->getId(), true));

		$change = null;
		$change = new Change();
		$dao->loadFromId($change, $id);
		Cli::yellow(var_export($change->getId(), true));
		Cli::blue(var_export($change->getArrayCopy(), true));
	}
} /* End of class*/
