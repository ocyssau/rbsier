<?php
namespace Change\Model;

use Rbplm\Org\Unit as OrgUnit;

/**
 *
 * @author olivier
 *
 */
class Change extends OrgUnit
{

	/**
	 *
	 */
	use \Rbplm\Owned, \Rbplm\LifeControl;

	/**
	 * @var string
	 */
	public static $classId = 'change47g548rh';

	/** @var string */
	protected $comment;

	/** @var string */
	protected $reasonOfChange;

	/** @var \Rbplm\Org\Unit */
	protected $container;

	/** @var string */
	protected $number;

	/** @var int */
	public $containerId;

	/** @var string */
	public $containerUid;

	/**
	 *
	 * @param array $properties
	 */
	public function __construct($name = null)
	{
		$this->cid = static::$classId;
		$this->setName($name);
	}

	/**
	 * Init properties.
	 *
	 * @return Change
	 */
	public static function init($name = null, $parent = null)
	{
		/* @var Change $obj */
		$class = get_called_class();
		$obj = new $class($name);
		$obj->newUid();
		self::lifeinit($obj);
		self::ownerinit($obj);
		return $obj;
	}

	/**
	 * @return Change
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		$this->ownedHydrate($properties);
		$this->lifeControlHydrate($properties);
		(isset($properties['reasonOfChange'])) ? $this->reasonOfChange = $properties['reasonOfChange'] : null;
		(isset($properties['comment'])) ? $this->comment = $properties['comment'] : null;
		(isset($properties['number'])) ? $this->number = $properties['number'] : null;

		/* Enable null values : */
		(array_key_exists('containerId', $properties)) ? $this->containerId = $properties['containerId'] : null;
		(array_key_exists('containerUid', $properties)) ? $this->containerUid = $properties['containerUid'] : null;

		return $this;
	}

	/**
	 *
	 * @param string $number
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 *
	 * @param \Rbplm\Org\Unit $container
	 * @return Change
	 */
	public function setContainer(\Rbplm\Org\Unit $container)
	{
		$this->container = $container;
		$this->containerId = $container->getId();
		$this->containerUid = $container->getUid();
		return $this;
	}

	/**
	 *
	 * @return \Rbplm\Org\Unit
	 */
	public function getContainer($asId = false)
	{
		if ( $asId == true ) {
			return $this->containerId;
		}
		return $this->container;
	}

	/**
	 *
	 * @param string $comment
	 * @return Change
	 */
	public function setComment($comment)
	{
		$this->comment = trim($comment);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 *
	 * @param string $string
	 * @return Change
	 */
	public function setReasonOfChange($string)
	{
		$this->reasonOfChange = trim($string);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getReasonOfChange()
	{
		return $this->reasonOfChange;
	}
}
