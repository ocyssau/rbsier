<?php
namespace Change\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class EditForm extends Form implements InputFilterProviderInterface
{

	public $template;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('changeEdit');

		$this->template = 'change/editform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		// FIRST NAME
		$this->add(array(
			'name' => 'firstname',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('First Name')
			)
		));

		// LAST NAME
		$this->add(array(
			'name' => 'lastname',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('Last Name')
			)
		));

		// MAIL
		$this->add(array(
			'name' => 'mail',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('Mail')
			)
		));

		// ADRESS
		$this->add(array(
			'name' => 'adress',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('Adress')
			)
		));
		$this->add(array(
			'name' => 'city',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('City')
			)
		));
		$this->add(array(
			'name' => 'zipcode',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('Zipcode')
			)
		));
		$this->add(array(
			'name' => 'phone',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('Phone')
			)
		));
		$this->add(array(
			'name' => 'cellphone',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('Cell Phone')
			)
		));
		$this->add(array(
			'name' => 'website',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('Web Site')
			)
		));
		$this->add(array(
			'name' => 'activity',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('Activity')
			)
		));
		$this->add(array(
			'name' => 'company',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => tra('Company')
			)
		));

		/* Type */
		$selectSet = array(
			null => '',
			'customer' => tra('Customer'),
			'supplier' => tra('Supplier'),
			'staff' => tra('Staff')
		);
		$this->add(array(
			'name' => 'type',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control icon-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Type'),
				'value_options' => $selectSet,
				'multiple' => false
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'firstname' => array(
				'required' => false
			),
			'lastname' => array(
				'required' => true
			),
			'mail' => array(
				'required' => false
			),
			'website' => array(
				'required' => false
			),
			'adress' => array(
				'required' => false
			),
			'city' => array(
				'required' => false
			),
			'zipcode' => array(
				'required' => false
			),
			'phone' => array(
				'required' => false
			),
			'cellphone' => array(
				'required' => false
			),
			'activity' => array(
				'required' => false
			),
			'company' => array(
				'required' => false
			),
			'type' => array(
				'required' => false
			)
		);
	}
} /* End of class */
