<?php
/**
 *
 */
return array(
	'router' => array(
		'routes' => array(
			'viewer-threed-fromfile' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/viewer/threed/fromfile[/:file]',
					'defaults' => array(
						'controller' => 'Viewer\Controller\Threed',
						'action' => 'fromfile'
					)
				)
			),
			'viewer-getfile' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/viewer/get[/:file]',
					'defaults' => array(
						'controller' => 'Viewer\Controller\Threed',
						'action' => 'getfile'
					)
				)
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Viewer\Controller\Threed' => 'Viewer\Controller\ThreedController'
		)
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'viewer' => __DIR__ . '/../view'
		)
	)
);
