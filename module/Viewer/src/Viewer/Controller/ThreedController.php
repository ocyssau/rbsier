<?php
namespace Viewer\Controller;

//use Zend\Mvc\Controller\AbstractActionController;
use Application\Controller\AbstractController;

//use Application\View\ViewModel;

/**
 *
 * @author olivier
 *
 */
class ThreedController extends AbstractController
{

	/**
	 *
	 * @var string $pageId
	 */
	public $pageId = 'viewer_threed';

	/**
	 *
	 * @var string $defaultSuccessForward
	 */
	public $defaultSuccessForward = 'viewer/threed/index';

	/**
	 *
	 * @var string $defaultFailedForward
	 */
	public $defaultFailedForward = 'viewer/threed/index';

	/**
	 *
	 * @return \Application\View\ViewModel|string
	 */
	public function getfileAction()
	{
		$filename = $this->params()->fromRoute('file', null);
		if ( !$filename ) return [];
		$path = \Ranchbe::get()->getConfig('viewer.reposit.path');
		$file = $path . '/' . $filename;

		if ( !is_file($file) ) {
			header("HTTP/1.0 404 Not Found");
			die('Not found');
		}

		header("Content-disposition: attachment; filename=\"$filename\"");
		header("Content-Type: application/sla");
		header("Content-Transfer-Encoding: \"$filename\"\n"); // Surtout ne pas enlever le \n
		header("Content-Length: " . filesize($file));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		readfile($file);
		die();
	}

	/**
	 *
	 * @return \Application\View\ViewModel|string
	 */
	public function fromfileAction()
	{
		//$view = new \Zend\View\Model\ViewModel();
		$view = new \Application\View\ViewModel(\Ranchbe::get());
		$view->setTemplate('viewer/threed/babylonjs');

		$filename = basename($this->params()->fromRoute('file', null));
		if ( !$filename ) return $view;

		$publicUrl = $this->url()->fromRoute('viewer-getfile');

		$view->filename = $filename;
		$view->publicUrl = $publicUrl;
		$view->format = 'stl';

		/* */
		return $view;
	}
}


