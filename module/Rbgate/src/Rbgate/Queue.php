<?php
namespace Rbgate;

use Rbplm\Dao\Connexion;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `rbgate_queue` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `file` VARCHAR(512) NOT NULL,
 `updated` DATETIME NULL,
 `owner_uid` VARCHAR(512) NULL,
 PRIMARY KEY (`id`),
 UNIQ `UK_rbgate_queue_1` (`file`),
 KEY `UK_rbgate_queue_2` (`updated`),
 KEY `UK_rbgate_queue_3` (`owner_uid`)
 ) ENGINE=MyIsam;
 << */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/** SQL_DROP>>
 DROP TABLE `rbgate_queue`;
 << */

/**
 * Alternative to Ds\Queue available in php7
 * @author ocyssau
 *
 */
class Queue
{

	/**
	 * Date format use by db
	 *
	 * @var string
	 */
	const DATE_FORMAT = 'Y-m-d H:i:s';

	/**
	 *
	 * @var string
	 */
	public static $table = 'rbgate_queue';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'file' => 'file',
		'updated' => 'updated',
		'owner_uid' => 'ownerUid'
	);

	/**
	 * 
	 * @var array
	 */
	protected $queue;

	/**
	 */
	public function __construct()
	{
		$this->queue = array();
		$this->connexion = Connexion::get();
		$this->_table = self::$table;
		$this->metaModel = self::$sysToApp;
		$this->limit = 10000;
	}

	/**
	 * 
	 * @return \Rbgate\Queue
	 */
	public function load()
	{
		$stmt = $this->getLoadStmt();
		$stmt->execute();
		$this->queue = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $this;
	}

	/**
	 * 
	 * @param string $value1
	 * @return \Rbgate\Queue
	 */
	public function push($value1)
	{
		$stmt = $this->getInsertStmt();
		$now = new \DateTime();

		$bind = array(
			':file' => $value1,
			':updated' => $now->format(self::DATE_FORMAT),
			':ownerUid' => ''
		);
		$stmt->execute($bind);

		array_push($this->queue, array(
			'id' => $this->connexion->lastInsertId($this->_table),
			'file' => $value1,
			'updated' => $now,
			'ownerUid' => ''
		));

		return $this;
	}

	/**
	 * 
	 * @return mixed
	 */
	public function pop()
	{
		$stmt = $this->getDeleteStmt();
		$current = array_pop($this->queue);

		$bind = array(
			':id' => $current['id']
		);
		$stmt->execute($bind);

		return $current;
	}

	/**
	 * 
	 * @return array
	 */
	public function toArray()
	{
		return $this->queue;
	}

	/**
	 * 
	 * @return mixed
	 */
	public function peek()
	{
		return current($this->queue);
	}

	/**
	 * 
	 * @return number
	 */
	public function count()
	{
		return count($this->queue);
	}

	/**
	 * 
	 * @return \Rbgate\Queue
	 */
	public function clear()
	{
		$this->queue = array();
		$this->getClearStmt()->execute();
		return $this;
	}

	/**
	 * @return \PDOStatement
	 */
	protected function getInsertStmt()
	{
		if ( !$this->insertStmt ) {
			$connexion = $this->connexion;
			$table = $this->_table;
			$sysToApp = $this->metaModel;

			foreach( $sysToApp as $asSys => $asApp ) {
				$sysSet[] = '`' . $asSys . '`';
				$appSet[] = ':' . $asApp;
			}

			$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
			$this->insertStmt = $connexion->prepare($sql);
		}

		return $this->insertStmt;
	}

	/**
	 *
	 * @return \PDOStatement
	 */
	protected function getClearStmt()
	{
		if ( !$this->clearStmt ) {
			$connexion = $this->connexion;
			$table = $this->_table;
			$limit = $this->limit;

			$sql = "DELETE FROM $table LIMIT $limit";
			$this->clearStmt = $connexion->prepare($sql);
		}

		return $this->clearStmt;
	}

	/**
	 *
	 */
	protected function getDeleteStmt()
	{
		if ( !$this->deleteStmt ) {
			$connexion = $this->connexion;
			$table = $this->_table;

			$sql = "DELETE FROM $table WHERE id=:id";
			$this->deleteStmt = $connexion->prepare($sql);
		}

		return $this->deleteStmt;
	}

	/**
	 *
	 */
	protected function getPopStmt()
	{
		if ( !$this->deleteStmt ) {
			$connexion = $this->connexion;
			$table = $this->_table;
			$sql = "DELETE FROM $table WHERE id=(SELECT MAX(id) FROM $table)";
			$this->deleteStmt = $connexion->prepare($sql);
		}

		return $this->deleteStmt;
	}

	/**
	 * 
	 */
	public function getLoadStmt()
	{
		if ( !$this->loadStmt ) {
			$connexion = $this->connexion;
			$table = $this->_table;
			$limit = $this->limit;

			$sysToApp = $this->metaModel;
			$select = array();
			foreach( $sysToApp as $asSys => $asApp ) {
				$select[] = $asSys . ' AS ' . $asApp;
			}
			$selectStr = implode(',', $select);

			$sql = "SELECT $selectStr FROM $table LIMIT $limit";
			$this->loadStmt = $connexion->prepare($sql);
		}

		return $this->loadStmt;
	}
} /* End of class */
