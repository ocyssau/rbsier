<?php
namespace Rbgate\Test\Catia;

use Application\Model\AbstractTest;
use Rbgate\Catia\CheckUuid;

/**
 *
/**
 * How to use :
 * open terminal.
 * 
 * 		cd [pat/to/ranchbe/root/directory]
 * 
 * To run all tests methods of a class
 * 
 * 		php public/index.php test /Rbgate/Test/Catia/CheckUuid all
 * 
 * To run one methods
 * 
 * 		php public/index.php test /Rbgate/Test/Catia/CheckUuid run
 * 
 * To display profiling informations
 * 
 * 		php public/index.php test /Module/Controller/Class run -p --loop 10
 * 
 * where 10 is number of loops run on each methods
 * 
 * @see \Application\Controller\CliTestController
 * 
 */
class CheckUuidTest extends AbstractTest
{

	/**
	 * 
	 */
	public function runTest()
	{
		$filename = '';
		$logger = \Ranchbe::get()->getLogger();
		$checkUuid = new CheckUuid($logger);
		$checkUuid->checkUuid($filename);
	}
}
