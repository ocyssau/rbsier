<?php
namespace Rbgate\Catia;

use Rbplm\Sys\Logger;
use SplFileObject;

/**
 * 
 *
 */
class CheckUuid
{

	/**
	 * 
	 * @var Logger
	 */
	private $logger;

	/**
	 * 
	 */
	public function __construct(Logger $logger)
	{
		$this->logger = $logger;
	}

	/**
	 * 
	 * @param string $filename
	 * @return array Reports in $report array
	 */
	public function checkUuid($filename)
	{
		$catiaFile = new SplFileObject($filename, 'rb');

		$report = [];

		if ( $catiaFile ) {
			$header = $catiaFile->fread(8);

			$this->logger->log('Header: ' . $header);

			$blocA = $this->uint($catiaFile->fread(4));
			$blocB = $this->uint($catiaFile->fread(4));

			$this->logger->log("BlocA = $blocA, BlocB = $blocB, somme = " . $blocA + $blocB . ", file size: ");

			$subrecordMatch = chr(0x44) . chr(0x41) . chr(0x53) . chr(0x53);
			$subrecordMSB = chr(00) . chr(00) . chr(00) . chr(17) . $subrecordMatch;
			$subrecordLSB = chr(17) . chr(00) . chr(00) . chr(00) . $subrecordMatch;

			$num = 0;

			for ($index = 0; $index < 8; $index++) {
				$catiaFile->fseek($index);

				$this->logger->log($index);

				while( !$catiaFile->eof() ) {
					$data = $catiaFile->fread(8);

					$num++;
					if ( $subrecordMSB == $data ) {
						$dataPos[++$num] = $catiaFile->ftell();
						$this->logger->log("MSB # " . $num . " , " . sprintf("%X", $dataPos[$num]) . " , DATA Found " . sprintf("%X", $this->uint($data)) . sprintf("%X", $this->uint($subrecordMatch)));
						$i = 0;
					}

					if ( $subrecordLSB == $data ) {
						$dataPos[++$num] = $catiaFile->ftell();
						$this->logger->log("LSB # " . $num . " , " . sprintf("%X", $dataPos[$num]) . " , DATA Found " . sprintf("%X", $this->uint($data)) . sprintf("%X", $this->uint($subrecordMatch)));
						$i = 0;
					}
				}

				$this->logger->log("End of file " . sprintf("%X", $catiaFile->ftell()));
			}

			for ($index = 1; $index <= $num; $index++) {
				/* Positionnement du pointeur au debut de la zone trouvee */
				$catiaFile->fseek($dataPos[$index] - 8);

				$this->logger->log("DataPos[$index] = " . $dataPos[$index] - 8);

				/* Lecture de 4 strings dont les longeurs sont definies sur 4 octets */
				for ($i = 1; $i <= 4; $i++) {
					$null = $this->string4c($catiaFile, "Loop");
					$this->logger->log("$null ; string4c $i , ftell = " . $catiaFile->ftell() . "<br>");
				}

				$null = $this->bytes($catiaFile, "Short", 2);

				$this->logger->log("$null ; 2 bytes , ftell = " . $catiaFile->ftell());

				/* Lecture de 1 string dont la longeur est definie sur 1 octet */
				$null = $this->stringc($catiaFile, "StoProp");
				$this->logger->log("$null ; stringc , ftell = " . $catiaFile->ftell());

				/* Lecture de 7 bytes */
				$null = $this->bytes($catiaFile, "StoPropVal", 7);
				$this->logger->log("$null ; 7 bytes , ftell = " . $catiaFile->ftell());

				$sum = 0;
				$data = $catiaFile->fread(4);
				for ($id1 = 0; $id1 < 4; $id1++) {
					$sum += ord(substr($data, $id1, 1));
				}

				$this->logger->log("StoPropVal index=" . $index . " , DataPos[index]=" . sprintf("%u", $dataPos[$index] - 8) . " , i=" . $i . " , sum=" . $sum);

				/* ************************************************** */
				if ( $sum == 12 ) {
					$this->bytes($catiaFile, "StoPropVal1", 5);
					#Unicode
					$this->stringc($catiaFile, "Unicode");
					#Unicodeval
					$this->bytes($catiaFile, "Unicodeval", 7); #Catia
					$result = $this->stringc($catiaFile, "Catia");

					$this->logger->log("Result: $result");

					#========================================
					if ( $result == "CATIA" ) {
						$this->bytes($catiaFile, "CatiaVal", 8);
						$report[$result]['InternalName'] = $this->stringc($catiaFile, "Partname");
						$report[$result]['FileName'] = $filename;

						#UUID
						//		  bytes($catiaFile,"UUID",uuid($catiaFile));
						# FINJPL
						//		stringc($catiaFile,"FINJPL");
					}

					#========================================
					else if ( $result == "CATProduct" ) {
						//$this->bytes($catiaFile,"CatiaVal",8);
						$this->stringc($catiaFile, "Catproduct");
						//$this->bytes($catiaFile,"CatproductVal",8);
						#UUID
						#print "UUID1="bytes("UUID",uuid());
						$this->bytes($catiaFile, "UUID", 21);
						$report['UUID'] = $this->bytes($catiaFile, "UUID", 16);
						$report['type'] = $result;

						//$this->bytes($catiaFile,"UUID",uuid($catiaFile));
						# FINJPL
						//$this->stringc($catiaFile,"FINJPL");
					}

					#========================================
					else if ( $result == "CATDrawing" ) {
						$this->bytes($catiaFile, "CatiaVal", 8);
						$this->stringc($catiaFile, "CATDrawing");
						//$this->bytes($catiaFile,"CatproductVal",8);
						$this->bytes($catiaFile, "UUID", 21);
						$report['UUID'] = $this->bytes($catiaFile, "UUID", 16);
						$report['type'] = $result;
						$this->bytes($catiaFile, "UUID", $this->uuid($catiaFile));
						# FINJPL
						//$this->stringc($catiaFile,"FINJPL");
					}

					#========================================
					else if ( $result == "CATPart" ) {
						$this->bytes($catiaFile, "CatiaVal", 8);
						$this->stringc($catiaFile, "CATDrawing");
						#CatproductVal
						//$this->bytes($catiaFile,"CatproductVal",8);
						#UUID
						$this->bytes($catiaFile, "Pre UUID", 21);
						$report['UUID'] = $this->bytes($catiaFile, "UUID", 16);
						$report['type'] = $result;
						//$this->bytes($catiaFile,"After UUID",uuid($catiaFile));
						# FINJPL
						//$this->stringc($catiaFile,"FINJPL");
					}
				}
			}

			$catiaFile->fclose();
		}
		else {
			die("fopen failed for $filename");
		}

		return $report;
	}

	/**
	 * Extraction d'un chaine de caractere sous format :
	 * 4 Octets d'index pour la longueur de la chaine (peut-etre MSB ou LSB).
	 * N Octets pour le contenu de la chaine.
	 * 
	 * @param SplFileObject $catiaFile
	 * @param string $name
	 * @return string
	 */
	private function string4c(SplFileObject $catiaFile, $name)
	{
		$data = $catiaFile->fread(4);
		$lenstr = ord(substr($data, 0, 1)) + ord(substr($data, 3, 1));
		$result = $catiaFile->fread($lenstr);

		$this->logger->log('string4c: ' . "$name, lenstr=$lenstr, str=$result");

		return $result;
	}

	/**
	 * Extraction d'un chaine de caractere sous format :
	 * 1 Octet d'index pour la longueur de la chaine (peut-etre MSB ou LSB).
	 * N Octets pour le contenu de la chaine.
	 * 
	 * @param SplFileObject $catiaFile
	 * @param string $name
	 * @return string
	 * 
	 */
	private function stringc(SplFileObject $catiaFile, $name)
	{
		$lenstr = ord($catiaFile->fread(1));
		$result = $catiaFile->fread($lenstr);

		$this->logger->log('stringc: ' . "$name, lenstr=$lenstr, str=$result");

		return $result;
	}

	/** 
	 * Extraction d'un double mot MSB
	 * 
	 * @param string $dataUint
	 * @return int
	 *
	 */
	private function uint($dataUint)
	{
		$uinteger = ord(substr($dataUint, 0, 1)) * 256 * 256 * 256;
		$uinteger += ord(substr($dataUint, 1, 1)) * 256 * 256;
		$uinteger += ord(substr($dataUint, 2, 1)) * 256;
		$uinteger += ord(substr($dataUint, 3, 1));
		return $uinteger;
	}

	/**
	 * 
	 * @param SplFileObject $catiaFile
	 * @param string $name
	 * @param int $lng
	 * @return string
	 */
	private function bytes(SplFileObject $catiaFile, $name, $lng)
	{
		$str = "";
		for ($id = 0; $id < $lng; $id++) {
			$str .= sprintf("%02x", ord(fread($catiaFile, 1)));
		}
		$this->logger->log("$name bytes=$str");
		return $str;
	}

	/**
	 * 
	 * @param SplFileObject $catiaFile
	 * @return number
	 */
	private function uuid(SplFileObject $catiaFile)
	{
		$l = 0;
		$log = [];

		$lenstr = ord($catiaFile->fread(1));
		$result = $catiaFile->fread($lenstr);

		for ($id = 0; $id < $lenstr; $id++) {
			if ( ord(substr($result, $id, 1)) == 70 ) {
				$log[] = "uuid, lenstr=$lenstr, result=$result";
				$str = '';
				for ($id1 = 0; $id1 < 6; $id1++) {
					$str .= sprintf("%c", ord($catiaFile->fread(1)));
				}
				$log[] = "str=$str";
				if ( $str == "FINJPL" ) {
					$l = $id - 1;
					$log[] = "l=$l";
				}

				$this->logger->log(implode(', ', $log));
			}
		}

		return $l;
	}
}
