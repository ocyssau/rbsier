<?php
//%LICENCE_HEADER%
namespace Rbgate\Cron;

use Rbs\Batch\CallbackInterface;

/**
 * 
 * Reconciliate Product to Document and 
 * populate document properties from Product properties if $this->sychroniseProperties = 'productToDocument'
 * or populate product properties from document properties if $this->sychroniseProperties = 'documentToProduct'
 */
class Reconciliate implements CallbackInterface
{

	/** None synchronization */
	const SYNC_NONE = 0;

	/** Syncronise document from product properties */
	const SYNC_DOCUMENT = 1;

	/** Syncronise product from document properties */
	const SYNC_PRODUCT = 2;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/** @var boolean */
	protected $verbose = true;

	/** @var string */
	protected $syncMode = 1;

	/** @var \Rbs\Pdm\Service\Reconciliate */
	protected $service;

	/**
	 * 
	 * @param array $params
	 * @param \Rbs\Batch\Job $job
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->job = $job;
		$this->service = new \Rbs\Pdm\Service\Reconciliate($params);
		isset($params['syncMode']) ? $this->syncMode = $params['syncMode'] : null;
		isset($params['verbose']) ? $this->verbose = (bool)$params['verbose'] : null;
	}

	/**
	 *
	 */
	public function run()
	{
		$service = $this->service;
		$job = $this->job;
		if ( $job->cron instanceof \Rbs\Batch\CronTask ) {
			$lastExec = $job->cron->getLastExec();
			if ( !$lastExec ) {
				$lastExec = null;
			}
		}

		/* Now run reconciliate */
		echo '#### Reconciliate Products with Documents #' . "\n";
		$stmt = $service->productToDocument();
		print_r($stmt->queryString);
		echo "\n";
		print_r($stmt->bind) . "\n";

		echo '#### Reconciliate Instances with Products #' . "\n";
		$stmt = $service->instanceToProduct();
		print_r($stmt->queryString);
		echo "\n";
		print_r($stmt->bind) . "\n";

		if ( $this->syncMode == 1 ) {
			echo '#### Synchronize documents properties with products properties #' . "\n";
			$stmt = $service->sychroniseDocument();
			print_r($stmt->queryString);
			echo "\n";
			print_r($stmt->bind) . "\n";
		}
		elseif ( $this->syncMode == 2 ) {
			echo '#### Synchronize products properties with documents properties #' . "\n";
			$stmt = $service->sychroniseProduct();
			print_r($stmt->queryString);
			echo "\n";
			print_r($stmt->bind) . "\n";
		}

		return $this;
	}
}
