<?php
//%LICENCE_HEADER%
namespace Rbgate\Cron;

use Rbs\Batch\CallbackInterface;
use Rbplm\Dao\Connexion;
use Rbs\Converter\Factory as ConverterFactory;
use Rbs\Dao\Sier\MetamodelTranslator;

/**
 * Used to be launch from the cronTasks manager or other cli scripts.
 * When run(), get docfiles to convert and call index() method.
 * Run a Converter and run the Importer
 */
class Batch implements CallbackInterface
{

	/** 
	 * Parameters as inputs
	 * 
	 * @var array
	 */
	protected $params;

	/** 
	 * Job dependance
	 * 
	 * @var \Rbs\Batch\Job 
	 */
	protected $job;

	/** 
	 * Path to directory where to put temp files
	 * 
	 * @var string
	 */
	protected $workingDirectory = '/data/rbgate';

	/**
	 * Vault reposit where putted the visualizations files (jpeg, stl...)
	 * 
	 * @var integer
	 */
	protected $repositForVisuId = null;

	/** 
	 * Rbgate server url
	 * 
	 * @var string 
	 */
	protected $serverUrl;

	/** 
	 * Filter used to get docfiles to convert
	 * 
	 * @var string 
	 */
	protected $filter = '1=1';

	/** 
	 * @var string 
	 */
	protected $spacename;

	/** 
	 * @var boolean 
	 */
	protected $verbose = true;

	/** 
	 * If true, delete tempfiles after work
	 * 
	 * @var boolean 
	 */
	protected $cleanupTempfile = true;

	/** 
	 * Cron task to run for reconciliates product after conversion/import work
	 * 
	 * @var string
	 */
	protected $reconciliateCronName = 'RbgateReconciliate';

	/**
	 * If true, convert only file updated since last run
	 * 
	 * @var boolean
	 */
	protected $onlyUpdatedFromLastRun = true;

	/** 
	 * Max files count to get for conversion
	 * 
	 * @var integer 
	 */
	protected $limit = 1000;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
		$this->connexion = Connexion::get();

		isset($params['workingDirectory']) ? $this->workingDirectory = $params['workingDirectory'] : null;
		isset($params['repositForVisuId']) ? $this->repositForVisuId = (int)$params['repositForVisuId'] : null;
		isset($params['serverUrl']) ? $this->serverUrl = $params['serverUrl'] : null;
		isset($params['filter']) ? $this->filter = $params['filter'] : null;
		isset($params['spacename']) ? $this->spacename = $params['spacename'] : null;
		isset($params['verbose']) ? $this->verbose = (bool)$params['verbose'] : null;
		isset($params['cleanupTempfile']) ? $this->cleanupTempfile = (bool)$params['cleanupTempfile'] : null;
		isset($params['limit']) ? $this->limit = (int)$params['limit'] : null;
		isset($params['reconciliateCronName']) ? $this->reconciliateCronName = $params['reconciliateCronName'] : null;
		isset($params['onlyUpdatedFromLastRun']) ? $this->onlyUpdatedFromLastRun = (bool)$params['onlyUpdatedFromLastRun'] : null;
	}

	/**
	 * 
	 * @param string $filter
	 * @return Batch
	 */
	public function setFilter($filter)
	{
		$this->filter = $filter;
		return $this;
	}

	/**
	 * @return Batch
	 */
	public function run()
	{
		$docfilesToIndexStmt = $this->getDocfilesToConvert($this->spacename, $this->filter);
		echo sprintf('Conversion of %s files', $docfilesToIndexStmt->rowCount()) . "\n\r";

		$this->index($docfilesToIndexStmt, $this->verbose);

		$cronService = $this->job->cron->service;
		$reconciliateCron = $cronService->cronFactory($this->reconciliateCronName);
		$cronService->run($reconciliateCron);
		$reconciliateCron->dao->save($reconciliateCron);

		return $this;
	}

	/**
	 * @param string $spacename
	 * @param string $filter
	 * @param array $bind
	 * 
	 * @throws \Exception
	 * @return \PDOStatement
	 */
	protected function getDocfilesToConvert($spacename, $filter = '1=1', $bind = array())
	{
		$connexion = $this->connexion;

		/* get all docfiles modified since the lastIndexerRun */
		try {
			/* get only file updated since last run */
			if ( $this->onlyUpdatedFromLastRun == true ) {
				$job = $this->job;
				if ( $job->cron instanceof \Rbs\Batch\CronTask ) {
					$lastRuntime = $job->cron->getLastExec();
					if ( $lastRuntime ) {
						$bind[':lastrun'] = MetamodelTranslator::datetimeToSys($lastRuntime);
						$where = 'df.updated > :lastrun';
						$filter = $filter . ' AND ' . $where;
					}
				}
			}

			/* select only free files */
			$where = 'df.acode = 0 AND (df.updated > product.updated OR product.updated IS NULL)';

			if ( $filter ) {
				$where .= ' AND ' . $filter;
			}
			$limit = $this->limit;

			$spacename = strtolower($spacename);
			$table = $spacename . '_doc_files';
			$bind[':spacename'] = $spacename;

			$select = 'df.id, df.name, df.path, df.extension, df.document_id as documentId, df.document_uid as documentUid' . ", '$spacename' as spacename";
			$sql = "SELECT $select FROM $table AS df";
			$sql .= " LEFT OUTER JOIN pdm_product_version as product ON df.root_name = product.number AND spacename=:spacename";
			$sql .= " WHERE $where LIMIT $limit";

			$stmt = $connexion->prepare($sql);
			$stmt->execute($bind);
			return $stmt;
		}
		catch( \Exception $e ) {
			throw $e;
		}
	}

	/**
	 * @param \PDOStatement $pdoStmt
	 * @param boolean $verbose
	 * @return Batch
	 */
	protected function index(\PDOStatement $pdoStmt, $verbose = false)
	{
		$rbgateConnexion = new \Rbgate\RbgateConnexion($this->serverUrl);
		$rbgateConnexion->isAlive();
		$converterFactory = ConverterFactory::get();

		/* insert each file in index */
		while( $fileEntry = $pdoStmt->fetch(\PDO::FETCH_ASSOC) ) {
			$path = $fileEntry['path'];
			$name = $fileEntry['name'];
			$extension = $fileEntry['extension'];
			$fullPath = $path . '/' . $name;

			if ( !is_file($fullPath) ) {
				if ( $verbose ) echo "$fullPath is not found \n";
				continue;
			}

			if ( $verbose ) echo "Conversion of $fullPath \n";

			/* extract content of file */
			/* and write in db indexer table */
			try {
				$converter = $converterFactory->getConverter($extension, 'ranchbe');
				$converter->setOptions([
					'workingDirectory' => $this->workingDirectory
				]);
				$converter->setConnexion($rbgateConnexion);

				$pi = pathinfo($fullPath);
				$tofile = $this->workingDirectory . '/' . $pi['filename'] . '.rbjson';

				echo "Convert to $tofile \n";

				$converter->convert($fullPath, $tofile);

				if ( $converter->getResult()->getErrors() ) {
					foreach( $converter->getResult()->getErrors() as $str ) {
						echo $str . "\n";
					}
				}

				$importer = new \Rbgate\Importer\FromRbjson();
				$importer->setOptions([
					'workingDirectory' => $this->workingDirectory,
					'repositForVisuId' => $this->repositForVisuId
				]);
				$jsonString = file_get_contents($tofile);
				$rbjson = \Rbgate\Rbjson::get($jsonString);
				$result = $importer->import($rbjson);

				/* delete working copy */
				if ( $this->cleanupTempfile ) {
					@unlink($tofile);
				}

				if ( $verbose ) {
					echo "Errors :\n";
					foreach( $result->getFeedbacks() as $name => $data ) {
						echo $name . ' ' . $data . "\n";
					}

					echo "Imported datas :\n";
					foreach( $result->getDatas() as $name => $data ) {
						if ( is_string($data) ) {
							echo $name . ' ' . $data . "\n";
						}
						else {
							echo $name;
						}
					}

					echo "Feedbacks :\n";
					foreach( $result->getFeedbacks() as $name => $data ) {
						echo $name . ' ' . $data . "\n";
					}
				}
			}
			catch( \Rbs\Converter\NotMappedException $e ) {
				echo $e->getMessage() . " - You must edit converter.map Ranchbe configuration key /\n";
				continue;
			}
			catch( \Exception $e ) {
				echo $e->getMessage() . "\n";
				continue;
			}
		}

		$rbgateConnexion->close();

		return $this;
	}
}
