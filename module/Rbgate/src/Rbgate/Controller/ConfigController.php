<?php
namespace Rbgate\Controller;

use Application\Controller\AbstractController;
use Application\Controller\ControllerException;

/**
 *
 *
 */
class ConfigController extends AbstractController
{

	/**
	 * HTTP GET ajax Query
	 * @return \Application\View\ViewModel
	 */
	public function getAction()
	{
		/* Init some helper */
		$view = $this->view;
		$request = $this->getRequest();

		if ( !$request->isGet() ) {
			throw new ControllerException('Accept only HTTP GET request');
		}

		$config = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Config');

		$conf = array(
			'rbgate.working.directory' => $config['rbp']['rbgate.working.directory'],
			'rbgate.server.url' => $config['rbp']['rbgate.server.url'],
			'rbgate.application' => $config['rbp']['rbgate.application']
		);
		$view->config = $conf;
		return $view;
	}
} /* End of class */
