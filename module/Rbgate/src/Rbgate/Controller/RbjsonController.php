<?php
namespace Rbgate\Controller;

use Application\Controller\AbstractController;

/**
 * Read rbjson from input and import in Ranchbe
 *
 */
class RbjsonController extends AbstractController
{

	/** 
	 * AJAX method
	 * 
	 */
	public function saveAction()
	{
		/* Init some helper */
		$view = $this->view;
		$view->setTerminal(true);
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$array = $request->getPost('tree');
		}
		else {
			throw new \BadMethodCallException('Request must be HTTP POST');
		}

		$importer = new \Rbgate\Importer\FromRbjson();
		$rbjson = \Rbgate\Rbjson::get($array, \Rbgate\Rbjson::IN_FORMAT_ARRAY);
		$result = $importer->import($rbjson);

		$view->result = $result;
		return $view;
	}
} /* End of class */
