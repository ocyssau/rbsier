<?php
namespace Rbgate\Controller;

use Application\Controller\AbstractController;

/**
 *
 *
 */
class ClientController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'rbgate_client';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'rbgate/client/index';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'rbgate/client/index';

	/**
	 */
	public function indexAction()
	{
		/* Init some helper */
		$view = $this->view;
		return $view;
	}

	/**
	 */
	public function ping1Action()
	{
		/* Init some helper */
		$view = $this->view;

		$jsonFile1 = 'data/dataSetForTest/rbgatejsonsample.json';
		$importer = new \Rbgate\Importer\FromRbjson();
		$result = $importer->run($jsonFile1);
		$view->result = $result;

		$reconciliate = new \Rbgate\Cron\Reconciliate();
		$reconciliate->productToDocument();
		$reconciliate->instanceToProduct();

		return $view;
	}

	/**
	 */
	public function ping2Action()
	{
		/* Init some helper */
		$view = $this->view;

		//var_dump($rbgateServerUrl, $workingDirectory);die;

		$fromfile = 'data/dataSetForTest/test.doc';
		$tofile = 'data/cache/test.pdf';

		// Crée un objet CURLFile
		$cfile = new \CURLFile($fromfile);

		$ch = curl_init();

		// Assigne les données POST
		$data = array(
			'file' => $cfile,
			'fromfile' => basename($fromfile),
			'tofile' => basename($tofile)
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		/* $output contains the output string */
		$output = curl_exec($ch);

		/* close curl resource to free up system resources */
		curl_close($ch);

		var_dump($output);
		die();
		return $view;
	}
} /* End of class */
