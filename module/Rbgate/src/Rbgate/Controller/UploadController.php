<?php
namespace Rbgate\Controller;

use Application\Controller\AbstractController;
use Application\Controller\ControllerException;

/**
 * Upload json file and visu to Rbgate server engine to prepare import.
 * Ajax only
 *
 */
class UploadController extends AbstractController
{

	/**
	 * HTTP POST ajax Query
	 * 
	 * @throws \Application\Controller\ControllerException
	 * @return \Application\View\ViewModel
	 */
	public function jsonAction()
	{
		/* Init some helper */
		$view = $this->view;
		$request = $this->getRequest();

		if ( !$request->isPost() ) {
			throw new ControllerException('Accept only HTTP POST request');
		}

		$config = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Config');
		$workingDir = $config['rbp']['rbgate.working.directory'];

		if ( !is_dir($workingDir) || !is_writable($workingDir) ) {
			throw new ControllerException(sprintf('Working directory "%s" is not reachable', $workingDir));
		}

		$name = basename($request->getPost('name', null));
		$data = $request->getPost('data', null);

		if ( !$name ) {
			throw new ControllerException(sprintf('%s is not set', '$name'));
		}
		if ( !$data ) {
			throw new ControllerException(sprintf('%s is not set', '$data'));
		}

		$file = $workingDir . '/' . $name;
		file_put_contents($file, $data);

		/* add file to queue */
		$rbGateQueue = new \Rbgate\Queue();
		$rbGateQueue->push($file);

		$view->file = $file;
		return $view;
	}

	/**
	 * HTTP POST ajax Query
	 */
	public function binaryAction()
	{
		/* Init some helper */
		$view = $this->view;
		$request = $this->getRequest();

		if ( !$request->isPost() ) {
			throw new ControllerException('Accept only HTTP POST request');
		}

		$config = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Config');
		$workingDir = $config['rbp']['rbgate.working.directory'];

		if ( !is_dir($workingDir) || !is_writable($workingDir) ) {
			throw new ControllerException(sprintf('Working directory "%s" is not reachable', $workingDir));
		}

		$name = basename($request->getPost('name', null));
		$data = $request->getPost('data', null);

		if ( !$name ) {
			throw new ControllerException(sprintf('%s is not set', '$name'));
		}
		if ( !$data ) {
			throw new ControllerException(sprintf('%s is not set', '$data'));
		}

		$file = $workingDir . '/' . $name;
		file_put_contents($file, $data);

		/* add file to queue */
		$rbGateQueue = new \Rbgate\Queue();
		$rbGateQueue->push($file);

		$view->file = $file;
		return $view;
	}
} /* End of class */
