<?php
namespace Rbgate\Rbjson;

/**
 * 
 * \Rbgate\Rbjson\Product is a class to bind datas from CAD datas to ranchbe.
 *
 * To build a input :
 * 
 * 		$input = new Product($datas, $key)
 * 
 * where $datas is array as:
 * 'fullName'=>string,
 * 'name'=>string,
 * 'path'=>string,
 * 'readonly'=>boolean,
 * 'saved'=>boolean,
 * 'product'=>array(
 * * 'id'=>integer,
 * * 'uid'=>string,
 * * 'name'=>string,
 * * 'number'=>string,
 * * 'nomenclature'=>string,
 * * 'version'=>$this->string,
 * * 'definition'=>string,
 * * 'description'=>string,
 * ),
 * 'instance'=>array(
 * * 'id'=>integer,
 * * 'uid'=>string,
 * * 'number'=>string,
 * * 'name'=>string,
 * * 'number'=>string,
 * * 'nomenclature'=>string,
 * * 'version'=>$this->string,
 * * 'description'=>string,
 * ),
 * 
 * The keys of product and instance must be name of properties as defined in classes \Rbplm\Pdm\Product\Version and \Rbplm\Pdm\Product\Instance
 * and defined too the properties to update. So, if you dont want change a value, simply dont set the property in json.
 * 
 * 
 */
class Product extends AbstractRbjson
{

	/**
	 * @param array $datas
	 */
	function __construct($datas)
	{
		parent::__construct($datas);
		$this->type = self::TYPE_PRODUCT;
	}

	/**
	 *
	 */
	public function getChildren()
	{
		if ( isset($this->datas['children']) ) {
			return $this->datas['children'];
		}
	}
}
