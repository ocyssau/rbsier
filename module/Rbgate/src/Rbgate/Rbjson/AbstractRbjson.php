<?php
namespace Rbgate\Rbjson;

/**
 * The classes Rbjson\* are used to transport result from Converter to Importer.
 * AbstractRbjson is the super class for Rbjson\* classes.
 */
abstract class AbstractRbjson
{

	const TYPE_PART = 'part';

	const TYPE_PRODUCT = 'product';

	const TYPE_DRAWING = 'drawing';

	/**
	 *
	 * @var array
	 */
	protected $datas;

	/**
	 * 
	 * @var string value of one constante TYPE_*
	 */
	protected $type;

	/**
	 * The instance of product to import
	 * 
	 * @var \Rbplm\Pdm\Product\Version
	 */
	protected $rootProduct;

	/**
	 * @param array $datas Datas from converter.
	 */
	public function __construct($datas)
	{
		$this->datas = $datas;
		$this->type = $datas['fromType'];
	}

	/**
	 * Factory method for returned fully init composite object Product\Instance->Product\Version
	 * 
	 * Convert current input element to Product\Instance and fill associated Product\Version
	 *     The returned product is associated to a new instance of Product\Version filled with properties of product.
	 * 
	 * @return \Rbplm\Pdm\Product\Instance
	 */
	public function getRootProduct()
	{
		if ( !$this->rootProduct ) {
			if ( isset($this->datas['properties']['product']) ) {
				$properties = array();
				foreach( $this->datas['properties']['product'] as $n => $v ) {
					$properties[$n] = trim($v);
				}
				$productVersion = \Rbplm\Pdm\Product\Version::init('none');
				$productVersion->populate($properties);
				$this->rootProduct = $productVersion;
			}
			else {
				throw new \Exception('properties.product is not set in input json');
			}
		}

		return $this->rootProduct;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return array
	 */
	public function getFeedback()
	{
		if ( isset($this->datas['feedback']) ) {
			return $this->datas['feedback'];
		}
	}

	/**
	 * @return array
	 */
	public function getErrors()
	{
		if ( isset($this->datas['errors']) ) {
			return $this->datas['errors'];
		}
	}

	/**
	 * @return array
	 */
	public function getProperties()
	{
		return $this->datas['properties'];
	}

	/**
	 * @return string
	 */
	public function getPicture()
	{
		if ( isset($this->datas['picture']) ) {
			return $this->datas['picture'];
		}
	}

	/**
	 * @return string
	 */
	public function getThreedim()
	{
		if ( isset($this->datas['3d']) ) {
			return $this->datas['3d'];
		}
	}

	/**
	 * @return string
	 */
	public function getConvert()
	{
		if ( isset($this->datas['convert']) ) {
			return $this->datas['convert'];
		}
	}

	/**
	 * @return array
	 */
	public function getLinks()
	{
		if ( isset($this->datas['links']) ) {
			return $this->datas['links'];
		}
	}
}
