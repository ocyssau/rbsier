<?php
namespace Rbgate\Rbjson;

/**
 * \Pdm\Input\PdmData\Drawing is a class to bind datas from CAD datas to ranchbe.
 *
 *
 *
 * To build a input :
 * $input = new Drawing($datas, $key)
 * where $datas is array as:
 * 'name'=>string
 * 'path'=>string
 * 'readonly'=>boolean
 * 'saved'=>boolean
 * 'format'=>boolean
 * 'orientation'=>boolean
 */
class Drawing extends AbstractRbjson
{

	/**
	 * @param array $datas
	 */
	function __construct($datas)
	{
		parent::__construct($datas);
		$this->type = self::TYPE_DRAWING;
	}
}
