<?php
namespace Rbgate\Callback;

use Rbplm\Ged\Document;
use Rbplm\Event;

/**
 */
class Catdrawing extends \Rbs\Observers\Callback
{

	/**
	 * @param Event $event
	 * @param Document\Version $reference
	 * @return Catdrawing
	 */
	public function postStore(Event $event, Document\Version $reference)
	{
		$queue = new \Rbgate\Queue();
		$queue->push($reference->getNumber());
		return $this;
	}

	/**
	 * @param Event $event
	 * @param Document\Version $reference
	 * @return Catdrawing
	 */
	public function postUpdate(Event $event, Document\Version $reference)
	{
		$queue = new \Rbgate\Queue();
		$queue->push($reference->getNumber());
		return $this;
	}
}
