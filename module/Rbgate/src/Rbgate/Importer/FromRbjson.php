<?php
namespace Rbgate\Importer;

use Rbplm\Pdm\Product;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Pdm\Service\Result;

/**
 * Read result of Converter, through data in Input\Rbjson\AbstractRbjson classe, and import data in Ranchbe.
 * 
 */
class FromRbjson
{

	/**
	 * 
	 * @var \Rbs\Pdm\Service\Result
	 */
	protected $result;

	/**
	 * @var array
	 */
	protected $options = array(
		'workingDirectory' => '\tmp',
		'repositForVisuId' => null
	);

	/**
	 * 
	 * @var \Rbs\Vault\Vault
	 */
	protected $vault;

	/**
	 *
	 */
	public function __construct(Result $result = null)
	{
		if ( $result ) {
			$this->result = $result;
		}
		else {
			$this->result = new Result();
		}
	}

	/**
	 *
	 * @param array $options
	 * @return FromRbjson
	 */
	public function setOptions($options)
	{
		$this->options = array_merge($this->options, $options);
		return $this;
	}

	/**
	 *
	 */
	public function setResult(Result $result)
	{
		$this->result = $result;
	}

	/**
	 *
	 * @param \Rbgate\Rbjson\AbstractRbjson $rbjson
	 * @return Result
	 */
	public function import(\Rbgate\Rbjson\AbstractRbjson $rbjson)
	{
		$result = $this->result;

		/* Init some helper */
		$factory = DaoFactory::get();
		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$productDao = $factory->getDao(Product\Version::$classId);

		/* Get the root Product\Version */
		$rootProduct = $rbjson->getRootProduct();
		$rootProductId = $rootProduct->getId();
		$rootProductNumber = $rootProduct->getNumber();

		/* if has links and its a product */
		$type = $rbjson->getType();
		if ( $type == $rbjson::TYPE_PRODUCT ) {
			/* instanciate each link as product instance */
			$children = $rbjson->getChildren();
			if ( $children ) {
				foreach( $children as $child ) {
					$childInstance = Product\Instance::init($child['name']);
					$childInstance->populate($child);
					$childInstance->setParent($rootProduct);
				}
			}
		}
		elseif ( $type == $rbjson::TYPE_PART ) {}
		elseif ( $type == $rbjson::TYPE_DRAWING ) {}

		$rbjsonProperties = $rbjson->getProperties();

		/* Try to load the Product\Version from his number */
		try {
			$existingRootProduct = new Product\Version();
			if ( $rootProductId ) {
				$productDao->loadFromId($existingRootProduct, $rootProductId);
			}
			elseif ( $rootProductNumber ) {
				$productDao->loadFromNumber($existingRootProduct, $rootProductNumber);
			}
			else {
				throw new \Rbplm\Dao\NotExistingException('Root product id or number not found');
			}

			/* Update existing product from properties set in json */
			$newProperties = $rootProduct->export(array_keys($rbjsonProperties['product']));
			$existingRootProduct->hydrate($newProperties);
			$productIsExisting = true;

			/* Save Product\Version */
			$productDao->save($existingRootProduct);
			$result->feedback(sprintf('Update product %s', $existingRootProduct->getName()));
			$result->setData('rootProduct', $existingRootProduct);
		}
		/* is not existing */
		catch( \Rbplm\Dao\NotExistingException $e ) {
			$productIsExisting = false;
			try {
				/* create Product\Version */
				$productDao->save($rootProduct);
			}
			catch( \PDOException $e ) {
				$msg = 'unable to save root product. Try to reconciliate instances before save: ';
				$msg .= $e->getMessage();
				throw new \Rbs\Pdm\Service\Reconciliate\Exception($msg);
			}

			$result->feedback(sprintf('Create product %s', $rootProduct->getName()));
			$result->setData('rootProduct', $rootProduct);
		}
		catch( \Exception $e ) {
			$result->error($e->getMessage());
			return $result;
		}

		/* UPDATE CHILDREN INSTANCE */
		if ( $rbjson instanceof \Rbgate\Rbjson\Product ) {
			if ( $productIsExisting ) {
				$children = $rbjson->getChildren();
				if ( $children ) {
					$toSync = array_keys(current($children));
					if ( !$toSync ) {
						$toSync = array(
							'name',
							'description'
						);
					}
					$this->synchroniseChildren($existingRootProduct, $rootProduct, $toSync);
				}
			}
			else {
				try {
					$children = array();
					foreach( $rootProduct->getChildren() as $new ) {
						$instanceDao->save($new);
						$result->feedback(sprintf('Create instance %s', $new->getName()));
						$children[] = $new;
					}
					$result->setData('children', $children);
				}
				catch( \PDOException $e ) {
					throw $e;
				}
			}
		}

		try {
			if ( $productIsExisting ) {
				$rootProduct = $existingRootProduct;
			}
			if ( $jsonInput = $rbjson->getPicture() ) {
				$file = $this->_extractFile($jsonInput);
				$this->_recordFile($file, $rootProduct);
				$result->setData('picture', $file);
			}
			if ( $jsonInput = $rbjson->getThreedim() ) {
				$file = $this->_extractFile($jsonInput);
				$this->_recordFile($file, $rootProduct);
				$result->setData('3d', $file);
			}
			if ( $jsonInput = $rbjson->getConvert() ) {
				$file = $this->_extractFile($jsonInput);
				$this->_recordFile($file, $rootProduct);
				$result->setData('convert', $file);
			}
		}
		catch( \BadMethodCallException $e ) {}

		$result->setData('rbjson', $rbjson);
		return $result;
	}

	/**
	 * $jsonInput is array['file'=>file name, 'data'=>base64 encoded data]
	 * 
	 * 
	 * @param array $jsonInput
	 */
	protected function _extractFile($jsonInput)
	{
		$workingDir = $this->options['workingDirectory'];
		$fileName = trim(basename($jsonInput['file']), '/');
		$file = $workingDir . '/' . $fileName;

		if ( isset($jsonInput['data']) ) {
			$data = base64_decode($jsonInput['data']);
			if ( !is_file($file) ) {
				file_put_contents($file, $data);
			}
		}
		return $file;
	}

	/**
	 * 
	 * @param string $file
	 * @param \Rbplm\Pdm\Product\Version $product
	 */
	protected function _recordFile($file, $product)
	{
		$pi = pathinfo($file);
		$extension = $pi['extension'];

		/**/
		$fsdata = new \Rbplm\Sys\Fsdata($file);
		$record = \Rbplm\Vault\Record::init();

		/* record file in vault with encoded name */
		$passphrase = \Ranchbe::get()->getConfig('visu.passphrase');
		$vaultfile = Product\File::encodename($product->getId(), $extension, $passphrase);
		$this->_getVault()->record($record, $fsdata, $vaultfile);

		/* delete working copy */
		@unlink($file);

		return $record;
	}

	/**
	 * $jsonInput is array['file'=>file name, 'data'=>base64 encoded data]
	 *
	 * @return \Rbs\Vault\Vault
	 */
	protected function _getVault()
	{
		if ( !isset($this->vault) ) {
			/* record in vault */
			$repositId = $this->options['repositForVisuId'];
			if ( !$repositId ) {
				throw new \BadMethodCallException('option "repositForVisuId" is not set, check parameters.');
			}
			$factory = DaoFactory::get();
			$reposit = new \Rbplm\Vault\Reposit();
			$reposit->dao = $factory->getDao(\Rbplm\Vault\Reposit::$classId);
			$reposit->dao->loadFromId($reposit, $repositId);
			$this->vault = new \Rbs\Vault\Vault($reposit);
		}
		return $this->vault;
	}

	/**
	 * Get children of new instance and replace children of oldInstance with.
	 *
	 * Bi-directionnal comparaison between new product and existing
	 * edit, add, delete instances of existing Product\Version
	 *
	 * @param Product\Version $oldProduct
	 * @param Product\Version $newProduct
	 * @param array $propertiesToSynch List of properties name to update
	 */
	protected function synchroniseChildren(Product\Version $oldProduct, Product\Version $newProduct, array $propertiesToSync)
	{
		$result = $this->result;
		$instanceDao = DaoFactory::get()->getDao(Product\Instance::$classId);

		/* Try to load the Product\Version from his number */
		/* load existing children */
		$oldChildrenStmt = $instanceDao->getChildrenFromParentId($oldProduct->getId());
		foreach( $oldChildrenStmt as $entry ) {
			$child = new Product\Instance();
			$child->hydrate($entry);
			$oldProduct->addChild($child);
		}

		$oldChildren = $oldProduct->getChildren();
		/* check new->old */
		foreach( $newProduct->getChildren() as $newChildInstance ) {
			$isExisting = false;

			/* new child is existing in old instance ? */
			foreach( $oldChildren as &$c ) {
				if ( $c->getName() == $newChildInstance->getName() ) {
					/* edit */
					$export = $newChildInstance->export($propertiesToSync);
					$c->populate($export);
					$isExisting = true;
				}
			}

			/* if not existing, add it */
			if ( $isExisting == false ) {
				$oldProduct->addChild($newChildInstance);
			}
		}
		$oldChildren = $oldProduct->getChildren();

		/**
		 * check old->new
		 * if is not existing in new, delete it
		 * else save it
		 */
		foreach( $oldChildren as $old ) {
			$isExisting = false;

			foreach( $newProduct->getChildren() as &$new ) {
				if ( $new->getName() == $old->getName() ) {
					$isExisting = true;
				}
			}

			/* delete if no more existing in new */
			try {
				if ( $isExisting == false ) {
					$instanceDao->deleteFromId($old->getId());
					$result->feedback(sprintf('Delete instance %s', $old->getName()));
				}
				else {
					if ( $old->getId() ) {
						$instanceDao->save($old);
						$result->feedback(sprintf('Update instance %s', $old->getName()));
					}
					else {
						$instanceDao->save($old);
						$result->feedback(sprintf('Create instance %s', $old->getName()));
					}
					$children[] = $old;
				}
			}
			catch( \PDOException $e ) {
				throw $e;
			}
		}

		$result->setData('children', $children);
	}
}
