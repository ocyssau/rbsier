<?php
namespace Rbgate\Logger;

use Rbs\EventDispatcher\Event;

/**
 */
class Log
{

	/**
	 *
	 * @var \Rbs\Space\Factory
	 */
	protected $factory;

	/**
	 *
	 * @var array
	 */
	public $eventsToListen;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory = null)
	{
		$this->factory = $factory;
		$this->eventsToListen = array(
			'create',
			'checkin',
			'copy',
			'delete',
			'newversion'
		);
	}

	/**
	 * @param Event $event
	 * @param \Rbplm\Any $sender
	 * @param string $actionName
	 * @return \Rbplm\Any
	 */
	public function onEvent(Event $event)
	{
		$splitted = explode('.', $event->getName());
		$eventName = $splitted[0];
		$when = $splitted[1];

		/**/
		if ( in_array($eventName, $this->eventsToListen) ) {
			if ( $when == 'pre' ) {
				$funcName = 'on' . ucfirst($eventName);
				return $this->$funcName($event, $event->getEmitter());
			}
		}
	}

	/**
	 * @param Event $event
	 */
	public function onCreate(Event $event)
	{}

	/**
	 * @param Event $event
	 */
	public function onDelete(Event $event)
	{}

	/**
	 * @param Event $event
	 */
	public function onCheckin(Event $event)
	{}

	/**
	 * @param Event $event
	 */
	public function onCopy(Event $event)
	{}

	/**
	 * @param Event $event
	 */
	public function onNewversion(Event $event)
	{}

	/**
	 *
	 * @return Log
	 */
	public function setConverter($conveter)
	{
		$this->converter = $conveter;
		return $this;
	}

	/**
	 *
	 */
	public function getConverter()
	{
		return $this->converter;
	}

	/**
	 *
	 * @param string $file
	 * @return Log
	 * @throws \Exception
	 */
	public function index($file)
	{
		$content = $this->converter->convert($file);
		if ( $content ) {}
		return $this;
	}
} /* End of class */
