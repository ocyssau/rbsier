<?php
namespace Rbgate\Log;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `cadimporter_log` (
 `document_id` int(11) NOT NULL,
 `docfile_id` int(11) NOT NULL,
 `spacename` VARCHAR(32) NOT NULL,
 `updated` DATETIME NULL,
 PRIMARY KEY (`docfile_id`, `spacename`),
 UNIQUE `UK_cadimporter_log_1` (`document_id`),
 KEY `UK_cadimporter_log_2` (`spacename`)
 ) ENGINE=InnoDb;
 << */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/** SQL_DROP>>
 DROP TABLE `cadimporter_log`;
 << */

/**
 * 
 *
 */
class LogDao extends \Rbs\Dao\Sier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'cadimporter_log';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'cadimporter_log';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'document_id' => 'documentId',
		'docfile_id' => 'docfileId',
		'spacename' => 'spacename',
		'updated' => 'status'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'updated' => 'datetime'
	);
}
