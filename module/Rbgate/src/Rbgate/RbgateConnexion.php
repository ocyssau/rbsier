<?php
namespace Rbgate;

/**
 * 
 *
 */
class RbgateConnexion
{

	/**
	 * Curl resource handler
	 */
	protected $curl;

	/**
	 * @param string
	 */
	protected $rbgateServerUrl;

	/**
	 * 
	 * @param string $rbgateServerUrl
	 */
	public function __construct($rbgateServerUrl)
	{
		/* Init Curl */
		$curl = curl_init($rbgateServerUrl);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$this->serverUrl = $rbgateServerUrl;
		$this->curl = $curl;
	}

	/**
	 * Test if connexion to rbgate server is alive
	 * 
	 * @throws \RuntimeException
	 * @return boolean
	 */
	public function isAlive()
	{
		curl_exec($this->curl);
		//$code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
		$curlError = curl_error($this->curl);
		if ( $curlError ) {
			throw new \RuntimeException($curlError);
		}
		return true;
	}

	/**
	 *
	 * @return
	 */
	public function getHandler()
	{
		return $this->curl;
	}

	/**
	 * Close curl resource to free up system resources
	 * @return RbgateConnexion
	 */
	public function close()
	{
		curl_close($this->curl);
		$this->curl = null;
		return $this;
	}

	/**
	 * Set params to send to server
	 *
	 * @param array $params
	 * @return RbgateConnexion
	 * @throws \RuntimeException
	 */
	public function exec($postParams)
	{
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $postParams);

		$curlResponse = curl_exec($this->curl);
		$curlError = curl_error($this->curl);

		if ( $curlError ) {
			throw new \RuntimeException($curlError);
		}

		return $curlResponse;
	}
}
