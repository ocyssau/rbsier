<?php
namespace Rbgate\Converter;

use Rbs\Converter\ConverterInterface;
use Rbs\Converter\Result;
use Rbgate\RbgateConnexion;

/**
 * Call rbgateServer (through \Rbgate\RbgateConnexion) to convert catia(c) datas to json to be imported in Ranchbe.
 * The result of conversion is put in Rbs\Converter\Result object.
 * To get result use CatiaToRanchbe::getResult().
 * "data" data is set in Result with content of the respons. This is too put in "tofile" file.
 * "tofile" data is set in Result with name of result file.
 *
 */
class CatiaToRanchbe implements ConverterInterface
{

	/**
	 * Data to convert
	 *
	 * @var string
	 */
	protected $data;

	/**
	 * Resulted datas
	 *
	 * @var Result
	 */
	protected $result;

	/**
	 * @var RbgateConnexion
	 */
	protected $connexion;

	/**
	 * @var array
	 */
	protected $options;

	/**
	 */
	public function __construct()
	{}

	/**
	 * 
	 * @param array $options
	 * @return CatiaToRanchbe
	 */
	public function setOptions(array $options)
	{
		$this->options = $options;
		return $this;
	}

	/**
	 *
	 * @param RbgateConnexion $rbgateConnexion
	 * @return CatiaToRanchbe
	 */
	public function setConnexion($rbgateConnexion)
	{
		$this->connexion = $rbgateConnexion;
		return $this;
	}

	/**
	 *
	 * @return Result
	 */
	public function getResult()
	{
		if ( !isset($this->result) ) {
			$this->result = new Result();
		}
		return $this->result;
	}

	/**
	 * if $tofile is null, the outoput file is ignored
	 * resulted data is always put in "data" key of Result::data
	 *
	 * @see \Rbs\Converter\ConverterInterface::convert()
	 */
	public function convert($fromfile, $tofile)
	{
		$this->result = null;
		$result = $this->getResult();

		/* Crée un objet CURLFile */
		$cfile = new \CURLFile($fromfile);

		$pi = pathinfo($fromfile);
		$fromtype = trim(strtolower($pi['extension']), '.');

		/* Assigne les données POST */
		$postParams = array(
			'file' => $cfile,
			'fromfile' => basename($fromfile),
			'tofile' => basename($tofile),
			'fromtype' => $fromtype,
			'totype' => 'ranchbe'
		);

		/* POST download file */
		try {
			$curlResponse = $this->connexion->exec($postParams);
			$result->setData('data', $curlResponse);
		}
		catch( \Exception $e ) {
			$result->error($e->getMessage());
			return $this;
		}

		/* put file data in file */
		if ( $tofile ) {
			file_put_contents($tofile, $curlResponse);
		}

		$result->setData('tofile', $tofile);
		return $this;
	}
}


