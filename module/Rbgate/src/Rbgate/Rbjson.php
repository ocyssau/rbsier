<?php
namespace Rbgate;

/**
 * 
 * Factory for Rbgate\Input\Rbjson\* classes
 */
class Rbjson
{

	const IN_FORMAT_JSON = 'json';

	const IN_FORMAT_ARRAY = 'array';

	/**
	 *
	 * @param string|array $input,
	 *        	JSON encoded string or array, must br in accordance with $format parameter
	 * @param string $format,
	 *        	self::IN_FORMAT_*
	 *        
	 * @return Rbjson\AbstractRbjson
	 * @throws \Exception
	 */
	public static function get($input, $format = 'json')
	{
		if ( $format == self::IN_FORMAT_JSON ) {
			$data = json_decode($input, true);
		}
		elseif ( $format == self::IN_FORMAT_ARRAY ) {
			$data = $input;
		}

		if ( !$data || count($data) == 0 ) {
			throw new \Exception(sprintf('input json %s is invalid', $data));
		}

		switch ($data['fromType']) {
			case 'product':
			case 'catproduct':
				return new Rbjson\Product($data);
				break;
			case 'part':
			case 'catpart':
				return new Rbjson\Part($data);
				break;
			case 'drawing':
			case 'catdrawing':
				return new Rbjson\Drawing($data);
				break;
			default:
				throw new \Exception(sprintf('fromType %s is unknow', $data['fromType']));
				break;
		}
	}
}
