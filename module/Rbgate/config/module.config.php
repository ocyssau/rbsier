<?php
return array(
	'router' => array(
		'routes' => array(
			'rbgate-rbjson' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/rbgate/json/:action',
					'defaults' => array(
						'controller' => 'Rbgate\Controller\Rbjson'
					)
				)
			),
			'rbgate-client' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/rbgate/client[/:action]',
					'defaults' => array(
						'controller' => 'Rbgate\Controller\Client',
						'action' => 'index'
					)
				)
			),
			'rbgate-upload' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/rbgate/upload/:action',
					'defaults' => array(
						'controller' => 'Rbgate\Controller\Upload'
					)
				)
			),
			'rbgate-config' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/rbgate/config/:action',
					'defaults' => array(
						'controller' => 'Rbgate\Controller\Config'
					)
				)
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Rbgate\Controller\Rbjson' => 'Rbgate\Controller\RbjsonController',
			'Rbgate\Controller\Client' => 'Rbgate\Controller\ClientController',
			'Rbgate\Controller\Upload' => 'Rbgate\Controller\UploadController',
			'Rbgate\Controller\Config' => 'Rbgate\Controller\ConfigController'
		)
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Rbgate' => __DIR__ . '/../view'
		)
	)
);
