<?php
return array(
	'router' => array(
		'routes' => array(
			'dav-wildspace' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/dav/wildspace[/][:file]',
					'defaults' => array(
						'__NAMESPACE__' => 'Dav\Controller',
						'controller' => 'Wildspace',
						'action' => 'index'
					)
				),
			),
			'dav-vault' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/dav/vault[/:file]',
					'defaults' => array(
						'__NAMESPACE__' => 'Dav\Controller',
						'controller' => 'Vault',
						'action' => 'index'
					)
				),
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Dav\Controller\Wildspace' => 'Dav\Controller\WildspaceController',
			'Dav\Controller\Vault' => 'Dav\Controller\VaultController',

		)
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Dav' => __DIR__ . '/../view'
		)
	),
);
