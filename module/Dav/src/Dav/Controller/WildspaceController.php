<?php
namespace Dav\Controller;

use Application\Controller\AbstractController;
use Sabre\DAV;
use Rbplm\People;
use Rbplm\People\User\Wildspace;

/**
 *
 * @author olivier
 *
 */
class WildspaceController extends AbstractController
{

	/** @var string */
	public $pageId = 'dav_wildspace';

	/** @var string */
	public $defaultSuccessForward = 'dav/wildspace';

	/** @var string */
	public $defaultFailedForward = 'dav/wildspace';

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
		return $this;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax($view = null, $errorStack = null)
	{
		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$user = People\CurrentUser::get();
		$wildspace = new Wildspace($user);
		$path = $wildspace->getPath();

		/* Now we're creating a whole bunch of objects */
		$rootDirectory = new DAV\FS\Directory($path);

		/* The server object is responsible for making sense out of the WebDAV protocol */
		$server = new DAV\Server($rootDirectory);

		/* This ensures that we get a pretty index in the browser, but it is optional. */
		$server->addPlugin(new DAV\Browser\Plugin());

		/* If your server is not on your webroot, make sure the following line has the correct information */
		$server->setBaseUri('/dav/wildspace');

		/* All we need to do now, is to fire up the server */
		$server->exec();
		die();
	}
} /* End of class */
