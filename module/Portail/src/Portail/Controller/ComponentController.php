<?php
namespace Portail\Controller;

use Application\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Rbs\Space\Factory as DaoFactory;
use Portail\Model\Component;
use Rbplm\People;

/**
 * 
 *
 */
class ComponentController extends AbstractController
{

	/** @var integer */
	public $pageId = 'portail_component';

	/** @var string */
	public $ifSuccessForward = 'portail';

	/** @var string */
	public $ifFailedForward = 'portail';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		/* Record url for page and Active the tab */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		return $this->view;
	}

	/**
	 * Ajax method
	 * Return a json array as:
	 *
	 * {
	 * 'id':'',
	 * 'updated':'',
	 * 'updatebyid':'',
	 * 'updatebyname':''
	 * }
	 * 
	 * @param \Zend\Http\Request $request
	 * @param Component\AbstractComponent $component
	 * @param array $properties
	 * @return \Application\View\ViewModel
	 */
	protected function _saveComponent(\Zend\Http\Request $request, Component\AbstractComponent $component, array $properties)
	{
		$view = $this->view;
		$uid = $request->getPost('uid', null);

		/* populate with commons properties */
		$properties['uid'] = $uid;
		$properties['name'] = $request->getPost('name', null);
		$properties['index'] = $request->getPost('index', null);
		$properties['parentId'] = $request->getPost('parentid', null);
		$properties['parentUid'] = $request->getPost('parentuid', null);
		$properties['attributes']['title'] = $request->getPost('title', '{empty}');
		$properties['attributes']['xoffset'] = $request->getPost('xoffset', null);
		$properties['attributes']['yoffset'] = $request->getPost('yoffset', null);
		$properties['attributes']['zindex'] = $request->getPost('zindex', null);
		$properties['attributes']['draggable'] = $request->getPost('draggable', null);
		$properties['attributes']['resizable'] = $request->getPost('resizable', null);
		$properties['attributes']['resizablex'] = $request->getPost('resizablex', null);
		$properties['attributes']['resizabley'] = $request->getPost('resizabley', null);
		$properties['attributes']['collapsed'] = $request->getPost('collapsed', false);

		//convert visibility code to visibility name
		/*
		 $visibility = $request->getPost('visibility', null);
		 switch (strtolower($visibility)) {
		 case 'private':
		 $visibility = AclSiergate::VISIBILITY_PRIVATE;
		 break;
		 case 'internal':
		 $visibility = AclSiergate::VISIBILITY_INTERNAL;
		 break;
		 case 'public':
		 $visibility = AclSiergate::VISIBILITY_PUBLIC;
		 break;
		 }
		 $properties['visibility'] = $visibility;
		 */

		$dao = DaoFactory::get()->getDao($component->cid);

		if ( $uid ) {
			try {
				$dao->loadFromUid($component, $uid);
			}
			catch( \Rbplm\Dao\NotExistingException $e ) {
				$component = $component::init($component);
				$component->setOwner(People\CurrentUser::get());
			}
		}
		else {
			$component = $component::init($component);
			$component->setOwner(People\CurrentUser::get());
		}
		
		$component->hydrate($properties)
			->setUpdated(new \DateTime())
			->setUpdateBy(People\CurrentUser::get());

		$dao->save($component);
		
		$view->component = $component;
		$view->setTemplate('portail/component/save');
		return $view;
	}

	/**
	 *
	 */
	public function saveboxAction()
	{
		$request = $this->getRequest();

		$component = new Component\Box();
		$body = $request->getPost('body', '{empty}');
		$properties = [
			'attributes' => [
				'body' => $body
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 *
	 */
	public function savetabAction()
	{
		$request = $this->getRequest();

		$component = new Component\Tab();
		$label = $request->getPost('label', '{empty}');
		$notice = $request->getPost('notice', '{empty}');

		$properties = [
			'attributes' => [
				'label' => $label,
				'notice' => $notice
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 *
	 */
	public function savestatusboxAction()
	{
		$request = $this->getRequest();

		$component = new Component\Statusbox();
		$body = $request->getPost('body', '{empty}');
		$label = $request->getPost('label', '{empty}');
		$status = $request->getPost('status', '');
		$notice = $request->getPost('notice', '{empty}');
		$statuslist = $request->getPost('statuslist', '{}');

		$properties = [
			'attributes' => [
				'body' => $body,
				'label' => $label,
				'status' => $status,
				'notice' => $notice,
				'statuslist' => $statuslist
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 *
	 */
	public function savecheckboxAction()
	{
		$request = $this->getRequest();

		$component = new Component\Checkbox();
		$notice = $request->getPost('notice', '{empty}');
		$items = $request->getPost('items', '{}');

		$properties = [
			'attributes' => [
				'notice' => $notice,
				'items' => $items
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 *
	 */
	public function savefilelistAction()
	{
		$request = $this->getRequest();

		$component = new Component\Filelist();
		$notice = $request->getPost('notice', '{empty}');

		$properties = [
			'attributes' => [
				'notice' => $notice
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 *
	 */
	public function savedocumentlistAction()
	{
		$request = $this->getRequest();

		$component = new Component\Documentlist();
		$notice = $request->getPost('notice', '{empty}');
		$headers = $request->getPost('headers', '{}');
		$actions = $request->getPost('actions', '{}');
		$config = $request->getPost('config', '{}');
		$filterform = $request->getPost('filterform', '');
		$template = $request->getPost('template', '');
		
		$properties = [
			'attributes' => [
				'notice' => $notice,
				'headers' => json_decode($headers, true),
				'filterform' => $filterform,
				'actions' => $actions,
				'config' => json_decode($config, true),
				'template' => $template
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 *
	 */
	public function savesimplegridAction()
	{
		$request = $this->getRequest();

		$component = new Component\Simplegrid();
		$notice = $request->getPost('notice', '{empty}');
		$items = $request->getPost('items', '{}');

		$properties = [
			'attributes' => [
				'notice' => $notice,
				'items' => json_decode($items, true)
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 *
	 */
	public function savejalonAction()
	{
		$request = $this->getRequest();

		$component = new Component\Jalon();
		$body = $request->getPost('body', '{empty}');
		$label = $request->getPost('label', '{empty}');
		$status = $request->getPost('status', '');
		$notice = $request->getPost('notice', '{empty}');
		$statuslist = $request->getPost('statuslist', '{}');
		$targetdate = $request->getPost('targetdate', '');

		$properties = [
			'attributes' => [
				'body' => $body,
				'label' => $label,
				'status' => $status,
				'notice' => $notice,
				'statuslist' => json_decode($statuslist, true),
				'targetdate' => $targetdate
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 *
	 */
	public function savetinymceAction()
	{
		$request = $this->getRequest();

		$component = new Component\Tinymce();
		$body = $request->getPost('body', '{empty}');

		$properties = [
			'attributes' => [
				'body' => $body
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 *
	 */
	public function saveindicatorAction()
	{
		$request = $this->getRequest();

		$component = new Component\Indicator();
		$body = $request->getPost('body', '{empty}');
		$label = $request->getPost('label', '{empty}');
		$status = $request->getPost('status', '');
		$notice = $request->getPost('notice', '{empty}');
		$value = $request->getPost('value', '');

		$properties = [
			'attributes' => [
				'body' => $body,
				'label' => $label,
				'status' => $status,
				'notice' => $notice,
				'value' => $value
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 *
	 */
	public function savetabpanelAction()
	{
		$request = $this->getRequest();

		$notice = $request->getPost('notice', '{empty}');
		$items = $request->getPost('items', null);
		json_decode($items, true);

		$component = new Component\Tabpanel();
		/* reinit items */
		$component->setItems([]);
		if ( is_array($items) ) {
			foreach( $items as $item ) {
				$tab = new Component\Tabpanel\Item($item->index, $item->notice, $item->label);
				$component->addItem($tab);
			}
		}

		$properties = [
			'items' => $items,
			'attributes' => [
				'notice' => $notice
			]
		];

		$view = $this->_saveComponent($request, $component, $properties);
		return $view;
	}

	/**
	 * HTTP/GET only
	 * 
	 * If $save is true, save new component in db
	 * @param string $type
	 * @param boolean $save
	 */
	protected function _getComponent($type)
	{
		$view = $this->view;

		$modelClass = 'Portail\Model\Component\\' . $type;
		$renderClass = 'Portail\Renderer\\' . $type;
		$factory = DaoFactory::get();

		$id = (int)$this->params()->fromRoute('id', null);

		if ( $id ) {
			$component = new $modelClass();
			$factory->getDao($component->cid)
				->setOption('withtrans', true)
				->loadFromId($component, $id);
		}
		else {
			$component = $modelClass::init();
			$request = $this->getRequest();
			$parentId = $request->getQuery('parentid', null);
			$parentUid = $request->getQuery('parentuid', null);
			$save = $request->getQuery('save', null);
			if ( $save && $parentId && $parentUid ) {
				$component->hydrate(array(
					'parentId' => $parentId,
					'parentUid' => $parentUid
				));
				$factory->getDao($component->cid)
					->setOption('withtrans', true)
					->save($component);
			}
		}

		$renderer = new $renderClass($component, new ViewModel());
		$renderer->setMode($renderer::MODE_BUILD);
		$renderer->bind();

		$view->setTemplate('component/get');
		$view->addChild($renderer->render(), 'component');
		return $view;
	}

	/**
	 *
	 */
	public function getboxAction()
	{
		return $this->_getComponent('Box');
	}

	/**
	 *
	 */
	public function getindicatorAction()
	{
		return $this->_getComponent('Indicator');
	}

	/**
	 *
	 */
	public function getstatusboxAction()
	{
		return $this->_getComponent('Statusbox');
	}

	/**
	 *
	 */
	public function getjalonAction()
	{
		return $this->_getComponent('Jalon');
	}

	/**
	 *
	 */
	public function gettinymceAction()
	{
		return $this->_getComponent('Tinymce');
	}

	/**
	 *
	 */
	public function getjssorAction()
	{
		return $this->_getComponent('Jssor');
	}

	/**
	 *
	 */
	public function getsimplegridAction()
	{
		return $this->_getComponent('Simplegrid');
	}

	/**
	 *
	 */
	public function getcheckboxAction()
	{
		return $this->_getComponent('Checkbox');
	}

	/**
	 *
	 */
	public function getfilelistAction()
	{
		return $this->_getComponent('Filelist');
	}

	/**
	 *
	 */
	public function getdocumentlistAction()
	{
		return $this->_getComponent('Documentlist');
	}

	/**
	 *
	 */
	public function gettabpanelAction()
	{
		return $this->_getComponent('Tabpanel');
	}

	/**
	 *
	 */
	public function gettabAction()
	{
		return $this->_getComponent('Tab');
	}

	/**
	 *
	 */
	public function getaccordionAction()
	{
		return $this->_getComponent('Accordion');
	}

	/**
	 * Delete a component from his uid
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();

		$uid = $request->getQuery('uid', null);
		$cssClass = $request->getQuery('cssclass', null);
		$factory = DaoFactory::get();

		if ( strstr($cssClass, 'rbp-filelist') ) {
			$factory->getDao(Component\Filelist::$classId)->delete($uid);
		}
		else {
			$factory->getDao(Component\AbstractComponent::$classId)->delete($uid);
		}

		$view = $this->view;
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		return $view;
	}
}
