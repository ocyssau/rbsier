<?php
namespace Portail\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Portail\Model\Component;

/**
 */
class IndexController extends AbstractController
{

	/** @var integer */
	public $pageId = 'portail_index';

	/** @var string */
	public $ifSuccessForward = 'portail';

	/** @var string */
	public $ifFailedForward = 'portail';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		
		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('portail');
		$this->layout()->tabs = $tabs;
		
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/**/
		$request = $this->getRequest();
		$view = $this->view;
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Init some helpers*/
		$factory = DaoFactory::get();
		$list = $factory->getList(Component\Project::$classId);
		$list->setTranslator($factory->getDao(Component\Project::$classId)
			->getTranslator());

		/**/
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Portail\Form\Project\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'portailfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$select = $list->getTranslator()->getSelectAsApp();
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->orderby = 'name';
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->mode = 'admin';
		$view->pageTitle = 'Portails';
		$view->pageSubTitle = 'pages';

		return $view;
	}
}
