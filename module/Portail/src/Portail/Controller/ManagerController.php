<?php
namespace Portail\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Portail\Model\Component\Project as PortailModel;

/**
 */
class ManagerController extends AbstractController
{

	/** @var string */
	public $pageId = 'portail_manager';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		/* before parent::init() */
		$toUrl = $this->url()->fromRoute('portail', []);
		$this->defaultSuccessForward = $toUrl;
		$this->defaultFailedForward = $toUrl;
		
		parent::init();
		
		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('portail');
		$this->layout()->tabs = $tabs;
		
		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function createAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
			$cancel = false;
		}
		else if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* init some helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(PortailModel::$classId);

		/* Init document */
		/* @var PortailModel $portail */
		$portail = PortailModel::init();
		$portail->setName('New Portail');
		$portail->setUrl('newPortail');
		
		/* Construct the form */
		$form = new \Portail\Form\EditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($portail);
		$form->setMode('create');
		
		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($portail);
				return $this->successForward();
			}
		}

		$view->pageTitle = tra('Create New Portail');
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$view = $this->view;
		$id = $this->params()->fromRoute('id');
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}
		else {
			$validate = false;
			$cancel = false;
		}

		/* init some helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(PortailModel::$classId);

		/* Load */
		$portail = new PortailModel();
		$dao->loadFromId($portail, $id);

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('edit', $portail->parentUid);

		/* Construct the form */
		$form = new \Portail\Form\EditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($portail);
		$form->setMode('create');

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($portail);
				return $this->successForward();
			}
		}

		$view->pageTitle = tra(sprintf('Edit Portail %s', $portail->getName()));
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 * 
	 */
	public function deleteAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', []);
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$ids = $request->getPost('checked', []);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* init some helpers */
		$factory = DaoFactory::get();
		/** @var \Rbs\Ged\PortailModelDao $dao */
		$dao = $factory->getDao(PortailModel::$classId);

		/* display form */
		if ( !$validate ) {
			$form = new \Portail\Form\ConfirmForm();
			$form->setUrlFromCurrentRoute($this);

			foreach( $ids as $id ) {
				/* Unselect document with other spacename */
				$portail = new PortailModel();
				$dao->loadFromId($portail, $id);

				/* Check permissions */
				$this->getAcl()->checkRightFromUid('delete', $portail->parentUid);

				$form->addToConfirm($portail);
			}

			$view->pageTitle = "Suppression is Definitif";
			$view->warning = "Are you sure that you want suppress this portails ?";
			$view->setTemplate($form->template);
			$view->form = $form;
			return $view;
		}
		/* validate */
		else {
			foreach( $ids as $id ) {
				try {
					$dao->deleteFromId($id);
				}
				catch( \PDOException $e ) {
					if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
						$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $id));
					}
					else {
						$this->errorStack()->error($e->getMessage());
					}
					continue;
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
					continue;
				}
			}
			return $this->successForward();
		}
	}
}
