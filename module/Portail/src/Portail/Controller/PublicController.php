<?php
namespace Portail\Controller;

use Application\Controller\AbstractController;
use Zend\Mvc\Controller\AbstractActionController;
use Rbs\Space\Factory as DaoFactory;
use Portail\Model\Component\Project as PortailModel;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;
use Portail\Renderer;
use Application\View\ViewModel as RbViewModel;
use Zend\View\Model\ViewModel;
use Ranchbe;

/**
 */
class PublicController extends AbstractController
{

	/** @var integer */
	public $pageId = 'portail_public';

	/** @var string */
	public $ifSuccessForward = 'portail';

	/** @var string */
	public $ifFailedForward = 'portail';

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractController::dispatch()
	 *
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		$this->maintenancemodeCheck();
		$this->view = new RbViewModel(Ranchbe::get());
		$this->errorStack = $this->getRbServiceManager()->getErrorStack();
		AbstractActionController::dispatch($request, $respons);
	}
	
	/**
	 *
	 */
	public function indexAction()
	{
		/**/
		$view = $this->view;
		$name = $this->params()->fromRoute('name');
		
		/* Init some helpers*/
		$factory = DaoFactory::get();
		$dao = $factory->getDao(PortailModel::$classId);
		
		/* load portail */
		try{
			$portail = new PortailModel();
			$dao->loadFromUrl($portail, $name);
		}
		catch(\Rbplm\Dao\NotExistingException $e){
			throw new \Application\Controller\ControllerException('this portail is not existing');
		}
		
		/* load components */
		try{
			$factory->getLoader()->loadChildren($portail);
		}
		catch(\Rbplm\Dao\NotExistingException $e){
			throw new \Application\Controller\ControllerException('Unable to load children');
		}
		
		$renderer = new Renderer\Project($portail, new ViewModel());
		$renderer->setMode($renderer::MODE_VIEW);
		$renderer->bind();

		/* Display the template */
		$view->mode = 'admin';
		$view->pageTitle = 'Portails';
		$view->pageSubTitle = 'pages';
		$view->portail = $portail;
		$view->addChild($renderer->render(), 'project');
		return $view;
	}
}
