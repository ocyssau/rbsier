<?php
namespace Portail\Controller;

use Application\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Rbs\Space\Factory as DaoFactory;
use Portail\Model\Component\Project as PortailModel;
use Portail\Renderer;

/**
 * 
 *
 */
class BuilderController extends AbstractController
{

	/** @var string */
	public $pageId = 'portail_builder';

	/** @var string */
	public $defaultSuccessForward = 'portail/index';

	/** @var string */
	public $defaultFailedForward = 'portail/index';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('portail');
		$this->layout()->tabs = $tabs;
		
		$toUrl = $this->url()->fromRoute('portail', []);
		$this->defaultSuccessForward = $toUrl;
		$this->defaultFailedForward = $toUrl;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * 
	 * @return \Application\View\ViewModel
	 */
	public function indexAction()
	{
		$view = $this->view;
		$projectId = (int)$this->params()->fromRoute('id');

		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/*
		 $acl = $this->getProjectAcl($projectId);
		 if(!$acl->hasRight($acl::RIGHT_UPDATE)){
		 return $this->notauthorized();
		 }
		 */

		$factory = DaoFactory::get();
		$portail = new PortailModel();
		$factory->getDao(PortailModel::$classId)->loadFromId($portail, $projectId);
		$factory->getLoader()->loadChildren($portail);

		$renderer = new Renderer\Project($portail, new ViewModel());
		$renderer->setMode($renderer::MODE_BUILD);
		$renderer->bind();

		$view->addChild($renderer->render(), 'project');
		return $view;
	}
}
