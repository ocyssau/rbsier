<?php
//%LICENCE_HEADER%
namespace Portail\Dao\Component;

/**
 * @brief Dao class for \Rbplm\Org\Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see \Rbplm\Dao\Pg
 * @see \Rbplm\Org\UnitTest
 *
 */
class BoxDao extends ComponentDao
{
}
