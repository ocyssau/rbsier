<?php
//%LICENCE_HEADER%
namespace Portail\Dao\Component;

use Rbplm\Sys\Filesystem;

/**
 * @brief Dao class for \Rbplm\Org\Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see \Rbplm\Dao\Pg
 * @see \Rbplm\Org\UnitTest
 *
 */
class FilelistDao extends ComponentDao
{
	/**
	 * (non-PHPdoc)
	 * @see Siergate\Dao.Dao::delete()
	 */
	public function delete($uid, $withChilds = true, $withTrans = true)
	{
		$component = $this->loadFromUid(new \Portail\Model\Component\Filelist(), $uid);

		/* delete record in db */
		parent::delete($uid, $withChilds, $withTrans);

		/* delete reposit */
		try {
			$reposit = Filesystem\Folder::initFromUid($component->getReposit());
			$reposit->delete();
		}
		catch( \Exception $e ) {}
	}
}
