<?php
//%LICENCE_HEADER%
namespace Portail\Dao\Component\Documentlist;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `portail_documentlist_items` (
 `link_id` INT(11) NOT NULL AUTO_INCREMENT,
 `parent_uid` VARCHAR(64) default NULL,
 `child_id` INT(11) default NULL,
 `child_uid` VARCHAR(64) default NULL,
 `child_space_id` INT(11) default NULL,
 `lindex` INT(11) default 0,
 data TEXT default NULL,
 PRIMARY KEY (`link_id`),
 UNIQUE KEY (`parent_uid`,`child_uid`),
 INDEX (`lindex`),
 INDEX (`parent_uid`),
 INDEX (`child_uid`),
 INDEX (`child_id`, `child_space_id`)
 ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=10;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE IF EXISTS `portail_documentlist_items`;
 <<*/

/**
 */
class ItemDao extends DaoSier
{

	/** @var \PDOStatement */
	protected $loadChildrenStmt;

	/** @var \PDOStatement */
	protected $loadParentStmt;

	/**
	 *
	 * @var string
	 */
	public static $table = 'portail_documentlist_items';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'portail_documentlist_items';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = null;

	/**
	 *
	 * @var string
	 */
	public static $parentTable = 'portail_components';

	/**
	 *
	 * @var string
	 */
	public static $parentForeignKey = 'uid';

	/**
	 *
	 * @var string
	 */
	public static $childTable = '%s_documents';

	/**
	 *
	 * @var string
	 */
	public static $childForeignKey = 'uid';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = [
		'link_id' => 'id',
		'parent_uid' => 'parentUid',
		'child_id' => 'childId',
		'child_uid' => 'childUid',
		'child_space_id' => 'childSpaceId',
		'lindex' => 'lindex',
		'data' => 'data'
	];

	/**
	 * @var array
	 */
	public static $sysToAppFilter = [
		'data' => 'json'
	];

	/**
	 *
	 * @param \PDO $conn
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);

		$this->_parentTable = static::$parentTable;
		$this->_parentForeignKey = static::$parentForeignKey;
		$this->_childTable = static::$childTable;
		$this->_childForeignKey = static::$childForeignKey;
	}

	/**
	 * Get list of children links
	 *
	 * Some alias are defined to be used in filters :
	 * <ul>
	 * * <li>.lt alias to refer to link table ([spacename]_doc_rel) </li>
	 * * <li>.doc to refer to children documents </li>
	 * </ul>
	 *
	 * @example
	 *  Return all documents children of document identified by $parentDocumentId :
	 *  $filter->select(['doc.*']);
	 * 	$filter->andfind($parentDocumentId, 'lt.parent_id', Op::EQUAL)
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return \PDOStatement
	 */
	public function getChildren($filter = null, $bind = [])
	{
		$leftTable = $this->_table;
		$leftKey = $this->toSys('childId');
		$rightTable = $this->_childTable;
		$rightKey = $this->_childForeignKey;
		$select = [];

		if ( !$this->loadChildrenStmt ) {
			if ( $filter instanceof \Rbplm\Dao\FilterAbstract ) {
				$filterStr = $filter->__toString();
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
			}
			else {
				$filterStr = '1=1';
				$selectStr = '*';
			}

			$sql = "SELECT $selectStr FROM $leftTable AS lt";
			$sql .= " JOIN $rightTable AS doc ON lt.$leftKey=doc.$rightKey";
			$sql .= " WHERE $filterStr";

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadChildrenStmt = $stmt;
		}

		$this->loadChildrenStmt->execute($bind);
		return $this->loadChildrenStmt;
	}

	/**
	 * Get documents father linked to document.
	 *
	 * Some alias are defined to be used in filters :
	 * <ul>
	 * * <li>.lt alias to refer to link table  ([spacename]_doc_rel) </li>
	 * * <li>.doc to refer to parent documents </li>
	 * </ul>
	 *
	 * @example
	 *  Return all documents parents of document identified by $childDocumentId :
	 *  $filter->select(['doc.*']);
	 * 	$filter->andfind($childDocumentId, 'lt.child_id', Op::EQUAL)
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return \PDOStatement
	 *
	 */
	function getParents($filter = null, $bind = [])
	{
		$leftTable = $this->_table;
		$leftKey = $this->toSys('parentId');
		$rightTable = $this->_parentTable;
		$rightKey = $this->_parentForeignKey;
		$select = [];

		if ( !$this->loadParentStmt ) {
			if ( $filter ) {
				$filterStr = $filter->__toString();
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
			}
			else {
				$filterStr = '1=1';
				$selectStr = '*';
			}

			$sql = "SELECT $selectStr FROM $leftTable AS lt";
			$sql .= " JOIN $rightTable AS doc ON lt.$leftKey=doc.$rightKey";
			$sql .= " WHERE $filterStr";

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadParentStmt = $stmt;
		}

		$this->loadParentStmt->execute($bind);
		return $this->loadParentStmt;
	}

	/**
	 * @param integer $parentId 	The id of parent document of links to delete
	 * @param integer $spaceId 		The space id as return by Space\Factory::getId()
	 * @param string $linkType 		[OPTIONAL] Limit the delete to type of link, or let null to delete all types
	 *
	 * @return ItemDao
	 */
	public function deleteFromParentId($parentId, $spaceId, $linkType = null)
	{
		$toSysId = $this->toSys('parentId');
		$toSysSpaceId = $this->toSys('parentSpaceId');
		$filter = "$toSysId=:parentId AND $toSysSpaceId=:spaceId";
		$bind = [
			':parentId' => $parentId,
			':spaceId' => $spaceId
		];

		$this->_deleteFromFilter($filter, $bind);
		return $this;
	}
}
