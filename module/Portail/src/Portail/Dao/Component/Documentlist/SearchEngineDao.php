<?php
namespace Portail\Dao\Component\Documentlist;

/** SQL_SCRIPTS>>
 << */

/** SQL_ALTER>>
 << */

/**
 *
 *
 */
class SearchEngineDao extends \Rbplm\Dao\DaoListAbstract
{
	
	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'doc.id' => 'id',
		'doc.uid' => 'uid',
		'doc.cid' => 'cid',
		'doc.name' => 'name',
		'doc.number' => 'number',
		'doc.designation' => 'designation',
		'doc.spacename' => 'spacename',
		'doc.lockById' => 'lockById',
		'doc.lockByUid' => 'lockByUid',
		'doc.locked' => 'locked',
		'doc.updateById' => 'updateById',
		'doc.updateByUid' => 'updateByUid',
		'doc.updated' => 'updated',
		'doc.createById' => 'createById',
		'doc.createByUid' => 'createByUid',
		'doc.created' => 'created'
	);
	
	/**
	 *
	 * @see \Rbplm\Dao\DaoInterface::load()
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return SearchEngineDao
	 */
	public function load($filter = null, $bind = null)
	{
		if ( $this->isLoaded == true ) {
			$this->_reset();
		}
		
		$sql = "SELECT * FROM(
		SELECT
		%select%,
		'workitem' AS `spacename`,
		'569e92709feb6' AS `cid`
		FROM `workitem_documents` AS `doc`
		WHERE %filter%
		UNION
		SELECT
		%select%,
		'bookshop' AS `spacename`,
		'569e92709feb6' AS `cid`
		FROM `bookshop_documents` AS `doc`
		WHERE %filter%
		UNION
		SELECT
		%select%,
		'cadlib' AS `spacename`,
		'569e92709feb6' AS `cid`
		FROM `cadlib_documents` AS `doc`
		WHERE %filter%
		) AS documents";
		
		$sql = str_replace('%filter%', $filter->whereToString(), $sql);
		$sql = str_replace('%select%', $filter->selectToString(), $sql);
		$sql = $sql . $filter->orderToString();
		
		//echo '<pre>'.$sql;die;
		
		$this->stmt = $this->connexion->prepare($sql);
		$this->stmt->setFetchMode($this->options['mode']);
		$this->stmt->execute($bind);
		$this->isLoaded = true;
		$this->filter = $filter;
		return $this;
	}
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::save()
	 */
	public function save($list)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::delete()
	 */
	public function delete($filter)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::countAll()
	 */
	public function countAll($filter, $bind = null)
	{
		$sql = "SELECT COUNT(*) FROM(
			SELECT doc.id FROM workitem_documents as doc
			WHERE %filter%
			UNION
			SELECT doc.id FROM bookshop_documents as doc
			WHERE %filter%
			UNION
			SELECT doc.id FROM cadlib_documents as doc
			WHERE %filter%
			UNION
			SELECT doc.id FROM mockup_documents as doc
			WHERE %filter%
		) AS documents";
		
		$sql = str_replace('%filter%', $filter->whereToString(), $sql);
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		$count = $stmt->fetch();
		return $count[0];
	}
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::areExisting()
	 */
	public function areExisting($propNameToTest, $list, $returnSelect = null)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}
} /* End of class */
