<?php
//%LICENCE_HEADER%
namespace Portail\Dao\Component;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `portail_components`(
 `id` INT NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `cid` VARCHAR(64) NOT NULL,
 `name` VARCHAR(255) NOT NULL,
 `title` MEDIUMTEXT,
 `url` VARCHAR(255),
 `internalref` VARCHAR(255) NULL,
 `index` INT NULL,
 `visibility` VARCHAR(255) NULL,
 `ownerId` VARCHAR(255) NULL,
 `parentId` INT NULL,
 `parentUid` VARCHAR(255) NULL,
 `updateById` VARCHAR(255) NULL,
 `updated` DATETIME NULL,
 `attributes` MEDIUMTEXT NULL,
 PRIMARY KEY (`id`),
 UNIQUE (uid),
 INDEX (`url` ASC),
 INDEX (`index` ASC),
 INDEX (`visibility` ASC),
 INDEX (`parentId` ASC),
 INDEX (`parentUid` ASC),
 INDEX (`cid` ASC),
 INDEX (`internalref` ASC)
 );
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE IF EXISTS `portail_components`;
 <<*/

/**
 * 
 *
 */
class ComponentDao extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table = 'portail_components';

	/** @var string */
	public static $vtable = 'portail_components';

	/** @var string */
	public static $sequenceName = null;

	/**
	 * @var array
	 */
	public static $sysToApp = [
		'id' => 'id',
		'cid' => 'cid',
		'uid' => 'uid',
		'name' => 'name',
		'internalref' => 'internalref',
		'title' => 'title',
		'url' => 'url',
		'index' => 'index',
		'visibility' => 'visibility',
		'ownerId' => 'ownerId',
		'parentId' => 'parentId',
		'parentUid' => 'parentUid',
		'updateById' => 'updateById',
		'updated' => 'updated',
		'attributes' => 'attributes'
	];

	/**
	 * @var array
	 */
	public static $sysToAppFilter = [
		'attributes' => 'json',
		'updated' => 'datetime'
	];

	/**
	 *
	 * Recursive function
	 * Load from Uid
	 * @param parent
	 * @param string
	 */
	public function loadChildren($parent, $filter = null)
	{
		if ( method_exists($parent, 'setChild') ) {
			throw new \Exception('parent object has no method setChild', 25000);
		}

		$parentUid = $parent->getUid();
		$select = $this->getTranslator()->getSelectAsApp(null, [
			'cid',
			'uid',
			'id'
		]);
		$filter = "parentUid=:parentUid ORDER BY `index` ASC";
		$bind = [
			':parentUid' => $parentUid
		];
		$stmt = $this->query($select, $filter = null, $bind);

		/**/
		foreach( $stmt->fetchAll(\PDO::FETCH_ASSOC) as $entry ) {
			$uid = $entry['uid'];
			$child = $this->factory->getModel($entry['cid']);
			$this->loadFromUid($child, $uid);
			$parent->addChild($child);
			if ( method_exists($child, 'setChild') ) {
				self::loadChildren($child);
			}
		}

		return $parent;
	}
}
