<?php
//%LICENCE_HEADER%
namespace Portail\Dao\Component;

use Rbplm\Dao\MappedInterface;

/**
 *
 */
class ProjectDao extends ComponentDao
{
	/**
	 */
	public function loadFromUrl(MappedInterface $mapped, $url)
	{
		$filter = 'obj.' . $this->toSys('url') . '=:url';
		$bind = [
			':url' => $url
		];
		return $this->load($mapped, $filter, $bind);
	}
	
}
