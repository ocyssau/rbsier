<?php
//%LICENCE_HEADER%
namespace Portail\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Simplegrid extends AbstractRenderer
{

	/**
	 * @param \Portail\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/simplegrid';
		AbstractRenderer::__construct($component, $view);
	}

	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$this->view->notice = $this->component->getAttribute('notice');
		$this->view->items = $this->component->getItems();
		return $this;
	}
}
