<?php
//%LICENCE_HEADER%
namespace Portail\Renderer;

use Portail\Model\Component\AbstractComponent;
use Zend\View\Model\ViewModel;

/**
 * 
 * @author ocyssau
 *
 */
class Factory
{

	/**
	 * @param \Portail\Model\Component\AbstractComponent $component
	 */
	public static function get($component)
	{
		if ( $component instanceof AbstractComponent ) {
			$className = get_class($component);
		}

		$parts = explode('\\', $className);
		$RendererClass = __NAMESPACE__ . '\\' . end($parts);

		return new $RendererClass($component, new ViewModel());
	}
}
