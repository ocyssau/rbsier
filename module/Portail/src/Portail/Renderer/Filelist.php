<?php
//%LICENCE_HEADER%
namespace Portail\Renderer;

use Portail\Model\Component\Filelist\Item;

/**
 * 
 * @author ocyssau
 *
 */
class Filelist extends AbstractRenderer
{

	/**
	 * @param \Portail\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/filelist';
		AbstractRenderer::__construct($component, $view);
	}

	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$this->view->notice = $this->component->getAttribute('notice');
		
		/* get pictures from files */
		$images = [];
		$items = [];
		foreach( $this->component->getItems() as $item ) {
			if($item instanceof Item){
				/* file is a image? */
				if ( $item->isImage == true ) {
					$images[] = $item;
				}
				else{
					$items[] = $item;
				}
			}
		}
		$this->view->images = $images;
		$this->view->items = $items;
		$this->view->reposit = $this->component->getReposit();
		return $this;
	}
}
