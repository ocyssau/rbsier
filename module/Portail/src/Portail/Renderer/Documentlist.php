<?php
//%LICENCE_HEADER%
namespace Portail\Renderer;

use Portail\Form\Component\Documentlist\BuilderForm;
use Rbs\Space\Factory as DaoFactory;

/**
 * 
 * @author ocyssau
 *
 */
class Documentlist extends AbstractRenderer
{

	/**
	 * @param \Portail\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/documentlist';
		$this->publicTemplate = 'component/public/documentlist/common';
		AbstractRenderer::__construct($component, $view);
	}

	/**
	 *
	 */
	private function getItems()
	{
		/* */
		$view = $this->view;

		$factory = DaoFactory::get();

		/* @var \Portail\Model\Component\Documentlist $component */
		$component = $this->getComponent();

		/* */
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		/* @var \Application\Form\AbstractFilterForm $filterform */
		$filterformClass = $component->getAttribute('filterform');
		if ( $filterformClass ) {
			$filterform = new $filterformClass($factory, $component);
			$searchEngine = $filterform->getSearchEngine();
		}
		else {
			$filterform = new \Portail\Form\Component\Documentlist\UserFilterForm($factory, $component);
			$searchEngine = new \Portail\Dao\Component\Documentlist\SearchEngineDao($factory->getConnexion());
			$filterform->setSearchEngine($searchEngine);
		}
		$filterform->load();
		$filterform->bindToFilter($filter);
		$filterform->save();

		/* Paginator */
		$paginator = new \Application\Form\PaginatorForm('paginator-' . $component->getUid());
		$paginator->load([
			'default' => [
				'orderby' => 'name',
				'order' => 'desc',
				'limit' => 50
			]
		]);

		/* get columns headers */
		$headers = $component->getAttribute('headers');

		$trans = $searchEngine->getTranslator();
		
		/* minimal select */
		$select = [
			sprintf('`%s`.`%s` AS `%s`', 'doc', $trans->toSys('id'), 'id'),
			sprintf('`%s`.`%s` AS `%s`', 'doc', $trans->toSys('name'), 'name'),
			sprintf('`%s`.`%s` AS `%s`', 'doc', $trans->toSys('accessCode'), 'accessCode')
		];
		
		foreach( $headers as $header ) {
			if ( $header['column'] == 'id' || $header['column'] == 'name' || $header['column'] == 'spacename' || $header['column'] == 'accessCode' ) {
				continue;
			}
			$asApp = $header['column'];
			$asSys = $trans->toSys($asApp);
			$select[] = sprintf('`%s`.`%s` AS `%s`', 'doc', $asSys, $asApp);
		}
		$filter->select($select);

		try {
			/* Set max limit paginator */
			$max = $searchEngine->countAll($filter, $filter->getBind());
			$max = ($max > 1000) ? 1000 : $max;
			$paginator->setMaxLimit($max);
			$view->countOutput = $max;

			$paginator->bindToFilter($filter);
			$searchEngine->load($filter, $filter->getBind());
			$view->list = $searchEngine->getStmt()->fetchAll(\PDO::FETCH_ASSOC);

			/* save paginator and filter */
			$paginator->bindToView($view)->save();
			$filterform->save();
		}
		catch( \Throwable $e ) {
			throw new \Exception($e->getMessage(), $e->getCode(), $e);
		}

		$view->filter = $filterform;
		$view->headers = $headers;
		return $view;
	}

	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$factory = \Rbs\Space\Factory::get('workitem');
		$this->view->headers = [];
		$this->view->notice = $this->component->getAttribute('notice');

		if ( $this->getMode() == self::MODE_BUILD ) {
			$conf = \Ranchbe::get()->getMvcApplication()->getConfig();
			$builderForm = new BuilderForm($factory);
			$builderForm->setFilterforms($conf['portail']['userfilterforms']);
			$builderForm->setTemplates($conf['portail']['templates'][get_class($this->component)]);

			$headers = $this->component->getAttribute('headers');
			$filterformClass = $this->component->getAttribute('filterform');
			$actions = $this->component->getAttribute('actions');
			$userConfig = $this->component->getAttribute('config');
			$template = $this->template;

			$builderForm->populateValues([
				'filterform' => $filterformClass,
				'userTemplate' => $template,
				'userConfig' => json_encode($userConfig),
				'actions' => $actions,
				'headers' => $headers
			]);

			$this->view->builderform = $builderForm;
		}
		else {
			$this->view->setTemplate($this->publicTemplate);
			$template = $this->component->getAttribute('template');
			if($template){
				$this->view->itemsTemplate = $template;
			}
			$this->view->items = $this->getItems();
		}

		return $this;
	}
}
