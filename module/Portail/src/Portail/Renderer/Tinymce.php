<?php
//%LICENCE_HEADER%
namespace Portail\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Tinymce extends AbstractRenderer
{

	/**
	 * @param \Portail\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/tinymce';
		parent::__construct($component, $view);
	}
}
