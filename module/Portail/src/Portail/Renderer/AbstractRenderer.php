<?php
//%LICENCE_HEADER%
namespace Portail\Renderer;

use Portail\Model\Component\AbstractComponent;
use Zend\View\Model\ViewModel;

/**
 * 
 *
 */
abstract class AbstractRenderer
{

	const MODE_BUILD = 'build';

	const MODE_VIEW = 'view';

	/**
	 * @var AbstractComponent
	 */
	protected $component;

	/**
	 * @var ViewModel
	 */
	protected $view;

	/**
	 * @var array
	 */
	protected $children;

	/**
	 * @var string
	 */
	protected $template;

	/**
	 * @var string
	 */
	protected $templateForView;

	/**
	 * @var string
	 */
	protected $html;

	/**
	 * value is one of const MODE_*
	 * @var string
	 */
	protected $mode;

	/**
	 * @param AbstractComponent $component
	 * @param ViewModel $view
	 */
	public function __construct(AbstractComponent $component, ViewModel $view)
	{
		$this->component = $component;
		$this->view = $view;
		$this->view->setTemplate($this->template);
	}

	/**
	 * Get children component of current component
	 * Create a renderer for each child
	 * Render each child renderer = get children of children...
	 * 
	 * Add a template var named as children# where # is a incremental integer start from 1
	 * children# template var contain the html of the child
	 * 
	 * @return string
	 */
	public function render()
	{
		$mode = $this->mode;
		$i = 0;
		foreach( $this->component->getChildren() as $childComponent ) {
			$i++;
			$renderer = $childComponent->getRenderer();
			$renderer->setMode($mode);
			$renderer->bind();
			$childView = $renderer->render();
			$this->view->addChild($childView, 'children' . $i);
		}
		$this->view->mode = $mode;
		return $this->view;
	}

	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		$this->view->setVariables([
			'component' => $this->component,
			'id' => $this->component->getId(),
			'uid' => $this->component->getUid(),
			'title' => $this->component->getAttribute('title'),
			'body' => $this->component->getAttribute('body'),
			'width' => $this->component->getAttribute('width'),
			'height' => $this->component->getAttribute('height'),
			'resizable' => $this->component->isResizable(),
			'resizablex' => $this->component->isResizablex(),
			'resizabley' => $this->component->isResizabley(),
			'draggable' => $this->component->isDraggable(),
			'collapsed' => $this->component->isCollapsed(),
			'visibility' => self::visibilityToView($this->component->getVisibility()),
			'updated' => $this->component->getUpdated()->format('Y-m-d H:i:s'),
			'updateBy' => $this->component->getUpdateBy(true)
		]);
		return $this;
	}

	/**
	 * Convert visibility code to visibility string name
	 * 
	 * @param integer $visibilityCode
	 * @return string
	 */
	public static function visibilityToView($visibility)
	{
		return 'public';
	}

	/**
	 * @param string
	 * @return AbstractRenderer
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
		return $this;
	}

	/**
	 * @param ViewModel
	 * @return AbstractRenderer
	 */
	public function setView($view)
	{
		$this->view = $view;
		return $this;
	}

	/**
	 * @return ViewModel
	 */
	public function getView()
	{
		return $this->view;
	}

	/**
	 * return one MODE_*
	 * @return string 
	 */
	public function getMode()
	{
		return $this->mode;
	}

	/**
	 * $string must be one const MODE_*
	 * @param string
	 * @return AbstractRenderer
	 */
	public function setMode($string)
	{
		$this->mode = $string;
		if ( $string == self::MODE_BUILD ) {
			$this->getView()->setTemplate($this->template);
		}
		elseif ( $string == self::MODE_VIEW ) {
			if ( !$this->templateForView ) {
				$this->templateForView = $this->template;
			}
			$this->getView()->setTemplate($this->templateForView);
		}
		return $this;
	}

	/**
	 * @param AbstractComponent
	 * @return AbstractRenderer
	 */
	public function setComponent($component)
	{
		$this->component = $component;
		return $this;
	}

	/**
	 * @return AbstractComponent
	 */
	public function getComponent()
	{
		return $this->component;
	}

	/**
	 * @return String
	 */
	public function getHtml()
	{}
}
