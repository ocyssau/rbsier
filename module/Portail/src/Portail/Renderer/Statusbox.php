<?php
//%LICENCE_HEADER%
namespace Portail\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Statusbox extends AbstractRenderer
{

	/**
	 * @param \Portail\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/statusbox';
		AbstractRenderer::__construct($component, $view);
	}

	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$this->view->label = $this->component->getAttribute('label');
		$this->view->status = $this->component->getAttribute('status');
		$this->view->notice = $this->component->getAttribute('notice');
		$this->view->values = $this->component->getStatuslist();
		return $this;
	}
}
