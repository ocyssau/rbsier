<?php
//%LICENCE_HEADER%
namespace Portail\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Project extends AbstractRenderer
{

	/**
	 * @param \Portail\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		parent::__construct($component, $view);
		$this->template = 'component/project';
		$this->templateForView = 'component/project';
		$this->view->setTemplate($this->template);
	}
}
