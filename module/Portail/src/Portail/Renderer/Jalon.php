<?php
//%LICENCE_HEADER%
namespace Portail\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Jalon extends Statusbox
{

	/**
	 * @param \Portail\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/jalon';
		AbstractRenderer::__construct($component, $view);
	}

	/**
	 * Transmit properties of component to view
	 * @return AbstractRenderer
	 */
	public function bind()
	{
		parent::bind();
		$this->view->targetDate = $this->component->getAttribute('targetdate');
		return $this;
	}
}
