<?php
//%LICENCE_HEADER%
namespace Portail\Renderer;

/**
 * 
 * @author ocyssau
 *
 */
class Jssor extends Filelist
{

	/**
	 * @param \Portail\Model\Component\AbstractComponent $component
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($component, $view)
	{
		$this->template = 'component/jssor';
		parent::__construct($component, $view);
	}
}
