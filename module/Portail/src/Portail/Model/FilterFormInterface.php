<?php
namespace Portail\Model;
use Portail\Model\Component\Documentlist;

/**
 * 
 *
 */
interface FilterFormInterface
{
	
	/**
	 * 
	 * @param \Rbs\Space\Factory $factory
	 * @param string $sessionId
	 */
	public function __construct($factory = null, Documentlist $component);
	

	/**
	 * @param \Rbplm\Dao\DaoListAbstract $engine
	 * @return \Portail\Form\Component\Documentlist\BuilderForm
	 */
	public function setSearchEngine(\Rbplm\Dao\DaoListAbstract $engine);

	/**
	 * @return \Rbplm\Dao\DaoListAbstract $engine
	 */
	public function getSearchEngine();

	/**
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter        	
	 * @return \Application\Form\AbstractFilterForm
	 */
	public function bindToFilter($filter);
}
