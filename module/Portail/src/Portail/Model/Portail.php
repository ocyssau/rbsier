<?php
// %LICENCE_HEADER%
namespace Portail\Model;

/**
 * @package 
 */
class Portail
{

	/** 
	 * name of template to used for display to users
	 * @var string  
	 */
	protected $userTemplate = '';

	/** 
	 * list of roles accepted to access to this portail. If empty, all users is accepted
	 * @var array 
	 *  
	 */
	protected $roles = [];

	/** 
	 * list of users accepted to access to this portail. If empty, all users is accepted
	 * @var array
	 *  
	 */
	protected $users = [];

	/**
	 * If true, accept users without authentication 
	 * @var bool
	 */
	protected $anonymousIsPermit = false;

	/**
	 * Structure of filters used to select documents to display in portail
	 * @var array
	 */
	protected $filters = [];

	/**
	 * Html content of portail
	 * @var string
	 */
	protected $content;

	/**
	 *
	 */
	public function __construct()
	{}
}
