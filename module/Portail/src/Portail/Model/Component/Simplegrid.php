<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;

/**
 * @property string $notice
 * @property array $items
 * 
 *
 */
class Simplegrid extends AbstractComponent
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcompsimplegrid';

	/**
	 * @param Simplegrid $obj
	 * @param array $properties
	 * @return Simplegrid
	 */
	public static function init(Simplegrid $obj = null, array $properties = [])
	{
		$obj = parent::init($obj, $properties);
		return $obj;
	}
	
	/**
	 * @param array $array
	 * @return Simplegrid
	 */
	public function setItems(array $array)
	{
		$this->attributes['items'] = $array;
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getItems()
	{
		return $this->getAttribute('items');
	}
}
