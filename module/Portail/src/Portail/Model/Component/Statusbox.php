<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;

/**
 * 
 * 
 * @property string $notice
 * @property string $label
 * @property string $status
 * @property array $statuslist
 *
 */
class Statusbox extends AbstractComponent
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcompstatusbox';

	/**
	 * @param array $array
	 * @return Statusbox
	 */
	public function setStatuslist(array $array)
	{
		$this->attributes['statuslist'] = $array;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getStatuslist()
	{
		return $this->getAttribute('statuslist');
	}
}
