<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;

use Portail\Model\Component\Tabpanel\Item;

/**
 * @property string $notice
 * @property array $items
 *
 */
class Tabpanel extends AbstractComponent
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcomptabpanel';

	/**
	 */
	public function __construct($properties = null)
	{
		parent::__construct($properties);
		$this->attributes['items'] = [];
	}
	
	/**
	 * @param array $array
	 * @return Tabpanel
	 */
	public function setItems(array $array)
	{
		$this->attributes['items'] = $array;
		return $this;
	}
	
	/**
	 * @param Item $item
	 * @return Tabpanel
	 */
	public function addItem(Item $item)
	{
		$this->attributes['items'][] = $item;
	}

	/**
	 * Return a array of Item objects
	 * @return array
	 */
	public function getItems()
	{
		return $this->getAttribute('items');
	}
}
