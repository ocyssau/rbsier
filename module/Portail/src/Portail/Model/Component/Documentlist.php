<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;


/**
 * @property string $notice
 * @property array $items
 * 
 *
 */
class Documentlist extends AbstractComponent
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcompdoclist';

	/**
	 */
	public function __construct($properties = null)
	{
		parent::__construct($properties);
	}

	/**
	 * @param Filelist $obj
	 * @param array $properties
	 * @return Filelist
	 */
	public static function init($obj = null, array $properties = [])
	{
		if ( $obj && !($obj instanceof Documentlist) ) {
			throw new Exception('Cant init other thing than Documentlist');
		}
		$obj = parent::init($obj, $properties);
		return $obj;
	}
}
