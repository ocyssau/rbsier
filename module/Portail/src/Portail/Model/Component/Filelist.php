<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;

use Portail\Model\Filesystem\Reposit;

/**
 * @property string $notice
 * @property array $items
 * 
 *
 */
class Filelist extends AbstractComponent
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcompfilelist';

	/**
	 * Reposit uid
	 * @var string
	 */
	protected $reposit = '';

	/**
	 */
	public function __construct($properties = null)
	{
		parent::__construct($properties);
		$this->attributes['items'] = [];
	}

	/**
	 * @param Filelist $obj
	 * @param array $properties
	 * @return Filelist
	 */
	public static function init($obj = null, array $properties = [])
	{
		if ( $obj && !($obj instanceof Filelist) ) {
			throw new Exception('Cant init other thing than Filelist');
		}
		$obj = parent::init($obj, $properties);
		$obj->setReposit(Reposit::init()->getUid());
		return $obj;
	}

	/**
	 * @param array $array
	 * @return Filelist
	 */
	public function setItems(array $array)
	{
		$this->attributes['items'] = $array;
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getItems()
	{
		return $this->getAttribute('items');
	}
	
	/**
	 * @param string
	 * @return AbstractComponent
	 */
	public function setReposit($string)
	{
		$this->reposit = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getReposit()
	{
		return $this->reposit;
	}
}
