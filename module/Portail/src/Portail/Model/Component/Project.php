<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;

/**
 * @property string $notice
 * @property string $description
 */
class Project extends AbstractComponent
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcompproject';

	/**
	 * 
	 * @var string
	 */
	protected $url = '';

	/**
	 * 
	 * @param string $url
	 * @return \Portail\Model\Component\Project
	 */
	public function setUrl(string $url)
	{
		$this->url = $url;
		return $this;
	}

	/**
	 * 
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Portail\Model\Component\AbstractComponent::hydrate()
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['url'])) ? $this->setUrl($properties['url']) : null;
		return $this;
	}
}
