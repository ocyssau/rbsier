<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;

/**
 * @property string $notice
 * @property array $items
 * 
 *
 */
class Checkbox extends AbstractComponent
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcompcheckbox';

	/**
	 */
	public function __construct($properties = null)
	{
		parent::__construct($properties);
		$this->attributes['items'] = [];
	}

	/**
	 * @param Checkbox $obj
	 * @param array $properties
	 * @return Checkbox
	 */
	public static function init($obj = null, array $properties = [])
	{
		if ( $obj && !($obj instanceof Filelist) ) {
			throw new Exception('Cant init other thing than Filelist');
		}
		$obj = parent::init($obj, $properties);
		return $obj;
	}

	/**
	 * @param array $array
	 * @return Jalon
	 */
	public function setItems(array $array)
	{
		$this->attributes['items'] = $array;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getItems()
	{
		return $this->getAttribute('items');
	}
}
