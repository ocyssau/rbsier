<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component\Checkbox;

/**
 * 
 * @author ocyssau
 *
 */
class Item
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcompcheckboxitem';

	/**
	 * 
	 * @param string $name
	 * @param string $checked
	 * @param string $notice
	 */
	public function __construct($name, $checked, $notice)
	{
		$this->name = $name;
		$this->checked = (boolean)$checked;
		$this->notice = $notice;
	}
}
