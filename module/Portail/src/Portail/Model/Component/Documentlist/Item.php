<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component\Documentlist;

/**
 * @brief link between document
 *
 */
class Item extends \Rbplm\Link
{

	public static $classId = 'pcompdoclistitem';

	/**
	 * datas set to this item
	 *
	 * @var array
	 */
	protected $datas = [];

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Item
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['datas']) && is_array($properties['datas'])) ? $this->datas = $properties['datas'] : null;
		return $this;
	}

	/**
	 * 
	 * @param array $datas
	 * @return Item
	 */
	public function setDatas(array $datas)
	{
		$this->datas = $datas;
		return $this;
	}

	/**
	 * 
	 * @return array
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 *
	 * @param array $datas
	 * @return Item
	 */
	public function addData($data)
	{
		$this->datas[] = $data;
		return $this;
	}
}
