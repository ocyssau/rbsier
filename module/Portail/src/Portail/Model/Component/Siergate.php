<?php
namespace Portail\Model\Component;

/**
 * 
 *
 */
class Portail
{

	/**
	 * @var integer
	 */
	public static $classId = 10;

	public static $id = 'Portail';

	public static $version = '0.3';

	public static $build = '07mai2015';

	public static function getCopyright()
	{
		$copyright = '&copy; 2015 - ' . date('Y') . ' By Olivier CYSSAU pour SIER - All rights reserved.';
		return $copyright;
	}

	/**
	 * 
	 * @return string
	 */
	public static function getVersion()
	{
		return self::$version;
	}

	/**
	 * 
	 * @return string
	 */
	public static function getId()
	{
		return self::$id;
	}
}
