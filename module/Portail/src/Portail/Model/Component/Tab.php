<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;

/**
 * 
 * @property string $notice
 * @property string $label
 *
 */
class Tab extends AbstractComponent
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcomptab';
}
