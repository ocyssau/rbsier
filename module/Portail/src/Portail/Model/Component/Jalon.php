<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;

/**
 * 
 * TargetDate is recorded as a simple string
 * @property string $targetdate
 *
 */
class Jalon extends Statusbox
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcompjalon';
}
