<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component\Tabpanel;

/**
 * 
 *
 */
class Item
{

	/**
	 * @var integer
	 */
	public static $classId = 'pcomptabpanelitem';
	
	/**
	 * 
	 * @param integer $index
	 * @param string $notice
	 * @param string $label
	 */
	public function __construct($index, $notice, $label)
	{
		$this->index = $index;
		$this->notice = $notice;
		$this->label = $label;
	}
}
