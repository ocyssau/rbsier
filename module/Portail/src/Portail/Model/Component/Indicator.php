<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;

/**
 * @property string $notice
 * @property string $label
 * @property integer $value
 * 
 */
class Indicator extends AbstractComponent
{


	/**
	 * @var integer
	 */
	public static $classId = 'pcompindicator';

}
