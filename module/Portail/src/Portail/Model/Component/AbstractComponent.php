<?php
//%LICENCE_HEADER%
namespace Portail\Model\Component;

use Portail\Renderer;
use Rbplm\Any;
use Rbplm\People;
use DateTime;
use Rbplm\Item as TreeItem;
use Rbplm\ItemInterface;
use Rbplm\Mapped as Permanent;
use Rbplm\Dao\MappedInterface as PermanentInterface;
use Rbplm\Owned;
use Rbplm\LifeControl;

#public function setWidth($string)
#public function setBody($string)
#public function getBody()
#public function setHeight($string)
#public function setXoffset($string)
#public function setYoffset($string)
#public function setZindex($int)
#public function getWidth()
#public function getXoffset()
#public function getYoffset()
#public function getZindex()
#public function getHeight()

/**
 * Attributes :
 * 
 * @property string $body
 * @property string $xoffset
 * @property string $yoffset
 * @property string $width
 * @property string $height
 * @property int $zindex
 * @property bool $resizable
 * @property bool $resizablex
 * @property bool $resizabley
 * @property bool $draggable
 * @property bool $collapsed
 */
abstract class AbstractComponent extends Any implements PermanentInterface, ItemInterface
{

	static $classId = 'pcompabstract';

	/* this objects are owned by someone */
	use Owned;

	/* this objects may be store in db */
	use Permanent;

	/* this objects is in tree */
	use TreeItem;

	/* this object is under life control */
	use LifeControl;

	/**
	 * @var string
	 */
	protected $index = 1;

	/**
	 * @var boolean
	 */
	protected $visibility = 'private';

	/**
	 * @var string
	 */
	protected $title = '{empty}';

	/**
	 * @var Renderer\AbstractRenderer
	 */
	protected $renderer;

	/**
	 *
	 * @var string
	 */
	protected $internalref;

	/**
	 * 
	 * @var array
	 */
	protected $attributes = [
		'body' => '',
		'xoffset' => '',
		'yoffset' => '',
		'width' => '100%',
		'height' => '',
		'zindex' => 100,
		'resizable' => false,
		'resizablex' => false,
		'resizabley' => false,
		'draggable' => false,
		'collapsed' => false
	];

	/**
	 */
	public function __construct($properties = null)
	{
		$this->cid = static::$classId;
		if ( $properties ) {
			$this->hydrate($properties);
		}
	}

	/**
	 */
	public function __get($name)
	{
		return $this->getAttribute($name);
	}

	/**
	 */
	public function __set($name, $value)
	{
		return $this->setAttribute($name, $value);
	}

	/**
	 * 
	 * @param AbstractComponent $obj
	 * @param array $properties
	 * @return AbstractComponent
	 */
	public static function init($obj = null, array $properties = [])
	{
		if ( !$obj ) {
			$class = get_called_class();
			$obj = new $class();
		}

		$obj->newUid();
		$obj->ownerId = People\User::SUPER_USER_ID;
		$obj->createById = People\User::SUPER_USER_ID;
		$obj->updateById = People\User::SUPER_USER_ID;
		$obj->updated = new DateTime();
		$obj->created = new DateTime();

		(!isset($properties['name'])) ? $properties['name'] = uniqid() : null;
		(!isset($properties['owner'])) ? $properties['owner'] = People\CurrentUser::get() : null;

		$obj->hydrate($properties);

		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return AbstractComponent
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['title'])) ? $this->setTitle($properties['title']) : null;
		
		$this->ownedHydrate($properties);
		$this->mappedHydrate($properties);
		$this->itemHydrate($properties);
		$this->lifeControlHydrate($properties);

		(isset($properties['index'])) ? $this->index = $properties['index'] : null;
		(isset($properties['visibility'])) ? $this->visibility = $properties['visibility'] : null;
		(isset($properties['internalref'])) ? $this->internalref = $properties['internalref'] : null;
		
		if ( isset($properties['attributes']) && is_array($properties['attributes']) ) {
			foreach( $properties['attributes'] as $name => $value ) {
				$this->setAttribute($name, $value);
			}
		}
		return $this;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return AbstractComponent
	 */
	public function setAttribute($name, $value)
	{
		if ( $value == 'true' ) $value = true;
		elseif ( $value == 'false' ) $value = false;
		else $value = $value; /* @todo; filter user input */
		$this->attributes[$name] = $value;
		return $this;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function getAttribute($name)
	{
		return (isset($this->attributes[$name])) ? $this->attributes[$name] : null;
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}
	
	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isResizable($bool = null)
	{
		if ( is_null($bool) ) {
			return isset($this->attributes['resizable']) ? $this->attributes['resizable'] : false;
		}
		$this->attributes['resizable'] = (boolean)$bool;
		return $this;
	}

	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isResizablex($bool = null)
	{
		if ( is_null($bool) ) {
			return isset($this->attributes['resizablex']) ? $this->attributes['resizablex'] : false;
		}
		$this->attributes['resizablex'] = (boolean)$bool;
		return $this;
	}

	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isResizabley($bool = null)
	{
		if ( is_null($bool) ) {
			return isset($this->attributes['resizabley']) ? $this->attributes['resizabley'] : false;
		}
		$this->attributes['resizabley'] = (boolean)$bool;
		return $this;
	}

	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isDraggable($bool = null)
	{
		if ( is_null($bool) ) {
			return isset($this->attributes['draggable']) ? $this->attributes['draggable'] : false;
		}
		$this->attributes['draggable'] = (boolean)$bool;
		return $this;
	}

	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isCollapsed($bool = null)
	{
		if ( is_null($bool) ) {
			return isset($this->attributes['collapsed']) ? $this->attributes['collapsed'] : false;
		}
		$this->attributes['collapsed'] = (boolean)$bool;
		return $this;
	}

	/**
	 * @param boolean $bool
	 * @return AbstractComponent | boolean
	 */
	public function isSaved($bool = null)
	{
		if ( is_null($bool) ) {
			return $this->saved;
		}
		$this->saved = (boolean)$bool;
		return $this;
	}

	/**
	 * @param string $string
	 * @return AbstractComponent
	 */
	public function setTitle($string)
	{
		$this->title = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param integer $int
	 * @return AbstractComponent
	 */
	public function setIndex($int)
	{
		$this->index = $int;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getIndex()
	{
		return $this->index;
	}

	/**
	 * @param string $string
	 * @return AbstractComponent
	 */
	public function setVisibility($string)
	{
		$this->visibility = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getVisibility()
	{
		return $this->visibility;
	}

	/**
	 * @param string $string
	 */
	public function setInternalref($string)
	{
		$this->internalref = $string;
		return $this;
	}

	/**
	 * $return string
	 */
	public function getInternalref()
	{
		return $this->internalref;
	}

	/**
	 * @return Renderer\AbstractRenderer
	 */
	public function getRenderer()
	{
		if ( !isset($this->renderer) ) {
			$this->renderer = Renderer\Factory::get($this);
		}
		return $this->renderer;
	}
}
