<?php
namespace Portail\Form\Project;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 * 
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @param \RBs\Space\Factory $factory
	 * @param string $nameSpace
	 */
	public function __construct($factory, $nameSpace)
	{
		parent::__construct($factory, $nameSpace);

		$this->template = 'portail/index/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Magic */
		$this->add(array(
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control',
				'title' => 'Search in name, number, designation, File extension'
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_magic',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$trans = $this->daoFactory->getDao(\Portail\Model\Component\Project::$classId)->getTranslator();
		$this->prepare()->isValid();
		$datas = $this->getData();

		$filter->andfind(\Portail\Model\Component\Project::$classId, $trans->toSys('cid'), Op::EQUAL);

		/* MAGIC */
		if ( $datas['find_magic'] ) {
			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, `%s`, `%s`, '-')", $trans->toSys('name'), $trans->toSys('description'), $trans->toSys('internalref'), $trans->toSys('attributes'));
			$filter->andFind($datas['find_magic'], $concat, Op::OP_CONTAINS);
		}

		return $this;
	}
}
