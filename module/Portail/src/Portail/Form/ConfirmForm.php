<?php
namespace Portail\Form;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class ConfirmForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 * Array of objects to confirm
	 * @var array
	 */
	public $toConfirm = [];

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 * @param \Application\View\ViewModel $view
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('confirmation');

		$this->template = 'portail/project/confirmform';
		$this->toConfirm = [];

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Submit button */
		$this->add([
			'name' => 'validate',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			]
		]);

		/* Cancel button */
		$this->add([
			'name' => 'cancel',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			]
		]);
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'checked' => [
				'required' => false
			]
		);
	}

	/**
	 * @param \Rbplm\Any $obj
	 */
	public function addToConfirm($obj)
	{
		$label = $obj->getName();
		$id = $obj->getId();
		$this->toConfirm[] = $obj;

		/* recap list with checkbox */
		$this->add([
			'name' => 'checked[' . $id . ']',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => [
				'class' => 'form-control',
				'checked' => true
			],
			'options' => [
				'label' => $label,
				'use_hidden_element' => false,
				'checked_value' => $id
			]
		]);
	}
}
