<?php
namespace Portail\Form;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Rbs\Model\Hydrator\Extendable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author olivier
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{
	use \Application\Form\ExtendableTrait;

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('portailEdit');

		$this->template = 'portail/project/editform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add([
			'name' => 'id',
			'attributes' => [
				'type' => 'hidden'
			]
		]);

		/* Name */
		$this->add([
			'name' => 'name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => [
				'placeholder' => '',
				'class' => 'form-control rbp-input-name'
			],
			'options' => [
				'label' => tra('Name')
			]
		]);

		/* Title */
		$this->add([
			'name' => 'title',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => [
				'placeholder' => '',
				'class' => 'form-control rbp-input-title'
			],
			'options' => [
				'label' => tra('Title')
			]
		]);
		
		/* Url */
		$this->add([
			'name' => 'url',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => [
				'placeholder' => '',
				'class' => 'form-control rbp-input-url',
				'size' => 80
			],
			'options' => [
				'label' => tra('Public Url')
			]
		]);

		/* Submit button */
		$this->add([
			'name' => 'validate',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			]
		]);

		/* Cancel button */
		$this->add([
			'name' => 'cancel',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			]
		]);
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'Int'
					]
				]
			],
			'title' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'StripTags'
					],
					[
						'name' => 'StringTrim'
					]
				],
				'validators' => [
					[
						'name' => 'StringLength',
						'options' => [
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 1024
						]
					]
				]
			],
			'name' => [
				'required' => true,
				'filters' => [
					[
						'name' => 'StripTags'
					],
					[
						'name' => 'StringTrim'
					]
				],
				'validators' => [
					[
						'name' => 'StringLength',
						'options' => [
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 255
						]
					]
				]
			],
			'url' => [
				'required' => true,
				'filters' => [
					[
						'name' => 'StripTags'
					],
					[
						'name' => 'StringTrim'
					],
					[
						'name' => 'StringToLower'
					],
					[
						'name' => 'Alpha',
						'options' => [
							'allowWhiteSpace' => false,
							'locale' => 'en_US',
						]
					],
					[
						'name' => 'Callback',
						'options' => [
							'callback' => [\Rbs\Number::class, 'normalize'],
						]
					],
				],
				'validators' => [
					[
						'name' => 'Uri',
						'options' => [
							'allowRelative' => true,
						]
					]
				]
			]
		);
	}

	/**
	 *
	 * @param string $mode
	 */
	public function setMode($mode = 'edit')
	{
		if ( $mode == 'edit' ) {
			$this->remove('number');
			$this->remove('name');
		}
	}
}
