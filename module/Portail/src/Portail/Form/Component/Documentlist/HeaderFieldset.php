<?php
namespace Portail\Form\Component\Documentlist;

use Zend\Form\Fieldset;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class HeaderFieldset extends Fieldset implements InputFilterProviderInterface
{

	static $columns = [];
	
	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct('headerfieldset');

		$this->setHydrator(new Hydrator(false));

		/**/
		$this->add([
			'name' => 'column',
			'type' => \Zend\Form\Element\Select::class,
			'attributes' => [
				'class' => 'form-control rb-select rb-select-column'
			],
			'options' => [
				'label' => 'Column',
				'value_options' => self::$columns
			]
		]);

		/**/
		$this->add([
			'name' => 'label',
			'type' => \Zend\Form\Element\Text::class,
			'attributes' => [
				'class' => 'form-control rb-input-label'
			],
			'options' => [
				'label' => 'Label'
			]
		]);
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'column' => [
				'required' => true
			],
			'label' => [
				'required' => false
			]
		);
	}

	/**
	 *
	 * @return array
	 */
	public static function populateColumn(\Rbs\Space\Factory $factory)
	{
		/* Load extended properties */
		$dao = $factory->getDao(\Rbplm\Ged\Document\Version::$classId);
		$loader = new \Rbs\Extended\Loader($factory);
		$loader->load(\Rbplm\Ged\Document\Version::$classId, $dao);
		$dao->loader = $loader;
		
		$s = [];
		foreach ($dao->metaModel as $asApp){
			$s[$asApp] = $asApp;
		}
		asort($s);
		self::$columns = $s;
		#$this->get('column')->setOption('value_options', $selectSet);
	}
} /* End of class */
