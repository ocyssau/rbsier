<?php
namespace Portail\Form\Component\Documentlist;

use Application\Form\AbstractFilterForm;
use Portail\Model\Component\Documentlist;

/**
 * 
 *
 */
abstract class PublicFilterAbtract extends AbstractFilterForm implements \Portail\Model\FilterFormInterface
{
	/**
	 *
	 * @var \Rbplm\Dao\DaoListAbstract
	 */
	protected $searchEngine;
	
	/**
	 *
	 * @var Documentlist
	 */
	protected $component;
	
	/**
	 * @param \Rbs\Space\Factory $factory
	 * @param integer $containerId
	 */
	public function __construct($factory = null, Documentlist $component)
	{
		parent::__construct($factory, $component->getUid());
		$this->component = $component;
		$this->template = 'component/form/documentlist/filterform.phtml';
	}
	
	/**
	 * @param \Rbplm\Dao\DaoListAbstract $engine
	 * @return \Portail\Form\Component\Documentlist\BuilderForm
	 */
	public function setSearchEngine(\Rbplm\Dao\DaoListAbstract $engine)
	{
		$this->searchEngine = $engine;
		return $this;
	}
	
	/**
	 * @return \Rbplm\Dao\DaoListAbstract $engine
	 */
	public function getSearchEngine()
	{
		return $this->searchEngine;
	}
	
}

