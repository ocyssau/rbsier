<?php
namespace Portail\Form\Component\Documentlist;

use Rbplm\Dao\Filter\Op;

#use Rbs\Dao\Sier\Filter as DaoFilter;
use Portail\Model\Component\Documentlist;

/**
 */
class UserFilterForm extends PublicFilterAbtract
{

	/**
	 * @param \Rbs\Space\Factory $factory
	 * @param integer $containerId
	 */
	public function __construct($factory = null, Documentlist $component)
	{
		parent::__construct($factory, $component);
		$elemtFactory = $this->getElementFactory();

		/* Spacename */
		$this->add(array(
			'name' => 'spacename',
			'type' => \Zend\Form\Element\Hidden::class,
			'attributes' => [
				'value' => $factory->getName()
			]
		));

		/* Magic */
		$elemtFactory->magicElement();

		/* Number */
		$elemtFactory->numberElement();

		/* Designation */
		$elemtFactory->designationElement();
	}

	/**
	 *
	 * {@inheritdoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();
		$alias = null;
		$trans = $this->daoFactory->getDao(\Rbplm\Ged\Document\Version::$classId)->getTranslator();

		// NUMBER
		if ( !empty($datas['find_number']) ) {
			$what = $datas['find_number'];
			$what = str_replace('  ', ' ', $what);

			if ( stristr($what, '*') ) {
				$what = str_replace('*', '%', $what);
			}
			else {
				$what = '%' . $what . '%';
			}

			$filter->andFind($what, $alias . $trans->toSys('number'), Op::LIKE);
		}

		// DESIGNATION
		if ( !empty($datas['find_designation']) ) {
			$what = $datas['find_designation'];
			$what = str_replace('  ', ' ', $what);

			if ( stristr($what, '*') ) {
				$what = str_replace('*', '%', $what);
			}
			else {
				$what = '%' . $what . '%';
			}

			$filter->andFind($what, $alias . $trans->toSys('description'), Op::LIKE);
		}

		// MAGIC
		if ( !empty($datas['find_magic']) ) {
			$what = $datas['find_magic'];
			$what = str_replace('  ', ' ', $what);
			$what = str_replace('*', '%', $what);
			$whats = explode(' AND ', $what);

			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, `%s`, `%s`, '-')", $trans->toSys('id'), $trans->toSys('name'), $trans->toSys('description'), $trans->toSys('number'));
			foreach( $whats as $what ) {
				$filter->andFind($what, $concat, Op::LIKE);
			}
		}

		return $this;
	}
}
