<?php
namespace Portail\Form\Component\Documentlist;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

#use Portail\Form\Component\Documentlist\HeaderFieldset;

/**
 *
 *
 */
class BuilderForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 * @var \Rbplm\Dao\DaoListAbstract
	 */
	private $searchEngine;

	/**
	 *
	 */
	public function __construct(\Rbs\Space\Factory $factory)
	{
		parent::__construct('headerselectorform');

		HeaderFieldset::populateColumn($factory);

		$this->template = 'portail/component/form/documentlist/builderform';
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/**/
		$this->add([
			'type' => \Zend\Form\Element\Collection::class,
			'name' => 'headers',
			'options' => [
				'label' => 'Select headers',
				'count' => 1, /* ensure than at least one element is set */
				'should_create_template' => true,
				'allow_add' => true,
				'target_element' => [
					'type' => HeaderFieldset::class
				]
			]
		]);

		/* Filter Form select */
		$this->add([
			'name' => 'filterform',
			'type' => \Zend\Form\Element\Select::class,
			'attributes' => [
				'class' => 'form-control selectpicker rb-select rb-select-filterform',
				'multiple' => false,
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1'
			],
			'options' => [
				'label' => tra('Filters Forms')
			]
		]);

		/* Template select */
		$this->add([
			'name' => 'userTemplate',
			'type' => \Zend\Form\Element\Select::class,
			'attributes' => [
				'class' => 'form-control selectpicker rb-select rb-select-template',
				'multiple' => false,
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1'
			],
			'options' => [
				'label' => tra('Templates')
			]
		]);

		/* Actions Enable */
		$this->add([
			'name' => 'actions',
			'type' => \Zend\Form\Element\Select::class,
			'attributes' => [
				'class' => 'form-control selectpicker rb-select rb-select-action',
				'multiple' => true,
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1'
			],
			'options' => [
				'label' => tra('Actions'),
				'options' => self::getActions()
			]
		]);

		/* Config editor */
		$this->add(array(
			'name' => 'userConfig',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '{"name1":"value1", "name2":"value2"}',
				'class' => 'form-control rb-user-config'
			),
			'options' => array(
				'label' => tra('Config Editor')
			)
		));

		/* Submit button */
		$this->add([
			'name' => 'validate',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			]
		]);

		/* Cancel button */
		$this->add([
			'name' => 'cancel',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			]
		]);
	}

	/**
	 * Populate filterform selector
	 * @param array $forms
	 * @return \Portail\Form\Component\Documentlist\BuilderForm
	 */
	public function setFilterforms(array $list)
	{
		$list = array_merge([
			'' => null
		], $list);
		$this->get('filterform')->setValueOptions($list);
		return $this;
	}

	/**
	 * Populate template selector
	 * @param array $forms
	 * @return \Portail\Form\Component\Documentlist\BuilderForm
	 */
	public function setTemplates(array $list)
	{
		$this->get('userTemplate')->setValueOptions($list);
		return $this;
	}

	/**
	 * @return array
	 */
	public static function getActions()
	{
		return [
			'download' => 'Download',
			'download-pdf' => 'Download As Pdf',
			'checkin-checkout' => 'Checkin/Checkout',
			'workflow' => 'workflow',
			'details' => 'Display Details'
		];
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return [
			'filterform' => [
				'required' => true
			],
			'userTemplate' => [
				'required' => false
			],
			'userConfig' => [
				'required' => false
			],
			'actions' => [
				'required' => false
			]
		];
	}
} /* End of class */
