<?php
/**
 *
 */
return array(
	'router' => [
		'routes' => [
			'portail' => [
				'type' => 'Segment',
				'options' => [
					'route' => '/portail/index/index',
					'defaults' => [
						'controller' => 'Portail\Controller\Index',
						'action' => 'index'
					]
				]
			],
			'portail-project' => [
				'type' => 'Segment',
				'options' => [
					'route' => '/portail/project[/:action[/][:id]]',
					'defaults' => [
						'controller' => 'Portail\Controller\Manager',
						'action' => 'index'
					]
				]
			],
			'portail-project-build' => [
				'type' => 'Segment',
				'options' => [
					'route' => '/portail/project/build/:id',
					'defaults' => [
						'controller' => 'Portail\Controller\Builder',
						'action' => 'index'
					]
				]
			],
			'component' => [
				'type' => 'Segment',
				'options' => [
					'route' => '/portail/component[/:action][/][:id]',
					'defaults' => [
						'controller' => 'Portail\Controller\Component',
						'action' => 'index'
					]
				]
			],
			'portail-public' => [
				'type' => 'Segment',
				'options' => [
					'route' => '/portail/:name',
					'defaults' => [
						'controller' => 'Portail\Controller\Public',
						'action' => 'index'
					]
				]
			]
		]
	],
	'controllers' => [
		'invokables' => [
			'Portail\Controller\Index' => 'Portail\Controller\IndexController',
			'Portail\Controller\Manager' => 'Portail\Controller\ManagerController',
			'Portail\Controller\Builder' => 'Portail\Controller\BuilderController',
			'Portail\Controller\Component' => 'Portail\Controller\ComponentController',
			'Portail\Controller\Public' => 'Portail\Controller\PublicController'
		]
	],
	'view_manager' => [
		'template_path_stack' => [
			'Portail' => __DIR__ . '/../view'
		],
		'template_map' => [
			'module_layouts' => [
				'Portail' => 'portail/layout/builder',
				'PortailPublic' => 'portail/layout/public'
			],
			'portail/layout/builder' => __DIR__ . '/../view/layout/builder.phtml',
			'portail/layout/public' => __DIR__ . '/../view/layout/public.phtml'
		]
	],
	'portail' => [
		'userfilterforms' => [
			\Portail\Form\Component\Documentlist\UserFilterForm::class => 'Standard Filter',
			\Portail\Form\Component\Documentlist\PublishedFilterForm::class => 'Published Only'
		],
		'templates' => [
			\Portail\Model\Component\Documentlist::class => [
				'component/public/documentlist/bycolumns' => 'By Columns',
				'component/public/documentlist/byboxes' => 'By Boxes'
			],
		],
		'di' => [
			\Portail\Form\Component\Documentlist\UserFilterForm::class => [
				'searchEngine' => \Portail\Dao\Component\Documentlist\SearchEngineDao::class
			],
		],
		'image_upload_path' => 'data/upload/portail/img',
		'image_upload_url' => 'service/portail/file/upload',
		'image_download_url' => 'service/portail/file/upload',
	],
	'contextual_menu' => [
		'document' => [
			'share' => [
				'menu' => 'Share',
				'class' => 'submenu',
				'items' => [
					'portail-publish' => [
						'name' => 'portail-publish',
						'assert' => 'multiSelect==false||multiSelect==true',
						'class' => 'publish-btn',
						'url' => '#',
						'title' => 'Publish this document to selected portail',
						'label' => 'Publish On Portail'
					]
				]
			]
		]
	],
	'menu' => [
		'admin' => [
			'portail' => [
				'name' => 'portail',
				'route' => [
					'params' => [
						'action' => 'index'
					],
					'options' => [
						'name' => 'portail'
					]
				],
				'class' => '%anchor%',
				'label' => 'Portails'
			]
		]
	]
);

