<?php
namespace Import\Form\Job;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 * @var string
	 */
	public $template;

	/**
	 * @var array
	 */
	public $packages = array();

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view, $factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('jobEdit');
		$this->template = 'import/job/editform';
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Set A Convenient Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'name' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z0-9-_]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, numbers, space, and -,_'
							)
						)
					)
				)
			)
		);
	}

	/**
	 *
	 */
	public function addPackage($package)
	{
		$id = $package->getId();
		$this->packages[] = $package;

		/* Add hidden fields */
		$this->add(array(
			'name' => "checked[$id]",
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		$this->count++;
	}
}
