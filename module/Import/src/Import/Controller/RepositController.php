namespace Import\Controller; use
Application\Controller\FilemanagerController; use Rbplm\Sys\Filesystem;
use Rbplm\People; use Rbs\Space\Factory as DaoFactory; /** * * */ class
RepositController extends FilemanagerController { /* @var string */
public $pageId = 'package_reposit'; /* @var string */ public
$defaultSuccessForward = 'import/reposit/index'; /* @var string */
public $defaultFailedForward = 'import/reposit/index'; /** * Run
controller */ public function init() { parent::init(); /* Record url for
page and Active the tab */ $tabs =
\Application\View\Menu\MainTabBar::get($this->view);
$this->layout()->tabs = $tabs; $this->ifSuccessForward =
$this->defaultSuccessForward; $this->ifFailedForward =
$this->defaultFailedForward; $this->basecamp()->setForward($this); } /**
* * {@inheritDoc} * @see
\Application\Controller\FilemanagerController::getDirectory() */ public
function getDirectory(People\User $user) { /** @var ServiceManager $sm
*/ $sm = $this->getEvent()->getApplication()->getServiceManager();
$config = $sm->get('Configuration'); $path =
$config['rbp']['path.reposit.importpackage']; $reposit = new
\Import\Model\Package\Reposit($path); $reposit->init(); return $reposit;
} /** * Read files in reposit, create package and display * * @param
string $template * @return string|\Application\View\ViewModel */ public
function indexAction($template = null) { $view = $this->view;
$currentUser = People\CurrentUser::get(); $factory = DaoFactory::get();
$reposit = $this->getDirectory($currentUser); ($template == null) ?
$template = 'import/reposit/index.phtml' : null;
$this->layoutSelector()->clear($this);
$this->basecamp()->save($this->defaultSuccessForward,
$this->defaultFailedForward, $view); $filter = new Filesystem\Filter('',
false); $filter->displayMd5 = false; $filterForm = new
\Import\Form\Reposit\FilterForm($factory, $this->pageId);
$filterForm->load(); $filterForm->bindToFilter($filter)->save();

$paginator = new \Application\Form\PaginatorForm($this->pageId); /*
Default order by */ $paginator->get('orderby')->setValue('name');
$paginator->limit = 50; $paginator->setMaxLimit(1000);
$paginator->load(); $paginator->prepare() ->bindToView($view)
->bindToFilter($filter) ->save(); /* get packages in reposit directories
*/ $path = $reposit->getPath(); $list = $reposit::getDatas($path,
$filter); /* Get Free Disk Space On Package Reposit Directory */ $t =
null; $execOut = null; exec('df -k ' . $path, $execOut); $t = explode('
', trim(preg_replace("/\s+/", " ", $execOut[1]))); $freeImportDiskSpace
= $t[2]; $view->freeMockupDiskSpace = 0; $view->freeImportDiskSpace =
$freeImportDiskSpace * 1024; $view->list = $list; $view->filter =
$filterForm; $view->pageTitle = tra('Package Reposit');
$view->repositPath = $path; $view->displayMd5 = $filter->displayMd5;

return $view; } }
