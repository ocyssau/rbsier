<?php
namespace Import\Controller;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Dao\Sier\Filter;
use Rbplm\Dao\Filter\Op;
use Rbs\Batch\Job;
use Rbplm\Sys\Datatype\Package;
use Rbplm\Org\Mockup;
use DateTime;
use Exception;

/**
 *
 *
 */
class JobController extends \Batch\Controller\JobController
{

	/** @var string */
	public $pageId = 'batch_index';

	/** @var string */
	public $defaultSuccessForward = 'import/job/index';

	/** @var string */
	public $defaultFailedForward = 'import/job/index';

	/**
	 */
	public function indexAction()
	{
		$request = $this->getRequest();
		$view = $this->view;
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Job::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Batch\Form\Job\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'batchfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$filter->andfind('import.%', $dao->toSys('name'), Op::LIKE);

		$list = $factory->getList(Job::$classId);
		foreach( $dao->metaModel as $asSys => $asApp ) {
			if ( $asApp == 'log' ) continue;
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);

		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Batch Jobs';
		return $view;
	}

	/**
	 *
	 * @return void|string|\Application\View\ViewModel
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
			$spacename = $request->getQuery('spacename', 'Mockup');
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
			$spacename = $request->getPost('spacename', null);
			$packageIds = $request->getPost('checked', []);
			$data = $request->getPost('data', null);
		}

		/**/
		$factory = DaoFactory::get($spacename);
		$job = Job::init('job');
		$dao = $factory->getDao(Job::$classId);

		/**/
		$form = new \Import\Form\Job\EditForm($view, $factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($job);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$job->hash();
				$dao->save($job);
				return $this->successForward();
			}
		}

		/**/
		$view->pageTitle = tra('Create Job');
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;

		/* Create the job */
		if ( $data ) {
			$data = json_decode($data);
			$cleanData = [];
			foreach( $data as $map ) {
				$packageId = $map[0];
				$targetId = $map[1];
				if ( $packageId && $targetId ) {
					$cleanData[] = $map;
				}
			}
			$data = $cleanData;
			if ( !$data ) {
				throw new Exception('Nothing to put in batch');
			}

			try {
				$batchJob = Job::init();
				$batchJob->setData($data);
				$batchJob->setCallback();
				$batchJob->setPlanned(new DateTime());
				$dao = $factory->getDao(Job::$classId);
				$dao->save($batchJob);
			}
			catch( \Exception $e ) {
				throw new Exception('Creation of job has failed. Check if job is not yet existing?');
			}

			/* attach job to package */
			$pdao = $factory->getDao(Package::$classId);
			foreach( $data as $map ) {
				$packageId = $map[0];
				$targetId = $map[1];
				try {
					$package = $pdao->loadFromId(new Package(), $packageId);
					$package->setJob($batchJob);
					$pdao->save($package);
				}
				catch( \Exception $e ) {
					throw new Exception(sprintf('unable to link job %s to package %s', $batchJob->getId(), $packageId));
				}
			}
		}
		/* Display creation screen */
		else {
			/* Get list of selected packages */
			$dao = $factory->getDao(Package::$classId);
			foreach( $packageIds as $packageId ) {
				$package = new Package();
				$dao->loadFromId($package, $packageId);
				$form->addPackage($package);
			}

			/* Get list of mockup */
			$dao = $factory->getDao(Mockup::$classId);
			$list = $factory->getList(Mockup::$classId);

			$filter = new Filter('', false);
			$select = array(
				$dao->toSys('id') . ' AS id',
				$dao->toSys('number') . ' AS number',
				$dao->toSys('description') . ' AS description'
			);
			$filter->select($select);
			$list->load($filter);
			$view->mockups = $list->toArray();
			$view->list = $list;
		}

		return $view;
	}
} /* End of class */
