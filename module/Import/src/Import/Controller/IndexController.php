<?php
namespace Import\Controller;

use Application\Controller\AbstractController;

/**
 */
class IndexController extends AbstractController
{

	/* @var string */
	public $pageId = 'import_index';

	/* @var string */
	public $defaultSuccessForward = 'import/index/index';

	/* @var string */
	public $defaultFailedForward = 'import/index/index';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
	}

	/**
	 */
	public function indexAction($template = null)
	{
		$view = $this->view;
		return $view;
	}
}
