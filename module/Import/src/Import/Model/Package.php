<?php
//%LICENCE_HEADER%
namespace Import\Model;

use Rbplm\AnyPermanent;
use Rbplm\People;

/**
 * A package is a record of a archive file to import or imported
 * Is set when the archive is involved in batchjob
 *
 */
class Package extends AnyPermanent
{

	/**
	 *
	 * @var string
	 */
	static $classId = 'package94f736';

	/**
	 * @var string
	 */
	protected $number;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var \Rbplm\Sys\Datatype\File
	 */
	protected $data;

	/**
	 * @var \Rbs\Batch\Job
	 */
	protected $batch;

	/**
	 * @var integer
	 */
	public $batchId;

	public $batchUid;

	/**
	 * @var string
	 */
	protected $lifeStage = 'init';

	/**
	 * @var string
	 */
	protected $contentListfile;

	/**
	 * @return Package
	 */
	public static function init($name = null, $parent = null)
	{
		$obj = parent::init($name, $parent);
		$currentUser = People\CurrentUser::get();
		$obj->created = new \DateTime();
		$obj->updated = new \DateTime();
		$obj->setCreateBy($currentUser);
		$obj->setUpdateBy($currentUser);
		$obj->setOwner($currentUser);
		$obj->number = $obj->getUid();
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Package
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);

		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['lifestage'])) ? $this->lifestage = $properties['lifestage'] : null;
		(isset($properties['path'])) ? $this->path = $properties['path'] : null;
		(isset($properties['batchId'])) ? $this->batchId = $properties['batchId'] : null;
		(isset($properties['batchUid'])) ? $this->batchUid = $properties['batchUid'] : null;
		(isset($properties['contentListfile'])) ? $this->contentListfile = $properties['contentListfile'] : null;
		(isset($properties['errorLogfile'])) ? $this->errorLogfile = $properties['errorLogfile'] : null;
		(isset($properties['feedbackLogfile'])) ? $this->feedbackLogfile = $properties['feedbackLogfile'] : null;
		(isset($properties['importedLogfile'])) ? $this->importedLogfile = $properties['importedLogfile'] : null;
		(isset($properties['path'])) ? $this->path = $properties['path'] : null;
		(isset($properties['rootname'])) ? $this->rootname = $properties['rootname'] : null;
		(isset($properties['extension'])) ? $this->extension = $properties['extension'] : null;
		(isset($properties['type'])) ? $this->type = $properties['type'] : null;
		(isset($properties['size'])) ? $this->size = $properties['size'] : null;
		(isset($properties['mtime'])) ? $this->mtime = $properties['mtime'] : null;
		(isset($properties['md5'])) ? $this->md5 = $properties['md5'] : null;
		return $this;
	}

	/**
	 * @return Package
	 *
	 */
	public function setContentListfile($fileName)
	{
		$this->contentListfile = $fileName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getContentListfile()
	{
		return $this->contentListfile;
	}

	/**
	 * @return Package
	 */
	public function setFeedbackLogfile($fileName)
	{
		$this->feedbackLogfile = $fileName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFeedbackLogfile()
	{
		return $this->feedbackLogfile;
	}

	/**
	 * @return Package
	 *
	 */
	public function setErrorLogfile($fileName)
	{
		$this->errorLogfile = $fileName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getErrorLogfile()
	{
		return $this->errorLogfile;
	}

	/**
	 * @return Package
	 *
	 */
	public function setImportedLogfile($fileName)
	{
		$this->importedLogfile = $fileName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getImportedLogfile()
	{
		return $this->importedLogfile;
	}

	/**
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 * @param string $number
	 * @return Package
	 */
	public function setNumber($number)
	{
		$this->number = $number;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLifeStage()
	{
		return $this->lifeStage;
	}

	/**
	 * @param string $lifeStage
	 * @return Package
	 */
	public function setLifeStage($lifeStage)
	{
		$this->lifeStage = $lifeStage;
		return $this;
	}

	/**
	 * @param \Rbs\Batch\Job $batch
	 * @return Package
	 */
	public function setBatch($batch)
	{
		$this->batch = $batch;
		$this->batchUid = $batch->getUid();
		$this->batchId = $batch->getId();
		return $this;
	}

	/**
	 * @return \Rbs\Batch\Job
	 */
	public function getBatch($asId = false)
	{
		if ( $asId === true ) {
			return $this->batchId;
		}
		else {
			return $this->batch;
		}
	}

	/**
	 * @param \Rbplm\Sys\Datatype\File $data
	 * @return Package
	 */
	public function setData($data)
	{
		$this->data = $data;
		$this->path = $data->getFullPath();
		$this->uid = $data->getMd5();
		return $this;
	}

	/**
	 * @return \Rbplm\Sys\Datatype\File
	 */
	public function getData($asPath = false)
	{
		if ( $asPath === true ) {
			return $this->path;
		}
		else {
			return $this->data;
		}
	}
}