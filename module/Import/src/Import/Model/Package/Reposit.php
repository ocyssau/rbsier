<?php
//%LICENCE_HEADER%
namespace Import\Model\Package;

use Exception;

/**
 *
 *
 */
class Reposit extends \Rbplm\Sys\Directory
{

	/**
	 * Suppress a file
	 *
	 * @param string $dname name of the data to suppress.
	 * @return boolean
	 */
	public function deleteData($dname)
	{
		$path = $this->path . '/' . basename($dname);
		if ( is_file($path) ) {
			unlink($path);
		}
		else {
			throw new Exception(sprintf('FILE %s NOT FOUND', $path));
		}
	}
} /* End of class */
