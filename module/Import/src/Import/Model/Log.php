<?php
// %LICENCE_HEADER%
namespace Import\Model;

use Rbplm\Sys\Directory;
use SplFileObject;

/**
 */
class Log
{

	/**
	 * Log file object
	 * @var SplFileObject
	 */
	protected $file = null;

	/**
	 * Full path of the log file
	 * @var string
	 */
	protected $path = null;

	/**
	 *
	 * @param string $fullPath
	 */
	function __construct($fullPath)
	{
		$this->path = $fullPath;
		$path = dirname($fullPath);
		if ( !is_dir($path) ) {
			Directory::create($path, 0777);
		}
		$this->file = new \SplFileObject($fullPath, 'w');
	}

	/**
	 * @param string $str
	 * @return Log
	 */
	function log($str)
	{
		$this->file->fwrite($str . PHP_EOL);
		return $this;
	}

	/**
	 * @return \SplFileObject
	 */
	function getFile()
	{
		return $this->file;
	}

	/**
	 *
	 */
	function __destruct()
	{
		$this->file = null;
	}
}//End of class

