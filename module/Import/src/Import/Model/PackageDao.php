<?php
//%LICENCE_HEADER%
namespace Import\Model;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `import_package` (
 `package_id` INT(11) NOT NULL,
 `uid` VARCHAR(64) NOT NULL,
 `batch_id` INT(11) DEFAULT NULL,
 `description` text,
 `state` VARCHAR(32) DEFAULT NULL,
 `imported_logfile` text,
 `error_logfile` text,
 `feedback_logfile` text,
 `file_name` VARCHAR(128) DEFAULT NULL,
 `file_path` VARCHAR(512) DEFAULT NULL,
 `file_extension` VARCHAR(32) DEFAULT NULL,
 `file_mtime` INT(11) DEFAULT NULL,
 `file_size` INT(11) DEFAULT NULL,
 `file_md5` VARCHAR(128) NOT NULL DEFAULT '',
 PRIMARY KEY (`package_id`)
 ) ENGINE=InnoDB;

 CREATE TABLE IF NOT EXISTS `import_package_in_reposit` (
 `file_name` VARCHAR(128) NOT NULL,
 `file_path` VARCHAR(512) NOT NULL,
 `file_extension` VARCHAR(32) DEFAULT NULL,
 `file_mtime` INT(11) DEFAULT NULL,
 `file_size` INT(11) DEFAULT NULL,
 `file_md5` VARCHAR(128) NOT NULL DEFAULT '',
 `package_id` INT(11) NOT NULL,
 PRIMARY KEY (`file_name`,`file_path`)
 ) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 ALTER TABLE `mockup_doc_files` ADD COLUMN `package_id` INT(11) DEFAULT NULL;
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class PackageDao extends DaoSier
{

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'package_id' => 'id',
		'uid' => 'uid',
		'batch_id' => 'batchId',
		'description' => 'description',
		'state' => 'lifeStage',
		'imported_logfile' => 'importedLogfile',
		'error_logfile' => 'errorLogfile',
		'feedback_logfile' => 'feedbackLogfile',
		'file_name' => 'name',
		'file_path' => 'path',
		'file_root_name' => 'rootname',
		'file_extension' => 'extension',
		'file_type' => 'type',
		'file_size' => 'size',
		'file_mtime' => 'mtime',
		'file_md5' => 'md5'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array();
}
