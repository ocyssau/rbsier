<?php
/**
 *
 */
return array(
	'router' => array(
		'routes' => array(
			'import-reposit' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/import/reposit[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Import\Controller\Reposit',
						'action' => 'index',
					),
				),
			),
			'import' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/import/index[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Import\Controller\Index',
						'action' => 'index',
					),
				),
			),
			'import-job' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/import/job[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Import\Controller\Job',
						'action' => 'index',
					),
				),
			),
			'import-pacakge' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/import/package[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Import\Controller\Package',
						'action' => 'index',
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Import\Controller\Index' => 'Import\Controller\IndexController',
			'Import\Controller\Job' => 'Import\Controller\JobController',
			'Import\Controller\Package' => 'Import\Controller\PackageController',
			'Import\Controller\Reposit' => 'Import\Controller\RepositController',
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Import'=>__DIR__ . '/../view',
		),
	),
);
