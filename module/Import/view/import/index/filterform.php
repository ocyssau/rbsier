<?php
/* @var \Zend\View\Renderer\PhpRenderer $this */
/* @var \Zend\Form\Form $form */
$this->headScript()->prependFile($this->basePath('/js/rb/rb.filter.js'));
$form->setAttribute('id', 'importfilterform');
$form = $this->filterForm;
?>

<script>
$(function() {
	var myFilter = new rbfilter('#<?=$form->getAttribute('id')?>');
	myFilter.init();
});
</script>


<div id="sitesearchbar">

<?=$this->form()->openTag($form)?>

<fieldset>
	<div class="form-group"><?php echo $this->formRow($form->get('find_name'));?></div>
	<div class="form-group"><?php echo $this->formRow($form->get('find_designation'));?></div>

	<input type="submit" name="filter" value="<?php echo tra('filter')?>" class="btn btn-default btn-sm"/>
	<input type="button" name="resetf" value="<?php echo tra('Reset all filters')?>" class="btn btn-default btn-sm btn-resetf"/>
</fieldset>
<?php echo $this->form()->closeTag();?>
</div>
