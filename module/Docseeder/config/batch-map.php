<?php
/* example of map */
return [
	'Default' => [
		'disabled' => false, /* if true, the checkbox will be not selectable */
		'selected' => false, /* if true, the checkbox will be selected */
		'type' => 'text__plain', /* the number of doctype to use as template. A template must be associate to this doctype */
		'label' => 'For example only', /* label displayed after checkbox */
		'target' => '%WILDSPACE%', /* optional, if result file must be copied on this directory */
		'document'=>[
			'description'=>'For test', /* set title of ranchbe document */
			'label'=>'Test' /* suffix label added to document auto-generated number. Must respect constraints on document number format */
		]
	],
];
