<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'docseeder' => array(
				'type'=> 'Segment',
				'options' => array(
					'route'=> '/docseeder[/][:action]',
					'defaults' => array(
						'__NAMESPACE__' => 'Docseeder\Controller',
						'controller'=> 'Index',
						'action'=> 'index',
					),
				),
			),
			'docseeder-batch-select' => array(
				'type'=> 'Segment',
				'options' => array(
					'route'=> '/docseeder/batch/select[/][:action]',
					'defaults' => array(
						'__NAMESPACE__' => 'Docseeder\Controller',
						'controller'=> 'Batch',
						'action'=> 'select',
					),
				),
			),
			'docseeder-batch' => array(
				'type'=> 'Segment',
				'options' => array(
					'route'=> '/docseeder/batch[/][:action]',
					'defaults' => array(
						'__NAMESPACE__' => 'Docseeder\Controller',
						'controller'=> 'Batch',
						'action'=> 'run',
					),
				),
			),
			'docseeder-success' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/docseeder/success',
					'defaults' => array(
						'__NAMESPACE__' => 'Docseeder\Controller',
						'controller' => 'Index',
						'action' => 'success',
					),
				),
			),
			'docseeder-type' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/docseeder/type[/][:action]',
					'defaults' => array(
						'__NAMESPACE__' => 'Docseeder\Controller',
						'controller' => 'Type',
						'action' => 'index',
					),
				),
			),
			'docseeder-mask' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/docseeder/mask[/][:action]',
					'defaults' => array(
						'__NAMESPACE__' => 'Docseeder\Controller',
						'controller' => 'Mask',
						'action' => 'index',
					),
				),
			),
			'docseeder-mask-edit' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/docseeder/mask/edit/[:id]',
					'defaults' => array(
						'__NAMESPACE__' => 'Docseeder\Controller',
						'controller' => 'Mask',
						'action' => 'edit',
					),
				),
			),
			'docseeder-reconcile' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/docseeder/reconcile',
					'defaults' => array(
						'__NAMESPACE__' => 'Docseeder\Controller',
						'controller' => 'Reconcile',
						'action' => 'create',
					),
				),
			),
		), //routes
	), //router
	'controllers' => array(
		'invokables' => array(
			'Docseeder\Controller\Index' => 'Docseeder\Controller\IndexController',
			'Docseeder\Controller\Batch' => 'Docseeder\Controller\BatchController',
			'Docseeder\Controller\Type' => 'Docseeder\Controller\TypeController',
			'Docseeder\Controller\Mask' => 'Docseeder\Controller\MaskController',
			'Docseeder\Controller\Reconcile' => 'Docseeder\Controller\ReconcileController',
		),
	), //controller
	'view_manager' => array(
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	), //view_manager
	'menu' => [
		'myspace' => [
			'docseeder' => [
				'name'=>'docseeder',
				'route'=>['params'=>['action' => 'index'], 'options'=>['name' => 'docseeder']],
				'class' => '%anchor%',
				'label' => 'Docseeder'
			]
		],
	], //menu
	'docseeder-batch-maps' => [
	]
);
