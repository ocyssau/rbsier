from pptx import Presentation
import sys, getopt, base64
import json

class Powerpoint():
    
    def searchAndReplace(self, infile, outfile, replacementMap):
        prs = Presentation(infile)

        for mapElemt in replacementMap.values():
            hook = mapElemt['hook']
            rchain = mapElemt['rchain']
            for slide in prs.slides:
                for shape in slide.shapes:
                    if(shape.__class__.__name__ == 'Shape'):
                        textFrame = shape.text_frame
                        paragraphs = textFrame.paragraphs
                        for  paragraph in paragraphs:
                            for run in paragraph.runs:
                                txt = run.text;
                                print("Search " + hook + " In " + txt)
                                if(txt.find(hook) > -1):
                                    newText = txt.replace(hook, rchain)
                                    print("Replace " + txt + " By " + newText)
                                    run.text = newText
        prs.save(outfile)

def main(argv):
    infile = ''
    outfile = ''
    replacementMap = {}
    replacementJsonMap = ''

    try:
        opts, args = getopt.getopt(argv, "i:o:m:", ["infile:", "outfile:", "map:"])
    except:
        print("populator.py -i <inputfile> -o <outpufile> -m <jsonmap>")

    for opt, arg in opts:
        if opt in ("-i", "--infile"):
            infile = arg
            print(infile)
        if opt in ("-o", "--outfile"):
            outfile = arg
            print(outfile)
        if opt in ("-m", "--map"):
            replacementJsonMap = arg
            replacementJsonMap = base64.b64decode(replacementJsonMap)
            print(replacementJsonMap)
            replacementMap = json.loads(replacementJsonMap)

    ppt = Powerpoint()
    ppt.searchAndReplace(infile, outfile, replacementMap)

if __name__ == "__main__":
    main(sys.argv[1:])
    # main({1:,'--infile':'file', '--outfile':'outfile.pptx'})
