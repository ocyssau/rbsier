<?php
// %LICENCE_HEADER%
namespace Docseeder\Dao\Template;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `docseeder_mask` (
`id` int(11) NOT NULL,
`doctype_id` int(11) NOT NULL,
`uid` VARCHAR(32) NOT NULL,
`name` VARCHAR(64) NOT NULL DEFAULT '',
`map` JSON DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`doctype_id`),
UNIQUE KEY (`uid`),
UNIQUE KEY (`name`)
) ENGINE=InnoDB;
 << */

/** SQL_INSERT>>
 << */

/** SQL_ALTER>>
 << */

/** SQL_FKEY>>
 << */

/** SQL_TRIGGER>>
 << */

/** SQL_VIEW>>
 << */

/** SQL_DROP>>
 << */

/**
 * Type of of document Direct Access Object
 */
class MaskDao extends \Rbs\Dao\Sier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'docseeder_mask';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'docseeder_mask';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'docseeder_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = [
		'id' => 'id',
		'doctype_id' => 'doctypeId',
		'uid' => 'uid',
		'name' => 'name',
		'map' => 'map'
	];

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array();

	/**
	 * 
	 * @param \Docseeder\Model\Template\Mask $mapped
	 * @param integer $id
	 * @return \Rbplm\Dao\MappedInterface
	 */
	public function loadFromTypeId(\Docseeder\Model\Template\Mask $mapped, $id)
	{
		$filter = 'obj.doctype_id=:id';
		$bind = array(
			':id' => $id
		);
		return $this->load($mapped, $filter, $bind);
	}
} /* End of class*/
