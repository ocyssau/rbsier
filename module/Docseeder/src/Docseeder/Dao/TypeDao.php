<?php
//%LICENCE_HEADER%
namespace Docseeder\Dao;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `docseeder_type` (
 `id` int(11) NOT NULL DEFAULT '0',
 `name` VARCHAR(128) NOT NULL DEFAULT '',
 `designation` VARCHAR(256) DEFAULT NULL,
 `domain` VARCHAR(64) DEFAULT NULL,
 `template_id` int(11) DEFAULT NULL COMMENT 'id of the template docfile',
 `template_uid` VARCHAR(64) DEFAULT NULL COMMENT 'uid of the template docfile',
 `mask_id` int(11) DEFAULT NULL COMMENT 'id of the mask to use',
 `mask_uid` VARCHAR(64) DEFAULT NULL COMMENT 'uid of the mask',
 PRIMARY KEY (`id`),
 UNIQUE KEY (`name`),
 INDEX (`template_id`),
 INDEX (`template_uid`),
 INDEX (`mask_id`),
 INDEX (`mask_uid`),
 INDEX (`domain`)
 ) ENGINE=InnoDB;

 CREATE TABLE IF NOT EXISTS `docseeder_seq`(
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 )ENGINE=MyISAM AUTO_INCREMENT=10;
 INSERT INTO docseeder_seq(id) VALUES(10);
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * Type of of document Direct Access Object
 *
 */
class TypeDao extends \Rbs\Dao\Sier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'docseeder_type';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'docseeder_type';

	/**
	 * @var string
	 */
	public static $sequenceName = 'docseeder_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'name' => 'name',
		'designation' => 'description',
		'domain' => 'domain',
		'template_id' => 'templateId',
		'template_uid' => 'templateUid',
		'mask_id' => 'maskId',
		'mask_uid' => 'maskUid'
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array();
} /* End of class*/
