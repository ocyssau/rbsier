<?php
namespace Docseeder\Form\Type;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author olivier
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	public $template;

	public $ranchbe;

	protected $inputFilter;

	protected $templateSelect;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('dstypeEdit');

		$this->template = 'docseeder/type/editform';
		$this->ranchbe = \Ranchbe::get();
		$this->daoFactory = $factory;

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Number */
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number')
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Regex */
		$this->add(array(
			'name' => 'recognitionRegexp',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'textarea',
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 60
			),
			'options' => array(
				'label' => tra('Regex')
			)
		));

		/* Number Generator Class */
		$this->add(array(
			'name' => 'numberGeneratorClass',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '\Full\Class\Name',
				'class' => 'form-control',
				'size' => 64
			),
			'options' => array(
				'label' => tra('Number Generator Class')
			)
		));

		/* Template */
		$this->add(array(
			'name' => 'templateId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control template-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Template'),
				'value_options' => $this->_getTemplates(),
				'multiple' => false
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$classValidator = array(
			array(
				'name' => 'Regex',
				'options' => array(
					'pattern' => '/^\\\\[a-z\\\\]*$/i',
					'messages' => array(
						\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, and \\. Must Start with \\'
					)
				)
			)
		);

		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => false
			),
			'name' => array(
				'required' => true
			),
			'templateId' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'ToNull'
					)
				)
			),
			'numberGeneratorClass' => array(
				'required' => false,
				'validators' => $classValidator
			),
			'number' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-zA-Z0-9-_.\/]+$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'The number may not be composed with space and accents'
							)
						)
					)
				)
			)
		);
	}

	/**
	 * Get type from db
	 *
	 * @return array
	 */
	protected function _getTemplates()
	{
		$daoFactory = $this->daoFactory->get('Bookshop');
		if ( !isset($this->templateSelect) ) {
			try {
				$selectSet = [];
				$list = $daoFactory->getList(\Rbplm\Ged\Document\Version::$classId);
				$list->select(array(
					'id',
					'name',
					'version',
					'iteration'
				));
				$list->load("as_template=1 ORDER BY number ASC LIMIT 100");
				$selectSet[null] = 'Select one...';
				foreach( $list as $item ) {
					$selectSet[$item['id']] = $item['name'] . ' v' . $item['version'] . '.' . $item['iteration'];
				}
				$this->templateSelect = $selectSet;
			}
			catch( \Exception $e ) {
				throw $e;
			}
		}
		return $this->templateSelect;
	}
}
