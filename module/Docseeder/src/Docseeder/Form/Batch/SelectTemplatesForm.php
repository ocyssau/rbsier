<?php
namespace Docseeder\Form\Batch;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;

/**
 *
 *
 */
class SelectTemplatesForm extends \Application\Form\AbstractForm implements InputFilterProviderInterface
{

	/**
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @var InputFilter
	 */
	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('ingenierieFiletreeEdit');
		$this->template = 'docseeder/batch/selecttemplatesform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'containerId',
			'attributes' => [
				'type' => 'hidden'
			]
		));
		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'attributes' => [
				'type' => 'hidden'
			]
		));
		/* Add hidden fields */
		$this->add(array(
			'name' => 'map',
			'attributes' => [
				'type' => 'hidden'
			]
		));
		
		/* Submit button */
		$this->add([
			'name' => 'validate',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			]
		]);

		/* Cancel button */
		$this->add([
			'name' => 'cancel',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			]
		]);
	}

	/**
	 * 
	 * @param array $map
	 * @return \Sier\Form\Filetree\Ingenierie\EditForm
	 */
	public function setMap(array $map)
	{
		$valueOptions = [];
		foreach( $map as $k => $elemt ) {

			$selected = isset($elemt['selected']) ? (bool)$elemt['selected'] : false;
			$disabled = isset($elemt['disabled']) ? (bool)$elemt['disabled'] : true;
			
			$valueOptions[] = [
				'value' => $k,
				'label' => $elemt['type'] . ' - ' . $elemt['label'],
				'selected' => $selected,
				'disabled' => $disabled,
				'attributes' => [
					'id' => $k
				],
				'label_attributes' => [
					'class' => 'checkbox'
				]
			];
		}

		/* template checkboxes*/
		$this->add([
			'name' => 'templates',
			'type' => 'Zend\Form\Element\MultiCheckbox',
			'attributes' => [
				'class' => ''
			],
			'options' => [
				'label' => tra('documents to inits'),
				'use_hidden_element' => true,
				'value_options' => $valueOptions
			]
		]);

		return $this;
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'containerId' => [
				'required' => true
			],
			'spacename' => [
				'required' => true
			],
			'templates' => [
				'required' => true
			]
		);
	}
}
