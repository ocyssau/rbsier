<?php
namespace Docseeder\Form\Batch;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author olivier
 *
 */
class SelectMapForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 * @param string $name
	 */
	public function __construct($spacename = 'workitem')
	{
		/* we want to ignore the name passed */
		parent::__construct('docseeder');
		$this->template = 'docseeder/batch/selectmapform';

		$daoFactory = \Rbs\Space\Factory::get($spacename);
		$this->daoFactory = $daoFactory;

		/* */
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'attributes' => [
				'type' => 'hidden',
				'value' => $spacename
			],
		));

		/* Map */
		$this->add(array(
			'name' => 'map',
			'type' => \Zend\Form\Element\Select::class,
			'attributes' => [
				'placeholder' => '',
				'class' => 'form-control map-select'
			],
			'options' => [
				'label' => tra('Map'),
				'multiple' => false
			]
		));

		/* Affaire */
		$this->add(array(
			'name' => 'containerId',
			'type' => 'Application\Form\Element\SelectContainer',
			'attributes' => array(
				'class' => 'form-control wi-select'
			),
			'options' => array(
				'label' => tra('Workitem'),
				'multiple' => false,
				'fullname' => true,
				'dbfilter' => 'closed is null',
				'daoFactory' => $daoFactory
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default rb-openreferrer'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return [
			'map' => [
				'required' => true
			],
			'containerId' => [
				'required' => true
			],
			'spacename' => [
				'required' => true
			]
		];
	}

	/**
	 *
	 * @param array $maps
	 * @return SelectMapForm
	 */
	public function setMaps(array $maps)
	{
		/**/
		$this->get('map')->setValueOptions($maps);
		return $this;
	}
}
