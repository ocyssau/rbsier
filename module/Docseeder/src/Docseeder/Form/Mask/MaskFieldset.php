<?php
namespace Docseeder\Form\Mask;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods as Hydrator;
use Docseeder\Model\Template\Mask;

/**
 *
 * @author olivier
 *        
 */
class MaskFieldset extends Fieldset implements InputFilterProviderInterface
{

	protected $inputFilter;

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('mask');

		$this->setHydrator(new Hydrator(false))->setObject(new Mask());

		/* HOOK COLLECTION */
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'hooks',
			'options' => array(
				'label' => 'Hooks',
				'count' => 2,
				'should_create_template' => true,
				'allow_add' => true,
				'target_element' => array(
					'type' => 'Docseeder\Form\Mask\HookFieldset'
				)
			)
		));
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Zend\InputFilter\InputFilterProviderInterface::getInputFilterSpecification()
	 */
	public function getInputFilterSpecification()
	{}
}
