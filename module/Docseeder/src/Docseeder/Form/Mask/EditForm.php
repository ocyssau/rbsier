<?php
namespace Docseeder\Form\Mask;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ClassMethods as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author olivier
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('maskform');

		$this->template = 'docseeder/mask/editform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		/* Add hidden fields */
		$this->add(array(
			'name' => 'doctypeId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* HOOK COLLECTION */
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'hooks',
			'options' => array(
				'label' => 'Hooks',
				'count' => 0,
				'should_create_template' => true,
				'allow_add' => true,
				'target_element' => array(
					'type' => 'Docseeder\Form\Mask\HookFieldset'
				)
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			)
		);
	}
}
