<?php
namespace Docseeder\Form\Mask;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as Hydrator;
use Docseeder\Model\Template\Hook;

/**
 *
 * @author olivier
 *        
 */
class HookFieldset extends Fieldset implements InputFilterProviderInterface
{

	protected $inputFilter;

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('hooks');

		$this->setHydrator(new Hydrator(false))->setObject(new Hook('init'));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element\Hidden'
		));

		/* Template property hook */
		$this->add(array(
			'name' => 'templateHook',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Template Hook Chain')
			)
		));

		/* Replacement chain */
		$this->add(array(
			'name' => 'replacementChain',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Replacement Chain')
			)
		));

		/* Add button */
		$this->add(array(
			'name' => 'addHook',
			'type' => 'Zend\Form\Element\Button',
			'attributes' => array(
				'id' => 'addhookbutton',
				'class' => 'btn btn-success add-btn'
			),
			'options' => array(
				'label' => 'Add'
			)
		));

		/* Remove button */
		$this->add(array(
			'name' => 'removeHook',
			'type' => 'Zend\Form\Element\Button',
			'attributes' => array(
				'id' => 'removehookbutton',
				'class' => 'btn btn-danger remove-btn'
			),
			'options' => array(
				'label' => 'Remove'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'replacementChain' => array(
				'required' => true
			),
			'templateHook' => array(
				'required' => true
			)
		);
	}
}
