<?php
namespace Docseeder\Form;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author olivier
 *
 */
class DocseederForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 * @param string $name
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('docseeder');

		$daoFactory = \Rbs\Space\Factory::get('workitem');
		$this->daoFactory = $daoFactory;

		$this->template = 'docseeder/index/docseederform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Type */
		$this->add(array(
			'name' => 'typeId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control type-select'
			),
			'options' => array(
				'label' => tra('Type'),
				'value_options' => $this->_getTypes(),
				'multiple' => false
			)
		));

		/* Affaire */
		$this->add(array(
			'name' => 'containerId',
			'type' => 'Application\Form\Element\SelectContainer',
			'attributes' => array(
				'class' => 'form-control wi-select'
			),
			'options' => array(
				'label' => tra('Workitem'),
				'multiple' => false,
				'fullname' => true,
				'dbfilter' => 'closed is null',
				'daoFactory' => $daoFactory
			)
		));

		/* Label */
		$this->add(array(
			'name' => 'label',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 80
			),
			'options' => array(
				'label' => tra('Suffix label')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 80,
				'rows' => 2
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default rb-openreferrer'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'typeId' => [
				'required' => false
			],
			'containerId' => [
				'required' => false
			],
			'description' => [
				'required' => true,
				'validators' => []
			],
			'label' => [
				'required' => true,
				'filters' => [
					[
						'name' => 'Alnum',
						'options' => [
							'allow_white_space' => false
						]
					]
				],
				'validators' => []
			]
		);
	}

	/**
	 * Get type from db
	 *
	 * @return array
	 */
	protected function _getTypes()
	{
		$daoFactory = $this->daoFactory;
		$selectSet = array();
		if ( !isset($this->typesSelect) ) {
			try {
				$list = $daoFactory->getList(\Rbplm\Ged\Doctype::$classId);
				$list->select(array(
					'id',
					'number',
					'name',
					'designation'
				));
				$list->load("docseeder=1 LIMIT 1000");
				foreach( $list as $item ) {
					$selectSet[$item['id']] = $item['name'] . ' - ' . $item['designation'];
				}
				$this->typesSelect = $selectSet;
			}
			catch( \Exception $e ) {
				throw $e;
			}
		}
		return $this->typesSelect;
	}
}
