<?php
namespace Docseeder\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Doctype as Type;
//use Docseeder\Model\Template\Mask;
use Rbplm\Ged\Document;

/**
 */
class ReconcileController extends AbstractController
{

	/* */
	public $pageId = 'docseeder_reconcile';

	/* */
	public $defaultSuccessForward = '/docseeder/type';

	/* */
	public $defaultFailedForward = '/docseeder/type';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* */
		$this->resourceCn = \Acl\Model\Resource\GedManager::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\GedManager::$appCn;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;

		/* Init some helpers */
		$factory = DaoFactory::get('Bookshop');
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->andfind(1, 'docseeder');
		$list = $factory->getList(Type::$classId);
		$list->load($filter, []);
		$dtDao = $factory->getDao(Type::$classId);
		$docdao = $factory->getDao(Document\Version::$classId);

		$output = [];
		foreach( $list as $asSys ) {
			if ( $asSys['template_id'] ) {
				$doctype = new Type();
				$dtDao->hydrate($doctype, $asSys);
				$templateId = $doctype->getTemplate(true);
				$number = $docdao->getNumberFromId($templateId);
				$document = new Document\Version();
				try {
					$docdao->loadFromNumber($document, $number);
				}
				catch( \Exception $e ) {
					continue;
				}

				/* must be updated? */
				if ( $document->getId() != $doctype->getTemplate(true) ) {
					$doctype->setTemplate($document);
					$dtDao->save($doctype);
					$doctype->document = $document;
				}
			}
		}

		$view->list = $output;
		return $view;
	}
}
