<?php
namespace Docseeder\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Docseeder\Model\Template\Mask;
use Docseeder\Model\Template\Hook;

/**
 */
class MaskController extends AbstractController
{

	/* */
	public $pageId = 'docseeder_mask';

	/* */
	public $defaultSuccessForward = '/docseeder/type';

	/* */
	public $defaultFailedForward = '/docseeder/type';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* */
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('document');
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$view->setTemplate('docseeder/mask/index/index');

		/* Init some helpers*/
		$factory = DaoFactory::get();

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Docseeder\Form\Mask\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'dsmaskfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Mask::$classId);
		$table = $factory->getTable(Mask::$classId);
		$select = array(
			$table . '.id as id',
			$table . '.uid as uid',
			$table . '.name as name'
		);

		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Docseeder Mask Manager';

		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();

		if ( $request->isGet() ) {
			$checked = $request->getQuery('checked', array());
			$id = $request->getQuery('id', null);
			if ( $checked && !$id ) {
				$id = current($checked);
			}
			$validate = false;
		}
		if ( $request->isPost() ) {
			$id = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$mask = new Mask();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Mask::$classId);

		try {
			$dao->loadFromTypeId($mask, $id);
			/* Convert map to hooks collection */
			$map = $mask->getMap();
			/* At least one hook is require to init form collection */
			if ( count($map) == 0 ) {
				$mask->addHook(new Hook(0));
			}
			else {
				foreach( $map as $m ) {
					$hook = new Hook($m['name']);
					$hook->hydrate($m);
					$mask->addHook($hook);
				}
			}
		}
		catch( \Rbplm\Dao\NotExistingException $e ) {
			$mask = Mask::init(uniqid());
			$mask->addHook(new Hook(0));
			$mask->setDoctypeId($id);
		}

		$form = new \Docseeder\Form\Mask\EditForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($mask);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				/* Convert hooks collection to map */
				$hooks = $mask->getHooks();
				$mask->setMap(json_encode($hooks, true));
				$dao->save($mask);
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Type Mask ' . $mask->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}
}
