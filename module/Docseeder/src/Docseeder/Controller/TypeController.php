<?php
namespace Docseeder\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Doctype as Type;
use Docseeder\Model\Template\Mask;

/**
 */
class TypeController extends AbstractController
{

	/* */
	public $pageId = 'docseeder_type';

	/* */
	public $defaultSuccessForward = '/docseeder/type';

	/* */
	public $defaultFailedForward = '/docseeder/type';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* */
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('document');
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\GedManager::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\GedManager::$appCn;
	}

	/**
	 */
	public function indexAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$request = $this->getRequest();
		$view = $this->view;
		$view->setTemplate('docseeder/type/index/index');
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Init some helpers */
		$factory = DaoFactory::get('Bookshop');

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Docseeder\Form\Type\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'dstypefilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();
		$filter->andfind(1, 'docseeder');

		$list = $factory->getList(Type::$classId);
		$table = $factory->getTable(Type::$classId);
		$select = array(
			$table . '.id as id',
			$table . '.name as name',
			$table . '.designation as description',
			$table . '.mask_id as maskId',
			$table . '.mask_uid as maskUid',
			'CONCAT(`template`.`name`," v",`template`.`version`)  as template'
		);

		$filter->select($select);
		$table = $factory->getTable(\Rbplm\Ged\Document\Version::$classId);
		$filter->with(array(
			'table' => $table,
			'alias' => 'template',
			'lefton' => 'template_id',
			'righton' => 'id'
		));
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Docseeder Types Manager';
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', array());
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Type::$classId);

		foreach( $ids as $id ) {
			try {
				$dao->deleteFromId($id);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $id));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', array());
			$id = $ids[0];
			$validate = false;
		}
		if ( $request->isPost() ) {
			$id = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$type = new Type();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Type::$classId);
		$dao->loadFromId($type, $id);

		$form = new \Docseeder\Form\Type\EditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($type);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($type);
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Docseeder Type ' . $type->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		/* */
		$type = Type::init('SIER-XXXX');
		$type->setNumber('XXXX');
		$type->docseeder = 1;
		$factory = DaoFactory::get();
		$dao = $factory->getDao($type::$classId);

		/* */
		$form = new \Docseeder\Form\Type\EditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($type);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				/* save Type */
				$type->setNumber(\Rbs\Number::normalize($type->getName()));
				$dao->save($type);

				/* create a mask */
				$mask = Mask::init($type->getName());
				$mask->setDoctypeId($type->getId());
				$factory->getDao($mask->cid)->save($mask);

				return $this->successForward();
			}
		}

		$view->pageTitle = 'Create Docseeder Type';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}
}
