<?php
namespace Docseeder\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Ranchbe;
use Rbplm\Ged\Document;
use Docseeder;
use Application\Controller\ControllerException;

#use Docseeder\Model\NoneTemplateException;
#use Rbplm\People\CurrentUser;
#use Rbs\Ged\Document\Event as DocumentEvent;

/**
 * Display and treat form to create new document from docseeder template.
 *
 */
class IndexController extends AbstractController
{

	/* */
	public $pageId = 'docseeder_index';

	/* */
	public $defaultSuccessForward = '/home';

	/* */
	public $defaultFailedForward = '/docseeder/index';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('docseeder');
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		/* Inits */
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}
		
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* Inits helpers */
		$workitemFactory = DaoFactory::get('Workitem');
		$docseeder = Docseeder\Model\Docseeder::init();
		$docseeder->containerId = \Ranchbe::get()->getServiceManager()->getContext()->getData('containerId');

		/* Form */
		$form = new Docseeder\Form\DocseederForm();
		$form->setAttribute('action', $this->url()->fromRoute('docseeder'));
		$form->bind($docseeder);
		
		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$helper = new \Docseeder\Model\DeployHelper($workitemFactory);
				$helper->setConfig(Ranchbe::get()->getConfig());
				
				/* Check permissions */
				$helper->loadContainer($docseeder);
				$this->getAcl()->checkRightFromUid('create', $docseeder->getContainer()->getUid());
				
				/**/
				$helper($docseeder);
				$document = $docseeder->getDocument();

				return $this->redirect()->toRoute('docseeder-success', [], [
					'query' => [
						'documentId' => $document->getId(),
						'spacename' => $document->spacename
					]
				]);
			} /* isvalid */
		} /* validate */

		/* init view */
		$view->setTemplate('docseeder/index/index');
		$view->form = $form;
		$view->pageTitle = 'Documents Seeder';

		return $view;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function successAction()
	{
		/* Inits */
		$view = $this->view;

		$documentId = $this->getRequest()->getQuery('documentId', null);
		$spacename = $this->getRequest()->getQuery('spacename', null);

		if ( !$documentId ) {
			throw new ControllerException('documentId get query parameter is not set');
		}
		if ( !$spacename ) {
			throw new ControllerException('spacename get query parameter is not set');
		}

		$factory = DaoFactory::get($spacename);
		$document = new Document\Version();
		$document->dao = $factory->getDao($document->cid);
		$document->dao->loadFromId($document, $documentId);

		$view->document = $document;
		$view->pageTitle = "Docseeder document generation is successed";
		$view->setTemplate('docseeder/index/success');
		return $view;
	}
}
