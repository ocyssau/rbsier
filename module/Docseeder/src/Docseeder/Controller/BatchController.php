<?php
namespace Docseeder\Controller;

use Application\Controller\AbstractController;
use Docseeder;
use Application\Controller\ControllerException;
use Rbs\Space\Factory as DaoFactory;

/**
 */
class BatchController extends AbstractController
{

	/* */
	public $pageId = 'docseeder_batch';

	/* */
	public $defaultSuccessForward = '/home';

	/* */
	public $defaultFailedForward = '/docseeder/index';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('docseeder');
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function selectAction()
	{
		/* Inits */
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* Form */
		$form = new Docseeder\Form\Batch\SelectMapForm();
		
		$config = $this->getEvent()->getApplication()->getServiceManager()->get('config');
		$maps = $config['docseeder-batch-maps'];
		$maps = array_flip($maps);
		sort($maps);
		$maps = array_combine($maps, $maps);
		$form->setMaps($maps);
		
		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$datas = $form->getData();
				return $this->redirect()->toRoute('docseeder-batch', ['action'=>'run'], ['query'=>[
					'map' => $datas['map'],
					'spacename' => $datas['spacename'],
					'containerId' => $datas['containerId']
				]]);
			} /* isvalid */
		}
		else{
			$datas = $request->getQuery();
			$context = \Ranchbe::get()->getServiceManager()->getContext();
			$datas['containerId'] = $context->getData('containerId');
			$datas['spacename'] = $context->getData('spacename');
			if($datas['spacename'] == 'workitem'){
				$form->setData($datas);
			}
		}

		/* init view */
		$view->setTemplate($form->template);
		$view->form = $form;
		$view->pageTitle = 'Select The Map';

		return $view;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function runAction()
	{
		/* Inits */
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
			
			$mapKey = $request->getQuery('map', null);
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
			
			$mapKey = $request->getPost('map', null);
		}
		
		if ( !$mapKey ) {
			throw new ControllerException('map get query parameter is not set');
		}
		
		$config = $this->getEvent()->getApplication()->getServiceManager()->get('config');
		$maps = $config['docseeder-batch-maps'];
		
		if( ! isset($maps[$mapKey])){
			throw new ControllerException(sprintf('map key %s is not existing in configuration', $mapKey));
		}
		
		$mapFile = $maps[$mapKey];
		$map = include($mapFile);
		
		/* Form */
		$form = new Docseeder\Form\Batch\SelectTemplatesForm();
		$form->setMap($map);
		
		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$datas = $form->getData();
				
				$selected = $datas['templates'];
				$spacename = $datas['spacename'];
				$containerId = $datas['containerId'];
				
				$factory = DaoFactory::get($spacename);
				$doctypeDao = $factory->getDao(\Rbplm\Ged\Doctype::$classId);
				
				/* build a list of elements to deploy */
				$toDeploy = [];
				foreach($map as $k => $mapItem){
					/* force ignore or execution of disabled elements */
					if ( $mapItem['disabled'] ) {
						if ( $mapItem['selected'] == false ) {
							continue;
						}
						else {
							$toDeploy[] = $mapItem;
						}
					}
					else if ( is_array($selected) ) {
						if ( in_array($k, $selected) ) {
							$toDeploy[] = $mapItem;
						}
					}
				}
				
				$helper = new \Docseeder\Model\DeployHelper($factory);
				$helper->setConfig($config['rbp']);
				
				$documents = [];
				
				/* deploy selected items */
				foreach($toDeploy as $mapItem){
					$dtName = $mapItem['type'];
					$doctype = $doctypeDao->loadFromName(new \Rbplm\Ged\Doctype(), $dtName);
					$dtId = $doctype->getId();
					
					$label = $mapItem['document']['label'];
					$description = $mapItem['document']['description'];
					
					$request->getPost()->set('containerId', $containerId);
					$request->getPost()->set('spacename', $spacename);
					$request->getPost()->set('label', $label);
					$request->getPost()->set('description', $description);
					$request->getPost()->set('typeId', $dtId);
					
					/* @var \Zend\Stdlib\Parameters $postDatas */
					#$postDatas = $request->getPost();
					
					$docseeder = Docseeder\Model\Docseeder::init();
					$docseeder->hydrate($request->getPost()->toArray());
					
					/* Check permissions */
					$helper->loadContainer($docseeder);
					$this->getAcl()->checkRightFromUid('create', $docseeder->getContainer()->getUid());
					
					$helper($docseeder);
					
					$documents[] = $docseeder->getDocument();
				}
				
				$view->documents = $documents;
				$view->pageTitle = 'Created documents';
				$view->setTemplate('docseeder/batch/success');
				return $view;
				
			} /* isvalid */
		} /* validate */
		else{
			$datas = $request->getQuery();
			$form->setData($datas);
		}
		
		$view->setTemplate($form->template);
		$view->form = $form;
		$view->pageTitle = 'Select templates to create documents';
		
		return $view;
	}
}
