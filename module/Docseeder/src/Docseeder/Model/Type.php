//%LICENCE_HEADER% namespace Docseeder\Model; use Rbplm\Any; use
Rbplm\Item; use Rbplm\Mapped; use Rbplm\Bewitched; /** * Type of of
document * */ class Type extends Any implements
\Rbplm\Dao\MappedInterface { use Item, Mapped, Bewitched; /** @var
string */ static $classId = 'docseedertype'; /** @var string */ public
$description; /** @var string */ public $domain; /** @var
\Rbplm\Ged\Document\Version */ protected $template; /** @var integer */
public $templateId; /** @var string */ public $templateUid; /** @var
\Docseeder\Model\Template\Mask */ protected $mask; /** @var integer */
public $maskId; /** @var string */ public $maskUid; /** @var
\Docseeder\Model\Number\Rule */ protected $numberGenerator; /** @var
string */ protected $numberGeneratorClass; /** * * @param string $name *
@return Type */ public static function init($name = null) { $class =
get_called_class(); $obj = new $class(); $obj->setName($name); return
$obj; } /** * Hydrator. * Load the properties in the mapped object. * *
@param array $properties * \PDO fetch result to load * @return Any */
public function hydrate(array $properties) {
$this->mappedHydrate($properties); (isset($properties['name'])) ?
$this->name = $properties['name'] : null; (isset($properties['uid'])) ?
$this->uid = $properties['uid'] : null; (isset($properties['cid'])) ?
$this->cid = $properties['cid'] : null; (isset($properties['domain'])) ?
$this->domain = $properties['domain'] : null;
(isset($properties['description'])) ? $this->description =
$properties['description'] : null; (isset($properties['templateId'])) ?
$this->templateId = $properties['templateId'] : null;
(isset($properties['templateUid'])) ? $this->templateUid =
$properties['templateUid'] : null; (isset($properties['maskId'])) ?
$this->maskId= $properties['maskId'] : null;
(isset($properties['maskUid'])) ? $this->maskUid= $properties['maskUid']
: null; (isset($properties['numberGeneratorClass'])) ?
$this->numberGeneratorClass = $properties['numberGeneratorClass'] :
null; (isset($properties['numberGenerator'])) ? $this->numberGenerator =
$properties['numberGenerator'] : null; return $this; } /** * @param
\Rbplm\Ged\Document\Version $document * @return Type */ public function
setTemplate($document) { $this->templateId = $document->getId();
$this->templateUid = $document->getUid(); $this->template = $document;
return $this; } /** * @return \Rbplm\Ged\Document\Version */ public
function getTemplate($asId=false) { if($asId){ return $this->templateId;
} return $this->template; } /** * @param \Docseeder\Model\Template\Mask
$mask * @return Type */ public function setMask($mask) { $this->maskId =
$mask->getId(); $this->maskUid = $mask->getUid(); $this->mask = $mask;
return $this; } /** * @return \Docseeder\Model\Template\Mask */ public
function getMask($asId=false) { if($asId){ return $this->maskId; }
return $this->mask; } /** * @param string $class * @return Type */
public function setNumberGeneratorClass($class) {
$this->numberGeneratorClass = $class; return $this; } /** * @return
string */ public function getNumberGeneratorClass() { return
$this->numberGeneratorClass; } /** * @param \Docseeder\Model\Number\Rule
$generator * @return Type */ public function
setNumberGenerator($generator) { $this->numberGenerator = $generator;
$this->numberGeneratorClass = get_class($generator); return $this; } /**
* @return \Docseeder\Model\Number\Rule */ public function
getNumberGenerator() { return $this->numberGenerator; } } /* End of
class*/
