<?php
//%LICENCE_HEADER%
namespace Docseeder\Model;

/**
 * To populate files with properties defined from the Map and Agregator
 *
 */
abstract class Populator
{

	/**
	 *
	 * @var Docseeder $docseeder
	 */
	protected $docseeder;

	/**
	 *
	 */
	public function construct()
	{}

	/**
	 * @param Docseeder $docseeder
	 * @param string $file File name for new document
	 * @return Populator
	 */
	abstract public function populate(Docseeder $docseeder, $file);

	/**
	 * @param string $replacementChain
	 * @param Docseeder $docseeder
	 * @return string
	 */
	public function replacementChain($replacementChain, $docseeder)
	{
		$log = \Ranchbe::get()->getLogger();

		$rules = [
			[
				"/([%]{1}(template){1}(.){1}([a-zA-Z0-9-_]+){1}[%]{1})/",
				$docseeder->getType()->getTemplate()
			],
			[
				"/([%]{1}(document){1}(.){1}([a-zA-Z0-9-_]+){1}[%]{1})/",
				$docseeder->getDocument()
			],
			[
				"/([%]{1}(type){1}(.){1}([a-zA-Z0-9-_]+){1}[%]{1})/",
				$docseeder->getType()
			],
			[
				"/([%]{1}(container){1}(.){1}([a-zA-Z0-9-_]+){1}[%]{1})/",
				$docseeder->getContainer()
			],
			[
				"/([%]{1}(workitem){1}(.){1}([a-zA-Z0-9-_]+){1}[%]{1})/",
				$docseeder->getContainer()
			],
			[
				"/([%]{1}(project){1}(.){1}([a-zA-Z0-9-_]+){1}[%]{1})/",
				($docseeder->getContainer()) ? $docseeder->getContainer()->getParent() : null
			],
			[
				"/([%]{1}(user){1}(.){1}([a-zA-Z0-9-_]+){1}[%]{1})/",
				$docseeder->getUser()
			]
		];

		foreach( $rules as $rule ) {
			/* init loop vars */
			$matches = [];
			$propertyName = null;
			$value = null;
			$pattern = $rule[0];
			$object = $rule[1];

			preg_match_all($pattern, $replacementChain, $matches, PREG_SET_ORDER);
			foreach( $matches as $match ) {
				if ( !$object ) {
					continue;
				}
				try {
					$propertyName = $match[4];
					$value = $object->$propertyName;
					$replacementChain = str_replace($match[1], $value, $replacementChain);
				}
				catch( \Exception $e ) {
					$log->log(sprintf('Property name %s is unreadable with reason > %s', $propertyName, $e->getMessage()));
					continue;
				}
			}
		}

		/* Date */
		$datePreg = preg_quote('-(),;_\#$?+{}<>:');
		$pattern = "/([%]{1}(date){1}(.){1}([a-zA-Z0-9\/$datePreg]+){1}[%]{1})/";
		$matches = array();
		preg_match_all($pattern, $replacementChain, $matches, PREG_SET_ORDER);
		foreach( $matches as $match ) {
			try {
				$format = $match[4];
				$date = new \DateTime();
				$value = $date->format($format);
				$replacementChain = str_replace($match[1], $value, $replacementChain);
			}
			catch( \Exception $e ) {
				$log->log(sprintf('Format of date %s is malformed > %s', $format, $e->getMessage()));
				continue;
			}
		}

		return $replacementChain;
	}
} /* End of class*/
