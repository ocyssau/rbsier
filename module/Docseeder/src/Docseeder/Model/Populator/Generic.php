<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Populator;

use Docseeder\Model\Docseeder;

/**
 * Writer for not managed templates files
 * Simply copy template file to target $file
 *
 */
class Generic extends \Docseeder\Model\Populator
{

	/**
	 * @param Docseeder $docseeder
	 * @param string $file File name for new document
	 * @return Generic
	 */
	public function populate(Docseeder $docseeder, $file)
	{
		/* Get word template file */
		$type = $docseeder->getType();
		$templateFile = $type->getTemplate()->mainDocfile->getData()->fullpath;
		@copy($templateFile, $file);
		return $this;
	}
} /* End of class*/
