<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Populator;

use Application\Console\Output as Cli;
use Application\Model\DataTestFactory as DataFactory;

/**
 * Writer for Powerpoint files
 * 
 * @see \Application\Model\AbstractTest
 */
class PowerpointPyTest extends \Application\Model\AbstractTest
{

	public function runTest()
	{
		//$templateFilepath = realpath('data/dataSetForTest/test.pptx');
		$templateFilepath = realpath('data/dataSetForTest/template_ppt_SIER_print_Anglais.pptx');

		Cli::green('Template file path: ' . $templateFilepath);

		/* load template type */
		$doctype = DataFactory::doctypeFactory('fortest');

		/* load template document */
		$template = DataFactory::documentFactory('fortest');
		$doctype->setTemplate($template);

		/* get template file */
		/* @var \Rbplm\Ged\Docfile\Version $docfile */
		$docfile = DataFactory::docfileFactory($templateFilepath);
		$template->mainDocfile = $docfile;

		/* load mask */
		/* @var \Docseeder\Model\Template\Mask $mask */
		$mask = DataFactory::maskFactory('fortest');
		$hook = new \Docseeder\Model\Template\Hook('hook1');
		$hook->setTemplateHook('title');
		$hook->setReplacementChain('%container.number%');
		$mask->addHook($hook);

		$hook2 = new \Docseeder\Model\Template\Hook('hook1');
		$hook2->setTemplateHook('number');
		$hook2->setReplacementChain('%document.number%');
		$mask->addHook($hook2);
		$doctype->setMask($mask);

		/* Load containr */
		$container = DataFactory::containerFactory('workitem', 'container1');

		/* Load document */
		$document = DataFactory::documentFactory('newDocument');
		$document->setDoctype($doctype);
		$document->setParent($container);

		/* Load docseeder */
		$docseeder = new \Docseeder\Model\Docseeder();
		$docseeder->hydrate([]);
		$docseeder->setType($doctype);
		$docseeder->setContainer($container);
		$docseeder->setDocument($document);

		/**/
		@mkdir('/tmp/rbtest');
		$toFilepath = tempnam('/tmp/rbtest', 'powerpointTest');
		$toFilepath = $toFilepath . '.pptx';
		$populator = new PowerpointPy();
		$populator->populate($docseeder, $toFilepath);

		Cli::blue('Outputs log :');
		Cli::blue(var_export($populator->outputs, true));
		Cli::green('See result file in ' . $toFilepath);
	}
} /* End of class*/
