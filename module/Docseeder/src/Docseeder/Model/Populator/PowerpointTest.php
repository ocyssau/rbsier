/*********** NOT USED *****************************/ //%LICENCE_HEADER%
namespace Docseeder\Model\Populator; use Application\Console\Output as
Cli; use Application\Model\DataTestFactory as DataFactory; /** * Writer
for Powerpoint files * * @see \Application\Model\AbstractTest */ class
PowerpointTest extends \Application\Model\AbstractTest { public function
runTest() { $templateFilepath =
realpath('data/dataSetForTest/test.pptx'); $templateFilepath =
realpath('/home/ocyssau/template_ppt_SIER_print_Anglais_2.pptx');

Cli::green('Template file path: ' . $templateFilepath); /* load template
type */ $doctype = DataFactory::doctypeFactory('fortest'); /* load
template document */ $template =
DataFactory::documentFactory('fortest');
$doctype->setTemplate($template); /* get template file */ /* @var
\Rbplm\Ged\Docfile\Version $docfile */ $docfile =
DataFactory::docfileFactory($templateFilepath); $template->mainDocfile =
$docfile; /* load mask */ /* @var \Docseeder\Model\Template\Mask $mask
*/ $mask = DataFactory::maskFactory('fortest'); $hook = new
\Docseeder\Model\Template\Hook('hook1');
$hook->setTemplateHook('{$containerNumber}');
$hook->setReplacementChain('%container.number%'); $mask->addHook($hook);

$hook2 = new \Docseeder\Model\Template\Hook('hook1');
$hook2->setTemplateHook('{$docnum}');
$hook2->setReplacementChain('%document.number%');
$mask->addHook($hook2); $doctype->setMask($mask); /* Load containr */
$container = DataFactory::containerFactory('workitem', 'container1'); /*
Load document */ $document =
DataFactory::documentFactory('newDocument');
$document->setDoctype($doctype); $document->setParent($container); /*
Load docseeder */ $docseeder = new \Docseeder\Model\Docseeder();
$docseeder->hydrate([]); $docseeder->setType($doctype);
$docseeder->setDocument($document);

var_dump($doctype->getMask()->getMap()); /**/ @mkdir('/tmp/rbtest');
$toFilepath = tempnam('/tmp/rbtest', 'powerpointTest'); $toFilepath =
$toFilepath . '.pptx'; $populator = new Powerpoint();
$populator->populate($docseeder, $toFilepath); Cli::green('See result
file in '.$toFilepath); } } /* End of class*/
