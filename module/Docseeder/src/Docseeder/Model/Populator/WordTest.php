<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Populator;

use Application\Console\Output as Cli;
use Application\Model\DataTestFactory as DataFactory;

/**
 * Writer for Powerpoint files
 * 
 * @see \Application\Model\AbstractTest
 */
class WordTest extends \Application\Model\AbstractTest
{

	/**
	 * 
	 */
	public function runTest()
	{
		$templateFilepath = realpath('data/dataSetForTest/test.docx');
		if ( !is_readable($templateFilepath) ) {
			throw new \Exception("template file $templateFilepath is not readable");
		}

		Cli::green('Template file path: ' . $templateFilepath);

		/* load template type */
		$doctype = DataFactory::doctypeFactory('fortest');

		/* load template document */
		$template = DataFactory::documentFactory('fortest');
		$doctype->setTemplate($template);

		/* get template file */
		$docfile = DataFactory::docfileFactory($templateFilepath);
		$template->mainDocfile = $docfile;

		/* load mask */
		$mask = DataFactory::maskFactory('fortest');
		$hook = new \Docseeder\Model\Template\Hook('hook1');
		$hook->setTemplateHook('containerNumber');
		$hook->setReplacementChain('%container.number%');
		$mask->addHook($hook);

		$hook2 = new \Docseeder\Model\Template\Hook('hook2');
		$hook2->setTemplateHook('user');
		$hook2->setReplacementChain('%user.login%');
		$mask->addHook($hook2);

		$hook3 = new \Docseeder\Model\Template\Hook('hook3');
		$hook3->setTemplateHook('docNumber');
		$hook3->setReplacementChain('%document.number%');
		$mask->addHook($hook3);

		$hook4 = new \Docseeder\Model\Template\Hook('hook4');
		$hook4->setTemplateHook('title');
		$hook4->setReplacementChain('%document.description%');
		$mask->addHook($hook4);

		$doctype->setMask($mask);

		/* Load containr */
		$container = DataFactory::containerFactory('workitem', 'container1');

		/* Load document */
		$document = DataFactory::documentFactory('newDocument');
		$document->setDoctype($doctype);
		$document->setParent($container);

		/* Load docseeder */
		$docseeder = new \Docseeder\Model\Docseeder();
		$docseeder->hydrate([]);
		$docseeder->setType($doctype);
		$docseeder->setDocument($document);
		$docseeder->setContainer($container);
		$docseeder->setUser(DataFactory::userFactory('tester'));

		Cli::console('Map content :');
		var_dump($doctype->getMask()->getMap());

		/**/
		@mkdir('/tmp/rbtest');
		$toFilepath = tempnam('/tmp/rbtest', 'wordTest');
		$toFilepath = $toFilepath . '.docx';
		$populator = Factory::getFromType('.docx');

		$tfile = $docseeder->getType()->getTemplate()->mainDocfile->getData()->fullpath;
		Cli::green('Template file: ' . $tfile);

		assert($populator instanceof Word);
		assert(get_class($populator) != 'Docseeder\Model\Populator\Generic');
		$populator->populate($docseeder, $toFilepath);

		Cli::green('See result file in ' . $toFilepath);
	}
} /* End of class*/
