/*********** NOT USED *****************************/ //%LICENCE_HEADER%
namespace Docseeder\Model\Populator; use Docseeder\Model\Docseeder; use
PhpOffice\PhpPresentation\IOFactory; /** * Writer for Powerpoint files *
* */ class Powerpoint extends \Docseeder\Model\Populator { /** * * @var
array */ private $phpSlides; /** * @param Docseeder $docseeder * @param
string $file File name for new document * @return Powerpoint */ public
function populate(Docseeder $docseeder, $file) { /**/ $type =
$docseeder->getType(); /* Get word template file */ $templateFile =
$type->getTemplate()->mainDocfile->getData()->fullpath; /* */ $pptReader
= IOFactory::createReader('PowerPoint2007'); $phpPresentation =
$pptReader->load($templateFile); $property =
$phpPresentation->getDocumentProperties();
$property->setCreator('Ranchbe Docseeder');
$property->setCompany('S.I.E.R, Société industrielle d\'étude et
réalisation'); $slides = $phpPresentation->getAllSlides();
$this->phpSlides = $slides; /* Get template hooks -> replacement chain
*/ $mask = $type->getMask(); if ( $mask ) { $map = $mask->getMap(); /**/
foreach( $map as $m ) { $templateHook = '${'.$m['templateHook'].'}';
$replacementChain = $m['replacementChain']; $replacementChain =
$this->replacementChain($replacementChain, $docseeder);
$this->searchAndReplace($templateHook, $replacementChain); } }

$writerPptx = IOFactory::createWriter($phpPresentation);
$writerPptx->save($file); return $this; } /** * @param string
$templateHook * @param string $replacementChain * @return Powerpoint */
private function searchAndReplace($templateHook, $replacementChain) {
$slides = $this->phpSlides; foreach( $slides as $slide ) { $shapes =
$slide->getShapeCollection(); foreach( $shapes as $shape ) { if ( $shape
instanceof \PhpOffice\PhpPresentation\Shape\RichText ) { $paragraphs =
$shape->getParagraphs(); foreach( $paragraphs as $paragraph ) {
$textElements = $paragraph->getRichTextElements(); foreach(
$textElements as $textElement ) { $text = $textElement->getText();
$newText = str_replace($templateHook, $replacementChain, $text);
$textElement->setText($newText); } } } } } return $this; } } /* End of
class*/
