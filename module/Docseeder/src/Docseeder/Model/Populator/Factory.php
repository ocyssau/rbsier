<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Populator;

/**
 * Factory for build application objets used to write properties in file
 *
 */
class Factory
{

	/**
	 *
	 * @param string $type Extension of file without .
	 * @return \Docseeder\Model\Populator
	 */
	public static function getFromType($type)
	{
		$type = strtolower(trim($type, '.'));
		switch ($type) {
			case 'docx':
				return new Word();
				break;
			case 'xlsx':
				return new Excel();
				break;
			case 'pptx':
				return new PowerpointPy();
				break;
			default:
				return new Generic();
				break;
		}
	}
} /* End of class*/
