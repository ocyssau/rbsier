<?php
// %LICENCE_HEADER%
namespace Docseeder\Model\Populator;

/**
 * Writer for Word files
 */
class Word extends \Docseeder\Model\Populator
{

	/**
	 * @param \Docseeder\Model\Docseeder $docseeder
	 * @param string $file File name for new word document
	 * @return Word
	 */
	public function populate(\Docseeder\Model\Docseeder $docseeder, $file)
	{
		/**/
		$type = $docseeder->getType();

		/* Get word template file */
		$templateFile = $type->getTemplate()->mainDocfile->getData()->fullpath;

		/* Init template processor */
		$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templateFile);

		/* Get template hooks -> replacement chain */
		$mask = $type->getMask();
		if ( $mask ) {
			$map = $mask->getMap();
			/**/
			foreach( $map as $m ) {
				$templateHook = $m['templateHook'];
				$replacementChain = $m['replacementChain'];
				/* In replacement chain, apply hooks replacements */
				$replacementChain = $this->replacementChain($replacementChain, $docseeder);
				$xmlEscaper = new \PhpOffice\PhpWord\Escaper\Xml();
				$replacementChain = $xmlEscaper->escape($replacementChain);
				$templateProcessor->setValue($templateHook, $replacementChain);
			}
		}

		$templateProcessor->saveAs($file);

		return $this;
	}
} /* End of class*/
