<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Populator;

use Application\Console\Output as Cli;
use Application\Model\DataTestFactory as DataFactory;

/**
 * Writer for Powerpoint files
 * 
 * @see \Application\Model\AbstractTest
 *
 * @example php public/index.php test /Docseeder/Model/Populator/Excel run
 *
 */
class ExcelTest extends \Application\Model\AbstractTest
{

	/**
	 * 
	 */
	public function runTest()
	{
		$templateFilepath = realpath('data/dataSetForTest/test.xlsx');
		#$templateFilepath = realpath('data/dataSetForTest/DQ102.xlsx');
		Cli::green('Template file path: ' . $templateFilepath);

		/* load template type */
		$doctype = DataFactory::doctypeFactory('fortest');

		/* load template document */
		$template = DataFactory::documentFactory('fortest');
		$doctype->setTemplate($template);

		/* get template file */
		$docfile = DataFactory::docfileFactory($templateFilepath);
		$template->mainDocfile = $docfile;

		/* load mask */
		$mask = DataFactory::maskFactory('fortest');
		$hook = new \Docseeder\Model\Template\Hook('hook1');
		$hook->setTemplateHook('sheet1Replace');
		$hook->setReplacementChain('%container.number%');
		$mask->addHook($hook);

		$hook2 = new \Docseeder\Model\Template\Hook('hook2');
		$hook2->setTemplateHook('sheet2Replace');
		$hook2->setReplacementChain('SHEET 2');
		$mask->addHook($hook2);

		$hook3 = new \Docseeder\Model\Template\Hook('hook3');
		$hook3->setTemplateHook('sheet3Replace');
		$hook3->setReplacementChain('%document.number%');
		$mask->addHook($hook3);

		$doctype->setMask($mask);

		/* Load containr */
		$container = DataFactory::containerFactory('workitem', 'container1');

		/* Load document */
		$document = DataFactory::documentFactory('newDocument');
		$document->setDoctype($doctype);
		$document->setParent($container);

		/* Load docseeder */
		$docseeder = new \Docseeder\Model\Docseeder();
		$docseeder->hydrate([]);
		$docseeder->setType($doctype);
		$docseeder->setDocument($document);
		$docseeder->setContainer($container);

		Cli::console('Map content :');
		var_dump($doctype->getMask()->getMap());

		/**/
		@mkdir('/tmp/rbtest');
		$toFilepath = tempnam('/tmp/rbtest', 'excelTest');
		$toFilepath = $toFilepath . '.xlsx';
		$populator = Factory::getFromType('.xlsx');

		assert($populator instanceof Excel);
		assert(get_class($populator) != 'Docseeder\Model\Populator\Generic');
		$populator->populate($docseeder, $toFilepath);

		Cli::green('See result file in ' . $toFilepath);
	}
} /* End of class*/
