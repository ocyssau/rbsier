<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Populator;

use Docseeder\Model\Docseeder;
use PhpOffice\PhpSpreadsheet;

/**
 * Writer for Excel files
 *
 */
class Excel extends \Docseeder\Model\Populator\Generic
{

	/**
	 * 
	 * @var PhpSpreadsheet\Spreadsheet
	 */
	private $spreadsheet;

	/**
	 * @param Docseeder $docseeder
	 * @param string $file File name for new document
	 * @return Excel
	 */
	public function populate(Docseeder $docseeder, $file)
	{
		/**/
		$type = $docseeder->getType();

		/* Get word template file */
		$templateFile = $type->getTemplate()->mainDocfile->getData()->fullpath;

		/* */
		$spreadsheet = PhpSpreadsheet\IOFactory::load($templateFile);
		$this->spreadsheet = $spreadsheet;

		/* set properties */
		$spreadsheet->getProperties()
			->setCreator('Ranchbe Docseeder')
			->setCompany('S.I.E.R, Société industrielle d\'étude et réalisation');

		/* Get template hooks -> replacement chain */
		$mask = $type->getMask();

		if ( $mask ) {
			$map = $mask->getMap();
			/**/
			foreach( $map as $m ) {
				$templateHook = '${' . $m['templateHook'] . '}';
				$replacementChain = $m['replacementChain'];
				$replacementChain = $this->replacementChain($replacementChain, $docseeder);
				$this->searchAndReplace($templateHook, $replacementChain);
			}
		}

		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$writer->save($file);

		return $this;
	}

	/**
	 * @param string $templateHook
	 * @param string $replacementChain
	 * @return Excel
	 */
	private function searchAndReplace($templateHook, $replacementChain)
	{
		/* set range where search hook to replace */
		/* [first row, last row], [first cols, last cols] */
		$range = [
			[
				0,
				100
			],
			[
				0,
				20
			]
		];

		/**/
		$sheets = $this->spreadsheet->getAllSheets();
		$logger = \Ranchbe::get()->getLogger();

		/* @var PhpSpreadsheet\Worksheet $sheet */
		foreach( $sheets as $sheet ) {
			/* Iterate on rows */
			for ($r = $range[0][0]; $r < $range[0][1]; $r++) {
				/* Iterate on columns */
				for ($c = $range[1][0]; $c < $range[1][1]; $c++) {
					/* Iterate on columns */
					$value = $sheet->getCellByColumnAndRow($c, $r)->getValue();
					if ( strstr($value, $templateHook) ) {
						$newValue = str_replace($templateHook, $replacementChain, $value);
						$logger->log('row:' . $r . ' col:' . $c . ' value : "' . $value . '" replace by "' . $newValue . '"');
						$sheet->getCellByColumnAndRow($c, $r)->setValue($newValue);
					}
				}
			}
		}
		return $this;
	}
} /* End of class*/
