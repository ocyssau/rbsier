<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Populator;

/**
 * Exception for not recognize extension file
 *
 */
class BadTypeException extends \Exception
{
} /* End of class*/
