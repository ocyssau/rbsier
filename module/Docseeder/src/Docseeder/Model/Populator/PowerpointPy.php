<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Populator;

use Docseeder\Model\Docseeder;

/**
 * Writer for Powerpoint files
 * 
 * need to install python and python-pptx
 * 
 * 		sudo apt install python python-pip
 * 		sudo pip install python-pptx
 *
 */
class PowerpointPy extends \Docseeder\Model\Populator
{

	public $outputs = [];

	public $return;

	/**
	 * @param Docseeder $docseeder
	 * @param string $file File name for new document
	 * @return PowerpointPy
	 */
	public function populate(Docseeder $docseeder, $file)
	{
		/**/
		$type = $docseeder->getType();

		/* Get word template file */
		$templateFile = $type->getTemplate()->mainDocfile->getData()->fullpath;

		/* Get template hooks -> replacement chain */
		$mask = $type->getMask();

		if ( $mask ) {
			$map = $mask->getMap();
			$searchAndReplace = [];
			/**/
			foreach( $map as $m ) {
				$templateHook = '${' . $m['templateHook'] . '}';
				$replacementChain = $m['replacementChain'];
				$replacementChain = $this->replacementChain($replacementChain, $docseeder);
				$searchAndReplace[] = [
					'hook' => $templateHook,
					'rchain' => $replacementChain
				];
			}

			//var_dump($searchAndReplace);die;

			/* linearize datas */
			$jdatas = json_encode($searchAndReplace, JSON_FORCE_OBJECT);
			$jdatas = base64_encode($jdatas);

			$cmd = sprintf('python module/Docseeder/pysrc/populatorPpt.py -i %s -o %s -m %s', $templateFile, $file, $jdatas);
			$outputs = [];
			$return = null;
			exec($cmd, $outputs, $return);
			$this->outputs = $outputs;
			$this->return = $return;
			if ( $return != 0 ) {
				$logger = \Ranchbe::get()->getLogger();
				foreach( $outputs as $msg ) {
					$logger->log($msg);
				}
				throw new \Exception("Unable to run template replacements. Consult the log to know more.");
			}
		}
		return $this;
	}
} /* End of class*/
