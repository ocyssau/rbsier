<?php
// %LICENCE_HEADER%
namespace Docseeder\Model\Template;

use Rbplm\Any;
use Rbplm\Item;
use Rbplm\Mapped;

/**
 * 
 * To use mask, set map array as :
 * 
 * 		name =>[string name for this hook definition]
 * 		templateHook=>[string to be replace in template document. i.e {$numeroAffaire}]
 * 		replacementChain=>[replacement string of the hook. i.e %container.number%]
 * 
 * the replacement chain may contains some specials code, as here, %contanier.number%, wich will be replaced by current container number property.
 * list of special code are availables in ....
 * 
 * The hooks is other representation of map, where each item is a Hook object and it be used only when editing mask (for Zend\Form Collection compatiblity)
 * 
 * IMPORTANT NOTE :
 * When a hook is added, the map array is updated.
 * When map array is set, hooks collection is not updated.
 * 
 * @see Docseeder\Test\MaskTest
 * @see Docseeder\Test\DeployHelperTest
 * @see Docseeder\Model\Populator\PowerpointTest
 * 
 * @author ocyssau
 *
 */
class Mask extends Any implements \Rbplm\Dao\MappedInterface
{
	use Item, Mapped;

	/** @var string */
	static $classId = 'docseedermask';

	/**
	 *
	 * @var array
	 */
	protected $map = [];

	/**
	 *
	 * @var array
	 */
	protected $hooks = [];

	/**
	 * 
	 * @var integer
	 */
	protected $doctypeId;

	/**
	 *
	 * @param string $name
	 * @return Mask
	 */
	public static function init($name = null)
	{
		$class = get_called_class();
		/* @var Mask $obj */
		$obj = new $class();
		$obj->setName($name);
		$obj->newUid();
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Mask
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['doctypeId'])) ? $this->doctypeId = $properties['doctypeId'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['map'])) ? $this->map = $properties['map'] : null;
		(isset($properties['hooks'])) ? $this->hooks = $properties['hooks'] : null;
		return $this;
	}

	/**
	 */
	public function getMap()
	{
		return $this->map;
	}

	/**
	 *
	 * @param array $map
	 * @return Mask
	 */
	public function setMap($map)
	{
		$this->map = $map;
		return $this;
	}

	/**
	 * 
	 * @return number
	 */
	public function getDoctypeId()
	{
		return $this->doctypeId;
	}

	/**
	 *
	 * @param integer $integer
	 * @return Mask
	 */
	public function setDoctypeId($integer)
	{
		$this->doctypeId = $integer;
		return $this;
	}

	/**
	 * 
	 * @return array
	 */
	public function getHooks()
	{
		return $this->hooks;
	}

	/**
	 *
	 * @param array $hooks
	 * @return Mask
	 */
	public function setHooks($hooks)
	{
		$this->hooks = $hooks;
		$this->map = [];
		foreach( $hooks as $hook ) {
			$this->map[] = $hook->toArray();
		}
		return $this;
	}

	/**
	 *
	 * @param Hook $hook
	 * @return Mask
	 */
	public function addHook($hook)
	{
		$this->hooks[$hook->getName()] = $hook;
		$this->map[] = $hook->toArray();
		return $this;
	}
} /* End of class */
