<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Number;

/**
 * Validate the generated number
 * 
 * This abstract class must be inherited by a concret class adapted to business
 *
 */
abstract class Validator
{
} /* End of class*/
