<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Number;

/**
 * Define rules to generate number.
 * 
 * This abstract class must be inherited by a concret class adapted to business
 *
 */
interface Rule
{

	public function getNumber();
} /* End of class*/
