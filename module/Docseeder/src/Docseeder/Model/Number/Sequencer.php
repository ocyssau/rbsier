<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Number;

/**
 * Define a sequence
 * 
 * This abstract class must be inherited by a concret class adapted to business
 *
 */
abstract class Sequencer
{
} /* End of class*/
