<?php
//%LICENCE_HEADER%
namespace Docseeder\Model\Number\Rule;

/**
 * Define rules to generate number.
 *
 * This abstract class must be inherited by a concret class adapted to business
 *
 */
class DefaultRule implements \Docseeder\Model\Number\Rule
{

	/** @var \Docseeder\Model\Docseeder */
	protected $docseeder;

	/**
	 * @param \Docseeder\Model\Docseeder $docseeder
	 *
	 */
	public function __construct(\Docseeder\Model\Docseeder $docseeder)
	{
		$this->docseeder = $docseeder;
	}

	/**
	 * @return \Docseeder\Model\Number
	 */
	public function getNumber()
	{
		$number = new \Docseeder\Model\Number();

		/* S for document de sortie */
		$number->part('S');

		/* Workitem number, without - */
		$wiNumber = $this->docseeder->getContainer()->getNumber();
		$number->part(str_replace('-', '', $wiNumber));

		/* Type code */
		$typeNumber = $this->docseeder->getType()->getName();
		$number->part($typeNumber);

		/* Order number */
		$serial = new SerialNumber();
		$id = $serial->get($wiNumber, $typeNumber);
		$number->part($id);

		/* date */
		//$now = new \DateTime();
		//$number->part($now->format('dmy'));

		/* add a separator */
		$number->separator('-');

		return $number;
	}
} /* End of class*/
