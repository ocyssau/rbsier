<?php
// %LICENCE_HEADER%
namespace Docseeder\Model\Number\Rule;

use Rbplm\Dao\Connexion;

/** SQL_SCRIPTS >>
 CREATE TABLE IF NOT EXISTS `sequence_docseeder_default`(
 `sequence` int(11) NOT NULL,
 `parameter1` VARCHAR(64) NOT NULL,
 `parameter2` VARCHAR(64) NOT NULL,
 `parameter3` VARCHAR(64) NOT NULL,
 UNIQUE KEY(`parameter1`,`parameter2`,`parameter3`)
 ) ENGINE=MyISAM;
<<*/

/**
 * Get order number from database recorded document
 */
class SerialNumber
{

	/**
	 * @var string
	 */
	public static $tablePrefix = 'sequence_docseeder_';

	/**
	 * @var \PDOStatement
	 */
	protected $updateseqStmt;
	
	
	/**
	 * @var \PDOStatement
	 */
	protected $getseqStmt;
	
	/**
	 * @var string
	 */
	protected $_table;
	
	/**
	 *
	 * @var \PDO
	 */
	protected $connexion;
	
	/**
	 * Constructor
	 *
	 * @param \PDO $conn
	 */
	public function __construct($name = 'default', $conn=null)
	{
		if ( $conn ) {
			$this->connexion = $conn;
		}
		else {
			$this->connexion = Connexion::get();
		}
		
		$this->_table = static::$tablePrefix.$name;
	}
	
	/**
	 * @param string $parameter1
	 * @param string $parameter2
	 * @param string $parameter3
	 * @return integer
	 */
	public function get($parameter1, $parameter2='none', $parameter3='none')
	{
		if(!$this->getseqStmt){
			$table = $this->_table;
			$sql = "INSERT INTO $table (`sequence`,`parameter1`,`parameter2`,`parameter3`) VALUES (1, :parameter1, :parameter2, :parameter3)";
			$sql .= ' ON DUPLICATE KEY UPDATE sequence = (sequence + 1)';
			
			$this->updateseqStmt = $this->connexion->prepare($sql);
			
			$sql = "SELECT `sequence` FROM $table WHERE parameter1 = :parameter1 AND parameter2 = :parameter2 AND parameter3 = :parameter3 LIMIT 1";
			$this->getseqStmt = $this->connexion->prepare($sql);
		}
		
		$bind=array(
			':parameter1'=>$parameter1,
			':parameter2'=>$parameter2,
			':parameter3'=>$parameter3,
		);
		
		$this->updateseqStmt->execute($bind);
		$this->getseqStmt->execute($bind);
		$seqId = $this->getseqStmt->fetchColumn(0);
		
		return $seqId;
	}
}

