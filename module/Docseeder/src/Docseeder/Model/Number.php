<?php
// %LICENCE_HEADER%
namespace Docseeder\Model;

/**
 */
class Number
{

	/**
	 * Element of string for compose number
	 *
	 * @var array
	 */
	protected $parts = [];

	/**
	 * String to separate parts
	 *
	 * @var string
	 */
	protected $separator;

	/**
	 *
	 * @param string $string
	 * @return Number
	 */
	public function part($string)
	{
		$this->parts[] = $string;
		return $this;
	}

	/**
	 *
	 * @param string $string
	 * @return Number
	 */
	public function separator($string)
	{
		$this->separator = $string;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getParts()
	{
		return $this->parts;
	}

	/**
	 *
	 * @return string
	 */
	public function __toString()
	{
		return implode($this->separator, $this->parts);
	}
} /* End of class*/
