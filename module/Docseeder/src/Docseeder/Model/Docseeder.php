<?php
//%LICENCE_HEADER%
namespace Docseeder\Model;

use Rbplm\Any;
use Rbplm\Mapped;
use Rbplm\Ged\Doctype as Type;
use Rbplm\People;

/**
 * Type of of document
 *
 */
class Docseeder extends Any
{

	use Mapped;

	/** @var string */
	static $classId = 'docseeder';

	/** @var string */
	public $description;

	/** @var string */
	public $label;

	/** @var string */
	public $spacename;

	/** @var Type */
	protected $type;

	/** @var integer */
	public $typeId;

	/** @var string */
	public $typeUid;

	/** @var Type */
	protected $container;

	/** @var integer */
	public $containerId;

	/** @var string */
	public $containerUid;

	/** @var \Rbplm\Ged\Document\Version */
	protected $document;

	/** @var integer */
	public $documentId;

	/** @var string */
	public $documentUid;

	/** @var \Rbplm\People\User */
	protected $user;

	/** @var integer */
	public $userId;

	/** @var string */
	public $userUid;

	/**
	 * Docseeder auto generated number.
	 * @var \Docseeder\Model\Number
	 */
	public $autonum;

	/**
	 * @var Populator\Generic
	 */
	public $populator;

	/**
	 * @var \Docseeder\Model\Number\Rule
	 */
	public $generator;

	/**
	 *
	 * @param string $name
	 * @return Docseeder
	 */
	public static function init($name = null)
	{
		$class = get_called_class();
		/* @var Docseeder $obj */
		$obj = new $class();
		$obj->setName($name);
		//$obj->setUid($name);
		$obj->newUid();
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return Docseeder
	 */
	public function hydrate(array $properties)
	{
		$this->mappedHydrate($properties);
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['label'])) ? $this->label = $properties['label'] : null;
		(isset($properties['typeId'])) ? $this->typeId = $properties['typeId'] : null;
		(isset($properties['typeUid'])) ? $this->typeUid = $properties['typeUid'] : null;
		(isset($properties['containerId'])) ? $this->containerId = $properties['containerId'] : null;
		(isset($properties['containerUid'])) ? $this->containerUid = $properties['containerUid'] : null;
		(isset($properties['userId'])) ? $this->userId = $properties['userId'] : null;
		(isset($properties['userUid'])) ? $this->userUid = $properties['userUid'] : null;
		(isset($properties['documentId'])) ? $this->documentId = $properties['documentId'] : null;
		(isset($properties['documentUid'])) ? $this->documentUid = $properties['documentUid'] : null;
		return $this;
	}

	/**
	 * @param array $config
	 * @return Docseeder
	 */
	public function setConfig(array $config)
	{
		$this->config = $config;
		return $this;
	}

	/**
	 * Set with null value to reset.
	 * 
	 * @param Type $type
	 * @return Docseeder
	 */
	public function setType($type)
	{
		if ( $type instanceof Type ) {
			$this->typeId = $type->getId();
			$this->typeUid = $type->getUid();
			$this->type = $type;
		}
		else {
			$this->typeId = null;
			$this->typeUid = null;
			$this->type = null;
		}
		return $this;
	}

	/**
	 * @return Type
	 */
	public function getType($asId = false)
	{
		if ( $asId ) {
			return $this->typeId;
		}
		return $this->type;
	}

	/**
	 * Set with null value to reset.
	 * 
	 * @param \Rbplm\Org\Workitem $container
	 * @return Docseeder
	 */
	public function setContainer($container)
	{
		if ( $container instanceof \Rbplm\Org\Workitem ) {
			$this->containerId = $container->getId();
			$this->containerUid = $container->getUid();
			$this->container = $container;
			$this->spacename = $container->spacename;
		}
		else {
			$this->containerId = null;
			$this->containerUid = null;
			$this->container = null;
		}
		return $this;
	}

	/**
	 * @return \Rbplm\Org\Workitem
	 */
	public function getContainer($asId = false)
	{
		if ( $asId ) {
			return $this->containerId;
		}
		return $this->container;
	}

	/**
	 * Set with null value to reset.
	 * 
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return Docseeder
	 */
	public function setDocument($document)
	{
		if ( $document instanceof \Rbplm\Ged\Document\Version ) {
			$this->documentId = $document->getId();
			$this->documentUid = $document->getUid();
			$this->document = $document;
		}
		else {
			$this->documentId = null;
			$this->documentUid = null;
			$this->document = null;
		}
		return $this;
	}

	/**
	 * @return \Rbplm\Ged\Document
	 */
	public function getDocument($asId = false)
	{
		if ( $asId ) {
			return $this->documentId;
		}
		return $this->document;
	}

	/**
	 * Set with null value to reset.
	 * 
	 * @param \Rbplm\People\User $user
	 * @return Docseeder
	 */
	public function setUser($user)
	{
		if ( $user instanceof \Rbplm\People\User ) {
			$this->userId = $user->getId();
			$this->userUid = $user->getUid();
			$this->user = $user;
		}
		else {
			$this->userId = null;
			$this->userUid = null;
			$this->user = null;
		}
		return $this;
	}

	/**
	 * @return \Rbplm\People\User
	 */
	public function getUser($asId = false)
	{
		if ( $asId ) {
			return $this->userId;
		}
		return $this->user;
	}

	/**
	 * @param Docseeder $docseeder
	 * @return DeployHelper
	 */
	public function initUser()
	{
		/* Init some work */
		$user = People\CurrentUser::get();
		//$user->wildspace = People\User\Wildspace::get($user);
		$this->setUser($user);
		return $this;
	}

	/**
	 *
	 * @param string $type Extension of file without '.'
	 * @return \Docseeder\Model\Populator
	 */
	public function getPopulator($type)
	{
		switch ($type) {
			case 'pdf':
				return new Populator\Word();
				break;
			case 'doc':
				break;
			case 'docx':
				return new Populator\Word();
				break;
			case 'ppt':
				break;
			case 'pptx':
				break;
			case 'xls':
				break;
			case 'xlsx':
				break;
			case 'odt':
				break;
			case 'opt':
				break;
			default:
				throw new Populator\BadTypeException($type . ' is not a valid type');
				break;
		}
	}
} /* End of class*/
