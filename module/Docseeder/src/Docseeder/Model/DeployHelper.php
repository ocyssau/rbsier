<?php
//%LICENCE_HEADER%
namespace Docseeder\Model;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Doctype as Type;
use Exception;
use Ranchbe;
use Rbplm\People\CurrentUser;
use Rbs\Ged\Document\Event as DocumentEvent;

/**
 * $docseeder = new Docseeder\Model\Docseeder;
 * $docseeder->hydrate($datas);
 * 
 * $helper = new Docseeder\Model\DeployHelper($daoFactory);
 * $helper->setConfig(Ranchbe::get()->getConfig());
 * $helper($docseeder)
 *
 */
class DeployHelper
{

	/** @var array */
	protected $config;

	/**
	 * @var \Application\Helper\ErrorStack
	 */
	protected $errorStack;

	/**
	 * @var \Rbs\EventDispatcher\EventManager
	 */
	protected $eventManager;

	/**
	 * @var \Rbplm\Org\Workitem
	 */
	protected $container;

	/**
	 * Factory for user documents and containers
	 * @var DaoFactory
	 */
	protected $targetFactory;

	/**
	 * Factory for documents used as templates
	 * @var DaoFactory
	 */
	protected $bookshopFactory;

	/**
	 * @param array $config
	 * @return DeployHelper
	 */
	public function __construct(DaoFactory $factory)
	{
		$this->bookshopFactory = DaoFactory::get('Bookshop');
		$this->targetFactory = $factory;
		$this->errorStack = Ranchbe::get()->getServiceManager()->getErrorStack();
		$this->eventManager = Ranchbe::get()->getShareEventManager();

		$this->config = [
			'document.number.generator' => \Docseeder\Model\Number\Rule\DefaultRule::class
		];
	}

	/**
	 *
	 * @param \Docseeder\Model\Docseeder $docseeder
	 */
	public function __invoke(Docseeder $docseeder)
	{
		/* Init current user */
		$docseeder->initUser();

		/* load project, workitem, type in docseeder */
		$this->loadContainer($docseeder);
		$this->loadType($docseeder);

		/* generate a new number */
		$number = $this->getAutonum($docseeder);

		try {
			/* load document template and his docfiles */
			$withFiles = true;
			$this->loadTemplate($docseeder, $withFiles);

			/* Load mask */
			try {
				$this->loadMask($docseeder);
			}
			catch( \Rbplm\Dao\NotExistingException $e ) {}
			$withTemplate = true;
		}
		catch( NoneTemplateException $e ) {
			$withTemplate = false;
			$this->errorStack()->warning($e->getMessage());
		}
		catch( \PDOException $e ) {
			$msg = sprintf('Unable to load template %s for this reason:', $this->templateId) . $e->getMessage();
			throw new Exception($msg);
		}

		/* save the document, without file */
		$container = $docseeder->getContainer();
		$type = $docseeder->getType();

		$numberAsString = $number->__toString();
		$document = Document\Version::init($numberAsString)->setNumber($numberAsString);
		$document->setParent($container);
		$document->setDoctype($type);
		$document->description = $docseeder->description;
		$document->setLabel($docseeder->label);
		/* set as checkout by current user */
		$document->lock(\Rbplm\Ged\AccessCode::CHECKOUT, CurrentUser::get());

		$workitemFactory = $this->targetFactory;

		/* Set listener */
		$event = new DocumentEvent(DocumentEvent::PRE_CREATE, $document, $workitemFactory);
		$this->getEventManager()->trigger($event);

		/* save */
		$workitemFactory->getDao($document->cid)->save($document);

		/* set document to docseeder */
		$docseeder->setDocument($document);

		if ( $withTemplate ) {
			$templateDocument = $type->getTemplate();

			/* File in Ws */
			$extension = $templateDocument->mainDocfile->getData()->extension;

			if ( $docseeder->label ) {
				$flabel = \Rbs\Number::normalize($docseeder->label);
				$fileName = $number->__toString() . '-' . $flabel . $extension;
			}
			else {
				$fileName = $number->__toString() . $extension;
			}

			$wildspace = $docseeder->getUser()->wildspace;
			$file = $wildspace->getPath() . '/' . $fileName;

			/* get populator, write file in ws */
			$populator = $this->getPopulator($docseeder);
			$populator->populate($docseeder, $file);

			/* Init the docfile */
			$docfile = \Rbplm\Ged\Docfile\Version::init();
			$docfile->lock(\Rbplm\Ged\AccessCode::CHECKOUT, CurrentUser::get());
			$docfile->fsdata = new \Rbplm\Sys\Fsdata($file);
			$docfile->dao = $workitemFactory->getDao($docfile->cid);

			/* Associate to document and save */
			$documentService = Ranchbe::get()->getServiceManager()->getDocumentService($workitemFactory);
			$documentService->wildspace = $wildspace;
			$documentService->addFileToDocument($document, $docfile, $docfile->fsdata); //docfile is save here
		}

		$this->getEventManager()->trigger($event->setName(DocumentEvent::POST_CREATE));

		return [
			'number' => $numberAsString,
			'document' => $document
		];
	}

	/**
	 * @param array $config
	 * @return DeployHelper
	 */
	public function setConfig($config)
	{
		$this->config = $config;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * @return array
	 */
	public function errorStack()
	{
		return $this->errorStack;
	}

	/**
	 * @param Docseeder $docseeder
	 * @return DeployHelper
	 */
	public function setDocseeder(Docseeder $docseeder)
	{
		$this->docseeder = $docseeder;
		return $this;
	}

	/**
	 * Load project and workitem in $docseeder
	 * 
	 * @param Docseeder $docseeder
	 * @return DeployHelper
	 */
	public function loadContainer(Docseeder $docseeder)
	{
		$workitemFactory = $this->targetFactory;

		/* load workitem */
		if ( !$this->container || $docseeder->containerId != $this->container->getId() ) {
			try {
				$container = new \Rbplm\Org\Workitem();
				$container->dao = $workitemFactory->getDao($container->cid);
				/* load extended properties in dao */
				$extendedLoader = new \Rbs\Extended\Loader($workitemFactory);
				$extendedLoader->load($container->cid, $container->dao);
				/* load container */
				$container->dao->loadFromId($container, $docseeder->containerId);
			}
			catch( Exception $e ) {
				throw new Exception(sprintf('Unable to load container from id %s > %s ', $docseeder->containerId, $e->getMessage()));
			}

			/* load project */
			try {
				$project = new \Rbplm\Org\Project();
				$workitemFactory->getDao($project->cid)->loadFromId($project, $container->parentId);
				$container->setParent($project);
			}
			catch( Exception $e ) {
				throw new Exception(sprintf('Unable to load project from id %s > %s', $container->parentId, $e->getMessage()));
			}

			$this->container = $container;
		}

		$docseeder->setContainer($this->container);

		return $this;
	}

	/**
	 * Load project, workitem, type in $docseeder
	 * 
	 * @param Docseeder $docseeder
	 * @return DeployHelper
	 */
	public function loadType(Docseeder $docseeder)
	{
		$workitemFactory = $this->targetFactory;
		if ( $docseeder->getType() == null ) {
			/* load type */
			try {
				$type = new Type();
				$workitemFactory->getDao($type->cid)->loadFromId($type, $docseeder->typeId);
				$docseeder->setType($type);
			}
			catch( Exception $e ) {
				$docseeder->setType(null);
				throw new Exception(sprintf('Unable to load type from id %s > %s', $docseeder->typeId, $e->getMessage()));
			}
		}
		return $this;
	}

	/**
	 * Load document template and his docfiles if $withFiles == true
	 *
	 * @param Docseeder $docseeder
	 * @return DeployHelper
	 */
	public function loadTemplate(Docseeder $docseeder, $withFiles = True)
	{
		$type = $docseeder->getType();
		if ( $type->getTemplate() == null ) {
			$bookshopFactory = $this->bookshopFactory;

			if ( !$type->templateId ) {
				throw new NoneTemplateException(sprintf('Doctype %s have not a associated template. Edit it, and check template.', $type->getName()));
			}

			$templateDocument = new Document\Version();
			$templateDocument->dao = $bookshopFactory->getDao($templateDocument->cid);
			/* load extend properties */
			$extendsloader = new \Rbs\Extended\Loader($bookshopFactory);
			$extendsloader->loadFromDocumentId($type->templateId, $templateDocument->dao);
			/* load document*/
			$templateDocument->dao->loadFromId($templateDocument, $type->templateId);

			if ( $withFiles ) {
				/* load docfile template */
				$bookshopFactory->getDao(Docfile\Version::$classId)->loadDocfiles($templateDocument);
				$templateDocument->mainDocfile = current($templateDocument->getDocfiles());
				if ( !$templateDocument->mainDocfile ) {
					throw new NoneTemplateException(sprintf('Template %s have not a associated file. Edit it, and check files.', $templateDocument->getName()));
				}
			}

			$type->setTemplate($templateDocument);
		}

		return $this;
	}

	/**
	 * Load document template and his docfiles if $withFiles == true
	 *
	 * @param Docseeder $docseeder
	 * @return DeployHelper
	 */
	public function loadMask(Docseeder $docseeder)
	{
		$type = $docseeder->getType();
		if ( $type->getMask() == null ) {
			$mask = new \Docseeder\Model\Template\Mask();
			$this->targetFactory->getDao($mask->cid)->loadFromTypeId($mask, $type->getId());
			$type->setMask($mask);
		}
		return $this;
	}

	/**
	 * @param Docseeder $docseeder
	 * @return \Docseeder\Model\Number\Rule
	 */
	public function getGenerator(Docseeder $docseeder)
	{
		if ( !isset($docseeder->generator) ) {
			/* get a number from rule */
			$generatorClass = $docseeder->getType()->getNumberGeneratorClass();
			if ( !$generatorClass ) {
				$generatorClass = $this->config['document.number.generator'];
			}
			$docseeder->generator = new $generatorClass($docseeder);
		}
		return $docseeder->generator;
	}

	/**
	 * Factory for populator
	 * 
	 * @param Docseeder $docseeder
	 * @throws Exception
	 * @return Populator\Generic
	 */
	public function getPopulator(Docseeder $docseeder)
	{
		if ( !isset($docseeder->populator) ) {
			$extension = $docseeder->getType()
				->getTemplate()
				->getMainDocfile()
				->getData()->extension;
			$populator = Populator\Factory::getFromType($extension);
			$docseeder->populator = $populator;
		}
		return $docseeder->populator;
	}

	/**
	 * Factory for docseeder autogen number
	 * 
	 * @param Docseeder $docseeder
	 * @return \Docseeder\Model\Number
	 */
	public function getAutonum(Docseeder $docseeder)
	{
		if ( !isset($docseeder->autonum) ) {
			$generator = $this->getGenerator($docseeder);
			$docseeder->autonum = $generator->getNumber();
		}
		return $docseeder->autonum;
	}

	/**
	 * 
	 * @return \Rbs\EventDispatcher\EventManager
	 */
	protected function getEventManager()
	{
		return $this->eventManager;
	}
} /* End of class*/
