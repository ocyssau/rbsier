<?php
namespace Docseeder\Test;

use Rbplm\Ged\Doctype;
use Rbs\Space\Factory as DaoFactory;

/**
 *
 * @see \Application\Model\AbstractTest
 *
 *
 * @example php public/index.php test /Docseeder/Test/MaskTest all
 */
class MaskTest extends \Application\Model\AbstractTest
{

	/**
	 *
	 */
	public function addhookTest()
	{
		$mask = new \Docseeder\Model\Template\Mask();
		$hook = new \Docseeder\Model\Template\Hook('hook1');
		$hook->setTemplateHook('{$docnum}');
		$hook->setReplacementChain('%document.number%');
		$mask->addHook($hook);

		$map = $mask->getMap();
		assert($map[0]['templateHook'] == '{$docnum}');
		assert($map[0]['replacementChain'] == '%document.number%');

		return $this;
	}

	/**
	 *
	 */
	public function loadTest()
	{
		$factory = DaoFactory::get('Bookshop');

		/* load template type */
		$type = 'SIER-CR';
		$doctype = new Doctype();
		$factory->getDao($doctype->cid)->loadFromNumber($doctype, $type);
		$id = $doctype->getId();

		/* load mask */
		$mask = new \Docseeder\Model\Template\Mask();
		$factory->getDao($mask->cid)->loadFromTypeId($mask, $id);

		/* hooks are not setted */
		assert(count($mask->getHooks()) == 0);

		var_dump($mask->getMap());

		return $this;
	}
}
