<?php
namespace Docseeder\Test;

use Rbplm\Ged\Doctype;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Docseeder\Model\DeployHelper;

/**
 *
 * @see \Application\Model\AbstractTest
 * 
 * 
 * @example php public/index.php test /Docseeder/Test/DeployHelper all
 *
 */
class DeployHelperTest extends \Application\Model\AbstractTest
{

	/**
	 *
	 */
	public function runTest()
	{
		//assert(false===true, 'for test');
	}

	/**
	 * 
	 */
	public function tutorial()
	{
		$type = 'SIER-DQ08';
		$target = '%AFFAIRE%';
		$target = str_replace('%AFFAIRE%', '/tmp', $target);

		/* init helpers */
		$deployHelper = new DeployHelper();
		$factory = DaoFactory::get('Bookshop');

		/* load template type */
		$doctype = new Doctype();
		$factory->getDao($doctype->cid)->loadFromNumber($doctype, $type);

		/* get template document */
		$templateId = $doctype->getTemplate(true);
		$template = new Document\Version();
		$factory->getDao($template->cid)->loadFromId($template, $templateId);

		/* get template file */
		$template->dao->loadDocfiles($template);
		$template->mainDocfile = current($template->getDocfiles());

		/* Create file from template */
		$template->mainDocfile->getData()->copy($target, 0777);

		/* load mask */
		$mask = new \Docseeder\Model\Template\Mask();
		$factory->getDao($mask->cid)->loadFromTypeId($mask, $doctype->getId());
		$doctype->setMask($mask);

		/* Set type of template to helper */
		$deployHelper->setDoctype($doctype);
	}
}
