<?php
namespace Vault\Form\Distantsite;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * 
	 * @param \Rbs\Space\Factory $factory
	 * @param string $namespace
	 */
	public function __construct($factory, $namespace)
	{
		parent::__construct($factory, $namespace);

		$this->template = 'vault/distantsite/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$this->setAttribute('id', 'repositfilterform');

		/* Name */
		$this->add(array(
			'name' => 'find_name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Input Name here',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Name'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_name',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 * @param \Rbplm\Sys\Filesystem\Filter $filter
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* NAME */
		if ( $datas['find_name'] ) {
			$filter->andFind($datas['find_name'], 'name', Op::OP_CONTAINS);
		}

		$filter->sort('name', 'asc');

		return $this;
	}
}

