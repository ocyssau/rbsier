<?php
namespace Vault\Form\Distantsite;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * 
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/* @var string */
	public $template;

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('distantsiteEdit');
		$this->template = 'vault/distantsite/editform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Set A Convinient Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Mode */
		$this->add(array(
			'name' => 'method',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'selectpicker method-select',
				'multiple' => false,
				'data-size' => 10,
				'data-live-search' => 'true'
			),
			'options' => array(
				'label' => tra('Method used for replication'),
				'value_options' => $this->_getMethods()
			)
		));

		/* Parameters */
		$this->add(array(
			'name' => 'parameters',
			'type' => 'Zend\Form\Element\TextArea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 100
			),
			'options' => array(
				'label' => tra('Parameters')
			)
		));

		/* AuthUser */
		$this->add(array(
			'name' => 'authUser',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 50
			),
			'options' => array(
				'label' => tra('Username')
			)
		));

		/* AuthPassword */
		$this->add(array(
			'name' => 'authPassword',
			'type' => 'Zend\Form\Element\Password',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 50
			),
			'options' => array(
				'label' => tra('Password')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * 
	 * @param boolean $bool
	 * @return EditForm
	 */
	public function setEditMode($bool)
	{
		$this->editMode = $bool;
		if ( $bool ) {
			//$this->get('method')->setAttribute('disabled', true);
		}
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getMethods()
	{
		$selectSet = [];
		try {
			//$selectSet[null] = 'Nothing...';
			$selectSet['rsync'] = tra('Rsync');
			//$selectSet['ftp'] = tra('Ftp');
			//$selectSet['copy'] = tra('File Copy');
			//$selectSet['symlink'] = tra('Sym links');
			//$selectSet['hardlink'] = tra('Hard links');
		}
		catch( \Exception $e ) {
			throw $e;
		}
		return $selectSet;
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$array = array(
			'id' => array(
				'required' => false
			),
			'name' => array(
				'required' => true
			),
			'description' => array(
				'required' => true
			),
			'method' => array(
				'required' => true
			),
			'parameters' => array(
				'required' => false
			),
			'authUser' => array(
				'required' => false
			),
			'authPassword' => array(
				'required' => false
			)
		);
		return $array;
	}
}
