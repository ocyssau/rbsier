<?php
namespace Vault\Form\Reposit;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use RBs\Space\Space;

/**
 * 
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/* @var string */
	public $template;

	/* @var \Ranchbe */
	public $ranchbe;

	/* @var boolean */
	public $editMode = false;

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('repositEdit');

		$this->template = 'vault/reposit/editform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Number */
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number')
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Set A Convinient Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Spacename */
		$this->add(array(
			'name' => 'spacename',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 1
			),
			'options' => array(
				'label' => tra('In Space'),
				'value_options' => $this->_getSpacename(),
				'multiple' => false
			)
		));

		/* Max Size */
		$this->add(array(
			'name' => 'maxsize',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'placeholder' => tra('Max Size'),
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Max Size')
			)
		));

		/* Max Count */
		$this->add(array(
			'name' => 'maxcount',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'placeholder' => tra('Max Files Number'),
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Max Files Number')
			)
		));

		/* is actif */
		$this->add(array(
			'name' => 'active',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'placeholder' => tra('Is Actif'),
				'class' => 'form-check-input'
			),
			'options' => array(
				'label' => tra('Is Actif')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * 
	 * @param boolean $bool
	 * @return EditForm
	 */
	public function setEditMode($bool)
	{
		$this->editMode = $bool;
		if ( $bool ) {
			$this->get('number')->setAttribute('disabled', true);
			$this->get('spacename')->setAttribute('disabled', true);
		}
		return $this;
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$array = array(
			'id' => array(
				'required' => false
			),
			'name' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/[a-zA-Z0-9 -_]/i',
							'messages' => array(
								\Zend\Validator\Regex::INVALID => 'Use only letters and numbers, space, -,_'
							)
						)
					)
				)
			),
			'description' => array(
				'required' => false
			),
			'maxsize' => array(
				'required' => false
			),
			'maxcount' => array(
				'required' => false
			),
			'active' => array(
				'required' => false
			)
		);
		if ( $this->editMode == false ) {
			$array = array_merge($array, array(
				'number' => array(
					'required' => true,
					'validators' => array(
						array(
							'name' => 'Regex',
							'options' => array(
								'pattern' => '/^[a-zA-Z0-9-_.\/]+$/i',
								'messages' => array(
									\Zend\Validator\Regex::INVALID => 'Use letters without spaces'
								)
							)
						)
					)
				),
				'spacename' => array(
					'required' => true
				)
			));
		}
		else {
			$array = array_merge($array, array(
				'number' => array(
					'required' => false
				),
				'spacename' => array(
					'required' => false
				)
			));
		}
		return $array;
	}

	/**
	 * @return array
	 */
	protected function _getSpacename()
	{
		return array(
			Space::NAME_DEFAULT => tra('Default'),
			Space::NAME_WORKITEM => tra('Workitem'),
			Space::NAME_CADLIB => tra('Cadlib'),
			Space::NAME_BOOKSHOP => tra('Bookshop'),
			Space::NAME_MOCKUP => tra('Mockup')
		);
	}
}
