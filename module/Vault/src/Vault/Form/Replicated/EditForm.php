<?php
namespace Vault\Form\Replicated;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Vault\DistantSite;
use Rbplm\Vault\Reposit;

/**
 * 
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/* @var string */
	public $template;

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('replicatedEdit');

		$this->template = 'vault/replicated/editform';

		$inputFilter = new InputFilter();
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter($inputFilter);

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'repositId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Set A Convinient Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Distant Site */
		$this->add(array(
			'name' => 'distantSiteId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'selectpicker distantsite-select',
				'multiple' => false,
				'data-size' => 10,
				'data-live-search' => 'true'
			),
			'options' => array(
				'label' => tra('Distant Site'),
				'value_options' => $this->_getDistantsite()
			)
		));

		/* Reposit Id */
		$this->add(array(
			'name' => 'repositId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'selectpicker reposit-select',
				'multiple' => false,
				'data-size' => 10,
				'data-live-search' => 'true'
			),
			'options' => array(
				'label' => tra('Reposit'),
				'value_options' => $this->_getReposit()
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * 
	 * @return array
	 */
	protected function _getDistantsite()
	{
		$selectSet = [];
		$daoFactory = DaoFactory::get();
		try {
			$list = $daoFactory->getList(DistantSite::$classId);
			$list->select(array(
				'id',
				'name'
			));
			$list->load();
			$selectSet[null] = 'Nothing...';
			foreach( $list as $item ) {
				$selectSet[$item['id']] = $item['name'];
			}
		}
		catch( \Exception $e ) {
			throw $e;
		}

		return $selectSet;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getReposit()
	{
		$selectSet = [];
		$daoFactory = DaoFactory::get();
		try {
			$list = $daoFactory->getList(Reposit::$classId);
			$list->select(array(
				'id',
				'name'
			));
			$list->load();
			$selectSet[null] = 'Nothing...';
			foreach( $list as $item ) {
				$selectSet[$item['id']] = $item['name'];
			}
		}
		catch( \Exception $e ) {
			throw $e;
		}

		return $selectSet;
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$array = array(
			'id' => array(
				'required' => false
			),
			'name' => array(
				'required' => true
			),
			'description' => array(
				'required' => true
			),
			'distantSiteId' => array(
				'required' => true,
				'filters' => array(
					array(
						'name' => 'Null',
						'options' => array(
							'type' => 'all'
						)
					)
				)
			)
		);
		return $array;
	}
}
