<?php
namespace Vault\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Dao\Sier\Filter;
use Rbs\Vault\DistantSite;

/**
 *
 * @author olivier
 *
 */
class DistantsiteController extends AbstractController
{

	/**
	 *
	 * @var string $pageId
	 */
	public $pageId = 'vault_distant_site';

	/**
	 *
	 * @var string $defaultSuccessForward
	 */
	public $defaultSuccessForward = 'vault/distantsite/index';

	/**
	 *
	 * @var string $defaultFailedForward
	 */
	public $defaultFailedForward = 'vault/distantsite/index';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('distantsite');
		$this->layout()->tabs = $tabs;

		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 * @return \Application\View\ViewModel|string
	 */
	public function indexAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {}

		/* */
		$factory = DaoFactory::get();

		/* Filter and paginations */
		$filter = new Filter('', false);
		$filterForm = new \Vault\Form\Distantsite\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		/* Get list*/
		$dao = $factory->getDao(DistantSite::$classId);
		$list = $factory->getList(DistantSite::$classId);
		$list->dao = $dao;
		$select = array();
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = $asSys . ' as ' . $asApp;
		}
		$filter->select($select);

		/* */
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->setMaxLimit($list->countAll($filter));
		$paginator->load([
			'default' => [
				'orderby' => 'name',
				'order' => 'asc',
				'limit' => 50
			]
		]);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		/* */
		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;
		$view->pageTitle = tra('Available Distant Sites');

		/* */
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		/**/
		$factory = DaoFactory::get();
		$dao = $factory->getDao(DistantSite::$classId);
		$site = DistantSite::init()->setName('Site 1');

		/**/
		$form = new \Vault\Form\Distantsite\EditForm($view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($site);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$dao->save($site);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error("Creation of site FAILED :" . $e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = tra('Create Distant Site');
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		$siteId = $this->params()->fromRoute('id');

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(DistantSite::$classId);
		$site = new DistantSite();
		$dao->loadFromId($site, $siteId);

		/**/
		$form = new \Vault\Form\Distantsite\EditForm($view);
		$form->setEditMode(true);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($site);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($site);
				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Edit Distant Site %s'), $site->getName());
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$siteIds = $request->getQuery('checked', array());
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(DistantSite::$classId);

		foreach( $siteIds as $siteId ) {
			try {
				/* delete directory on file system */
				//$site = new DistantSite();
				//$dao->loadFromId($site, $siteId);
				/* delete record */
				$dao->setOption('withtrans', false);
				$dao->getConnexion()->beginTransaction();
				$dao->deleteFromId($siteId);
				$dao->getConnexion()->commit();
			}
			catch( \PDOException $e ) {
				$dao->getConnexion()->rollback();
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $siteId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$dao->getConnexion()->rollback();
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}
}
