<?php
namespace Admin\Controller;

use Application\Controller\AbstractController;

/**
 */
class TrashController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'reposit_trash';

	/**
	 * @var string
	 */
	public $defaultSuccessForward = 'vault/reposit/index';

	/**
	 * @var string
	 */
	public $defaultFailedForward = 'vault/reposit/index';

	/**
	 */
	public function init()
	{
		parent::init();

		$toUrl = $this->url()->fromRoute('vault', array(
			'action' => 'index'
		));

		$this->ifSuccessForward = $toUrl;
		$this->ifFailedForward = $toUrl;
		$this->basecamp()->setForward($this);

		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('trash');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 * Get files in trash
	 */
	public function putintrashAction()
	{
		return $this->view;
	}
}
