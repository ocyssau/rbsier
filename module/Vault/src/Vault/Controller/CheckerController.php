<?php
namespace Vault\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Vault;

/**
 *
 * @author olivier
 *
 */
class CheckerController extends AbstractController
{

	/**
	 *
	 * @var string $pageId
	 */
	public $pageId = 'vault_checker';

	/**
	 *
	 * @var string $defaultSuccessForward
	 */
	public $defaultSuccessForward = 'vault/reposit/index';

	/**
	 *
	 * @var string $defaultFailedForward
	 */
	public $defaultFailedForward = 'vault/reposit/index';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('reposit');
		$this->layout()->tabs = $tabs;

		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 * 
	 */
	public function checkAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$repositIds = $request->getQuery('checked', array());
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Vault\Reposit::$classId);
		$logs = [];

		foreach( $repositIds as $repositId ) {
			try {
				$reposit = new Vault\Reposit();
				$dao->loadFromId($reposit, $repositId);
			}
			catch( \PDOException $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}

			$logs = array_merge($this->check($reposit), $logs);
		}

		$view->logs = $logs;
		$view->pageTitle = 'Check of vaults';
		return $view;
	}

	/**
	 * 
	 * @param Vault\Reposit $reposit
	 */
	protected function check(Vault\Reposit $reposit)
	{
		$path = $reposit->getPath();
		$log = [];

		/* check if exists*/
		if ( !is_dir($path) ) {
			$log[] = sprintf('Path %s is not existing, try to create it', $path);
			$reposit::initPath($path);
		}
		/* check if is writable */
		if ( !is_writable($path) ) {
			$log[] = sprintf('Path %s is not writable, you must check permissions', $path);
		}

		/* check if __versions exists */
		$versionPath = $reposit->getVersionPath();
		if ( !is_dir($versionPath) ) {
			$log[] = sprintf('Path %s is not existing, try to create it', $versionPath);
			$reposit::initPath($path);
		}
		/* check if __versions is writable */
		if ( !is_writable($versionPath) ) {
			$log[] = sprintf('Path %s is not writable, you must check permissions', $versionPath);
		}

		/* check if __iterations exists */
		$iterationPath = $reposit->getIterationPath();
		if ( !is_dir($iterationPath) ) {
			$log[] = sprintf('Path %s is not existing, try to create it', $iterationPath);
			$reposit::initPath($path);
		}
		/* check if __iterations is writable */
		if ( !is_writable($iterationPath) ) {
			$log[] = sprintf('Path %s is not writable, you must check permissions', $iterationPath);
		}

		/* check if __trash exists */
		$trashPath = $reposit->getTrashPath();
		if ( !is_dir($trashPath) ) {
			$log[] = sprintf('Path %s is not existing, try to create it', $trashPath);
			$reposit::initPath($path);
		}
		/* check if __trash is writable */
		if ( !is_writable($trashPath) ) {
			$log[] = sprintf('Path %s is not writable, you must check permissions', $trashPath);
		}

		/* check recorded file - files on filesystem */
		$missings = $this->checkFilesystemDbCoherence($reposit);
		if ( count($missings) > 0 ) {
			$log = array_merge($log, $missings);
		}

		return $log;
	}

	/**
	 * Check if each file recorded in db is existing as define
	 * 
	 * @param Vault\Reposit $reposit
	 * @return array
	 */
	protected function checkFilesystemDbCoherence(Vault\Reposit $reposit)
	{
		$path = $reposit->getPath();
		$iterationPath = $reposit->getIterationPath();
		$versionPath = $reposit->getVersionPath();
		$missings = [];
		$connexion = DaoFactory::get()->getConnexion();

		$sql = "SELECT name,path FROM workitem_doc_files";
		$sql .= " UNION ALL ";
		$sql .= "SELECT name,path FROM bookshop_doc_files";
		$sql .= " UNION ALL ";
		$sql .= "SELECT name,path FROM cadlib_doc_files";
		$filter = 'df.path = :path OR df.path = :iterationPath OR df.path = :versionPath';
		$bind = [
			':path' => $path,
			':iterationPath' => $iterationPath,
			':versionPath' => $versionPath
		];

		$sql1 = "SELECT df.* FROM ($sql) AS df WHERE " . $filter;
		$stmt = $connexion->prepare($sql1);
		$stmt->execute($bind);

		while( $entry = $stmt->fetch(\PDO::FETCH_ASSOC) ) {
			$file = $entry['path'] . '/' . $entry['name'];
			if ( !is_file($file) ) {
				$missings[] = sprintf('File %s is missing', $file);
			}
		}

		return $missings;
	}

	/**
	 * Check if each file recorded in db is existing as define
	 *
	 * @param Vault\Reposit $reposit
	 * @return array
	 */
	protected function countFiles(Vault\Reposit $reposit)
	{
		$path = $reposit->getPath();
		$iterationPath = $reposit->getIterationPath();
		$versionPath = $reposit->getVersionPath();
		$connexion = DaoFactory::get()->getConnexion();

		$filter = 'df.path = :path OR df.path = :iterationPath OR df.path = :versionPath';
		$bind = [
			':path' => $path,
			':iterationPath' => $iterationPath,
			':versionPath' => $versionPath
		];

		$sql = "SELECT path FROM workitem_doc_files";
		$sql .= " UNION ALL SELECT path FROM bookshop_doc_files";
		$sql .= " UNION ALL SELECT path FROM cadlib_doc_files";
		$sqlcount = "SELECT COUNT(*) FROM ($sql) AS df WHERE " . $filter;
		$countstmt = $connexion->prepare($sqlcount);
		$countstmt->execute($bind);

		return $countstmt->fetchColumn(0);
	}
}
