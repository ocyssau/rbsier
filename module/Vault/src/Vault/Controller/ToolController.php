<?php
namespace Vault\Controller;

use Application\Controller\AbstractController;
use Rbs\Vault\MissingFileDao;

/**
 *
 * @author olivier
 *
 */
class ToolController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'tool_index';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'admin/tools';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'home';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('tools');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$view = $this->view;
		return $view;
	}

	/**
	 * Get files without documents
	 */
	public function getmissingfilesAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$view = $this->view;
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$missingFile = new MissingFileDao();

		/** @var \Rbs\Dao\Sier\DaoList $list */
		$list = new \Rbs\Dao\Sier\DaoList($missingFile->getTable());
		$select = array(
			'id',
			'uid',
			'document_id as documentId',
			'path',
			'name',
			"CONCAT(path, '/', name) as file",
			'spacename'
		);

		$paginator = new \Application\Form\PaginatorForm('tools_getmissingfiles');
		$paginator->load([
			'default' => [
				'orderby' => 'name',
				'order' => 'asc',
				'limit' => 50
			]
		])
			->setMaxLimit(5000)
			->bindToFilter($filter)
			->bindToView($view)
			->save();

		$list = $missingFile->getMissingFiles($select, $filter);

		$view->list = $list;
		$view->columns = array(
			array(
				'name' => 'uid',
				'label' => 'Uid'
			),
			array(
				'name' => 'spacename',
				'label' => 'Spacename'
			)
		);

		$view->paginator = $paginator;
		$view->pageTitle = 'Missing files in vault';
		return $view;
	}

	/**
	 * Update index of missing files. This index is saved in table tools_missing_files
	 */
	public function updatemissingfilesAction()
	{
		$missingFile = new MissingFileDao();
		$c = $missingFile->updateMissingFiles();

		$view = $this->view;
		$view->pageTitle = 'Update of missings files index is success';
		$view->count = $c;
		return $view;
	}
}
