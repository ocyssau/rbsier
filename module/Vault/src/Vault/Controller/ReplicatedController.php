<?php
namespace Vault\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Dao\Sier\Filter;
use Rbs\Vault\Replicated;
use Rbplm\Dao\Filter\Op;
use Rbplm\Vault\Reposit;
use Rbs\Vault\DistantSite;

/**
 *
 * @author olivier
 *
 */
class ReplicatedController extends AbstractController
{

	/**
	 *
	 * @var string $pageId
	 */
	public $pageId = 'vault_replicated';

	/**
	 *
	 * @var string $defaultSuccessForward
	 */
	public $defaultSuccessForward = 'vault/replicated/index';

	/**
	 *
	 * @var string $defaultFailedForward
	 */
	public $defaultFailedForward = 'vault/replicated/index';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('replicated');
		$this->layout()->tabs = $tabs;

		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 * @return \Application\View\ViewModel|string
	 */
	public function indexAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {}

		$ofRepositId = $this->params()->fromRoute('id');

		$factory = DaoFactory::get();

		/* Filter and paginations */
		$filter = new Filter('', false);
		$filterForm = new \Vault\Form\Replicated\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$filter->with([
			'table' => 'vault_reposit',
			'alias' => 'reposit',
			'lefton' => 'reposit_id',
			'righton' => 'id'
		]);

		$filter->with([
			'table' => 'vault_distant_site',
			'alias' => 'site',
			'lefton' => 'distantsite_id',
			'righton' => 'id'
		]);

		$filter->setAlias('replicated');

		/* Get list*/
		$dao = $factory->getDao(Replicated::$classId);
		$list = $factory->getList(Replicated::$classId);
		$list->dao = $dao;
		$select = array();
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = 'replicated.' . $asSys . ' as ' . $asApp;
		}
		$select[] = 'reposit.name AS repositName';
		$select[] = 'site.name AS siteName';
		$filter->select($select);

		/* select only replicated of the selected reposit */
		if ( $ofRepositId ) {
			$filter->andfind($ofRepositId, $dao->toSys('repositId'), OP::EQUAL);
		}

		/* */
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->load([
			'default' => [
				'orderby' => 'name',
				'order' => 'asc',
				'limit' => 50
			]
		]);
		$paginator->setMaxLimit($list->countAll($filter))
			->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		/* */
		$list->load($filter, $filterForm->bind);

		if ( $list->count() == 0 ) {
			return $this->redirect()->toRoute('vault-replicated', [
				'action' => 'create',
				'id' => $ofRepositId
			]);
		}

		$view->list = $list->toArray();
		$view->filter = $filterForm;
		if ( $ofRepositId ) {
			$view->pageTitle = sprintf(tra('Replications Of Reposit %s'), $ofRepositId);
		}
		else {
			$view->pageTitle = tra('Replications Of All Reposits');
		}

		/* */
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			$ofRepositId = $request->getPost('repositId', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		$ofRepositId = $this->params()->fromRoute('id');

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		/**/
		$replication = Replicated::init()->setName('');
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Replicated::$classId);

		/* load reposit */
		if ( $ofRepositId ) {
			$reposit = new Reposit();
			$reposit->dao = $factory->getDao(Reposit::$classId);
			$reposit->dao->loadFromId($reposit, $ofRepositId);
			$replication->setReposit($reposit);
		}

		/**/
		$form = new \Vault\Form\Replicated\EditForm($view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($replication);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					/* load distant site */
					$distantSite = new DistantSite();
					$distantSite->dao = $factory->getDao(DistantSite::$classId);
					$distantSite->dao->loadFromId($distantSite, $replication->distantSiteId);
					$replication->setDistantSite($distantSite);
					$dao->save($replication);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error("Creation of replication FAILED :" . $e->getMessage());
				}
				return $this->successForward();
			}
		}

		if ( isset($reposit) ) {
			$view->pageTitle = sprintf(tra('Create Replication for reposit %s'), $reposit->getName());
		}
		else {
			$view->pageTitle = tra('Create Replication');
		}

		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		$replicatedId = $this->params()->fromRoute('id');

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$replicated = new Replicated();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Replicated::$classId);
		$dao->loadFromId($replicated, $replicatedId);

		/**/
		$form = new \Vault\Form\Replicated\EditForm($view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($replicated);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($replicated);
				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Edit Replication %s'), $replicated->getName());
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$replicatedIds = $request->getQuery('checked', array());
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Replicated::$classId);

		foreach( $replicatedIds as $replicatedId ) {
			try {
				/* delete directory on file system */
				$replicated = new Replicated();
				$dao->loadFromId($replicated, $replicatedId);

				/* delete record */
				$dao->setOption('withtrans', false);
				$dao->getConnexion()->beginTransaction();
				$dao->deleteFromId($replicatedId);
				$dao->getConnexion()->commit();
			}
			catch( \PDOException $e ) {
				$dao->getConnexion()->rollback();
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $replicatedId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$dao->getConnexion()->rollback();
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}
}
