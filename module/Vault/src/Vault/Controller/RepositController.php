<?php
namespace Vault\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Dao\Sier\Filter;
use Rbplm\Vault;
use Rbplm\People;
use DateTime;

/**
 *
 * @author olivier
 *
 */
class RepositController extends AbstractController
{

	/**
	 *
	 * @var string $pageId
	 */
	public $pageId = 'vault_reposit';

	/**
	 *
	 * @var string $defaultSuccessForward
	 */
	public $defaultSuccessForward = 'vault/reposit/index';

	/**
	 *
	 * @var string $defaultFailedForward
	 */
	public $defaultFailedForward = 'vault/reposit/index';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('reposit');
		$this->layout()->tabs = $tabs;

		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 * @return \Application\View\ViewModel|string
	 */
	public function indexAction()
	{
		$view = $this->view;

		/* */
		$view->setTemplate('vault/reposit/index');
		$this->layoutSelector()->clear($this);
		$this->basecamp()->save('vault/reposit/index', 'vault/reposit/index', $view);
		$factory = DaoFactory::get();

		/* Filter and paginations */
		$filter = new Filter('', false);
		$filterForm = new \Vault\Form\Reposit\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		/* Get list*/
		$dao = $factory->getDao(Vault\Reposit::$classId);
		$list = $factory->getList(Vault\Reposit::$classId);
		$list->dao = $dao;
		$select = array();
		$filter->setAlias('vault');
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = 'vault.' . $asSys . ' as ' . $asApp;
		}
		$filter->select($select);

		/* */
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->load([
			'default' => [
				'orderby' => 'name',
				'order' => 'asc',
				'limit' => 50
			]
		]);
		$paginator->setMaxLimit($list->countAll($filter))
			->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		/* */
		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;
		$view->pageTitle = tra('Vault Reposits');

		/* */
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			$inSpacename = strtolower($request->getPost('spacename', ''));
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		/**/
		$reposit = Vault\Reposit::init()->setName('My vault')
			->setNumber(uniqid())
			->setCreated(new DateTime())
			->setCreateBy(People\CurrentUser::get());
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Vault\Reposit::$classId);

		/**/
		$form = new \Vault\Form\Reposit\EditForm($view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($reposit);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$basePath = \Ranchbe::get()->getConfig('path.reposit.' . $inSpacename);
					$path = $basePath . '/' . $reposit->getNumber();
					Vault\Reposit::initPath($path);
					$reposit->setPath($path);
					$dao->save($reposit);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error("Creation of vault FAILED :" . $e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = tra('Create Reposit');
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		$repositId = $this->params()->fromRoute('id');

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$reposit = new Vault\Reposit();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Vault\Reposit::$classId);
		$dao->loadFromId($reposit, $repositId);

		/**/
		$form = new \Vault\Form\Reposit\EditForm($view);
		$form->setEditMode(true);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($reposit);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($reposit);
				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Edit Reposit %s'), $reposit->getName());
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$repositIds = $request->getQuery('checked', array());
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Vault\Reposit::$classId);

		foreach( $repositIds as $repositId ) {
			try {
				/* delete directory on file system */
				$reposit = new Vault\Reposit();
				$dao->loadFromId($reposit, $repositId);

				/* delete record */
				$dao->setOption('withtrans', false);
				$dao->getConnexion()->beginTransaction();
				$dao->deleteFromId($repositId);

				try {
					$path = $reposit->getVersionPath();
					if ( is_dir($path) ) {
						rmdir($path);
					}
					$path = $reposit->getIterationPath();
					if ( is_dir($path) ) {
						rmdir($path);
					}
					$path = $reposit->getTrashPath();
					if ( is_dir($path) ) {
						rmdir($path);
					}
					$path = $reposit->getPath();
					if ( is_dir($path) ) {
						rmdir($path);
					}
				}
				catch( \Exception $e ) {
					throw new \Exception(sprintf('Failed to delete directory %s with error %s', $path, $e->getMessage()));
				}

				$dao->getConnexion()->commit();
			}
			catch( \PDOException $e ) {
				$dao->getConnexion()->rollback();
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $repositId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$dao->getConnexion()->rollback();
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}
}
