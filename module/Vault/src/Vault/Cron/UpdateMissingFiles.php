<?php
// %LICENCE_HEADER%
namespace Vault\Cron;

use Rbs\Batch\CallbackInterface;
use Rbs\Vault\MissingFileDao;

/**
 * 
 * @author ocyssau
 *
 */
class UpdateMissingFiles implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 *
	 */
	public function run()
	{
		echo date(DATE_RFC822) . "\n";
		echo 'Update misssings files index' . "\n";

		$missingFile = new MissingFileDao();
		$c = $missingFile->updateMissingFiles();

		echo 'MISSING FILES COUNT:' . $c . "\n";
		echo "End at " . date(DATE_RFC822) . "\n";
		return $this;
	}
}
