<?php
// %LICENCE_HEADER%
namespace Vault\Cron;

use Rbs\Batch\CallbackInterface;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Vault\Replicated;
use Rbs\Vault\Replication\MethodFactory;

/**
 * 
 * @author ocyssau
 *
 */
class Replicate implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 *
	 */
	public function run()
	{
		echo date(DATE_RFC822) . "\n";
		echo 'Synchronize datas of replicated reposits' . "\n";

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Replicated::$classId);
		$stmt = $dao->getWithRepositAndSite([
			'reposit.default_file_path as path',
			'site.method',
			'site.parameters',
			'site.auth_user as authUser',
			'site.auth_password as authPassword'
		]);

		$logs = [];
		foreach( $stmt->fetchAll(\PDO::FETCH_ASSOC) as $entry ) {
			$parameters = json_decode($entry['parameters'], true);
			$parameters['source'] = $entry['path'];
			$authUser = $entry['authUser'];
			$authPassword = \Rbs\Sys\Crypt::decrypt($entry['authPassword']);
			$method = $entry['method'];

			$method = MethodFactory::get()->fromMethod($method);
			$method->setParams($parameters)
				->setCredential($authUser, $authPassword)
				->run();
			$logs[] = $method->getLog();
		}

		echo 'SQL QUERY: ' . $stmt->queryString . "\n";
		echo 'LOG:' . "\n";
		foreach( $logs as $log ) {
			echo '+++++++++++++++++++++++++++++++++' . "\n";
			echo 'Command: ' . $log['cmd'] . "\n";
			echo 'Return Value: ' . $log['return'] . "\n";
			echo 'Output: ' . "\n";
			foreach( $log['outputs'] as $output ) {
				echo '    ' . $output . "\n";
			}
			echo '---------------------------------' . "\n";
		}
		echo "End at " . date(DATE_RFC822) . "\n";
		return $this;
	}
}
