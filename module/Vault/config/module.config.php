<?php
/**
 *
 */
return array(
	'router' => array(
		'routes' => array(
			'vault' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/vault/reposit[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Vault\Controller\Reposit',
						'action' => 'index',
					),
				),
			),
			'vault-distant-site' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/vault/distantsite[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Vault\Controller\Distantsite',
						'action' => 'index',
					),
				),
			),
			'vault-replicated' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/vault/replicated[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Vault\Controller\Replicated',
						'action' => 'index',
					),
				),
			),
			'vault-checker' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/vault/reposit/check',
					'defaults' => array(
						'controller' => 'Vault\Controller\Checker',
						'action' => 'check',
					),
				),
			),
			'vault-trash' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/vault/reposit/trash/:id/:action',
					'defaults' => array(
						'controller' => 'Vault\Controller\Trash',
					),
				),
			),
			'admin-tools-getmissingfiles' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/tools/getmissingfiles',
					'defaults' => array(
						'controller' => 'Vault\Controller\Tool',
						'action' => 'getmissingfiles',
					),
				),
			),
			'admin-tools-updatemissingfiles' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/tools/updatemissingfiles',
					'defaults' => array(
						'controller' => 'Vault\Controller\Tool',
						'action' => 'updatemissingfiles',
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Vault\Controller\Reposit'=>'Vault\Controller\RepositController',
			'Vault\Controller\Distantsite'=>'Vault\Controller\DistantsiteController',
			'Vault\Controller\Replicated'=>'Vault\Controller\ReplicatedController',
			'Vault\Controller\Checker'=>'Vault\Controller\CheckerController',
			'Vault\Controller\Tool'=>'Vault\Controller\ToolController',
			'Vault\Controller\Trash'=>'Vault\Controller\TrashController'
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Vault'=>__DIR__ . '/../view',
		),
	),
);
