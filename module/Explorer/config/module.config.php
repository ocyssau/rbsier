<?php
/**
 *
 */
return array(
	'router' => array(
		'routes' => array(
			'explorer' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/explorer',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Index',
						'action' => 'index',
					),
				),
			),
			'explorer-level0' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/explorer/level0',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Index',
						'action' => 'index',
					),
				),
			),
			'explorer-level1' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/explorer/level1',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Index',
						'action' => 'index',
					),
				),
			),
			'explorer-level2' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/explorer/level2',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Index',
						'action' => 'index',
					),
				),
			),
			'explorer-level3' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/explorer/level3',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Index',
						'action' => 'index',
					),
				),
			),
			'explorer-level4' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/explorer/level4',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Index',
						'action' => 'index',
					),
				),
			),
			
			'treeexplorer' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/treeexplorer',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Tree',
						'action' => 'index',
					),
				),
			),
			'explorer-load' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/explorer/load',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Load',
						'action' => 'load',
					),
				),
			),
			'explorer-load-root' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/explorer/load/root',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Load',
						'action' => 'root',
					),
				),
			),
			'explorer-load-space' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/explorer/load/spaces',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Load',
						'action' => 'spaces',
					),
				),
			),
			'explorer-load-project' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/explorer/load/projects',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Load',
						'action' => 'projects',
					),
				),
			),
			'explorer-load-project-space' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/explorer/load/project/[:projectId]/spaces',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Load',
						'action' => 'projectspaces',
					),
				),
			),
			'explorer-load-project-container' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/explorer/load/project/[:projectId]/[:spacename]',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Load',
						'action' => 'projectcontainers',
					),
				),
			),
			'explorer-load-container-category' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/explorer/load/[:spacename]/container/[:containerId]/categories',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Load',
						'action' => 'containercategories',
					),
				),
			),
			'explorer-load-container-category-doctype' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/explorer/load/[:spacename]/container/[:containerId]/[:categoryId]',
					'defaults' => array(
						'controller' => 'Explorer\Controller\Load',
						'action' => 'containercategorydoctypes',
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Explorer\Controller\Index' => 'Explorer\Controller\IndexController',
			'Explorer\Controller\Tree' => 'Explorer\Controller\TreeController',
			'Explorer\Controller\Load' => 'Explorer\Controller\LoadController',
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Explorer'=>__DIR__ . '/../view',
		),
	),
);
