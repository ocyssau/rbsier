<?php
namespace Explorer\Controller;

use Application\Controller\AbstractController;

/**
 */
class TreeController extends AbstractController
{

	/** @var integer */
	public $pageId = 'explorer_index';

	/** @var string */
	public $ifSuccessForward = 'explorer';

	/** @var string */
	public $ifFailedForward = 'explorer';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get();
		$this->layout()->tabs = $tabs;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$view = $this->view;
		return $view;
	}
}

