<?php
namespace Explorer\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged;
use Rbplm\Org;
use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\Category;

/**
 */
class LoadController extends AbstractController
{

	/** @var integer */
	public $pageId = 'explorer_loader';

	/** @var string */
	public $ifSuccessForward = 'explorer';

	/** @var string */
	public $ifFailedForward = 'explorer';

	/**
	 *
	 * @param string $dn
	 * @return mixed[]
	 */
	protected static function parseDn($dn)
	{
		$return = [];
		foreach( explode(',', $dn) as $s ) {
			$a = explode('=', $s);
			$return[$a[0]] = $a[1];
		}
		return $return;
	}

	/**
	 * 
	 * @return \Application\View\ViewModel
	 */
	public function loadAction()
	{
		$type = $this->getRequest()->getQuery('type', null);

		switch ($type) {
			case 'projects':
				return $this->getprojectsAction();
			case 'project':
				return $this->getspacesfromprojectAction();
			case 'space':
				return $this->getcontainerfromspaceAction();
				break;
			case 'container':
				return $this->getcategoryfromcontainerAction();
				break;
			case 'category':
				return $this->getdocumentfromcategoryAction();
				break;
			case 'document':
				return $this->getdocfilefromdocumentAction();
				break;
			default:
				return $this->rootAction();
				break;
		}
	}

	/**
	 */
	public function rootAction()
	{
		$view = $this->view;
		$view->setTemplate('explorer/load/generic');

		$list = array(
			array(
				'id' => 'projects',
				'uid' => 'projects',
				'name' => 'Projects',
				'parent' => '#',
				'type' => 'projects',
				'children' => true,
				'dn' => 'projects=1'
			)
		);

		$view->list = $list;
		return $view;
	}

	/**
	 */
	public function getprojectsAction()
	{
		$view = $this->view;
		$view->setTemplate('explorer/load/generic');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$jsNodeParentId = $request->getQuery('jsnodeId', null);
			$baseDn = $request->getQuery('dn', null);
		}
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Org\Project::$classId);
		$list = $factory->getList(Org\Project::$classId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->page(1, 1000);
		$filter->select(array(
			$dao->toSys('id') . ' as id',
			$dao->toSys('name') . ' as name',
			$dao->toSys('uid') . ' as uid',
			$dao->toSys('cid') . ' as cid',
			"'project' as type",
			"'$jsNodeParentId' as parent",
			"1 as children",
			"concat('$baseDn,project=', id) as dn"
		));
		$list->load($filter);
		$view->list = $list;
		return $view;
	}

	/**
	 */
	public function getspacesfromprojectAction()
	{
		$view = $this->view;
		$view->setTemplate('explorer/load/generic');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$jsNodeParentId = $request->getQuery('jsnodeId', null);
			$baseDn = $request->getQuery('dn', null);
		}
		$list = array(
			array(
				'id' => 'workitem',
				'uid' => $jsNodeParentId . 'wi',
				'name' => 'workitem',
				'parent' => $jsNodeParentId,
				'type' => 'space',
				'children' => true,
				'dn' => $baseDn . ',spacename=workitem'
			),
			array(
				'id' => 'cadlib',
				'uid' => $jsNodeParentId . 'cl',
				'name' => 'cadlib',
				'parent' => $jsNodeParentId,
				'type' => 'space',
				'children' => true,
				'dn' => $baseDn . ',spacename=cadlib'
			),
			array(
				'id' => 'bookshop',
				'uid' => $jsNodeParentId . 'bs',
				'name' => 'bookshop',
				'parent' => $jsNodeParentId,
				'type' => 'space',
				'children' => true,
				'dn' => $baseDn . ',spacename=bookshop'
			),
			array(
				'id' => 'mockup',
				'uid' => $jsNodeParentId . 'mu',
				'name' => 'mockup',
				'parent' => $jsNodeParentId,
				'type' => 'space',
				'children' => true,
				'dn' => $baseDn . ',spacename=mockup'
			)
		);
		$view->list = $list;
		return $view;
	}

	/**
	 */
	public function getcontainerfromspaceAction()
	{
		$view = $this->view;
		$view->setTemplate('explorer/load/generic');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('name', null);
			$jsNodeParentId = $request->getQuery('jsnodeId', null);
			$page = $request->getQuery('page', 1);
			$order = $request->getQuery('order', 'asc');
			$orderBy = $request->getQuery('orderby', 'name');
			$baseDn = $request->getQuery('dn', null);
			$baseDnArray = self::parseDn($baseDn);
			isset($baseDnArray['project']) ? $parentId = $baseDnArray['project'] : null;
			isset($baseDnArray['spacename']) ? $spacename = $baseDnArray['spacename'] : null;
		}

		/* get containers of space */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Org\Workitem::$classId);
		$list = $factory->getList(Org\Workitem::$classId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->page($page, 1000);
		$filter->sort($orderBy, $order);
		$filter->select(array(
			$dao->toSys('id') . ' as id',
			$dao->toSys('name') . ' as name',
			$dao->toSys('uid') . ' as uid',
			$dao->toSys('cid') . ' as cid',
			"'container' as type",
			"'$jsNodeParentId' as parent",
			"1 as children",
			"'$spacename' as spacename",
			"concat('$baseDn,container=', id) as dn"
		));

		/**/
		if ( $parentId ) {
			$filter->andfind($parentId, $dao->toSys('parentId'), Op::EQUAL);
		}
		$list->load($filter);

		$view->list = $list;
		return $view;
	}

	/**
	 */
	public function getprojectlinksfromspaceAction()
	{
		$view = $this->view;
		$view->setTemplate('explorer/load/generic');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('name', null);
			$baseDn = $request->getQuery('dn', null);
			$baseDnArray = self::parseDn($baseDn);
			isset($baseDnArray['project']) ? $parentId = $baseDnArray['project'] : null;
			isset($baseDnArray['spacename']) ? $spacename = $baseDnArray['spacename'] : null;
		}

		/* get links of project */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Org\Workitem::$classId);
		$lnkDao = $factory->getDao(\Rbs\Org\Project\Link\Container::$classId);
		$list = $lnkDao->getChildrenFromId($parentId, $dao->getSelectAsApp('child'), "child.spacename='$spacename'");

		$view->list = $list;
		return $view;
	}

	/**
	 */
	public function getcategoryfromcontainerAction()
	{
		$view = $this->view;
		$view->setTemplate('explorer/load/generic');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$jsNodeParentId = $request->getQuery('jsnodeId', null);
			$spacename = $request->getQuery('spacename', null);
			$order = $request->getQuery('order', 'asc');
			$orderBy = $request->getQuery('orderby', 'name');
			$baseDn = $request->getQuery('dn', null);
			$baseDnArray = self::parseDn($baseDn);
			isset($baseDnArray['container']) ? $parentId = $baseDnArray['container'] : null;
			isset($baseDnArray['spacename']) ? $spacename = $baseDnArray['spacename'] : null;
		}

		/* init some helpers */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Category::$classId);

		$select = array(
			'cat.' . $dao->toSys('id') . ' AS id',
			'cat.' . $dao->toSys('name') . ' AS name',
			"concat('$jsNodeParentId',cat." . $dao->toSys('uid') . ") as uid",
			'cat.' . $dao->toSys('cid') . ' AS cid',
			"'category' AS type",
			"'$jsNodeParentId' AS parent",
			"1 AS children",
			"'$spacename' AS spacename",
			"concat('$baseDn,category=', cat.id) AS dn"
		);
		$filter = "1=1 ORDER BY cat.$orderBy $order LIMIT 1000 OFFSET 0";
		$stmt = $dao->getUsedByContainer($parentId, $spacename, $select, $filter);

		$all = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$all[] = [
			'id' => 5,
			'name' => 'ALL',
			'uid' => $jsNodeParentId . '569e9ALL',
			'cid' => '569e918a134ca',
			'type' => 'category',
			'parent' => $jsNodeParentId,
			'children' => 1,
			'spacename' => $spacename,
			'dn' => $baseDn . ',category=5'
		];
		$all[] = [
			'id' => 6,
			'name' => 'UNCATEGORIZED',
			'uid' => $jsNodeParentId . '569e9UNCATEGORIZED',
			'cid' => '569e918a134ca',
			'type' => 'category',
			'parent' => $jsNodeParentId,
			'children' => 1,
			'spacename' => $spacename,
			'dn' => $baseDn . ',category=6'
		];
		$view->list = $all;
		return $view;
	}

	/**
	 */
	public function getdocumentfromcategoryAction()
	{
		$view = $this->view;
		$view->setTemplate('explorer/load/generic');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$jsNodeParentId = $request->getQuery('jsnodeId', null);
			$spacename = $request->getQuery('spacename', null);
			$parentId = $request->getQuery('id', null);
			$page = $request->getQuery('page', 0);
			$order = $request->getQuery('order', 'asc');
			$orderBy = $request->getQuery('orderby', 'name');
			$baseDn = $request->getQuery('dn', null);
			$baseDnArray = self::parseDn($baseDn);
			isset($baseDnArray['container']) ? $parentId = $baseDnArray['container'] : null;
			isset($baseDnArray['category']) ? $categoryId = $baseDnArray['category'] : null;
			isset($baseDnArray['spacename']) ? $spacename = $baseDnArray['spacename'] : null;
		}

		/* get containers of space */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Ged\Document\Version::$classId);
		$list = $factory->getList(Ged\Document\Version::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->page($page, 1000);
		$filter->sort($orderBy, $order);
		$filter->select(array(
			$dao->toSys('id') . ' as id',
			$dao->toSys('name') . ' as name',
			"concat('$jsNodeParentId'," . $dao->toSys('uid') . ") as uid",
			$dao->toSys('cid') . ' as cid',
			"'document' as type",
			"'$jsNodeParentId' as parent",
			"1 as children",
			"'$spacename' as spacename",
			"concat('$baseDn,document=', id) as dn"
		));

		/**/
		if ( $parentId ) {
			$filter->andfind($parentId, $dao->toSys('parentId'), Op::EQUAL);
		}
		if ( $categoryId ) {
			if ( $categoryId == 5 ) {
				/* get all */
			}
			elseif ( $categoryId == 6 ) {
				/* get uncategorized */
				$filter->andfind(null, $dao->toSys('categoryId'), Op::ISNULL);
			}
			else {
				$filter->andfind($categoryId, $dao->toSys('categoryId'), Op::EQUAL);
			}
		}

		$list->load($filter);
		$view->list = $list;
		return $view;
	}

	/**
	 */
	public function getdocfilefromdocumentAction()
	{
		$view = $this->view;
		$view->setTemplate('explorer/load/generic');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$jsNodeParentId = $request->getQuery('jsnodeId', null);
			$page = $request->getQuery('page', 1);
			$order = $request->getQuery('order', 'asc');
			$orderBy = $request->getQuery('orderby', 'name');
			$baseDn = $request->getQuery('dn', null);
			$baseDnArray = self::parseDn($baseDn);
			isset($baseDnArray['spacename']) ? $spacename = $baseDnArray['spacename'] : null;
			isset($baseDnArray['document']) ? $parentId = $baseDnArray['document'] : null;
		}

		/* get containers of space */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Ged\Docfile\Version::$classId);
		$list = $factory->getList(Ged\Docfile\Version::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->page($page, 1000);
		$filter->sort($orderBy, $order);
		$filter->select(array(
			$dao->toSys('id') . ' as id',
			$dao->toSys('name') . ' as name',
			$dao->toSys('uid') . ' as uid',
			$dao->toSys('cid') . ' as cid',
			"'docfile' as type",
			"'$jsNodeParentId' as parent",
			"0 as children",
			"'$spacename' as spacename",
			"concat('$baseDn,docfile=', id) as dn"
		));

		/**/
		if ( $parentId ) {
			$filter->andfind($parentId, $dao->toSys('parentId'), Op::EQUAL);
		}

		$list->load($filter);
		$view->list = $list;
		return $view;
	}
} /* End of class */
