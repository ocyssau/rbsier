<?php
namespace Explorer\Controller;

use Application\Controller\AbstractController;
use Rbplm\Org\Project;
use Rbs\Space\Factory as DaoFactory;

/**
 */
class IndexController extends AbstractController
{

	/** @var integer */
	public $pageId = 'explorer_index';

	/** @var string */
	public $ifSuccessForward = 'explorer';

	/** @var string */
	public $ifFailedForward = 'explorer';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get();
		$this->layout()->tabs = $tabs;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$view = $this->view;

		/* set some helpers */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Project::$classId);
		$list = $factory->getList(Project::$classId);
		$select = $dao->getSelectAsApp();
		$filter->select($select);
		$list->load($filter);
		$view->list = $list->toArray();
		$view->pageTitle = tra('Projects');
		return $view;
	}
}
