<?php
//%LICENCE_HEADER%
namespace Sier\Cron;

use Rbs\Batch\CallbackInterface;

/**
 * CallbackInterface interface
 * 
 * Must be implemented by callbock for execution of cronTasks
 * 
 */
class InitMockupMod implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 * 
	 */
	public function run()
	{
		$config = \Ranchbe::get()->getConfig();
		$mockupReposit = $config['path.reposit.mockup'];

		echo date(DATE_RFC822) . "\n";

		/* Utilise les fonctions systeme */
		$command = "find $mockupReposit/. -exec chown caoadmin:caomeca {} \;";
		$return = null;
		system($command, $return);

		if ( $return != 0 ) {
			throw new \RuntimeException('chown comand failed');
		}

		$command = "find $mockupReposit/. -exec chmod 755 {} \;";
		system($command, $return);

		if ( $return != 0 ) {
			throw new \RuntimeException('chmod comand failed');
		}

		echo "End at " . date(DATE_RFC822) . "\n";
		return $this;
	}
}
