<?php
//%LICENCE_HEADER%
namespace Sier\Cron;

use Rbs\Batch\CallbackInterface;

/**
 * CallbackInterface interface
 * 
 * Must be implemented by callbock for execution of cronTasks
 * 
 */
class ArchiveCreate implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 * 
	 */
	public function run()
	{
		return $this;
	}
}
