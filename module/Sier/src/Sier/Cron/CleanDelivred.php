<?php
//%LICENCE_HEADER%
namespace Sier\Cron;

use Rbs\Batch\CallbackInterface;

/**
 * CallbackInterface interface
 * 
 * Must be implemented by callbock for execution of cronTasks
 * 
 */
class CleanDelivred implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 * 
	 */
	public function run()
	{
		$config = \Ranchbe::get()->getConfig();
		$delivredReposit = $config['sier']['delivred.path'];

		if ( $delivredReposit == '' ) {
			die('sier delivred.path is not defined');
		}

		echo date(DATE_RFC822) . "\n";

		/* Utilise les fonctions systeme */
		$command = "find $delivredReposit -type f -mtime +30 -exec rm -Rf {} \;";
		$return = null;
		system($command, $return);

		if ( $return != 0 ) {
			throw new \RuntimeException('rm comand failed');
		}

		echo "End at " . date(DATE_RFC822) . "\n";
		return $this;
	}
}
