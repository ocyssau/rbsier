<?php
//%LICENCE_HEADER%
namespace Sier\Cron;

use Rbs\Batch\CallbackInterface;

/**
 * CallbackInterface interface
 * 
 * Must be implemented by callbock for execution of cronTasks
 * 
 */
class InitSharedCgr implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 * 
	 */
	public function run()
	{
		echo date(DATE_RFC822) . "\n";

		/* Utilise les fonctions systeme */
		$command = "chmod -R 777 /MAQUETTE/CGRV5R18";
		$return = null;
		system($command, $return);

		if ( $return != 0 ) {
			throw new \RuntimeException('chmod comand failed');
		}

		echo "End at " . date(DATE_RFC822) . "\n";
		return $this;
	}
}
