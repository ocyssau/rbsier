<?php
namespace Sier\Test\Ssh;

use Sier\Controller\Filetree\PermissionFailed;

/**
 *
/**
 * How to use :
 * open terminal.
 * 
 * 		cd [pat/to/ranchbe/root/directory]
 * 
 * To run all tests methods of a class
 * 
 * 		php public/index.php test /Sier/Test/Ssh/Connexion all
 * 
 * To run one methods
 * 
 * 		php public/index.php test /Sier/Test/Ssh/Connexion run
 * 
 * To display profiling informations
 * 
 * 		php public/index.php test /Module/Controller/Class run -p --loop 10
 * 
 * where 10 is number of loops run on each methods
 * 
 * @see \Application\Controller\CliTestController
 * 
 * For memory :
 * 
 * on client:
 * ssh-keygen
 * ssh-add
 * scp ~/.ssh/id_rsa.pub remoteuser@domain@fully.qualified.hostname:c:/users/remoteuser/.ssh/host.pub
 * 
 * on server
 * inclure le contenu du host.pub au fichier authorized_keys
 * 
 */
class ConnexionTest extends \Application\Model\AbstractTest
{

	/**
	 * 
	 */
	public function runTest()
	{
		/* @var \Sier\Ssh\Factory $factory */
		$factory = $this->controller->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Ssh');

		$fileserver = 'test';
		
		var_dump($factory->getConfig());

		try {
			$connexion = $factory->get($fileserver);
		}
		catch( \Throwable $e ) {
			if ( $e->getCode() == 7100 ) {
				throw new PermissionFailed("Config key rbp.sier.ssh must have a key = name of fileserver '$fileserver'", 7100);
			}
			else {
				throw $e;
			}
		}

		$ret = [];
		try {
			$cmd = sprintf('dir');
			$ret[] = $connexion->exec($cmd);
		}
		catch( \Throwable $e ) {
			throw new PermissionFailed(sprintf('Permission settings failed for reason %s', $e->getMessage()), 7200);
		}

		var_dump($ret);
	}
}
