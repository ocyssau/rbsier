<?php
namespace Sier\Callback;

use Rbs\Ged\Document\Event;
use Rbplm\Ged\Docfile\Role;

/**
 * 
 *
 */
class Catpart extends CadAbstract
{

	/**
	 *
	 * @param Event $event
	 * @return Catpart
	 */
	public function postStore(Event $event)
	{
		$this->event = $event;
		$document = $event->getEmitter();
		$document->factory = $event->getFactory();
		$this->associate($document, '.pdf', 'document', Role::VISU);
		$this->associate($document, '.qcseal', 'document', Role::ANNEX);
		$this->associate($document, '.stp', 'document', Role::CONVERT);
		return $this;
	}

	/**
	 *
	 * @param Event $event
	 * @return Catpart
	 */
	public function postUpdate(Event $event)
	{
		$this->event = $event;
		$document = $event->getEmitter();
		$document->factory = $event->getFactory();
		$this->associate($document, '.pdf', 'document', Role::VISU);
		$this->associate($document, '.qcseal', 'document', Role::ANNEX);
		$this->associate($document, '.stp', 'document', Role::CONVERT);
		return $this;
	}
}
