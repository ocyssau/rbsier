<?php
namespace Sier\Callback;

use Rbs\Ged\Document\Event;

/**
 * 
 *
 */
class Catproduct extends CadAbstract
{

	/**
	 *
	 * @param Event $event
	 * @return Catproduct
	 */
	public function postStore(Event $event)
	{
		$this->event = $event;
		$document = $event->getEmitter();
		$document->factory = $event->getFactory();
		$this->associate($document, '.qcseal', 'document');
		$this->associate($document, '.nom.xlsx', 'document');
		return $this;
	}

	/**
	 *
	 * @param Event $event
	 * @return Catproduct
	 */
	public function postUpdate(Event $event)
	{
		$this->event = $event;
		$document = $event->getEmitter();
		$document->factory = $event->getFactory();
		$this->associate($document, '.qcseal', 'document');
		$this->associate($document, '.nom.xlsx', 'document');
		return $this;
	}
}
