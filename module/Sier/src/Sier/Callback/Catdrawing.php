<?php
namespace Sier\Callback;

use Rbs\Ged\Document\Event;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile\Role;

/**
 * 
 *
 */
class Catdrawing extends CadAbstract
{

	/**
	 *
	 * @param Event $event        	
	 * @param Document\Version $reference        	
	 * @return Catdrawing
	 */
	public function postStore(Event $event)
	{
		$this->event = $event;
		$document = $event->getEmitter();
		$document->factory = $event->getFactory();
		$this->associate($document, '.pdf', 'document', Role::VISU);
		$this->associate($document, '.qcseal', 'document', Role::ANNEX);
		$this->associate($document, '.dxf', 'document', Role::ANNEX);
		return $this;
	}

	/**
	 *
	 * @param Event $event        	
	 * @param Document\Version $reference        	
	 * @return Catdrawing
	 */
	public function postUpdate(Event $event)
	{
		$this->event = $event;
		$document = $event->getEmitter();
		$document->factory = $event->getFactory();
		$this->associate($document, '.pdf', 'document', Role::VISU);
		$this->associate($document, '.qcseal', 'document', Role::ANNEX);
		$this->associate($document, '.dxf', 'document', Role::ANNEX);
		return $this;
	}

	/**
	 * 
	 * @param Event $event        	
	 * @param Document\Version $reference        	
	 * @return Catdrawing
	 */
	public function preStore(Event $event)
	{
		$this->event = $event;
		$document = $event->getEmitter();
		$document->factory = $event->getFactory();
		$filename = $document->getNumber() . '.pdf';
		$this->checkDate($document, $filename, 30);
		return $this;
	}

	/**
	 *
	 * @param Event $event        	
	 * @param Document\Version $reference
	 * @return Catdrawing
	 */
	public function preUpdate(Event $event)
	{
		$this->event = $event;
		$document = $event->getEmitter();
		$document->factory = $event->getFactory();
		$filename = $document->getNumber() . '.pdf';
		$this->checkDate($document, $filename, 30);
		return $this;
	}
}
