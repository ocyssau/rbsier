<?php
namespace Sier\Callback;

use Rbplm\People;
use Rbplm\Ged\Document;
use Rbs\Observers\InformationException;
use Rbs\Observers\CallbackException;
use Rbs\Observers\Callback;

/**
 * Abstract class for Sier callback to implements common methods.
 * 
 * @author ocyssau
 *
 */
abstract class CadAbstract extends Callback
{

	/**
	 * Associe le fichier avec extension $extension s'il existe dans le wildspace et s'il n'est pas deja associe.
	 *
	 * @param Document\Version $document        	
	 * @param string $extension
	 *        	extension du fichier a associer sans le point (ex: tiff)
	 * @param string $mode
	 *        	document: build assoc file name from document number. ex: file.ext->assoc to file.extension
	 *        	file: build assoc file name from file name. ex: file.ext->assoc to file.ext.extension
	 * @return CadAbstract
	 */
	protected function associate($document, $extension, $mode = 'file', $role = 8)
	{
		/* Get the main file */
		$docfiles = $document->getDocfiles();
		$mainDocfile = reset($docfiles);

		/* if none docfile, get out */
		if ( !is_object($mainDocfile) ) {
			throw new InformationException('None docfiles founded');
		}

		$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
		$wildspacePath = $wildspace->getPath();

		/* Chemin du fichier dans le wildspace */
		if ( $mode == 'file' ) {
			$filename = $mainDocfile->getName() . $extension;
		}
		else {
			$filename = $document->getNumber() . $extension;
		}
		$file = $wildspacePath . '/' . $filename;

		if ( is_file($file) ) {
			/* Verifie que le fichier n'est pas deja attache */
			foreach( $docfiles as $docfile ) {
				if ( $docfile->getName() == $filename ) {
					throw new InformationException(sprintf('%s is yet associated', $filename));
				}
			}

			/* Associe le fichier au document */
			if ( !isset($document->service) ) {
				$document->service = new \Rbs\Ged\Document\Service($document->factory);
			}

			$document->service->associateFile($document, $file, $filename, $role);
		}

		return $this;
	}

	/**
	 * Verifie la date du pdf
	 *
	 * @param Document\Version $document        	
	 * @param string $filename        	
	 * @param integer $maxDiff max difference of time between doc and file to check
	 * @return CadAbstract
	 */
	protected function checkDate($document, $filename, $maxDiff = 30)
	{
		/* Get the main file */
		/** @var \Rbplm\Ged\Docfile\Version $mainDocfile */
		$docfiles = $document->getDocfiles();
		$mainDocfile = reset($docfiles);

		/* if none docfile, get out */
		if ( !is_object($mainDocfile) ) {
			throw new InformationException('None docfiles founded');
		}

		/** @var \Rbplm\Vault\Record $mainData */
		$mainData = $mainDocfile->getData();

		$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
		$wildspacePath = $wildspace->getPath();

		/* file to test */
		$referenceFile = $wildspacePath . '/' . $mainDocfile->getName();
		$checkedFile = $wildspacePath . '/' . $filename;

		if ( is_file($checkedFile) ) {
			$checkedMtime = filemtime($checkedFile);

			if ( is_file($referenceFile) ) {
				$referenceMtime = filemtime($referenceFile);
			}
			else {
				$referenceMtime = $mainData->mtime;
			}
			
			$timeShift = $referenceMtime - $checkedMtime;
			
			if ( $timeShift > $maxDiff ) {
				$msg = sprintf('The file %s has a mtime %s seconds older than the main file %s', $checkedFile, $maxDiff, $referenceFile);
				throw new CallbackException($msg);
			}
		}
		else {
			throw new InformationException("File $filename is not existing");
		}
		return $this;
	}
}
