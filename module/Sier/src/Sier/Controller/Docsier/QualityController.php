<?php
namespace Sier\Controller\Docsier;

use Sier\Controller\AbstractPublicController;
use Rbs\Space\Factory as DaoFactory;
use Application\Controller\ControllerException;

/**
 */
class QualityController extends AbstractPublicController
{

	/** @var string */
	public $pageId = 'docsier_quality';

	/** @var string */
	public $defaultSuccessForward = 'docsier/quality';

	/** @var string */
	public $defaultFailedForward = 'docsier/quality';

	/** @var integer */
	public $qualityContainerId;

	public $qualitySpacename;

	public $cartographieDocumentId;

	/**
	 */
	public function init()
	{
		parent::init();

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
		$this->layout()->setTemplate('sier/layout/layout');

		$config = \Ranchbe::get()->getConfig();
		$this->qualityContainerId = $config['sier']['qualitycontainerid'];
		$this->qualitySpacename = $config['sier']['qualityspacename'];
		$this->cartographieDocumentId = $config['sier']['cartographiedocumentid'];
	}

	/**
	 * Cartographie DQ Affaire
	 * 
	 * @throws \Application\Controller\ControllerException
	 * @return \Application\View\ViewModel
	 */
	public function mapAction()
	{
		$view = $this->view;
		$view->setTemplate('sier/docsier/quality/cartographie');
		$documentId = $this->params()->fromRoute('id', null);
		$spacename = $this->params()->fromRoute('spacename', 'bookshop');
		$factory = DaoFactory::get($spacename);

		try {
			/* get document */
			$document = new \Rbplm\Ged\Document\Version();
			$dao = $factory->getDao($document->cid);
			$dao->loadFromId($document, $documentId);
			$dfDao = $factory->getDao(\Rbplm\Ged\Docfile\Version::$classId);
			$dfDao->loadDocfiles($document);
			$docfiles = $document->getDocfiles();
			$mapFile = null;
			$gifFile = null;

			/**/
			foreach( $docfiles as $docfile ) {
				if ( $docfile->getData()->extension == '.gif' ) {
					$gifFile = $docfile;
				}
				else if ( $docfile->getName() == $document->getName() . '-map.txt' ) {
					$mapFile = $docfile;
				}
				else if ( $docfile->getData()->extension == '.map' ) {
					$mapFile = $docfile;
				}
			}
		}
		catch( \Rbplm\Dao\NotExistingException $e ) {
			throw new ControllerException(sprintf('The document is not existing. Please check and adjust the cartographie document id config key sier-cartographieDocumentId'));
		}
		catch( \Exception $e ) {
			throw new ControllerException($e->getMessage());
		}

		if ( !$mapFile ) {
			throw new ControllerException(sprintf('None map file found. You must associate a file with "-map.txt" suffix or ".map" extension to ranchbe document for generate cartographie'));
		}
		if ( !$gifFile ) {
			throw new ControllerException(sprintf('None gif file found. You must associate a file with ".gif" extension to ranchbe document for generate cartographie'));
		}

		$view->gifFile = $gifFile->getData()->fullpath;
		$view->mapFile = $mapFile->getData()->fullpath;
		$view->document = $document;
		return $view;
	}
}
