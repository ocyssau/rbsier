<?php
namespace Sier\Controller\Docsier;

use Sier\Controller\AbstractPublicController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Dao\Filter\Op;

/**
 */
class IndexController extends AbstractPublicController
{

	/** @var string */
	public $pageId = 'docsier_index';

	/** @var string */
	public $defaultSuccessForward = 'docsier/index';

	/** @var string */
	public $defaultFailedForward = 'docsier/index';

	/**
	 */
	public function init()
	{
		parent::init();

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
		$this->containerIds = array(
			840
		);
		$this->layout()->setTemplate('sier/layout/layout');
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('sier/docsier/index/index');
		$spacename = 'bookshop';
		$factory = DaoFactory::get($spacename);
		$request = $this->getRequest();
		$containerId = $this->containerIds[0];

		$validate = $request->getQuery('filtered');
		$reset = $request->getQuery('resetf');

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Sier\Form\Docsier\Index\FilterForm($factory, $this->pageId, $containerId);
		$filterForm->setAttribute('id', 'docsierfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		if ( $filterForm->isValid() && $validate && !$reset ) {
			$dao = $factory->getDao(Document\Version::$classId);
			$select = array();
			$select[] = 'docsier_date';
			$select[] = 'docsier_famille';
			$select[] = 'docsier_keyword';
			$select[] = $dao->toSys('description') . ' as description';
			$select[] = $dao->toSys('number') . ' as number';
			$select[] = $dao->toSys('id') . ' as id';

			$filter->select($select);
			foreach( $this->containerIds as $containerId ) {
				$filter->andfind($containerId, $dao->toSys('parentId'), Op::EQUAL);
			}

			$list = $factory->getList(Document\Version::$classId);
			$list->countAll = $list->countAll($filter);
			$list->load($filter, $filterForm->bind);
			$view->list = $list->toArray();
			$view->countOutput = $list->countAll;
		}
		else {
			$view->list = array();
			$view->countOutput = 0;
		}

		$view->searchFeedback = "";
		$view->filter = $filterForm;
		return $view;
	}
}
