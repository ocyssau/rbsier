<?php
namespace Sier\Controller\Filetree;

use Sier\Model\Filetree as FiletreeModel;
use Rbs\Space\Factory as DaoFactory;
use Sier\Model\Filetree\DeployHelper;
use Docseeder\Model\Docseeder;
use Exception;
use Application\Controller\ControllerException;


/**
 * 
 * Deploy some standard documents from docseeder in new doa workitem.
 * If necessary, create folders tree in affaires directory
 *
 */
class DoaController extends IngenierieController
{

	/** @var string */
	public $pageId = 'sier_filetree_ingenierie';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		$config = \Ranchbe::get()->getConfig('sier');
		$this->config = [
			'basepath' => rtrim($config['filetree']['doa']['local'], '/'),
			'template' => $config['filetree']['doa']['template'],
			'fileserver' => $config['filetree']['doa']['fileserver'],
			'localFileserverPath' => rtrim($config['filetree']['doa']['localfileserver'], '/'),
			'securityRules' => $config['filetree']['doa']['security.rule'],
			'winBasePath' => $config['filetree']['doa']['clientaccess'],
			'dq115Map' => $config['filetree']['doa']['dq115Map']
		];
	}
	
	/**
	 * @param FiletreeModel $filetreeModel
	 */
	protected function createDirectoryTree(FiletreeModel $filetreeModel)
	{
		/* unactive files access limitation */
		\Rbplm\Sys\Filesystem::isSecure(false);
		
		$basePath = $this->config['basepath'];
		$path = $basePath . '/' . $filetreeModel->getName();
		
		try {
			$notYetExisting = true;
			$filetreeModel::initPath($path);
			$filetreeModel->setPath($path);
		}
		catch( \Rbplm\Vault\ExistingDirectoryException $e ) {
			$notYetExisting = false;
		}
		catch( \Exception $e ) {
			throw new ControllerException(sprintf('Unable to create reposit %s > %s', $path, $e->getMessage()), 10001);
		}
		
		/**/
		if ( !is_dir($path) ) {
			throw new ControllerException(sprintf('%s is not founded', $path), 10010);
		}
		
		/* Uncompress template archive if required */
		if ( $notYetExisting ) {
			
			$template = $this->config['template'];
			
			if ( !is_file($template) ) {
				throw new ControllerException(sprintf('%s is not founded', $template), 10000);
			}
			
			/* Uncompress template archive */
			$zip = new \ZipArchive();
			if ( $zip->open($template) === true ) {
				$zip->extractTo($path);
				$zip->close();
			}
			else {
				throw new ControllerException(sprintf('cant uncompress file %s', $template), 10020);
			}
		}
		
		/* init permissions */
		$fileserver = $this->config['fileserver'];
		if ( $fileserver ) {
			$localFileserverPath = $this->config['localFileserverPath'];
			$securityRules = $this->config['securityRules'];
			$this->setWinpermissions($localFileserverPath, $securityRules, $fileserver);
		}
		
		/* create link */
		$desc = \Rbs\Number::normalize($filetreeModel->getDescription());
		$link = $basePath . '/' . $filetreeModel->getName() . '-' . $desc . '.lnk';
		$winBasePath = $this->config['winBasePath'];
		$toWinPath = '\\\\' . $winBasePath . '\\' . $filetreeModel->getName(); // Double escape
		$this->createWindowsLink($link, $toWinPath);
		
		return $this;
	}
	

	/**
	 * @param FiletreeModel $filetreeModel
	 */
	protected function deploy(FiletreeModel $filetreeModel)
	{

		/* init helpers */
		$workitemFactory = DaoFactory::get('Workitem');
		$helper = new DeployHelper($workitemFactory);
		$helper->setConfig(\Ranchbe::get()->getConfig());
		$filetreeModel->deployHelper = $helper;
		
		/* load project, workitem, type in docseeder */
		try {
			$docseeder = new Docseeder();
			$docseeder->containerId = $filetreeModel->getContainerId();
			$helper->loadContainer($docseeder);
			$filetreeModel->setName($docseeder->getContainer()
				->getNumber());
			$filetreeModel->setDescription($docseeder->getContainer()->description);
			$filetreeModel->docseeder = $docseeder;
		}
		catch( Exception $e ) {
			$msg = sprintf('Unable to load container for this reason: > %s', $e->getMessage());
			throw new ControllerException($msg);
		}
		
		/* Create directory */
		try{
			$this->createDirectoryTree($filetreeModel);
		}
		catch( \Rbs\Sys\Win\ShortcutException $e ) {
			$this->errorStack()
			->warning(sprintf("Creation of shortcut failed: %s", $e->getMessage()))
			->log($e->getMessage());
		}
		catch( PermissionFailed $e ) {
			$this->errorStack()->error("Init of permissions FAILED :" . $e->getMessage());
		}
		catch( ControllerException $e ) {
			$this->errorStack()->log($e->getMessage());
		}
		catch( \Exception $e ) {
			$this->errorStack()->log($e->getMessage());
		}

		/* deploy files from docseeder types */
		$selected = $filetreeModel->templates;
		foreach( $this->getDq115Map() as $k => $dq115MapElemt ) {
			try {
				/* force ignore or execution of disabled elements */
				if ( $dq115MapElemt['disabled'] ) {
					if ( $dq115MapElemt['selected'] == false ) {
						continue;
					}
					else {
						self::deployFile($filetreeModel, $dq115MapElemt);
					}
				}
				else if ( is_array($selected) ) {
					if ( in_array($k, $selected) ) {
						self::deployFile($filetreeModel, $dq115MapElemt);
					}
				}
			}
			catch( \Throwable $e ) {
				$msg = sprintf("Unable to deploy file %s for reason : %s", $dq115MapElemt['label'], $e->getMessage());
				$this->errorStack()
					->error($msg)
					->log($msg);
			}
		}

		return $this;
	}
}
