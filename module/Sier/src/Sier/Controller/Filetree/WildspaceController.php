<?php
namespace Sier\Controller\Filetree;

use Application\Controller\AbstractController;
use Sier\Model\Filetree\Wildspace as FiletreeModel;

/**
 *
 * @author olivier
 *
 */
class WildspaceController extends AbstractController
{

	/** @var string */
	public $pageId = 'sier_filetree_wildspace';

	/**
	 * 
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		else {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/**/
		$reposit = FiletreeModel::init()->setName('')->setDescription('');

		/* init Form */
		$form = new \Sier\Form\Filetree\Wildspace\EditForm($this->view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($reposit);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$this->deploy($reposit);
					$this->errorStack()->success(sprintf('Directory %s is successfully created', $reposit->getPath()));
				}
				catch( \Exception $e ) {
					$this->errorStack()->error("Creation of reposit FAILED :" . $e->getMessage());
				}
			}
		}

		$view->form = $form;
		$view->pageTitle = 'Create Wildspace Folders Tree';
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	protected function deploy($reposit)
	{
		/* unactive files access limitation */
		\Rbplm\Sys\Filesystem::isSecure(false);
		$basePath = '/CREATE';
		$path = $basePath . '/' . $reposit->getName();
		$reposit->setPath($path);

		if ( !is_dir($basePath) ) {
			throw new \Exception(sprintf('%s is not founded', $basePath), 10010);
		}
		if ( is_dir($path) ) {
			throw new \Exception(sprintf('%s is existing', $path), 10011);
		}

		/* create directory /CREATE/user */
		$ret = @mkdir($path);
		if ( !$ret ) {
			throw new \Exception(sprintf('unable to create directory %s', $path), 10012);
		}

		/* create directory /CREATE/user/WildSpace */
		$path = $path . '/WildSpace';
		$ret = @mkdir($path, '0775', false);
		if ( !$ret ) {
			throw new \Exception(sprintf('unable to create directory %s', $path), 10012);
		}

		return $this;
	}

	/**
	 *
	 */
	public function archiveAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		else {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/**/
		$reposit = FiletreeModel::init()->setName('')->setDescription('');

		/* init Form */
		$form = new \Sier\Form\Filetree\Wildspace\EditForm($this->view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($reposit);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$this->deploy($reposit);
					$this->errorStack()->success(sprintf('Directory %s is successfully created', $reposit->getPath()));
				}
				catch( \Exception $e ) {
					$this->errorStack()->error("Creation of reposit FAILED :" . $e->getMessage());
				}
				//return $this->successForward();
			}
		}

		$view->form = $form;
		$view->pageTitle = 'Archive Wildspace Directory';
		$view->setTemplate($form->template);
		return $view;
	}
}
