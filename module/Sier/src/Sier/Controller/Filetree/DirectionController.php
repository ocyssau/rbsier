<?php
namespace Sier\Controller\Filetree;

use Sier\Controller\Filetree\FiletreeController as AbstractController;
use Sier\Model\Filetree\Direction as FiletreeModel;
use Rbs\Space\Factory as DaoFactory;
use Sier\Model\Filetree\DeployHelper;
use Docseeder\Model\Docseeder;
use Exception;
use Application\Controller\ControllerException;

/**
 *
 * @author olivier
 *
 */
class DirectionController extends AbstractController
{

	/** @var string */
	public $pageId = 'sier_filetree_direction';

	/** @var array */
	public $dq115Map;

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();
		$this->dq115Map = null;

		$config = $this->getEvent()->getApplication()->getServiceManager()->get('config')['sier_filetree']['direction'];
		
		$this->config = [
			'basepath' => rtrim($config['local'], '/'),
			'template' => $config['template'],
			'fileserver' => $config['fileserver'],
			'localFileserverPath' => rtrim($config['localfileserver'], '/'),
			'securityRules' => $config['security.rule'],
			'winBasePath' => $config['clientaccess'],
			'dq115Map' => $config['dq115Map']
		];
	}

	/**
	 * 
	 */
	protected function getForm($request = null)
	{
		$year = $request->getPost('year', $request->getQuery('year', date('Y')));
		$form = new \Sier\Form\Filetree\Direction\EditForm($this->view, $year);

		$form->setDq115Map($this->getDq115Map($year));
		$basePath = FiletreeModel::pathReplacements($this->config['basepath'], [
			'%year%' => $year
		]);

		$form->setBasedir($basePath);
		return $form;
	}

	/**
	 * @var \Zend\Http\PhpEnvironment\Request $request
	 */
	protected function getModel($request = null)
	{
		$request = $this->getRequest();
		$reposit = FiletreeModel::init();
		$reposit->populate($request->getQuery()
			->toArray());
		return $reposit;
	}

	/**
	 * @throws \Exception
	 * @return array
	 */
	protected function getDq115Map($year)
	{
		if ( !isset($this->dq115Map) ) {
			$mapFile = FiletreeModel::pathReplacements($this->config['dq115Map'], [
				'%year%' => $year
			]);
			if ( !is_readable($mapFile) ) {
				throw new ControllerException(sprintf('file %s is not readable', $mapFile));
			}

			try {
				$this->dq115Map = include ($mapFile);
			}
			catch( \Throwable $e ) {
				throw new ControllerException(sprintf('%s have synthaxe errors : %s on line %s', $mapFile, $e->getMessage(), $e->getLine()), $e);
			}

			if ( !is_array($this->dq115Map) ) {
				throw new ControllerException(sprintf('Import of map %s failed, creation of filetree is canceled', $mapFile));
			}
		}
		return $this->dq115Map;
	}

	/**
	 * @param FiletreeModel $reposit
	 */
	protected function deploy(\Sier\Model\Filetree $filetreeModel)
	{
		/* init helpers */
		$workitemFactory = DaoFactory::get('Workitem');
		$helper = new DeployHelper($workitemFactory);
		$helper->setConfig(\Ranchbe::get()->getConfig());
		$filetreeModel->deployHelper = $helper;

		/* load project, workitem, type in docseeder */
		try {
			$docseeder = new Docseeder();
			$docseeder->containerId = $filetreeModel->getContainerId();
			$helper->loadContainer($docseeder);
			$filetreeModel->setName($docseeder->getContainer()
				->getNumber());
			$filetreeModel->setDescription($docseeder->getContainer()->description);
			$filetreeModel->docseeder = $docseeder;
		}
		catch( Exception $e ) {
			$msg = sprintf('Unable to load container for this reason: > %s', $e->getMessage());
			throw new ControllerException($msg);
		}

		/* unactive files access limitation */
		\Rbplm\Sys\Filesystem::isSecure(false);

		/* init paths */
		$year = $filetreeModel->getYear();
		$basePath = FiletreeModel::pathReplacements($this->config['basepath'], [
			'%year%' => $year
		]);
		$template = FiletreeModel::pathReplacements($this->config['template'], [
			'%year%' => $year
		]);
		$localFileserverPath = FiletreeModel::pathReplacements($this->config['localFileserverPath'], [
			'%year%' => $year
		]);
		$path = $basePath . '/' . $filetreeModel->getCustomer() . '/' . $filetreeModel->getName();

		try {
			$filetreeModel::initPath($path);
			$filetreeModel->setPath($path);
		}
		catch( \Rbplm\Vault\ExistingDirectoryException $e ) {
			throw new ControllerException(sprintf('Reposit %s is yet existing, the creation is canceled', $path));
		}
		catch( \Exception $e ) {
			throw new ControllerException(sprintf('Unable to create reposit %s > %s', $path, $e->getMessage()), 10001);
		}

		if ( !is_file($template) ) {
			throw new ControllerException(sprintf('%s is not founded', $template), 10000);
		}

		if ( !is_dir($path) ) {
			throw new ControllerException(sprintf('%s is not founded', $path), 10010);
		}

		/* uncompress template archive */
		$zip = new \ZipArchive();
		if ( $zip->open($template) === true ) {
			$zip->extractTo($path);
			$zip->close();
		}
		else {
			throw new ControllerException(sprintf('cant uncompress file %s', $template), 10020);
		}

		/* init permissions */
		try {
			$fileserver = FiletreeModel::pathReplacements($this->config['fileserver'], [
				'%year%' => $year
			]);
			if ( $fileserver ) {
				$localFileserverPath = $localFileserverPath . '\\' . $filetreeModel->getCustomer() . '\\' . $filetreeModel->getName();
				$securityRules = $this->config['securityRules'];
				$this->setWinpermissions($localFileserverPath, $securityRules, $fileserver);
			}
		}
		catch( PermissionFailed $e ) {
			$this->errorStack()->error("Init of permissions FAILED :" . $e->getMessage());
		}

		/* create link */
		try {
			$winBasePath = FiletreeModel::pathReplacements($this->config['winBasePath'], [
				'%year%' => $year
			]);
			
			$desc = \Rbs\Number::normalize($filetreeModel->getDescription());
			$link = $basePath . '/' . $filetreeModel->getName() . '-' . $desc . '.lnk';
			
			$toWinPath = '\\\\' . $winBasePath . '\\' . $filetreeModel->getCustomer() . '\\' . $filetreeModel->getName(); // Double escape
			$this->createWindowsLink($link, $toWinPath);
		}
		catch( \Rbs\Sys\Win\ShortcutException $e ) {
			$this->errorStack()
				->warning("Creation of shortcut failed")
				->log($e->getMessage());
		}

		/* deploy files from docseeder types */
		$selected = $filetreeModel->templates;
		foreach( $this->getDq115Map($year) as $k => $dq115MapElemt ) {
			try {
				/* force ignore or execution of disabled elements */
				if ( $dq115MapElemt['disabled'] ) {
					if ( $dq115MapElemt['selected'] == false ) {
						continue;
					}
					else {
						self::deployFile($filetreeModel, $dq115MapElemt);
					}
				}
				else if ( is_array($selected) ) {
					if ( in_array($k, $selected) ) {
						self::deployFile($filetreeModel, $dq115MapElemt);
					}
				}
			}
			catch( Exception $e ) {
				$msg = sprintf("Unable to deploy file %s for reason : %s", $dq115MapElemt['label'], $e->getMessage());
				if ( $e->getCode() > 6000 ) {
					$this->errorStack()->error($msg);
				}
				else {
					$this->errorStack()->warning($msg);
				}
			}
		}

		return $this;
	}
}
