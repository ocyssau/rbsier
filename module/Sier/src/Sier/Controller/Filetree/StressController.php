<?php
namespace Sier\Controller\Filetree;

use Sier\Controller\Filetree\FiletreeController as AbstractController;
use Sier\Model\Filetree as FiletreeModel;

/**
 *
 * @author olivier
 *
 */
class StressController extends AbstractController
{

	/** @var string */
	public $pageId = 'sier_filetree_stress';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();
		
		$config = $this->getEvent()->getApplication()->getServiceManager()->get('config')['sier_filetree']['stress'];
		
		$this->config = [
			'basepath' => rtrim($config['local'], '/'),
			'template' => $config['template'],
			'fileserver' => $config['fileserver'],
			'localFileserverPath' => rtrim($config['localfileserver'], '/'),
			'securityRules' => $config['security.rule'],
			'winBasePath' => $config['clientaccess'],
			'dq115Map' => $config['dq115Map']
		];
	}
	
	/**
	 *
	 */
	protected function getForm($request = null)
	{
		$form = new \Sier\Form\Filetree\Stress\EditForm($this->view);
		return $form;
	}

	/**
	 *
	 */
	protected function getModel($request = null)
	{
		$reposit = FiletreeModel::init()->setName('')->setDescription('');
		return $reposit;
	}

	/**
	 * @param FiletreeModel $reposit
	 *
	 */
	protected function deploy(FiletreeModel $reposit)
	{
		/* unactive files access limitation */
		\Rbplm\Sys\Filesystem::isSecure(false);
		
		$basePath = $this->config['basepath'];
		$template = $this->config['template'];
		
		$path = $basePath . '/' . $reposit->getName();
		$reposit::initPath($path);
		$reposit->setPath($path);
		chgrp($path, 'calcul');

		if ( !is_file($template) ) {
			throw new \Exception(sprintf('%s is not founded', $template), 10000);
		}

		if ( !is_dir($path) ) {
			throw new \Exception(sprintf('%s is not founded', $path), 10010);
		}

		/* uncompress template archive */
		$zip = new \ZipArchive();
		if ( $zip->open($template) === true ) {
			$zip->extractTo($path);
			$zip->close();
		}
		else {
			throw new \Exception(sprintf('cant uncompress file %s', $template), 10020);
		}

		/* init permissions */
		try {
			$fileserver = $this->config['fileserver'];
			if ( $fileserver ) {
				$localFileserverPath = $this->config['localFileserverPath'];
				$localFileserverPath = $localFileserverPath . '\\' . $reposit->getName();
				
				$securityRules = $this->config['securityRules'];
				$this->setWinpermissions($localFileserverPath, $securityRules, $fileserver);
			}
		}
		catch( PermissionFailed $e ) {
			$this->errorStack()->error("Init of permissions FAILED :" . $e->getMessage());
		}

		/* create link */
		$link = $basePath . '/' . $reposit->getName() . '-' . $reposit->getDescription() . '.lnk';
		
		$winBasePath = $this->config['winBasePath'];
		$toWinPath = '\\\\' . $winBasePath . '\\' . $reposit->getName(); // Double escape
		
		$this->createWindowsLink($link, $toWinPath);

		return $this;
	}
}
