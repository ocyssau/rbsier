<?php
namespace Sier\Controller\Filetree;

use Application\Controller\AbstractController;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;
use Rbs\Space\Factory as DaoFactory;
use Sier\Model\Filetree;

/**
 *
 *
 */
class IndexController extends AbstractController
{

	/* @var string */
	public $pageId = 'sier_filetree';

	/* @var string */
	public $defaultSuccessForward = 'sier/filetree';

	/* @var string */
	public $defaultFailedForward = 'sier/filetree';

	/**
	 * Squeeze access control
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		$this->authIdentityCheck();

		if ( $request->isXmlHttpRequest() ) {
			$this->initAjax();
		}
		else {
			$this->init();
		}

		return AbstractActionController::dispatch($request, $respons);
	}

	/**
	 */
	public function init()
	{
		parent::init();
		$this->layout()->setTemplate('sier/layout/layout');
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;

		$view->setTemplate('sier/filetree/index');
		$view->pageTitle = 'Gestion des dossiers affaires';

		$view->config = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('config')['sier_filetree'];

		return $view;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function testAction()
	{
		$view = $this->view;
		$view->setTemplate('sier/filetree/test');
		$view->pageTitle = 'Tests of configurations';

		$config = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('config')['sier_filetree'];
		$view->config = $config;

		try {
			$this->testConf($config, 'ingenierie');
			$this->testConf($config, 'stress');
			$this->testConf($config, 'direction');
		}
		catch( ConfigurationError $e ) {
			$this->errorStack()->error($e->getMessage());
		}

		return $view;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function testsshconnexionAction()
	{
		
		$domain = $this->params()->fromRoute('domain');
		
		$view = $this->view;
		$view->setTemplate('sier/filetree/testsshconnexion');
		$view->pageTitle = sprintf('Tests ssh connexion for %s', $domain);

		$sm = $this->getEvent()
			->getApplication()
			->getServiceManager();

		$config = $sm->get('config')['sier_filetree'][$domain];
		
		$factory = $sm->get('Ssh');
		$fileserver = $config['fileserver'];
		
		if(!$fileserver){
			throw new \Exception(sprintf('fileserver directive is not set in config key sier_filetree.%s',$domain));
		}
		
		/* get ssh configuration for this server */
		$sshConfiguration = $sm->get('config')['rbp']['sier']['ssh'];
		if(!isset($sshConfiguration[$fileserver])){
			throw new \Exception(sprintf('ssh configuration key is not existing for this server. You must create and populate directive rbp.sier.ssh.%s', $fileserver));
		}
		else{
			$sshConfiguration = $sshConfiguration[$fileserver];
		}
		
		if($sshConfiguration['pubkeyfile']){
			$f = $sshConfiguration['pubkeyfile'];
			/* check public key */
			if(!is_readable($f)){
				throw new \Exception(sprintf('Public key file %s is not readable or not existing', $f));
			}
			
			$f = $sshConfiguration['privkeyfile'];
			if(!is_readable($f)){
				throw new \Exception(sprintf('Private key file %s is not readable or not existing', $f));
			}
		}
		
		$sshConfiguration['password'] = 'BLANKED - NOT VISIBLE HERE';
		$sshConfiguration['passphrase'] = 'BLANKED - NOT VISIBLE HERE';
		$view->sshConfiguration = $sshConfiguration;
		
		try {
			$connexion = $factory->get($fileserver);
			$connexion->setTimeOut(10);
		}
		catch( \Throwable $e ) {
			if ( $e->getCode() == 7100 ) {
				throw new PermissionFailed("Config key rbp.sier.ssh must have a key = name of fileserver '$fileserver'", 7100);
			}
			else {
				$this->errorStack()->error($e->getMessage());
			}
		}

		return $view;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function testConf($conf, $key)
	{
		$conf = $conf[$key];

		/* base path exists and it be writable */
		$year = date('Y');

		$path = $conf['local'];
		$path = Filetree::pathReplacements($path, [
			'%year%' => $year
		]);
		if ( !is_dir($path) ) {
			throw new ConfigurationError(sprintf('base path %s is not existing', $path));
		}
		if ( !is_writable($path) ) {
			throw new ConfigurationError(sprintf('base path %s is not writable', $path));
		}

		/* map file is existing and readable */
		$dq115Map = [];
		if ( isset($conf['dq115Map']) ) {
			$path = $conf['dq115Map'];
			if ( $path ) {
				$path = Filetree::pathReplacements($path, [
					'%year%' => $year
				]);
				if ( !is_readable($path) ) {
					throw new ConfigurationError(sprintf('mapfile for DQ115 %s is not readable', $path));
				}
				/* check each map115element */
				try {
					$dq115Map = include ($path);
					$this->checkDq115Map($dq115Map);
				}
				catch( \Exception $e ) {
					$content = file_get_contents($path);
					throw new ConfigurationError(sprintf('mapfile for DQ115 %s is malformed : %s , content : %s', $path, $e->getMessage(), $content));
				}
			}
		}
		$varname = 'dq115Map' . $key;
		$this->view->$varname = $dq115Map;

		/* template is existing */
		$path = $conf['template'];
		$path = Filetree::pathReplacements($path, [
			'%year%' => $year
		]);
		if ( !is_readable($path) ) {
			throw new ConfigurationError(sprintf('directory structure template %s is not readable', $path));
		}
	}

	/**
	 * 
	 * @param array $dq115Map
	 * @throws \Exception
	 */
	public function checkDq115Map(array $dq115Map)
	{
		/**/
		if ( !is_array($dq115Map) ) {
			throw new \Exception('dq115map is not a array');
		}

		/* check keys */
		$keys = [
			'disabled',
			'selected',
			'label',
			'filename',
			'type',
			'target',
			'document'
		];
		$docKeys = [
			'toSave',
			'number',
			'description',
			'label'
		];

		$workitemFactory = DaoFactory::get('Workitem');
		foreach( $dq115Map as $i => $dq115MapElmt ) {
			/**/
			if ( !is_array($dq115MapElmt) ) {
				throw new ConfigurationError(sprintf('dq115MapElmt %s is not a array', $i));
			}

			/* check format of map array */
			foreach( $keys as $k ) {
				if ( !isset($dq115MapElmt[$k]) ) {
					throw new ConfigurationError(sprintf('key %s is not set in the dq115map %s', $k));
				}
			}
			foreach( $docKeys as $k ) {
				if ( !isset($dq115MapElmt['document'][$k]) ) {
					throw new ConfigurationError(sprintf('dq115map key [document][%s] is not set', $k));
				}
			}

			/* load type */
			$number = $dq115MapElmt['type'];
			try {
				$type = new \Rbplm\Ged\Doctype();
				$workitemFactory->getDao($type->cid)->loadFromNumber($type, $number);
			}
			catch( \Rbplm\Dao\NotExistingException $e ) {
				throw new ConfigurationError(sprintf('Unable to load type number %s', $number));
			}
		}
	}
}
