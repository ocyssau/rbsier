<?php
namespace Sier\Controller\Filetree;

use Application\Controller\AbstractController;
use Sier\Model\Filetree as FiletreeModel;
use Rbplm\People\CurrentUser;
use Application\Controller\ControllerException;
use Throwable;
use Rbs\Sys\Win;

/**
 *
 * @author olivier
 *
 */
abstract class FiletreeController extends AbstractController
{

	/**
	 *
	 * @var string $pageId
	 */
	public $pageId = '';

	/**
	 *
	 * @var string $defaultSuccessForward
	 */
	public $defaultSuccessForward = 'sier/filetree';

	/**
	 *
	 * @var string $defaultFailedForward
	 */
	public $defaultFailedForward = 'sier/filetree';

	/** @var array */
	public $dq115Map;

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();
		$this->resourceCn = \Acl\Model\Resource\Ged::$appCn;

		$this->config = [
			'basepath' => '',
			'template' => '',
			'fileserver' => '',
			'localFileserverPath' => '',
			'securityRules' => '',
			'winBasePath' => '',
			'dq115Map' => ''
		];
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 * 
	 */
	abstract protected function getForm($request = null);

	/**
	 * 
	 */
	abstract protected function getModel($request = null);

	/**
	 * 
	 * @param \Sier\Model\Filetree $reposit
	 */
	abstract protected function deploy(\Sier\Model\Filetree $reposit);

	/**
	 * @param FiletreeModel $filetreeModel
	 * @param array $dq115MapElemt
	 */
	protected function deployFile(FiletreeModel $filetreeModel, array $dq115MapElemt)
	{
		/* @var \Sier\Model\Filetree\DeployHelper $helper */
		$helper = $filetreeModel->deployHelper;

		/* @var \Docseeder\Model\Docseeder $docseeder */
		$docseeder = clone ($filetreeModel->docseeder);
		
		$this->errorStack()->log(sprintf('try to deploy type %s from docseeder', $dq115MapElemt['type']));
		
		/* load doctype */
		try {
			$helper->loadTypeFromNumber($docseeder, $dq115MapElemt['type']);
		}
		catch( Throwable $e ) {
			throw new ControllerException(sprintf('creation of document is canceled for this reason > %s', $e->getMessage()), 5040);
		}

		try {
			/* load document template and his docfiles */
			$helper->loadTemplate($docseeder, /*withFiles*/ true);

			/* Load mask */
			try {
				$helper->loadMask($docseeder);
			}
			catch( \Rbplm\Dao\NotExistingException $e ) {
				$msg = sprintf('There is none mask to populate for this element %s > %s', $dq115MapElemt['label'], $e->getMessage());
				$this->errorStack()->warning($msg, 5055);
			}
		}
		catch( \Docseeder\Model\NoneTemplateException $e ) {
			$msg = sprintf('There is none template to populate for this element %s > %s', $dq115MapElemt['label'], $e->getMessage());
			$this->errorStack()->warning($msg, 5060);
		}
		catch( Throwable $e ) {
			$msg = sprintf('Unable to load template %s for this reason: > %s', $e->getMessage());
			throw new ControllerException($msg, 5065);
		}

		/* init directory path */
		$path = $helper->replacementApplyToPath($docseeder, $filetreeModel, $dq115MapElemt);
		if ( $path == "" ) {
			throw new ControllerException(sprintf('target directory %s is not existing', $dq115MapElemt['target']), 6010);
		}

		/* init filepath */
		$filename = $helper->replacementApplyToFilename($docseeder, $dq115MapElemt);
		
		$this->errorStack()->log(sprintf('try to deploy file %s from docseeder', $filename));
		
		/* add extension */
		$templateData = $docseeder->getType()->getTemplate()->mainDocfile->getData();
		$extension = $templateData->extension;
		$filename = $filename . $extension;
		$filepath = $path . "/" . $filename;
		if ( is_file($filepath) ) {
			throw new ControllerException(sprintf('file %s is existing and we dont want overwrite', $filepath), 5075);
		}

		/* init a document and populate with datas usable by populator */
		$this->errorStack()->log(sprintf('init document %s and populate with datas usable by populator', $filepath));
		$helper->buildDocument($docseeder, $dq115MapElemt);

		/* populate target file */
		try {
			$populator = $helper->getPopulator($docseeder);
			$this->errorStack()->log(sprintf('generate new file from template document %s with populator type %s', $filepath, get_class($populator)));
			if ( get_class($populator) == 'Docseeder\Model\Populator\Generic' ) {
				$this->errorStack()->log(sprintf('Type %s is not managed, and replacements can not be applied', $extension));
			}
			$populator->populate($docseeder, $filepath);
		}
		catch( Throwable $e ) {
			$this->errorStack()->log($e->getMessage());
			/* copy template if not */
			if ( !is_file($filepath) ) {
				@copy($templateData->fullpath, $filepath);
			}
		}
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		else {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}
		
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		/**/
		$reposit = $this->getModel();

		/* init Form */
		$form = $this->getForm($request);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($reposit);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$this->deploy($reposit);

					/* set identity file*/
					$this->setIdentityFile($reposit);

					/**/
				}
				catch( \Throwable $e ) {
					$this->errorStack()->error("Creation of reposit FAILED :" . $e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	protected function setWinpermissions($path, $securityRules, $fileserver)
	{
		//throw new PermissionFailed('setWinpermissions is desactivated', 7150);
		/* @var \Sier\Ssh\Factory $factory */
		
		$path = str_replace('/', '\\', $path);
		$factory = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Ssh');
		
		try {
			$connexion = $factory->get($fileserver);
			$connexion->setTimeOut(10);
		}
		catch( \Sier\Ssh\SshException $e ) {
			$this->errorStack()->warning("Initialization of permission failed")->log($e->getMessage());
			return;
		}
		catch( \Throwable $e ) {
			if ( $e->getCode() == 7100 ) {
				throw new PermissionFailed("Config key rbp.sier.ssh must have a key = name of fileserver '$fileserver'", 7100);
			}
			else {
				throw $e;
			}
		}

		$ret = [];
		foreach( $securityRules as $rule ) {
			try{
				$cmd = sprintf('icacls %s\* %s', $path, $rule);
				$this->errorStack()->log(sprintf('try to init permissions from command %s', $cmd));
				$ret[] = $connexion->exec($cmd);
			}
			catch( \Throwable $e ) {
				throw new PermissionFailed(sprintf('Permission settings failed for reason %s', $e->getMessage()), 7200);
			}
		}
		
		$this->errorStack()->log( implode(' - ', $ret));
		
		$connexion->restoreTimeOut();

		return $ret;
	}

	/**
	 * @param FiletreeModel $reposit
	 */
	protected function setIdentityFile(FiletreeModel $reposit)
	{
		$currentUser = CurrentUser::get();
		$identityFile = $reposit->getPath() . '/' . $reposit->getName() . '.xml';
		touch($identityFile);
		chmod($identityFile, 0755);
		$now = new \DateTime();
		$data = array(
			'<?xml version="1.0" encoding="UTF-8"?>',
			'<identity>',
			'<name>' . $reposit->getName() . '</name>',
			'<description>' . $reposit->getDescription() . '</description>',
			'<created>' . $now->format('d-m-Y H:i:s') . '</created>',
			'<createdBy>' . $currentUser->getLogin() . '</createdBy>',
			'</identity>'
		);
		file_put_contents($identityFile, implode("\r\n", $data));
	}

	/**
	 * @param string $link
	 * @param string $toPath
	 */
	protected function createWindowsLink($link, $toPath)
	{
		$link = Win\Shortcut::new($link, $toPath);
	}
}
