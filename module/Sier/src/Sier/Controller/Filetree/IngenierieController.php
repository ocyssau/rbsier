<?php
namespace Sier\Controller\Filetree;

use Sier\Controller\Filetree\FiletreeController as AbstractController;
use Sier\Model\Filetree as FiletreeModel;
use Rbs\Space\Factory as DaoFactory;
use Sier\Model\Filetree\DeployHelper;
use Docseeder\Model\Docseeder;
use Exception;
use Application\Controller\ControllerException;

/** @var array */
/** 
 $dq115Map = [
 'DQ30' => [
 'disabled' => true,
 'selected' => true,
 'label' => 'DQ30 Fiche identité affaire',
 'filename' => 'DQ30-%CONTAINER_NUMBER%-%LABEL%',
 'type' => 'SIER-CR',
 'target' => '%AFFAIRE%/PROJET',
 'document' => [
 'toSave' => false,
 'number' => '%AUTONUM%',
 'description' => 'DQ30 Fiche identité affaire',
 'label' => ''
 ]
 ],
 'DQ102' => [
 'disabled' => false,
 'selected' => false,
 'label' => 'DQ102 Matrice de conformité',
 'filename' => '%AUTONUM%-DQ102',
 'type' => 'SIER-CR',
 'target' => '%AFFAIRE%/PROJET',
 'document' => [
 'toSave' => true,
 'number' => '%AUTONUM%',
 'description' => '',
 'label' => ''
 ]
 ],
 'DQ116' => [
 'disabled' => true,
 'selected' => true,
 'label' => 'DQ116 Checklist directive machine',
 'filename' => '%AUTONUM%-DQ116',
 'type' => 'SIER-CR',
 'target' => '%AFFAIRE%/PROJET/BE',
 'document' => [
 'toSave' => true,
 'number' => '%AUTONUM%',
 'description' => '',
 'label' => ''
 ]
 ]
 ];
 */

/**
 *
 * @author olivier
 *
 */
class IngenierieController extends AbstractController
{

	/** @var string */
	public $pageId = 'sier_filetree_ingenierie';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		$config = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('config')['sier_filetree']['ingenierie'];

		$this->config = [
			'basepath' => rtrim($config['local'], '/'),
			'template' => $config['template'],
			'fileserver' => $config['fileserver'],
			'localFileserverPath' => rtrim($config['localfileserver'], '/'),
			'securityRules' => $config['security.rule'],
			'winBasePath' => $config['clientaccess'],
			'dq115Map' => $config['dq115Map']
		];
	}

	/**
	 *
	 */
	protected function getForm($request = null)
	{
		$form = new \Sier\Form\Filetree\Ingenierie\EditForm($this->view);
		$form->setDq115Map($this->getDq115Map());
		$form->setBasedir($this->config['basepath']);
		return $form;
	}

	/**
	 *
	 */
	protected function getModel($request = null)
	{
		$reposit = FiletreeModel::init()->setName('')->setDescription('');
		return $reposit;
	}

	/**
	 * @throws \Exception
	 * @return array
	 */
	protected function getDq115Map()
	{
		if ( !isset($this->dq115Map) ) {
			$mapFile = $this->config['dq115Map'];
			if ( !is_readable($mapFile) ) {
				throw new \Exception(sprintf('file %s is not readable', $mapFile));
			}

			try {
				$this->dq115Map = include ($mapFile);
			}
			catch( \Throwable $e ) {
				throw new ControllerException(sprintf('%s have synthaxe errors : %s on line %s', $mapFile, $e->getMessage(), $e->getLine()), $e);
			}

			if ( !is_array($this->dq115Map) ) {
				throw new \Exception(sprintf('Import of map %s failed, creation of filetree is canceled', $mapFile));
			}
		}
		return $this->dq115Map;
	}

	/**
	 * @param FiletreeModel $reposit
	 */
	protected function deploy(FiletreeModel $filetreeModel)
	{

		/* init helpers */
		$workitemFactory = DaoFactory::get('Workitem');

		$helper = new DeployHelper($workitemFactory);
		$helper->setConfig(\Ranchbe::get()->getConfig());

		$filetreeModel->deployHelper = $helper;

		/* load project, workitem, type in docseeder */
		try {
			$docseeder = new Docseeder();
			$docseeder->containerId = $filetreeModel->getContainerId();
			$helper->loadContainer($docseeder);
			$filetreeModel->setName($docseeder->getContainer()
				->getNumber());
			$filetreeModel->setDescription($docseeder->getContainer()->description);
			$filetreeModel->docseeder = $docseeder;
		}
		catch( Exception $e ) {
			$msg = sprintf('Unable to load container for this reason: > %s', $e->getMessage());
			throw new ControllerException($msg);
		}

		/* unactive files access limitation */
		\Rbplm\Sys\Filesystem::isSecure(false);

		$basePath = $this->config['basepath'];
		$template = $this->config['template'];

		$path = $basePath . '/' . $filetreeModel->getName();

		$this->errorStack()->log(sprintf('basepath = %s; template=%s', $basePath, $template));

		try {
			$filetreeModel::initPath($path);
			$filetreeModel->setPath($path);
		}
		catch( \Rbplm\Vault\ExistingDirectoryException $e ) {
			throw new ControllerException(sprintf('Reposit %s is yet existing, the creation is canceled', $path));
		}
		catch( \Exception $e ) {
			throw new ControllerException(sprintf('Unable to create reposit %s > %s', $path, $e->getMessage()), 10001);
		}

		if ( !is_file($template) ) {
			throw new ControllerException(sprintf('%s is not founded', $template), 10000);
		}

		if ( !is_dir($path) ) {
			throw new ControllerException(sprintf('%s is not founded', $path), 10010);
		}

		/* Uncompress template archive */
		$zip = new \ZipArchive();
		if ( $zip->open($template) === true ) {
			$zip->extractTo($path);
			$zip->close();
		}
		else {
			throw new ControllerException(sprintf('cant uncompress file %s', $template), 10020);
		}

		/* */
		$path = $filetreeModel->getPath();

		/* init permissions */
		try {
			$fileserver = $this->config['fileserver'];
			if ( $fileserver ) {
				$localFileserverPath = $this->config['localFileserverPath'];
				$localFileserverPath = $localFileserverPath . '/' . $filetreeModel->getName();

				$securityRules = $this->config['securityRules'];
				$this->setWinpermissions($localFileserverPath, $securityRules, $fileserver);
			}
		}
		catch( PermissionFailed $e ) {
			$this->errorStack()->error("Init of permissions FAILED :" . $e->getMessage());
		}

		/* create link */
		try {
			$desc = \Rbs\Number::normalize($filetreeModel->getDescription());

			$link = $basePath . '/' . $filetreeModel->getName() . '-' . $desc . '.lnk';

			$winBasePath = $this->config['winBasePath'];
			$toWinPath = '\\\\' . $winBasePath . '\\' . $filetreeModel->getName(); // Double escape

			$this->createWindowsLink($link, $toWinPath);
		}
		catch( \Rbs\Sys\Win\ShortcutException $e ) {
			$this->errorStack()
				->warning("Creation of shortcut failed")
				->log($e->getMessage());
		}

		/* deploy files from docseeder types */
		$selected = $filetreeModel->templates;
		foreach( $this->getDq115Map() as $k => $dq115MapElemt ) {
			try {
				/* force ignore or execution of disabled elements */
				if ( $dq115MapElemt['disabled'] ) {
					if ( $dq115MapElemt['selected'] == false ) {
						continue;
					}
					else {
						self::deployFile($filetreeModel, $dq115MapElemt);
					}
				}
				else if ( is_array($selected) ) {
					if ( in_array($k, $selected) ) {
						self::deployFile($filetreeModel, $dq115MapElemt);
					}
				}
			}
			catch( \Throwable $e ) {
				$msg = sprintf("Unable to deploy file %s for reason : %s", $dq115MapElemt['label'], $e->getMessage());
				$this->errorStack()
					->error($msg)
					->log($msg);
			}
		}

		return $this;
	}
}
