<?php
namespace Sier\Controller;

use Application\Controller\AbstractController;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;

/**
 *
 *
 */
class AbstractPublicController extends AbstractController
{

	/**
	 * Squeeze access control
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		if ( $request->isXmlHttpRequest() ) {
			$this->initAjax();
		}
		else {
			$this->init();
		}

		return AbstractActionController::dispatch($request, $respons);
	}

	/**
	 * HTTP GET REQUEST
	 * 
	 * Download file
	 */
	public function viewdocumentAction()
	{
		$documentId = $this->getRequest()->getQuery('documentid', null);

		$document = new \Rbplm\Ged\Document\Version();
		$factory = $this->factory;
		$factory->getDao($document->cid)->loadFromId($document, $documentId);

		$viewer = new \Rbs\Ged\Document\Viewer();
		$viewer->initFromDocument($document);
		return $viewer->push();
	}
}
