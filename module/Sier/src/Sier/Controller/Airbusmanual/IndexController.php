<?php
namespace Sier\Controller\Airbusmanual;

use Sier\Controller\AbstractPublicController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\AccessCode;

/**
 */
class IndexController extends AbstractPublicController
{

	public $pageId = 'airbus_manual';

	public $defaultSuccessForward = 'airbusmanual/index';

	public $defaultFailedForward = 'airbusmanual/index';

	/**
	 */
	public function init()
	{
		parent::init();

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		$this->containerIds = array(
			835, // Id du conteneur NormesAirbus
			836, // Id du conteneur MemosAirbus
			837, // Id du conteneur DirectivesFAL-GSE
			838 // Id du conteneur NormesDassaultBreguet
		);
		$this->layout()->setTemplate('sier/layout/layout');
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('sier/airbusmanual/index');
		$spacename = 'bookshop';
		$factory = DaoFactory::get($spacename);
		$request = $this->getRequest();
		$containerId = $this->containerIds[0];

		$validate = $request->getQuery('filtered');
		$reset = $request->getQuery('resetf');

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Sier\Form\Airbusmanual\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'filterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		if ( $filterForm->isValid() && $validate && !$reset ) {
			$dao = $factory->getDao(Document\Version::$classId);
			$select = array();
			foreach( $dao->metaModel as $asSys => $asApp ) {
				$select[] = $asSys . ' as ' . $asApp;
			}
			$select[] = 'maj_int_ind';
			$select[] = 'min_int_ind';

			$filter->select($select);
			$filter->andfind(AccessCode::HISTORY, $dao->toSys('accessCode'), Op::NOTEQUAL);

			$subfilter = new \Rbs\Dao\Sier\Filter('', false);
			foreach( $this->containerIds as $containerId ) {
				$subfilter->orfind($containerId, $dao->toSys('parentId'), Op::EQUAL);
			}
			$filter->suband($subfilter);

			$list = $factory->getList(Document\Version::$classId);
			$list->countAll = $list->countAll($filter);
			$list->load($filter, $filterForm->bind);
			$view->list = $list->toArray();
			$view->countOutput = $list->countAll;
		}
		else {
			$view->list = array();
			$view->countOutput = 0;
		}

		$view->searchFeedback = "";
		$view->filter = $filterForm;
		return $view;
	}
}
