<?php
namespace Sier\Controller;

use Application\Controller\AbstractCliController as AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Document;

/**
 *  
 *
 */
class CliController extends AbstractController
{

	/**
	 */
	public function init()
	{
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 */
	public function renamefileAction()
	{
		$request = $this->getRequest();
		//$this->authenticate($request);

		/*
		 ([A-Za-z0-9]+[\.]{1})([A-Za-z0-9]+[\.]{1})([A-Za-z0-9]+)
		 */

		//$regex1 = $request->getParam('r', '/([A-Za-z0-9]+[\.]{1})([A-Za-z0-9]+[\.]{1})([A-Za-z0-9]+)/i');
		//$groups = $request->getParam('g', [0, 1]);

		$spacename = $request->getParam('spacename');
		$vaultDirectory = $request->getParam('directory');

		$factory = DaoFactory::get($spacename);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$docDao = $factory->getDao(Document\Version::$classId);

		$directory = new \DirectoryIterator($vaultDirectory);
		foreach( $directory as $file ) {
			if ( $directory->isFile() ) {
				$filename1 = $file->getFilename();
				$matches = [];

				//preg_match_all($regex1, $filename1, $matches);
				//var_dump($matches);

				$matches = explode('.', $filename1);
				//var_dump($matches);die;

				$filename2 = $matches[0] . '.' . $matches[1];
				echo $filename2 . "\n\r";

				try {
					$docfile = new Docfile\Version();
					$dfDao->loadFromName($docfile, $filename1);
					$docfile->setName($filename2);
					$docfile->setNumber($filename2);
					//$dfDao->save($docfile);
				}
				catch( \Exception $e ) {
					throw $e;
				}

				try {
					$document = new Document\Version();
					$docDao->loadFromId($document, $docfile->parentId);
					$document->setName($matches[0]);
					$document->setNumber($matches[0]);
					//$docDao->save($document);
				}
				catch( \Exception $e ) {
					throw $e;
				}
			}
		}
	}
}
