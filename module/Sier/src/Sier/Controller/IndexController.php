<?php
namespace Sier\Controller;

/**
 *
 *
 */
class IndexController extends AbstractPublicController
{

	public $pageId = 'sier_index';

	public $defaultSuccessForward = 'sier';

	public $defaultFailedForward = 'sier';

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$this->layout('layout/ranchbe');

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('custom');
		$this->layout()->tabs = $tabs;
		return $this->view;
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function sleepAction()
	{
		while( true ) {
			null;
		}
		die();
	}
}
