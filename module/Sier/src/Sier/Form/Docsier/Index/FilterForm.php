<?php
namespace Sier\Form\Docsier\Index;

use Rbplm\Dao\Filter\Op;
use Application\Form\AbstractFilterForm;
use Rbplm\Ged\Document;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 *
	 */
	public function __construct($factory = null, $name = null, $containerId)
	{
		parent::__construct($factory, $name);

		$this->template = 'sier/docsier/index/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Quick search */
		$this->add(array(
			'name' => 'quick_search',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class' => ''
			),
			'options' => array(
				'label' => 'Recherche Rapide'
			)
		));
		$inputFilter->add(array(
			'name' => 'quick_search',
			'required' => false
		));

		/* KeyWord */
		$this->add(array(
			'name' => 'docsier_keyword',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class' => ''
			),
			'options' => array(
				'label' => 'Mot clé'
			)
		));
		$inputFilter->add(array(
			'name' => 'docsier_keyword',
			'required' => false
		));

		/* Construct select field for docsier_fournisseur */
		$this->add(array(
			'name' => 'docsier_fournisseur',
			'type' => 'Application\Form\Element\SelectFromDb',
			'attributes' => array(
				'multiple' => false
			),
			'options' => array(
				'label' => tra('Fournisseur'),
				'dbFieldForName' => 'number',
				'dbFieldForValue' => 'id',
				'dbtable' => 'partners',
				'dbfilter' => '1=1'
			)
		));
		$inputFilter->add(array(
			'name' => 'docsier_fournisseur',
			'required' => false
		));

		/* Construct select field for docsier_famille */
		$this->add(array(
			'name' => 'docsier_famille',
			'type' => 'Application\Form\Element\SelectFromDb',
			'attributes' => array(
				'multiple' => false
			),
			'options' => array(
				'label' => tra('Famille'),
				'dbFieldForName' => "concat(id, '-', famille_name)",
				'dbFieldForValue' => 'id',
				'dbtable' => 'bookshop_familles',
				'dbsortBy' => 'famille_name'
			)
		));
		$inputFilter->add(array(
			'name' => 'docsier_famille',
			'required' => false
		));

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(Document\Version::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		//QUICK SEARCH
		if ( $datas['quick_search'] ) {
			$filter->andFind($datas['quick_search'], $dao->toSys('number'), Op::CONTAINS);
		}

		//KEYWORD
		if ( $datas['docsier_keyword'] ) {
			$filter->andFind($datas['docsier_keyword'], $dao->toSys('description'), Op::CONTAINS);
		}

		//SUPPLIER
		if ( $datas['docsier_fournisseur'] ) {
			$filter->andFind($datas['docsier_fournisseur'], 'docsier_fournisseur', Op::EQUAL);
		}

		//FAMILLE
		if ( $datas['docsier_famille'] ) {
			$filter->andFind($datas['docsier_famille'], 'docsier_famille', Op::EQUAL);
		}

		return $this;
	}
}
