<?php
namespace Sier\Form\Docsier\Aidememoire;

use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\Document;
use Application\Form\AbstractFilterForm;


/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{
	
	/**
	 *
	 */
	public function __construct($factory = null, $name = null, $containerId)
	{
		parent::__construct($factory, $name);

		$this->template = 'sier/docsier/aidememoire/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Magic */
		$this->add(array(
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control',
				'title' => 'Search in name, number, designation, File extension'
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_magic',
			'required' => false
		));

		/* Only published */
		$this->add(array(
			'name' => 'cb_published',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Display Published Only',
				'value' => 1
			)
		));
		$inputFilter->add(array(
			'name' => 'cb_published',
			'required' => false
		));

		/* Category */
		$this->add(array(
			'name' => 'find_category',
			'type' => 'Application\Form\Element\SelectCategory',
			'attributes' => array(
				'multiple' => false
			),
			'options' => array(
				'label' => tra('Category'),
				'containerId' => $containerId,
				'daoFactory' => $factory,
				'fullname' => true
			)
		));

		/* Designation */
		$this->add(array(
			'name' => 'find_designation',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Designation',
				'class' => 'form-control',
				'data-where' => 'description',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_designation',
			'required' => false
		));

		/* Number */
		$this->add(array(
			'name' => 'find_number',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Input here',
				'class' => 'form-control',
				'data-where' => 'number',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Number'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_number',
			'required' => false
		));

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter($filter)
	{
		$translator = $this->daoFactory->getDao(Document\Version::$classId)->getTranslator();
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* MAGIC */
		if ( $datas['find_magic'] ) {
			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, `%s`, '-')", $translator->toSys('name'), $translator->toSys('description'), $translator->toSys('tags'));
			$filter->andFind($datas['find_magic'], $concat, Op::OP_CONTAINS);
		}

		/* PUBLISHED */
		if ( $datas['cb_published'] == 1 || is_null($datas['cb_published']) ) {
			$filter->andfind('published', $translator->toSys('lifeStage'), Op::CONTAINS);
		}

		/* CATEGORY */
		if ( isset($datas['find_category']) && $datas['find_category'] ) {
			$filter->andFind($datas['find_category'], $translator->toSys('categoryId'), Op::CONTAINS);
		}

		/* NUMBER */
		if ( $datas['find_number'] ) {
			$filter->andFind($datas['find_number'], $translator->toSys('number'), Op::CONTAINS);
		}

		/* DESIGNATION */
		if ( $datas['find_designation'] ) {
			$filter->andFind($datas['find_designation'], $translator->toSys('description'), Op::CONTAINS);
		}

		return $this;
	}

	/**
	 * @param \Rbplm\Dao\DaoListAbstract $engine
	 * @return \Portail\Form\Component\Documentlist\BuilderForm
	 */
	public function setSearchEngine(\Rbplm\Dao\DaoListAbstract $engine)
	{
		$this->searchEngine = $engine;
		return $this;
	}

	/**
	 * @return \Rbplm\Dao\DaoListAbstract $engine
	 */
	public function getSearchEngine()
	{
		return $this->searchEngine;
	}
}
