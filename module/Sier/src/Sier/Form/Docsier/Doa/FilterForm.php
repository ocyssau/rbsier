<?php
namespace Sier\Form\Docsier\Doa;

use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\Document;
use Portail\Form\Component\Documentlist\PublicFilterAbtract;
use Portail\Model\Component\Documentlist;


/**
 *
 *
 */
class FilterForm extends PublicFilterAbtract
{

	/**
	 *
	 */
	public function __construct($factory = null, Documentlist $component)
	{
		parent::__construct($factory, $component);
		$this->template = 'sier/docsier/doa/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$containerId = null;

		/* Magic */
		$this->add(array(
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control',
				'title' => 'Search in name, number, designation, File extension'
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_magic',
			'required' => false
		));

		/* Category */
		$this->add(array(
			'name' => 'find_category',
			'type' => 'Application\Form\Element\SelectCategory',
			'attributes' => array(
				'multiple' => false
			),
			'options' => array(
				'label' => tra('Category'),
				'containerId' => $containerId,
				'daoFactory' => $factory,
				'fullname' => true
			)
		));

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(Document\Version::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* MAGIC */
		if ( isset($datas['find_magic']) && $datas['find_magic'] ) {
			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, `%s`, '-')", $dao->toSys('name'), $dao->toSys('description'), $dao->toSys('tags'));
			$filter->andFind($datas['find_magic'], $concat, Op::OP_CONTAINS);
		}

		/* CATEGORY */
		if ( isset($datas['find_category']) && $datas['find_category'] ) {
			$filter->andFind($datas['find_category'], $dao->toSys('categoryId'), Op::CONTAINS);
		}

		return $this;
	}
}
