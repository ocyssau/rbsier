<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Sier\Form\Element;

use Application\Form\Element\SelectContainer as BaseElement;

/**
 * 
 *
 */
class SelectContainerWithoutFiltree extends BaseElement
{

	/**
	 *
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		/* add default options */
		$this->localOptions['basedir'] = null;
		parent::__construct($name, $options);
	}

	/**
	 *
	 * @param \PDOStatement $stmt
	 */
	protected function stmtToSelectSet(\PDOStatement $stmt)
	{
		$maybenull = $this->options['maybenull'];
		$displayBoth = $this->options['displayBoth'];
		$fullname = $this->options['fullname'];
		$returnName = $this->options['returnName'];
		($maybenull) ? $selectSet = [
			null => $this->getUnselectedValue()
		] : $selectSet = [];
		$baseDir = $this->options['basedir'];

		if ( !$baseDir ) {
			throw new \Exception('baseDir option is not set');
		}

		while( $item = $stmt->fetch(\PDO::FETCH_ASSOC) ) {
			$itemLabel = $item['label'];

			/* filetree directory is existing */
			$path = $baseDir . '/' . $itemLabel;
			if ( is_dir($path) ) {
				continue;
			}

			$id = $item['id'];
			if ( $displayBoth ) {
				$itemLabel = $id . '-' . $itemLabel;
			}
			if ( $fullname && isset($item['title']) ) {
				$itemLabel = $itemLabel . ' (' . $item['title'] . ')';
			}
			if ( $returnName ) {
				$selectSet[$itemLabel] = $itemLabel;
			}
			else {
				$selectSet[$id] = $itemLabel;
			}
		}
		return $selectSet;
	}
}
