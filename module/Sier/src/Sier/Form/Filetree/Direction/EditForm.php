<?php
namespace Sier\Form\Filetree\Direction;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Rbs\Space\Factory as DaoFactory;

/**
 *
 *
 */
class EditForm extends \Application\Form\AbstractForm implements InputFilterProviderInterface
{

	/**
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @var InputFilter
	 */
	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view, $year)
	{
		/* we want to ignore the name passed */
		parent::__construct('directionFiletreeEdit');
		$this->template = 'sier/filetree/direction/editform';
		$view->pageTitle = 'Create Direction Data Folders Tree';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 40
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Year */
		$this->add(array(
			'name' => 'year',
			'type' => 'Application\Form\Element\SelectPicker',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Year'),
				'value_options' => $this->_getYears(),
				'multiple' => false,
				'livesearch' => false
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$spacename = 'workitem';
		$ranchbe = \Ranchbe::get();

		return array(
			'id' => array(
				'required' => false
			),
			'containerId' => array(
				'required' => true
			),
			'description' => array(
				'required' => false
			),
			'customer' => array(
				'required' => true
			),
			'templates' => array(
				'required' => false
			),
			'name' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/' . $ranchbe->getConfig($spacename . '.name.mask') . '/i',
							'messages' => array(
								\Zend\Validator\Regex::INVALID => $ranchbe->getConfig($spacename . '.name.mask.help')
							)
						)
					)
				)
			)
		);
	}

	/**
	 *
	 * @param string $baseDir
	 */
	public function setBasedir($baseDir, $year = null)
	{
		if ( !$year ) {
			$year = date('Y');
		}

		/* build list of customers folders from basedir */
		try {
			$it = new \DirectoryIterator($baseDir);
			$output = [];
			foreach( $it as $fileinfo ) {
				if ( $fileinfo->isDot() ) {
					continue;
				}
				if ( $fileinfo->isDir() ) {
					$fileName = $fileinfo->getFilename();
					if ( $fileName == 'TEMPLATE' ) {
						continue;
					}
					$output[$fileName] = $fileName;
				}
			}
		}
		catch( \Exception $e ) {
			throw new \Exception(sprintf('INVALID_PATH %s, see your admin to create this folder.', $baseDir));
		}
		ksort($output);

		/* Customer */
		$this->add(array(
			'name' => 'customer',
			'type' => 'Application\Form\Element\SelectPicker',
			'attributes' => array(
				'class' => 'select-customer'
			),
			'options' => array(
				'label' => tra('Customer'),
				'value_options' => $output,
				'multiple' => false,
				'livesearch' => true
			)
		));

		/* build list of containers without courrier folder */
		$this->add(array(
			'name' => 'containerId',
			'type' => 'Sier\Form\Element\SelectContainerWithoutFiltree',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'select-containerid'
			),
			'options' => array(
				'label' => tra('Number'),
				'daoFactory' => DaoFactory::get('Workitem'),
				'basedir' => $baseDir,
				'dbsortBy' => 'id',
				'dbsortOrder' => 'desc',
				'fullname' => true
			)
		));
	}

	/**
	 *
	 * @param array $dq115map
	 * @return \Sier\Form\Filetree\Ingenierie\EditForm
	 */
	public function setDq115Map(array $dq115map)
	{
		$valueOptions = [];
		foreach( $dq115map as $k => $elemt ) {

			$selected = isset($elemt['selected']) ? (bool)$elemt['selected'] : false;
			$disabled = isset($elemt['disabled']) ? (bool)$elemt['disabled'] : true;

			$valueOptions[] = [
				'value' => $k,
				'label' => $elemt['label'],
				'selected' => $selected,
				'disabled' => $disabled,
				'attributes' => [
					'id' => $k
				],
				'label_attributes' => [
					'class' => 'checkbox'
				]
			];
		}

		/* template checkboxes*/
		$this->add(array(
			'name' => 'templates',
			'type' => 'Zend\Form\Element\MultiCheckbox',
			'attributes' => array(
				'class' => ''
			),
			'options' => array(
				'label' => tra('documents to inits'),
				'use_hidden_element' => true,
				'value_options' => $valueOptions
			)
		));

		return $this;
	}

	/**
	 *
	 */
	protected function _getYears()
	{
		$year = date('Y');
		$output = [
			$year => $year,
			$year + 1 => $year + 1
		];
		return $output;
	}
}
