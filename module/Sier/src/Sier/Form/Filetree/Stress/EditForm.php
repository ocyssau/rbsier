<?php
namespace Sier\Form\Filetree\Stress;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;

/**
 *
 *
 */
class EditForm extends \Application\Form\AbstractForm implements InputFilterProviderInterface
{

	/**
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @var InputFilter
	 */
	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('stressFiletreeEdit');
		$this->template = 'sier/filetree/stress/editform';
		$view->pageTitle = 'Create Stress Data Folders Tree';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 40
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$spacename = 'workitem';
		$ranchbe = \Ranchbe::get();

		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => true
			),
			'name' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/' . $ranchbe->getConfig($spacename . '.name.mask') . '/i',
							'messages' => array(
								\Zend\Validator\Regex::INVALID => $ranchbe->getConfig($spacename . '.name.mask.help')
							)
						)
					)
				)
			)
		);
	}
}
