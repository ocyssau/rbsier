<?php
namespace Sier\Form\Filetree\Ingenierie;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Rbs\Space\Factory as DaoFactory;

/**
 *
 *
 */
class EditForm extends \Application\Form\AbstractForm implements InputFilterProviderInterface
{

	/**
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @var InputFilter
	 */
	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('ingenierieFiletreeEdit');
		$this->template = 'sier/filetree/ingenierie/editform';
		$view->pageTitle = 'Create Ingenierie Data Folders Tree';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Container name */
		/*
		 $this->add(array(
		 'name' => 'name',
		 'type' => 'Zend\Form\Element',
		 'attributes' => array(
		 'type' => 'text',
		 'placeholder' => '',
		 'class' => 'form-control'
		 ),
		 'options' => array(
		 'label' => tra('Number')
		 )
		 ));
		 */

		/* Affaires liées */
		/*
		 $this->add(array(
		 'name' => 'linkedWorkitem',
		 'type' => 'Zend\Form\Element',
		 'attributes' => array(
		 'type' => 'text',
		 'placeholder' => '',
		 'class' => 'form-control'
		 ),
		 'options' => array(
		 'label' => tra('Linked Workitems')
		 )
		 ));
		 */

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 40
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * 
	 * @param string $baseDir
	 */
	public function setBasedir($baseDir)
	{
		/* Container Id */
		$this->add(array(
			'name' => 'containerId',
			'type' => 'Sier\Form\Element\SelectContainerWithoutFiltree',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number'),
				'daoFactory' => DaoFactory::get('Workitem'),
				'basedir' => $baseDir,
				'dbsortBy' => 'id',
				'dbsortOrder' => 'desc',
				'fullname' => true
			)
		));
	}

	/**
	 * 
	 * @param array $dq115map
	 * @return \Sier\Form\Filetree\Ingenierie\EditForm
	 */
	public function setDq115Map(array $dq115map)
	{
		$valueOptions = [];
		foreach( $dq115map as $k => $elemt ) {

			$selected = isset($elemt['selected']) ? (bool)$elemt['selected'] : false;
			$disabled = isset($elemt['disabled']) ? (bool)$elemt['disabled'] : true;
			
			$valueOptions[] = [
				'value' => $k,
				'label' => $elemt['label'],
				'selected' => $selected,
				'disabled' => $disabled,
				'attributes' => [
					'id' => $k
				],
				'label_attributes' => [
					'class' => 'checkbox'
				]
			];
		}

		/* template checkboxes*/
		$this->add(array(
			'name' => 'templates',
			'type' => 'Zend\Form\Element\MultiCheckbox',
			'attributes' => array(
				'class' => ''
			),
			'options' => array(
				'label' => tra('documents to inits'),
				'use_hidden_element' => true,
				'value_options' => $valueOptions
			)
		));

		return $this;
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$spacename = 'workitem';
		$ranchbe = \Ranchbe::get();

		return array(
			'id' => array(
				'required' => false
			),
			'containerId' => array(
				'required' => true
			),
			'description' => array(
				'required' => false
			),
			'linkedWorkitem' => array(
				'required' => false
			),
			'templates' => array(
				'required' => false
			),
			'name' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/' . $ranchbe->getConfig($spacename . '.name.mask') . '/i',
							'messages' => array(
								\Zend\Validator\Regex::INVALID => $ranchbe->getConfig($spacename . '.name.mask.help')
							)
						)
					)
				)
			)
		);
	}
}
