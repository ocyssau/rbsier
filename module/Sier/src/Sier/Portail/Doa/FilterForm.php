<?php
namespace Sier\Portail\Doa;

use Rbplm\Dao\Filter\Op;
use Portail\Form\Component\Documentlist\PublicFilterAbtract;
use Portail\Model\Component\Documentlist;
use Rbplm\Ged\AccessCode;
use Rbs\Space\Factory as DaoFactory;

/**
 *
 *
 */
class FilterForm extends PublicFilterAbtract
{

	/**
	 *
	 */
	public function __construct($factory = null, Documentlist $component)
	{
		$compConfig = $component->getAttribute('config');
		if ( !isset($compConfig['containers']) ) {
			throw new \Exception('You must define a config key "containers" as array of ids of containers to interrogate. Set this config key in the document list component');
		}

		$this->containers = $compConfig['containers'];
		$container = current($this->containers);
		$containerId = $container[0];
		$spacename = $container[1];
		
		$factory = DaoFactory::get($spacename);
		parent::__construct($factory, $component);
		$this->setSearchEngine(new SearchEngineDao($factory->getConnexion()));

		if ( !isset($compConfig['template']) ) {
			$this->template = 'sier/portail/aidememoire/filterform';
		}
		else {
			$this->template = $compConfig['template'];
		}
		$inputFilter = $this->getInputFilter();

		/* Magic */
		$this->add(array(
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control',
				'title' => 'Search in name, number, designation, File extension'
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_magic',
			'required' => false
		));

		/* Only published */
		$this->add(array(
			'name' => 'cb_published',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Display Published Only',
				'value' => 1
			)
		));
		$inputFilter->add(array(
			'name' => 'cb_published',
			'required' => false
		));

		/* Category */
		$this->add(array(
			'name' => 'find_category',
			'type' => 'Application\Form\Element\SelectCategory',
			'attributes' => array(
				'multiple' => false
			),
			'options' => array(
				'label' => tra('Category'),
				'containerId' => $containerId,
				'daoFactory' => $factory,
				'fullname' => true
			)
		));

		/* Designation */
		$this->add(array(
			'name' => 'find_designation',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Designation',
				'class' => 'form-control',
				'data-where' => 'description',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_designation',
			'required' => false
		));

		/* Number */
		$this->add(array(
			'name' => 'find_number',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Input here',
				'class' => 'form-control',
				'data-where' => 'number',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Number'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_number',
			'required' => false
		));

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter($filter)
	{
		$trans = $this->getSearchEngine()->getTranslator();
		$this->prepare()->isValid();
		$datas = $this->getData();

		$filter->select($trans->getSelectAsApp());
		$filter->andfind(AccessCode::HISTORY, 'doc.' . $trans->toSys('accessCode'), Op::NOTEQUAL);

		foreach( $this->containers as $container ) {
			$subfilter = new \Rbs\Dao\Sier\Filter('', false);
			$containerId = $container[0];
			$spacename = $container[1];
			$subfilter->andfind($containerId, $trans->toSys('parentId'), Op::EQUAL);
			$subfilter->andfind($spacename, $trans->toSys('spacename'), Op::EQUAL);
			$filter->subor($subfilter);
		}

		/* MAGIC */
		if ( $datas['find_magic'] ) {
			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, `%s`, '-')", $trans->toSys('name'), $trans->toSys('description'), $trans->toSys('tags'));
			$filter->andFind($datas['find_magic'], $concat, Op::OP_CONTAINS);
		}

		/* PUBLISHED */
		if ( $datas['cb_published'] == 1 || is_null($datas['cb_published']) ) {
			$filter->andfind('published', $trans->toSys('lifeStage'), Op::CONTAINS);
		}

		/* CATEGORY */
		if ( isset($datas['find_category']) && $datas['find_category'] ) {
			$filter->andFind($datas['find_category'], $trans->toSys('categoryName'), Op::CONTAINS);
		}

		/* NUMBER */
		if ( $datas['find_number'] ) {
			$filter->andFind($datas['find_number'], $trans->toSys('number'), Op::CONTAINS);
		}

		/* DESIGNATION */
		if ( $datas['find_designation'] ) {
			$filter->andFind($datas['find_designation'], $trans->toSys('description'), Op::CONTAINS);
		}

		return $this;
	}

	/**
	 * @param \Rbplm\Dao\DaoListAbstract $engine
	 * @return \Portail\Form\Component\Documentlist\BuilderForm
	 */
	public function setSearchEngine(\Rbplm\Dao\DaoListAbstract $engine)
	{
		$this->searchEngine = $engine;
		return $this;
	}

	/**
	 * @return \Rbplm\Dao\DaoListAbstract $engine
	 */
	public function getSearchEngine()
	{
		return $this->searchEngine;
	}
}
