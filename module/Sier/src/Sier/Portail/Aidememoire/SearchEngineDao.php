<?php
namespace Sier\Portail\Aidememoire;

/** SQL_SCRIPTS>>
 << */

/** SQL_ALTER>>
 << */

/** SQL_VIEW>>
 << */


/**
 * Search in documents withs categories, doctype, container
 * 
 */
class SearchEngineDao extends \Rbs\Dao\Sier\DaoList
{
	
	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'number' => 'number',
		'name' => 'name',
		'label' => 'label',
		'life_stage' => 'lifeStage',
		'acode' => 'accessCode',
		'iteration' => 'iteration',
		'version' => 'version',
		'branch_id' => 'branchId',
		'container_id' => 'parentId',
		'container_uid' => 'parentUid',
		'doctype_id' => 'doctypeId',
		'category_id' => 'categoryId',
		'tags' => 'tags',
		'designation' => 'description',
		'from_document_id' => 'fromId',
		'lock_by_id' => 'lockById',
		'lock_by_uid' => 'lockByUid',
		'locked' => 'locked',
		'updated' => 'updated',
		'update_by_id' => 'updateById',
		'update_by_uid' => 'updateByUid',
		'created' => 'created',
		'create_by_id' => 'createById',
		'create_by_uid' => 'createByUid',
		'spacename' => 'spacename',
		'as_template' => 'asTemplate',
	);
	
	/**
	 *
	 * @see \Rbplm\Dao\DaoInterface::load()
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return SearchEngineDao
	 */
	public function load($filter = null, $bind = null)
	{
		if ( $this->isLoaded == true ) {
			$this->_reset();
		}
		
		$sql = "SELECT * FROM(
		SELECT
		%select%,
		'workitem' AS `spacename`,
		'569e92709feb6' AS `cid`
		FROM `workitem_documents` AS `doc`
		WHERE %filter%
		UNION
		SELECT
		%select%,
		'bookshop' AS `spacename`,
		'569e92709feb6' AS `cid`
		FROM `bookshop_documents` AS `doc`
		WHERE %filter%
		UNION
		SELECT
		%select%,
		'cadlib' AS `spacename`,
		'569e92709feb6' AS `cid`
		FROM `cadlib_documents` AS `doc`
		WHERE %filter%
		) AS documents";
		
		$sql = str_replace('%filter%', $filter->whereToString(), $sql);
		$sql = str_replace('%select%', $filter->selectToString(), $sql);
		$sql = $sql . $filter->orderToString();
		
		//echo '<pre>'.$sql;var_dump($bind);die;
		
		$this->stmt = $this->connexion->prepare($sql);
		$this->stmt->setFetchMode($this->options['mode']);
		$this->stmt->execute($bind);
		$this->isLoaded = true;
		$this->filter = $filter;
		return $this;
	}
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::save()
	 */
	public function save($list)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::delete()
	 */
	public function delete($filter)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::countAll()
	 */
	public function countAll($filter, $bind = null)
	{
		$sql = "SELECT COUNT(*) FROM(
		SELECT
		%select%
		FROM `workitem_documents` AS `doc`
		WHERE %filter%
		UNION
		SELECT
		%select%
		FROM `bookshop_documents` AS `doc`
		WHERE %filter%
		UNION
		SELECT
		%select%
		FROM `cadlib_documents` AS `doc`
		WHERE %filter%
		) AS documents";
		
		$sql = str_replace('%select%', 'doc.id', $sql);
		$sql = str_replace('%filter%', $filter->whereToString(), $sql);
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		$count = $stmt->fetch();
		return $count[0];
	}
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::areExisting()
	 */
	public function areExisting($propNameToTest, $list, $returnSelect = null)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}
} /* End of class */
