<?php
namespace Sier\Model;

use Rbplm\Any;
use Rbplm\Sys\Directory;
use Rbplm\Vault\ExistingDirectoryException;
use Rbplm\Rbplm;

/**
 *
 *
 */
class Filetree extends Any
{

	/**
	 * @var string
	 */
	static $classId = 'asierfiletree';

	/* @var string */
	protected $description;

	/* @var string */
	protected $path;

	/* @var integer */
	protected $containerId;

	/* @var \Docseeder\Model\Docseeder */
	public $docseeder;

	/* @var \Docseeder\Model\DeployHelper */
	public $deployHelper;

	/* @var array */
	public $templates;

	/**
	 *
	 * @param string $path
	 *        	Path to reposit
	 * @return \Rbplm\Vault\Reposit
	 */
	public static function init($path = null)
	{
		$class = get_called_class();
		/* @var Filetree $obj */
		$obj = new $class();
		if ( $path ) {
			$obj->setPath($path);
		}
		$obj->newUid();
		return $obj;
	}

	/**
	 */
	public function populate(array $properties)
	{
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['path'])) ? $this->path = $properties['path'] : null;
		(isset($properties['containerId'])) ? $this->containerId = (int)$properties['containerId'] : null;
		(isset($properties['templates'])) ? $this->templates = $properties['templates'] : null;
		return $this;
	}

	/**
	 * Return base path configured with replacement keys.
	 *
	 * @param string $string
	 * @param array $replace
	 * @return string
	 */
	public static function pathReplacements($string, $replace)
	{
		foreach( $replace as $needle => $value ) {
			$string = str_replace($needle, $value, $string);
		}
		$string = trim($string);
		return rtrim($string, '/');
	}

	/**
	 * Init the directory where store files
	 */
	public static function initPath($path)
	{
		if ( empty($path) ) {
			throw new \Exception(sprintf('BAD_PARAMETER_OR_EMPTY %s', 'path'));
		}

		/* Check if Reposit dir is existing */
		if ( !is_dir($path) ) {
			\Ranchbe::get()->log('Try to create Reposit directory ' . $path);
			Directory::create($path, 0775);
		}
		else {
			throw new ExistingDirectoryException(sprintf('Directory %s is exisiting', $path));
		}
		return true;
	}

	/**
	 *
	 * @param integer $id
	 * @return Filetree
	 */
	public function setContainerId($id)
	{
		$this->containerId = $id;
		return $this;
	}

	/**
	 * Getter
	 * @return integer
	 */
	public function getContainerId()
	{
		return $this->containerId;
	}

	/**
	 *
	 * @param string $url
	 * @return Filetree
	 */
	public function setPath($path)
	{
		$this->path = $path;
		return $this;
	}

	/**
	 * Getter
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 *
	 * @param string $desc
	 * @return Filetree
	 */
	public function setDescription($desc)
	{
		$this->description = $desc;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
}

