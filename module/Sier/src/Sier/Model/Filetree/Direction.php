<?php
namespace Sier\Model\Filetree;

use Sier\Model\Filetree;

/**
 *
 *
 */
class Direction extends Filetree
{

	/**
	 * @var string
	 */
	static $classId = 'asierdirection';

	/** @var string */
	protected $customer;

	/** @var string */
	protected $year;

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Sier\Model\Filetree::populate()
	 */
	public function populate(array $properties)
	{
		parent::populate($properties);
		(isset($properties['customer'])) ? $this->customer = $properties['customer'] : null;
		(isset($properties['year'])) ? $this->year = $properties['year'] : null;
		return $this;
	}

	/**
	 *
	 * @param string $desc
	 * @return Direction
	 */
	public function setCustomer($customer)
	{
		$this->customer = $customer;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

	/**
	 *
	 * @param string $year
	 * @return Direction
	 */
	public function setYear($year)
	{
		$this->year = $year;
		return $this;
	}

	/**
	 * 
	 * @return string
	 */
	public function getYear()
	{
		return $this->year;
	}
}
