<?php
//%LICENCE_HEADER%
namespace Sier\Model\Filetree;

use Rbs\Space\Factory as DaoFactory;
use Docseeder\Model\DeployHelper as DocseederDeployHelper;
use Sier\Model\Filetree as FiletreeModel;
use Docseeder\Model\Docseeder;
use Rbplm\Ged\Doctype as Type;

/**
 * Type of of document
 *
 */
class DeployHelper extends DocseederDeployHelper
{

	/** @var array 
	 * ~~~~~~~~~{.php}
	 * public $dq115Map = [
	 *	 'id' => [
	 *			 'label' => 'label01',
	 *			 'filename' => 'file01with%replacement%.ext',
	 *			 'type' => 'type_number',
	 *			 'target' => 'path/to/targetDir/with/%replacements%',
	 *			 'document'=>[
	 *				 'toSave'=>boolean,
	 *				 'number'=>string,
	 *				 'description'=>string,
	 *				 'label'=>string
	 *		 	 ]
	 *	 	 ]
	 *	 ];
	 *	 ~~~~~~~~~
	 **/
	public $dq115Map = [];

	/**
	 * @param array $config
	 * @return DeployHelper
	 */
	public function __construct(DaoFactory $factory)
	{
		parent::__construct($factory);
	}

	/**
	 * Load project, workitem, type in $docseeder
	 *
	 * @param Docseeder $docseeder
	 * @param string $number
	 * @return DeployHelper
	 */
	public function loadTypeFromNumber(Docseeder $docseeder, $number)
	{
		$workitemFactory = $this->targetFactory;

		/* load type */
		try {
			$type = new Type();
			$workitemFactory->getDao($type->cid)->loadFromNumber($type, $number);
			$docseeder->setType($type);
		}
		catch( \Exception $e ) {
			throw new \Exception(sprintf('Unable to load type from number %s > %s', $number, $e->getMessage()));
		}

		return $this;
	}

	/**
	 *
	 * @param array $dq115MapElemt
	 * @return string
	 */
	public function replacementApplyToFilename(Docseeder $docseeder, $dq115MapElemt)
	{
		$filename = $dq115MapElemt['filename'];

		/* CONTAINER */
		if ( strstr($filename, '%CONTAINER_NUMBER%') ) {
			$container = $docseeder->getContainer();
			$filename = str_replace('%CONTAINER_NUMBER%', $container->getNumber(), $filename);
		}
		if ( strstr($filename, '%CONTAINER_ID%') ) {
			$container = $docseeder->getContainer();
			$filename = str_replace('%CONTAINER_ID%', $container->getId(), $filename);
		}

		/* PROJECT */
		if ( strstr($filename, '%PROJECT_NUMBER%') ) {
			$project = $docseeder->getContainer()->getProject();
			$filename = str_replace('%PROJECT_NUMBER%', $project->getNumber(), $filename);
		}
		if ( strstr($filename, '%PROJECT_ID%') ) {
			$project = $docseeder->getContainer()->getProject();
			$filename = str_replace('%PROJECT_ID%', $project->getId(), $filename);
		}

		/* LABEL AND OTHERS */
		if ( strstr($filename, '%LABEL%') ) {
			$label = $dq115MapElemt['label'];
			$label = \Rbs\Number::normalize($label);
			$filename = str_replace('%LABEL%', $label, $filename);
		}

		/* AUTONUM */
		if ( strstr($filename, '%AUTONUM%') ) {
			/* load genertor and generate a new number */
			$number = $this->getAutonum($docseeder);
			$filename = str_replace('%AUTONUM%', $number->__toString(), $filename);
		}

		$filename = \Rbs\Number::noaccent($filename);
		return $filename;
	}

	/**
	 * @param FiletreeModel $reposit
	 * @param array $dq115MapElemt
	 * @return string
	 */
	public function replacementApplyToPath(Docseeder $docseeder, FiletreeModel $filetreeModel, $dq115MapElemt)
	{
		$path = $dq115MapElemt['target'];

		/* BASE DIRECTORY */
		if ( strstr($path, '%BASEDIR%') ) {
			$path = str_replace('%BASEDIR%', $filetreeModel->getPath(), $path);
		}
		else {
			throw new \Exception(sprintf('The hook %%BASEDIR%% must be define in taget key of the map for: %s', $dq115MapElemt['target']));
		}

		/* AFFAIRE */
		if ( strstr($path, '%CONTAINER_NUMBER%') ) {
			$container = $docseeder->getContainer();
			$path = str_replace('%CONTAINER_NUMBER%', $container->getName(), $path);
		}
		if ( strstr($path, '%CONTAINER_ID%') ) {
			$container = $docseeder->getContainer();
			$path = str_replace('%CONTAINER_ID%', $container->getId(), $path);
		}

		/* PROJECT */
		if ( strstr($path, '%PROJECT_NUMBER%') ) {
			$project = $docseeder->getContainer()->getProject();
			$path = str_replace('%PROJECT_NUMBER%', $project->getNumber(), $path);
		}
		if ( strstr($path, '%PROJECT_ID%') ) {
			$project = $docseeder->getContainer()->getProject();
			$path = str_replace('%PROJECT_ID%', $project->getId(), $path);
		}

		return $path;
	}

	/**
	 *
	 * @param array $dq115MapElemt
	 * @return string
	 */
	public function replacementApplyToNumber(Docseeder $docseeder, $dq115MapElemt)
	{
		$number = $dq115MapElemt['document']['number'];

		/* AFFAIRE */
		if ( strstr($number, '%CONTAINER_NUMBER%') ) {
			$container = $docseeder->getContainer();
			$number = str_replace('%CONTAINER_NUMBER%', $container->getName(), $number);
		}
		if ( strstr($number, '%CONTAINER_ID%') ) {
			$container = $docseeder->getContainer();
			$number = str_replace('%CONTAINER_ID%', $container->getId(), $number);
		}

		/* PROJECT */
		if ( strstr($number, '%PROJECT_NUMBER%') ) {
			$project = $docseeder->getContainer()->getProject();
			$number = str_replace('%PROJECT_NUMBER%', $project->getNumber(), $number);
		}
		if ( strstr($number, '%PROJECT_ID%') ) {
			$project = $docseeder->getContainer()->getProject();
			$number = str_replace('%PROJECT_ID%', $project->getId(), $number);
		}

		/* AUTONUM */
		if ( strstr($number, '%AUTONUM%') ) {
			/* load genertor and generate a new number */
			$autonum = $this->getAutonum($docseeder);
			$number = str_replace('%AUTONUM%', $autonum->__toString(), $number);
		}

		/* LABEL AND OTHERS */
		if ( strstr($number, '%LABEL%') ) {
			$label = $dq115MapElemt['label'];
			$label = \Rbs\Number::normalize($label);
			$number = str_replace('%LABEL%', $label, $number);
		}

		$number = strtoupper(\Rbs\Number::normalize($number));
		return $number;
	}

	/**
	 *
	 * @param array $dq115MapElemt
	 * @return DeployHelper
	 */
	public function buildDocument(Docseeder $docseeder, $dq115MapElemt)
	{
		if ( !isset($dq115MapElemt['document']) ) {
			throw new \Exception('document key is not set in the map element');
		}

		$docElemt = $dq115MapElemt['document'];
		$number = $this->replacementApplyToNumber($docseeder, $dq115MapElemt);
		$description = $docElemt['description'];
		$label = $docElemt['label'];

		$document = \Rbplm\Ged\Document\Version::init($number)->setNumber($number);
		$document->setParent($docseeder->getContainer());
		$document->setDoctype($docseeder->getType());
		$document->description = $description;
		$document->setLabel($label);
		$docseeder->setDocument($document);

		if ( $docElemt['toSave'] ) {
			$document->dao = $this->targetFactory->getDao($document->cid);
			$document->toSave = true;
		}

		return $this;
	}
} /* End of class*/
