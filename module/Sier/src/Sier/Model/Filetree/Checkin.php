//%LICENCE_HEADER% namespace Sier\Model\Filetree; use
Rbplm\People\CurrentUser; /** * Type of of document * */ class Checkin {

public function __construct(){ } /** * Before use as input : * -
Container must be associate to Document * - Dao must associate to
Docfile and Document * - A factory must associate to Dao * * @param
\Rbplm\Ged\Document\Version $document * @param
\Rbplm\Ged\Docfile\Version $docfile * @param string $file * @throws
\InvalidArgumentException * @throws \Exception */ public function
saveAndKeepCo($document, $docfile, $file){ try{ if(!($document->dao
instanceof \Rbs\Dao\Sier)){ throw new
\InvalidArgumentException('$document->dao is not set'); }
if(!($docfile->dao instanceof \Rbs\Dao\Sier)){ throw new
\InvalidArgumentException('$docfile->dao is not set'); } /* save the
document */ $document->lock(\Rbplm\Ged\AccessCode::CHECKOUT,
CurrentUser::get()); $document->dao->save($document); /* Init the
docfile */ $docfile->lock(\Rbplm\Ged\AccessCode::CHECKOUT,
CurrentUser::get()); $docfile->fsdata = new \Rbplm\Sys\Fsdata($file); /*
Associate to document and save */ $documentService = new
\Rbs\Ged\Document\Service($document->dao->factory);
$documentService->addFileToDocument($document, $docfile,
$docfile->fsdata); //docfile is save here } catch(\Throwable $e){ throw
new \Exception(sprintf('Unable to save document for this reason > %s',
$e->getMessage())); } } } /* End of class*/
