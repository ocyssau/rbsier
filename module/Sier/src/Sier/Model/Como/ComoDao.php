<?php
namespace Sier\Model\Como;

use Rbs\Dao\Sier as DaoSier;

/*

 CREATE ALGORITHM=UNDEFINED VIEW `view_como` AS
 SELECT
 `como`.`document_id` AS `como_id`,
 `como`.`document_number` AS `como_number`,
 `como`.`document_state` AS `como_status`,
 `como`.`document_access_code` AS `como_access_code`,
 `como`.`created` AS `como_created`,
 `como`.`como_reason` AS `como_reason`,
 `como`.`como_type` AS `como_type`,
 `applydocs`.`document_id` AS `applyto_id`,
 `applydocs`.`document_number` AS `applyto_number`,
 `applydocs`.`document_indice_id` AS `applyto_indice`,
 `applydocs`.`document_version` AS `applyto_version`,
 `applydocs`.`document_state` AS `applyto_status`,
 `linked_docs`.`document_number` AS `from_number`,
 `linked_docs`.`document_indice_id` AS `from_version`,
 `linked_docs`.`container_id`
 FROM (
 (`bookshop_documents` AS `como`
 LEFT JOIN `workitem_documents` AS `linked_docs` ON `linked_docs`.`document_id` = `como`.`como_apply_to`
 )
 JOIN `workitem_documents` AS `applydocs` ON `linked_docs`.`document_number` = `applydocs`.`document_number`
 );

 */

/**
 *
 *
 */
class ComoDao extends DaoSier
{

	/**
	 * Id of the bookshop where are stored the comos
	 *
	 * @var integer
	 */
	public static $bookshop_id = 13;

	public static $category_id = 13;

	public $viewName = 'view_como';

	/**
	 *
	 * @var string
	 */
	public static $table = 'view_como';

	public static $vtable = 'view_como';

	public static $sequenceName = '';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array();

	/**
	 * @param	\Rbplm\Ged\Document\Version	$document
	 * @param	boolean	$onlyCount
	 * @param	boolean	$rsetReturn
	 * @param	array	$select
	 * @param	string	$filter
	 * @return array
	 */
	function getComoApplyToDocument($document, $onlyCount = false, $rsetReturn = false, $select = array(), $filter = '')
	{
		$viewName = $this->viewName;
		/* @var \PDO $conn */
		$conn = $this->connexion;

		$select = implode(',', $select);
		if ( !$select ) {
			$select = 'como_id, como_number, como_status, como_access_code, como_created, applyto_id, applyto_number, applyto_indice, applyto_version, applyto_status, from_number, from_version';
		}

		if ( $onlyCount ) {
			$sql = 'SELECT COUNT(como_id) FROM ' . $viewName;
		}
		else {
			$sql = 'SELECT ' . $select . ' FROM ' . $viewName;
		}
		$sql .= ' WHERE applyto_number=\'' . $document->getNumber() . '\'';

		if ( $filter ) {
			$sql .= ' AND ' . $filter;
		}

		if ( $onlyCount ) {
			$stmt = $conn->query($sql);
		}
		else {
			$stmt = $conn->query($sql);
			if ( $rsetReturn ) {
				return $stmt;
			}
			return $stmt->fetchAll();
		}
	}
}