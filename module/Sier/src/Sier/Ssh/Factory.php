<?php
// %LICENCE_HEADER%
namespace Sier\Ssh;

/**
 * 
 * @author ocyssau
 *
 */
class Factory
{

	/**
	 *
	 * @var array
	 */
	protected static $config = false;

	/**
	 * Associative array where key is name of classe that require the connexion
	 *
	 * @var array Collection of Connexion
	 */
	protected static $registry = [];

	/**
	 * Name of last used connexion
	 * 
	 * @var Connexion
	 */
	protected static $lastConnexion = null;

	/**
	 * Get the config
	 *
	 * @return array
	 */
	public static function getConfig()
	{
		return self::$config;
	}

	/**
	 * Set the config
	 *
	 * array( 'connexion name'=> array(
	 * 'params'=>array(
	 * 'host'=>
	 * 'dbname'=>
	 * 'port'=>
	 * 'username'=>
	 * 'password'=>
	 *
	 * @param array $config
	 * @return void
	 */
	public static function setConfig(array $config)
	{
		self::$config = $config;
	}

	/**
	 *
	 * Add a connexion to registry.
	 *
	 * @param string $name
	 * @param Connexion $conn
	 */
	public static function add($name, $conn)
	{
		self::$registry[$name] = $conn;
	}

	/**
	 * Get connexion by his name. If name is not set, return the last used connexion.
	 *
	 * This method work as a singleton, factory and accessor.
	 * Only if connexion is not existing, she will be created and stored in a internal registry,
	 * else the previous created connexion is returned.
	 *
	 * @param string $name name of connexion to use
	 * @return Connexion
	 */
	public static function get($name = null)
	{
		if ( !$name ) {
			$name = self::$lastConnexion;
		}
		else {
			self::$lastConnexion = $name;
		}

		if ( !isset(self::$registry[$name]) ) {
			if ( !isset(self::$config[$name]) ) {
				throw new \Exception(sprintf('the config key %s is not set', $name), 7100);
			}
			$params = self::$config[$name];
			isset($params['port']) ? $port = $params['port'] : $port = 22;

			if ( !$params['host'] ) {
				throw new SshException('host config key is not set');
			}

			$conn = ssh2_connect($params['host'], $port);

			if ( is_null($params['pubkeyfile']) ) {
				ssh2_auth_password($conn, $params['username'], $params['password']);
			}
			else {
				ssh2_auth_pubkey_file($conn, $params['username'], $params['pubkeyfile'], $params['privkeyfile'], $params['passphrase']);
			}

			$connexion = new Connexion($conn);
			self::$registry[$name] = $connexion;
		}

		return self::$registry[$name];
	}
}
