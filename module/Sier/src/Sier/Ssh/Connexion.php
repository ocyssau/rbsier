<?php
// %LICENCE_HEADER%
namespace Sier\Ssh;

/**
 * 
 * @author ocyssau
 *
 */
class Connexion
{

	/**
	 * ssh2 resource
	 *
	 * @var resource
	 */
	protected $conn;

	/**
	 * ssh2 steam
	 */
	protected $stream;

	/**
	 * @var integer
	 */
	protected $originalTimeout;

	/**
	 * 
	 * @param resource $conn
	 */
	public function __construct($conn)
	{
		$this->conn = $conn;
		$this->originalTimeout = ini_get('default_socket_timeout');
	}

	/**
	 * 
	 */
	public function getFingerprint()
	{
		return ssh2_fingerprint($this->conn, SSH2_FINGERPRINT_SHA1);
	}

	/**
	 * Set timeout from ini_set
	 * 
	 * @param integer $sec
	 * @return Connexion
	 */
	public function setTimeOut($sec)
	{
		ini_set('default_socket_timeout', $sec);
		return $this;
	}

	/**
	 *
	 * @param integer $sec
	 * @return Connexion
	 */
	public function restoreTimeOut()
	{
		ini_set('default_socket_timeout', $this->originalTimeout);
		return $this;
	}

	/**
	 * 
	 * @param string $cmd
	 * @throws \Exception
	 * @return string
	 */
	public function exec($cmd)
	{
		$conn = $this->conn;
		$stream = ssh2_exec($conn, $cmd);
		stream_set_blocking($stream, false);
		$stdOutput = stream_get_contents($stream);

		$errorsStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
		$error = stream_get_contents($errorsStream);

		if ( $error ) {
			throw new \Exception($error, 7020);
		}

		$this->stream = $stream;

		return $stdOutput;
	}
}
