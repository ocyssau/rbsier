<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'portail'=>[
		'userfilterforms' => [
			\Sier\Portail\Aidememoire\FilterForm::class => 'For Aidememoire Only',
			\Sier\Portail\Doa\FilterForm::class => 'For Doa Only',
		],
		'templates' => [
			\Portail\Model\Component\Documentlist::class => [
				'sier/portail/aidememoire/index' => 'For Aidememoire Portail Only',
				'sier/portail/doa/index' => 'For Doa Portail Only',
			],
		],
		'di' => [
			\Sier\Portail\Aidememoire\FilterForm::class => [
				'searchEngine' => \Sier\Portail\Aidememoire\SearchEngineDao::class
			],
			\Sier\Portail\Doa\FilterForm::class => [
				'searchEngine' => \Sier\Portail\Doa\SearchEngineDao::class
			],
		]
	],
	'router' => array(
		'routes' => array(
			'custom' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/custom[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Sier\Controller\Index',
						'action' => 'index',
					),
				),
			),
			'sier-filetree' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/sier/filetree[/][:action[/][:domain]]',
					'defaults' => array(
						'controller' => 'Sier\Controller\Filetree\Index',
						'action' => 'index',
					),
				),
			),
			'sier-filetree-ingienerie' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/sier/filetree/ingenierie[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Sier\Controller\Filetree\Ingenierie',
						'action' => 'index',
					),
				),
			),
			'sier-filetree-stress' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/sier/filetree/stress[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Sier\Controller\Filetree\Stress',
						'action' => 'index',
					),
				),
			),
			'sier-filetree-direction' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/sier/filetree/direction[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Sier\Controller\Filetree\Direction',
						'action' => 'index',
					),
				),
			),
			'sier-filetree-wildspace' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/sier/filetree/wildspace[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Sier\Controller\Filetree\Wildspace',
						'action' => 'index',
					),
				),
			),
			'sier-airbusmanual' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/airbusmanual[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Sier\Controller\Airbusmanual\Index',
						'action' => 'index',
					),
				),
			),
			'sier-docsier' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/docsier[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Sier\Controller\Docsier\Index',
						'action' => 'index',
					),
				),
			),
			'sier-quality' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/sier/quality[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Sier\Controller\Docsier\Quality',
						'action' => 'index',
					),
				),
			),
			'sier-quality-cartographie' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/sier/quality/cartographie/:spacename/:id',
					'defaults' => array(
						'controller' => 'Sier\Controller\Docsier\Quality',
						'action' => 'map',
					),
				),
			),
			'sier-rt' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/rt[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Sier\Controller\Rt\Index',
						'action' => 'index',
					),
				),
			),
		), //routes
	), //router
	'console' => array(
		'router'=>array(
			'routes'=>array(
				'sier-renamer'=>array(
					'type'=>'simple',
					'options'=>array(
						'route'=>'sier renamer <spacename> <directory>',
						'defaults' => array(
							'controller'=>'Sier\Controller\Cli',
							'action'=>'renamefile'
						)
					)
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Sier\Controller\Index' => 'Sier\Controller\IndexController',
			'Sier\Controller\Airbusmanual\Index' => 'Sier\Controller\Airbusmanual\IndexController',
			'Sier\Controller\Docsier\Index' => 'Sier\Controller\Docsier\IndexController',
			'Sier\Controller\Docsier\Quality' => 'Sier\Controller\Docsier\QualityController',
			'Sier\Controller\Rt\Index' => 'Sier\Controller\Rt\IndexController',
			'Sier\Controller\Filetree\Index'=>'Sier\Controller\Filetree\IndexController',
			'Sier\Controller\Filetree\Ingenierie'=>'Sier\Controller\Filetree\IngenierieController',
			'Sier\Controller\Filetree\Stress'=>'Sier\Controller\Filetree\StressController',
			'Sier\Controller\Filetree\Direction'=>'Sier\Controller\Filetree\DirectionController',
			'Sier\Controller\Filetree\Wildspace'=>'Sier\Controller\Filetree\WildspaceController',
			'Sier\Controller\Cli'=>'Sier\Controller\CliController',
		),
	), //controller
	'view_manager' => [
		'template_path_stack' => [
			__DIR__ . '/../view',
		],
		'template_map' => [
			'module_layouts' => [
				'Sier' => 'sier/layout/layout'
			],
			'sier/layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
		]
	], //view_manager
	'menu' => [
		'myspace' => [
			'custom' => [
				'name'=>'custom',
				'route'=>['params'=>['action' => 'index'], 'options'=>['name' => 'custom']],
				'class' => '%anchor%',
				'label' => 'Custom'
			]
		],
	],
	'docseeder-batch-maps' => [
		'doa-change' => __DIR__ . '/dq115map.doa-change.php',
		'doa-repair' => __DIR__ . '/dq115map.doa-repair.php',
	],
	'config_editor' => [
		'editables' => [
			'files' => [
				'Docseeder-DoaMap'=>__DIR__ . '/dq115map.doa-all.php',
				'Dq115MapDirection'=>__DIR__ . '/dq115map.direction.php',
				'Dq115MapIngenierie'=>__DIR__ . '/dq115map.ingenierie.php',
				'Dq115MapStress'=>__DIR__ . '/dq115map.stress.php'
			],
		],
	],
	'sier_filetree' => array(
		'ingenierie' => array(
			'local' => '/PATH/TO/DIRECTORY',
			'fileserver' => 'servername',
			'localfileserver' => 'Z:\\PATH\TO\DIRECTORY\FROM\CLIENTMACHINE',
			'clientaccess' => '\\\\servername\TO\DIRECTORY\FROM\CLIENTMACHINE',
			'template' => '/PATH/TO/template.zip',
			'security.rule' => array(
				#0 => '/grant:r "GROUPE 1":(OI)(CI)F /c /q',
				#1 => '/grant:r "GROUPE 2":(OI)(CI)F /c /q'
			),
			'dq115Map' => 'module/Sier/config/dq115map.ingenierie.php'
		),
		'stress' => array(
			'local' => '/PATH/TO/DIRECTORY',
			'fileserver' => 'servername',
			'localfileserver' => 'Z:\\PATH\TO\DIRECTORY\FROM\CLIENTMACHINE',
			'clientaccess' => '\\\\servername\TO\DIRECTORY\FROM\CLIENTMACHINE',
			'template' => '/PATH/TO/template.zip',
			'security.rule' => array(
				#0 => '/grant:r "GROUPE 1":(OI)(CI)F /c /q',
				#1 => '/grant:r "GROUPE 2":(OI)(CI)F /c /q'
			),
			'dq115Map' => 'module/Sier/config/dq115map.stress.php'
		),
		'direction' => array(
			'local' => '/PATH/TO/DIRECTORY',
			'fileserver' => 'servername',
			'localfileserver' => 'Z:\\PATH\TO\DIRECTORY\FROM\CLIENTMACHINE',
			'clientaccess' => '\\\\servername\TO\DIRECTORY\FROM\CLIENTMACHINE',
			'template' => '/PATH/TO/template.zip',
			'security.rule' => array(
				#0 => '/grant:r "GROUPE 1":(OI)(CI)F /c /q',
				#1 => '/grant:r "GROUPE 2":(OI)(CI)F /c /q'
			),
			'dq115Map' => 'module/Sier/config/dq115map.direction.php'
		)
	)
);
