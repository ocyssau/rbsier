DROP VIEW `view_docseeder_log`;
CREATE VIEW `view_docseeder_log` AS 
SELECT 
	`wi`.`number` AS `Workitem`,
	`doc`.`designation` AS `Subjet`,
	`doc`.`label` AS `Label`,
	CONCAT_WS('-',`dt`.`name`,`dt`.`designation`) AS `Type`,
	`doc`.`number` AS `reference`,
    CONCAT(CONCAT_WS('-', `doc`.`number`, REPLACE(`doc`.`label`, " ", "_")), ".ext") AS `File Name`,
	`doc`.`version` AS `Version`,
	`doc`.`create_by_uid` AS `Redactor`,
	`doc`.`created` AS `Creation Date`,
	`doc`.`id` AS `Id`
FROM (
	(`workitem_documents` `doc` JOIN `doctypes` `dt` ON((`doc`.`doctype_id` = `dt`.`id`))) JOIN `workitems` `wi` ON((`doc`.`container_id` = `wi`.`id`))) WHERE ((`dt`.`number` LIKE 'sier%') OR (`dt`.`number` LIKE '%dq%')
);

