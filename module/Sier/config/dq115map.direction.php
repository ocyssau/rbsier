<?php
return [
	'DQ104' => [
		'disabled' => true,
		'selected' => true,
		'label' => 'DQ104 Cahier des charges fournisseurs, Fr',
		'filename' => 'DQ104-CdC-fournisseurs_%CONTAINER_NUMBER%',
		'type' => 'dq104-fr',
		'target' => '%BASEDIR%/ACHATS/DEMANDE-ACHATS',
		'document' => [
			'toSave' => false,
			'number' => 'DQ104-CdC-fournisseurs_%CONTAINER_NUMBER%',
			'description' => 'Cahier des charges fournisseur',
			'label' => 'CDCF'
		]
	],
	'DQ101' => [
		'disabled' => false,
		'selected' => true,
		'label' => 'DQ101 Demande achats',
		'filename' => 'DQ101-Demande-achats_%CONTAINER_NUMBER%',
		'type' => 'dq101',
		'target' => '%BASEDIR%/ACHATS/DEMANDE-ACHATS',
		'document' => [
			'toSave' => false,
			'number' => 'DQ101-Demande-achats_%CONTAINER_NUMBER%',
			'description' => 'Demande achats',
			'label' => 'DemandeAchats'
		]
	],
	'DQ08' => [
		'disabled' => true,
		'selected' => true,
		'label' => 'DQ08 Chiffrage dossier client',
		'filename' => 'DQ08-Chiffrage_%CONTAINER_NUMBER%',
		'type' => 'dq08',
		'target' => '%BASEDIR%/COMMERCIAL/DQ08',
		'document' => [
			'toSave' => false,
			'number' => 'DQ08-Chiffrage_%CONTAINER_NUMBER%',
			'description' => 'Chiffrage dossier client',
			'label' => 'Chiffrage'
		]
	],
	'DQ120' => [
		'disabled' => true,
		'selected' => true,
		'label' => 'DQ120 Matrice préconisation achats',
		'filename' => 'DQ120-Matrice-preco-achats_%CONTAINER_NUMBER%',
		'type' => 'dq120',
		'target' => '%BASEDIR%/ACHATS/DEVIS',
		'document' => [
			'toSave' => false,
			'number' => 'DQ120-Matrice-preco-achats_%CONTAINER_NUMBER%',
			'description' => 'Matrice préconisation achats',
			'label' => 'MAT-PREC-ACHATS'
		]
	],
	'DQ74' => [
		'disabled' => true,
		'selected' => true,
		'label' => 'DQ74 Réponse à appel d\'offre',
		'filename' => 'DQ74-Reponse-appel-offre_%CONTAINER_NUMBER%',
		'type' => 'dq74',
		'target' => '%BASEDIR%',
		'document' => [
			'toSave' => false,
			'number' => 'DQ74-Reponse-appel-offre_%CONTAINER_NUMBER%',
			'description' => 'DQ74 Réponse à appel d\'offre',
			'label' => 'REP-APPEL-OFFRE'
		]
	],
	'DQ115' => [
		'disabled' => true,
		'selected' => true,
		'label' => 'DQ115 Check list documents gestion d\'affaire',
		'filename' => 'DQ115-Check-list-docs-gestion-affaire_%CONTAINER_NUMBER%',
		'type' => 'dq115',
		'target' => '%BASEDIR%',
		'document' => [
			'toSave' => false,
			'number' => 'DQ115-Check-list-docs-gestion-affaire_%CONTAINER_NUMBER%',
			'description' => 'Check list documents gestion d\'affaire',
			'label' => 'REP-APPEL-OFFRE'
		]
	],
	'DQ129' => [
		'disabled' => true,
		'selected' => true,
		'label' => 'DQ129 Fiche LET',
		'filename' => 'DQ129-Fiche_LET_%CONTAINER_NUMBER%',
		'type' => 'dq129',
		'target' => '%BASEDIR%',
		'document' => [
			'toSave' => false,
			'number' => 'DQ129-Fiche_LET_%CONTAINER_NUMBER%',
			'description' => 'Fiche de liste des écarts techniques - Communication avec clients / fournisseurs - Demandes d\'avenants',
			'label' => 'FICHE-LET'
		]
	]
];
