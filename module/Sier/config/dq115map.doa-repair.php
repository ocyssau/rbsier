<?php
$map = include('dq115map.doa-all.php');

$map['DO-DQ-300']['disabled'] = false;
$map['DO-DQ-300']['selected'] = true;

$map['DO-DQ-360']['disabled'] = false;
$map['DO-DQ-360']['selected'] = true;

$map['DO-DQ-400']['disabled'] = false;
$map['DO-DQ-400']['selected'] = true;

$map['DO-DQ-460']['disabled'] = false;
$map['DO-DQ-460']['selected'] = true;

$map['DO-DQ-480']['disabled'] = false;
$map['DO-DQ-480']['selected'] = true;

return $map;
