<?php
return [
	'DQ103' => [
		'disabled' => true,
		'selected' => true,
		'label' => 'DQ103 Cahier de recette',
		'filename' => 'DQ103-Cahier-recette_%CONTAINER_NUMBER%',
		'type' => 'dq103',
		'target' => '%BASEDIR%/PROJET/FAB',
		'document'=>[
			'toSave'=>false,
			'number'=>'DQ103-Cahier-recette_%CONTAINER_NUMBER%',
			'description'=>'Cahier de recette',
			'label'=>'CahierRecette'
		]
	],
	'DQ117' => [
		'disabled' => false,
		'selected' => false,
		'label' => 'DQ117 Notice de maintenance',
		'filename' => 'DQ117-Notice-maintenance_%CONTAINER_NUMBER%',
		'type' => 'dq117',
		'target' => '%BASEDIR%/PROJET/FAB',
		'document'=>[
			'toSave'=>false,
			'number'=>'DQ117-Notice-maintenance_%CONTAINER_NUMBER%',
			'description'=>'Notice de maintenance',
			'label'=>'NoticeMaintenance'
		]
	],
	'DQ102' => [
		'disabled' => false,
		'selected' => true,
		'label' => 'DQ102 Matrice de conformité',
		'filename' => 'DQ102-Matrice-conformite_%CONTAINER_NUMBER%',
		'type' => 'dq102',
		'target' => '%BASEDIR%/PROJET',
		'document'=>[
			'toSave'=>false,
			'number'=>'DQ102-Matrice-conformite_%CONTAINER_NUMBER%',
			'description'=>'Matrice de conformité',
			'label'=>'MatConf'
		]
	],
	'DQ95' => [
		'disabled' => false,
		'selected' => false,
		'label' => 'DQ95 Rapport de contrôle dimensionnel',
		'filename' => 'DQ95-Rapport-controle-dim_%CONTAINER_NUMBER%',
		'type' => 'dq95',
		'target' => '%BASEDIR%/PROJET/FAB',
		'document'=>[
			'toSave'=>false,
			'number'=>'DQ95-Rapport-controle-dim_%CONTAINER_NUMBER%',
			'description'=>'Rapport de contrôle dimensionnel',
			'label'=>'ControleDimensionel'
		]
	],
	'DQ89' => [
		'disabled' => false,
		'selected' => false,
		'label' => 'DQ89 Certificat conformité CE',
		'filename' => 'DQ89-Certif-conformite-machine-CE_%CONTAINER_NUMBER%',
		'type' => 'dq89',
		'target' => '%BASEDIR%/DONNEES CLIENT/Donnees de sortie/Livraisons',
		'document'=>[
			'toSave'=>false,
			'number'=>'DQ89-Certif-conformite-machine-CE_%CONTAINER_NUMBER%',
			'description'=>'Certificat conformité CE',
			'label'=>'CertifCe'
		]
	],
	'DQ112' => [
		'disabled' => true,
		'selected' => true,
		'label' => 'DQ112 Certificat de conformité aux stipulations d\'une commande',
		'filename' => 'DQ112-Certif-conformite-commande_%CONTAINER_NUMBER%',
		'type' => 'dq112',
		'target' => '%BASEDIR%/DONNEES CLIENT/Donnees de sortie/Livraisons',
		'document'=>[
			'toSave'=>false,
			'number'=>'DQ112-Certif-conformite-commande_%CONTAINER_NUMBER%',
			'description'=>'Certificat de conformité aux stipulations d\'une commande',
			'label'=>'ConfCmd'
		]
	],
	'DQ111' => [
		'disabled' => false,
		'selected' => false,
		'label' => 'DQ111 Déclaration d\'incorporation quasi-machine',
		'filename' => 'DQ111-Declaration-incorporation-quasi-machine_%CONTAINER_NUMBER%',
		'type' => 'dq111',
		'target' => '%BASEDIR%/DONNEES CLIENT/Donnees de sortie/Livraisons',
		'document'=>[
			'toSave'=>false,
			'number'=>'DQ111-Declaration-incorporation-quasi-machine_%CONTAINER_NUMBER%',
			'description'=>'Déclaration d\'incorporation quasi-machine',
			'label'=>'IncQMa'
		]
	],
	'DQ116' => [
		'disabled' => false,
		'selected' => false,
		'label' => 'DQ116 Check list directive machine',
		'filename' => 'DQ116-Check-list-directive-machine_%CONTAINER_NUMBER%',
		'type' => 'dq116',
		'target' => '%BASEDIR%/PROJET/BE',
		'document'=>[
			'toSave'=>false,
			'number'=>'DQ116-Check-list-directive-machine_%CONTAINER_NUMBER%',
			'description'=>'Check list directive machine',
			'label'=>'ChecklistDirectiveMachine'
		]
	],
	'DQ81' => [
		'disabled' => false,
		'selected' => true,
		'label' => 'DQ81 Suivi des approvisionnements',
		'filename' => 'DQ81-Suivi-appro_%CONTAINER_NUMBER%',
		'type' => 'dq81',
		'target' => '%BASEDIR%/PROJET/FAB',
		'document'=>[
			'toSave'=>false,
			'number'=>'DQ81-Suivi-appro_%CONTAINER_NUMBER%',
			'description'=>'Suivi des approvisionnements',
			'label'=>'SuiviAppro'
		]
	],
	'PR01-6' => [
		'disabled' => true,
		'selected' => true,
		'label' => 'PR01-6 Annexe 2, points de controle étude liasses',
		'filename' => 'PR01-6-Annexe2-points-de-controle-etude-liasses_%CONTAINER_NUMBER%',
		'type' => 'dq87',
		'target' => '%BASEDIR%/PROJET/BE',
		'document'=>[
			'toSave'=>false,
			'number'=>'PR01-6-Annexe2-points-de-controle-etude-liasses_%CONTAINER_NUMBER%',
			'description'=>'Annexe 2, points de controle étude liasses',
			'label'=>'ControleLiasse'
		]
	],
	'SIER-NOTICE-UTILE-1' => [
		'disabled' => false,
		'selected' => false,
		'label' => 'Notice d\'utilisation',
		'filename' => 'Notice-Utilisation_%CONTAINER_NUMBER%',
		'type' => 'sier-nutil',
		'target' => '%BASEDIR%/PROJET',
		'document'=>[
			'toSave'=>false,
			'number'=>'Notice-Utilisation_%CONTAINER_NUMBER%',
			'description'=>'Notice d\'utilisation',
			'label'=>'NoticeUtilisation'
		]
	],
	'SIER-NOTICE-UTILE-AIF' => [
		'disabled' => false,
		'selected' => false,
		'label' => 'Notice d\'utilisation',
		'filename' => 'Notice-Utilisation_Airbus_%CONTAINER_NUMBER%',
		'type' => 'sier-nutil-airbus',
		'target' => '%BASEDIR%/PROJET',
		'document'=>[
			'toSave'=>false,
			'number'=>'Notice-Utilisation_Airbus_%CONTAINER_NUMBER%',
			'description'=>'Notice d\'utilisation',
			'label'=>'NoticeUtilisationAirbus'
		]
	]
];
