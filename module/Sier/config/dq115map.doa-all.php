<?php
return [
	'DO-DQ-300' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-300',
		'label' => 'Technical Justification Note',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Technical Justification Note',
			'label'=>''
		]
	],
	'DO-DQ-301' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-301',
		'label' => 'Technical Justification Note EZAP',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Technical Justification Note EZAP',
			'label'=>''
		]
	],
	'DO-DQ-302' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-302',
		'label' => 'Technical Justification Note MMEL',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Technical Justification Note MMEL',
			'label'=>''
		]
	],
	'DO-DQ-310' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-310',
		'label' => 'Design Project Plan',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Design Project Plan',
			'label'=>''
		]
	],
	'DO-DQ-320' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-320',
		'label' => 'Engineering change control form',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Engineering change control form',
			'label'=>''
		]
	],
	'DO-DQ-330' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-330',
		'label' => 'Bill of material',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Bill of material',
			'label'=>''
		]
	],
	'DO-DQ-340' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-340',
		'label' => 'Design dossier',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Design dossier',
			'label'=>''
		]
	],
	'DO-DQ-350' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-350',
		'label' => 'Statement of approval of design data',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Statement of approval of design data',
			'label'=>''
		]
	],
	'DO-DQ-360' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-360',
		'label' => 'Change repair classification',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Change repair classification',
			'label'=>''
		]
	],
	'DO-DQ-370' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-370',
		'label' => 'Certification dossier',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Certification dossier',
			'label'=>''
		]
	],
	'DO-DQ-380' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-380',
		'label' => 'Service bulletin',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Service bulletin',
			'label'=>''
		]
	],
	'DO-DQ-390' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-390',
		'label' => 'Change approval sheet',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Change approval sheet',
			'label'=>''
		]
	],
	'DO-DQ-400' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-400',
		'label' => 'Repair approval sheet',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Repair approval sheet',
			'label'=>''
		]
	],
	'DO-DQ-420' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-420',
		'label' => 'Document circulation notice',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Document circulation notice',
			'label'=>''
		]
	],
	'DO-DQ-460' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-460',
		'label' => 'Repair design dossier',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Repair design dossier',
			'label'=>''
		]
	],
	'DO-DQ-480' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-480',
		'label' => 'Repair instructions',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Repair instructions',
			'label'=>''
		]
	],
	'DO-DQ-490' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-490',
		'label' => 'Test dossier',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Test dossier',
			'label'=>''
		]
	],
	'DO-DQ-520' => [
		'disabled' => false,
		'selected' => false,
		'type' => 'DO-DQ-520',
		'label' => 'Manual Supplement Cover Page',
		'target' => '%WILDSPACE%',
		'document'=>[
			'toSave'=>true,
			'description'=>'Manual Supplement Cover Page',
			'label'=>''
		]
	],
];
