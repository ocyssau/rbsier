<?php
$map = include('dq115map.doa-all.php');

$map['DO-DQ-300']['disabled'] = false;
$map['DO-DQ-300']['selected'] = true;

$map['DO-DQ-380']['disabled'] = false;
$map['DO-DQ-380']['selected'] = true;

$map['DO-DQ-310']['disabled'] = false;
$map['DO-DQ-310']['selected'] = true;

$map['DO-DQ-330']['disabled'] = false;
$map['DO-DQ-330']['selected'] = true;

$map['DO-DQ-340']['disabled'] = false;
$map['DO-DQ-340']['selected'] = true;

$map['DO-DQ-350']['disabled'] = false;
$map['DO-DQ-350']['selected'] = true;

$map['DO-DQ-360']['disabled'] = false;
$map['DO-DQ-360']['selected'] = true;

$map['DO-DQ-370']['disabled'] = false;
$map['DO-DQ-370']['selected'] = true;

$map['DO-DQ-390']['disabled'] = false;
$map['DO-DQ-390']['selected'] = true;

$map['DO-DQ-490']['disabled'] = false;
$map['DO-DQ-490']['selected'] = true;

$map['DO-DQ-520']['disabled'] = false;
$map['DO-DQ-520']['selected'] = true;

return $map;