<?php
namespace Sier;

class Module
{

	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	/**
	 */
	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'ssh' => function ($sm) {
					/** @var ServiceManager $sm */
					$config = $sm->get('Configuration');
					$factory = new Ssh\Factory();
					$factory::setConfig($config['rbp']['sier']['ssh']);
					return $factory;
				}
			),
			'aliases' => array(),
			'abstract_factories' => array(),
			'invokables' => array(),
			'services' => array(),
			'shared' => array()
		);
	}

	/**
	 * 
	 * @return string[][][]
	 */
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
				)
			)
		);
	}
}
