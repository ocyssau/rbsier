<?php
namespace Service\Batch;

/**
 * 
 */
class StoreBatch extends \Rbs\Batch\Batch
{

	/**
	 *
	 * @param array $properties
	 */
	public function __construct($callbackClass = null, $callbackMethod = null, $callbackParams = null)
	{
		parent::__construct();
		$this->callbackClass = $callbackClass;
		$this->callbackMethod = $callbackMethod;
		$this->callbackParams = $callbackParams;
	}
}
