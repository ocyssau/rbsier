<?php
namespace Service\Controller;

class ServiceFeedback extends \Exception
{

	/**
	 * Data is object concerned by exception
	 *
	 * @var \stdClass
	 */
	protected $data;

	/**
	 * @param string $message
	 */
	function __construct($message, $data = null)
	{
		parent::__construct($message, E_USER_NOTICE);
		$this->data = $data;
	}

	/**
	 *
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * implements JsonSerializable
	 */
	public function jsonSerialize()
	{
		return [
			'message' => $this->getMessage(),
			'data' => json_encode($this->data)
		];
	}
}
