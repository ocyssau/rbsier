<?php
namespace Service\Controller\Application;

use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Service\Controller\ServiceController;

/**
 * Class to get files from ranchbe objects
 */
class ViewerService extends ServiceController
{

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Get the files associated to Document\Version or Docfile\Version
	 * $_GET['id'] may be id of document or docfile
	 *
	 * HTTP:GET method
	 *
	 * @param $_GET['id'] int        	
	 * @param $_GET['cid'] string
	 *        	- 569e92709feb6 if get file from Document\Versoin,
	 *        	- 569e92b86d248 to get file from Docfile\Version
	 * @param $_GET['spacename'] string
	 *        	Spacename of the object to get. Permited values are
	 *        	- 'workitem'
	 *        	- 'bookshop'
	 *        	- 'cadlib'
	 *        	- 'mockup'
	 *        	
	 * @return string base64 encoded datas
	 */
	public function downloadService()
	{
		$request = $this->getRequest();
		$cid = $request->getQuery('cid', null);

		switch ($cid) {
			case Document\Version::$classId:
				$controller = new \Service\Controller\Document\ViewerService();
				$controller->downloadService();
				break;
			case Docfile\Version::$classId:
				$controller = new \Service\Controller\Docfile\ViewerService();
				$controller->downloadService();
				break;
			default:
				break;
		}
	}
}