<?php
namespace Service\Controller\Application;

use Rbplm\People;
use Rbplm\Sys\Datatype;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;
use Service\Controller\ServiceFeedback;
use Zend\Http\Request as HttpRequest;

/**
 * Abstract file manager service.
 *
 * Define some commons methods availables for all files managers
 *
 */
abstract class AbstractFilemanager extends ServiceController
{

	/**
	 *
	 * @param People\User $user
	 * @return \Rbplm\Sys\Directory
	 */
	abstract protected function getDirectory(People\User $user);

	/**
	 * Rename a file from the wildspace.
	 *
	 *
	 * HTTP GET method
	 *
	 * @param HttpRequest $request
	 * ~~~~~~~~~{.json}
	 * files:[
	 * 		'file1':{
	 * 		'name':string,
	 * 		'newname':string,
	 * 		},
	 * 		file2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @return ServiceRespons
	 *
	 */
	public function renameService(HttpRequest $request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( $request->isGet() == false ) {
			throw new ServiceException(__FUNCTION__ . ' accept only GET HTTP Method');
		}

		$files = $request->getQuery('files', []);
		if ( !($files) ) {
			throw new ServiceException('files is not set or is not a array', 5030, $this);
		}

		$user = People\CurrentUser::get();
		$directory = $this->getDirectory($user);

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'file1'; isset($files[$index]); $index = 'file' . ++$i) {
			/* file1 is the current entry in input data */
			$file1 = $files[$index];
			/* set vars */
			$fileName = basename($file1['name']);
			$newName = basename($file1['newname']);

			if ( !$newName || !$fileName ) {
				throw new ServiceException(sprintf(tra('file%s name or newname is not setted'), $index));
			}

			try {
				$path = $directory->getPath();
				$file = new Datatype\File($path . '/' . $fileName);

				$file->rename($path . '/' . $newName);
				$feedback = new ServiceFeedback(sprintf(tra('%s is rename to %s'), $fileName, $newName), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, null);
				$return->addError($index, $error);
				continue;
			}
		}
		return $return;
	}

	/**
	 * Copy a file from the wildspace to the wildspace.
	 *
	 *
	 * HTTP GET method
	 *
	 * @param HttpRequest $request
	 * ~~~~~~~~~{.json}
	 * files:[
	 * 		'file1':{
	 * 		'name':string,
	 * 		'newname':string,
	 * 		},
	 * 		file2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @return ServiceRespons
	 */
	public function copyService(HttpRequest $request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( $request->isGet() == false ) {
			throw new ServiceException(__FUNCTION__ . ' accept only GET HTTP Method');
		}

		$files = $request->getQuery('files', []);

		if ( !($files) ) {
			throw new ServiceException('files is not set or is not a array', 5030, $this);
		}

		$user = People\CurrentUser::get();
		$directory = $this->getDirectory($user);

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'file1'; isset($files[$index]); $index = 'file' . ++$i) {
			/* file1 is the current entry in input data */
			$file1 = $files[$index];
			/* set vars */
			$fileName = basename($file1['name']);
			$newName = basename($file1['newname']);

			if ( !$newName || !$fileName ) {
				throw new ServiceException(sprintf(tra('file%s name or newname is not setted'), $index));
			}

			try {
				$path = $directory->getPath();
				$file = new Datatype\File($path . '/' . $fileName);
				$file->copy($path . '/' . $newName, 0666, false);

				$feedback = new ServiceFeedback(sprintf(tra('%s is copy to %s'), $fileName, $newName), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, null);
				$return->addError($index, $error);
				continue;
			}
		}
		return $return;
	}

	/**
	 * Delete a file from the wildspace.
	 * 
	 * HTTP GET method
	 *
	 * @param HttpRequest $request
	 * ~~~~~~~~~{.json}
	 * files:[
	 * 		'file1':{
	 * 		'name':string,
	 * 		},
	 * 		file2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @return ServiceRespons
	 *
	 */
	public function deleteService(HttpRequest $request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( $request->isGet() == false ) {
			throw new ServiceException(__FUNCTION__ . ' accept only GET HTTP Method');
		}

		$files = $request->getQuery('files', []);

		if ( !($files) ) {
			throw new ServiceException('files is not set or is not a array', 5030, $this);
		}

		$user = People\CurrentUser::get();
		$directory = $this->getDirectory($user);

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'file1'; isset($files[$index]); $index = 'file' . ++$i) {
			/* file1 is the current entry in input data */
			$file1 = $files[$index];
			/* set vars */
			$fileName = basename($file1['name']);

			if ( !$fileName ) {
				throw new ServiceException(sprintf(tra('file %s name is not setted'), $index));
			}

			try {
				$directory->suppressData($fileName);
				$feedback = new ServiceFeedback(sprintf(tra('%s is put in trash'), $fileName), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, null);
				$return->addError($index, $error);
				continue;
			}
		}
		return $return;
	}

	/**
	 * Add files to the wildspace
	 * HTTP POST method
	 *
	 * @param HttpRequest $request
	 * ~~~~~~~~~
	 * 		'name':string as file name in wildspace,
	 * 		'id':int as docfile id to compare,
	 * ~~~~~~~~~
	 *
	 * @return ServiceRespons
	 */
	public function uploadService(HttpRequest $request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$user = People\CurrentUser::get();
		$directory = $this->getDirectory($user);
		$path = $directory->getPath();

		if ( $request->isPost() ) {
			if ( !$request->getFiles() ) {
				throw new ServiceException('None found files', 5050, $this);
			}
			$uploadInfo = $request->getFiles('file');
			$replace = (bool)$request->getPost('replace', true);
		}
		else {
			throw new ServiceException('Bad request. This must be a POST octet stream', 5034, $this);
		}

		try {
			$tmpfile = $uploadInfo['tmp_name'];
			$fileName = $uploadInfo['name'];
			$index = $fileName;
			$toPath = $path . '/' . $fileName;

			if ( is_file($toPath) ) {
				if ( !$replace ) {
					throw new ServiceException(sprintf('file %s is existing', $toPath), 5037, $this);
				}
				else {
					$feedback = new ServiceFeedback(sprintf(tra('file %s is existing and will be replaced'), $fileName), null);
					$return->addFeedback($index, $feedback);
				}
			}

			$ret = move_uploaded_file($tmpfile, $toPath);
			if ( !$ret ) {
				throw new ServiceException('move_uploaded_file failed', 5035, $this);
			}

			$feedback = new ServiceFeedback(sprintf(tra('%s is put in wilspace'), $fileName), null);
			$return->addFeedback($index, $feedback);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError($index, $error);
		}

		return $return;
	}

	/**
	 * Add files to the wildspace
	 * HTTP POST method
	 *
	 * @param HttpRequest $request
	 * ~~~~~~~~~{.json}
	 * files:[
	 * 		'file1':{
	 * 			'name':string,
	 * 			'md5':string,
	 * 			'size':string,
	 * 			'data':string BASE64_ENCODED,
	 * 		},
	 * 		file2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @return ServiceRespons
	 */
	protected function _uploadFromData(HttpRequest $request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( $request->isPost() == false ) {
			throw new ServiceException(__FUNCTION__ . ' accept only POST HTTP Method');
		}

		$files = $request->getPost('files', []);

		if ( !($files) ) {
			throw new ServiceException('files is not set or is not a array', 5030, $this);
		}

		$user = People\CurrentUser::get();
		$directory = $this->getDirectory($user);
		$path = $directory->getPath();

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'file1'; isset($files[$index]); $index = 'file' . ++$i) {
			/* file1 is the current entry in input data */
			$file1 = $files[$index];
			/* set vars */
			$fileName = basename($file1['name']);
			$data = $file1['data']['data'];
			//$size = $file1['data']['size'];
			//$md5 = $file1['data']['md5'];

			if ( !$fileName ) {
				throw new ServiceException(sprintf(tra('file%s name is not setted'), $index));
			}

			try {
				/* Put data in Ws */
				$data = base64_decode($data);
				$toPath = $path . '/' . $fileName;
				file_put_contents($toPath, $data);

				$feedback = new ServiceFeedback(sprintf(tra('%s is put in wilspace'), $fileName), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, null);
				$return->addError($index, $error);
				continue;
			}
		}
		return $return;
	}

	/**
	 * Uncompress files
	 *
	 * @param HttpRequest $request
	 * ~~~~~~~~~{.json}
	 * files:[
	 * 		'file1':{
	 * 			'name':string,
	 * 		},
	 * 		file2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @return ServiceRespons
	 */
	public function uncompressService(HttpRequest $request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( $request->isGet() == false ) {
			throw new ServiceException(__FUNCTION__ . ' accept only GET HTTP Method');
		}

		$files = $request->getQuery('files', []);

		if ( !($files) ) {
			throw new ServiceException('files is not set or is not a array', 5030, $this);
		}

		$user = People\CurrentUser::get();
		$directory = $this->getDirectory($user);
		$path = $directory->getPath();

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'file1'; isset($files[$index]); $index = 'file' . ++$i) {
			/* file1 is the current entry in input data */
			$file1 = $files[$index];
			/* set vars */
			$filename = basename($file1['name']);
			//$data = $file1['data']['data'];
			//$size = $file1['size'];
			$fullpath = $path . '/' . $filename;

			if ( !$filename ) {
				throw new ServiceException(sprintf(tra('file%s name is not setted'), $index));
			}

			/* To prevent lost of data */
			if ( !is_file($fullpath) ) {
				continue;
			}

			try {
				$extension = substr($filename, strrpos($filename, '.'));

				if ( $extension == '.Z' ) {
					if ( !exec(UNZIPCMD . " $fullpath") ) {
						throw new \Exception("cant uncompress $fullpath");
					}
				}

				if ( $extension == '.adraw' || $extension == '.zip' ) {
					$zip = new \ZipArchive();
					if ( $zip->open($fullpath) === TRUE ) {
						$zip->extractTo($path);
						$zip->close();
					}
					else {
						throw new \Exception("cant uncompress $filename");
					}
					break;
				}

				$feedback = new ServiceFeedback(sprintf(tra('%s is put in %s'), $filename, $path), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, null);
				$return->addError($index, $error);
				continue;
			}
		}

		return $return;
	}
}
