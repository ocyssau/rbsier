<?php
namespace Service\Controller\Application;

use Service\Controller\ServiceController;
use Service\Controller\ServiceRespons;
use Zend\Http\PhpEnvironment\Request as HttpRequest;

/**
 * Class to get files from ranchbe objects
 */
class PingService extends ServiceController
{

	/**
	 */
	public function __construct()
	{
		$this->request = new HttpRequest();
	}

	/**
	 */
	public function getService()
	{
		return $this->postService();
	}

	/**
	 */
	public function postService()
	{
		$return = new ServiceRespons($this);
		$request = $this->request;
		$data = [];
		$data['http-post-data'] = $request->getPost();
		$data['http-get-data'] = $request->getQuery();
		$data['raw-data'] = $request->getContent();
		//file_get_contents("php://input");

		$contentType = $request->getHeader('Content-Type', null);
		if ( $contentType == 'application/json' ) {
			$data['jsonData'] = json_decode($data['raw-data']);
		}

		/**
		 $parameter = new \Zend\Stdlib\Parameters(['docfiles'=>[
		 'docfile1'=>['name'=>'test'], 
		 'docfile2'=>['name'=>'test']
		 ]]);
		 */

		$data['headers'] = $request->getHeaders()->toArray();
		//var_dump($request->getHeader('User-Agent')->getFieldValue());die;
		//$data['headers'] = apache_request_headers();

		$return->setData($data);
		return $return;
	}

	/**
	 * 
	 */
	public function serverdumpService()
	{
		var_export($_SERVER);
		die();
	}
}
