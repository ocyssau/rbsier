<?php
namespace Service\Controller\Application;

use Service\Controller\ServiceController;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceFeedback;
use Ranchbe;
use Rbs\Space\Factory as DaoFactory;

/**
 * Class to get files from ranchbe objects
 */
class AuthService extends ServiceController
{

	/**
	 */
	public function __construct()
	{}

	/**
	 */
	public function init()
	{
		$this->isService = true;
		rbinit_context();
		return $this;
	}

	/**
	 *
	 */
	public static function remoteIsOnsameNetwork()
	{
		$remoteIp = explode('.', self::getRemoteIp());
		$localIp = explode('.', $_SERVER['SERVER_ADDR']);
		unset($localIp[3], $remoteIp[3]);
		return ($localIp == $remoteIp);
	}

	/**
	 * 
	 * @return string
	 */
	public static function getRemoteIp()
	{
		// IP si internet partagé
		if ( isset($_SERVER['HTTP_CLIENT_IP']) ) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		// IP derrière un proxy
		elseif ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		// Sinon : IP normale
		else {
			$ip = (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
		}
		return $ip;
	}

	/**
	 *
	 * @return string
	 */
	public function getUserAgent()
	{
		return $_SERVER['USER-AGENT'];
	}

	/**
	 * Authenticate from HTTP POST datas
	 * $_POST['username']
	 * $_POST['password']
	 * $_POST['rememberme']
	 * 
	 */
	public function authenticateService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;
		if ( $request->isPost() ) {
			$username = $request->getPost('username');
			$password = $request->getPost('password');
			$rememberme = $request->getPost('rememberme');
		}
		else if ( $request->isGet() ) {
			$username = $request->getQuery('username');
			$password = $request->getQuery('password');
			$rememberme = $request->getQuery('rememberme');
		}

		$rbServiceManager = Ranchbe::get()->getServiceManager();
		$authService = $rbServiceManager->getAuthservice();
		$storage = $authService->getStorage();
		
		/* check authentication... */
		$authService->getAdapter()
			->setIdentity($username)
			->setCredential($password);
		$result = $authService->authenticate();
		$messages = $result->getMessages();
		$return->addFeedback(1, new ServiceFeedback(array_pop($messages)));

		if ( $result->isValid() ) {
			/* Check if it has rememberMe : */
			if ( $rememberme == 1 ) {
				$storage->setRememberMe(1);
			}

			$storage->write($result->getIdentity());
			try {
				$currentUser = $rbServiceManager->currentUSerService();
				$currentUser->setExtend('fromIp', $this->getRemoteIp());
				$currentUser->setExtend('user-agent', $request->getHeader('User-Agent')
					->getFieldValue());
				if ( $this->remoteIsOnsameNetwork() == true ) {
					$currentUser->setExtend('connFrom', $currentUser::CONN_FROM_LOCAL);
				}
				else {
					$currentUser->setExtend('connFrom', $currentUser::CONN_FROM_DISTANT);
				}
			}
			catch( \Exception $e ) {
				throw $e;
			}

			/* auth from ldap */
			if ( $currentUser->authFrom == 'ldap' ) {
				try {
					$factory = DaoFactory::get();

					/* Save ldap user in db */
					$id = $factory->getDao($currentUser::$classId)->getIdFromUid($currentUser->getUid());
					$currentUser->setId($id);
					$factory->getDao($currentUser::$classId)->save($currentUser);

					/* Roles mapping */
					(isset($currentUser->memberof)) ? $memberof = $currentUser->memberof : $memberof = array();
					if ( count($memberof) > 0 ) {
						$urDao = $factory->getDao(\Acl\Model\UserRole::$classId);
						$roleDao = $factory->getDao(\Acl\Model\Role::$classId);
						$roleMap = $authService->getAdapter()->getOptions()['rolemap'];
						foreach( $memberof as $ldapRole ) {
							if ( isset($roleMap[$ldapRole]) ) {
								$roleName = $roleMap[$ldapRole];
								$roleId = $roleDao->getIdFromUid($roleName);
								try {
									$urDao->assign($currentUser->getId(), $roleId, '\app');
								}
								catch( \PDOException $e ) {
									/* Nothing */
								}
							}
						}
					}
				}
				catch( \Rbplm\Dao\NotExistingException $e ) {
					DaoFactory::get()->getDao($currentUser::$classId)->save($currentUser);
				}
			}
			/* auth from db */
			elseif ( $currentUser->authFrom == 'db' ) {
				DaoFactory::get()->getDao($currentUser::$classId)->save($currentUser);
			}

			/** set the connexion from option */
			$session = $rbServiceManager->getSessionManager()->getStorage();
			if ( $currentUser->connFrom == $currentUser::CONN_AUTO_DETECT ) {
				$session->connFrom = $currentUser->getExtend('connFrom');
			}
			else {
				$session->connFrom = $currentUser->connFrom;
			}
			$return->setHttpErrorCode(200);
		}
		else {
			/* Auth failed */
			$return->addFeedback(1, new ServiceFeedback("Credential auth failed"));
			$return->setHttpErrorCode(401);
		}

		return $return;
	}

	/**
	 */
	public function logoutService()
	{
		$return = new ServiceRespons($this);

		/* Trash all records in session */
		$_SESSION = null;
		$authService = Ranchbe::get()->getServiceManager()->getAuthservice();
		$storage = $authService->getStorage();
		$authService->clearIdentity();
		$storage->forgetMe();
		session_destroy();

		$return->addFeedback(1, new ServiceFeedback("You've been logged out"));
		return $return;
	}

	/**
	 *
	 */
	public function getmeService()
	{
		$return = new ServiceRespons($this);
		$user = \Rbplm\People\CurrentUser::get();
		$return->setData($user->getArrayCopy());
		return $return;
	}
}

