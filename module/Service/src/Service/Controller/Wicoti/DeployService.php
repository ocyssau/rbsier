<?php
namespace Service\Controller\Wicoti;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Document;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;
use Rbplm\Dao\NotExistingException;
use Service\Controller\ServiceFeedback;

/**
 * Get and set some datas from one document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	id:string,
 *	spacename:string
 * ]
 * ~~~~~~~~~
 * 
 */
class DeployService extends ServiceController
{

	/**
	 * remove check access and context manager
	 * 
	 * {@inheritDoc}
	 * @see \Service\Controller\ServiceController::init()
	 */
	public function init()
	{
		$this->isService = true;
		return $this;
	}

	/**
	 * Get the docfile for wicoti package
	 * 
	 * @throws ServiceException
	 * @throws \Throwable
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	private function getDocfile()
	{
		$config = \Ranchbe::get()->getConfig('wicoti');
		$isDeployed = $config['isdeployed'];

		if ( $isDeployed == false ) {
			return new Docfile\Version();
		}

		$filename = $config['package']['name'];
		$spacename = $config['package']['spacename'];

		if ( !($filename) ) {
			throw new ServiceException('filename is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5051, $this);
		}

		try {
			$docfile = new Docfile\Version();

			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/* @var \Rbs\Ged\Docfile\VersionDao $dao */
			$docfile->dao = $factory->getDao(Docfile\Version::$classId);
			$docfile->dao->loadFromName($docfile, $filename);
		}
		catch( \Throwable $e ) {
			throw $e;
		}

		try {
			$documentId = $docfile->getParent(true);
			$docdao = $factory->getDao(Document\Version::$classId);
			$select = $docdao->getTranslator()->getSelectAsApp('', [
				'label',
				'version',
				'tags'
			]);
			$assoc = $docdao->query($select, 'id=:id', [
				':id' => $documentId
			])->fetch(\PDO::FETCH_ASSOC);

			$docfile->version = $assoc['version'];
			$docfile->tags = $assoc['tags'];
			$docfile->label = $assoc['label'];
		}
		catch( \Throwable $e ) {
			throw $e;
		}

		return $docfile;
	}

	/**
	 * GET Method
	 *
	 * HTTP GET method
	 * NONE ACCESS RESTRICTIONS
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 * ]
	 * ~~~~~~~~~
	 * 
	 * if version is set, get the document with the specified version, elese return the last version.
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function getcurrentdatasService()
	{
		$return = new ServiceRespons($this);

		try {
			$docfile = $this->getDocfile();
			$return->setData($docfile->getArrayCopy());
		}
		catch( NotExistingException $e ) {
			$fb = new ServiceFeedback('this file is not existing', null, null);
			$return->addFeedback(1, $fb);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), null, null);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * HTTP GET method
	 * NONE ACCESS RESTRICTIONS
	 *        	
	 * @return string base64 encoded datas
	 */
	public function downloadService()
	{
		$return = new ServiceRespons($this);

		try {
			$docfile = $this->getDocfile();
			$fsdata = $docfile->getData()->getFsdata();
			$asName = $fsdata->getData()->getRootname() . '.' . $docfile->label . $fsdata->getData()->getExtension();
			$fsdata->download($asName);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), null, null);
			$return->addError(1, $error);
		}
		return $return;
	}
} /* End of class */
