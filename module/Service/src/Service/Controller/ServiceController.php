<?php
namespace Service\Controller;

use Ranchbe;
use Rbplm\People;
use Rbs\Space\Factory as DaoFactory;
use Acl\AccessRestrictionException;
use Zend\Http\PhpEnvironment\Request as HttpRequest;

/**
 * 
 *
 */
abstract class ServiceController
{

	/**
	 * 
	 */
	public $view;

	/**
	 * 
	 */
	public $errorStack;

	/**
	 * 
	 */
	public $layout;

	/**
	 * @var HttpRequest
	 */
	protected $request;

	/**
	 * 
	 * @var boolean
	 */
	protected $isService;

	/**
	 *
	 * @var \Acl\Service\Acl
	 */
	protected $acl;

	/**
	 *
	 * @var string
	 */
	public $resourceCn;

	/**
	 *
	 */
	public function __construct()
	{}

	/**
	 */
	public function init()
	{
		$this->isService = true;
		$this->checkAccess();
		return $this;
	}

	/**
	 * To run after action
	 * Not called if redirect is call
	 */
	public function postDispatch()
	{}

	/**
	 */
	public function checkAccess()
	{
		$authService = Ranchbe::get()->getServiceManager()->getAuthservice();
		$storage = $authService->getStorage();

		if ( !$authService->hasIdentity() ) {
			throw new AccessRestrictionException('user has no access');
		}

		$userProperties = $storage->read();
		$user = new People\User();
		$user->hydrate($userProperties);
		People\CurrentUser::set($user);

		$user->setLastLogin(new \DateTime());
		if ( $user->isLoaded() ) {
			DaoFactory::get()->getDao($user->cid)->updateLastLogin($user);
		}
	}

	/**
	 *
	 */
	public function checkRight($rightName, $resourceCn)
	{
		$this->getAcl()->checkRightFromCn($rightName, $resourceCn);
	}

	/**
	 * @return \Acl\Service\Acl
	 */
	public function getAcl()
	{
		if ( !isset($this->acl) ) {
			$this->acl = new \Acl\Service\Acl($this);
			$this->acl->setBaseCn($this->resourceCn);
		}
		return $this->acl;
	}

	/**
	 * @return HttpRequest
	 */
	public function getRequest()
	{
		if ( !isset($this->request) ) {
			$this->request = new HttpRequest();
		}
		return $this->request;
	}

	/**
	 *
	 */
	public function setRequest($request)
	{
		$this->request = $request;
		return $this;
	}

	/**
	 * Alias for getRequest
	 * Implement ZF2 controller helper
	 */
	public function params()
	{
		return $this->getRequest();
	}
}
