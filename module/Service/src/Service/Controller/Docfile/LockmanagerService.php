<?php
namespace Service\Controller\Docfile;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceError;
use Service\Controller\ServiceException;

/**
 * Lock manager for files.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * docfiles:[
 * 		docfile1:{
 * 			id:string,
 * 			spacename:string,
 * 		},
 * 		docfile2:{...},
 * 		...
 * 	}
 * ]
 * ~~~~~~~~~
 * 
 * 
 */
class LockmanagerService extends \Service\Controller\ServiceController
{

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Unlock file.
	 * 
	 * Back to free the status of the checkouted files.
	 * Reseted file becomes with Free status, the datas in vault is not updated with the datas of the user, file in wildspace are not deleted.
	 * 
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * docfiles:[
	 * 		docfile1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 		},
	 * 		docfile2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 *
	 */
	public function resetfileService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$docfiles = $request->getQuery('docfiles', null);

		if ( !($docfiles) ) {
			throw new ServiceException('docfiles is not set or is not a array', 5050, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'docfile' . $i; isset($docfiles[$index]); $index = 'docfile' . ++$i) {
			/* document1 is the current entry in input data */
			$docfile1 = $docfiles[$index];
			/* set vars */
			$fileId = $docfile1['id'];
			$spacename = $docfile1['spacename'];

			/* init some helpers */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Docfile\VersionDao $dao */
			$dao = $factory->getDao(Docfile\Version::$classId);
			$service = new \Rbs\Ged\Docfile\Service($factory);

			$docfile = new Docfile\Version();
			$dao->loadFromId($docfile, $fileId);

			/* Check permission */
			$this->getAcl()->checkRightFromUid('read', $docfile->getParentUid());

			try {
				$service->checkin($docfile, true, false, true);
			}
			catch( \Exception $e ) {
				$return->addError($fileId, new ServiceError($e->getMessage(), $e, $docfile));
				continue;
			}
			$return->addFeedback($fileId, new ServiceFeedback('success', $docfile));
		}

		return $return;
	}
} /* End of class */

