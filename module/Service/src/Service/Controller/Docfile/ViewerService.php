<?php
namespace Service\Controller\Docfile;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Document;
use Service\Controller\ServiceController;

/**
 * Get file from Docfile
 *
 */
class ViewerService extends ServiceController
{

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Get the files associated to Document\Version or Docfile\Version
	 * $_GET['id'] may be id of document or docfile
	 *
	 * HTTP:GET method
	 *
	 * @param $_GET['id'] int 
	 * 					Set id to get only one file.
	 * @param $_GET['checked'] array 
	 * 					If you want get many files, use the checked parameter.
	 * @param $_GET['spacename'] string
	 *        	Spacename of the object to get. Permited values are
	 *        	- 'workitem'
	 *        	- 'bookshop'
	 *        	- 'cadlib'
	 *        	- 'mockup'
	 *        	
	 * @return string base64 encoded datas
	 */
	public function downloadService($request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;

		$id = $request->getQuery('id', null);
		$checked = $request->getQuery('checked', null);
		$spacename = $request->getQuery('spacename', null);

		if ( $checked ) {
			$id = current($checked);
		}

		$docfile = new Docfile\Version();
		$factory = DaoFactory::get($spacename);

		/* @var \Rbs\Ged\Docfile\VersionDao $dao */
		$dao = $factory->getDao($docfile->cid);
		$dao->loadFromId($docfile, $id);

		/* Check permission, only if document is a resource */
		$docDao = $factory->getDao(Document\Version::$classId);
		$res = $docDao->query([
			$docDao->toSys('parentUid')
		], $docDao->toSys('id') . '=:id', [
			':id' => $docfile->parentId
		]);
		$this->getAcl()->checkRightFromUid('read', $res->fetchColumn(0));

		$fsdata = $docfile->getData()->getFsdata();
		$fsdata->download();
	}
}
