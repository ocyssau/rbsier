<?php
namespace Service\Controller\Docfile;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\People;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceError;
use Service\Controller\ServiceException;

/**
 *
 *
 */
class IterationService extends \Service\Controller\ServiceController
{

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Delete iteration files.
	 * 
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * docfiles:[
	 * 		docfile1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 		},
	 * 		docfile2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 *
	 */
	function deleteService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$docfiles = $request->getQuery('docfiles', null);

		if ( !($docfiles) ) {
			throw new ServiceException('docfiles is not set or is not a array', 5050, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'docfile' . $i; isset($docfiles[$index]); $index = 'docfile' . ++$i) {
			/* document1 is the current entry in input data */
			$docfile1 = $docfiles[$index];
			/* set vars */
			$fileId = $docfile1['id'];
			$spacename = $docfile1['spacename'];

			/* init some helpers */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Docfile\IterationDao $dao */
			$dao = $factory->getDao(Docfile\Iteration::$classId);
			$service = new \Rbs\Ged\Docfile\Iteration\Service($factory);

			try {
				$docfile = new Docfile\Iteration();
				$dao->loadFromId($docfile, $fileId);

				/* Check permission */
				$this->getAcl()->checkRightFromUid('read', $docfile->getParentUid());

				$service->delete($docfile, true);
			}
			catch( \Exception $e ) {
				$return->addError($fileId, new ServiceError($e->getMessage(), $e, $docfile));
				continue;
			}

			$return->addFeedback($fileId, new ServiceFeedback('success', $docfile));
		}

		return $return;
	}

	/**
	 * 
	 * Put iterations files in the wildspace of the current user.
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * docfiles:[
	 * 		docfile1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 		},
	 * 		docfile2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	function putinwsService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$docfiles = $request->getQuery('docfiles', null);

		if ( !($docfiles) ) {
			throw new ServiceException('docfiles is not set or is not a array', 5050, $this);
		}

		$wildspace = new People\User\Wildspace(People\CurrentUser::get());
		$toPath = $wildspace->getPath();
		$prefix = 'consult_';

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'docfile' . $i; isset($docfiles[$index]); $index = 'docfile' . ++$i) {
			/* document1 is the current entry in input data */
			$docfile1 = $docfiles[$index];
			/* set vars */
			$fileId = $docfile1['id'];
			$spacename = $docfile1['spacename'];

			$classId = Docfile\Iteration::$classId;

			/* init some helpers */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Docfile\VersionDao $dao */
			$dao = $factory->getDao(Docfile\Iteration::$classId);

			try {
				$docfile = $factory->getModel($classId);
				$dao->loadFromId($docfile, $fileId);

				/* Check permission */
				//$this->getAcl()->checkRightFromUid('read', $docfile->getParentUid());

				$docfile->getData()
					->getFsData()
					->copy($toPath . '/' . $prefix . $docfile->getData()
					->getName(), 0777, true);
			}
			catch( \Exception $e ) {
				$return->addError($fileId, new ServiceError($e->getMessage(), $e, $docfile));
				continue;
			}

			$feedback = new ServiceFeedback(sprintf(tra('data %s copied in wildspace'), $docfile->getName()), null);
			$return->addFeedback($index, $feedback);
		}

		return $return;
	}

	/**
	 * 
	 * Get the files associated to Document\Version or Docfile\Version
	 * $_GET['id'] may be id of document or docfile
	 *
	 * HTTP:GET method
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * 
	 * @param $_GET['id'] int 
	 * 					Set id to get only one file.
	 * @param $_GET['checked'] array 
	 * 					If you want get many files, use the checked parameter.
	 * @param $_GET['spacename'] string
	 *        	Spacename of the object to get. Permited values are
	 *        	- 'workitem'
	 *        	- 'bookshop'
	 *        	- 'cadlib'
	 *        	- 'mockup'
	 *        	
	 * @return string base64 encoded datas
	 */
	function downloadService($request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$id = $request->getQuery('id', null);
		$ids = $request->getQuery('checked', null);
		if ( is_array($ids) ) {
			$id = current($ids);
		}
		$spacename = $request->getQuery('spacename', null);

		$docfile = new Docfile\Iteration();
		$factory = DaoFactory::get($spacename);
		/* @var \Rbs\Ged\Docfile\IterationDao $dao */
		$dao = $factory->getDao($docfile->cid);
		$dao->loadFromId($docfile, $id);

		/* Check permission */
		//$this->getAcl()->checkRightFromUid('read', $docfile->getParentUid());

		$fsdata = $docfile->getData()->getFsdata();
		$fsdata->download();
	}
} /* End of class */
