<?php
namespace Service\Controller\Docfile;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\People\User\Wildspace;
use Rbplm\People\CurrentUser;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceError;
use Service\Controller\ServiceException;

/**
 * Manage docfiles.
 *
 */
class DatamanagerService extends \Service\Controller\ServiceController
{

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * Put files in wildspace of the current user.
	 * 
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * docfiles:[
	 * 		docfile1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 			class:string
	 * 		},
	 * 		docfile2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * Class key may have values :
	 * - iteration
	 * - version
	 * Default value is version, and it must be set only when a iteration file is requested.
	 * 
	 * @return ServiceRespons
	 */
	function putinwsService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$docfiles = $request->getQuery('docfiles', null);
		if ( !($docfiles) ) {
			throw new ServiceException('docfiles is not set or is not a array', 5050, $this);
		}

		$wildspace = new Wildspace(CurrentUser::get());
		$toPath = $wildspace->getPath();
		$prefix = '';

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'docfile' . $i; isset($docfiles[$index]); $index = 'docfile' . ++$i) {
			/* document1 is the current entry in input data */
			$docfile1 = $docfiles[$index];
			/* set vars */
			$fileId = $docfile1['id'];
			$spacename = $docfile1['spacename'];

			if ( isset($docfile1['class']) && $docfile1['class'] == 'iteration' ) {
				$classId = Docfile\Iteration::$classId;
			}
			else {
				$classId = Docfile\Version::$classId;
			}

			/* init some helpers */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Docfile\VersionDao $dao */
			$dao = $factory->getDao($classId);

			try {
				$docfile = $factory->getModel($classId);
				$dao->loadFromId($docfile, $fileId);

				$suffix = '.v' . $docfile->iteration;

				/* Check permission */
				$this->getAcl()->checkRightFromUid('read', $docfile->getParentUid());
				/* copy to ws */
				$data = $docfile->getData();
				$data->getFsData()->copy($toPath . '/' . $prefix . $data->rootname . $suffix . $data->extension, 0777, true);
			}
			catch( \Exception $e ) {
				$return->addError($fileId, new ServiceError($e->getMessage(), $e, $docfile));
				continue;
			}

			$feedback = new ServiceFeedback(sprintf(tra('data %s copied in wildspace'), $docfile->getName()), null);
			$return->addFeedback($index, $feedback);
		}

		return $return;
	}
} /* End of class */
