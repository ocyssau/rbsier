<?php
namespace Service\Controller\Docfile;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\AccessCodeException;
use Rbplm\Sys\Error;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceError;
use Service\Controller\ServiceException;

/**
 * Manager of docfiles
 *
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * docfiles:[
 * 		docfile1:{
 * 			id:string,
 * 			spacename:string
 * 		},
 * 		docfile2:{...},
 * 		...
 * 	}
 * ]
 * ~~~~~~~~~
 */
class ManagerService extends \Service\Controller\ServiceController
{

	public $pageId = 'docfile_service';

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Delete docfile
	 * 
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * docfiles:[
	 * 		docfile1:{
	 * 			id:string,
	 * 			spacename:string
	 * 		},
	 * 		docfile2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 *
	 */
	function deleteService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$docfiles = $request->getQuery('docfiles', null);

		if ( !($docfiles) ) {
			throw new ServiceException('docfiles is not set or is not a array', 5050, $this);
		}

		$withfiles = true;
		$withiterations = true;

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'docfile' . $i; isset($docfiles[$index]); $index = 'docfile' . ++$i) {
			/* docfile1 is the current entry in input data */
			$docfile1 = $docfiles[$index];

			/* set vars */
			isset($docfile1['id']) ? $fileId = $docfile1['id'] : $fileId = null;
			isset($docfile1['spacename']) ? $spacename = $docfile1['spacename'] : $spacename = null;

			/* init some helpers */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Docfile\VersionDao $dao */
			$dao = $factory->getDao(Docfile\Version::$classId);
			$service = new \Rbs\Ged\Docfile\Service($factory);

			try {
				$docfile = new Docfile\Version();
				$dao->loadFromId($docfile, $fileId);

				/* Check permission */
				//$this->getAcl()->checkRightFromUid('delete', $docfile->getParentUid());

				/**/
				$service->delete($docfile, $withfiles, $withiterations);
			}
			catch( \Exception $e ) {
				$return->addError($fileId, new ServiceError($e->getMessage(), $e, $docfile));
				continue;
			}

			$return->addFeedback($fileId, new ServiceFeedback('success', $docfile));
		}

		return $return;
	}

	/**
	 * Set the role of docfile.
	 * 
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * docfiles:[
	 * 		docfile1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 			'role':string
	 * 		},
	 * 		docfile2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * Role name must be put in docfile[n] json inputs datas.
	 * role may be a value of constants of \Rbplm\Ged\Docfile\Role class.
	 * - MAIN = 1 : Main file
	 * - FRAGMENT = 4 : A part of the document
	 * - ANNEX = 8: Annex to document
	 * - VISU = 16: To use to visualize document
	 * - PICTURE = 32 : A picture to show content of document
	 * - CONVERT = 64 : Converted in other format
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	function setroleService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$docfiles = $request->getQuery('docfiles', null);

		if ( !($docfiles) ) {
			throw new ServiceException('docfiles is not set or is not a array', 5050, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'docfile' . $i; isset($docfiles[$index]); $index = 'docfile' . ++$i) {
			/* document1 is the current entry in input data */
			$docfile1 = $docfiles[$index];
			/* set vars */
			$fileId = $docfile1['id'];
			$spacename = $docfile1['spacename'];
			$roleId = $docfile1['role'];

			/* init some helpers */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Docfile\VersionDao $dao */
			$dao = $factory->getDao(Docfile\Version::$classId);

			try {
				$docfile = new Docfile\Version();
				$dao->loadFromId($docfile, $fileId);

				/* Check permission */
				$this->getAcl()->checkRightFromUid('edit', $docfile->getParentUid());

				$aCode = $docfile->checkAccess();
				if ( $aCode >= 100 ) {
					throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array(
						'accessCode' => $aCode
					));
				}

				$docfile->setMainrole($roleId);
				$dao->save($docfile);
			}
			catch( \Exception $e ) {
				$return->addError($fileId, new ServiceError($e->getMessage(), $e, $docfile));
				continue;
			}
			$return->addFeedback($fileId, new ServiceFeedback('success', $docfile));
		}

		return $return;
	}
} /* End of class */

