<?php
namespace Service\Controller\Docfile;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Document;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;
use Rbplm\Dao\NotExistingException;
use Service\Controller\ServiceFeedback;

/**
 * Get and set some datas from one document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	id:string,
 *	spacename:string
 * ]
 * ~~~~~~~~~
 * 
 */
class EntityService extends ServiceController
{

	/**
	 * GET Method
	 *
	 * HTTP GET method
	 * WITH ACLS CONTROL
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	id:string,
	 *	spacename:string
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function getfromidService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$id = $request->getQuery('id', null);
		$spacename = $request->getQuery('spacename', null);

		if ( !($id) ) {
			throw new ServiceException('id is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5051, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbplm\Ged\Docfile\Version $document */
			$docfile = $factory->getDao(Docfile\Version::$classId)->loadFromId(new Docfile\Version(), $id);

			/* Check permission from parent container */
			$docDao = $factory->getDao(Document\Version::$classId);
			$res = $docDao->query([
				$docDao->toSys('parentUid')
			], $docDao->toSys('id') . '=:id', [
				':id' => $docfile->parentId
			]);
			$docfile->containerUid = $res->fetchColumn(0);
			$this->getAcl()->checkRightFromUid('read', $docfile->containerUid);

			$return->setData($docfile->getArrayCopy());
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, $docfile);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * GET Method
	 *
	 * HTTP GET method
	 * WITH ACLS CONTROL
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	name:string,
	 *	version:integer, [OPTIONAL], if not set return the last
	 *	spacename:string,
	 *  select: array of properties name to return
	 * ]
	 * ~~~~~~~~~
	 * 
	 * if version is set, get the document with the specified version, elese return the last version.
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	function getfromnameService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$name = $request->getQuery('name', null);
		$spacename = $request->getQuery('spacename', null);
		$versionId = $request->getQuery('version', null);
		$select = $request->getQuery('select', null);

		if ( !($name) ) {
			throw new ServiceException('number is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5052, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);

			/* @var \Rbs\Ged\Docfile\VersionDao $dao */
			$dao = $factory->getDao(Docfile\Version::$classId);

			/* @var \Rbplm\Ged\Docfile\Version $docfile */
			$docfile = new Docfile\Version();
			$dao->loadFromName($docfile, $name, $versionId);

			/* Check permission from parent container */
			$docDao = $factory->getDao(Document\Version::$classId);
			$res = $docDao->query([
				$docDao->toSys('parentUid')
			], $docDao->toSys('id') . '=:id', [
				':id' => $docfile->parentId
			]);
			$docfile->containerUid = $res->fetchColumn(0);
			$this->getAcl()->checkRightFromUid('read', $docfile->containerUid);

			if ( $select ) {
				if ( !is_array($select) ) {
					throw new ServiceException('select must be a array', 5053, $this);
				}
				$return->setData($docfile->export($select));
			}
			else {
				$return->setData($docfile->getArrayCopy());
			}
		}
		catch( NotExistingException $e ) {
			$fb = new ServiceFeedback('this file is not existing', null, null);
			$return->addFeedback(1, $fb);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), null, null);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * HTTP GET method
	 * NONE ACCESS RESTRICTIONS
	 *
	 * @param \Zend\Http\Request
	 *	filename:string,
	 *	spacename:string,
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	function isexistingService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$name = $request->getQuery('name', null);
		$spacename = $request->getQuery('spacename', null);

		if ( !($name) ) {
			throw new ServiceException('filename is not set', 5060, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5062, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/* @var \Rbs\Ged\Docfile\VersionDao $dao */
			$dao = $factory->getDao(Docfile\Version::$classId);
			$stmt = $dao->query([
				'id',
				$dao->toSys('iteration')
			], 'name=:name ORDER BY ' . $dao->toSys('iteration'), [
				':name' => $name
			]);
			if ( $stmt->rowCount() == 0 ) {
				$return->setData("");
			}
			else {
				$return->setData($stmt->fetchAll(\PDO::FETCH_ASSOC));
			}
		}
		catch( \Exception $e ) {
			throw new ServiceException($e->getMessage(), 5060, $this);
		}

		return $return;
	}
} /* End of class */
