<?php
namespace Service\Controller\Pdm;

use Rbplm\Pdm\Product;
use Rbplm\Ged\Document;
use Rbs\Space\Factory as DaoFactory;
use Service\Controller\ServiceController;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceException;
use Service\Controller\ServiceRespons;

/**
 */
class DocumentversionService extends ServiceController
{

	public $pageId = 'documentversion_service';

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Pdm::$appCn;
	}

	/**
	 * HTTP GET AJAX METHO
	 *
	 * Load or init a new product from a document
	 * 
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 * 
	 * 
	 */
	public function initproductService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$documents = $request->getQuery('documents', null);

		/* Check permission */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5050, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document' . $i; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];

			/* set vars */
			isset($document1['id']) ? $documentId = $document1['id'] : $documentId = null;
			isset($document1['spacename']) ? $spacename = $document1['spacename'] : $spacename = null;

			/* init dao */
			$factory = DaoFactory::get($spacename);
			$productDao = $factory->getDao(Product\Version::$classId);
			$documentDao = $factory->getDao(Document\Version::$classId);

			/* Load document */
			$document = new Document\Version();
			$documentDao->loadFromId($document, $documentId);

			/* Load product from document id */
			try {
				$product = new Product\Version();
				$filter = $productDao->toSys('documentId') . '=:documentId';
				$productDao->load($product, $filter, array(
					':documentId' => $documentId
				));
				$product->setDocument($document);

				$feedback = new ServiceFeedback('Load existing product', $product);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				/* If none product found, create it */
				$product = Product\Version::init();
				$product->setName($document->getName())
					->setNumber($document->getNumber());
				$product->setDocument($document);
				$productDao->save($product);

				$feedback = new ServiceFeedback('Create new product', $product);
				$return->addFeedback($index, $feedback);
			}
		}

		return $return;
	}
} /* End of class */
