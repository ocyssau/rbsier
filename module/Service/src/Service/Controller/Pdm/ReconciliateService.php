<?php
namespace Service\Controller\Pdm;

use Service\Controller\ServiceController;
use Service\Controller\ServiceError;
use Service\Controller\ServiceException;
use Service\Controller\ServiceRespons;
use Rbs\Pdm\Service\Reconciliate;
use Service\Controller\ServiceFeedback;

/**
 */
class ReconciliateService extends ServiceController
{

	/* @var string */
	public $pageId = 'reconciliate_service';

	/**
	 * 
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Pdm::$appCn;
	}

	/**
	 * HTTP GET Method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 *        	<code>
	 *        	** 'limit':integer
	 *        	</code>
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function producttodocumentService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$instanceId = $request->getQuery('instanceId', null);
		$params = [];
		$params['limit'] = $request->getQuery('limit', null);
		$service = new Reconciliate($params);

		if ( $instanceId ) {
			$filter = 'instance.id=:instanceId';
			$bind = array(
				':instanceId' => (int)$instanceId
			);
		}
		else {
			$filter = '1=1';
			$bind = [];
		}

		try {
			$stmt = $service->productToReconcilateWithDocument($filter, $bind);
			$return->setData($stmt->fetchAll(\PDO::FETCH_ASSOC));
			$stmt = $service->productToDocument($filter, $bind);
			$feedback = new ServiceFeedback('Ok');
			$return->addFeedback(0, $feedback);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e);
			$return->addError(0, $error);
		}

		return $return;
	}

	/**
	 * HTTP GET Method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 *        	<code>
	 *        	** 'limit':integer
	 *        	** 'instanceId':integer
	 *        	</code>
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function instancetoproductService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$instanceId = $request->getQuery('instanceId', null);
		$params = [];
		$params['limit'] = $request->getQuery('limit', null);
		$service = new Reconciliate($params);

		if ( $instanceId ) {
			$filter = 'instance.id=:instanceId';
			$bind = array(
				':instanceId' => (int)$instanceId
			);
		}
		else {
			$filter = '1=1';
			$bind = array();
		}

		try {
			$stmt = $service->instanceToReconcilateWithProduct($filter, $bind);
			$return->setData($stmt->fetchAll(\PDO::FETCH_ASSOC));
			$stmt = $service->instanceToProduct($filter, $bind);
			$feedback = new ServiceFeedback('Ok');
			$return->addFeedback(0, $feedback);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e);
			$return->addError(0, $error);
		}

		return $return;
	}

	/**
	 * HTTP GET Method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 *        	<code>
	 *        	** 'limit':integer
	 *        	** 'spacename':string
	 *        	</code>
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function sychronisedocumentService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$params = [];
		$params['limit'] = $request->getQuery('limit', null);
		$spacename = $request->getQuery('spacename', null);

		try {
			/* validate spacename value */
			$ok = \Rbs\Space\Space::getIdFromName($spacename);
			if ( !$ok ) {
				throw new \InvalidArgumentException("$spacename is not a valid spacename");
			}
			$params['spacename'] = $spacename;
			$service = new Reconciliate($params);
			$stmt = $service->instanceToProduct();
			$return->setData($stmt->queryString);
			$return->setData($stmt->bind);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e);
			$return->addError(0, $error);
		}

		return $return;
	}

	/**
	 * HTTP GET Method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 *        	<code>
	 *        	** 'limit':integer
	 *        	** 'spacename':string
	 *        	</code>
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function sychroniseproductService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$params = [];
		$params['limit'] = $request->getQuery('limit', null);
		$spacename = $request->getQuery('spacename', null);

		try {
			/* validate spacename value */
			$ok = \Rbs\Space\Space::getIdFromName($spacename);
			if ( !$ok ) {
				throw new \InvalidArgumentException("$spacename is not a valid spacename");
			}
			$params['spacename'] = $spacename;
			$service = new Reconciliate($params);
			$stmt = $service->sychroniseProduct();
			$return->setData($stmt->queryString);
			$return->setData($stmt->bind);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e);
			$return->addError(0, $error);
		}

		return $return;
	}
} /* End of class */
