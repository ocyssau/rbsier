<?php
namespace Service\Controller\Pdm;

use Rbplm\Pdm\Product;
use Rbs\Space\Factory as DaoFactory;
use Service\Controller\ServiceController;
use Service\Controller\ServiceError;
use Service\Controller\ServiceException;
use Service\Controller\ServiceRespons;

/**
 */
class ProductversionService extends ServiceController
{

	public $pageId = 'productversion_service';

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Pdm::$appCn;
	}

	/**
	 * AJAX Method to search in product version table
	 * HTTP GET Method
	 * 
	 * @param \Zend\Http\Request
	 *        	<code>
	 *        	** 'what':string, String to find
	 *        	** 'where':string, in property name as define in Product\Version class
	 *        	** 'select':string, array of properties name to return, as define in Product\Version class
	 *        	** 'op':string, operator, @see \Rbplm\Dao\Filter\Op class
	 *        	</code>
	 * 
	 */
	public function searchService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$factory = DaoFactory::get();

		$what = $request->getQuery('what', null);
		$where = $request->getQuery('where', null);
		$select = $request->getQuery('select', null);
		$op = $request->getQuery('op', null);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$dao = $factory->getDao(Product\Version::$classId);
		$list = $factory->getList(Product\Version::$classId);
		$filter->andfind($what, $dao->toSys($where), $op);

		$reducedSelect = array();
		if ( is_string($select) ) {
			$select = array(
				$select
			);
		}
		if ( is_array($select) ) {
			foreach( $dao->metaModel as $asSys => $asApp ) {
				if ( in_array($asApp, $select) ) {
					$reducedSelect[] = $asSys . ' as ' . $asApp;
				}
			}
			$filter->select($reducedSelect);
		}

		$list->load($filter);
		$return->setData($list->toArray());
		return $return;

		echo json_encode($list->toArray());
		die();
	}

	/**
	 * Get a product from his id
	 * HTTP GET Method
	 *
	 * @param \Zend\Http\Request
	 *        	<code>
	 *        	** 'id':string,
	 *        	</code>
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function getService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$id = $request->getQuery('id', null);

		if ( !($id) ) {
			throw new ServiceException('id is not set', 5051, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get();

			/* @var \Rbs\Pdm\Product\VersionDao $dao */
			$dao = $factory->getDao(Product\Version::$classId);

			/* @var \Rbplm\Pdm\Product\Version $product */
			$product = new Product\Version();
			$dao->loadFromId($product, $id);
			$this->getAcl()->checkRightFromUid('read', $product->parentUid);
			$return->setData($product->getArrayCopy());
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * Get a product from his number
	 * HTTP GET Method
	 *
	 * @param \Zend\Http\Request
	 *        	<code>
	 *        	** 'number':string,
	 *        	** 'version':integer,
	 *        	</code>
	 *
	 * @return \Service\Controller\ServiceRespons
	 */
	function getfromnumberService($request = null)
	{
		$return = new \Service\Controller\ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$number = $request->getQuery('number', null);
		$versionId = $request->getQuery('version', null);

		if ( !($number) ) {
			throw new ServiceException('number is not set', 5051, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get();

			/* @var \Rbs\Pdm\Product\VersionDao $dao */
			$dao = $factory->getDao(Product\Version::$classId);

			/* @var \Rbplm\Pdm\Product\Version $product */
			$product = new Product\Version();
			$dao->loadFromNumber($product, $number, $versionId);
			$this->getAcl()->checkRightFromUid('read', $product->parentUid);
			$return->setData($product->getArrayCopy());
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * Checkin, create, edit product
	 * HTTP POST Method
	 *
	 * @param \Zend\Http\Request
	 * @see \Pdm\Input\PdmData\Element
	 *
	 */
	public function checkinService($request = null)
	{
		$return = new \Service\Controller\ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$pdmdata = $request->getPost('pdmdata', null);

		if ( !($pdmdata) ) {
			throw new ServiceException('pdmdata is not set or is not a array', 5040, $this);
		}

		$pdmdata = \Pdm\Input\PdmDataFactory::getElement($pdmdata);

		/* init some helpers */
		$pdmService = new \Rbs\Pdm\Product\Service($this->factory);

		/* ------------- parse the document from input ------------- */
		foreach( $pdmdata as $current ) {
			/* Create a new product */
			$pdmService->create($current['properties']);
			if ( $pdmService->respons->hasErrors() ) {
				// ...
			}
		}

		return $return;
	}
} /* End of class */
