<?php
namespace Service\Controller\Object;

use Rbs\Space\Factory as DaoFactory;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;
use Search\Engine\SearchInFile;
use Rbplm\Dao\Filter\Op;
use Service\Controller\ServiceFeedback;
use Rbs\Ged\Docfile\VersionDao as DocfileDao;

/**
 * Service for create document search request.
 *
 */
class SearchService extends ServiceController
{

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
	}

	/**
	 * Search docfile.
	 *
	 * HTTP:GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * name: string as filename to Search
	 * limit: integer as max rows to return
	 * select: array of properties to return, if no set, return all
	 * lastversiononly: if true, return only the more recent docfile version
	 * ~~~~~~~~~
	 *
	 *
	 * @return string json string
	 */
	public function forfilenameService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$filename = $request->getQuery('name', null);
		$limit = (int)$request->getQuery('limit', 500);
		$select = $request->getQuery('select', null);
		$lastVersionOnly = $request->getQuery('lastversiononly', false);

		if ( !($filename) ) {
			throw new ServiceException('file is not set', 5050, $this);
		}

		/* file exist in vault */
		$searchEngine = new SearchInFile(DaoFactory::getConnexion());
		$filter = DaoFactory::get()->getFilter(\Rbplm\Ged\Docfile\Version::$classId);
		$filter->page(1, $limit);
		$filter->andfind($filename, 'name', Op::EQUAL);

		if ( $lastVersionOnly ) {
			$filter->sort('iteration', 'desc');
			$filter->groupBy('spacename');
			if ( $select ) {
				$select[] = 'iteration';
			}
		}

		if ( $select ) {
			$selectAsSys = [];
			foreach( $select as $asApp ) {
				$selectAsSys[] = 'obj.' . @DocfileDao::toSys($asApp) . ' as ' . $asApp;
			}
			$filter->select($selectAsSys);
			unset($selectAsSys);
		}

		$searchEngine->load($filter);
		if ( $searchEngine->count() == 0 ) {
			$return->addFeedback($filename, new ServiceFeedback('is not founded'));
			return $return;
		}
		else {
			$return->setData($searchEngine->getStmt()
				->fetchAll(\PDO::FETCH_ASSOC));
		}
		return $return;
	}

	/**
	 * Search document from fulltext index.
	 * 
	 * HTTP:GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * @return string json string
	 */
	public function fulltextService($request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;
		$what = $request->getQuery('what', null);

		$connexion = DaoFactory::getConnexion();
		$fullTextSearchEngine = new \Search\Engine\FullText($connexion);
		$stmt = $fullTextSearchEngine->search($what);

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Search document from fulltext index.
	 *
	 * HTTP:GET method
	 * 
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * attributes:[
	 * 		attribute1:{
	 * 			name:string,
	 * 			value:string,
	 * 			op:string
	 * 		},
	 * 		attribute2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 *
	 * @return string json string
	 */
	public function searchService($request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;
		$attributes = $request->getQuery('attributes', null);

		if ( !($attributes) ) {
			throw new ServiceException('attributes is not set or is not a array', 5050, $this);
		}

		$filter = new \Rbs\Dao\Sier\Filter();

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'attribute' . $i; isset($attributes[$index]); $index = 'attribute' . ++$i) {
			/* document1 is the current entry in input data */
			$attribute1 = $attributes[$index];

			/* set vars */
			isset($attribute1['name']) ? $name = $attribute1['name'] : $name = null;
			isset($attribute1['value']) ? $value = $attribute1['value'] : $value = null;
			isset($attribute1['op']) ? $op = $attribute1['op'] : $op = null;
			$filter->andfind($value, $name, $op);
		}
	}
} /* End of class */
