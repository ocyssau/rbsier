<?php
namespace Service\Controller\Doctype;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Doctype;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;

/**
 * Manage doctype of the document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * documents:[
 * 		document1:{
 * 			id:string,
 * 			spacename:string
 * 		},
 * 		document2:{...},
 * 		...
 * 	}
 * ]
 * ~~~~~~~~~
 *
 */
class IndexService extends ServiceController
{

	/**
	 * Recalculate the doctype of the input documents.
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * containerid:"",
	 * spacename:""
	 * ~~~~~~~~~
	 *
	 * @return ServiceRespons
	 *
	 */
	public function getofcontainerService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$containerId = $request->getQuery('containerid', null);
		$spacename = $request->getQuery('spacename', null);

		$data = [];
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Doctype::$classId);
		$stmt = $dao->getUsedByContainer($containerId, $spacename);
		while( $dtEntry = $stmt->fetch() ) {
			$doctype = new Doctype();
			$dao->hydrate($doctype, $dtEntry);
			$data[] = $doctype->getArrayCopy();
		}
		$return->setData($data);

		return $return;
	}
}/* End of class */
