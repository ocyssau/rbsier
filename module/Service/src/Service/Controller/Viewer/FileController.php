<?php
namespace Service\Controller\Viewer;

use Service\Controller\ServiceController;

/**
 */
class FileService extends ServiceController
{

	/**
	 * HTTP GET method
	 * NONE ACCESS RESTRICTIONS
	 *
	 * @param \Zend\Http\Request
	 *	file: string,
	 *
	 */
	public function getfileAction($request)
	{
		(!$request) ? $request = $this->getRequest() : null;

		$filename = $request->getQuery('file');
		if ( !$filename ) return;
		$path = \Ranchbe::get()->getConfig('viewer.reposit.path');

		$file = $path . '/' . $filename;

		if ( !is_file($file) ) {
			header("HTTP/1.0 404 Not Found");
			die('Not found');
		}

		header("Content-disposition: attachment; filename=\"$filename\"");
		header("Content-Type: application/sla");
		header("Content-Transfer-Encoding: \"$filename\"\n"); // Surtout ne pas enlever le \n
		header("Content-Length: " . filesize($file));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		readfile($file);
		die();
	}
}
