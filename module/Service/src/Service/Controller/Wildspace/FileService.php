<?php
namespace Service\Controller\Wildspace;

use Rbplm\People;
use Rbplm\People\User\Wildspace;
use Service\Controller\Application\AbstractFilemanager;
use Rbs\Space\Factory as DaoFactory;
use Zend\Http\Request as HttpRequest;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceException;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceError;

/**
 * Manage files in the user wildspace.
 *
 */
class FileService extends AbstractFilemanager
{

	/**
	 * 
	 * @param People\User $user
	 * @return \Rbplm\People\User\Wildspace
	 */
	protected function getDirectory(People\User $user)
	{
		$wildspace = new Wildspace($user);
		return $wildspace;
	}

	/**
	 * Compare md5 checksum of file in wildspace with docfile recorded in vault.
	 * 
	 * The Respons contain a feedback message = changed|unchanged, 
	 * and a data array where first key is wildspace file md5 and second is docfile md5.
	 *
	 * HTTP GET method
	 *
	 * @param HttpRequest $request
	 * ~~~~~~~~~.json
	 * 		'name':string as file name in wildspace,
	 * 		'id':int as docfile id to compare,
	 * ~~~~~~~~~
	 *
	 * @return ServiceRespons
	 *
	 */
	public function comparemd5Service(HttpRequest $request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$spacename = $request->getQuery('spacename', null);
		$fileName = $request->getQuery('name', null);
		$docfileId = $request->getQuery('id', null);

		if ( !($fileName) ) {
			throw new ServiceException('name is not set', 5030, $this);
		}
		else {
			$fileName = basename($fileName);
		}
		if ( !($docfileId) ) {
			throw new ServiceException('id is not set', 5031, $this);
		}
		else {
			$docfileId = (int)$docfileId;
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5032, $this);
		}

		$user = People\CurrentUser::get();
		$directory = $this->getDirectory($user);
		$path = $directory->getPath();

		try {
			$wsMd5 = md5_file($path . '/' . $fileName);

			/* load docfile md5 */
			$dao = DaoFactory::get($spacename)->getDao(\Rbplm\Ged\Docfile\Version::$classId);
			$trans = $dao->getTranslator();
			$r = $dao->query([
				$trans->toSys('md5')
			], $trans->toSys('id') . '=:id', [
				':id' => $docfileId
			]);
			$vaultMd5 = $r->fetchColumn(0);

			if ( $vaultMd5 == $wsMd5 ) {
				$return->addFeedback(0, new ServiceFeedback('unchanged'));
				$return->setData([
					$wsMd5,
					$vaultMd5
				]);
			}
			else {
				$return->addFeedback(0, new ServiceFeedback('changed'));
				$return->setData([
					$wsMd5,
					$vaultMd5
				]);
			}
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError(0, $error);
		}

		return $return;
	}
} /* End of class */
