<?php
namespace Service\Controller\Smartstore;

use Rbplm\People;
use Service\Controller\ServiceException;
use Service\Controller\ServiceError;
use Service\Controller\ServiceController;
use Application\Controller\ControllerException;

/**
 */
class BatchService extends ServiceController
{

	const FROM_WS = 'FROM_WS';

	const FROM_DATA = 'FROM_DATA';

	/**
	 *
	 * @var People\User\Wildspace
	 */
	public $wildspace;

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 */
	public function getWildspace()
	{
		if ( !$this->wildspace ) {
			$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
			//$wildspace->setPath($wildspace->getPath() . '/.rbbatch')->init();
			$this->wildspace = $wildspace;
		}
		return $this->wildspace;
	}

	/**
	 * Check datas and forward to smartstore form page
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 *        	as JSON :
	 *        	@code
	 *        	files{
	 *        	'file[index]':{
	 *        	'name':string,
	 *        	'spacename':string,
	 *        	}}
	 *        	@endcode
	 */
	public function indexService($request = null)
	{
		$return = new \Service\Controller\ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;
		if ( $request->isPost() ) {
			$files = $request->getPost('files', []);
			$containerId = $request->getPost('containerid', null);
			$spacename = $request->getPost('spacename', null);
		}
		else {
			throw new ControllerException(__FUNCTION__ . ' accept only HTTP POST METHOD');
		}

		if ( !($files) ) {
			throw new ServiceException('$files is not set or is not a array', 5030, $this);
		}

		$wsPath = $this->getWildspace()->getPath();
		$checked = array();

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'file1'; isset($files[$index]); $index = 'file' . ++$i) {
			try {
				/* document1 is the current entry in input data */
				$file1 = $files[$index];
				/**@var $fileName string Name of file as must be saved in vault */
				isset($file1['name']) ? $fileName = basename($file1['name']) : $fileName = uniqid();
				/**@var $wsFilename string Name of data file in the ws */
				isset($file1['data']['name']) ? $wsFilename = basename($file1['data']['name']) : $wsFilename = uniqid();

				/* check data type */
				$data = $file1['data']['data'];
				if ( $data != self::FROM_WS ) {
					/* Put data in ws file */
					try {
						$data = base64_decode($data);
						/* check md5 */
						if ( md5($data) != $file1['data']['md5'] ) {
							throw new ServiceException(sprintf('Md5 error'), 5014, $this);
						}
						$wsFile = $wsPath . '/' . $wsFilename;
						file_put_contents($wsFile, $data);
					}
					catch( \Exception $e ) {
						throw new ServiceException(sprintf('Error during file upload of index %s: %s', index, $e->getMessage()), 5013, $this);
					}
				}
				else {
					/* take the existing ws file */
					$wsFile = $wsPath . '/' . $wsFilename;

					/* wait ending of upload */
					while( !file_exists($wsFile) ) {
						sleep(1);
					}
				}

				if ( !is_file($wsFile) ) {
					throw new ServiceException(sprintf('file %s is not in wilspace for index %s', $wsFilename, $index), 5011, $this);
				}

				if ( !$fileName ) {
					throw new ServiceException(sprintf('var $filename is not set for index %s', $index), 5012, $this);
				}

				$checked[] = $wsFilename;
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e);
				$return->addError($index, $error);
				continue;
			}
		} /* forend */

		$location = "/ged/document/smartstore/store?containerid=$containerId&spacename=$spacename";
		foreach( $checked as $i => $v ) {
			$location = $location . "&checked[$i]=$v";
		}
		header("Location: $location");
	}

	/**
	 * WebService only
	 *
	 * Add a file to the wildspace
	 * Input is a json array in POST[files] as
	 * .file1.name
	 * .file1.data.name
	 * .file1.data.md5
	 * .file1.data.size
	 * .file1.data.data
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 *        	<code>
	 *        	files=>JSON
	 *        	** 'file[index]':{
	 *        	** ** 'name':string,
	 *        	** ** 'md5':string,
	 *        	** ** 'size':string,
	 *        	** ** 'data':string BASE64_ENCODED
	 *        	}}
	 *        	</code>
	 *
	 * @return \Service\Controller\ServiceRespons
	 */
	public function uploadService($request = null)
	{
		$return = new \Service\Controller\ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;
		if ( $request->isPost() ) {
			$files = $request->getPost('files', []);
		}
		else {
			throw new ControllerException(__FUNCTION__ . ' accept only HTTP POST METHOD');
		}

		if ( !($files) ) {
			throw new ServiceException('$files is not set or is not a array', 5030, $this);
		}

		$wsPath = $this->getWildspace()->getPath();

		/* ------------- parse the files from input ------------- */
		for ($i = 1, $index = 'file1'; isset($files[$index]); $index = 'file' . ++$i) {
			/* file1 is the current entry in input data */
			$file1 = $files[$index];
			/* set vars */
			$fileName = basename($file1['name']);
			$data = $file1['data']['data'];
			#$size = $file1['size'];
			#$md5 = $file1['md5'];

			if ( !$fileName ) {
				throw new ServiceException(sprintf(tra('file%s name is not setted'), $index));
			}

			try {
				/* Put data in Ws */
				$data = base64_decode($data);
				$toPath = $wsPath . '/' . $fileName;
				file_put_contents($toPath, $data);

				$feedback = new \Service\Controller\ServiceFeedback(sprintf(tra('%s is put in wilspace'), $fileName), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, null);
				$return->addError($index, $error);
				continue;
			}
		}

		return $return;
	}
} /* End of class */
