<?php
namespace Service\Controller\Wildspace;

use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;
use Zend\Http\Request as HttpRequest;

/**
 *
 *
 */
class GraphService extends ServiceController
{

	/**
	 * Get graph from processId
	 *
	 * HTTP GET method
	 *
	 * @param HttpRequest $request
	 *        	<code>
	 *        	files=>JSON
	 *        	** 'file[index]':{
	 *        	** ** 'name':string,
	 *        	** ** 'newname':string,
	 *        	}}
	 *        	</code>
	 *
	 * @return \Service\Controller\ServiceRespons
	 *
	 */
	public function getfromnormalizednameService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;
		
		$normalizedName = $request->getQuery('normalizedname', null);
		
		if ( !($normalizedName) ) {
			throw new ServiceException('normalizedName is not set', 5030, $this);
		}

		try {

			/* put img in public folder */
			/* anti collision name */
			$publicImg = realpath('public/img/Graph568ce3c4d4590');
			if ( $publicImg == "" ) {
				throw new \Exception('public/img/Graph568ce3c4d4590 is not existing :', 5040);
			}

			/* */
			#$processFolderName = $normalizedName();
			#$graph = $publicImg . '/' . $processFolderName . '/' . $normalizedName;
			#$graphUrl = str_replace(getcwd() . '/public/', '', $graph);

			/* */
			#$graphImg = $graphUrl . '.' . \Docflow\Service\Workflow\GraphViz::$imageType;
			#$graphMap = $graphUrl . '.map';
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError(1, $error);
			continue;
		}

		return $return;
	}
} /* End of class */
