<?php
namespace Service\Controller\Import;

use Rbplm\People;
use Service\Controller\Application\AbstractFilemanager;
use Ranchbe;
use Import\Model\Package\Reposit as PackageReposit;

/**
 * Manage files in the user wildspace.
 *
 */
class PackageService extends AbstractFilemanager
{

	/**
	 *
	 * @param People\User $user
	 * @return \Import\Model\Package\Reposit
	 */
	public function getDirectory(People\User $user)
	{
		$path = Ranchbe::get()->getConfig('path.reposit.importpackage');
		$directory = new PackageReposit($path);
		$directory->init();
		return $directory;
	}
} /* End of class */
