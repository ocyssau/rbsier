<?php
namespace Service\Controller\Container;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Org\Container\Favorite;
use Rbplm\Org\Workitem as Container;
use Rbplm\People\CurrentUser;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;

/**
 * Manage bookmarks to users prefered containers.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * containers:[
 * 		'container1':{
 * 		'id':string,
 * 		'spacename':string,
 * 		},
 * 		container2:{...},
 * 		...
 * 	}
 * ]
 * ~~~~~~~~~
 * 
 */
class PreferedService extends ServiceController
{

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Get list of prefered containers for the current user
	 *
	 * HTTP GET method
	 *
	 * @return ServiceRespons
	 *
	 */
	public function indexService()
	{
		$return = new ServiceRespons($this);
		$userId = CurrentUser::get()->getId();
		$factory = DaoFactory::get();
		$favorites = $factory->getDao(Favorite::$classId)->getFromUserId($userId);
		$return->setData($favorites);
		return $return;
	}

	/**
	 * Set a container as prefered by user
	 * 
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * containers:[
	 * 		'container1':{
	 * 		'id':string,
	 * 		'spacename':string,
	 * 		},
	 * 		container2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @return ServiceRespons
	 *
	 */
	public function addService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$containers = $request->getQuery('containers', null);

		if ( !($containers) ) {
			throw new ServiceException('containers is not set or is not a array', 5030, $this);
		}

		$userId = CurrentUser::get()->getId();

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'container1'; isset($containers[$index]); $index = 'container' . ++$i) {
			/* container1 is the current entry in input data */
			$container1 = $containers[$index];
			/* set vars */
			$containerId = $container1['id'];
			$spacename = strtolower($container1['spacename']);

			$factory = DaoFactory::get($spacename);
			$dao = $factory->getDao(Container::$classId);
			$favoriteDao = $factory->getDao(Favorite::$classId);
			$spaceId = $factory->getId();

			try {
				$qstmt = $dao->query([
					$dao->toSys('number'),
					$dao->toSys('description')
				], $dao->toSys('id') . '=:id', [
					':id' => $containerId
				]);
				$fetch = $qstmt->fetch(\PDO::FETCH_NUM);
				$number = $fetch[0];
				$description = $fetch[1];
				$bind = [
					':ownerId' => $userId,
					':containerId' => $containerId,
					':containerNumber' => $number,
					':containerDescription' => $description,
					':spacename' => $spacename,
					':spaceId' => $spaceId
				];
				$favoriteDao->add($bind);

				$feedback = new \Service\Controller\ServiceFeedback(sprintf(tra('%s is set as prefered'), $number), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				if ( strstr($e->getMessage(), '1062 Duplicate entry') ) {
					$feedback = new \Service\Controller\ServiceFeedback(sprintf(tra('%s is yet prefered'), $number), null);
					$return->addFeedback($index, $feedback);
				}
				else {
					$error = new ServiceError($e->getMessage(), $e, null);
					$return->addError($index, $error);
				}
				continue;
			}
		}
		return $return;
	}

	/**
	 * End of love.
	 *
	 * HTTP GET method.
	 * 
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * containers:[
	 * 		'container1':{
	 * 		'id':string,
	 * 		'spacename':string,
	 * 		},
	 * 		container2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @return ServiceRespons
	 *
	 */
	public function removeService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$containers = $request->getQuery('containers', null);

		if ( !($containers) ) {
			throw new ServiceException('containers is not set or is not a array', 5030, $this);
		}

		$userId = CurrentUser::get()->getId();

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'container1'; isset($containers[$index]); $index = 'container' . ++$i) {
			/* container1 is the current entry in input data */
			$container1 = $containers[$index];
			/* set vars */
			$containerId = $container1['id'];
			$spacename = strtolower($container1['spacename']);
			$number = '';

			$factory = DaoFactory::get($spacename);
			$favoriteDao = $factory->getDao(Favorite::$classId);
			$spaceId = $factory->getId();

			try {
				$favoriteDao->remove($userId, $containerId, $spaceId);
				$feedback = new \Service\Controller\ServiceFeedback(sprintf(tra('end of love'), $number), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, null);
				$return->addError($index, $error);
				continue;
			}
		}
		return $return;
	}
} /* End of class */
