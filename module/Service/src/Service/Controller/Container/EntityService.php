<?php
namespace Service\Controller\Container;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;

/**
 * Get and set some datas from one document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	id:string,
 *	spacename:string
 * ]
 * ~~~~~~~~~
 * 
 */
class EntityService extends ServiceController
{

	/**
	 * GET Method
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	id:string,
	 *	spacename:string
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function getfromidService($request = null)
	{
		$return = new ServiceRespons($this);
		$request = $this->getRequest();

		$id = $request->getQuery('id', null);
		$spacename = $request->getQuery('spacename', null);

		if ( !($id) ) {
			throw new ServiceException('id is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5051, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbplm\Org\Workitem $container */
			$container = $factory->getDao(Workitem::$classId)->loadFromId(new Workitem(), $id);
			$this->getAcl()->checkRightFromUid('read', $container->getUid());
			$return->setData($container->getArrayCopy());
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e);
			$return->addError(1, $error);
		}

		return $return;
	}
} /* End of class */
