<?php
namespace Service\Controller\Trash;

use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;

/**
 *
 *
 */
class FileService extends ServiceController
{

	/**
	 * delete a file from the wildspace
	 *
	 * @param string|array $input
	 *        	<code>
	 *        	files=>JSON
	 *        	** 'file[index]':{
	 *        	** ** 'name':string,
	 *        	}}
	 *        	</code>
	 *
	 * @return \Service\Controller\ServiceRespons
	 *
	 */
	public function deleteService($input = null)
	{
		$return = new ServiceRespons($this);
		(!$input) ? $input = $_REQUEST : null;

		isset($input['files']) ? $files = $input['files'] : $files = array();

		if ( !($files) ) {
			throw new ServiceException('files is not set or is not a array', 5030, $this);
		}

		$path = \Ranchbe::get()->getConfig('path.reposit.trash');

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'file1'; isset($files[$index]); $index = 'file' . ++$i) {
			/* file1 is the current entry in input data */
			$file1 = $files[$index];
			/* set vars */
			$fileName = basename($file1['name']);
			$filePath = $path . '/' . $fileName;

			try {
				if ( !$fileName ) {
					throw new ServiceException(sprintf(tra('file %s name is not setted'), $index));
				}

				if ( is_file($filePath) == false ) {
					throw new ServiceException(sprintf(tra('%s is not a file'), $fileName));
				}

				if ( !@unlink($filePath) ) {
					$err = error_get_last();
					throw new ServiceException(sprintf('Unable to delete file %s. Error %s', $filePath, $err['message']));
				}
				$feedback = new \Service\Controller\ServiceFeedback(sprintf(tra('%s is put in trash'), $fileName), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, null);
				$return->addError($index, $error);
				continue;
			}
		}
		return $return;
	}
} /* End of class */
