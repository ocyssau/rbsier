<?php
namespace Service\Controller\Metadata;

use Rbs\Space\Factory as DaoFactory;
use Service\Controller\ServiceController;

/**
 * Get/Set properties of Metadata.
 *
 */
class ServiceService extends ServiceController
{

	/**
	 * Get the extended properties associated to container.
	 *
	 * HTTP:GET method
	 *
	 * @param $_GET['containerid'] int 
	 * @param $_GET['spacename'] string
	 *        	Spacename of the object to get. Permited values are
	 *        	- 'workitem'
	 *        	- 'bookshop'
	 *        	- 'cadlib'
	 *        	- 'mockup'
	 *        
	 * @return string json string
	 */
	function getfromcontaineridService()
	{
		$request = $this->getRequest();

		/* get the q parameter from URL */
		$containerId = $request->getQuery('containerid', null);
		$spacename = strtolower($request->getQuery('spacename', null));

		$select = array();
		foreach( \Rbs\Extended\PropertyDao::$sysToApp as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}

		$factory = DaoFactory::get($spacename);
		$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Property::$classId);
		$extended = $lnkDao->getChildren($containerId, $select)->fetchAll();

		/* output the response */
		echo json_encode($extended);
	}

	/**
	 * Get the extended properties associated to document.
	 *
	 * HTTP:GET method
	 *
	 * @param $_GET['documentid'] int 
	 * @param $_GET['spacename'] string
	 *        	Spacename of the object to get. Permited values are
	 *        	- 'workitem'
	 *        	- 'bookshop'
	 *        	- 'cadlib'
	 *        	- 'mockup'
	 *        
	 * @return string json string
	 */
	function getfromdocumentidService()
	{
		$request = $this->getRequest();

		/* get the q parameter from URL */
		$documentId = $request->getQuery('documentid', null);
		$spacename = strtolower($request->getQuery('spacename', null));

		$factory = DaoFactory::get($spacename);
		$table = $spacename . '_metadata';
		$relTable = $table . '_rel';
		$docTable = $spacename . '_documents';

		$select = array();
		foreach( \Rbs\Extended\PropertyDao::$sysToApp as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}
		$connexion = $factory->getConnexion();

		$sql = "
		SELECT $select
		FROM $docTable as parent
		JOIN $relTable as link ON link.container_id=parent.container_id
		JOIN $table as child ON link.property_id=child.id
		WHERE parent.id=:documentId";
		$stmt = $connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute(array(
			':documentId' => $documentId
		));
		$extended = $stmt->fetchAll();

		/* output the response */
		echo json_encode($extended);
	}
}
