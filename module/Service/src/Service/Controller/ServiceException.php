<?php
namespace Service\Controller;

class ServiceException extends \Exception
{

	/**
	 * Data is object concerned by exception
	 *
	 * @var \stdClass
	 */
	protected $data;

	/**
	 * @param string $message
	 * @param string $code
	 * @param mixed $data
	 */
	function __construct($message, $code = null, $data = null)
	{
		parent::__construct($message, $code);
		$this->data = $data;
	}

	/**
	 *
	 */
	public function getData()
	{
		return $this->data;
	}
}
