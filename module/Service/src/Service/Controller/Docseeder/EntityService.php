<?php
namespace Service\Controller\Docseeder;

use Rbs\Space\Factory as DaoFactory;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;
use Docseeder\Model\Docseeder;
use Rbplm\People;
use Rbplm\Ged\Doctype as Type;
use Rbplm\Ged\Document;

/**
 * Get and set some datas from one document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	id:string,
 *	spacename:string
 * ]
 * ~~~~~~~~~
 * 
 */
class EntityService extends ServiceController
{

	public $docseeder;

	/**
	 * GET Method
	 * Build a new ref, reserve it in db, and return in respons.
	 * docseeder property may be set to $request->docseeder property, if not a new Docseeder instance will be create.
	 * The docseeder instance is exposed in this->docseeder property
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	spacename:string,
	 *	containerid:int,
	 *  doctypeid:int,
	 *  description:string,
	 *  label:string
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function createService($request = null)
	{
		$return = new ServiceRespons($this);
		$request = $this->getRequest();

		if ( isset($request->docseeder) ) {
			$docseeder = $request->docseeder;
			if ( !($docseeder instanceof Docseeder) ) {
				throw new ServiceException('docseeder is not a Docseeder\Model\Docseeder type', 5060, $this);
			}
		}
		else {
			$docseeder = Docseeder::init();
		}

		/* The docseeder instance is exposed by service object */
		$this->docseeder = $docseeder;

		/**/
		$spacename = $request->getPost('spacename', $docseeder->spacename);
		$containerId = $request->getPost('containerid', $docseeder->containerId);
		$doctypeId = $request->getPost('doctypeid', $docseeder->typeId);
		$description = $request->getPost('description', $docseeder->description);
		$label = $request->getPost('label', $docseeder->label);

		if ( !($containerId) ) {
			throw new ServiceException('containerid is not set', 5051, $this);
		}
		if ( !($doctypeId) ) {
			throw new ServiceException('doctypeid is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5051, $this);
		}

		try {
			/* Inits helpers */
			$workitemFactory = DaoFactory::get($spacename);

			/* Init some work */
			$user = People\CurrentUser::get();
			$docseeder->setUser($user);

			/* load workitem */
			$container = new \Rbplm\Org\Workitem();
			$workitemFactory->getDao($container->cid)->loadFromId($container, $containerId);
			$docseeder->setContainer($container);

			/* Check permissions */
			$this->getAcl()->checkRightFromUid('create', $container->getUid());

			/* load project */
			$project = new \Rbplm\Org\Project();
			$workitemFactory->getDao($project->cid)->loadFromId($project, $container->parentId);
			$container->setParent($project);

			/* load type */
			$type = new Type();
			$workitemFactory->getDao($type->cid)->loadFromId($type, $doctypeId);
			$docseeder->setType($type);

			/* get a number from rule */
			$generatorClass = $type->getNumberGeneratorClass();
			if ( !$generatorClass ) {
				$generatorClass = \Ranchbe::get()->getConfig('document.number.generator');
			}
			$generator = new $generatorClass($docseeder);
			$number = $generator->getNumber();

			/* save the document, without file */
			$numberAsString = $number->__toString();
			$document = Document\Version::init($numberAsString)->setNumber($numberAsString);
			$document->setParent($container);
			$document->setDoctype($type);
			$document->description = $description;
			$document->setLabel($label);
			$workitemFactory->getDao($document->cid)->save($document);

			/* set document with docseeder */
			$docseeder->setDocument($document);
			$return->setData($document->getArrayCopy());
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e);
			$return->addError(1, $error);
		}

		return $return;
	}
} /* End of class */
