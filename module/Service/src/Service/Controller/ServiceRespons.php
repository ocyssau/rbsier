<?php
namespace Service\Controller;

/**
 * Service respons return by all Service objects.
 * 
 * This class is used to collects all messages and datas during the execution of service.
 * It return to end user as JSON datas with structure like :
 * 
 * ~~~~~~~~~~~~~~~~~{.json}
 * [
 * 		errors:[],
 * 		feedbacks:[],
 * 		exception:{},
 * 		data:{}
 * ]
 * ~~~~~~~~~~~~~~~~~
 * 
 * - If errors is populated, the a fatal error is occured. 
 * - It may be too have a exception associated in exception key.
 * - Feddbacks contains only warnings or informations messages.
 * - If somes datas are attempted, it may found in data key.
 */
class ServiceRespons implements \JsonSerializable
{

	/**
	 *
	 * @var \Service\Controller\ServiceController
	 */
	protected $controller;

	/**
	 *
	 * @var array
	 */
	protected $children;

	/**
	 *
	 * @var array
	 */
	protected $errors;

	/**
	 *
	 * @var array
	 */
	protected $feedbacks;

	/**
	 */
	protected $exception;

	/**
	 *
	 * @var array
	 */
	protected $data;

	/**
	 *
	 * @var integer
	 */
	protected $httpErrorCode;

	/**
	 *
	 * @var integer
	 */
	public $feedbackCount = 0;

	/**
	 *
	 * @var integer
	 */
	public $errorCount = 0;

	/**
	 *
	 * @param \Service\Controller\ServiceController $controller        	
	 */
	public function __construct($controller = null)
	{
		$this->controller = $controller;
		$this->errors = [];
		$this->feedbacks = [];
		$this->exception = null;
	}

	/**
	 * @implements JsonSerializable
	 */
	public function jsonSerialize()
	{
		$ret = array(
			'data' => [],
			'errors' => [],
			'feedbacks' => [],
			'exception' => null
		);
		
		if ( is_array($this->data) ) {
			$data = null;
			$k = null;
			foreach( $this->data as $k => $data ) {
				if ( $data instanceof \Rbplm\Any ) {
					$ret['data'][$k] = $data->getArrayCopy();
				}
				else {
					$ret['data'][$k] = $data;
				}
			}
		}
		
		foreach( $this->errors as $error ) {
			$ret['errors'][] = $error->jsonSerialize();
		}
		
		foreach( $this->feedbacks as $fb ) {
			$ret['feedbacks'][] = $fb->jsonSerialize();
		}
		
		$ret['exception'] = ($this->exception) ? self::exceptionToArray($this->exception, JSON_FORCE_OBJECT) : null;

		return $ret;
	}

	/**
	 *
	 * @param \Exception $e        	
	 */
	private static function exceptionToArray(\Exception $e)
	{
		$return = [
			'message' => $e->getMessage(),
			'code' => $e->getCode(),
			'file' => $e->getFile(),
			'line' => $e->getLine()
		];
		return $return;
	}

	/**
	 * 
	 * @param integer $int
	 * @return ServiceRespons
	 */
	public function setHttpErrorCode($int = null)
	{
		$this->httpErrorCode = $int;
		return $this;
	}

	/**
	 *
	 * @param integer $int
	 * @return $int
	 */
	public function getHttpErrorCode()
	{
		return $this->httpErrorCode;
	}

	/**
	 *
	 * @param \Rbs\Ged\Document\Service $respons        	
	 */
	public function bindServiceRespons($index, $respons)
	{
		/* transmit feedbacks to Datamanager */
		if ( $respons->hasFeedbacks() ) {
			foreach( $respons->getFeedbacks() as $msg ) {
				$feedback = new \Service\Controller\ServiceFeedback($msg, null);
				$this->addFeedback($index, $feedback);
			}
		}
		
		if ( $respons->hasErrors() ) {
			foreach( $respons->getErrors() as $msg ) {
				$error = new \Service\Controller\ServiceError($msg, null, null);
				$this->addError($index, $error);
			}
		}
	}

	/**
	 *
	 * @param \Exception $e        	
	 * @return ServiceRespons
	 */
	public function setException($e)
	{
		$this->exception = $e;
		return $this;
	}

	/**
	 *
	 * @return \Exception
	 */
	public function getException()
	{
		return $this->exception;
	}

	/**
	 *
	 * @param string $index        	
	 * @param ServiceRespons $return        	
	 * @return ServiceRespons
	 */
	public function addChild($index, ServiceRespons $child)
	{
		$this->children[$index] = $child;
		return $this;
	}

	/**
	 *
	 * @param string $index        	
	 * @return ServiceRespons
	 *
	 */
	public function getChild($index)
	{
		return $this->children[$index];
	}

	/**
	 *
	 * @param string $index        	
	 * @param ServiceFeedback $feedback        	
	 * @return ServiceRespons
	 */
	public function addFeedback($index, ServiceFeedback $feedback)
	{
		$this->feedbacks[$index] = $feedback;
		$this->feedbackCount++;
		return $this;
	}

	/**
	 *
	 * @param string $index        	
	 * @return boolean;
	 */
	public function hasFeedback($index)
	{
		return (isset($this->feedbacks[$index]));
	}

	/**
	 *
	 * @param string $index        	
	 * @return ServiceFeedback;
	 */
	public function getFeedback($index)
	{
		return $this->feedbacks[$index];
	}

	/**
	 *
	 * @return boolean;
	 */
	public function hasFeedbacks()
	{
		return (count($this->feedbacks) > 0);
	}

	/**
	 *
	 * @return array;
	 */
	public function getFeedbacks()
	{
		return $this->feedbacks;
	}

	/**
	 *
	 * @param string $index        	
	 * @param ServiceError $exception        	
	 * @return ServiceRespons
	 */
	public function addError($index, ServiceError $exception)
	{
		$this->errors[$index] = $exception;
		$this->errorCount++;
		return $this;
	}

	/**
	 *
	 * @param string $index        	
	 * @return boolean;
	 */
	public function hasError($index)
	{
		return (isset($this->errors[$index]));
	}

	/**
	 *
	 * @param string $index        	
	 * @return ServiceError;
	 */
	public function getError($index)
	{
		return $this->errors[$index];
	}

	/**
	 *
	 * @return boolean;
	 */
	public function hasErrors()
	{
		return (count($this->errors) > 0);
	}

	/**
	 *
	 * @return array;
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 *
	 * @param array $data        	
	 * @return ServiceRespons
	 */
	public function setData($data)
	{
		$this->data = $data;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}
}
