<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Org\Workitem;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceException;
use Rbs\Ged\Document\Event;

/**
 * Manage doctype of the document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * documents:[
 * 		document1:{
 * 			id:string,
 * 			spacename:string
 * 		},
 * 		document2:{...},
 * 		...
 * 	}
 * ]
 * ~~~~~~~~~
 *
 */
class DoctypeService extends AbstractController
{

	const SIGNAL_POST_RESETDOCTYPE = 'resetdoctype.post';

	const SIGNAL_PRE_RESETDOCTYPE = 'resetdoctype.pre';

	/**
	 * Recalculate the doctype of the input documents.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request $request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @return ServiceRespons
	 *
	 */
	public function resetService($request = null)
	{
		$return = new ServiceRespons($this);
		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/** @var $document1 array is the current entry in input datas */
			$document1 = $documents[$index];
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];

			try {
				/* load helpers */
				$factory = DaoFactory::get($spacename);
				$dao = $factory->getDao(Document\Version::$classId);
				$dfDao = $factory->getDao(Docfile\Version::$classId);

				/* load work objects */
				$document = new Document\Version();
				$dao->loadFromId($document, $documentId);
				/* load docfiles */
				$dfDao->loadDocfiles($document);

				$containerId = $document->getParent(true);
				$container = $factory->getDao(Workitem::$classId)->loadFromId(new Workitem(), $containerId);
				$document->setParent($container);

				/* Check rights */
				$this->getAcl()->checkRightFromUid('edit', $container->getUid());

				/* */
				if ( $docfiles = $document->getDocfiles() ) {
					$mainData = array_shift($docfiles)->getData();
					$fileExtension = $mainData->extension;
					$fileType = $mainData->type;
				}
				else {
					$fileExtension = NULL;
					$fileType = 'nofile';
				}

				try {
					$doctype = new \Rbplm\Ged\Doctype();
					$factory->getDao($doctype::$classId)->loadFromDocument($doctype, $document->getNumber(), $fileExtension, $fileType);
					$event = new Event(Event::PRE_SETDOCTYPE, $document, $factory);
					$event->doctypeIdBefore = $document->doctypeId;
					$event->doctypeAfter = $doctype;
					$this->getEventManager()->trigger($event);
					$document->setDoctype($doctype);
					$dao->save($document);
					$this->getEventManager()->trigger($event->setName(Event::POST_SETDOCTYPE));

					$feedback = new \Service\Controller\ServiceFeedback(sprintf(tra('%s is a %s'), $document->getUid(), $doctype->getName()), $document);
					$return->addFeedback($index, $feedback);
				}
				catch( \Rbs\Ged\Doctype\NotFoundException $e ) {
					$feedback = new \Service\Controller\ServiceFeedback(sprintf(tra('None doctypes found for document %s'), $document->getUid()), $document);
					$return->addFeedback($index, $feedback);

					$document->doctypeId = null;
					$document->doctypeUid = null;
					$dao->save($document);
				}
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage() . $fileExtension . $fileType, $e, $document);
				$return->addError($index, $error);
				continue;
			}
		} /* forend */

		return $return;
	}
}/* End of class */
