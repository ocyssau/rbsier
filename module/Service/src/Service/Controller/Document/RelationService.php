<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\AccessCodeException;
use Rbplm\Ged\Document;
use Service\Controller\ServiceError;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;
use Rbplm\Sys\Error;

/**
 * Manage links between documents.
 *
 */
class RelationService extends ServiceController
{

	/**
	 * Remove sons of documents
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		link1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 		},
	 * 		link2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function removelinkService($request = null)
	{
		$return = new ServiceRespons($this);
		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5050, $this);
		}

		/* ------------- parse the links from input ------------- */
		for ($i = 1, $index = 'link' . $i; isset($documents[$index]); $index = 'link' . ++$i) {
			/* document1 is the current entry in input data */
			$link1 = $documents[$index];
			/* set vars */
			$linkId = $link1['id'];
			$spacename = $link1['spacename'];

			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Document\LinkDao $dao */
			$dao = $factory->getDao(Document\Link::$classId);

			/*
			 * @todo :check right
			 */
			try {
				$dao->deleteFromId($linkId);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new ServiceFeedback('');
			$return->addFeedback($index, $feedback);
		} /* forend */

		return $return;
	}

	/**
	 * Add as child
	 * 
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 			children:[
	 * 				child1:{
	 * 					id:string,
	 * 					spacename:string,
	 * 				}
	 * 				child2:{...},
	 * 				...
	 * 			]
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function addchildrenService($request = null)
	{
		$return = new ServiceRespons($this);
		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5050, $this);
		}

		/* @todo: relation between documents with heterogen space */
		$dao = DaoFactory::get('workitem')->getDao(Document\Link::$classId);

		/* ------------- parse the links from input ------------- */
		for ($i = 1, $index = 'document' . $i; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];
			$children = $document1['children'];

			try {
				/* init some objects */
				$factory = DaoFactory::get($spacename);
				$document = $factory->getDao(Document\Version::$classId)->loadFromId(new Document\Version(), $documentId);
				$document->spaceId = $factory->getId();

				$aCode = $document->checkAccess();
				if ( $aCode >= 100 ) {
					throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR);
				}

				/* Check rights */
				$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

				/* ------------- parse the children from input ------------- */
				for ($j = 1, $index = 'child1'; isset($children[$index]); $index = 'child' . ++$j) {
					$child1 = $children[$index];

					isset($child1['id']) ? $childId = $child1['id'] : $childId = null;
					isset($child1['uid']) ? $childUid = $child1['uid'] : $childUid = null;
					isset($child1['spacename']) ? $childSpacename = $child1['spacename'] : $childSpacename = $spacename;
					isset($child1['spaceId']) ? $childSpaceId = $child1['spaceId'] : $childSpaceId = DaoFactory::get()->getIdFromName($childSpacename);

					$docLink = new Document\Link();
					$docLink->setParent($document);
					$docLink->hydrate(array(
						'childId' => $childId,
						'childUid' => $childUid,
						'childSpaceId' => $childSpaceId,
						'childSpacename' => $childSpacename,
						'childCid' => Document\Version::$classId
					));

					try {
						$dao->save($docLink);
					}
					catch( \Exception $e ) {
						$error = new ServiceError($e->getMessage(), $e, $docLink);
						$return->addError($index, $error);
						continue;
					}

					$feedback = new ServiceFeedback('doclink is created', $docLink);
					$return->addFeedback($index, $feedback);
				} /* forend */
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}
			$feedback = new ServiceFeedback('', $document);
			$return->addFeedback($index, $feedback);
		} /* forend */

		return $return;
	}
} /* End of class */
