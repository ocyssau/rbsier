<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbs\Postit\Postit;
use Rbplm\People\CurrentUser;
use Service\Controller\ServiceError;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;

/**
 * 
 * Add, remove postits associated to documents.
 *
 */
class PostitService extends ServiceController
{

	/**
	 * Add a postit to documents.
	 * 
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:integer,
	 * 			spacename:string,
	 * 			postit:string
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * document1.postit must contains string. Its the text of postit that must UTF8 encoded.
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function addService($request = null)
	{
		$return = new ServiceRespons($this);

		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5040, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];
			$body = $document1['postit'];

			if ( $body == '' ) {
				$error = new ServiceError('Postit is empty', null, null);
				$return->addError($index, $error);
				return $return;
			}

			/* load some helpers */
			$factory = DaoFactory::get($spacename);

			$postit = new Postit();
			/** @var \Rbs\Postit\PostitDao $dao */
			$dao = $factory->getDao($postit->cid);

			try {
				$postit->newUid();
				$postit->parentId = $documentId;
				$postit->parentCid = Document\Version::$classId;
				$postit->spacename = $spacename;
				$postit->setOwner(CurrentUser::get());
				$postit->setBody($body);
				$dao->save($postit);

				$feedback = new ServiceFeedback('Postit is added', $postit);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $postit);
				$return->addError($index, $error);
				continue;
			}
		}

		return $return;
	}

	/**
	 * Remove the postits.
	 *        
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request $request
	 * ~~~~~~~~~{.json}
	 * postits:[
	 * 		postit1:{
	 * 			id:integer,
	 * 		},
	 * 		postit2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function deleteService($request = null)
	{
		$return = new ServiceRespons($this);

		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$postits = $request->getPost('postits', null);

		if ( !($postits) ) {
			throw new ServiceException('postits is not set or is not a array', 5040, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'postit1'; isset($postits[$index]); $index = 'postit' . ++$i) {
			/* postit1 is the current entry in input data */
			$postit1 = $postits[$index];
			/* set vars */
			$postitId = $postit1['id'];

			/* load some helpers */
			$factory = DaoFactory::get('default');

			/** @var \Rbs\Postit\PostitDao $dao */
			$dao = $factory->getDao(Postit::$classId);

			try {
				$dao->deleteFromId($postitId);
				$feedback = new ServiceFeedback('Postit is deleted', null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, null);
				$return->addError($index, $error);
				continue;
			}
		}

		return $return;
	}
}
