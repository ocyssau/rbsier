<?php
namespace Service\Controller\Document;

use Service\Controller\ServiceController;
use Rbs\EventDispatcher\Dispatcher;
use Rbs\EventDispatcher\EventManager;
use Rbs\Ged\Document\ListenerProviderFactory;

/** 
 * @author olivier
 * 
 */
abstract class AbstractController extends ServiceController
{

	/**
	 *
	 * @var EventManager
	 */
	public $eventManager;

	/**
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * @param EventManager $eventManager
	 */
	public function setEventManager(EventManager $eventManager)
	{
		$this->eventManager = $eventManager;
		return $this;
	}

	/**
	 *
	 * @return EventManager
	 */
	public function getEventManager()
	{
		if ( !isset($this->eventManager) ) {
			$this->eventManager = new EventManager(new Dispatcher([
				ListenerProviderFactory::get()
			]));
		}
		return $this->eventManager;
	}
}
