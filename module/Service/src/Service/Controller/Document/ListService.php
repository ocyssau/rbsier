<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;
use Rbplm\Dao\Filter\Op;

/**
 * Get and set some datas from one document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	id:string,
 *	spacename:string
 * ]
 * ~~~~~~~~~
 * 
 */
class ListService extends ServiceController
{

	/**
	 * 
	 * @var \Rbs\Dao\Sier\Filter
	 */
	protected $filter;

	/**
	 * 
	 * @var \Rbs\Dao\Sier\DaoList
	 */
	protected $list;

	/**
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 *        	<code>
	 *        	** 'select':array 	select the main table columns
	 *        	** 'limit':int 		number of rows to return
	 *        	** 'page':int 		from page number
	 *        	** 'filter:array 	of array where 0 is name of var, 1 is value, 3 is operator as define by constants of \Rbplm\Dao\Filter\Op
	 *        	** 'search:array 	words to be search in full text research
	 *        	** withdoctype: boolean if true, join doctype table. In filter, alias dt may be used
	 *        	** dtselect: array, if option withdoctype is true, columns of doctypes table may be select here. The result will be automatically prefix with "dt"
	 *        
	 *        	Examples:
	 *        			get the 100 first documents
	 *        			.../search?limit=100
	 *        
	 *        			get document with id=1
	 *        			.../search?filter[0][0]=id&filter[0][1]=2&filter[0][2]=EQUAL
	 *        
	 *        			Join doctypes to result:
	 *        			.../search?limit=100&withdoctype=1
	 *        			
	 *        			Select columns name of table doctype and column number of document
	 *        			.../search?withdoctype=1&select[]=name&select[]=number
	 *        
	 *        			Select columns name of table doctype and column name of document
	 *        			.../search?withdoctype=1&select[]=name&dtselect[]=name
	 *        				=> return ['name'=>'document1', 'dtname'=>'text__msword']
	 *        
	 *        
	 *        	</code>
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter [OPTIONAL] Filter object to inject in List
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function searchService($request = null, $filter = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$spacename = $request->getQuery('spacename', null);
		$page = $request->getQuery('page', 1);
		$limit = $request->getQuery('limit', 1000);
		$userSelect = $request->getQuery('select', null);
		$userFilter = $request->getQuery('filter', null);
		$searchs = $request->getQuery('search', null);
		$withDoctype = (bool)$request->getQuery('withdoctype', false);
		$dtSelect = $request->getQuery('dtselect', null);
		$this->filter = $filter;
		
		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);
			$list = $this->getList($factory);
			$filter = $this->getFilter($factory);
			$dao = $list->dao;
			$trans = $factory->getTranslator($dao);

			/**/
			$alias = 'doc';
			$filter->setAlias($alias);
			if ( $userSelect ) {
				$select = [];
				foreach( $userSelect as $asApp ) {
					if ( $asSys = $trans->toSys($asApp) ) {
						$select[] = $alias . '.' . $asSys . ' AS ' . $asApp;
					}
					else {
						throw new ServiceException(sprintf('Selection of %s impossible', $asApp));
					}
				}
				$filter->select($select);
			}
			else {
				$select = $trans->getSelectAsApp($alias);
				$filter->select($select);
			}

			/**/
			if ( $userFilter ) {
				$i = 0;
				foreach( $userFilter as $f ) {
					if ( count($f) != 3 ) {
						throw new ServiceException(sprintf('Filter %s must have tree item: name, value, op', var_export($f, true)));
					}
					$asApp = $f[0];
					if ( $asSys = $trans->toSys($asApp) ) {
						$op = constant('\Rbplm\Dao\Filter\Op::' . strtoupper($f[2]));
						$what = $f[1];
						$filter->andfind($what, $alias . '.' . $asSys, $op);
					}
					else {
						throw new ServiceException(sprintf('Filter %s is not correctly formated', var_export($f, true)));
					}
					$i++;
				}
			}

			/**/
			if ( $searchs ) {
				$i = 0;
				foreach( $searchs as $term ) {
					$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, `%s`, '-')", $trans->toSys('name'), $trans->toSys('description'), $trans->toSys('number'));
					$filter->andFind($term, $concat, Op::OP_CONTAINS);
				}
			}

			if ( $withDoctype ) {
				if ( $dtSelect ) {
					$s = [];
					foreach( $dtSelect as $col ) {
						$s[] = $col . ' as dt' . $col;
					}
				}
				else {
					$s = [
						'name as dtname',
						'number as dtnumber',
						'icon as icon'
					];
				}
				$filter->with([
					'table' => 'doctypes',
					'righton' => 'id',
					'lefton' => 'doctype_id',
					'alias' => 'dt',
					'select' => $s
				]);
			}

			$filter->page($page, $limit);
			$list->load($filter);
			$return->setData($list->toArray());
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * @param DaoFactory $factory
	 * @return \Rbs\Dao\Sier\Filter
	 */
	protected function getFilter(DaoFactory $factory)
	{
		if ( !$this->filter ) {
			$this->filter = $factory->getFilter(Document\Version::$classId);
		}
		return $this->filter;
	}

	/**
	 * @param DaoFactory $factory
	 * @return \Rbs\Dao\Sier\DaoList
	 */
	protected function getList(DaoFactory $factory)
	{
		if ( !$this->list ) {
			$this->list = $factory->getList(Document\Version::$classId);
		}
		return $this->list;
	}
} /* End of class */
