<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Ranchbe;
use Rbplm\Ged\Document;
use Service\Controller\ServiceException;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Rbs\Ged\Document\Event;

/**
 * Manager of documents.
 *
 */
class ManagerService extends AbstractController
{

	/**
	 * Update documents metadata with datas send in input.
	 *
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 			name:string,
	 * 			number:string,
	 * 			description:string,
	 * 			status:string,
	 * 			...And all others valids properties
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @see \Rbplm\Ged\Document\Version for all others valid properties
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function editService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5050, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document' . $i; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];

			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);

			try {
				$document = $dao->loadFromId(new Document\Version(), $documentId);

				/* Check permission */
				$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

				$this->getEventManager()->trigger(new Event(Event::PRE_EDIT, $document, $factory));
				$document->dao->save($document);
				$this->getEventManager()->trigger(new Event(Event::POST_EDIT, $document, $factory));
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new ServiceFeedback('', $document);
			$return->addFeedback($index, $feedback);
		} /* forend */

		return $return;
	}

	/**
	 * Create documents from inputs properties.
	 * 
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			spacename:string,
	 * 			name:string,
	 * 			number:string,
	 * 			description:string,
	 * 			status:string,
	 * 			...And all others valids properties
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @see \Rbplm\Ged\Document\Version for all others valid properties
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function createService($request = null)
	{
		throw new \Exception('NOT IMPLEMENTED');

		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		//$documents = $request->getPost('documents', null);

		/* Check permission */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$factory = DaoFactory::get($this->spacename);
		$dao = $factory->getDao(Document\Version::$classId);

		$document = Document\Version::init();

		try {
			$dao->save($document);
			return $this->redirect($this->ifSuccessForward, array(
				'spacename' => $this->spacename,
				'containerid' => $this->containerId
			));
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
		}

		return $return;
	}

	/**
	 * Delete documents and all associated files.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:integer,
	 * 			spacename:string,
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function deleteService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5050, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document' . $i; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];

			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);
			$conn = $factory->getConnexion();

			$conn->beginTransaction();
			$document = new Document\Version();
			try {
				$dao->loadFromId($document, $documentId);

				/* Check permission */
				$this->getAcl()->checkRightFromUid('delete', $document->parentUid);

				$service->delete($document);

				$conn->commit();
			}
			catch( \Exception $e ) {
				$conn->rollBack();
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}
		} /*Forend */

		return $return;
	}

	/**
	 * Set this documents as fixed on current user document manager.
	 * 
	 * 
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:integer,
	 * 			spacename:string,
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function fixService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5050, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document' . $i; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];

			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];
			
			$context = \Ranchbe::get()->getServiceManager()->getContext();
			$context->setData('fixedDocumentId', $documentId);
			$context->setData('fixedDocumentSpacename', $spacename);
			$context->saveInSession()->getDao()->save($context);
		}

		$feedback = new ServiceFeedback('Fixed', null);
		$return->addFeedback($index, $feedback);

		return $return;
	}

	/**
	 * Unfix this documents of the current user document manager.
	 *
	 * HTTP POST method
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function unfixService()
	{
		$return = new ServiceRespons($this);

		$context = \Ranchbe::get()->getServiceManager()->getContext();
		$context->setData('fixedDocumentId', null);
		$context->setData('fixedDocumentSpacename', null);
		$context->saveInSession()->getDao()->save($context);
		
		$feedback = new ServiceFeedback('UnFixed', null);
		$return->addFeedback(1, $feedback);

		return $return;
	}
} /* End of class */
