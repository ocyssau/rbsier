<?php
namespace Service\Controller\Document;

use Ranchbe;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\AccessCodeException;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;
use Rbplm\Ged\AccessCode;
use Service\Controller\ServiceException;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;

/**
 * 
 * Lock manager for documents.
 * 
 * 
 *
 */
class LockmanagerService extends AbstractController
{

	/**/
	const FROM_WS = 'FROM_WS';

	/**/
	const FROM_DATA = 'FROM_DATA';

	/**
	 * Checkout the documents and associated files.
	 * 
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request $request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			object: Document\Version [optional],
	 * 			id:string,
	 * 			spacename:string,
	 * 			replace:boolean,
	 * 			getfile:boolean
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * If document1.replace is true, replace existings files in wildspace. Else, return Exception.
	 * If document1.getfile is true, copy files to wildspace.
	 * If document1.object is only for internal use.
	 *
	 * @throws ServiceException
	 * @return ServiceRespons with data populate with docfiles datas
	 */
	public function checkoutService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);
		if ( !$documents ) {
			$documentId = $request->getPost('id', null);
			$spacename = $request->getPost('spacename', null);
			$replace = $request->getPost('replace', null);
			$getfile = $request->getPost('getfile', null);
			if ( $documentId && $spacename ) {
				$documents = [
					'document1' => [
						'id' => $documentId,
						'spacename' => $spacename,
						'replace' => $replace,
						'getfile' => $getfile
					]
				];
			}
		}

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5050, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document' . $i; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			isset($document1['id']) ? $documentId = $document1['id'] : $documentId = null;
			isset($document1['spacename']) ? $spacename = $document1['spacename'] : $spacename = null;
			isset($document1['replace']) ? $replace = (boolean)$document1['replace'] : $replace = false;
			isset($document1['getfiles']) ? $getFiles = (boolean)$document1['getfiles'] : $getFiles = true;
			if ( isset($document1['object']) ) {
				$document = $document1['object'];
			}
			else {
				$document = null;
			}

			/* init some objects */
			$factory = DaoFactory::get($spacename);

			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);
			$service->wildspace = Wildspace::get(CurrentUser::get());

			/* Docfile service */
			$dfService = Ranchbe::get()->getServiceManager()->getDocfileService($factory);

			/* returned data */
			$datas = [];

			try {
				if ( !$document ) {
					/** @var \Rbs\Ged\Document\VersionDao $dao */
					$dao = $factory->getDao(Document\Version::$classId);
					$document = $dao->loadFromId(new Document\Version(), $documentId);
				}
				$containerId = $document->getParent(true);

				/* check access */
				$aCode = $document->checkAccess();
				if ( ($aCode > AccessCode::FREE) || ($aCode < AccessCode::FREE || $aCode === AccessCode::CHECKOUT) ) {
					throw new AccessCodeException(sprintf(tra('this document is locked with code %s, %s is not permit'), $aCode, 'Checkout'));
				}

				/* load docfiles */
				/* Checkout only MAIN, ANNEX, FRAGMENT and all other with role < 64 */
				/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
				$dfDao = $factory->getDao(Docfile\Version::$classId);
				$dfDao->loadDocfiles($document, $dfDao->getTranslator()->toSys('mainrole') . ' <= ' . Docfile\Role::CONVERT);

				/* load container */
				$container = $factory->getLoader()->loadFromId($containerId, Workitem::$classId);
				$document->setParent($container);

				/* checkout each docfile, 
				 * independantly of document service to be able to have atomic checkout, 
				 * when one df is failed, continue to others 
				 */
				foreach( $document->getDocfiles() as $docfile ) {
					/* @var Docfile\Version $docfile */
					if ( $docfile->checkAccess() !== AccessCode::FREE ) {
						continue;
					}

					$docfile->setParent($document);
					try {
						$dfService->checkOut($docfile, $replace, $getFiles);
						$data = $docfile->getArrayCopy();
						foreach( $data as $k => $v ) {
							if ( !is_scalar($v) ) {
								unset($data[$k]);
							}
						}
						$datas[] = $data;
					}
					catch( \Exception $e ) {
						$error = new ServiceError('error during checkout of docfile', $e, $docfile);
						$return->addError($index, $error);
						continue;
					}
				} /* foreach docfiles */

				try {
					$service->checkOut($document, false);
				}
				catch( \Exception $e ) {
					$error = new ServiceError('error during checkout of document', $e, $document);
					$return->addError($index, $error);
					continue;
				}
			}
			catch( AccessCodeException $e ) {
				$error = new ServiceError('Access Code Error', $e, $document);
				$return->addError($index, $error);
				continue;
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new ServiceFeedback('checkout success', $document);
			$return->addFeedback($index, $feedback);
		} /* forend */

		$return->setData($datas);
		return $return;
	}

	/**
	 * Checkin documents and associated files.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 			comment:string,
	 * 			checkinandkeep:boolean
	 * 			pdmdata:json
	 * 			pdmdatafile:string filename in working dir with pdmDataSet definition as content
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * document1.checkinandkeep If true, vault files and document are updated with new datas, but all is keep checkouted by user.
	 * document1.comment To send a comment and explain the work
	 * document1.pdmdata @see \Pdm\Input\PdmData
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function checkinService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);
		$batch = $request->getPost('batch', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5040, $this);
		}

		/* Init batch */
		if ( ($batch instanceof \Rbs\Batch\Batch) == false ) {
			$batch = new \Service\Batch\CheckinBatch(get_class($this), 'checkinService');
			$batch->newUid()
				->setOwner(CurrentUser::get())
				->setComment($documents['document1']['comment']);
			DaoFactory::get()->getDao($batch::$classId)->save($batch);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];

			/* set vars */
			isset($document1['comment']) ? $comment = trim($document1['comment']) : $comment = '';
			isset($document1['checkinandkeep']) ? $ciAndKeep = (boolean)($document1['checkinandkeep']) : $ciAndKeep = false;

			if ( isset($document1['object']) ) {
				$document = $document1['object'];
				$documentId = $document->getId();
				$spacename = $document->spacename;
			}
			else {
				$document = null;
				$documentId = $document1['id'];
				$spacename = $document1['spacename'];
			}

			/** @var \Rbs\Ged\Document\VersionDao $dao */
			if ( isset($document->dao) ) {
				$dao = $document->dao;
				$factory = $dao->factory;
			}
			else {
				/* init some objects */
				$factory = DaoFactory::get($spacename);
				$dao = $factory->getDao(Document\Version::$classId);
			}

			/* Load service */
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);
			/* Compose wildspace with service */
			$service->wildspace = Wildspace::get(CurrentUser::get());
			/* Compose batch with service */
			$service->batch = $batch;

			try {
				if ( !$document ) {
					$document = $dao->loadFromId(new Document\Version(), $documentId);
				}
				$containerId = $document->getParent(true);

				/* load docfiles */
				/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
				$dfDao = $factory->getDao(Docfile\Version::$classId);
				$dfDao->loadDocfiles($document, $dfDao->toSys('accessCode') . '=:accessCode', array(
					':accessCode' => AccessCode::CHECKOUT
				));

				/* load container */
				$container = $factory->getLoader()->loadFromId($containerId, Workitem::$classId);
				$document->setParent($container);

				/* Service call */
				/* Update data and keep in ws */
				$document->comment = $comment;
				if ( $ciAndKeep ) {
					$releasing = false;
					$updateData = true;
					$deleteFileInWs = false;
					$fromWildspace = true;
					$checkAccess = true;
					$service->checkin($document, $releasing, $updateData, $deleteFileInWs, $fromWildspace, $checkAccess);
				}
				/* Checkin data in vault */
				else {
					$releasing = true;
					$updateData = true;
					$deleteFileInWs = true;
					$fromWildspace = true;
					$checkAccess = true;
					$service->checkin($document, $releasing, $updateData, $deleteFileInWs, $fromWildspace, $checkAccess);

					/* delete obsolete iterations */
					$toKeep = Ranchbe::get()->getConfig('document.assocfile.iterationtokeep');
					/* Docfile service */
					$dfService = Ranchbe::get()->getServiceManager()->getDocfileService($factory);
					foreach( $document->getDocfiles() as $docfile ) {
						$dfService->deleteIterations($docfile, $toKeep);
					}
				}
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new ServiceFeedback('checkin success', $document);
			$return->addFeedback($index, $feedback);
		}

		return $return;
	}

	/**
	 * Cancel checkout of documents and files.
	 * Files are let on wildspace of user.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 *        	
	 */
	public function resetService($request = null, $checkAccess = true)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);
		if ( !$documents ) {
			$documentId = $request->getPost('id', null);
			$spacename = $request->getPost('spacename', null);
			if ( $documentId && $spacename ) {
				$documents = [
					'document1' => [
						'id' => $documentId,
						'spacename' => $spacename
					]
				];
			}
		}

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5040, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];

			/** @var Document\Version $document */

			/* set vars */
			if ( isset($document1['object']) ) {
				$document = $document1['object'];
				$documentId = $document->getId();
				$spacename = $document->spacename;
			}
			else {
				$document = null;
				$documentId = $document1['id'];
				$spacename = $document1['spacename'];
			}

			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/* Load service */
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);

			try {
				if ( !$document ) {
					/** @var \Rbs\Ged\Document\VersionDao $dao */
					$dao = $factory->getDao(Document\Version::$classId);
					$document = $dao->loadFromId(new Document\Version(), $documentId);
				}

				/* load docfiles */
				if ( count($document->getDocfiles()) == 0 ) {
					/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
					$dfDao = $factory->getDao(Docfile\Version::$classId);
					$dfDao->loadDocfiles($document);
				}

				/* service call */
				$releasing = true;
				$updateData = false;
				$deleteFileInWs = false;
				$fromWildspace = true;
				$service->checkin($document, $releasing, $updateData, $deleteFileInWs, $fromWildspace, $checkAccess);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new ServiceFeedback('reset success', $document);
			$return->addFeedback($index, $feedback);
		}

		return $return;
	}

	/**
	 * Cancel checkout of documents and files.
	 * Files are let on wildspace of user.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 *
	 */
	public function adminresetService($request = null)
	{
		/* Check permission */
		try {
			$this->getAcl()->checkRightFromCn('admin', \Acl\Model\Resource\Project::$appCn);
		}
		catch( \Acl\AccessRestrictionException $e ) {
			throw new \Acl\AccessRestrictionException(sprintf(tra('To do this, you must have %s admin on resource %s'), 'admin', \Acl\Model\Resource\Project::$appCn));
		}

		return $this->resetService($request, false);
	}

	/**
	 *
	 * Cancel checkout of documents and files.
	 * Files are let on wildspace of user.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 			object: Document\Version instance. If set, spacename and id are ignored
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ],
	 * comment: string
	 * ]
	 * ~~~~~~~~~
	 * 
	 * document1.accesscode is one of constants of \Rbplm\Ged\AccessCode :
	 * - FREE = 0 : All modifications enables
	 * - CHECKOUT = 1 : Checkouted by a user
	 * - INWORKFLOW = 5 : A workflow is started
	 * - LOCKEDLEVEL1 = 6 : Level 1
	 * - LOCKEDLEVEL2 = 7 : Level 2
	 * - LOCKEDLEVEL3 = 8 : Level 3
	 * - LOCKEDLEVEL4 = 9 : Level 4
	 * - VALIDATE = 10 : Checkout is locked, Metadatas edition is locked
	 * - LOCKED = 11 : Checkout is locked, Metadatas edition is locked, versioning is locked
	 * - SUPPRESS = 12 : Document is suppressed
	 * - ITERATION = 13 : Document is a iteration
	 * - VERSION = 14 : Document is a version
	 * - HISTORY = 15 : Document is a version
	 * - ARCHIVE = 20 : Document is archived
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 * @see \Rbplm\Ged\AccessCode
	 */
	public function lockService($request = null, $accessCode = AccessCode::LOCKED)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);
		$comment = $request->getPost('comment', '');

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5040, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];

			/* @var Document\Version $document */

			/* set vars */
			if ( isset($document1['object']) ) {
				$document = $document1['object'];
				$spacename = $document->spacename;
			}
			else {
				$document = null;
				$spacename = $document1['spacename'];
			}

			/* load some helpers */
			$factory = DaoFactory::get($spacename);

			/* Load service */
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);

			try {
				if ( !$document ) {
					/** @var \Rbs\Ged\Document\VersionDao $dao */
					$dao = $factory->getDao(Document\Version::$classId);
					$documentId = $document1['id'];
					$document = $dao->loadFromId(new Document\Version(), $documentId);
				}

				/* Check permission */
				$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

				/* load docfiles */
				/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
				$dfDao = $factory->getDao(Docfile\Version::$classId);
				$dfDao->loadDocfiles($document);

				/* service call */
				$document->comment = $comment;
				$service->lock($document, $accessCode, true);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new ServiceFeedback('lock success', $document);
			$return->addFeedback($index, $feedback);
		}
		return $return;
	}

	/**
	 *
	 * Unlock the documents
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function unlockService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5040, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];

			/* @var Document\Version $document */

			/* set vars */
			if ( isset($document1['object']) ) {
				$document = $document1['object'];
				$documentId = $document->getId();
				$spacename = $document->spacename;
			}
			else {
				$document = null;
				$documentId = $document1['id'];
				$spacename = $document1['spacename'];
			}

			/* load some helpers */
			$factory = DaoFactory::get($spacename);

			/* Load service */
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);

			try {
				if ( !$document ) {
					/** @var \Rbs\Ged\Document\VersionDao $dao */
					$dao = $factory->getDao(Document\Version::$classId);
					$document = $dao->loadFromId(new Document\Version(), $documentId);
				}

				/* Check permission */
				$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

				/* check current access code */
				$aCode = $document->checkAccess();
				if ( $aCode <= AccessCode::INWORKFLOW || $aCode >= AccessCode::HISTORY ) {
					$msg = sprintf(tra('You cant unlock document %s because its not permit by his lock'), $document->getNumber());
					throw new ServiceException($msg);
				}

				/* load docfiles */
				/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
				$dfDao = $factory->getDao(Docfile\Version::$classId);
				$dfDao->loadDocfiles($document);

				/* service call */
				$service->lock($document, AccessCode::FREE, true);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new ServiceFeedback('unlock success', $document);
			$return->addFeedback($index, $feedback);
		}
		return $return;
	}

	/**
	 * Set status of document as SUPRESSED, but let datas as it.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function marktosuppressService($request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$return = $this->lockService($request, AccessCode::SUPPRESS);

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5040, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];

			/* @var Document\Version $document */

			/* set vars */
			if ( isset($document1['object']) ) {
				$document = $document1['object'];
				$documentId = $document->getId();
				$spacename = $document->spacename;
			}
			else {
				$document = null;
				$documentId = $document1['id'];
				$spacename = $document1['spacename'];
			}

			/* load some helpers */
			$factory = DaoFactory::get($spacename);

			/* Load service */
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);

			try {
				if ( !$document ) {
					/** @var \Rbs\Ged\Document\VersionDao $dao */
					$dao = $factory->getDao(Document\Version::$classId);
					$document = $dao->loadFromId(new Document\Version(), $documentId);
				}

				/* Check permission */
				$this->getAcl()->checkRightFromUid('delete', $document->parentUid);

				/* load docfiles */
				/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
				$dfDao = $factory->getDao(Docfile\Version::$classId);
				$dfDao->loadDocfiles($document);

				/* Service call */
				$accessCode = AccessCode::SUPPRESS;
				$service->lock($document, $accessCode, true);

				/* Move file to trash dir */
				//$reposit = new Vault\Reposit();
				//$repositId = $document->parentId;
				//$factory->getDao(Vault\Reposit::$classId)->loadFromId($reposit, $repositId);
				//$vault = new \Rbs\Vault\Vault($reposit, $factory->getDao(Vault\Record::$classId));

				/* */
				//foreach($document->getDocfiles() as $docfile){
			/** @var Docfile\Version $docfile */
				//$record = $docfile->getData();
				//$vault->putInTrash($record);
				//$docfile->setData($record);
				//$dfDao->save($docfile);
				//}
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new ServiceFeedback('lock success', $document);
			$return->addFeedback($index, $feedback);
		}
		return $return;

		$factory = DaoFactory::get($spacename);
	}

	/**
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 *
	 */
	public function unmarktosuppressService($request = null)
	{
		return $this->lockService($request, AccessCode::FREE);
	}
} /* End of class */
