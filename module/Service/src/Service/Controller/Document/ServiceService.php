<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;

/**
 */
class ServiceService extends ServiceController
{

	/**
	 * AJAX Method to search in product version table
	 */
	public function searchService()
	{
		$request = $this->getRequest();

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$what = $request->getQuery('what', null);
		$where = $request->getQuery('where', null);
		$select = $request->getQuery('select', null);
		$op = $request->getQuery('getopfile', null);
		$spacename = $request->getQuery('spacename', null);

		$factory = DaoFactory::get($spacename);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$dao = $factory->getDao(Document\Version::$classId);
		$list = $factory->getList(Document\Version::$classId);
		$filter->andfind($what, $dao->toSys($where), $op);

		$reducedSelect = array();
		if ( is_string($select) ) {
			$select = array(
				$select
			);
		}
		if ( is_array($select) ) {
			foreach( $dao->metaModel as $asSys => $asApp ) {
				if ( in_array($asApp, $select) ) {
					$reducedSelect[] = $asSys . ' as ' . $asApp;
				}
			}
			$filter->select($reducedSelect);
		}

		$list->load($filter);
		echo json_encode($list->toArray());
	}
}
