<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Service\Controller\ServiceError;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;

/**
 * Add, remove tags from documents.
 *
 */
class ShareService extends ServiceController
{

	/**
	 * 
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request
	 * ~~~~~~~~~{.json}
	 * [
	 * spacename : string,
	 * id : integer
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @return \Service\Controller\ServiceRespons
	 */
	public function createService($request = null)
	{
		$return = new ServiceRespons($this);
		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$spacename = $request->getQuery('spacename', null);
		$documentId = $request->getQuery('id', null);

		/* Init some helpers */
		$factory = DaoFactory::get($spacename);

		/* Load document */
		$document = new Document\Version();
		$factory->getDao(Document\Version::$classId)->loadFromId($document, $documentId);
		$document->spacename = $spacename;

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

		/* Save link */
		try {
			$publicShare = new \Rbs\Ged\Document\Share\PublicUrl();
			$publicShare->newUid();
			$publicShare->setDocument($document);
			$publicShare->dao = $factory->getDao($publicShare->cid);
			$publicShare->dao->save($publicShare);
		}
		catch( \PDOException $e ) {
			throw $e;
		}

		/* return link to requestor */
		$return->setData($publicShare->getArrayCopy());

		return $return;
	}

	/**
	 * HTTP GET method
	 * @param \Zend\Http\Request $request
	 * ~~~~~~~~~{.json}
	 * [
	 * uid : string,
	 * expirationDate : string,
	 * comment : string
	 * ]
	 * ~~~~~~~~~
	 *
	 * @return \Service\Controller\ServiceRespons
	 */
	public function editService($request = null)
	{
		$return = new ServiceRespons($this);
		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$uid = $request->getQuery('uid', null);
		$expirationDate = $request->getQuery('expirationDate', null);
		$comment = $request->getQuery('comment', null);

		/* Init some helpers */
		$factory = DaoFactory::get();

		/* Check permissions */
		//$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

		/* Save link */
		try {
			$publicShare = new \Rbs\Ged\Document\Share\PublicUrl();
			$factory->getDao($publicShare->cid)->loadFromUid($publicShare, $uid);
			if ( $comment ) {
				$publicShare->setComment($comment);
			}
			if ( $expirationDate === '' ) {
				$publicShare->setExpirationDate(null);
			}
			elseif ( $expirationDate ) {
				$expirationDate = new \DateTime($expirationDate);
				$publicShare->setExpirationDate($expirationDate);
			}
			$publicShare->dao->save($publicShare);
		}
		catch( \PDOException $e ) {
			throw $e;
		}

		/* return link to requestor */
		$return->setData($publicShare->getArrayCopy());
		return $return;
	}

	/**
	 * Delete
	 * 
	 * HTTP GET method
	 * @param \Zend\Http\Request $request
	 * ~~~~~~~~~{.json}
	 * [
	 * id : int,
	 * uid : string,
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @var array $input. Keys: id: id of link, uid: uid of link
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function deleteService($request = null)
	{
		$return = new ServiceRespons($this);
		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$id = $request->getQuery('id', null);
		$uid = $request->getQuery('uid', null);

		/* load some helpers */
		$factory = DaoFactory::get();

		/** @var \Rbs\Ged\Document\Share\PublicUrlDao $dao */
		$dao = $factory->getDao(\Rbs\Ged\Document\Share\PublicUrl::$classId);

		try {
			if ( $uid ) {
				$dao->deleteFromUid($uid);
			}
			else {
				$dao->deleteFromId($id);
			}
			$feedback = new ServiceFeedback('Share is deleted', $uid);
			$return->addFeedback(1, $feedback);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, $uid);
			$return->addError(1, $error);
		}

		return $return;
	}
}
