<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbs\Ged\Document\Event;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceException;

/**
 * Get and set some datas from one document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	id:string,
 *	spacename:string
 * ]
 * ~~~~~~~~~
 * 
 */
class EntityService extends AbstractController
{

	/**
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 * ~~~~~~~~~{.json}
	 * [
	 *	name:string,
	 *	spacename:string
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function isexistingService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$name = $request->getQuery('name', null);
		$spacename = $request->getQuery('spacename', null);

		if ( !($name) ) {
			throw new ServiceException('name is not set', 5060, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5062, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/* @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);
			$stmt = $dao->query([
				'id',
				$dao->toSys('iteration')
			], 'name=:name ORDER BY ' . $dao->toSys('iteration'), [
				':name' => $name
			]);
			if ( $stmt->rowCount() == 0 ) {
				$return->setData("");
			}
			else {
				$return->setData($stmt->fetchAll(\PDO::FETCH_ASSOC));
			}
		}
		catch( \Exception $e ) {
			throw new ServiceException($e->getMessage(), 5060, $this);
		}

		return $return;
	}

	/**
	 * GET Method
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	id:string,
	 *	spacename:string
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function getfromidService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$id = $request->getQuery('id', null);
		$spacename = $request->getQuery('spacename', null);

		if ( !($id) ) {
			throw new ServiceException('id is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5051, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/* @var \Rbplm\Ged\Document\Version $document */
			$document = $factory->getDao(Document\Version::$classId)->loadFromId(new Document\Version(), $id);
			$this->getAcl()->checkRightFromUid('read', $document->parentUid);
			$return->setData($document->getArrayCopy());
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, $document);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * GET Method
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	number:string,
	 *	version:integer,
	 *	spacename:string
	 * ]
	 * ~~~~~~~~~
	 * 
	 * if version is set, get the document with the specified version, elese return the last version.
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function getfromnumberService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$number = $request->getQuery('number', null);
		$spacename = $request->getQuery('spacename', null);
		$versionId = $request->getQuery('version', null);

		if ( !($number) ) {
			throw new ServiceException('number is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5051, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);

			/* @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);
			$document = new Document\Version();
			$dao->loadFromNumber($document, $number, $versionId);
			$this->getAcl()->checkRightFromUid('read', $document->parentUid);
			$return->setData($document->getArrayCopy());
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, $document);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * GET Method
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	name:string,
	 *	version:integer,
	 *	spacename:string
	 * ]
	 * ~~~~~~~~~
	 *
	 * if version is set, get the document with the specified version, elese return the last version.
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function getfromnameService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$name = $request->getQuery('name', null);
		$spacename = $request->getQuery('spacename', null);
		$versionId = $request->getQuery('version', null);

		if ( !($name) ) {
			throw new ServiceException('name is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5051, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);

			/* @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);
			$document = new Document\Version();
			$dao->loadFromName($document, $name, $versionId);
			$this->getAcl()->checkRightFromUid('read', $document->parentUid);
			$return->setData($document->getArrayCopy());
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, $document);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * return the number of children in return
	 * GET Method
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	id:integer,
	 *	spacename:string
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function haschildService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$id = $request->getQuery('id', null);
		$spacename = $request->getQuery('spacename', null);

		if ( !($id) ) {
			throw new ServiceException('id is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5051, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/* @var \Rbs\Ged\Document\LinkDao $dao */
			$dao = $factory->getDao(Document\Link::$classId);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * return the number of children in return
	 * GET Method
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	id:integer,
	 *	spacename:string
	 * ]
	 * ~~~~~~~~~
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function hasparentService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$id = $request->getQuery('id', null);
		$spacename = $request->getQuery('spacename', null);

		if ( !($id) ) {
			throw new ServiceException('id is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5051, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/* @var \Rbs\Ged\Document\LinkDao $dao */
			$dao = $factory->getDao(Document\Link::$classId);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError(1, $error);
		}

		return $return;
	}

	/**
	 * return the number of children in return
	 * GET Method
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	id:integer,
	 *	spacename:string
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function editService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$id = $request->getPost('id', null);
		$spacename = $request->getPost('spacename', null);
		$pnames = $request->getPost('pnames', []);
		$pvalues = $request->getPost('pvalues', []);

		if ( !($id) ) {
			throw new ServiceException('id is not set', 5051, $this);
		}
		if ( !($spacename) ) {
			throw new ServiceException('spacename is not set', 5051, $this);
		}

		try {
			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$document = new Document\Version();
			$document->dao = $factory->getDao(Document\Version::$classId);
			$document->dao->loadFromId($document, $id);

			/* trigger event */
			$event = new Event(Event::PRE_EDIT, $document, $factory);
			$this->getEventManager()->trigger($event);

			$properties = [];
			for ($i = 0; $i < count($pnames); $i++) {
				$pname = $pnames[$i];
				$pvalue = $pvalues[$i];
				$properties[$pname] = $pvalue;
			}

			$document->hydrate($properties);
			$document->dao->save($document);
			$this->getEventManager()->trigger($event->setName(Event::POST_EDIT));
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError(1, $error);
		}

		return $return;
	}
} /* End of class */
