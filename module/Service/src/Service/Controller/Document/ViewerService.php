<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Sys\Fsdata;
use Service\Controller\ServiceController;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Rbs\Converter\Factory as ConverterFactory;

/**
 * Get files associated to document.
 *
 */
class ViewerService extends ServiceController
{

	/**
	 *
	 */
	public function __construct()
	{
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Service\Controller\ServiceController::init()
	 * 
	 * Unactive access control
	 */
	public function init($view = null, $errorStack = null)
	{
		$this->isService = true;
		return $this;
	}

	/**
	 * Get the files associated to Document\Version
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request
	 * @param $_GET['id'] int 
	 * 					Set id to get only one file.
	 * @param $_GET['spacename'] string
	 *        	Spacename of the object to get. Permited values are
	 *        	- 'workitem'
	 *        	- 'bookshop'
	 *        	- 'cadlib'
	 *        	- 'mockup'
	 *        
	 * @param $_GET['checked'] array 
	 * 					If you want get many files, use the checked parameter.
	 * @param $_GET['spacenames'] array of spacenames in case of multiselection where key is id of document
	 *        
	 *        
	 * @return string base64 encoded datas
	 */
	public function downloadService($request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;

		$id = $request->getQuery('id', null);
		$spacename = $request->getQuery('spacename', null);
		$checked = $request->getQuery('checked', []);
		$spacenames = $request->getQuery('spacenames', []);

		if ( $id ) {
			$checked[] = $id;
			$spacenames[$id] = $spacename;
		}

		$collection = [];
		foreach( $checked as $id ) {
			$spacename = $spacenames[$id];

			$factory = DaoFactory::get($spacename);
			$document = new Document\Version();
			/* @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao($document->cid);
			$dao->loadFromId($document, $id);

			/* Check permission */
			//$this->getAcl()->checkRightFromUid('read', $document->parentUid);

			/* @var \Rbs\Ged\Docfile\VersionDao $dfDao */
			$dfDao = $factory->getDao(Docfile\Version::$classId);
			$dfDao->loadDocfiles($document, $dfDao->toSys('mainrole') . '=' . Docfile\Role::MAIN);
			$docfiles = $document->getDocfiles();
			$mainDf = array_shift($docfiles);

			/* @var \Rbplm\Sys\Datatype\File $fsdata */
			$fsdata = $mainDf->getData()->getFsdata();
			$fileName = $document->getName() . '.v' . $document->version . '.' . $document->iteration . $fsdata->getExtension();

			if ( count($checked) == 1 ) {
				return self::download($fileName, $fsdata->getMimetype(), $fsdata->getSize(), $fsdata->getFullpath());
			}
			else {
				$collection[] = [
					$fileName,
					$fsdata
				];
			}
		}

		try {
			$zipfile = tempnam(sys_get_temp_dir(), 'rb') . '.zip';
			$zip = new \ZipArchive();
			$zip->open($zipfile, \ZipArchive::CREATE);
			foreach( $collection as $data ) {
				$fileName = $data[0];
				$fsdata = $data[1];
				$file = $fsdata->getFullpath();
				$zip->addFile($file, $fileName);
			}
			$zip->close();

			$fsdata = new Fsdata($zipfile);
			$fsdata->download();
			unlink($zipfile);
		}
		catch( \Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Get the files associated to Document\Version
	 *
	 * HTTP:GET method
	 *
	 * @param \Zend\Http\Request
	 * @param $_GET['id'] int 
	 * 					Set id to get only one file.
	 * @param $_GET['spacename'] string
	 *        	Spacename of the object to get. Permited values are
	 *        	- 'workitem'
	 *        	- 'bookshop'
	 *        	- 'cadlib'
	 *        	- 'mockup'
	 *        
	 * @param $_GET['checked'] array 
	 * 					If you want get many files, use the checked parameter.
	 * @param $_GET['spacenames'] array of spacenames in case of multiselection where key is id of document
	 *
	 * @return string base64 encoded datas
	 */
	public function downloadvisuService($request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;

		$id = $request->getQuery('id', null);
		$spacename = $request->getQuery('spacename', null);
		$checked = $request->getQuery('checked', []);
		$spacenames = $request->getQuery('spacenames', []);

		if ( $id ) {
			$checked[] = $id;
			$spacenames[$id] = $spacename;
		}

		$collection = [];
		foreach( $checked as $id ) {
			$spacename = $spacenames[$id];

			$factory = DaoFactory::get($spacename);
			$document = new Document\Version();
			/* @var \Rbs\Ged\Document\VersionDao $dao */
			$document->dao = $factory->getDao($document->cid);
			$document->dao->loadFromId($document, $id);

			/* @var \Rbs\Ged\Docfile\VersionDao $dfDao */
			$dfDao = $factory->getDao(Docfile\Version::$classId);

			/* Check permission */
			//$this->getAcl()->checkRightFromUid('read', $document->parentUid);

			try {
				$dfDao->loadDocfiles($document, $dfDao->toSys('mainrole') . '=' . Docfile\Role::VISU);
				$docfiles = $document->getDocfiles();
				$visuDf = array_shift($docfiles);
				if ( !$visuDf ) {
					$dfDao->loadDocfiles($document, $dfDao->toSys('mainrole') . '=' . Docfile\Role::MAIN);
					$docfiles = $document->getDocfiles();
					$visuDf = array_shift($docfiles);
				}
				if ( $visuDf ) {
					/* @var \Rbplm\Sys\Datatype\File $fsdata */
					$fsdata = $visuDf->getData()->getFsdata();
					$fileName = $document->getName() . '.v' . $document->version . '.' . $document->iteration . $fsdata->getExtension();
				}
				else {
					throw new ServiceError('file not found. Check that document has at least one file with role=MAIN or VISU');
				}
			}
			catch( ServiceError $e ) {
				$return = new ServiceRespons($this);
				$return->setHttpErrorCode(404);
				$return->addError(0, $e);
				return $return;
			}
			catch( \Exception $e ) {
				$return = new ServiceRespons($this);
				$return->setHttpErrorCode(500);
				$return->setException($e);
				return $return;
			}

			if ( count($checked) == 1 ) {
				return self::download($fileName, $fsdata->getMimetype(), $fsdata->getSize(), $fsdata->getFullpath());
			}
			else {
				$collection[] = [
					$fileName,
					$fsdata
				];
			}
		}

		try {
			$zipfile = tempnam(sys_get_temp_dir(), 'rb') . '.zip';
			$zip = new \ZipArchive();
			$zip->open($zipfile, \ZipArchive::CREATE);
			foreach( $collection as $data ) {
				$fileName = $data[0];
				$fsdata = $data[1];
				$file = $fsdata->getFullpath();
				$zip->addFile($file, $fileName);
			}
			$zip->close();

			$fsdata = new Fsdata($zipfile);
			$fsdata->download();
			unlink($zipfile);
		}
		catch( \Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Get the last version of document from his number.
	 * 
	 * HTTP:GET method
	 *
	 * @param \Zend\Http\Request
	 * @param $_GET['number'] string 
	 * 					The document number.
	 * @param $_GET['spacename'] string
	 *        	Spacename of the object to get. Permited values are
	 *        	- 'workitem'
	 *        	- 'bookshop'
	 *        	- 'cadlib'
	 *        	- 'mockup'
	 *        
	 * @return string base64 encoded datas
	 */
	public function getlastService($request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;

		$number = $request->getQuery('number', null);
		$spacename = $request->getQuery('spacename', null);

		/* Some helpers */
		$factory = DaoFactory::get($spacename);
		$document = new Document\Version();
		/* @var \Rbs\Ged\Document\VersionDao $dao */
		$dao = $factory->getDao(Document\Version::$classId);
		$dao->loadFromNumber($document, $number);

		/* Check permission */
		//$this->getAcl()->checkRightFromUid('read', $document->parentUid);

		/* @var \Rbs\Ged\Docfile\VersionDao $dfDao */
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$dfDao->loadDocfiles($document, $dfDao->toSys('mainrole') . '=' . Docfile\Role::MAIN);
		$docfiles = $document->getDocfiles();
		$mainDf = array_shift($docfiles);

		/* @var \Rbplm\Sys\Datatype\File $fsdata */
		$fsdata = $mainDf->getData()->getFsdata();
		$fileName = $document->getName() . '.v' . $document->version . '.' . $document->iteration . $fsdata->getExtension();
		self::download($fileName, $fsdata->getMimetype(), $fsdata->getSize(), $fsdata->getFullpath());
	}

	/**
	 * Get the files associated to Document\Version
	 *
	 * HTTP:GET method
	 *
	 * @param \Zend\Http\Request
	 * @param $_GET['id'] int
	 * 					Set id to get only one file.
	 * @param $_GET['checked'] array
	 * 					If you want get many files, use the checked parameter.
	 * @param $_GET['spacename'] string
	 *        	Spacename of the object to get. Permited values are
	 *        	- 'workitem'
	 *        	- 'bookshop'
	 *        	- 'cadlib'
	 *        	- 'mockup'
	 *
	 * @return string base64 encoded datas
	 */
	public function topdfService($request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;

		$id = $request->getQuery('id', null);
		$checked = $request->getQuery('checked', null);
		$spacename = $request->getQuery('spacename', null);

		if ( $checked ) {
			$id = current($checked);
		}

		$document = new Document\Version();
		$factory = DaoFactory::get($spacename);
		/* @var \Rbs\Ged\Document\VersionDao $dao */
		$dao = $factory->getDao($document->cid);
		$dao->loadFromId($document, $id);

		/* Check permission */
		//$this->getAcl()->checkRightFromUid('read', $document->parentUid);

		/* @var \Rbs\Ged\Docfile\VersionDao $dfDao */
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$dfDao->loadDocfiles($document, $dfDao->toSys('mainrole') . '=' . Docfile\Role::MAIN);
		$docfiles = $document->getDocfiles();
		$mainDf = array_shift($docfiles);

		/* @var \Rbplm\Sys\Datatype\File $fsdata */
		$fsdata = $mainDf->getData()->getFsdata();
		$extension = $fsdata->getExtension();

		if ( $extension != '.pdf' ) {
			try {
				$converter = ConverterFactory::get()->getConverter(trim($extension, '.'), 'pdf');
				$tmpfile = tempnam('/tmp', 'rb');
				$converter->convert($fsdata->getFullpath(), $tmpfile);
				$size = filesize($tmpfile);
				$fileName = $document->getName() . '.v' . $document->version . '.' . $document->iteration . '.pdf';
				self::download($fileName, 'application/pdf', $size, $tmpfile);
				unlink($tmpfile);
				die();
			}
			catch( \Rbs\Converter\NotMappedException $e ) {
				$fileName = $document->getName() . '.v' . $document->version . '.' . $document->iteration . $extension;
				self::download($fileName, $fsdata->getMimetype(), $fsdata->getSize(), $fsdata->getFullpath());
			}
		}
		else {
			self::download($fileName, 'application/pdf', $fsdata->getSize(), $fsdata->getFullpath());
		}
	}

	/**
	 * @todo put in Http class
	 * @param string $fileName
	 * @param string $mimeType
	 * @param integer $size
	 * @param string $file
	 * @return boolean
	 */
	public static function download($fileName, $mimeType, $size, $file)
	{
		if ( !$mimeType ) {
			$mimeType = 'application/default';
		}
		elseif ( $mimeType == 'no_read' ) {
			return false;
		}

		header("Content-disposition: attachment; filename=\"$fileName\"");
		header("Content-Type: " . $mimeType);
		header("Content-Transfer-Encoding: \"$fileName\"\n"); // Surtout ne pas enlever le \n
		header("Content-Length: " . ($size));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		readfile($file);
		return true;
	}

	/**
	 * @param string $rootname
	 * @param string $extension
	 * @param Document\Version $document
	 * @return string
	 */
	public static function versionedName($rootname, $extension, $document)
	{
		return $rootname . '.v' . $document->version . '.' . $document->iteration . $extension;
	}
}
