<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Ged\Document\BasketDao;
use Rbplm\People\CurrentUser;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;

/**
 * Class to manage documents in the user basket.
 * Basket is used to create a set of documents to list in a separate windows.
 * 
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * documents:[
 * 		document1:{
 * 			id:string,
 * 			spacename:string
 * 		},
 * 		document2:{...},
 * 		...
 * 	}
 * ]
 * ~~~~~~~~~
 * 
 * 
 */
class BasketService extends ServiceController
{

	/**
	 * Add a document to current user basket
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function addService($request = null)
	{
		$return = new ServiceRespons($this);

		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5030, $this);
		}

		$basket = new BasketDao(CurrentUser::get(), DaoFactory::get('default'));

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = strtolower($document1['spacename']);
			$spaceId = \Rbs\Space\Space::getIdFromName($spacename);

			try {
				$basket->add($documentId, $spaceId);
				$feedback = new ServiceFeedback(sprintf(tra('data %s is put in your basket'), ''), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				if ( strstr($e->getMessage(), '1062 Duplicate entry') ) {
					$feedback = new ServiceFeedback(sprintf(tra('data is existing in your basket'), ''), null);
					$return->addFeedback($index, $feedback);
				}
				else {
					$error = new ServiceError($e->getMessage(), $e, null);
					$return->addError($index, $error);
				}
				continue;
			}
		}

		return $return;
	}

	/**
	 * Remove a document of the basket of current user
	 *
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function removeService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5030, $this);
		}

		$basket = new BasketDao(CurrentUser::get(), DaoFactory::get('default'));

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = strtolower($document1['spacename']);
			$spaceId = \Rbs\Space\Space::getIdFromName($spacename);

			try {
				$basket->remove($documentId, $spaceId);
				$feedback = new ServiceFeedback(sprintf(tra('data %s is removed'), ''), null);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, null);
				$return->addError($index, $error);
				continue;
			}
		}

		return $return;
	}

	/**
	 * Empty the basket of current user.
	 *
	 *
	 * HTTP GET method
	 * 
	 * None parameters is required. Simply empty the basket of the current user.
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function cleanService()
	{
		$return = new ServiceRespons($this);

		try {
			$basket = new BasketDao(CurrentUser::get(), DaoFactory::get('default'));
			$basket->clean();
			$feedback = new ServiceFeedback(tra('your basket is cleaned'), null);
			$return->addFeedback(1, $feedback);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError(1, $error);
		}

		return $return;
	}
} /* End of class */
