<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbs\Dao\Sier\Filter;
use Rbplm\Dao\Filter\Op;
use Service\Controller\ServiceError;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;

/**
 * Add, remove tags from documents.
 *
 */
class TagsService extends ServiceController
{

	/**
	 * HTTP GET METHOD
	 * @param \Zend\Http\Request $request
	 * @return \Service\Controller\ServiceRespons
	 */
	public function indexService($request = null)
	{
		$return = new ServiceRespons($this);
		$request = $this->getRequest();

		$spacename = $request->getQuery('spacename', null);
		$selectedTags = $request->getQuery('tags', []);
		$containerId = $request->getQuery('containerId', []);

		/* load some helpers */
		$factory = DaoFactory::get($spacename);

		$list = $factory->getList(Document\Version::$classId);
		$filter = new Filter('', false);
		$filter->select(array(
			'id',
			'tags'
		));
		$filter->page(1, 1000);

		/* */
		$filter->andfind('', 'tags', Op::NOTNULL);
		if ( $selectedTags ) {
			foreach( $selectedTags as $selectedTag ) {
				$selectedTag = trim($selectedTag);
				if ( $selectedTag ) {
					$filter->andfind($selectedTag, 'tags', Op::FINDINSET);
				}
			}
		}
		if ( $containerId ) {
			$dao = $factory->getDao(Document\Version::$classId);
			$filter->andfind($containerId, $dao->toSys('parentId'), Op::EQUAL);
		}

		$list->load($filter);

		$filtered = array();
		foreach( $list as $item ) {
			$exploded = explode(',', $item['tags']);
			$filtered = array_merge($filtered, $exploded);
		}

		$filtered = array_unique($filtered);
		$return->setData($filtered);
		return $return;
	}

	/**
	 * HTTP GET method
	 * 
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}[
	 * spacename="STRING",
	 * term="STRING"
	 * ]
	 * ~~~~~~~~~
	 */
	public function searchService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$spacename = $request->getQuery('spacename', null);
		$term = $request->getQuery('term', null);

		/* load some helpers */
		$factory = DaoFactory::get($spacename);

		$list = $factory->getList(Document\Version::$classId);
		$filter = new Filter('', false);
		$filter->select(array(
			'id',
			'tags'
		));
		$filter->page(1, 100);

		/* */
		try {
			$filter->andfind('', 'tags', Op::NOTNULL);
			$filter->andfind('""', 'tags', Op::NOTEQUAL);
			if ( $term ) {
				$filter->andfind($term . '%', 'tags', Op::LIKE);
			}
			$list->load($filter);
		}
		catch( \Exception $e ) {
			throw new ServiceException($e->getMessage() . ' - ' . $list->stmt->queryString);
		}

		$filtered = array();
		foreach( $list as $item ) {
			$exploded = explode(',', $item['tags']);
			$filtered = array_merge($filtered, $exploded);
		}

		$return->setData(array_unique($filtered));
		return $return;
	}

	/**
	 * 
	 * Replace tags of documents.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:integer,
	 * 			spacename:string,
	 * 			tags:string
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * document1.tags must contains string. Its the text of tag that must UTF8 encoded.
	 * documents.tags is the complete list of tags to associate to document. To add a tag to existing tag list, use addService.
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function updateService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5040, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];
			$tags = str_replace(' ', '_', trim($document1['tags']));

			if ( $tags == '' ) {}

			/* load some helpers */
			$factory = DaoFactory::get($spacename);

			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(\Rbplm\Ged\Document\Version::$classId);

			try {
				$data = array(
					'tags' => $tags
				);
				$dao->updateFromArray($documentId, $data);
				$feedback = new ServiceFeedback('Tag is updated/added', $tags);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $tags);
				$return->addError($index, $error);
				continue;
			}
		}

		return $return;
	}

	/**
	 * Add a tag to documents.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:integer,
	 * 			spacename:string,
	 * 			tag:string
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * document1.tag must contains string. Its the text of tag that must UTF8 encoded.
	 * 
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function addService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5040, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];
			$tag = str_replace(' ', '_', trim($document1['tag']));

			if ( $tag == '' ) {
				continue;
			}

			/* load some helpers */
			$factory = DaoFactory::get($spacename);

			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(\Rbplm\Ged\Document\Version::$classId);

			/* get previous tags */
			$exisitingTags = $dao->query([
				'tags'
			], 'id=:id', [
				':id' => $documentId
			])->fetchColumn(0);
			$exisitingTags = str_replace($tag, '', $exisitingTags);
			$exisitingTags = trim($exisitingTags);

			try {
				$data = array(
					'tags' => $exisitingTags . ',' . $tag
				);
				$dao->updateFromArray($documentId, $data);
				$feedback = new ServiceFeedback('Tag is updated/added', $tag);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $tag);
				$return->addError($index, $error);
				continue;
			}
		}

		return $return;
	}
}
