<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Ranchbe;
use Rbplm\Org\Workitem;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\People;
use Rbplm\Dao\Registry;
use Service\Controller\ServiceException;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;

/**
 * Manager for documents.
 *
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * documents:[
 * 		document1:{
 * 			id:string,
 * 			spacename:string
 * 		},
 * 		document2:{...},
 * 		...
 * 	}
 * ]
 * ~~~~~~~~~
 *
 */
class DatamanagerService extends AbstractController
{

	const FROM_WS = 'FROM_WS';

	const FROM_DATA = 'FROM_DATA';

	/**
	 * Copy all files of documents in the user wildspace
	 *
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function putinwsService($request = null)
	{
		$return = new ServiceRespons($this);

		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		$documents = $request->getQuery('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5030, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];

			$factory = DaoFactory::get($spacename);
			$dao = $factory->getDao(Document\Version::$classId);
			$dfDao = $factory->getDao(Docfile\Version::$classId);
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);
			try {
				$document = $dao->loadFromId(new Document\Version(), $documentId);
				/* Check permission */
				$this->getAcl()->checkRightFromUid('read', $document->parentUid);
				/* */
				$dfDao->loadDocfiles($document);
				$service->putinws($document);
				$return->bindServiceRespons($index, $service->respons);

				$feedback = new \Service\Controller\ServiceFeedback(sprintf(tra('data %s copied in wildspace'), $document->getUid()), $document);
				$return->addFeedback($index, $feedback);
			}
			catch( \Exception $e ) {
				$error = new \Service\Controller\ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}
		}

		return $return;
	}

	/**
	 * Move document from container to another.
	 * Move too associated files from source assiciated vault to target container vault.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request $request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 			tocontainer:integer
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * The target container is set in tocontainer key of inputs. Its the id of the target container.
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function movedocumentService($request = null)
	{
		$return = new ServiceRespons($this);

		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5030, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];

			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];
			$toContainerId = $document1['tocontainer'];

			/* init some basics objects */
			$factory = DaoFactory::get($spacename);
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);

			try {
				/* load document */
				$document = new Document\Version();
				$dao = $factory->getDao(Document\Version::$classId);
				$dao->loadFromId($document, $documentId);

				/* Check permission */
				$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

				/* load docfiles - require by service to move with docfiles */
				$dfDao = $factory->getDao(Docfile\Version::$classId);
				$dfDao->loadDocfiles($document);

				/* set from container Id */
				$containerId = $document->getParent(true);

				/* check that to is not same that from */
				if ( $containerId == $toContainerId ) {
					$fb = new \Service\Controller\ServiceError('Target container is same that original container', null, $document);
					$return->addError($index, $fb);
					continue;
				}

				/* load from container */
				$container = $factory->getDao(Workitem::$classId)->loadFromId(new Workitem(), $containerId);

				/* inject container object in document */
				$document->setParent($container);

				/* load target container */
				$toContainer = $factory->getModel(Workitem::$classId);
				$factory->getDao(Workitem::$classId)->loadFromId($toContainer, $toContainerId);

				/* check rights on target container */
				$this->getAcl()->checkRightFromUid('edit', $toContainer->getUid());

				$service->moveDocument($document, $toContainer);

				/* transmit feedbacks to Datamanager */
				if ( $service->respons->hasFeedbacks() ) {
					foreach( $service->respons->getFeedbacks() as $msg ) {
						$feedback = new \Service\Controller\ServiceFeedback($msg, $document);
						$return->addFeedback($index, $feedback);
					}
				}
				if ( $service->respons->hasErrors() ) {
					foreach( $service->respons->getErrors() as $msg ) {
						$error = new \Service\Controller\ServiceError($msg, null, $document);
						$return->addError($index, $error);
					}
				}
			}
			catch( \Exception $e ) {
				$error = new \Service\Controller\ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}
		}

		return $return;
	}

	/**
	 * Copy documents to another name and container.
	 *
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request $request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 			tocontainer:integer,
	 * 			toname:string,
	 * 			toversion:integer,
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * The target container is set in tocontainer key of inputs. Its the id of the target container.
	 * The new name is setted in toname as string.
	 * The new name is setted in toversion as integer.
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function copydocumentService($request = null)
	{
		$return = new ServiceRespons($this);

		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5020, $this);
		}

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];
			$toContainerId = $document1['tocontainer'];
			$toName = $document1['toname'];
			$toVersion = $document1['toversion'];

			/* init some basics objects */
			$factory = DaoFactory::get($spacename);
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);

			try {
				/* load document */
				$document = new Document\Version();
				$docDao = $factory->getDao(Document\Version::$classId);
				$docDao->loadFromId($document, $documentId);
				/* load docfiles - require by service to copy with docfiles */
				$dfDao = $factory->getDao(Docfile\Version::$classId);
				$dfDao->loadDocfiles($document);

				/* Check permission */
				$this->getAcl()->checkRightFromUid('read', $document->parentUid);

				/* load container */
				$containerId = $document->getParent(true);
				$container = $factory->getDao(Workitem::$classId)->loadFromId(new Workitem(), $containerId);

				/* inject container object in document */
				$document->setParent($container);

				/* load target */
				$toContainer = $factory->getModel(Workitem::$classId);
				$factory->getDao(Workitem::$classId)->loadFromId($toContainer, $toContainerId);

				/* Check permission */
				$this->getAcl()->checkRightFromUid('create', $toContainer->getUid());

				$newDocument = $service->copyDocument($document, $toContainer, $toName, $toVersion);
			}
			catch( \Exception $e ) {
				$error = new \Service\Controller\ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new \Service\Controller\ServiceFeedback(null, $newDocument);
			$return->addFeedback($index, $feedback);
		}

		return $return;
	}

	/**
	 * Associate file to document.
	 *
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request $request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 			files:{
	 * 				file1:{
	 * 					name:string,
	 * 					role:integer,
	 * 					data:{
	 * 						name:string,
	 * 						md5:string,
	 * 						data:string,
	 * 					}
	 * 				},
	 * 				file2{...},
	 * 				...
	 * 			},
	 * 			toname:string,
	 * 			toversion:integer,
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * file1.data.data must be defined as base64 datas or
	 * file must be load from wildspace if data.data = 'FROM_WS'.
	 *
	 * In this last case, file1.data.name must be set with the name of the wildspace file.
	 *
	 * If data is base64, data will be temporary write in ws file with name set by file1.data.name, or if not set
	 * with a uniqid.
	 *
	 * file1.name set the name of data as he must be recorded in vault.
	 *
	 * file1.data.md5 is calculated from decoded raw data.
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function assocfileService($request = null)
	{
		$return = new ServiceRespons($this);

		/* @var \Zend\Http\Request $request */
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP POST method request');
		}

		$documents = $request->getPost('documents', null);

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5020, $this);
		}

		$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
		$registry = new Registry();
		$token = uniqid();
		$ranchbe = Ranchbe::get();

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document1'; isset($documents[$index]); $index = 'document' . ++$i) {
			/** @var $document1 array is the current entry in input datas */
			$document1 = $documents[$index];
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];

			/** @var $files array list of files to associate */
			$files = $document1['files'];

			try {
				/* load work objects */
				if ( !$document = $registry->getFromId($documentId, Document\Version::$classId) ) {
					/* load helpers */
					$factory = DaoFactory::get($spacename);
					$service = $ranchbe->getServiceManager()->getDocumentService($factory);

					$document = new Document\Version();
					$dao = $factory->getDao(Document\Version::$classId);
					$dao->loadFromId($document, $documentId);
					$containerId = $document->getParent(true);
					$container = $factory->getDao(Workitem::$classId)->loadFromId(new Workitem(), $containerId);
					$document->setParent($container);
					$registry->add($document);

					/* Check permission */
					$this->getAcl()->checkRightFromUid('create', $document->parentUid);

					/* set a token to check rights validation on next loops */
					$document->token = $token;
					$document->factory = $factory;
					$document->service = $service;
					$document->dao = $dao;
				}
				else {
					if ( $document->token != $token ) {
						continue;
					}

					$factory = $document->factory;
					$service = $document->service;
				}

				/* ------------- parse the files from input ------------- */
				for ($j = 1, $findex = 'file1'; isset($files[$findex]); $findex = 'file' . ++$j) {
					/* init file to associate */
					$file1 = $files[$findex];

					/**@var $fileName string Name of file as must be saved in vault */
					isset($file1['name']) ? $fileName = basename($file1['name']) : $fileName = uniqid();

					/**@var $wsFilename string Name of data file in the ws */
					isset($file1['data']['name']) ? $wsFilename = basename($file1['data']['name']) : $wsFilename = uniqid();

					/**@var $role integer Docfile role id */
					isset($file1['role']) ? $role = (int)($file1['role']) : $role = 8;

					/* check data type */
					$data = $file1['data']['data'];
					if ( $data == self::FROM_WS ) {
						/* take the existing ws file */
						$wsFile = $wildspace->getPath() . '/' . $wsFilename;
					}
					else {
						/* Put data in ws file */
						try {
							$data = base64_decode($file1['data']['data']);
							/* check md5 */
							if ( md5($data) != $file1['data']['md5'] ) {
								throw new ServiceException(sprintf('Md5 error'), 5014, $this);
							}
							$wsFile = $wildspace->getPath() . '/' . $wsFilename;
							file_put_contents($wsFile, $data);
						}
						catch( \Exception $e ) {
							throw new ServiceException(sprintf('Error during file upload of index %s: %s', index, $e->getMessage()), 5013, $this);
						}
					}

					if ( !is_file($wsFile) ) {
						throw new ServiceException(sprintf('file %s is not in wilspace for index %s', $wsFilename, $index), 5011, $this);
					}

					if ( !$fileName ) {
						throw new ServiceException(sprintf('var $filename is not set for index %s', $index), 5012, $this);
					}

					$file = $wildspace->getPath() . '/' . $fileName;
					if ( $fileName && is_file($file) ) {
						$ranchbe->log(sprintf('Associate file %s to document %s with role %s', $file, $document->getNumber(), $role));
						$service->associateFile($document, $file, $fileName, $role);
					}
				} /* forend */
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new \Service\Controller\ServiceFeedback(null, $document);
			$return->addFeedback($index, $feedback);
		} /* forend */

		return $return;
	}
} /* End of class */

