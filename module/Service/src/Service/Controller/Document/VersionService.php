<?php
namespace Service\Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Ranchbe;
use Rbplm\Ged\Document;
use Rbplm\Org\Workitem;
use Rbplm\Ged\AccessCode;
use Rbplm\Ged\AccessCodeException;
use Service\Controller\ServiceException;
use Service\Controller\ServiceFeedback;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Rbplm\People;

/**
 */
class VersionService extends AbstractController
{

	/**
	 * Upgrade version of document
	 * 
	 * HTTP POST method
	 *
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * documents:[
	 * 		document1:{
	 * 			id:string,
	 * 			spacename:string,
	 * 			version:integer,
	 * 			to:integer
	 * 		},
	 * 		document2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 * 
	 * document1.version is the version id for the new version.
	 * document1.to is the id of the target container where to create the new document.
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function newversionService($request = null)
	{
		$return = new \Service\Controller\ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isPost() ) {
			throw new ServiceException('Request must be post');
		}

		$documents = $request->getPost('documents', null);
		$toContainerId = $request->getPost('tocontainerid', null);
		$version = $request->getPost('version', null);
		$comment = $request->getPost('comment', '');

		if ( !$documents ) {
			$documentId = $request->getPost('id', null);
			$spacename = $request->getPost('spacename', null);
			if ( $documentId && $spacename ) {
				$documents = [
					'document1' => [
						'id' => $documentId,
						'spacename' => $spacename
					]
				];
			}
		}

		if ( !($documents) ) {
			throw new ServiceException('documents is not set or is not a array', 5050, $this);
		}

		if ( $version ) {
			$version = (int)(trim($version));
		}
		if ( $toContainerId ) {
			$toContainerId = (int)(trim($toContainerId));
		}

		/* Init batch */
		$batch = \Rbs\Batch\Batch::init();
		$batch->setOwner(People\CurrentUser::get())->setComment($comment);
		DaoFactory::get()->getDao($batch->cid)->save($batch);

		/* ------------- parse the document from input ------------- */
		for ($i = 1, $index = 'document' . $i; isset($documents[$index]); $index = 'document' . ++$i) {
			/* document1 is the current entry in input data */
			$document1 = $documents[$index];
			/* set vars */
			$documentId = $document1['id'];
			$spacename = $document1['spacename'];

			/* init some objects */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);
			$loader = $factory->getLoader();

			/* Set some properties used by checkin service */
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($factory);
			$service->batch = $batch;

			try {
				$document = $dao->loadFromId(new Document\Version(), $documentId);
				$containerId = $document->getParent(true);
				$container = $loader->loadFromId($containerId, Workitem::$classId);
				$document->setParent($container);

				/* Check permission */
				$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

				if ( $toContainerId ) {
					$toContainer = $loader->loadFromId($toContainerId, Workitem::$classId);
					/* Check permission */
					$this->getAcl()->checkRightFromUid('create', $toContainer->getUid());
				}
				else {
					$toContainer = null;
				}

				/* check access */
				$aCode = $document->checkAccess();
				if ( $aCode != AccessCode::FREE && $aCode < AccessCode::LOCKEDLEVEL1 ) {
					throw new AccessCodeException(sprintf(tra('this document is locked with code %s, %s is not permit'), $aCode, 'New Version'));
				}

				/* Call service */
				$newDocument = $service->newVersion($document, $toContainer, $version);
			}
			catch( AccessCodeException $e ) {
				$error = new ServiceError('Access Code Error', $e, $document);
				$return->addError($index, $error);
				continue;
			}
			catch( \Exception $e ) {
				$error = new ServiceError($e->getMessage(), $e, $document);
				$return->addError($index, $error);
				continue;
			}

			$feedback = new ServiceFeedback(sprintf(tra('New %s in version %s'), $newDocument->getNumber(), $newDocument->version), $newDocument);
			$return->addFeedback($index, $feedback);
		} /* forend */

		$service->batch = null;
		return $return;
	}
} /* End of class */
