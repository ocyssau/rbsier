<?php
namespace Service\Controller\Space;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Space\Space;
use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;
use Rbplm\Dao\Filter\Op;

/**
 * Get and set some datas from one document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	id:string,
 *	spacename:string
 * ]
 * ~~~~~~~~~
 * 
 */
class ListService extends ServiceController
{

	/**
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 *        	<code>
	 *        	** 'select':array 	properties name to return
	 *        	** 'limit':int 		number of rows to return
	 *        	** 'page':int 		from page number
	 *        	** 'filter:array 	of array where 0 is name of var, 1 is value, 2 is operator as define by constants of \Rbplm\Dao\Filter\Op
	 *        	** 'search:array 	words to be search in full text research
	 *        	</code>
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter [OPIONNAL] Filter object to inject in List
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function searchService($request = null, $filter = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		if ( !$request->isGet() ) {
			throw new ServiceException(__FUNCTION__ . ' require HTTP GET method request');
		}

		$page = $request->getQuery('page', 1);
		$limit = $request->getQuery('limit', 1000);
		$userSelect = $request->getQuery('select', null);
		$userFilter = $request->getQuery('filter', null);
		$searchs = $request->getQuery('search', null);

		try {
			/* init some objects */
			$factory = DaoFactory::get();
			$list = $factory->getList(Space::$classId);
			$filter = $factory->getFilter(Space::$classId);
			$dao = $list->dao;
			$trans = $factory->getTranslator($dao);

			/**/
			if ( $userSelect ) {
				$select = [];
				foreach( $userSelect as $asApp ) {
					if ( $asSys = $trans->toSys($asApp) ) {
						$select[] = $asSys . ' AS ' . $asApp;
					}
					else {
						throw new ServiceException(sprintf('Selection of %s impossible', $asApp));
					}
				}
				$filter->select($select);
			}
			else {
				$select = $trans->getSelectAsApp();
				$filter->select($select);
			}

			/**/
			if ( $userFilter ) {
				$i = 0;
				foreach( $userFilter as $f ) {
					if ( count($f) != 3 ) {
						throw new ServiceException(sprintf('Filter %s must have tree item: name, value, op', var_export($f, true)));
					}
					$asApp = $f[0];
					if ( $asSys = $trans->toSys($asApp) ) {
						$op = constant('\Rbplm\Dao\Filter\Op::' . $f[2]);
						$what = $f[1];
						$filter->andfind($what, $asSys, $op);
					}
					else {
						throw new ServiceException(sprintf('Filter %s is not correctly formated', var_export($f, true)));
					}
					$i++;
				}
			}

			/**/
			if ( $searchs ) {
				$i = 0;
				foreach( $searchs as $term ) {
					$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, '-')", $trans->toSys('name'), $trans->toSys('description'));
					$filter->andFind($term, $concat, Op::OP_CONTAINS);
				}
			}

			$filter->page($page, $limit);
			$list->load($filter);
			$return->setData($list->toArray());
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError(1, $error);
		}

		return $return;
	}
} /* End of class */
