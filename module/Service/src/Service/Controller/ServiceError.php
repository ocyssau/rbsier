<?php
namespace Service\Controller;

/**
 *
 *
 */
class ServiceError extends \Exception
{

	/**
	 * Data is object concerned by exception
	 *
	 * @var \stdClass
	 */
	protected $data;

	/**
	 * @param string $message
	 * @param \Exception $exception Previous exeption
	 */
	function __construct($message, \Exception $exception = null, $data = null)
	{
		parent::__construct($message, E_USER_ERROR, $exception);
		$this->data = $data;
	}

	/**
	 *
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * implements JsonSerializable
	 */
	public function jsonSerialize($e = null)
	{
		if ( !$e ) {
			$e = $this;
		}
		
		$previous = $e->getPrevious();
		
		if ( $e instanceof ServiceError ) {
			$data = $e->getData();
		}
		else {
			$data = null;
		}
		
		return [
			'message' => $e->getMessage(),
			'code' => $e->getCode(),
			'file' => $e->getFile(),
			'line' => $e->getLine(),
			'data' => ($data) ? json_encode($data) : null,
			'trace' => $e->getTraceAsString(),
			'previous' => ($previous) ? $this->jsonSerialize($previous) : null
		];
	}
}



