<?php
namespace Service\Controller\Portail;

use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\People;
use Portail\Model\Component;

/**
 * Get and set some datas from one document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	id:string,
 *	spacename:string
 * ]
 * ~~~~~~~~~
 * 
 */
class ComponentService extends ServiceController
{

	
	/**
	 * GET Method
	 * 
	 * 
	 * HTTP POST method
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 * ]
	 * ~~~~~~~~~
	 *
	 *
	 * return properties as json :
	 * {
	 * 'id':'',
	 * 'updated':'',
	 * 'updatebyid':'',
	 * 'updatebyname':''
	 * }
	 *
	 * @param \Zend\Http\Request $request
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function saveService(\Zend\Http\Request $request = null)
	{
		if($request === null){
			$request = $this->getRequest();
		}
		$return = new ServiceRespons($this);
		
		if($request->isPost() == false){
			throw new ServiceException(__METHOD__ . ' accept only HTTP/POST method');
		}
		
		if ( isset($request->component) && $request->component instanceof Component\AbstractComponent) {
			$component = $request->component;
		}
		else {
		}
		
		if ( isset($request->properties) ) {
			$properties = $request->properties;
		}
		else {
		}
		
		//populate with commons properties
		$properties['name'] = $request->getPost('name', null);
		$properties['index'] = $request->getPost('index', null);
		$properties['parentId'] = $request->getPost('parentid', null);
		$properties['parentUid'] = $request->getPost('parentuid', null);
		$properties['attributes']['title'] = $request->getPost('title', '{empty}');
		$properties['attributes']['xoffset'] = $request->getPost('xoffset', null);
		$properties['attributes']['yoffset'] = $request->getPost('yoffset', null);
		$properties['attributes']['zindex'] = $request->getPost('zindex', null);
		$properties['attributes']['draggable'] = $request->getPost('draggable', null);
		$properties['attributes']['resizable'] = $request->getPost('resizable', null);
		$properties['attributes']['resizablex'] = $request->getPost('resizablex', null);
		$properties['attributes']['resizabley'] = $request->getPost('resizabley', null);
		$properties['attributes']['collapsed'] = $request->getPost('collapsed', false);
		
		//convert visibility code to visibility name
		/*
		 $visibility = $request->getPost('visibility', null);
		 switch (strtolower($visibility)) {
		 case 'private':
		 $visibility = AclSiergate::VISIBILITY_PRIVATE;
		 break;
		 case 'internal':
		 $visibility = AclSiergate::VISIBILITY_INTERNAL;
		 break;
		 case 'public':
		 $visibility = AclSiergate::VISIBILITY_PUBLIC;
		 break;
		 }
		 $properties['visibility'] = $visibility;
		 */
		
		try{
			$component->hydrate($properties);
			
			if ( !$component->getId() ) {
				$component->setOwner(People\CurrentUser::get());
			}
			
			$component->setUpdateBy(People\CurrentUser::get());
			$dao = DaoFactory::get()->getDao($component->cid);
			$dao->save($component);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e);
			$return->addError(1, $error);
		}
		
		$return->setData([
			'uid' => $component->getUid(),
			'id' => $component->getId(),
			'updated' => $component->getUpdated(),
			'updatedByName' => People\CurrentUser::get()->getId(),
			'updatedById' => People\CurrentUser::get()->getId(),
			'component' => $component
		]);
		
		return $return;
	}
	
	/**
	 * GET Method
	 * Build a new ref, reserve it in db, and return in respons.
	 * docseeder property may be set to $request->docseeder property, if not a new Docseeder instance will be create.
	 * The docseeder instance is exposed in this->docseeder property
	 *
	 * HTTP POST method
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 *	spacename:string,
	 *	containerid:int,
	 *  doctypeid:int,
	 *  description:string,
	 *  label:string
	 * ]
	 * ~~~~~~~~~
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
}

