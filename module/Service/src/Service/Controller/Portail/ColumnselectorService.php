<?php
namespace Service\Controller\Portail;

use Service\Controller\ServiceError;
use Service\Controller\ServiceRespons;
use Service\Controller\ServiceController;
use Service\Controller\ServiceException;

/**
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	paramName:paramValue,
 * ]
 * ~~~~~~~~~
 * 
 */
class ColumnselectorService extends ServiceController
{

	/**
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 *        	<code>
	 *        	** 'searchengine':string 	Class name of search engine from where extract metamodel
	 *        	</code>
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function getfromsearchengineService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;

		try {
			$searchEngineClass = $request->getQuery('searchengine', null);
			$metamodel = $searchEngineClass::$metamodel;
			$return->setData($metamodel);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError(1, $error);
		}

		return $return;
	}
	
	/**
	 * HTTP GET method
	 *
	 * @param \Zend\Http\Request $request [OPTIONAL]
	 *        	<code>
	 *        	** 'filterform':string 	Class name of filter form
	 *        	</code>
	 *
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function getfromfilterformService($request = null)
	{
		$return = new ServiceRespons($this);
		(!$request) ? $request = $this->getRequest() : null;
		
		try {
			$config = \Ranchbe::get()->getMvcApplication()->getConfig();
			$filterformClass = $request->getQuery('filterform', null);
			$searchEngineClass = $config['portail']['di'][$filterformClass]['searchEngine'];
			$metamodel = $searchEngineClass::$metamodel;
			$return->setData($metamodel);
		}
		catch( \Exception $e ) {
			$error = new ServiceError($e->getMessage(), $e, null);
			$return->addError(1, $error);
		}
		
		return $return;
	}
}
