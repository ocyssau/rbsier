<?php
namespace Service\Controller\Portail;

use Service\Controller\ServiceRespons;
use Service\Controller\ServiceException;
use Rbs\Space\Factory as DaoFactory;
use Portail\Model\Component;

/**
 * Get and set some datas from one document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	id:string,
 *	spacename:string
 * ]
 * ~~~~~~~~~
 * 
 */
class DocumentlistService extends \Service\Controller\Document\ListService
{

	/**
	 * GET Method
	 * 
	 * 
	 * HTTP POST method
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 * ]
	 * ~~~~~~~~~
	 *
	 *
	 * return properties as json :
	 * {
	 * 'id':'',
	 * 'updated':'',
	 * 'updatebyid':'',
	 * 'updatebyname':''
	 * }
	 *
	 * @param \Zend\Http\Request $request
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function getitemsService(\Zend\Http\Request $request = null)
	{
		if ( $request == null ) {
			$request = $this->getRequest();
		}

		$uid = $request->getQuery('uid', null);
		$spacename = $request->getQuery('spacename', null);

		$factory = DaoFactory::get($spacename);
		$item = new Component\Documentlist\Item();
		/* @var \Portail\Dao\Component\Documentlist\ItemDao $dao */
		$itemDao = $factory->getDao($item->cid);
		$filter = $this->getFilter();
		$filter->andfind($uid, 'item.' . $itemDao->toSys('parentUid'));

		$s = [
			'name AS itemname',
			'number AS itemdata'
		];
		$filter->with([
			'table' => $itemDao->getTable(),
			'righton' => $itemDao->toSys('childUid'),
			'lefton' => 'uid',
			'alias' => 'item',
			'select' => $s
		]);

		return $this->searchService($request, $filter);
	}
}
