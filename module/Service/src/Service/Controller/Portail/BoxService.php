<?php
namespace Service\Controller\Portail;

use Service\Controller\ServiceRespons;
use Service\Controller\ServiceException;
use Rbs\Space\Factory as DaoFactory;
use Portail\Model\Component;

/**
 * Get and set some datas from one document.
 * 
 * All methods of this service accepts json datas as input parameter with structure
 * as :
 * ~~~~~~~~~{.json}
 * [
 *	id:string,
 *	spacename:string
 * ]
 * ~~~~~~~~~
 * 
 */
class BoxService extends ComponentService
{

	
	/**
	 * GET Method
	 * 
	 * 
	 * HTTP POST method
	 * @param \Zend\Http\Request
	 * ~~~~~~~~~{.json}
	 * [
	 * ]
	 * ~~~~~~~~~
	 *
	 *
	 * return properties as json :
	 * {
	 * 'id':'',
	 * 'updated':'',
	 * 'updatebyid':'',
	 * 'updatebyname':''
	 * }
	 *
	 * @param \Zend\Http\Request $request
	 * @throws ServiceException
	 * @return ServiceRespons
	 */
	public function saveService(\Zend\Http\Request $request = null)
	{
		$request = $this->getRequest();
		
		$uid = $request->getPost('uid', null);
		$id = $request->getPost('id', null);
		$body = $request->getPost('body', '{empty}');
		
		$component = new Component\Box();
		$dao = DaoFactory::get()->getDao($component->cid);
		
		if ( $id ) {
			$dao->loadFromUid($component, $uid);
			$component->setUpdated(new \DateTime());
		}
		else {
			$component = $component::init();
		}
		
		$properties = [
			'uid' => $uid,
			'attributes' => [
				'body' => $body
			]
		];
		
		return $this->_saveComponent($request, $component, $properties);
	}
}
