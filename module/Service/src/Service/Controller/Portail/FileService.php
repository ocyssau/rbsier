<?php
namespace Service\Controller\Portail;

use Rbplm\Sys\Directory;
use Service\Controller\ServiceController;
use Zend\Http\Request as HttpRequest;
use Service\Controller\ServiceException;

#use Service\Controller\ServiceRespons;
#use Service\Controller\ServiceFeedback;
#use Service\Controller\ServiceError;
#use Service\Controller\Application\AbstractFilemanager;

/**
 * Manage files in the user wildspace.
 *
 */
class FileService extends ServiceController
{

	/**
	 * Add files to the wildspace
	 * HTTP POST method
	 *
	 * @param HttpRequest $request
	 * ~~~~~~~~~{.json}
	 * files:[
	 * 		'file1':{
	 * 			'name':string,
	 * 			'md5':string,
	 * 			'size':string,
	 * 			'data':string BASE64_ENCODED,
	 * 		},
	 * 		file2:{...},
	 * 		...
	 * 	}
	 * ]
	 * ~~~~~~~~~
	 *
	 * @return array
	 */
	public function uploadService(HttpRequest $request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;

		if ( $request->isPost() == false ) {
			throw new ServiceException(__FUNCTION__ . ' accept only POST HTTP Method');
		}

		$file = $request->getFiles('file');

		if ( !(is_uploaded_file($file['tmp_name'])) ) {
			throw new ServiceException('files is not uploaded', 5030, $this);
		}
		
		$filename = $file['name'];
		$tmpname = $file['tmp_name'];
		$path = 'data/upload/portail/img';
		#$path = \Ranchbe::get()->getConfig()['portail']['image_upload_path'];
		
		$directory = new Directory($path);
		if ( !is_writable($directory->getPath()) ) {
			throw new ServiceException(sprintf('Directory %s is not writable', $directory->getPath()), 5030, $this);
		}

		try{
			$destination = $directory->getPath($filename);
			if(is_file($destination)){
				$c1 = md5_file($destination);
				$c2 = md5_file($tmpname);
				if($c1 != $c2){
					$destination = $directory->getPath(uniqid());
					move_uploaded_file($tmpname, $destination);
				}
			}
			else{
				move_uploaded_file($tmpname, $destination);
			}
		}
		catch(\Throwable $e){
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}

		$url = $request->getBasePath() . '/service/portail/file/download?name=' . $filename;
		return ['location' => $url];
	}


	/**
	 * download a file from the wildspace
	 */
	public function downloadService(HttpRequest $request = null)
	{
		(!$request) ? $request = $this->getRequest() : null;
		
		if ( $request->isGet() == false ) {
			throw new ServiceException(__FUNCTION__ . ' accept only GET HTTP Method');
		}
		
		$filename = basename($request->getQuery('name', null));
		$path = 'data/upload/portail/img';
		$directory = new Directory($path);
		$file = $directory->getPath($filename);
		
		if ( !is_readable($file) ) {
			throw new ServiceException(sprintf('File %s is not readable', $file), 5032, $this);
		}
		
		$fsdata = new \Rbplm\Sys\Fsdata($file);
		$fsdata->download();
	}


} /* End of class */
