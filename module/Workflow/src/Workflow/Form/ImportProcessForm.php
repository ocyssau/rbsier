<?php
namespace Workflow\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class ImportProcessForm extends Form implements InputFilterProviderInterface
{

	protected $inputFilter;

	public $template;

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('workunit');
		$this->template = 'workflow/process/import';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$this->add(array(
			'name' => 'file',
			'type' => 'Zend\Form\Element\File',
			'attributes' => array(
				'class' => 'file'
			),
			'options' => array(
				'label' => 'File To Import'
			)
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Load',
				'id' => 'submitbutton'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'file' => array(
				'required' => true
			)
		);
	}
}
