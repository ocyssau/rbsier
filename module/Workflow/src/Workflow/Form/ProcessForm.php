<?php
namespace Workflow\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class ProcessForm extends Form implements InputFilterProviderInterface
{

	protected $inputFilter;

	public $template;

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('workunit');

		$this->template = 'workflow/process/editForm';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Name'
			)
		));

		$this->add(array(
			'name' => 'title',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Description',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Description'
			)
		));

		$this->add(array(
			'name' => 'isActive',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'placeholder' => 'isValid',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'isActive'
			)
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'name' => array(
				'required' => true
			)
		);
	}
}
