<?php
namespace Workflow\Form;

use Zend\Form\Form;

/**
 * 
 *
 */
class InstanceFilterForm extends Form // implements InputFilterAwareInterface
{

	/**
	 * 
	 * @var string
	 */
	public $where;

	/**
	 * 
	 * @var string
	 */
	public $passThrough = true;

	/**
	 * key for search input
	 * @var string
	 */
	public $key1;

	/**
	 * key for search input
	 * @var string
	 */
	public $key2;

	/**
	 * 
	 * @var array
	 */
	public $bind = array();

	/**
	 *
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('contextfilter');
		$this->setAttribute('id', 'contextfilter');
		$this->setAttribute('method', 'post');
		$this->setAttribute('class', 'form-inline');

		$this->add(array(
			'name' => 'stdfilter-id',
			'type' => 'Zend\Form\Element\Hidden'
		));

		$this->add(array(
			'name' => 'stdfilter-searchInput',
			'type' => 'Zend\Form\Element\Text',
			'options' => array(
				'label' => ''
			),
			'attributes' => array(
				'onChange' => '',
				'class' => 'form-control'
			)
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Submit',
			'name' => 'stdfilter-submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Filter',
				'id' => 'stdfilter-submit',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Form.Form::prepare()
	 */
	public function prepare()
	{
		parent::prepare();
		$where = array();
		$search = $this->get('stdfilter-searchInput')->getValue();
		$id = $this->get('stdfilter-id')->getValue();

		if ( $search ) {
			$where[] = $this->key1 . " LIKE :search";
			if ( $this->passThrough ) {
				$search = str_replace('*', '%', $search);
				$this->bind[':search'] = '%' . $search . '%';
			}
			else {
				$search = str_replace('*', '%', $search);
				$this->bind[':search'] = $search;
			}
		}
		if ( $id ) {
			$where[] = $this->key2 . " = :id";
			$this->bind[':id'] = $id;
		}

		$this->where = implode(' AND ', $where);
	}

	/**
	 *
	 * @param \Application\View\ViewModel $view
	 */
	public function render($view)
	{
		$html = $view->form()->openTag($this);
		$html .= '<div class="form-group">' . $view->formRow($this->get('stdfilter-searchInput')) . '</div>';
		$html .= '<div class="form-group">' . $view->formRow($this->get('stdfilter-submit')) . '</div>';
		$html .= '<div class="form-group">' . $view->formRow($this->get('stdfilter-id')) . '</div>';
		$html .= $view->form()->closeTag();
		return $html;
	}

	/**
	 *
	 * @param string $key
	 *        	Name of session key
	 */
	public function saveToSession($key)
	{
		$_SESSION[$key]['stdfilter-searchInput'] = $this->get('stdfilter-searchInput')->getValue();
	}

	/**
	 *
	 * @param string $key
	 *        	Name of session key
	 */
	public function loadFromSession($key)
	{
		if ( isset($_SESSION[$key]) ) {
			$data = $_SESSION[$key];
			$this->get('stdfilter-searchInput')->setValue($data['stdfilter-searchInput']);
		}
	}
}

