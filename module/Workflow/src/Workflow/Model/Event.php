<?php
namespace Workflow\Model;

use Workflow\Service\Workflow as Emitter;

/**
 * VERY generic to see if a task can be stopped
 */
class Event implements EventInterface
{

	const START_PRE = 'start.pre';

	const START_POST = 'start.post';

	const RUNACTIVITY_PRE = 'runactivity.pre';

	const RUNACTIVITY_POST = 'runactivity.post';

	const TRANSLATE_PRE = 'translate.pre';

	const TRANSLATE_POST = 'translate.post';

	const COMPLET_POST = 'complete.post';

	const SAVEPROCESS_PRE = 'saveprocess.pre';

	const SAVEPROCESS_POST = 'saveprocess.post';

	const DELETEPROCESS_PRE = 'deleteprocess.pre';

	const DELETEPROCESS_POST = 'deleteprocess.post';

	const SAVEACTIVITY_POST = 'saveactivity.post';

	const DELETEACTIVITY_POST = 'deleteactivity.post';

	const DELETEINSTANCE_POST = 'deleteinstance.post';

	/**
	 *
	 * @var string
	 */
	protected $name;

	/**
	 *
	 * @var object
	 */
	protected $emitter;

	/**
	 *
	 * @param string $name
	 * @param Emitter $emitter
	 */
	public function __construct(string $name, Emitter $emitter)
	{
		$this->setName($name);
		$this->emitter = $emitter;
	}

	/**
	 *
	 * @return object
	 */
	public function getEmitter()
	{
		return $this->emitter;
	}

	/**
	 *
	 * @return string
	 */
	public function getName(): String
	{
		return $this->name;
	}

	/**
	 *
	 * @param string $name
	 * @return Event
	 */
	public function setName(string $name)
	{
		$this->name = $name;
		return $this;
	}
}
