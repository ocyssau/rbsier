<?php
//%LICENCE_HEADER%
namespace Workflow\Model;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
class Factory
{

	/**
	 * Registry of instanciated DAO.
	 *
	 * @var array
	 */
	private static $_registry = array();

	/**
	 * Assiciate for each Component Class ID a DAO CLASS
	 * ClassId 200 return the super class DAO Component/Component
	 * @var array
	 */
	private static $_map = array(
		420 => array(
			'Wf\Process',
			'wf_process'
		),
		421 => array(
			'Wf\Activity\Activity',
			'wf_activity'
		),
		422 => array(
			'Wf\Activity\Start',
			'wf_activity'
		),
		423 => array(
			'Wf\Activity\End',
			'wf_activity'
		),
		424 => array(
			'Wf\Activity\Join',
			'wf_activity'
		),
		425 => array(
			'Wf\Activity\Split',
			'wf_activity'
		),
		426 => array(
			'Wf\Activity\Aswitch',
			'wf_activity'
		),
		427 => array(
			'Wf\Activity\Standalone',
			'wf_activity'
		),
		428 => array(
			'Wf\Activity',
			'wf_activity'
		),
		430 => array(
			'Wf\Transition',
			'wf_transition'
		),
		440 => array(
			'Wf\Instance',
			'wf_instance'
		),
		441 => array(
			'Wf\Instance\Activity',
			'wf_activity_instance'
		),
		442 => array(
			'Link',
			''
		),
		450 => array(
			'Wf\Instance\Standalone',
			'wf_activity_instance'
		)
	);

	/**
	 * Return a instance of class specified by id.
	 *
	 * @param integer $classId
	 * @return \Rbplm\Dao\DaoInterface
	 */
	public static function get($classId)
	{
		$class = __NAMESPACE__ . '\\' . self::$_map[$classId][0];
		return new $class();
	}

	/**
	 * Return a initialized instance of class specified by id.
	 *
	 * @param integer $classId
	 * @return \Rbplm\Dao\DaoInterface
	 */
	public static function getNew($classId)
	{
		$class = __NAMESPACE__ . '\\' . self::$_map[$classId][0];
		return $class::init();
	}
}
