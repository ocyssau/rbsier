<?php
namespace Workflow\Model\Wf;

use Workflow\Model\Link;

/**
 */
class Transition extends Link
{

	/**
	 * 
	 * @var string
	 */
	public static $classId = '56acc299ed150';

	/**
	 * Id of the process
	 * @var integer
	 */
	public $processId;

	/**
	 *
	 * @param Activity $from
	 * @param Activity $to
	 * @return Transition
	 */
	public static function factory(Activity $from, Activity $to)
	{
		$transition = self::init();
		$transition->setParent($from);
		$transition->setChild($to);
		$transition->name = $from->getName() . '-' . $to->getName();
		return $transition;
	}

	/**
	 *
	 * @return string
	 */
	public function getStatus()
	{
		return $this->name;
	}

	/**
	 *
	 * @param string $string
	 * @return Transition
	 */
	public function setStatus($string)
	{
		$this->name = $string;
		return $this;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Transition
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['childId'])) ? $this->childId = $properties['childId'] : null;
		(isset($properties['parentId'])) ? $this->parentId = $properties['parentId'] : null;
		(isset($properties['childUid'])) ? $this->childUid = $properties['childUid'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid = $properties['parentUid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['processId'])) ? $this->processId = $properties['processId'] : null;

		if ( isset($properties['attributes']) ) {
			$attributes = $properties['attributes'];
			if ( is_array($attributes) ) {
				$this->attributes = $attributes;
			}
			elseif ( $attributes[0] = '{' ) {
				$this->attributes = json_decode($attributes);
			}
		}
		return $this;
	}
}

