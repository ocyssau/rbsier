<?php
namespace Workflow\Model\Wf;

/**
 * A class representing workitems
 * Not yet implemented
 */
class Workitem extends \Workflow\Model\Any
{

	/**
	 *
	 * @var Instance
	 */
	public $instance;

	/**
	 *
	 * @var array
	 */
	public $properties = array();

	/**
	 *
	 * @var integer
	 */
	public $started;

	/**
	 *
	 * @var integer
	 */
	public $ended;

	/**
	 *
	 * @var Activity
	 */
	public $activity;
}
