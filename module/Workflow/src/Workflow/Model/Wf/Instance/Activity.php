<?php
// %LICENCE_HEADER%
namespace Workflow\Model\Wf\Instance;

use DateTime;
use Rbplm\People;
use Workflow\Model\Wf;
use Exception;

/**
 */
class Activity extends Wf\Activity
{

	const STATUS_RUNNING = 'running';

	const STATUS_ABORTED = 'aborted';

	const STATUS_ACTIVE = 'active';

	const STATUS_COMPLETED = 'completed';

	const STATUS_EXCEPTION = 'exception';

	const SIGNAL_RUN_PRE = 'runactivity.pre';

	const SIGNAL_RUN_POST = 'runactivity.post';

	/**
	 *
	 * @var integer
	 */
	public static $classId = '56acc299ed22a';

	/**
	 *
	 * @var array
	 */
	static $_registry = array();

	/**
	 *
	 * @var string
	 */
	protected $status = null;

	/**
	 *
	 * @var DateTime
	 */
	protected $started = null;

	/**
	 *
	 * @var DateTime
	 */
	protected $ended = null;

	/**
	 *
	 * @var Wf\Workitem
	 */
	protected $workitem = null;

	protected $workitemId = null;

	/**
	 *
	 * @var Wf\Instance
	 */
	protected $instance = null;

	protected $instanceId = null;

	/**
	 *
	 * @var Wf\Activity
	 */
	protected $activity = null;

	protected $activityId = null;

	/**
	 * Collection of Wf\Activity
	 *
	 * @var array
	 */
	protected $previous = null;

	/**
	 *
	 * @var string
	 */
	protected $comment = null;

	/**
	 *
	 * @var array
	 */
	public $actAttributes = array();

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Activity
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['instanceId'])) ? $this->instanceId = $properties['instanceId'] : null;
		(isset($properties['instanceUid'])) ? $this->instanceUid = $properties['instanceUid'] : null;

		(isset($properties['activityId'])) ? $this->activityId = $properties['activityId'] : null;
		(isset($properties['activityUid'])) ? $this->activityUid = $properties['activityUid'] : null;

		(isset($properties['status'])) ? $this->status = $properties['status'] : null;
		(isset($properties['type'])) ? $this->type = $properties['type'] : null;

		if ( isset($properties['actAttributes']) ) {
			$fc = $properties['actAttributes'][0];
			if ( $fc == '[' || $fc == '{' ) {
				$properties['actAttributes'] = json_decode($properties['actAttributes'], true);
			}
			$this->actAttributes = $properties['actAttributes'];
		}
		;

		if ( isset($properties['started']) ) {
			$date = $properties['started'];
			if ( is_a($date, 'DateTime') ) {
				$this->started = $date;
			}
			elseif ( $date ) {
				$this->started = new \DateTime($date);
			}
		}

		if ( isset($properties['ended']) ) {
			$date = $properties['ended'];
			if ( is_a($date, 'DateTime') ) {
				$this->ended = $date;
			}
			elseif ( $date ) {
				$this->ended = new \DateTime($date);
			}
		}

		return $this;
	}

	/**
	 * Start a new activity instance.
	 * Only one activity_instance must be create for each activity.
	 * This method ensure that this rule is respected by compare activity name with internal registry.
	 *
	 * @param Wf\Activity $activity
	 * @param Wf\Instance $instance
	 * @return Activity
	 */
	public static function start(Wf\Activity $activity, Wf\Instance $instance)
	{
		$id = $instance->getUid() . '_' . $activity->getName();

		if ( isset(self::$_registry[$id]) ) {
			return self::$_registry[$id];
		}

		$instanceAct = new Activity($activity->getArrayCopy());
		$instanceAct->newUid();
		$instanceAct->setActivity($activity);
		$instanceAct->setInstance($instance);
		$instanceAct->updated = new DateTime();
		$instanceAct->setOwner(People\CurrentUser::get());
		$instanceAct->status = self::STATUS_RUNNING;
		$instanceAct->started = new DateTime();
		self::$_registry[$id] = $instanceAct;
		return $instanceAct;
	}

	/**
	 * Prepare and execute code of activity
	 * Return boolean
	 *
	 * this method is call by instance::sendTo methods to execute automatic activity
	 * she dont must be directly call to create a new instance.
	 *
	 * @return Activity
	 */
	public function execute()
	{
		if ( $this->status != self::STATUS_RUNNING ) {
			return;
		}

		$this->ended = new DateTime();
		$this->status = self::STATUS_COMPLETED;

		return $this;
	}

	/**
	 *
	 * @return Wf\Workitem
	 */
	public function getWorkitem()
	{
		if ( !$this->workitem ) {
			throw new Exception(sprintf('PROPERTY_%s_IS_NOT_SET', 'workitem'));
		}
		return $this->workitem;
	}

	/**
	 *
	 * @param Wf\Workitem $workitem
	 * @return Activity
	 */
	public function setWorkitem(Wf\Workitem $workitem)
	{
		$this->workitem = $workitem;
		$this->workitemId = $this->workitem->getUid();
		$this->getLinks()->add($this->workitem);
		return $this;
	}

	/**
	 *
	 * @return Wf\Instance
	 */
	public function getInstance($asId = false)
	{
		if ( $asId ) {
			return $this->instanceId;
		}
		else {
			return $this->instance;
		}
	}

	/**
	 * Set the process instance for this activity instance.
	 *
	 * @param Wf\Instance $instance
	 * @return Activity
	 */
	public function setInstance(Wf\Instance $instance, $bidirectional = false)
	{
		$this->instance = $instance;
		$this->instanceId = $instance->getId();
		$this->instanceUid = $instance->getUid();
		if ( $bidirectional ) {
			$instance->addActivity($this);
		}
		return $this;
	}

	/**
	 *
	 * @return Wf\Activity
	 */
	public function getActivity($asId = false)
	{
		if ( $asId ) {
			return $this->activityId;
		}
		else {
			return $this->activity;
		}
	}

	/**
	 *
	 * @param Wf\Activity $activity
	 * @return Activity
	 */
	public function setActivity(Wf\Activity $activity)
	{
		$this->activity = $activity;
		$this->activityUid = $activity->getUid();
		$this->activityId = $activity->getId();
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 *
	 * @param string $comment
	 * @return Activity
	 */
	public function setComment($comment)
	{
		$this->comment = $comment;
		return $this;
	}

	/**
	 *
	 * @return DateTime
	 */
	public function getEnded($format = null)
	{
		if ( $this->ended && $format ) {
			return $this->ended->format($format);
		}
		elseif ( $this->ended ) {
			return $this->ended;
		}
	}

	/**
	 *
	 * @param DateTime $date
	 * @return Activity
	 */
	public function setEnded($date)
	{
		$this->ended = $date;
		return $this;
	}

	/**
	 *
	 * @return DateTime
	 */
	public function getStarted($format = null)
	{
		if ( $this->started && $format ) {
			return $this->started->format($format);
		}
		elseif ( $this->started ) {
			return $this->started;
		}
	}

	/**
	 *
	 * @param string $string
	 * @return Activity
	 */
	public function setStatus($string)
	{
		$this->status = $string;
		return $this;
	}

	/**
	 *
	 * @param string $string
	 * @return Activity
	 */
	public function setType($string)
	{
		$this->type = $string;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 *
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 *
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 *
	 * @param array $array
	 * @return Activity
	 */
	public function setAttributes($array)
	{
		$this->attributes = $array;
		return $this;
	}
}
