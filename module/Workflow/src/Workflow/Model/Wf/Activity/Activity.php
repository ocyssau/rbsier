<?php
namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;

/**
 * This class handles activities of type 'activity'
 */
class Activity extends Wf\Activity
{

	/**
	 *
	 * @var string
	 */
	protected $type = Wf\Activity::TYPE_ACTIVITY;

	/**
	 * @var string
	 */
	public static $classId = '56acc299ecdd8';

	/**
	 * Shape use to generate graph.
	 *
	 * @var string
	 */
	protected $shape = 'box';
}
