<?php
namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;

/**
 * This class handles activities of type 'join'
 */
class Join extends Wf\Activity
{

	/**
	 *
	 * @var string
	 */
	protected $type = Wf\Activity::TYPE_JOIN;

	/**
	 *
	 * @var string
	 */
	public static $classId = '56acc299ecf1a';

	/**
	 * Shape use to generate graph.
	 *
	 * @var string
	 */
	protected $shape = 'invtriangle';
}

