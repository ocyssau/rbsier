<?php
// %LICENCE_HEADER%
namespace Workflow\Model\Wf;

/**
 */
class Comment
{

	/**
	 * Uuid
	 *
	 * @var string
	 */
	protected $uid = null;

	/**
	 *
	 * @var string
	 */
	protected $title = null;

	/**
	 *
	 * @var string
	 */
	protected $body = null;

	/**
	 *
	 * @param string $title
	 * @param string $body
	 * @return void
	 */
	public function __construct($title = '', $body = '')
	{
		$this->uid = \Rbplm\Uuid::newUid();
		$this->title = $title;
		$this->body = $body;
	}

	/**
	 *
	 * @return string
	 */
	public function getUid()
	{
		return $this->uid;
	}

	/**
	 *
	 * @param string $uid
	 */
	public function setUid($uid)
	{
		$this->uid = $uid;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 *
	 * @param string $title
	 * @return Comment
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 *
	 * @param string $comment
	 * @return Comment
	 */
	public function setBody($comment)
	{
		$this->body = $comment;
		return $this;
	}
}

