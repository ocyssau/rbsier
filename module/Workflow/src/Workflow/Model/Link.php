<?php
// %LICENCE_HEADER%
namespace Workflow\Model;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Link extends \Rbplm\Link
{

	public static $classId = '56acc299ed301';

	/**
	 *
	 * @var array
	 */
	public $attributes = null;

	/**
	 *
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
	}

	/**
	 * @param string $name
	 * @return string
	 */
	public function getAttribute($name)
	{
		return $this->attributes[$name];
	}

	/**
	 *
	 * @param array $array
	 * @return Link
	 */
	public function setAttributes($array)
	{
		$this->attributes = $array;
		return $this;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Link
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['attributes'])) ? $this->attributes = json_decode($properties['attributes']) : null;
		return $this;
	}
}
