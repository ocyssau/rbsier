<?php
namespace Workflow\Controller;

use Zend\View\Model\ViewModel;
use Workflow\Model\Wf;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;

/**
 * 
 *
 */
class ExportController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractController::dispatch()
	 *
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		$this->authIdentityCheck();
		\Zend\Mvc\Controller\AbstractActionController::dispatch($request, $respons);
		$this->resourceCn = \Acl\Model\Resource\Workflow::$appCn;
	}

	/**
	 */
	public function exportAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);
		$this->layout('layout/fragment');
		$view = new ViewModel();
		$view->setTerminal(true);

		$processId = $this->params()->fromRoute('id');
		$workflow = $this->getWorkflowService();
		
		/* Load the process definition */
		$process = new Wf\Process();
		$process->dao = $workflow->getFactory()->getDao(Wf\Process::$classId);
		$process->dao->loadFromId($process, $processId);
		
		/* export datas */
		$rawdata = $workflow->export($process);
		$rawdata = json_encode($rawdata, JSON_PRETTY_PRINT);
		
		$filename = 'processExport_' . $process->getNormalizedName() . '.json';
		
		//$view->content = iconv('UTF-8', "ISO-8859-1//TRANSLIT", $rawdata);
		$view->content = $rawdata;

		$this->getResponse()
			->getHeaders()
			->addHeaderLine('Content-Type', 'text/json; charset=utf-8')
			->addHeaderLine('Content-Transfer-Encoding', 'Binary')
			->addHeaderLine('Content-Disposition', "attachment; filename=$filename");
		// ->addHeaderLine( 'Pragma', 'no-cache' )
		// ->addHeaderLine( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0, public' )
		// ->addHeaderLine( 'Expires', 0 );

		return $view;
	}
} /* End of class */
