<?php
namespace Workflow\Controller;

use Zend\View\Model\ViewModel;
use Application\Form\PaginatorForm;
use Rbs\Space\Factory as DaoFactory;
use Workflow\Model\Wf;
use Workflow\Form;
use Workflow\Form\InstanceFilterForm as stdFilterForm;
use Application\Controller\ControllerException;
use Zend\Form\FormInterface;

/**
 * 
 *
 */
class ProcessController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$view = $this->view;
		$request = $this->getRequest();
		$factory = DaoFactory::get();

		$filter = $this->_getFilter('workflow/process/filter');
		$filter->prepare();
		$filter->saveToSession('workflow/process/filter');

		/* search from header search area : */
		$bind = array();

		/** @var \Rbs\Dao\Sier\DaoList $list */
		$list = $factory->getList(Wf\Process::$classId);
		$dao = $factory->getDao(Wf\Process::$classId);
		$list->dao = $dao;
		$select = array();
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = $asSys . ' as ' . $asApp;
		}

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll(''));
		$paginator->setData(array(
			'orderby' => 'name',
			'order' => 'asc'
		));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$table = $factory->getTable(Wf\Process::$classId);
		$strSelect = implode(',', $select);
		$sql = "SELECT $strSelect FROM $table";

		if ( $filter->where ) {
			$sql .= ' WHERE ' . $filter->where;
			$bind = array_merge($bind, $filter->getBind());
		}
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$view->list = $list;
		$view->headers = array(
			'#' => 'id',
			'Name' => 'name',
			'Full Id' => 'normalizedName',
			'Description' => 'title',
			'Version' => 'version',
			'isValid' => 'isValid',
			'isActive' => 'isActive'
		);

		$view->filter = $filter;
		$view->paginator = $paginator;

		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$request = $this->getRequest();
		$view = new ViewModel();
		$factory = DaoFactory::get();

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$process = Wf\Process::init();
		$form = new Form\ProcessForm();
		$form->bind($process);

		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$process->getNormalizedName(); // to init normalized name
				$factory->getDao($process->cid)->save($process);
				return $this->redirect()->toRoute('process');
			}
		}

		$view->setTemplate($form->template);
		$view->pageTitle = 'Create process';
		$view->form = $form;
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$id = $this->params()->fromRoute('id');
		$request = $this->getRequest();
		$view = new ViewModel();
		$factory = DaoFactory::get();

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$process = new Wf\Process();
		$factory->getDao($process->cid)->loadFromId($process, $id);

		$form = new Form\ProcessForm();
		$form->bind($process);

		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$factory->getDao($process->cid)->save($process);
				return $this->redirect()->toRoute('process');
			}
		}

		$view->setTemplate($form->template);
		$view->pageTitle = sprintf('Edit process %s', $process->getName());
		$view->form = $form;
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$view = new ViewModel();
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		$processId = $this->params()->fromRoute('id');
		$workflow = $this->getWorkflowService();
		/* set property to indicate if codes must be deleted */
		$workflow->deleteWithCode = false;
		/**/
		$workflow->deleteProcess($processId);

		return $this->redirect()->toRoute('process');
	}

	/**
	 * 
	 * @return \Zend\Http\Response
	 */
	public function newmajorAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$processId = $this->params()->fromRoute('id');
		$workflow = $this->getWorkflowService();
		$factory = $workflow->getFactory();

		/* Load the process definition */
		$process = new Wf\Process();
		$process->dao = $factory->getDao(Wf\Process::$classId);
		$process->dao->loadFromId($process, $processId);

		/* export datas */
		$rawdata = $workflow->export($process);
		$rawdata = json_encode($rawdata);

		$versions = $factory->getDao($process->cid)->getVersions($process->getName());
		$lastVersion = $versions[0];

		$version = explode('.', $lastVersion);
		$major = (int)$version[0];
		$minor = (string)$version[1];
		$newVersion = (string)($major + 1) . '.' . $minor;

		/* re-import in new version */
		$workflow->import($rawdata, $newVersion);

		/**/
		return $this->redirect()->toRoute('process');
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function newminorAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$processId = $this->params()->fromRoute('id');
		$workflow = $this->getWorkflowService();
		$factory = $workflow->getFactory();

		/* Load the process definition */
		$process = new Wf\Process();
		$process->dao = $factory->getDao(Wf\Process::$classId);
		$process->dao->loadFromId($process, $processId);

		$rawdata = $workflow->export($process);
		$rawdata = json_encode($rawdata);

		$currentVersion = explode('.', $process->getVersion());
		$currentMajor = (string)$currentVersion[0];
		$factory = DaoFactory::get();

		$versions = $process->dao->getVersions($process->getName());
		foreach( $versions as $v ) {
			$v = explode('.', $v);
			if ( (int)$v[0] == $currentMajor ) {
				$lastMinor = (int)$v[1];
				break;
			}
		}

		$newVersion = $currentMajor . '.' . (string)($lastMinor + 1);

		/* re-import in new version */
		$workflow->import($rawdata, $newVersion);

		return $this->redirect()->toRoute('process');
	}

	/**
	 * Ajax Method
	 */
	public function copytoAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$view = new ViewModel();

		$processId = $this->params()->fromRoute('id');
		$newName = $this->params()->fromQuery('newname', null);
		$newVersion = '1.0';

		if ( !$newName ) {
			throw new \Exception('Name is empty');
		}

		$workflow = $this->getWorkflowService();

		/* Load the process definition */
		$process = new Wf\Process();
		$process->dao = $workflow->getFactory()->getDao(Wf\Process::$classId);
		$process->dao->loadFromId($process, $processId);

		/* export datas */
		$rawdata = $workflow->export($process);
		$rawdata = json_encode($rawdata);

		/* and re-import */
		$workflow->import($rawdata, $newVersion, $newName);

		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		return $view;
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function importAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$request = $this->getRequest();
		$view = new ViewModel();
		$form = new Form\ImportProcessForm();

		if ( $request->isPost() ) {
			$post = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
			$form->setData($post);
			if ( $form->isValid() ) {
				$data = $form->getData(FormInterface::VALUES_AS_ARRAY);
				$file = $data['file']['tmp_name'];
				$fileContent = file_get_contents($file);

				$workflow = $this->getWorkflowService();
				$workflow->import($fileContent);

				return $this->redirect()->toRoute('process');
			}
		}

		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function editgraphAction()
	{
		$view = new ViewModel();
		$view->admin = false;

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$view->admin = true;
		$processId = $this->params()->fromRoute('id', null);
		$view->processId = $processId;
		$factory = DaoFactory::get();

		$process = new Wf\Process();
		$dao = $factory->getDao($process->cid);
		$dao->loadFromId($process, $processId);
		$view->process = $process;

		$graph = $dao->getGraphAsArray($processId, $factory);
		$view->graph = json_encode($graph);
		return $view;
	}

	/**
	 */
	private function _getFilter($sessionKey)
	{
		$request = $this->getRequest();
		$filter = new stdFilterForm();
		$filter->loadFromSession($sessionKey);
		$filter->setData($request->getPost());
		$filter->key1 = "CONCAT_WS('-', `name`, `title`, `ownerId`, '-')";
		$filter->key2 = 'id';
		return $filter;
	}

	/**
	 * query graph =
	 * array (size=3)
	 * 'activities' =>
	 * array (size=3)
	 * 0 =>
	 * array (size=8)
	 * 'id' => string 'sample' (length=6)
	 * 'name' => string 'sample' (length=6)
	 * 'title' => string 'SAMPLE' (length=6)
	 * 'type' => string 'activity' (length=8)
	 * 'isAutomatic' => string 'false' (length=5)
	 * 'isComment' => string 'false' (length=5)
	 * 'isInteractive' => string 'false' (length=5)
	 * 'attributes' =>
	 * array (size=2)
	 * 'positionx' => string '630px' (length=5)
	 * 'positiony' => string '90.5px' (length=6)
	 * 1 =>
	 * 'transitions' =>
	 * array (size=2)
	 * 0 =>
	 * array (size=3)
	 * 'id' => string 'con_12' (length=6)
	 * 'fromid' => string 'sample' (length=6)
	 * 'toid' => string '50' (length=2)
	 * 'processid' => string '8' (length=1)
	 */
	public function savegraphAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$view = $this->view;
		$factory = DaoFactory::get();

		$workflow = $this->getWorkflowService();
		$processId = $this->params()->fromPost('processid', null);
		$activities = $this->params()->fromPost('activities', array());
		$transitions = $this->params()->fromPost('transitions', array());

		$process = new Wf\Process();
		$factory->getDao($process->cid)->loadFromId($process, $processId);
		$return = $workflow->saveProcess($process, $activities, $transitions);

		$view->activities = $return['activities'];
		$view->transitions = $return['transitions'];
		return $view;
	}

	/**
	 * @deprecated
	 * @return \Zend\View\Model\ViewModel
	 */
	public function savetemplateAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		return new ViewModel();
	}

	/**
	 * @deprecated
	 */
	public function getscriptsrcAction()
	{
		$processId = (int)$this->params()->fromPost('processid', null);
		$activityId = (int)$this->params()->fromPost('activityid', null);

		$request = $this->getRequest();
		$view = new ViewModel();
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		$workflow = $this->getWorkflowService();
		$factory = $workflow->getFactory();

		$process = new Wf\Process();
		$process->dao = $factory->getDao($process->cid);
		$process->dao->loadFromId($process, $processId);

		$activity = new Wf\Activity();
		$activity->dao = $factory->getDao($activity->cid);
		$activity->dao->loadFromId($activity, $activityId);

		$code = $workflow->getCode($process, $activity);
		$modelFile = $code->getModelFile();
		$tplFile = $code->getTemplateFile();
		$formFile = $code->getFormFile();

		$view->modelFile = $modelFile;
		$view->templateFile = $tplFile;
		$view->formFile = $formFile;

		$view->modelSrc = file_get_contents($modelFile, true);
		$view->templateSrc = file_get_contents($tplFile, true);
		$view->formSrc = file_get_contents($formFile, true);

		return $view;
	}

	/**
	 * @deprecated
	 */
	public function savescriptAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$request = $this->getRequest();
		$view = new ViewModel();
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		$activityId = (int)$this->params()->fromPost('activityid', null);
		$processId = (int)$this->params()->fromPost('processid', null);
		$src = trim($this->params()->fromPost('scriptsrc', null));

		if ( $src != "" ) {
			$workflow = $this->getWorkflowService();
			$factory = $workflow->getFactory();

			$process = new Wf\Process();
			$process->dao = $factory->getDao($process->cid);
			$process->dao->loadFromId($process, $processId);

			$activity = new Wf\Activity();
			$activity->dao = $factory->getDao($activity->cid);
			$activity->dao->loadFromId($activity, $activityId);

			$code = $workflow->getCode($process, $activity);

			$modelFile = $code->getModelFile();

			if ( is_writable($modelFile) ) {
				file_put_contents($modelFile, $src);
			}
			else {
				throw new ControllerException(sprintf('src code file %s is not writable', $modelFile));
			}
		}

		return $view;
	}
} /* End of class */
