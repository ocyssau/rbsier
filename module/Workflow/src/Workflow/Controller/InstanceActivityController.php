<?php
namespace Workflow\Controller;

use Rbs\Space\Factory as DaoFactory;
use Application\Form\PaginatorForm;
use Workflow\Model\Wf;
use Workflow\Form\InstanceFilterForm as Filter;

/**
 * 
 *
 */
class InstanceActivityController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$view = $this->view;
		$request = $this->getRequest();

		$instanceId = $this->params()->fromRoute('id');

		$filter = $this->_getFilter('workflow/instanceActivity/filter');
		$filter->get('stdfilter-id')->setValue($instanceId);
		$filter->prepare();
		$filter->saveToSession('workflow/instanceActivity/filter');

		$bind = array();
		$factory = DaoFactory::get();
		$list = $factory->getList(Wf\Instance\Activity::$classId);
		$table = $factory->getTable(Wf\Instance\Activity::$classId);

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$sql = "SELECT * FROM $table";

		if ( $filter->where ) {
			$sql .= ' WHERE ' . $filter->where;
			$bind = array_merge($bind, $filter->getBind());
		}
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$list->setTranslator($factory->getTranslator($list->dao));
		$view->list = $list;

		$view->headers = array(
			'#' => 'id',
			'Name' => 'name',
			'Description' => 'title',
			'Owner' => 'ownerId',
			'Started' => 'started',
			'Ended' => 'ended',
			'Status' => 'status',
			'Process Instance' => 'instanceId'
		);

		$view->filter = $filter;
		$view->paginator = $paginator;

		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$aInstanceId = $this->params()->fromRoute('id');
		$aInstanceDao = $factory->getDao(Wf\Instance\Activity::$classId);
		$aInstanceDao->deleteFromId($aInstanceId);

		return $this->redirect()->toRoute('instance');
	}

	/**
	 */
	public function editAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);
		return $this->redirect()->toRoute('instance');
	}

	/**
	 */
	private function _getFilter($sessionKey)
	{
		$request = $this->getRequest();

		$filter = new Filter();
		$filter->loadFromSession($sessionKey);
		$filter->setData($request->getPost());
		$filter->key1 = 'CONCAT_WS(name, title, status, id, ownerId)';
		$filter->key2 = 'instanceId';
		return $filter;
	}
}
