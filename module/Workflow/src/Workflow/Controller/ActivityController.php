<?php
namespace Workflow\Controller;

/**
 * 
 *
 */
class ActivityController extends AbstractController
{

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		return $this->view;
	}

	/**
	 */
	public function editAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);
		return $this->view;
	}

	/**
	 */
	public function createAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);
		return $this->view;
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function deleteAction()
	{
		$view = $this->view;

		$activityId = $this->params()->fromRoute('id');

		/* Check rights */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$workflow = $this->getWorkflowService();
		$activity = $workflow->deleteActivity($activityId);

		$view->object = $activity;
		return $view;
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function runAction()
	{
		$actInstId = $this->params()->fromRoute('id');

		/* instanciate Workflow service */
		$workflow = $this->getWorkflowService();

		/* RUN ACTIVITY: */
		$act = $workflow->runActivity($actInstId)->lastActivity;
		if ( $act->getType() == 'end' ) {
			$workflow->complete($workflow->instance->getId());
		}
		else {
			/* TRANSLATE : */
			$workflow->translateActivity($actInstId);
		}

		return $this->redirect()->toRoute('workflow');
	}
}
