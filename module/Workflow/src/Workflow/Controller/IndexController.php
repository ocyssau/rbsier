<?php
namespace Workflow\Controller;

use Application\Form\PaginatorForm;
use Application\Form\StdFilterForm;
use Workflow\Model\Wf;

/**
 *
 *
 */
class IndexController extends AbstractController
{

	/**
	 * Display instance of all process
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$factory = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('DaoFactory');

		$filter = new StdFilterForm();
		$filter->setData($request->getPost());
		$filter->key = 'CONCAT(procInst.name,procInst.pId)';
		$filter->passThrough = true;
		$filter->prepare();

		/* search from header search area : */
		$bind = array();
		$list = $factory->getList(Wf\Instance::$classId);

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$sql = "SELECT * FROM galaxia_instances AS procInst";
		$sql .= " LEFT OUTER JOIN galaxia_processes AS proc ON proc.pId=procInst.pId";
		$sql .= " WHERE (procInst.owner=:ownerId)";

		$bind[':ownerId'] = '99999999-9999-9999-9999-00000000abcd';

		if ( $filter->where ) {
			$sql .= ' AND ' . $filter->where;
			$bind = array_merge($bind, $filter->getBind());
		}
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$view->myProcessInstance = $list;

		$view->headers = array(
			'#' => 'id',
			'Name' => 'name',
			'Started' => 'started',
			'Ended' => 'ended'
		);

		$view->filter = $filter;
		$view->title = 'My process instances';
		return $view;
	}

	/**
	 *  ajax method
	 */
	public function getnextAction()
	{
		$id = $this->params()->fromRoute('id');
		$view = $this->view;
		$factory = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('DaoFactory');

		$process = new Wf\Process();
		$factory->getDao($process->cid)->loadFromId($process, $id);

		$processInstance = new Wf\Instance();
		$factory->getDao($processInstance->cid)->loadFromId($processInstance, $id);

		/* Get next candidates activities */
		$runningList = $factory->getDao($processInstance->cid)->getRunningActivities($processInstance->getId());
		foreach( $runningList as $properties ) {
			$return[] = $properties;
		}

		$view->activities = $return;
		return $view;
	}
}

