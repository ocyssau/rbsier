<?php
namespace Workflow\Controller;

use Application\Form\PaginatorForm;
use Workflow\Model\Wf;
use Rbs\Space\Factory as DaoFactory;
use Workflow\Form\InstanceFilterForm as Filter;

/**
 * 
 *
 */
class InstanceController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$view = $this->view;
		$request = $this->getRequest();

		$processId = $this->params()->fromRoute('id');

		$filter = $this->_getFilter('workflow/instance/filter');
		$filter->get('stdfilter-id')->setValue($processId);
		$filter->prepare();
		$filter->saveToSession('workflow/instance/filter');

		// search from header search area :
		$bind = array();
		$select = array();
		$factory = DaoFactory::get();
		$list = $factory->getList(Wf\Instance::$classId);
		$table = $factory->getTable(Wf\Instance::$classId);
		$dao = $factory->getDao(Wf\Instance::$classId);
		$list->dao = $dao;
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = $asSys . ' as ' . $asApp;
		}

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$strSelect = implode(',', $select);
		$sql = "SELECT $strSelect FROM $table";
		if ( $filter->where ) {
			$sql .= ' WHERE ' . $filter->where;
			$bind = array_merge($bind, $filter->getBind());
		}
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$view->list = $list;
		$view->headers = array(
			'#' => 'id',
			'Name' => 'name',
			'Description' => 'title',
			'Owner' => 'ownerId',
			'Started' => 'started',
			'Ended' => 'ended',
			'ProcessId' => 'processId',
			'Status' => 'status'
		);

		$view->filter = $filter;
		$view->paginator = $paginator;

		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);
		$instanceId = $this->params()->fromRoute('id');

		$workflow = $this->getWorkflowService();
		$workflow->deleteInstance($instanceId);

		return $this->redirect()->toRoute('instance');
	}

	/**
	 */
	private function _getFilter($sessionKey)
	{
		$request = $this->getRequest();

		$filter = new Filter();
		$filter->loadFromSession($sessionKey);
		$filter->setData($request->getPost());
		$filter->key1 = "CONCAT_WS('-', `name`, `title`, `status`, `ownerId`)";
		$filter->key2 = 'processId';
		return $filter;
	}
}
