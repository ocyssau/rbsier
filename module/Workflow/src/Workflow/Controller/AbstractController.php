<?php
namespace Workflow\Controller;

/**
 *
 *
 */
class AbstractController extends \Application\Controller\AbstractController
{

	/**
	 *
	 */
	public function init($view = null, $errorStack = null)
	{
		parent::init($view, $errorStack);
		$tabs = \Application\View\Menu\MainTabBar::get();
		$tabs->getTab('Admin')->activate('workflow');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Workflow::$appCn;
	}

	/**
	 *
	 */
	public function initAjax($view = null, $errorStack = null)
	{
		parent::initAjax($view, $errorStack);
		$this->resourceCn = \Acl\Model\Resource\Workflow::$appCn;
	}

	/**
	 * 
	 * @return \Docflow\Service\Workflow
	 */
	protected function getWorkflowService()
	{
		return $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Workflow');
	}
}
