<?php
namespace Workflow\Service;

use Workflow\Model\Wf;
use Workflow\Model\Event;
use Rbplm\Dao\Registry;
use Rbplm\Dao\Filter\Op;
use Rbs\Space\Factory as DaoFactory;
use Workflow\Model\Exception as WorkflowException;
use Rbplm\People;
use Rbs\EventDispatcher\EventManager;

/**
 * Service class for Workflows managements
 *
 */
class Workflow
{

	/**
	 *
	 * @var DaoFactory
	 */
	protected $factory;

	/**
	 *
	 */
	protected $activityQueue;

	/**
	 *
	 * @var EventManager
	 */
	protected $eventManager;

	/**
	 * Function used to create events
	 * This callable must return a \Workflow\Model\EventInterface instance type
	 * 
	 * @var callable
	 */
	protected $eventFactory;

	/**
	 * The process definition
	 *
	 * @var Wf\Process
	 */
	public $process;

	/**
	 * Process instance
	 *
	 * @var Wf\Instance
	 */
	public $instance;

	/**
	 * The activity
	 *
	 * @var Wf\Activity
	 */
	public $activity;

	/**
	 *
	 * @var Wf\Activity\Start
	 */
	public $startActivity;

	/**
	 *
	 * @var Wf\Instance\Activity
	 */
	public $startInstance;

	/**
	 * The last running activity
	 *
	 * @var Wf\Instance\Activity
	 */
	public $lastActivity;

	/**
	 *
	 * @var string
	 */
	public $nextStatus;

	/**
	 *
	 * @var array
	 */
	public $nextTransitions = array();

	/**
	 *
	 * @var Wf\Activity
	 */
	public $nextActivity;

	/**
	 *
	 * @var Wf\Instance\Activity
	 */
	public $nextActivityInstance;

	/**
	 */
	public function __construct()
	{
		$this->factory = DaoFactory::get();
	}

	/**
	 *
	 * @param EventManager $eventManager
	 */
	public function setEventManager(EventManager $eventManager)
	{
		$this->eventManager = $eventManager;
		return $this;
	}

	/**
	 *
	 * @return EventManager
	 */
	public function getEventManager()
	{
		if ( !isset($this->eventManager) ) {
			/* construct a fake EventManager to prevent Exceptions */
			$this->eventManager = new EventManager();
		}
		return $this->eventManager;
	}

	/**
	 * @param callable $callback function to used to create events
	 */
	public function setEventFactory($callback)
	{
		$this->eventFactory = $callback;
		return $this;
	}

	/**
	 * @param string $name
	 * @param object $emitter
	 * @return \Workflow\Model\EventInterface
	 */
	public function getEvent($name, $emitter = null)
	{
		if ( !$emitter ) {
			$emitter = $this;
		}
		if ( is_callable($this->eventFactory) ) {
			return call_user_func($this->eventFactory, $name, $emitter);
		}
		else {
			throw new WorkflowException('this.eventFactory is not callable. Set a factory with Workflow.setEventFactory($callable)');
		}
	}

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function setFactory($factory)
	{
		$this->factory = $factory;
		return $this;
	}

	/**
	 *
	 * @return DaoFactory
	 */
	public function getFactory()
	{
		return $this->factory;
	}

	/**
	 *
	 * query graph =
	 * array (size=3)
	 * 'activities' =>
	 * array (size=3)
	 * 0 =>
	 * array (size=8)
	 * 'id' => string 'sample' (length=6)
	 * 'name' => string 'sample' (length=6)
	 * 'title' => string 'SAMPLE' (length=6)
	 * 'type' => string 'activity' (length=8)
	 * 'isAutomatic' => string 'false' (length=5)
	 * 'isComment' => string 'false' (length=5)
	 * 'isInteractive' => string 'false' (length=5)
	 * 'attributes' =>
	 * array (size=2)
	 * 'positionx' => string '630px' (length=5)
	 * 'positiony' => string '90.5px' (length=6)
	 * 1 =>
	 * 'transitions' =>
	 * array (size=2)
	 * 0 =>
	 * array (size=3)
	 * 'id' => string 'con_12' (length=6)
	 * 'fromid' => string 'sample' (length=6)
	 * 'toid' => string '50' (length=2)
	 * 'processid' => string '8' (length=1)
	 *
	 *
	 * @param \Workflow\Model\Wf\Process $process
	 * @param array $activities
	 * @param array $transitions
	 */
	public function saveProcess($process, $activities, $transitions)
	{
		$this->process = $process;
		$processId = $process->getId();

		/* create activitities */
		$registry = array();
		$return = array(
			'activities' => array(),
			'transitions' => array()
		);

		if ( $activities ) {
			foreach( $activities as $properties ) {
				if ( $properties['loaded'] == true ) {
					$aid = $properties['id'];
					if ( !$aid ) {
						throw new WorkflowException("Activity Id is not set");
					}
					$activity = Wf\Activity::factory($properties['type'], false);
					$this->factory->getDao($activity->cid)->loadFromId($activity, $aid);
				}
				else {
					$activity = Wf\Activity::factory($properties['type'], true);
					$properties['processId'] = $processId;
				}

				if ( isset($properties['roles']) ) {
					if ( !is_array($properties['roles']) ) {
						$properties['roles'] = explode(',', $properties['roles']);
					}
				}

				if ( isset($properties['name']) ) {
					$properties['name'] = \Workflow\Helper\Normalizer::normalize($properties['name']);
				}

				$domId = $properties['attributes']['id']; /* id of element as record in dom */

				$activity->hydrate($properties);

				/* PRE EVENT */
				$event = $this->getEvent(Event::SAVEPROCESS_PRE, $this);
				$event->activity = $activity;
				$this->getEventManager()->trigger($event);

				/* save */
				try {
					$this->factory->getDao($activity->cid)->save($activity);
				}
				catch( \Exception $e ) {
					throw new WorkflowException($e->getMessage());
				}

				/* POST EVENT */
				$this->getEventManager()->trigger($event->setName(Event::SAVEACTIVITY_POST));

				$registry[$domId] = $activity;
				$properties['id'] = $activity->getId();
				$return['activities'][] = $properties;
			}
		}

		/* delete transitions... */
		$dao = $this->factory->getDao(Wf\Transition::$classId);
		$dao->deleteFromProcess($processId);

		/* ...and re-create transitions */
		if ( $transitions ) {
			foreach( $transitions as $properties ) {
				$fromActivity = $registry[$properties['fromid']];
				$toActivity = $registry[$properties['toid']];

				$transition = Wf\Transition::factory($fromActivity, $toActivity);
				$transition->setStatus($properties['status']);
				$transition->setAttributes(array(
					'fromAnchor' => $properties['fromAnchor'],
					'toAnchor' => $properties['toAnchor']
				));
				$transition->processId = $this->process->getId();
				$dao->insert($transition);
				$properties['id'] = $transition->id;
				$return['transitions'][] = $properties;
			}
		}

		/* signal is send after save */
		$this->getEventManager()->trigger($event->setName(Event::SAVEPROCESS_POST));

		$this->process->isValid(true);
		$this->factory->getDao($this->process->cid)->save($this->process);

		return $return;
	}

	/**
	 *
	 * @param integer $processId
	 * @throws WorkflowException
	 * @return \Workflow\Model\Wf\Process
	 */
	public function deleteProcess($processId)
	{
		/* load */
		$dao = $this->factory->getDao(Wf\Process::$classId);

		$process = new Wf\Process();
		$dao->loadFromId($process, $processId);
		$this->process = $process;

		$event = $this->getEvent(Event::DELETEPROCESS_PRE, $this);
		$this->getEventManager()->trigger($event);

		$connexion = $dao->getConnexion();
		$connexion->beginTransaction();

		try {
			/* 1>delete activity instance */
			$this->factory->getDao(Wf\Instance\Activity::$classId)->deleteFromProcess($processId);

			/* 2>delete instances */
			$this->factory->getDao(Wf\Instance::$classId)->deleteFromProcess($processId);

			/* 3>delete transitions */
			$this->factory->getDao(Wf\Transition::$classId)->deleteFromProcess($processId);

			/* 4>delete activity */
			$this->factory->getDao(Wf\Activity::$classId)->deleteFromProcess($processId);

			/* 5>delete process */
			$dao->deleteFromId($processId, true, false);

			$connexion->commit();
		}
		catch( \Exception $e ) {
			$connexion->rollBack();
			throw new WorkflowException($e->getMessage());
		}

		$this->getEventManager()->trigger($event->setName(Event::DELETEPROCESS_POST));

		return $process;
	}

	/**
	 *
	 * @param integer $activityId
	 * @throws WorkflowException
	 * @return \Workflow\Model\Wf\Activity
	 */
	public function deleteActivity($activityId)
	{
		/* load activity */
		$activity = new Wf\Activity();
		$dao = $this->factory->getDao($activity->cid);
		$dao->loadFromId($activity, $activityId);

		$ok = $dao->deleteFromId($activityId, false, true);
		if ( $ok ) {
			$this->activity = $activity;
			/* signal is send after save */
			$event = $this->getEvent(Event::DELETEACTIVITY_POST, $this);
			$this->getEventManager()->trigger($event);
		}

		return $activity;
	}

	/**
	 *
	 * @param integer $instanceId
	 * @throws WorkflowException
	 * @return Workflow
	 */
	public function deleteInstance($instanceId)
	{
		try {
			$instanceDao = $this->factory->getDao(Wf\Instance::$classId);
			$connexion = $instanceDao->getConnexion();
			$connexion->beginTransaction();

			/* 1>delete activity instance */
			$this->factory->getDao(Wf\Instance\Activity::$classId)->deleteFromInstance($instanceId, false, false);

			/* 2>delete instances */
			$instanceDao->deleteFromId($instanceId, false, false);

			$connexion->commit();

			$event = $this->getEvent(Event::DELETEINSTANCE_POST, $this);
			$this->getEventManager()->trigger($event);
		}
		catch( \Exception $e ) {
			$connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 * Start a process from his id and return an instance with the start activity instanciate
	 * Return a start activity
	 *
	 * @param integer $processId
	 * @return Workflow
	 */
	public function startProcess($processId = null)
	{
		/* set DAOs */
		$factory = $this->factory;
		$procDao = $factory->getDao(Wf\Process::$classId);
		$procInstDao = $factory->getDao(Wf\Instance::$classId);
		$actDao = $factory->getDao(Wf\Activity::$classId);

		/* Load the process definition */
		if ( !$this->process && $processId ) {
			$this->process = new Wf\Process();
			$procDao->loadFromId($this->process, $processId);
		}
		else {
			$processId = $this->process->getId();
		}
		$proc = $this->process;

		if ( $proc->isActive() == false ) {
			throw new WorkflowException('this process is not active');
		}

		/* Instanciate process */
		$procInst = Wf\Instance::start($proc);

		/* Find Start activity... */
		$startAct = new Wf\Activity\Start();
		$filter = $factory->getFilter($startAct->cid)->setOption('asapp', true);
		$filter->andfind($processId, $actDao->toSys('processId'), Op::EQUAL);
		$filter->andfind('start', 'type', Op::EQUAL);
		$actDao->load($startAct, $filter);

		/* Instanciate activity and execute */
		$startActInst = $procInst->execute($startAct);

		/* populate this object */
		$this->lastActivity = $startActInst;
		$this->startInstance = $startActInst;
		$this->startActivity = $startAct;
		$this->instance = $procInst;

		/* signal is send before save */
		$event = $this->getEvent(Event::START_PRE, $this);
		$this->getEventManager()->trigger($event);
		$this->getEventManager()->trigger($event->setName(Event::RUNACTIVITY_PRE));

		/* record in registry, @todo explain for what?.... */
		$registry = Registry::singleton();
		$registry->add($proc);
		$registry->add($procInst);
		$registry->add($startAct);

		/* save process instance */
		$procInstDao->save($procInst);

		/* now proc instance id is set, set process instance to activity instance here */
		$startActInst->setInstance($procInst);

		/* translate to next activity */
		$this->translateActivity($startActInst);

		/* signal is send before save */
		$this->getEventManager()->trigger($event->setName(Event::START_POST));

		$this->_completeActivity($startActInst);

		return $this;
	}

	/**
	 * Run the instance of an activity
	 * Return Lazy interface
	 *
	 * @param Wf\Instance\Activity $actInst
	 * @throws WorkflowException
	 * @return Workflow
	 */
	public function runActivity(Wf\Instance\Activity $actInst)
	{
		/** @var  Wf\Instance $procInst */
		$procInst = $actInst->getInstance();

		/* Load instance of process */
		if ( !$procInst ) {
			$procInst = $this->factory->getLoader()->loadFromId($actInst->getInstance(true), Wf\Instance::$classId);
			/* create link, need by execute method */
			$actInst->setInstance($procInst);
		}

		/* populate this object */
		$this->instance = $procInst;
		$this->lastActivity = $actInst;

		/* signal is send before save */
		$event = $this->getEvent(Event::RUNACTIVITY_PRE, $this);
		$this->getEventManager()->trigger($event);

		/* execute activity */
		$actInst->execute();
		$type = $actInst->getType();

		/* END activity */
		if ( $type == Wf\Activity::TYPE_END ) {
			/* close the current process */
			$this->_complete($this->instance->getId());
		}
		/* JOIN activity */
		elseif ( $type == Wf\Activity::TYPE_JOIN ) {
			/* check that all previous acitivies are completed before translate this activity */
			$this->translateActivity($actInst);
		}
		/* SWITCH activity */
		elseif ( $type == Wf\Activity::TYPE_SWITCH ) {
			if ( !$this->nextTransitions ) {
				throw (new NoneTransitionException('None transitions for switch activity'));
			}
			$this->translateActivity($actInst);
		}
		/* ACTIVITY activity */
		elseif ( $type == Wf\Activity::TYPE_ACTIVITY ) {
			$this->translateActivity($actInst);
		}

		$this->_completeActivity($actInst);

		return $this;
	}

	/**
	 * Close and save the Instance of activity
	 *
	 * @param Wf\Instance\Activity $actInst
	 * @throws WorkflowException
	 * @return \Workflow\Service\Workflow
	 */
	protected function _completeActivity(Wf\Instance\Activity $actInst)
	{
		/** @var  Wf\Instance $procInst */
		$procInst = $actInst->getInstance();
		$type = $actInst->getType();

		$actInstDao = $this->factory->getDao($actInst->cid);
		$procInstDao = $this->factory->getDao($procInst->cid);

		/* signal is send before save */
		$event = $this->getEvent(Event::RUNACTIVITY_POST, $this, $actInst);
		$this->getEventManager()->trigger($event);

		if ( $type == Wf\Activity::TYPE_STANDALONE ) {
			/* dont save STANDALONE activity */
			$this->factory->getDao($procInst->cid)->save($procInst);
		}
		else {
			/* save process and activity */
			$procInstDao->save($procInst);
			$actInstDao->save($actInst);
		}

		if ( $this->nextActivityInstance ) {
			/* run automaticaly end activity or automatic activity */
			if ( $this->nextActivityInstance->getType() == 'end' ) {
				$nextActivityInstance = $this->nextActivityInstance;
				$this->nextTransitions = null;
				$this->nextActivityInstance = null;
				$this->runActivity($nextActivityInstance);
			}
			elseif ( $this->nextActivityInstance->isAutomatic() == true ) {
				/* save the running status */
				$actInstDao->save($this->nextActivityInstance);
				$this->nextTransitions = null;
			}
			else {
				/* save the running status */
				$actInstDao->save($this->nextActivityInstance);
			}
		}

		return $this;
	}

	/**
	 *
	 * @param Wf\Activity $activity
	 * @param Wf\Instance $procInst
	 * @throws WorkflowException
	 * @return Workflow
	 */
	public function runStandalone(Wf\Activity $activity, Wf\Instance $procInst)
	{
		/* Start activity = Create an activity instance of standalone activity, not saved */
		$actInst = Wf\Instance\Activity::start($activity, $procInst);
		$actInst->setInstance($procInst);

		return $this->runActivity($actInst);
	}

	/**
	 * Start a process from his id and return an instance with the start activity instanciate
	 *
	 * @param Wf\Instance\Activity $actInst
	 *        	activity instance to translate
	 * @return Workflow
	 */
	public function translateActivity(Wf\Instance\Activity $actInst)
	{
		/* get the instance of process */
		if ( !isset($this->instance) ) {
			throw (new WorkflowException('$this->instance is not set'));
		}
		$procInst = $this->instance;

		/* signal is send before save */
		$event = $this->getEvent(Event::TRANSLATE_PRE, $this);
		$this->getEventManager()->trigger($event);

		/* Init dao helpers */
		$actInstDao = $this->factory->getDao(Wf\Instance\Activity::$classId);
		$procInstDao = $this->factory->getDao(Wf\Instance::$classId);

		/* get list of candidates */
		$selectedNextTrans = $this->nextTransitions;
		$selectedNextAct = $procInst->getNextActivities();

		/* @var array $nexts */
		//$nexts = $procInstDao->getNextCandidates($actInstId, $selectedNextAct, $selectedNextTrans);
		$nexts = $procInstDao->getNextCandidatesFromActivityAndProcessinst($actInst->getActivity(true), $procInst->getId(), $selectedNextAct, $selectedNextTrans);

		if ( count($nexts) == 0 ) {
			throw new WorkflowException("None next activities for activity instance " . $actInst->getId());
		}

		/* instanciate candidates activities and start it */
		foreach( $nexts as $next ) {
			$nextActivity = new Wf\Activity($next);

			/* If join activity, check if all splitted activities are completed */
			if ( $nextActivity->getType() == Wf\Activity::TYPE_JOIN ) {
				/* Get parents activities of the join with activityInstance */
				$filter = "(parentsActInst.status NOT LIKE :status OR parentsActInst.status IS NULL) AND act.id=:activityId";
				$select = 'parentsAct.id, parentsActInst.status';
				$bind = array(
					':activityId' => $nextActivity->getId(),
					':status' => 'completed'
				);

				/* @var \PDOStatement $stmt */
				$stmt = $actInstDao->getParentsInstanceActivity($procInst->getId(), $filter, $select, $bind);

				/* Some instance activities are not completed (but ignore current), don't execute the join */
				if ( $stmt->rowCount() > 1 ) {
					continue;
				}
			}

			/* */
			$this->nextStatus = $next['nextStatus'];

			/* Init the Instance\Activity next activity */
			$nextActivityInstance = Wf\Instance\Activity::start($nextActivity, $procInst);

			$this->nextActivityInstance = $nextActivityInstance;
			if ( $this->lastActivity ) {
				$nextActivityInstance->setParent($this->lastActivity);
			}
		}

		/* signal is send before save */
		$this->getEventManager()->trigger($event->setName(Event::TRANSLATE_POST));

		return $this;
	}

	/**
	 * @return \SplQueue
	 */
	public function getQueue()
	{
		if ( !isset($this->activityQueue) ) {
			$this->activityQueue = new \SplQueue();
		}
		return $this->activityQueue;
	}

	/**
	 * Close the current process instance
	 *
	 * @param
	 *        	integer
	 * @return Workflow
	 */
	protected function _complete()
	{
		$this->instance->setStatus(Wf\Instance::STATUS_COMPLETED)->setEnded(new \DateTime());

		/* signal is send before save */
		$event = $this->getEvent(Event::COMPLET_POST, $this);
		$this->getEventManager()->trigger($event);

		$this->factory->getDao($this->instance->cid)->save($this->instance);
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getNextTransitions()
	{
		return $this->nextTransitions;
	}

	/**
	 *
	 * @param array $array
	 * @return Workflow
	 */
	public function setNextTransition($transition)
	{
		if ( $transition ) {
			$this->nextTransitions[] = $transition;
		}
		return $this;
	}

	/**
	 * 
	 * @param Wf\Process $process
	 * @return array
	 */
	public function export(Wf\Process $process)
	{
		$procDao = $this->factory->getDao(Wf\Process::$classId);

		$return = array(
			'process' => [],
			'activities' => [],
			'transitions' => []
		);

		$return['process'] = $process->getArrayCopy();

		/* load activities */
		$factory = $this->factory;
		$activities = $factory->getList(Wf\Activity\Activity::$classId);
		$activityDao = $activities->dao;
		$activities->setTranslator($factory->getTranslator($activityDao));
		$activities->load($activityDao->toSys('processId') . '=:processId', [
			':processId' => $process->getId()
		]);

		foreach( $activities as $item ) {
			$item['updated'] = null;
			$item['expirationTime'] = null;
			$return['activities'][$item['uid']] = $item;
		}

		/* load transitions */
		$transitions = $procDao->getTransitions($process->getId());
		$transDao = $factory->getDao(Wf\Transition::$classId);
		foreach( $transitions as $item ) {
			$item = $transDao->getTranslator()->toApp($item);
			$return['transitions'][$item['uid']] = $item;
		}

		return $return;
	}

	/**
	 *
	 * @param
	 *        	string json $data
	 * @param string $version
	 * @param string $name
	 * @throws WorkflowException
	 * @return \Workflow\Model\Wf\Process
	 */
	public function import($data, $version = null, $name = null)
	{
		//$data = iconv('ISO-8859-1', 'ISO-8859-1//TRANSLIT', $data);
		$data = json_decode($data, true);

		if ( !isset($data['process']) ) {
			throw new WorkflowException('Bad import data format');
		}

		$process = new Wf\Process();
		$process->hydrate($data['process']);

		$process->newUid();
		$process->isActive(false);
		$process->setOwner(People\CurrentUser::get());

		if ( $version ) {
			$process->setVersion($version);
			$process->setNormalizedName(null);
		}

		if ( $name ) {
			$process->setName($name);
			$process->setNormalizedName(null);
		}

		/* reinit normalizedName */
		$process->getNormalizedName();

		try {
			$this->factory->getDao($process->cid)->save($process);
		}
		catch( \Exception $e ) {
			$nName = $process->getNormalizedName();
			throw new WorkflowException('this process can not be saved. The name-version ' . $nName . ' is probably yet in used.' . $e->getMessage());
		}

		/* import activities */
		if ( isset($data['activities']) ) {

			$activityRegistry = [];

			foreach( $data['activities'] as $item ) {
				$activity = Wf\Activity::factory($item['type'], false);
				$activity->hydrate($item);
				/* registry is indexed with original uid */
				$activityRegistry[$activity->getUid()] = $activity;
				$activity->newUid();
				$activity->setProcess($process, true);
				$this->factory->getDao($activity->cid)->save($activity);
			}
		}

		/* import transitions */
		if ( isset($data['transitions']) ) {
			foreach( $data['transitions'] as $item ) {
				$parent = null;
				$child = null;
				$transition = new Wf\Transition();
				$transition->hydrate($item);
				$transition->newUid();
				$transition->processId = $process->getId();

				if ( isset($activityRegistry[$transition->parentUid]) ) {
					$parent = $activityRegistry[$transition->parentUid];
					$transition->setParent($parent);
					$parent->transition[] = $transition;
				}
				if ( isset($activityRegistry[$transition->childUid]) ) {
					$child = $activityRegistry[$transition->childUid];
					$transition->setChild($child);
				}

				$this->factory->getDao($transition->cid)->insert($transition);
			}
		}

		return $process;
	}

	/**
	 *
	 * @param Wf\Process $process        	
	 * @param Wf\Activity $activity  
	 */
	public function getCode(Wf\Process $process, Wf\Activity $activity)
	{
		throw new WorkflowException('Method not implemented');
	}
}
