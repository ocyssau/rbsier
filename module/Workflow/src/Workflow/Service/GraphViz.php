<?php
// +----------------------------------------------------------------------+
// | PEAR :: Image :: GraphViz                                            |
// +----------------------------------------------------------------------+
// | Copyright (c) 2002 Sebastian Bergmann <sb@sebastian-bergmann.de> and |
// |                    Dr. Volker Göbbels <vmg@arachnion.de>.
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.00 of the PHP License,      |
// | that is available at http://www.php.net/license/3_0.txt.             |
// | If you did not receive a copy of the PHP license and are unable to   |
// | obtain it through the world-wide-web, please send a note to          |
// | license@php.net so we can mail you a copy immediately.               |
// +----------------------------------------------------------------------+
namespace Workflow\Service;

use Exception;

/**
 * PEAR::Image_GraphViz
 *
 * Purpose
 *
 *     Allows for the creation of and the work with directed
 *     and undirected graphs and their visualization with
 *     AT&T's GraphViz tools. These can be found at
 *     http://www.research.att.com/sw/tools/graphviz/
 *
 * Example
 *
 *     require_once 'Image/GraphViz.php';
 *     $graph = new Image_GraphViz();
 *
 *     $graph->addNode('Node1', array('URL'      => 'http://link1',
 *                                    'label'    => 'This is a label',
 *                                    'shape'    => 'box'
 *                                    )
 *                     );
 *     $graph->addNode('Node2', array('URL'      => 'http://link2',
 *                                    'fontsize' => '14'
 *                                    )
 *                     );
 *     $graph->addNode('Node3', array('URL'      => 'http://link3',
 *                                    'fontsize' => '20'
 *                                    )
 *                     );
 *
 *     $graph->addEdge(array('Node1' => 'Node2'), array('label' => 'Edge Label'));
 *     $graph->addEdge(array('Node1' => 'Node2'), array('color' => 'red'));
 *
 *     $graph->image();
 *
 * @author  Sebastian Bergmann <sb@sebastian-bergmann.de>
 *          Dr. Volker Göbbels <vmg@arachnion.de>
 * @package Image
 *
 *
 * @see http://www.graphviz.org
 * @see http://pear.php.net/package/Image_GraphViz/docs
 */
class GraphViz
{

	/**
	 * Path to GraphViz/dot command
	 *
	 * @var  string
	 */
	protected $dotCommand = 'dot';

	/**
	 *
	 * @var string
	 */
	protected $pid;

	/**
	 * Path to GraphViz/neato command
	 *
	 * @var  string
	 */
	protected $neatoCommand = 'neato';

	/**
	 *
	 * @var string
	 */
	public $mapFile;

	/**
	 *
	 * @var string
	 */
	public $markupFile;

	/**
	 *
	 * @var string
	 */
	public $imageFile;

	/**
	 *
	 * @var boolean
	 */
	protected $directed = true;

	/**
	 *
	 * @var array
	 */
	protected $clusters = array();

	/**
	 *
	 * @var array
	 */
	protected $nodes = array();

	/**
	 *
	 * @var array
	 */
	protected $edges = array();

	/**
	 *
	 * @var array
	 */
	protected $edgeAttributes = array();

	/**
	 *
	 * @var array
	 */
	protected $attributes = array();

	/**
	 * Constructor
	 *
	 * @param  array   Attributes of the graph
	 * @param  string  path to dot command
	 */
	public function __construct($attributes = array(), $binDir = null)
	{
		$this->setAttributes($attributes);

		if ( $binDir ) {
			$this->dotCommand = $binDir . '/' . $this->dotCommand;
			$this->neatoCommand = $binDir . '/' . $this->neatoCommand;
		}
	}

	/**
	 * Generate a image and a map for the graph.
	 * @param string $toDirectory		base path for result files. must existing
	 * @param string $format
	 * @return GraphViz
	 */
	public function imageAndMap($toDirectory, $format = 'png')
	{
		if ( !is_dir($toDirectory) ) {
			throw new Exception(sprintf('PATH_%s_IS_NOT_EXISTING', $toDirectory), E_USER_WARNING);
		}

		$file = $toDirectory . '/' . $this->pid;
		$imageFile = $file . '.' . $format;
		$mapFile = $file . '.' . 'map';
		$markupFile = $file . '.graph';

		$this->_saveParsedGraph($markupFile);

		if ( substr(php_uname(), 0, 7) == "Windows" ) {
			$src = '"' . $markupFile . '"';
			$imageFile = '"' . $imageFile . '"';
			$mapFile = '"' . $mapFile . '"';
		}
		else {
			$src = $markupFile;
		}

		if ( !isset($this->directed) ) {
			$this->directed = true;
		}

		$return = $output = null;
		$command = $this->directed ? $this->dotCommand : $this->neatoCommand;
		$command .= " -T$format -o $imageFile $src";
		exec($command, $output, $return);
		if ( $return > 0 ) {
			throw new \Exception(sprintf('Execution failed durring generation of Graphviz image. Check command: <%s>', $command));
		}

		$return = $output = null;
		$command = $this->dotCommand;
		$command .= " -Tcmap -o $mapFile $src";
		exec($command, $output, $return);
		if ( $return > 0 ) {
			throw new \Exception(sprintf('Execution failed durring generation of Graphviz map. Check command: <%s>', $command));
		}

		$this->imageFile = $imageFile;
		$this->mapFile = $mapFile;
		$this->markupFile = $markupFile;

		return $this;
	}

	/**
	 * Add a cluster to the graph.
	 *
	 * @param  string  ID.
	 * @param  array   Title.
	 * @return GraphViz
	 */
	public function addCluster($id, $title)
	{
		$this->clusters[$id] = $title;
		return $this;
	}

	/**
	 * Add a note to the graph.
	 *
	 * @param  string  Name of the node.
	 * @param  array   Attributes of the node.
	 * @param  string  Group of the node.
	 * @return GraphViz
	 */
	public function addNode($name, $attributes = array(), $group = 'default')
	{
		$this->nodes[$group][$name] = $attributes;
		return $this;
	}

	/**
	 * Remove a node from the graph.
	 *
	 * @param  Name of the node to be removed.
	 * @return GraphViz
	 */
	public function removeNode($name, $group = 'default')
	{
		if ( isset($this->nodes[$group][$name]) ) {
			unset($this->nodes[$group][$name]);
		}
		return $this;
	}

	/**
	 * Add an edge to the graph.
	 *
	 * @param  array Start and End node of the edge.
	 * @param  array Attributes of the edge.
	 * @return GraphViz
	 */
	public function addEdge($edge, $attributes = array())
	{
		if ( is_array($edge) ) {
			$from = key($edge);
			$to = $edge[$from];
			$id = $from . '_' . $to;

			if ( !isset($this->edges['edges'][$id]) ) {
				$this->edges[$id] = $edge;
			}
			else {
				$this->edges[$id] = array_merge($this->edges[$id], $edge);
			}

			if ( is_array($attributes) ) {
				if ( !isset($this->edgeAttributes[$id]) ) {
					$this->edgeAttributes[$id] = $attributes;
				}
				else {
					$this->edgeAttributes[$id] = array_merge($this->edgeAttributes[$id], $attributes);
				}
			}
		}
		return $this;
	}

	/**
	 * Remove an edge from the graph.
	 *
	 * @param  array Start and End node of the edge to be removed.
	 * @return GraphViz
	 */
	public function removeEdge($edge)
	{
		if ( is_array($edge) ) {
			$from = key($edge);
			$to = $edge[$from];
			$id = $from . '_' . $to;

			if ( isset($this->edges[$id]) ) {
				unset($this->edges[$id]);
			}

			if ( isset($this->edgeAttributes[$id]) ) {
				unset($this->edgeAttributes[$id]);
			}
		}
		return $this;
	}

	/**
	 * Add attributes to the graph.
	 *
	 * @param  array Attributes to be added to the graph.
	 * @return GraphViz
	 */
	public function addAttributes(array $attributes)
	{
		$this->attributes = array_merge($this->attributes, $attributes);
		return $this;
	}

	/**
	 * Set attributes of the graph.
	 *
	 * @param  array Attributes to be set for the graph.
	 */
	public function setAttributes(array $attributes)
	{
		$this->attributes = $attributes;
		return $this;
	}

	/**
	 * Set directed/undirected flag for the graph.
	 *
	 * @param  boolean Directed (true) or undirected (false) graph.
	 * @return GraphViz
	 */
	public function setDirected($directed)
	{
		$this->directed = (boolean)$directed;
		return $this;
	}

	/**
	 * Set directory where dot command is reachable
	 * @param string	$binDir
	 * @return GraphViz
	 */
	public function setBindir($binDir)
	{
		$this->dotCommand = $binDir . '/' . $this->dotCommand;
		$this->neatoCommand = $binDir . '/' . $this->neatoCommand;
		return $this;
	}

	/**
	 * Set id for graph
	 * @param string	$pid
	 * @return GraphViz
	 */
	public function setPid($pid)
	{
		$this->pid = $pid;
		return $this;
	}

	/**
	 * Load graph from file.
	 *
	 * @param  string  File to load graph from.
	 * @return GraphViz
	 */
	public function load($file)
	{
		if ( $serialized_graph = implode('', @file($file)) ) {
			$graph = unserialize($serialized_graph);

			$this->attributes = $graph['attributes'];
			$this->clusters = $graph['clusters'];
			$this->directed = $graph['directed'];
			$this->edges = $graph['edges'];
			$this->edgesAttributes = $graph['edgesAttributes'];
			$this->nodes = $graph['nodes'];
		}
		else {
			throw new Exception(sprintf('UNABLE_TO_LOAD_%s', $file), E_USER_WARNING);
		}
		return $this;
	}

	/**
	 * Save graph to file.
	 *
	 * @param  string  File to save the graph to.
	 * @return mixed   File the graph was saved to, false on failure.
	 */
	public function save($file = '')
	{
		$graph = array(
			'attributes' => $this->attributes,
			'clusters' => $this->clusters,
			'directed' => $this->directed,
			'edges' => $this->edges,
			'edgesAttributes' => $this->edgesAttributes,
			'nodes' => $this->nodes
		);
		$serializedGraph = serialize($graph);

		if ( empty($file) ) {
			$file = tempnam('temp', 'graph_');
		}

		if ( !file_put_content($file, $serializedGraph) ) {
			throw new Exception(sprintf('UNABLE_TO_WRITE_IN_FILE_%s', $file), E_USER_WARNING);
		}
		return $file;
	}

	/**
	 * Parse the graph into GraphViz markup.
	 *
	 * @return string  GraphViz markup
	 */
	public function parse()
	{
		$parsedGraph = "digraph G {\n";

		if ( isset($this->attributes) ) {
			foreach( $this->attributes as $key => $value ) {
				$attributeList[] = $key . '="' . $value . '"';
			}

			if ( !empty($attributeList) ) {
				$parsedGraph .= implode(',', $attributeList) . ";\n";
			}
		}

		if ( isset($this->nodes) ) {
			foreach( $this->nodes as $group => $nodes ) {
				if ( $group != 'default' ) {
					$parsedGraph .= sprintf("subgraph \"cluster_%s\" {\nlabel=\"%s\";\n", 
					$group, isset($this->clusters[$group]) ? $this->clusters[$group] : '');
				}

				foreach( $nodes as $node => $attributes ) {
					unset($attributeList);

					foreach( $attributes as $key => $value ) {
						$attributeList[] = $key . '="' . $value . '"';
					}

					if ( !empty($attributeList) ) {
						$parsedGraph .= sprintf("\"%s\" [ %s ];\n", addslashes(stripslashes($node)), implode(',', $attributeList));
					}
				}

				if ( $group != 'default' ) {
					$parsedGraph .= "}\n";
				}
			}
		}

		if ( isset($this->edges) ) {
			foreach( $this->edges as $label => $node ) {
				unset($attributeList);

				$from = key($node);
				$to = $node[$from];

				foreach( $this->edgeAttributes[$label] as $key => $value ) {
					$attributeList[] = $key . '="' . $value . '"';
				}

				$parsedGraph .= sprintf('"%s" -> "%s"', addslashes(stripslashes($from)), addslashes(stripslashes($to)));

				if ( !empty($attributeList) ) {
					$parsedGraph .= sprintf(' [ %s ]', implode(',', $attributeList));
				}

				$parsedGraph .= ";\n";
			}
		}

		return $parsedGraph . "}\n";
	}

	/**
	 * Save GraphViz markup to file.
	 *
	 * @param  string  File to write the GraphViz markup to.
	 * @return mixed   File to which the GraphViz markup was
	 *                 written, false on failure.
	 */
	protected function _saveParsedGraph($file)
	{
		$parsedGraph = $this->parse();
		if ( !file_put_contents($file, $parsedGraph) ) {
			throw new Exception(sprintf('UNABLE_TO_WRITE_IN_FILE_%s', $file), E_USER_WARNING);
		}
		return $file;
	}
} //End of class
