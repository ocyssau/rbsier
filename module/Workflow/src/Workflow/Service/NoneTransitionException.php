<?php
namespace Workflow\Service;

/**
 * Throw if transition is not founded
 *
 */
class NoneTransitionException extends \Exception
{
} /* End of class */