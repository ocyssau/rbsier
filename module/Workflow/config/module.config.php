<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'process'=>array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/workflow/process/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Workflow\Controller\Process',
						'action' => 'index',
					),
				),
			),
			'process-export'=>array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/workflow/export/[:id]',
					'defaults' => array(
						'controller' => 'Workflow\Controller\Export',
						'action' => 'export',
					),
				),
			),
			'instance'=>array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/workflow/instance/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Workflow\Controller\Instance',
						'action' => 'index',
					),
				),
			),
			'ainstance'=>array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/workflow/ainstance/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Workflow\Controller\InstanceActivity',
						'action' => 'index',
					),
				),
			),
			'activity'=>array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/workflow/activity/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Workflow\Controller\Activity',
						'action' => 'index',
					),
				),
			),
			'workflow' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/workflow',
					'defaults' => array(
						'__NAMESPACE__' => 'Workflow\Controller',
						'controller'=> 'Index',
						'action'=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'=> 'Segment',
						'options' => array(
							'route'=> '/[:controller[/:action]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Workflow\Controller\Index' => 'Workflow\Controller\IndexController',
			'Workflow\Controller\Process' => 'Workflow\Controller\ProcessController',
			'Workflow\Controller\Export' => 'Workflow\Controller\ExportController',
			'Workflow\Controller\Instance' => 'Workflow\Controller\InstanceController',
			'Workflow\Controller\InstanceActivity' => 'Workflow\Controller\InstanceActivityController',
			'Workflow\Controller\Activity' => 'Workflow\Controller\ActivityController',
			),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	),
);
