<?php
/* @var \Zend\View\Renderer\PhpRenderer $this */

$this->admin = false;

$this->headLink([
	'rel' => 'shortcut icon',
	'type' => 'image/vnd.microsoft.icon',
	'href' => $this->favicon
	])
	->prependStylesheet($this->basePath('/styles/default.css?reload=' . $this->cssReload))
	->appendStylesheet($this->basePath($this->cssSheet2 . '?reload=' . $this->cssReload))
	->appendStylesheet($this->basePath($this->cssSheet . '?reload=' . $this->cssReload))
	->prependStylesheet($this->basePath('/bootstrap-select/bootstrap-select.min.css'))
	->prependStylesheet($this->basePath('/js/jqueryui/themes/smoothness/jquery-ui.min.css'))
	->prependStylesheet($this->basePath('/bootstrap/css/bootstrap-theme.min.css'))
	->prependStylesheet($this->basePath('/bootstrap/css/bootstrap.min.css'));

$this->headScript()
	->prependFile($this->basePath('/js/rb/model/rb.form.js?reload=' . $this->jsReload))
	->prependFile($this->basePath('/js/rb/model/rb.table.js?reload=' . $this->jsReload))
	->prependFile($this->basePath('/js/rb/model/rb.objectbox.js?reload=' . $this->jsReload))
	->prependFile($this->basePath('/js/rb/model/rb.model.js?reload=' . $this->jsReload))
	->prependFile($this->basePath('/bootstrap-select/bootstrap-select.min.js'))
	->prependFile($this->basePath('/bootstrap/js/bootstrap.min.js'))
	->prependFile($this->basePath('/js/respond.min.js'), 'text/javascript', ['conditional' => 'lt IE 9'])
	->prependFile($this->basePath('/js/html5shiv.js'), 'text/javascript', ['conditional' => 'lt IE 9'])
	->prependFile($this->basePath('/js/tinymce/tinymce.min.js'))
	->prependFile($this->basePath('/js/jqueryui/jquery-ui.min.js'))
	->prependFile($this->basePath('/js/jquery/jquery.min.js'));
?>
