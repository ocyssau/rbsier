<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
	'router' => array(
		'routes' => array(
			'home' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/[home[/:action]]',
					'defaults' => array(
						'controller' => 'Application\Controller\Home',
						'action' => 'index'
					)
				)
			),
			'extendedproperties' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/extendedproperties[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Application\Controller\Extendedproperties',
						'action' => 'index'
					)
				)
			),
			'auth' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/auth',
					'defaults' => array(
						'controller' => 'Application\Controller\Auth',
						'action' => 'login'
					)
				),
				'may_terminate' => true,
				'child_routes' => array(
					'login' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/login',
							'defaults' => array(
								'action' => 'login'
							)
						)
					),
					'logout' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/logout',
							'defaults' => array(
								'action' => 'logout'
							)
						)
					),
					'authenticate' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/authenticate',
							'defaults' => array(
								'action' => 'authenticate'
							)
						)
					),
					'fakelogin' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/fakelogin',
							'defaults' => array(
								'action' => 'fakelogin'
							)
						)
					)
				)
			),
			'help' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/help[/:action]',
					'defaults' => array(
						'controller' => 'Application\Controller\Help',
						'action' => 'index'
					)
				)
			),
			'settings' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/settings[/:action]',
					'defaults' => array(
						'controller' => 'Application\Controller\Setting',
						'action' => 'editme'
					)
				)
			),
			'documentation' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ranchbe/documentation[/:action]',
					'defaults' => array(
						'controller' => 'Application\Controller\Documentation',
						'action' => 'index'
					)
				)
			),
			'session' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/session/manager[/:action]',
					'defaults' => array(
						'controller' => 'Application\Controller\Session',
						'action' => 'index'
					)
				)
			),
			'object' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/object[/:action]',
					'defaults' => array(
						'controller' => 'Application\Controller\Object',
						'action' => 'view'
					)
				)
			),
			'notauthorized' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/notauthorized[/:rightname/:resourcecn][/]',
					'defaults' => array(
						'controller' => 'Application\Controller\Error',
						'action' => 'notauthorized'
					)
				)
			),
			'sandbox' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/sandbox[/:action]',
					'defaults' => array(
						'controller' => 'Application\Controller\Sandbox',
						'action' => 'index'
					)
				)
			),
			// The following is a route to simplify getting started creating
			// new controllers and actions without needing to create a new
			// module. Simply drop new controllers in, and you can access them
			// using the path /application/:controller/:action
			'application' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/application[/:action]',
					'defaults' => array(
						'__NAMESPACE__' => 'Application\Controller',
						'controller' => 'Index',
						'action' => 'index'
					)
				)
			)
		)
	),
	'console' => array(
		'router' => array(
			'routes' => array(
				'tool-daogen' => array(
					'type' => 'simple',
					'options' => array(
						'route' => 'tools daogenerator [-h] [--class=] [--tofile=] [--template=] [--model=]',
						'defaults' => array(
							'controller' => 'Application\Controller\CliTools',
							'action' => 'daogenerator'
						)
					)
				),
				'tool-errorcode-extractor' => array(
					'type' => 'simple',
					'options' => array(
						'route' => 'tools errorcodeextractor --path=',
						'defaults' => array(
							'controller' => 'Application\Controller\CliTools',
							'action' => 'extractErrorCode'
						)
					)
				),
				'tool-sql-extractor' => array(
					'type' => 'simple',
					'options' => array(
						'route' => 'tools sqlextractor --path= --tofile=',
						'defaults' => array(
							'controller' => 'Application\Controller\CliTools',
							'action' => 'extractSql'
						)
					)
				),
				'tool-totraduct' => array(
					'type' => 'simple',
					'options' => array(
						'route' => 'tools totraduct --path= --tofile= --lang=',
						'defaults' => array(
							'controller' => 'Application\Controller\CliTools',
							'action' => 'toTraduct'
						)
					)
				),
				'tool-build' => array(
					'type' => 'simple',
					'options' => array(
						'route' => 'tools build',
						'defaults' => array(
							'controller' => 'Application\Controller\CliTools',
							'action' => 'build'
						)
					)
				),
				'tool-sample-data-files' => array(
					'type' => 'simple',
					'options' => array(
						'route' => 'tools samplefiles [--user=] [--pwd=]',
						'defaults' => array(
							'controller' => 'Application\Controller\CliTools',
							'action' => 'generateSampleDataFiles'
						)
					)
				),
				'test-run' => array(
					'type' => 'simple',
					'options' => array(
						'route' => 'test [<class>] [<method>] [-a] [-p] [--loop=]',
						'defaults' => array(
							'controller' => 'Application\Controller\Test',
							'action' => 'test'
						)
					)
				)
			)
		)
	),
	'navigation' => array(
		'default' => array(
			array(
				'label' => 'Home',
				'route' => 'home',
				'pages' => array(
					array(
						'label' => 'Projects',
						'route' => 'ged-project'
					),
					array(
						'label' => 'Workitems',
						'route' => 'ged-workitem',
						'pages' => array(
							array(
								'label' => 'Documents',
								'route' => 'ged-document-manager',
								'params' => array(
									'spacename' => 'workitem'
								)
							)
						)
					),
					array(
						'label' => 'Cadlibs',
						'route' => 'ged-cadlib',
						'pages' => array(
							array(
								'label' => 'Documents',
								'route' => 'ged-document-manager',
								'params' => array(
									'spacename' => 'cadlib'
								)
							)
						)
					),
					array(
						'label' => 'Mockups',
						'route' => 'ged-mockup',
						'pages' => array(
							array(
								'label' => 'Documents',
								'route' => 'ged-document-manager',
								'params' => array(
									'spacename' => 'mockup'
								)
							)
						)
					),
					array(
						'label' => 'Bookshops',
						'route' => 'ged-bookshop',
						'pages' => array(
							array(
								'label' => 'Documents',
								'route' => 'ged-document-manager',
								'params' => array(
									'spacename' => 'bookshop'
								)
							)
						)
					)
				)
			)
		)
	),
	'service_manager' => array(
		'abstract_factories' => array(
			'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
			'Zend\Log\LoggerAbstractServiceFactory'
		),
		'factories' => array(
			'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory'
		),
		'aliases' => array(
			'translator' => 'MvcTranslator'
		)
	),
	'translator' => array(
		'locale' => 'en_US',
		'translation_file_patterns' => array(
			array(
				'type' => 'gettext',
				'base_dir' => __DIR__ . '/../language',
				'pattern' => '%s.mo'
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Application\Controller\Home' => 'Application\Controller\HomeController',
			'Application\Controller\Index' => 'Application\Controller\IndexController',
			'Application\Controller\Auth' => 'Application\Controller\AuthController',
			'Application\Controller\Help' => 'Application\Controller\HelpController',
			'Application\Controller\Search' => 'Application\Controller\SearchController',
			'Application\Controller\Object' => 'Application\Controller\ObjectController',
			'Application\Controller\Error' => 'Application\Controller\ErrorController',
			'Application\Controller\Extendedproperties' => 'Application\Controller\ExtendedpropertiesController',
			'Application\Controller\Documentation' => 'Application\Controller\DocumentationController',
			'Application\Controller\Session' => 'Application\Controller\SessionController',
			'Application\Controller\Setting' => 'Application\Controller\SettingController',
			'Application\Controller\Sandbox' => 'Application\Controller\SandboxController',
			'Application\Controller\CliTools' => 'Application\Controller\CliToolsController',
			'Application\Controller\Test' => 'Application\Controller\CliTestController'
		)
	),
	'view_manager' => array(
		'display_not_found_reason' => true,
		'display_exceptions' => true,
		'doctype' => 'HTML5',
		'not_found_template' => 'error/404',
		'exception_template' => 'error/index',
		'template_map' => [
			'module_layouts' => [
				'Ranchbe' => 'layout/ranchbe'
			],
			'layout/ranchbe' => __DIR__ . '/../view/layout/ranchbe.phtml',
			'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
			'layout/consult' => __DIR__ . '/../view/layout/consult.phtml',
			'error/404' => __DIR__ . '/../view/error/404.phtml',
			'error/403' => __DIR__ . '/../view/error/403.phtml',
			'error/500' => __DIR__ . '/../view/error/500.phtml',
			'error/index' => __DIR__ . '/../view/error/index.phtml'
		],
		'template_path_stack' => [
			__DIR__ . '/../view'
		],
		'strategies' => []
	),
	'main_menu' => [],
	'menu' => [
		'user' => [],
		'application' => [],
		'myspace' => [],
		'admin' => []
	]
);
