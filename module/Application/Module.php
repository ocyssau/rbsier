<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\EventManager\EventInterface as Event;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\View\HelperPluginManager;
use Ranchbe;

/**
 *
 * @author olivier
 *
 */
class Module implements AutoloaderProviderInterface
{

	/**
	 *
	 * @param Event $event
	 * @throws \Exception
	 */
	public function onBootstrap(Event $event)
	{
		$serviceManager = $event->getApplication()->getServiceManager();
		$config = $serviceManager->get('Configuration');

		/**/
		if ( !isset($config['rbp']) ) {
			throw new \Exception('Config for Ranchbe is not set', E_ERROR);
		}

		$request = $event->getRequest();
		if ( is_a($request, "\Zend\Http\Request") ) {
			$isService = $request->getQuery('isService', $request->getPost('isService', null));
			if ( $request->isXmlHttpRequest() || $isService !== null ) {
				$isService = true;
			}
			else {
				$isService = false;
			}
		}
		$event->isService = $isService;

		$rbConfig = $config['rbp'];
		$ranchbe = Ranchbe::get();
		$rbServiceManager = $ranchbe->getServiceManager();
		$rbServiceManager->isService($isService);
		$ranchbe->setConfig($rbConfig);
		$ranchbe->setErrorStack($rbServiceManager->getErrorStack());
		$ranchbe->setLogger($rbServiceManager->getLogger());

		try {
			$rbServiceManager->getDaoService();
		}
		catch( \Throwable $e ) {
			$ranchbe->getLogger()->log("Db connexion is not available > " . $e->getMessage());
		}
		$rbServiceManager->getSessionManager();
		rbinit_rbservice($rbConfig);

		if ( php_sapi_name() == 'cli' ) {
			rbinit_cli();
		}
		else {
			rbinit_web();
		}

		if ( DEBUG === true ) {
			rbinit_checkInstall($rbConfig);
			rbinit_enableDebug(true);
			error_reporting(E_ALL);
		}
		else {
			rbinit_enableDebug(false);
		}

		rbinit_filesystem($ranchbe);

		$eventManager = $event->getApplication()->getEventManager();

		/* attach layout specifics to modules configuration */
		$eventManager->getSharedManager()->attach(AbstractActionController::class, 'dispatch', function ($event) {

			$controller = $event->getTarget();

			if ( $controller instanceof \Application\Controller\AbstractController ) {
				$isService = $event->isService;
				if ( $isService ) {
					$controller->initAjax();
				}
				else {
					$controller->init();
				}
			}
		}, 100);

		/* configure permanent vars to view and layout */
		$eventManager->getSharedManager()->attach(\Zend\Mvc\Application::class, 'render', function ($event) {
			$request = $event->getRequest();
			if ( $request instanceof \Zend\Console\Request ) {
				return;
			}

			try {
				/* @var \Zend\Mvc\MvcEvent $event */
				/* @var \Zend\Http\PhpEnvironment\Request $request*/
				$config = $event->getApplication()
					->getServiceManager()
					->get('Configuration')['rbp'];

				$layout = $event->getViewModel();
				$baseurl = $request->getBaseUrl();

				/* Force no cache */
				/* @var \Zend\Http\Response $respons*/
				$respons = $event->getResponse();
				$respons->getHeaders()
					->addHeaders([
					'Cache-Control' => 'no-cache, must-revalidate',
					'Expires' => 'Wed, 01 Jan 2020 00:00:00 GMT',
					'Pragma' => 'no-cache'
				]);

				ini_set('default_charset', $config['view.charset']);
				$dateformat = str_replace('-Y', '-yy', $config['view.shortdate.format']);
				$dateformat = str_replace('d-', 'dd-', $dateformat);
				$dateformat = str_replace('-m', '-mm', $dateformat);
				$shortDateformat = str_replace('%', '', $dateformat);

				$dateformat = str_replace('-Y', '-yy', $config['view.longdate.format']);
				$dateformat = str_replace('d-', 'dd-', $dateformat);
				$dateformat = str_replace('-m', '-mm', $dateformat);
				$longDateformat = str_replace('%', '', $dateformat);

				$variables = [
					'debug' => (DEBUG) ? 1 : 0,
					'siteName' => $config['view.sitename'],
					'logo' => $config['view.url.logo'],
					'logoUrl' => $baseurl . '/' . $config['view.url.logo'],
					'favicon' => $baseurl . '/' . $config['view.url.favicon'],
					'shortcutIcon' => $config['view.url.shortcuticon'],
					'charset' => $config['view.charset'],
					'lang' => $config['view.lang'],
					'allowUserPrefs' => $config['user.allowprefs'],
					'baseurl' => $baseurl,
					'baseJsUrl' => $baseurl . '/public/js',
					'baseCssUrl' => $baseurl . '/public/css',
					'baseImgUrl' => $baseurl . '/img',
					'baseCustomImgUrl' => $baseurl . '/data/img',
					'doctypeIconBaseUrl' => $baseurl . '/' . $config['icons.doctype.compiled.url'],
					'doctypeSrcIconBaseUrl' => $baseurl . '/' . $config['icons.doctype.source.url'],
					'thumbnailBaseurl' => $config['thumbnails.url'],
					'iconsFileBaseurl' => $config['icons.file.url'],
					'rbgateServerUrl' => $config['rbgate.server.url'],
					'shortDateformat' => $shortDateformat,
					'longDateformat' => $longDateformat,
					'jsReload' => 11,
					'cssReload' => 11
				];

				$layout->setVariables($variables);
				if ( count($layout->getChildren()) > 0 ) {
					$view = $layout->getChildren()[0];
					$view->setVariables($layout->getVariables());
				}

				$layout->cssSheet = $config['view.stylesheet'];
				$layout->cssSheet2 = $config['view.stylesheet.custom'];
			}
			catch( \Throwable $e ) {}
		}, 101);

		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);

		/** handle the dispatch error (exception) */
		$eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, [
			$this,
			'handleError'
		]);

		/** handle the view render error (exception) */
		$eventManager->attach(MvcEvent::EVENT_RENDER_ERROR, [
			$this,
			'handleError'
		]);

		/** convert json request to php array */
		/* @var \Zend\Http\Request $request */
		$request = $event->getRequest();
		if ( is_a($request, "\Zend\Http\Request") ) {
			/* @var \Zend\Http\Header\ContentType $contentType */
			if ( $contentType = $request->getHeader('Content-Type') ) {

				if ( $contentType->getMediaType() == 'application/json' ) {

					$jsonData = json_decode($request->getContent(), true);

					$parameter = new \Zend\Stdlib\Parameters($jsonData);

					if ( $request->isPost() ) {
						$request->setPost($parameter);
					}
					else {
						$request->setQuery($parameter);
					}
				}
			}
		}
	}

	/**
	 *
	 * @param MvcEvent $event
	 */
	public function handleError(MvcEvent $event)
	{
		$request = $event->getRequest();
		if ( $request instanceof \Zend\Console\Request ) {}
		elseif ( $event->getRequest()->isXmlHttpRequest() ) {
			//$exception = $event->getParam('exception');
			$viewModel = $event->getResult();
			$viewModel->setTerminal(true);
			$viewModel->setTemplate('error/service');
		}
	}

	/**
	 */
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	/**
	 */
	public function getViewHelperConfig()
	{
		return [
			'invokables' => [
				'tableSqlSortableHeader' => 'Application\View\Helper\TableSqlSortableHeader',
				'accessCodeFormater' => 'Application\View\Helper\AccessCodeFormater',
				'categoryIdToName' => 'Application\View\Helper\CategoryIdToName',
				'userIdToName' => 'Application\View\Helper\UserIdToName',
				'processIdToName' => 'Application\View\Helper\ProcessIdToName',
				'projectIdToName' => 'Application\View\Helper\ProjectIdToName',
				'versionFormater' => 'Application\View\Helper\VersionFormater',
				'getPostit' => 'Application\View\Helper\GetPostit',
				'getThumbnail' => 'Application\View\Helper\GetThumbnail',
				'dateFormater' => 'Application\View\Helper\DateFormater',
				'doctypeFormater' => 'Application\View\Helper\DoctypeFormater',
				'fileicon' => 'Application\View\Helper\FileIcon',
				'displayPoint' => 'Application\View\Helper\DisplayPoint',
				'filesizeFormater' => 'Application\View\Helper\FilesizeFormater',
				'displayAddress' => 'Application\View\Helper\DisplayAddress',
				'contextualMenu' => 'Application\View\Helper\ContextualMenu',
				'cidToClass' => 'Application\View\Helper\CidToClass',
				'stringify' => 'Application\View\Helper\Stringify',
				'yesOrNo' => 'Application\View\Helper\YesOrNo',
				'secondToConvenientFormat' => 'Application\View\Helper\SecondToConvenientFormat',
				'getUnreadmessage' => 'Application\View\Helper\GetUnreadMessage',
				'displayPosition' => 'Application\View\Helper\DisplayPosition',
				'listFormater' => 'Application\View\Helper\ListFormater',
				'displayMeasure' => 'Application\View\Helper\DisplayMeasure',
				'displayExtended' => 'Application\View\Helper\DisplayExtended',
				'formTreeview' => 'Application\Form\View\Helper\FormTreeview'
			],
			'factories' => [
				'formRow' => function (HelperPluginManager $helperPluginManager) {
					/* @var \Zend\Form\View\Helper\FormElementErrors $formElementErrorsViewHelper*/
					$formElementErrorsViewHelper = $helperPluginManager->get('formElementErrors');
					$formElementErrorsViewHelper->setAttributes(array(
						'class' => 'alert-danger rb-form-error'
					));
					$formRow = new \Zend\Form\View\Helper\FormRow();
					$formRow->setRenderErrors($formElementErrorsViewHelper);
					return $formRow;
				},
				'Application\Form\View\Helper\FormTreeview' => function ($sm) {
					return new \Application\Form\View\Helper\FormTreeview();
				}
			]
		];
	}

	/**
	 *
	 */
	public function getControllerPluginConfig()
	{
		return array(
			'invokables' => [
				'myForward' => \Application\Controller\Plugin\MyForward::class
			],
			'factories' => [
				'flashMessenger' => function ($pluginManager) {
					/* @var \Zend\Mvc\Controller\PluginManagerSM2 $pluginManager */
					$flash = Ranchbe::get()->getServiceManager()->getFlashMessenger();
					return $flash;
				},
				'layoutSelector' => function ($pluginManager) {
					/* @var \Zend\Mvc\Controller\PluginManagerSM2 $pluginManager */
					$sessionStorage = Ranchbe::get()->getServiceManager()
						->getSessionManager()
						->getStorage();
					$layoutSelector = new \Application\Controller\Plugin\LayoutSelector($sessionStorage);
					return $layoutSelector;
				},
				'basecamp' => function ($pluginManager) {
					/* @var \Zend\Mvc\Controller\PluginManagerSM2 $pluginManager */
					$sessionStorage = Ranchbe::get()->getServiceManager()
						->getSessionManager()
						->getStorage();
					$basecamp = new \Application\Controller\Plugin\Basecamp($sessionStorage);
					return $basecamp;
				},
				'columnSelector' => function ($pluginManager) {
					/* @var \Zend\Mvc\Controller\PluginManagerSM2 $pluginManager */
					$sessionManager = Ranchbe::get()->getServiceManager()->getSessionManager();
					$columnSelector = new \Application\Controller\Plugin\ColumnSelectorFactory($sessionManager);
					return $columnSelector;
				},
				'flood' => function ($pluginManager) {
					/* @var \Zend\Mvc\Controller\PluginManagerSM2 $pluginManager */
					$ranchbe = Ranchbe::get();
					$sessionManager = $ranchbe->getServiceManager()->getSessionManager();
					$flood = new \Application\Controller\Plugin\Flood($sessionManager, $ranchbe->getConfig());
					return $flood;
				}
			]
		);
	}

	/**
	 */
	public function getServiceConfig()
	{
		return array(
			'factories' => [
				'Mail' => function ($sm) {
					return Ranchbe::get()->getServiceManager()->getMailService();
				},
				'Ranchbe' => function ($sm) {
					return Ranchbe::get();
				},
				'Session' => function ($sm) {
					$sessionStorage = Ranchbe::get()->getServiceManager()
						->getSessionManager()
						->getStorage();
					//$session = new \Application\Controller\Plugin\Session($sessionStorage);
					return $sessionStorage;
				}
			],
			'aliases' => [],
			'abstract_factories' => [],
			'invokables' => [],
			'services' => [],
			'shared' => []
		);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\ModuleManager\Feature.AutoloaderProviderInterface::getAutoloaderConfig()
	 */
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => [
				'namespaces' => [
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
					'Service' => realpath('./module/Service/src/Service'),
					'PhpOffice\PhpWord' => realpath('./vendor/ocyssau/PHPWord/src/PhpWord')
				]
			]
		);
	}
}
