<?php
namespace Application\Validator;

use Zend\Validator\Exception\RuntimeException;

/**
 *
 *
 */
class Json implements \Zend\Validator\ValidatorInterface
{

	/**
	 * @var array
	 */
	protected $message = array();

	/**
	 * Returns true if and only if $value meets the validation requirements
	 *
	 * If $value fails validation, then this method returns false, and
	 * getMessages() will return an array of messages that explain why the
	 * validation failed.
	 *
	 * @param  mixed $value
	 * @return bool
	 * @throws RuntimeException If validation of $value is impossible
	 */
	public function isValid($value)
	{
		if ( is_null($value) || $value == '' ) {
			return true;
		}
		if ( !is_string($value) ) {
			throw new RuntimeException('Validation of this data is not possible');
		}

		/* try to decode */
		if ( $value == '{}' || $value == '[]' ) {
			return true;
		}

		$d = json_decode($value, true);
		if ( !$d ) {
			$this->message[] = 'Json synthax error. Check it and retry.';
			return false;
		}

		return true;
	}

	/**
	 * Returns an array of messages that explain why the most recent isValid()
	 * call returned false. The array keys are validation failure message identifiers,
	 * and the array values are the corresponding human-readable message strings.
	 *
	 * If isValid() was never called or if the most recent isValid() call
	 * returned true, then this method returns an empty array.
	 *
	 * @return array
	 */
	public function getMessages()
	{
		return $this->message;
	}
}
