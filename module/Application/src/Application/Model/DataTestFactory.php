<?php
namespace Application\Model;

/** 
 * Set of factories ti init some datas usable in tests.
 *
 */
class DataTestFactory
{

	/**
	 *
	 * @return \Rbplm\Ged\Doctype
	 */
	public static function doctypeFactory($name)
	{
		$doctype = \Rbplm\Ged\Doctype::init($name);
		$doctype->hydrate([
			'id' => 20,
			'uid' => $name . '345',
			'name' => $name,
			'number' => strtoupper($name),
			'description' => 'FORTEST ' . $name,
			'fileExtensions' => [
				'pptx'
			],
			'fileType' => 'file',
			'priority' => 1,
			'mayBeComposite' => true,
			'templateId' => null,
			'templateUid' => null,
			'maskId' => null,
			'maskUid' => null,
			'docseeder' => true,
			'numberGeneratorClass' => null,
			'numberGenerator' => null
		]);
		return $doctype;
	}

	/**
	 *
	 * @return \Rbplm\Ged\Document\Version:
	 */
	public static function documentFactory($name)
	{
		$document = \Rbplm\Ged\Document\Version::init($name);
		$document->hydrate([
			'id' => 20,
			'uid' => $name . 'Document345',
			'name' => $name,
			'number' => strtoupper($name),
			'description' => 'FORTEST ' . $name,
			'accessCode' => 0,
			'label' => 'LabelFor-' . $name,
			'spacename' => 'bookshop',
			'asTemplate' => 1
		]);
		return $document;
	}

	/**
	 *
	 * @return \Rbplm\Org\Workitem
	 */
	public static function containerFactory($spacename, $name)
	{
		$container = \Rbplm\Org\Workitem::init($name);
		$container->hydrate([
			'id' => 20,
			'uid' => $name . 'Container345',
			'name' => $name,
			'number' => strtoupper($name),
			'description' => 'FORTEST ' . $name,
			'spacename' => $spacename
		]);
		return $container;
	}

	/**
	 *
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	public static function docfileFactory($filepath)
	{
		$name = basename($filepath);
		$docfile = \Rbplm\Ged\Docfile\Version::init($name);
		$docfile->hydrate([
			'id' => 20,
			'uid' => $name . 'Docfile345',
			'name' => $name,
			'number' => strtoupper($name),
			'accessCode' => 0,
			'roles' => [
				'MAIN'
			],
			'mainrole' => 'MAIN'
		]);

		/* @var \Rbplm\Vault\Record $data */
		$data = \Rbplm\Vault\Record::init($name);
		$fsdata = new \Rbplm\Sys\Fsdata($filepath);
		$data->setFsdata($fsdata);
		$docfile->setData($data);

		return $docfile;
	}

	/**
	 *
	 * @return \Docseeder\Model\Template\Mask
	 */
	public static function maskFactory($name)
	{
		$mask = \Docseeder\Model\Template\Mask::init();
		$mask->hydrate([
			'id' => 20,
			'uid' => $name . 'Mask0354465',
			'name' => $name,
			'number' => strtoupper($name),
			'map' => [],
			'hooks' => []
		]);
		return $mask;
	}

	/**
	 *
	 * @return \Rbplm\People\User
	 */
	public static function userFactory($name)
	{
		/* @var \Rbplm\People\User $user */
		$user = \Rbplm\People\User::init();
		$user->hydrate([
			'id' => 20,
			'uid' => $name . 'User0354465',
			'name' => $name,
			'login' => 'r_johnson',
			'firstname' => 'robert',
			'lastname' => 'johnson',
			'hooks' => []
		]);
		return $user;
	}
}
