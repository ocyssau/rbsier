<?php
namespace Application\Model;

/**
 * Abstract class to be extends by all test classes
 * Methods to run in tests must be named with "Test" suffix
 * In tests methods, may be use assertion as :
 * 
 * 		assert(false===true, 'a description')
 * 
 * To use assertions, directive zend.assertions must be set to 1 ini php.ini
 * 
 * @author ocyssau
 * @see Application\Controller\CliTestController
 *
 */
abstract class AbstractTest
{

	/**
	 * 
	 * @var \Application\Controller\CliTestController
	 */
	protected $controller;

	/**
	 * 
	 */
	function __construct($controller)
	{
		$this->controller = $controller;
	}

	/**
	 *
	 */
	function init()
	{}

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	public function setUp()
	{}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	public function tearDown()
	{}
}
