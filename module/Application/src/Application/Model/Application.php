<?php
namespace Application\Model;

/**
 * 
 *
 */
class Application
{

	/**
	 * @var integer
	 */
	public static $classId = 1;

	public static $id = 'application';

	public static $version = '0.1';

	public static $build = '';

	/**
	 * 
	 * @return string
	 */
	public static function getCopyright()
	{
		$copyright = '&copy; 2015 - ' . date('Y') . ' By Olivier CYSSAU pour SIER - All rights reserved.';
		return $copyright;
	}

	/**
	 * 
	 * @return string
	 */
	public static function getVersion()
	{
		return self::$version;
	}

	/**
	 * 
	 * @return string
	 */
	public static function getId()
	{
		return self::$id;
	}
}
