<?php
//%LICENCE_HEADER%
namespace Application\Cron;

use Rbs\Batch\CallbackInterface;

/**
 * CallbackInterface interface
 *
 * Must be implemented by callbock for execution of cronTasks
 *
 */
class Test implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 *
	 */
	public function run()
	{
		echo "Message 1 \n";
		echo "Message 2 \n";

		$return = 0;
		if ( $return != 0 ) {
			throw new \RuntimeException('Command failed');
		}

		sleep(5);

		echo "Message 3 \n";
		return $this;
	}
}
