<?php
//%LICENCE_HEADER%
namespace Application\Cron;

use Rbs\Batch\CallbackInterface;

/**
 * CallbackInterface interface
 * 
 * Must be implemented by callbock for execution of cronTasks
 * 
 */
class DbBackup implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 *
	 */
	public function run()
	{
		return $this->daily();
	}

	/**
	 *
	 */
	public function daily()
	{
		$config = \Ranchbe::get()->getConfig();
		//$backupRetentionDelay = $config['dbbackup.daily.retention.delay'];

		$suffix = '_daily_backup';
		$dbConfig = $config['db']['default']['params'];
		$toDir = $config['path.database.backup'];
		$this->save($suffix, $dbConfig, $toDir);

		/* cleanup of olds backups files */
		//$olderTime = time() - ($backupRetentionDelay * 3600 * 24);
		//self::clean($toDir, $olderTime, $suffix);
		return $this;
	}

	/**
	 *
	 */
	public function weekly()
	{
		$config = \Ranchbe::get()->getConfig();
		//$backupRetentionDelay = $config['dbbackup.weekly.retention.delay'];

		$suffix = '_weekly_backup';
		$dbConfig = $config['db']['default']['params'];
		$toDir = $config['path.database.backup'];
		$this->save($suffix, $dbConfig, $toDir);

		/* cleanup of olds backups files */
		//$olderTime = time() - ($backupRetentionDelay * 3600 * 24);
		//self::clean($toDir, $olderTime, $suffix);
		return $this;
	}

	/**
	 * 
	 */
	protected function save($suffix = '_backup', $dbConfig, $toDir)
	{
		$host = $dbConfig['host'];
		$user = $dbConfig['username'];
		$password = $dbConfig['password'];
		$db = $dbConfig['dbname'];

		if ( empty($toDir) || !is_dir($toDir) || is_writable($toDir) ) {
			new \Exception("$toDir is not writable");
		}

		echo date(DATE_RFC822) . "\n";
		echo "Backup of $db @ $host \n";

		$mysqldumpcmd = 'mysqldump';
		$toSqlFile = $toDir . '/' . $db . $suffix . '.sql';

		if ( is_file($toSqlFile) ) {
			$i = 1;
			$backupFile = $toSqlFile . '.' . $i;
			copy($toSqlFile, $backupFile);
		}

		echo 'Backup in file ' . $toSqlFile . "\n";

		/* Utilise les fonctions systeme : MySQLdump 
		 Backup in SQL format */
		$command = "$mysqldumpcmd --opt -h $host -u $user --password=$password $db --result-file=$toSqlFile";
		$return = null;
		system($command, $return);

		if ( $return != 0 ) {
			throw new \RuntimeException('Dump comand failed');
		}

		/**/
		echo "End of backup at " . date(DATE_RFC822) . "\n";
		return $this;
	}

	/**
	 * @param string $path
	 * @param integer $older Timestamp
	 */
	public static function clean($path, $older, $suffix)
	{
		echo 'Cleanup old backup files' . "\n";
		$directory = new \DirectoryIterator($path);

		$regex = '/' . $suffix . '\.sql\.[0-9]+$/';

		foreach( $directory as $file ) {
			if ( $directory->isFile() ) {
				if ( preg_match($regex, $file->getFilename()) ) {
					$ctime = $file->getCTime();
					if ( $ctime < $older ) {
						echo 'Delete backup file ' . $file->getFilename() . "\n";
						unlink($directory->getPathname());
					}
				}
			}
		}
	}

	/**
	 * Circular logs
	 *
	 * @since version 0.85
	 * @param $task
	 **/
	protected static function circularlogs($task)
	{
		/*
		 * compute date in the past for the archived log to be deleted
		 * compute current date - param as days and format it like YYYYMMDD
		 */
		$firstdate = date("Ymd", time() - ($task->fields['param'] * DAY_TIMESTAMP));

		/*  first look for bak to delete */
		$dir = GLPI_LOG_DIR . "/*.bak";
		$findfiles = glob($dir);
		foreach( $findfiles as $file ) {
			$shortfile = str_replace(GLPI_LOG_DIR . '/', '', $file);
			/* now depending on the format of the name we delete the file (for aging archives) or rename it (will add Ymd.log to the end of the file) */
			$match = null;
			if ( preg_match('/.+[.]log[.](\\d{8})[.]bak$/', $file, $match) > 0 ) {
				if ( $match[1] < $firstdate ) {
					$task->addVolume(1);
					if ( unlink($file) ) {
						$task->log(sprintf(__('Deletion of archived log file: %s'), $shortfile));
						$actionCode = 1;
					}
					else {
						$task->log(sprintf(__('Unable to delete archived log file: %s'), $shortfile));
						$error = true;
					}
				}
			}
		}

		/* second look for log to archive */
		$dir = GLPI_LOG_DIR . "/*.log";
		$findfiles = glob($dir);
		foreach( $findfiles as $file ) {
			$shortfile = str_replace(GLPI_LOG_DIR . '/', '', $file);
			/* rename the file */
			/* will add to filename a string with format YYYYMMDD (= current date) */
			$newfilename = $file . "." . date("Ymd", time()) . ".bak";
			$shortnewfile = str_replace(GLPI_LOG_DIR . '/', '', $newfilename);

			$task->addVolume(1);
			if ( !file_exists($newfilename) && rename($file, $newfilename) ) {
				$task->log(sprintf(__('Archiving log file: %1$s to %2$s'), $shortfile, $shortnewfile));
				$actionCode = 1;
			}
			else {
				$task->log(sprintf(__('Unable to archive log file: %1$s. %2$s already exists. Wait till next day.'), $shortfile, $shortnewfile));
				$error = true;
			}
		}

		if ( $error ) {
			return -1;
		}
		return $actionCode;
	}
}
