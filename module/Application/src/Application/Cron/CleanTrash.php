<?php
//%LICENCE_HEADER%
namespace Application\Cron;

use Rbs\Batch\CallbackInterface;

/**
 * CallbackInterface interface
 *
 * Must be implemented by callbock for execution of cronTasks
 *
 */
class CleanTrash implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 *
	 */
	public function run()
	{
		$config = \Ranchbe::get()->getConfig();
		$trashDir = $config['path.reposit.trash'];

		if ( $trashDir == '' ) {
			die('None trash is configured');
		}

		/* in days */
		$trashRetentionDelay = $config['trash.retention.delay'];
		$olderTime = time() - ($trashRetentionDelay * 3600 * 24);
		self::clean($trashDir, $olderTime);
		return $this;
	}

	/**
	 * @param string $path
	 * @param integer $older timestamp of older date
	 */
	public static function clean($path, $older)
	{
		$directory = new \DirectoryIterator($path);
		foreach( $directory as $file ) {
			if ( $directory->isFile() ) {
				$ctime = $file->getCTime();
				if ( $ctime < $older ) {
					echo sprintf('Delete file %s', $directory->getFilename()) . "\n";
					unlink($directory->getPathname());
				}
			}
			elseif ( $directory->isDir() && !$directory->isDot() ) {
				self::clean($directory->getPathname(), $older);
			}
		}
	}
}
