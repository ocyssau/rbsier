<?php
namespace Application\Controller;

/* Zend */
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;
use Zend\View\Model\ViewModel;

/**
 *
 *
 */
class DocumentationController extends AbstractActionController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractController::dispatch()
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		$this->view = new ViewModel();
		parent::dispatch($request, $respons);
	}

	/**
	 *
	 */
	public function developerAction()
	{
		$view = $this->view;
		$this->layout('layout/ranchbe');
		
		if ( is_file('public/html/documentation/developer/html/index.html') ) {
			$baseurl = $this->getRequest()->getBaseUrl();
			return $this->redirect()->toUrl($baseurl . '/html/documentation/developer/html/index.html');
		}

		return $view;
	}

	/**
	 *
	 */
	public function apiAction()
	{
		$view = $this->view;
		$this->layout('layout/ranchbe');

		if ( is_file('public/html/documentation/api/html/index.html') ) {
			$baseurl = $this->getRequest()->getBaseUrl();
			return $this->redirect()->toUrl($baseurl . '/html/documentation/api/html/index.html');
		}

		return $view;
	}

	/**
	 *
	 */
	public function enduserAction()
	{
		$view = $this->view;
		$this->layout('layout/ranchbe');

		return $view;
	}
}
