<?php
namespace Application\Controller;

use Rbplm\People;
use Rbs\People\User\Preference as UserPreference;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\NotExistingException;

/**
 *
 *
 */
class SettingController extends AbstractController
{

	/** @var string $pageId */
	public $pageId = 'user_settings';

	/** @var string $defaultSuccessForward */
	public $defaultSuccessForward = 'home';

	/** @var string $defaultSuccessForward */
	public $defaultFailedForward = 'settings';

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function init()
	{
		parent::init();
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('User')->activate('user-settings');
		$this->layout()->tabs = $tabs;
	}

	/**
	 *
	 */
	public function editmeAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}
		else {
			$validate = false;
		}

		/* Helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(UserPreference::$classId);
		$currentUser = People\CurrentUser::get();
		$userId = $currentUser->getId();
		$preference = UserPreference::get($currentUser);

		try {
			$dao->loadFromOwnerId($preference, $userId);
		}
		catch( NotExistingException $e ) {}

		$form = new \Application\Form\Setting\EditmeForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($preference);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				/*save prefs to db */
				$dao->save($preference);

				/* update session */
				/** @var \Rbs\Auth\Storage $storage */
				$storage = $this->authservice->getStorage();
				$datas = $storage->read();
				$datas['preferences'] = $preference->getPreferences();
				$storage->write($datas);

				return $this->successForward();
			}
		}

		$view->form = $form;
		$view->pageTitle = 'Preference Of User ' . $currentUser->getLogin();
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$view = $this->view;
		return $view;
	}
}
