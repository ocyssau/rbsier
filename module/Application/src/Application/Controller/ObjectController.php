<?php
namespace Application\Controller;

/**
 *
 *
 */
class ObjectController extends AbstractController
{

	/**
	 * GET method
	 *
	 * @param spacename
	 * @param id
	 * @param class
	 *
	 */
	public function viewAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename', null);
			$id = $request->getQuery('id', null);
			$cid = $request->getQuery('cid', null);
		}

		if ( $cid == \Rbplm\Ged\Document\Version::$classId ) {
			$params = array(
				"id=$id",
				"spacename=$spacename"
			);
			$url = $this->url()->fromRoute('ged-document-detail') . '?' . implode('&', $params);
			return $this->redirect()->toUrl($url);
		}
		elseif ( $cid == \Rbplm\Ged\Docfile\Version::$classId ) {
			$params = array(
				"id=$id",
				"spacename=$spacename"
			);
			$url = $this->url()->fromRoute('ged-docfile-detail') . '?' . implode('&', $params);
			return $this->redirect()->toUrl($url);
		}
		elseif ( $cid == \Rbplm\Org\Workitem::$classId ) {
			$params = array(
				"id=$id",
				"spacename=$spacename"
			);
			$url = $this->url()->fromRoute('ged-container-detail') . '?' . implode('&', $params);
			return $this->redirect()->toUrl($url);
		}
		elseif ( $cid == \Rbplm\Pdm\Product\Version::$classId ) {
			$params = array(
				"id=$id"
			);
			$url = $this->url()->fromRoute('pdm-product-explorer') . '?' . implode('&', $params);
			return $this->redirect()->toUrl($url);
		}
		elseif ( $cid == \Rbplm\Org\Project::$classId ) {
			$params = array(
				"id=$id"
			);
			$url = $this->url()->fromRoute('ged-project-detail') . '?' . implode('&', $params);
			return $this->redirect()->toUrl($url);
		}
		else {
			throw new \Exception(sprintf('Object class %s is unknow', $cid));
		}
	}
}