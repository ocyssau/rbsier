<?php
namespace Application\Controller;

/**
 * 
 */
class ControllerException extends \Exception
{

	public $controller;

	/**
	 * 
	 * @param string [optional] $message
	 * @param integer [optional] $code
	 * @param \Exception [optional] $previous
	 * @param \Zend\Mvc\Controller\AbstractActionController $controller
	 * 
	 */
	function __construct($message = null, $code = null, $previous = null, $controller = null)
	{
		parent::__construct($message, $code, $previous);
		$this->controller = $controller;
	}
}
