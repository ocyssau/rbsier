<?php
namespace Application\Controller;

/**
 *
 *
 */
class SandboxController extends AbstractController
{

	/**
	 *
	 */
	public function indexAction()
	{
		return $this->view;
	}

	/**
	 *
	 */
	public function routerAction()
	{
		/* @var \Zend\Mvc\Router\Http\TreeRouteStack $router */
		$router = $this->getEvent()->getApplication()->getServiceManager()->get('router');
		var_dump($router->assemble(['id'=>1, 'spacename'=>'workitem'], ['name'=>'ged-document-detail']));
		
		var_dump($router->assemble([], ['name'=>'docflow']));
		
		
		#$url = $baseurl . '/ged/document/detail/%s/%s';
		#$url = sprintf($url, strtolower($document->spacename), $document->getId());
		
		
		die;
	}
	
	/**
	 *
	 */
	public function sessionAction()
	{
		$session = \Ranchbe::get()->getServiceManager()->getSessionManager();
		$container = $session->containerRegistry->get('my5');
		$container->myvar = 50;
		
		/* cleanup a container */
		/* @var \Zend\Session\Storage\SessionArrayStorage $storage */
		//$container->getManager()->getStorage()->clear('my5');
		/* Or, if builded from registry-factory  */
		$session->containerRegistry->remove('my5');
	
		var_dump($container->myvar);
		var_dump($_SESSION);
		die;
	}
	
	/**
	 *
	 */
	public function jsoneditorAction()
	{
		return $this->view;
	}
	
	/**
	 *
	 */
	public function mailAction()
	{
		$view = $this->view;
		//$mailService = $this->getServiceLocator()->get('Mail');
		$mailService = \Ranchbe::get()->getServiceManager()->getMail();
		$message = $mailService->messageFactory();
		$message->setFrom('ranchbe@sierbla.com', 'Ranchbe');
		$message->setBody('FOR TEST');
		$message->setSubject('RANCHBE TEST');
		$message->addTo('olivier.cyssau@sierbla.com', 'Olivier CYSSAU');
		$mailService->send($message);

		return $view;
	}

	/**
	 *
	 */
	public function deferredAction()
	{
		return $this->view;
	}
}
