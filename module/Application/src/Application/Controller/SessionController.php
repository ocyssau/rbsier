<?php
namespace Application\Controller;

use Rbplm\People\CurrentUser;

/**
 */
class SessionController extends AbstractController
{

	/** @var string $pageId */
	public $pageId = 'session_manager';

	/** @var string $defaultSuccessForward */
	public $defaultSuccessForward = 'session/manager/index';

	/** @var string $defaultSuccessForward */
	public $defaultFailedForward = 'session/manager/index';

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function init()
	{
		parent::init();

		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('User')->activate('context');
		$this->layout()->tabs = $tabs;
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		
		/** @var \Rbs\People\User\Context $context */
		$context = \Ranchbe::get()->getServiceManager()->getContext();
		$currentUser = CurrentUser::get();
		
		/* Assign context to view */
		$view->context = $context;
		$view->pageTitle = sprintf('Context session of user "%s"', $currentUser->getLogin());
		return $view;
	}

	/**
	 */
	function resetAction()
	{
		$context = \Ranchbe::get()->getServiceManager()->getContext();
		$context->clean();
		$context->getDao()->save($context);
		
		$sessionStorage = \Ranchbe::get()->getServiceManager()->getSessionManager()->getStorage();
		$sessionStorage->offsetSet(\Application\Form\AbstractFilterForm::SESSION_KEY, null);
		$sessionStorage->offsetSet(\Application\Form\PaginatorForm::SESSION_KEY, null);
		$sessionStorage->offsetSet(\Application\Controller\Plugin\Basecamp::SESSION_KEY, null);
		
		return $this->successForward();
	}
} /* End of class */
