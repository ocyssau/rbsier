<?php
namespace Application\Controller;

use Ranchbe;

/**
 *
 *
 */
class IndexController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function init()
	{
		parent::init();
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$this->layout()->tabs = $tabs;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		return $this->view;
	}

	/**
	 *
	 */
	public function aboutAction()
	{
		$this->layout()->tabs->getTab('Ranchbe')->activate('about');
		return array(
			'version' => array(
				'ver' => Ranchbe::VERSION,
				'build' => Ranchbe::BUILD,
				'copyright' => sprintf(Ranchbe::COPYRIGHT, date('Y'))
			)
		);
	}

	/**
	 *
	 */
	public function phpinfoAction()
	{
		$this->layout()->tabs->getTab('Ranchbe')->activate('about');
	}

	/**
	 * To get log, you must enable read access to current apache running user.
	 * Dont forget to edit /etc/logrotate.d/apache2 to set mods
	 */
	public function syslogAction()
	{
		$view = $this->view;

		$config = \Ranchbe::get()->getConfig();
		$logfile = $config['apache.log.path'];
		if ( is_file($logfile) ) {
			$content = file_get_contents($logfile);
		}
		else {
			$content = 'To get log, you must enable read access to current apache running user' . "\n";
			$content .= 'Edit too /etc/logrotate.d/apache2 to set directive "create root [apache2 user] 640"' . "\n";
		}

		$view->pageTitle = 'Sys Log of Apache: ' . $logfile;
		$view->logfile = $logfile;
		$view->log = $content;
		return $view;
	}

	/**
	 *
	 */
	public function licenceAction()
	{
		$lang = Ranchbe::get()->getConfig('view.lang');
		$file = './licence_' . $lang . '.txt';
		if ( !is_file($file) ) {
			$file = './licence_en.txt';
		}
		$this->view->licence = file_get_contents($file);
		return $this->view;
	}

	/**
	 */
	public function pingAction()
	{
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			var_export($request->getPost());
		}
		else {
			var_export($request->getQuery());
		}
		die();
	}
}
