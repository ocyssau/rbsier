<?php
namespace Application\Controller;

use Application\Controller\AbstractCliController as AbstractController;
use Zend\Console\Request as ConsoleRequest;
use Application\Tools\FileSystemTool;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;

#use Zend\Stdlib\ArrayUtils;
#use Application\Console\Output as Cli;

/**
 * How to use :
 * open terminal.
 * cd [pat/to/ranchbe/root/directory]
 * php public/index.php <method> <params>
 * 
 * @see  https://framework.zend.com/manual/2.3/en/modules/Zend.console.introduction.html
 *
 */
class CliToolsController extends AbstractController
{

	/**
	 */
	public function init()
	{
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	public function daogeneratorAction()
	{
		$request = $this->getRequest();
		if ( !$request instanceof ConsoleRequest ) {
			throw new \RuntimeException('You can only use this action from a console!');
		}

		//$class = $request->getParam('class');
		//$toFile = $request->getParam('tofile', getcwd() . '/tmp/Dao');
		//$templateFile = $request->getParam('template', LIB_PATH . '/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl');
		//$modelFile = $request->getParam('model', LIB_PATH . '/Rbplm/Dao/Schemas/pgsql/MODL.csv');

		//$loader = new \Rbplm\Sys\Meta\Loader\Csv(array('filename' => $modelFile));
		//$model = new \Rbplm\Sys\Meta\Model();
		//$loader->load($model);

		//$generator = new \Rbplm\Dao\Pg\Generator();
		//$generator->generate($class, $toFile, $templateFile, $Model);
	}

	/**
	 *
	 */
	public function modelGeneratorAction()
	{
		$request = $this->getRequest();
		if ( !$request instanceof ConsoleRequest ) {
			throw new \RuntimeException('You can only use this action from a console!');
		}

		$class = $request->getParam('class');
		//$toFile = $request->getParam('tofile', getcwd() . '/tmp/Model');
		//$templateFile = $request->getParam('template', LIB_PATH . '/Rbplm/Model/Schemas/classTemplate.tpl');
		//$modelFile = $request->getParam('model', LIB_PATH . '/Rbplm/Dao/Schemas/pgsql/MODL.csv');

		if ( !$class ) {
			echo "l'option -c est obligatoire, vous devez spécifier la classe model" . CRLF;
			die();
		}

		//$loader = new \Rbplm\Sys\Meta\Loader\Csv(array('filename' => $modelFile));
		//$model = new \Rbplm\Sys\Meta\Model();
		//$loader->load($Model);

		//$generator = new \Rbplm\Model\Generator();
		//$generator->generate($class, $toFile, $templateFile, $model);
	}

	/**
	 *
	 */
	public function generateSampleDataFilesAction()
	{
		$request = $this->getRequest();
		if ( !$request instanceof ConsoleRequest ) {
			throw new \RuntimeException('You can only use this action from a console!');
		}

		$this->authenticate($request);
		$wildspace = new Wildspace(CurrentUser::get());
		$path = $wildspace->getPath();

		$range = range(0, 1000);
		foreach( $range as $i ) {
			$filename = 'file' . $i . '.txt';
			$filepath = $path . '/' . $filename;
			echo "Create file $filepath \n";
			touch($filepath);
			file_put_contents($filepath, 'version 1');
		}
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	public function extractErrorCodeAction()
	{
		$request = $this->getRequest();
		if ( !$request instanceof ConsoleRequest ) {
			throw new \RuntimeException('You can only use this action from a console!');
		}

		$path = $request->getParam('path');

		$extract = new \Application\Tools\ErrorsCode();
		$extract->parseDirectoryTree($path);
		$extract->toStaticClass('Error');
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	public function toTraductAction()
	{
		$request = $this->getRequest();
		if ( !$request instanceof ConsoleRequest ) {
			throw new \RuntimeException('You can only use this action from a console!');
		}

		$lang = $request->getParam('lang', 'fr');
		$path = $request->getParam('path', 'module');
		$toFile = $request->getParam('tofile', "data/lang/$lang/toTraduct.php");
		$langMap = include ("data/lang/$lang/language.php");

		$extractor = new \Application\Tools\ToTraductString();
		$extractor->exists = $langMap;
		$extractor->parseDirectoryTree($path);

		/* save in file */
		$str = var_export($extractor->toTraductStrings, true);
		file_put_contents($toFile, $str);

		echo "result is saved in file $toFile \n";

		if ( $toFile ) {
			$extractor->resultPath = $toFile;
		}
		$extractor->parse($path);
		echo 'See result in ' . $extractor->resultPath . CRLF;
	}

	/**
	 *
	 */
	public function buildAction()
	{
		chdir(__DIR__ . '/../.');
		echo getcwd() . "\n";

		$version = '';
		$build = time();
		$copyright = '2017 Olivier CYSSAU';
		$verbose = false;
		$this->build('c:/temp', $version, $build, $copyright, $verbose);
	}

	/**
	 * 
	 * @param string $tmpdir
	 * @param string $version
	 * @param string $build
	 * @param string $copyright
	 * @param string $verbose
	 */
	protected function build($tmpdir, $version, $build, $copyright, $verbose = false)
	{
		/* Create build directory */
		$sourceDir = realpath(dirname(__FILE__));
		$buildName = '' . $version . '_build' . $build;
		$buildDir = $tmpdir . '/' . $buildName;

		/* Copier les données */
		mkdir($buildDir, 0755, true);
		$filter = [];
		$logger = \Ranchbe::get()->getLogger();
		if ( !FileSystemTool::dircopy($sourceDir, $buildDir, $filter, $logger, true, 0777) ) die("ne peut pas copier $sourceDir vers $buildDir");

		/* clean svn */
		FileSystemTool::cleansvn($buildDir);

		/* nettoyage */
		if ( is_file($buildDir . '/config/autoload/local.php') ) {
			unlink($buildDir . '/config/autoload/local.php');
		}

		if ( is_file($buildDir . '/config/application.config.php') ) {
			unlink($buildDir . '/config/application.config.php');
		}

		if ( is_file($buildDir . '/public/.htaccess') ) {
			unlink($buildDir . '/public/.htaccess');
		}

		if ( is_file($buildDir . '/.buildpath') ) {
			unlink($buildDir . '/.buildpath');
		}

		if ( is_file($buildDir . '/.project') ) {
			unlink($buildDir . '/.project');
		}

		if ( is_dir($buildDir . '/.settings') ) {
			FileSystemTool::removeDir($buildDir . '/.settings', true, $verbose);
		}

		if ( is_dir($buildDir . '/.metadata') ) {
			FileSystemTool::removeDir($buildDir . '/.metadata', true, $verbose);
		}

		if ( is_file($buildDir . '/.zfproject.xml') ) {
			unlink($buildDir . '/.zfproject.xml');
		}

		if ( is_file($buildDir . '/build.log') ) {
			unlink($buildDir . '/build.log');
		}

		/* Set the version and build number */

		$gitId = 1;

		$classFile = $buildDir . '/module/Ranchbe/Ranchbe.php';
		$classContent = file_get_contents($classFile);
		$classContent = str_replace('%BUILD%', $build, $classContent);
		$classContent = str_replace('%GITID%', $gitId, $classContent);
		$classContent = str_replace('%VERSION%', $version, $classContent);
		$classContent = str_replace('%COPYRIGHT%', $copyright, $classContent);
		file_put_contents($classFile, $classContent);

		/* Generate md5 signatures */
		$md5s = FileSystemTool::md5ofAllDirectoryFiles($buildDir, $buildDir, $verbose);
		$signs = array();
		foreach( $md5s as $path => $md5 ) {
			$signs[] = $path . '=>' . $md5;
		}
		file_put_contents($buildDir . '/distrib.sign', implode("\r\n", $signs));

		/* Generate zip */
		$zip = new \ZipArchive();
		$zipfile = $tmpdir . '/' . $buildName . '.zip';
		if ( $zip->open($zipfile, \ZIPARCHIVE::CREATE) !== true ) {
			die("Impossible de créer le zip $zipfile");
		}
		chdir($tmpdir);
		FileSystemTool::zipDir($buildName, $zip, true, $verbose);
		$zip->close();

		/* clean the temp */
		FileSystemTool::removeDir($buildDir, true, $verbose);
	}
}
