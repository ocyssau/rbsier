<?php
namespace Application\Controller;

use Rbplm\People;
use Rbplm\Sys\Fsdata;

/**
 *
 * @author olivier
 *        
 */
abstract class FilemanagerController extends AbstractController
{

	/**
	 * @var \Rbplm\Sys\Directory
	 */
	protected $directory;

	/**
	 * @var \Service\Controller\ServiceController
	 */
	protected $fileService;

	/**
	 * @param People\User $user
	 * @return \Rbplm\Sys\Directory
	 */
	public abstract function getDirectory(People\User $user);

	/**
	 * @return \Service\Controller\ServiceController
	 */
	public abstract function getFileService();

	/**
	 * download a file from the wildspace
	 */
	public function downloadAction()
	{
		$request = $this->getRequest();

		$filename = $request->getQuery('file', null);
		$checked = $request->getQuery('checked', null);

		if ( $checked ) {
			$filename = current($checked);
		}

		$user = People\CurrentUser::get();
		$directory = $this->getDirectory($user);

		$file = $directory->getPath() . '/' . $filename;
		$fsdata = new Fsdata($file);
		$fsdata->download();
	}

	/**
	 * put many files from the wildspace in a zip and download
	 */
	public function downloadzipAction()
	{
		$request = $this->getRequest();
		$files = $request->getQuery('checked', null);

		$user = People\CurrentUser::get();
		$directory = $this->getDirectory($user);

		$tmpFile = uniqid('tempranchbedowloadzip') . '.zip';
		$zipfile = $directory->getPath() . '/' . $tmpFile;

		if ( is_array($files) ) {
			$zip = new \ZipArchive();
			$zip->open($zipfile, \ZipArchive::CREATE);
			foreach( $files as $file ) {
				$zip->addFile($directory->getPath() . '/' . $file, $file);
			}
			$zip->close();

			$fsdata = new Fsdata($zipfile);
			$fsdata->download();
		}
	}

	/**
	 * Uncompress files in wildspace
	 */
	public function uncompressAction()
	{
		$request = $this->getRequest();
		$files = $request->getQuery('checked', []);
		$file = $request->getQuery('file', null);

		/* To increase default TimeOut.No effect if php is in safe_mode */
		set_time_limit(24 * 3600);
		ignore_user_abort(true);

		if ( empty($files) ) {
			$this->errorStack()->error('You must select at least one item');
			return $this->redirect($this->ifSuccessForward);
		}

		$serviceDatas = [];
		$serviceDatas['files'] = [];
		$i = 1;
		foreach( $files as $file ) {
			$serviceDatas['files']['file' . $i] = array(
				'name' => basename($file)
			);
			$i++;
		}

		try {
			$service = $this->getFileService();
			$service->uncompressService($serviceDatas);
		}
		catch( \Exception $e ) {
			throw $e;
		}
	}
}
