<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;
use Ranchbe;
use Rbs\Space\Factory as DaoFactory;

/**
 */
class AuthController extends AbstractController
{

	/** */
	protected $form;

	/** @var string */
	protected $successRedirect = 'home';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::dispatch()
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		$this->authservice = Ranchbe::get()->getServiceManager()->getAuthservice();
		$this->storage = $this->authservice->getStorage();
		$this->init();
		
		$this->layout('layout/ranchbe');

		return AbstractActionController::dispatch($request, $respons);
	}

	/**
	 *
	 */
	public function authIdentityCheck()
	{
		return $this;
	}

	/**
	 *
	 * @return \Application\Form\AuthForm
	 */
	protected function _getForm()
	{
		if ( !$this->form ) {
			$this->form = new \Application\Form\AuthForm();
			$url = $this->url()->fromRoute('auth/authenticate');
			$this->form->setAttribute('action', $url);
			$layout = $this->getRequest()->getQuery('layout', null);
			$applayout = $this->getRequest()->getQuery('applayout', null);
			$this->form->get('layout')->setValue($layout);
			$this->form->get('applayout')->setValue($applayout);
		}
		return $this->form;
	}

	/**
	 * Login
	 */
	public function loginAction()
	{
		/* if already login, redirect to success page */
		if ( $this->getAuthService()->hasIdentity() ) {
			return $this->redirect()->toRoute($this->successRedirect);
		}

		$view = $this->view;
		$form = $this->_getForm();

		$view->form = $form;
		$view->messages = $this->flashMessenger()->getMessages();

		return $view;
	}

	/**
	 * Login
	 */
	public function fakeloginAction()
	{
		$request = $this->getRequest();
		$authService = $this->getAuthService();
		$storage = $authService->getStorage();

		/* if already login, redirect to success page */
		if ( $authService->hasIdentity() ) {
			return $this->redirect()->toRoute($this->successRedirect);
		}

		$view = $this->view;
		$form = new \Application\Form\AuthForm();
		$url = $this->url()->fromRoute('auth/fakelogin');
		$form->setAttribute('action', $url);

		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				/* check authentication... */
				$authService->getAdapter()
					->setIdentity($request->getPost('username'))
					->setCredential($request->getPost('password'));
				$result = $authService->authenticate();

				$messages = $result->getMessages();
				$this->errorStack()->warning(array_pop($messages));

				if ( $result->isValid() ) {
					/* Check if it has rememberMe : */
					if ( $request->getPost('rememberme') == 1 ) {
						$storage->setRememberMe(1);
					}

					$storage->write($result->getIdentity());

					/* Init current user, Load session */
					$rbSm = Ranchbe::get()->getServiceManager();
					$rbSm->getAuthService();
					$currentUser = $rbSm->currentUserService();

					if ( $currentUser->authFrom == 'ldap' ) {
						try {
							$factory = DaoFactory::get();

							/* Save ldap user in db */
							$id = $factory->getDao($currentUser->cid)->getIdFromUid($currentUser->getUid());
							$currentUser->setId($id);
							$factory->getDao($currentUser->cid)->save($currentUser);

							/* Roles mapping */
							(isset($currentUser->memberof)) ? $memberof = $currentUser->memberof : $memberof = array();
							if ( count($memberof) > 0 ) {
								$urDao = $factory->getDao(\Acl\Model\UserRole::$classId);
								$roleDao = $factory->getDao(\Acl\Model\Role::$classId);
								$roleMap = $authService->getAdapter()->getOptions()['rolemap'];
								foreach( $memberof as $ldapRole ) {
									if ( isset($roleMap[$ldapRole]) ) {
										$roleName = $roleMap[$ldapRole];
										$roleId = $roleDao->getIdFromUid($roleName);
										try {
											$urDao->assign($currentUser->getId(), $roleId, '\app');
										}
										catch( \PDOException $e ) {
											/* Nothing */
										}
									}
								}
							}
						}
						catch( \Rbplm\Dao\NotExistingException $e ) {
							DaoFactory::get()->getDao($currentUser->cid)->save($currentUser);
						}
					}
					elseif ( $currentUser->authFrom == 'db' ) {
						DaoFactory::get()->getDao($currentUser::$classId)->save($currentUser);
					}
				}
			}
		}

		$view->form = $form;
		$view->messages = $this->flashmessenger()->getMessages();
		return $view;
	}

	/**
	 * Authenticate
	 */
	public function authenticateAction()
	{
		$form = $this->_getForm();
		$redirect = 'auth/login';

		$request = $this->getRequest();
		$sessionStorage = Ranchbe::get()->getServiceManager()
			->getSessionManager()
			->getStorage();

		/* set before load of user session */
		$previousRequestUri = $sessionStorage->offsetGet('previousRequestUri');
		$layout = $request->getPost('layout', null);
		$appLayout = $request->getPost('applayout', null);
		if ( $appLayout ) {
			$this->layoutSelector()->setApplicationLayout($appLayout);
		}

		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$authWebService = new \Service\Controller\Application\AuthService();
				$serviceReturn = $authWebService->authenticateService($request);

				foreach( $serviceReturn->getFeedbacks() as $feedback ) {
					$this->errorStack()->warning($feedback->getMessage());
				}
				if ( $serviceReturn->getHttpErrorCode() == 401 ) {
					$this->getResponse()->setStatusCode(401);
					return $this->forward()->dispatch('Application\Controller\Auth', [
						'action' => 'login'
					]);
				}

				$redirect = $this->successRedirect;

				if ( $previousRequestUri ) {
					$sessionStorage->offsetSet('previousRequestUri', null);
					return $this->redirect()->toUrl($previousRequestUri);
				}
				else {
					if ( $layout ) {
						$query = [
							'layout' => $layout
						];
					}
					else {
						$query = [];
					}

					return $this->redirect()->toRoute($redirect, [], [
						'query' => $query
					]);
				}
			}
		}
	}

	/**
	 */
	public function logoutAction()
	{
		/* Trash all records in session */
		$this->authservice->clearIdentity();
		$this->errorStack()->feedback("You've been logged out");
		session_destroy();
		return $this->redirect()->toRoute('auth/login');
	}
}
