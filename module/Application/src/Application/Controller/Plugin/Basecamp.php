<?php
namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Basecamp save page. At end of actions that implements basecamp call, page recorded as basecamp
 * will be redisplay in place of default forward.
 *
 * How to use :
 * In controller, set the var ifSuccessForward and ifFailedForward.
 * In page controller to return call :
 * basecamp::save($this->defaultSuccessForward, $this->defaultFailedForward, $view);
 * The property default* are setted to squize previous setted basecamp on this page.
 * In action page, call :
 * basecamp::setForward($this=[The Action Controller]);
 *
 * Callback pages are stored in Session ['navigation']['from']['if*Forward'] keys.
 * In view parameter "basecamp" is setted.
 *
 * To cancel basecamp, simply call
 * Basecamp::clear();
 *
 */
class Basecamp extends AbstractPlugin
{

	/**
	 *
	 * @var string
	 */
	const SESSION_KEY = 'basecamp';

	/**
	 *
	 * @var \Zend\Session\Storage\StorageInterface
	 */
	private $sessionStorage;

	/**
	 *
	 * @var array
	 */
	private $datas = [];

	/**
	 *
	 * @param \Zend\Session\Storage\StorageInterface $sessionStorage
	 * @return void
	 */
	public function __construct(\Zend\Session\Storage\StorageInterface $sessionStorage)
	{
		$this->sessionStorage = $sessionStorage;
		$this->datas = [
			'fromIfSuccessForward' => null,
			'fromIfFailedForward' => null,
			'basecamp' => 'default'
		];

		if ( $sessionStorage->offsetExists(self::SESSION_KEY) && is_array($sessionStorage->offsetGet(self::SESSION_KEY)) ) {
			$this->datas = $sessionStorage->offsetGet(self::SESSION_KEY);
		}
		else {
			$this->datas = [];
		}
	}

	/**
	 * @return \Zend\Session\Storage\StorageInterface $sessionStorage
	 *
	 */
	public function getSessionStorage()
	{
		return $this->sessionStorage;
	}

	/**
	 *
	 */
	public function __destruct()
	{
		$this->sessionStorage->offsetSet(self::SESSION_KEY, $this->getDatas());
	}

	/**
	 *
	 * @return self
	 */
	private function setData($name, $value)
	{
		$this->datas[$name] = $value;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	private function getDatas()
	{
		return $this->datas;
	}

	/**
	 *
	 * @return mixed
	 */
	private function getData($name)
	{
		if ( isset($this->datas[$name]) ) {
			return $this->datas[$name];
		}
		else {
			return null;
		}
	}

	/**
	 * @return Basecamp
	 */
	public function setForward()
	{
		$controller = $this->getController();
		$request = $controller->getRequest();
		$basecamp = $request->getPost('basecamp', $request->getQuery('basecamp', null));

		if ( $basecamp ) {
			$controller->view->basecamp = $basecamp;
			if ( $basecamp == 'default' ) {
				$this->setData('basecamp', $basecamp);
				$basecamp = null;
			}
			else if ( $basecamp ) {
				$controller->ifSuccessForward = urldecode($basecamp);
			}
			$this->setData('basecamp', $basecamp);
		}
		elseif ( $this->getData('basecamp') ) {
			$basecamp = $this->getData('basecamp');
			if ( $basecamp != '' ) {
				$controller->ifSuccessForward = urldecode($basecamp);
			}
		}
		elseif ( $this->getData('fromIfSuccessForward') ) {
			$controller->ifSuccessForward = $this->getData('fromIfSuccessForward');
			$controller->ifFailedForward = $this->getData('fromIfFailedForward');
		}

		$this->controller = $controller;
		return $this;
	}

	/**
	 * Save current page as basecamp
	 *
	 * @param string $ifSuccessForward
	 * @param string $ifFailedForward
	 * @param \Zend\View\Model\ViewModel $view
	 * @return Basecamp
	 */
	public function save($ifSuccessForward, $ifFailedForward, $view = null)
	{
		$this->datas = [
			'fromIfSuccessForward' => $ifSuccessForward,
			'fromIfFailedForward' => $ifFailedForward,
			'basecamp' => $ifSuccessForward
		];

		if ( $view ) {
			$view->basecamp = urlencode($ifSuccessForward);
		}

		$this->sessionStorage->offsetSet(self::SESSION_KEY, $this->getDatas());
		return $this;
	}

	/**
	 * @return Basecamp
	 *
	 */
	public function clear()
	{
		$this->sessionStorage->clear(self::SESSION_KEY);
		$this->datas = [
			'fromIfSuccessForward' => null,
			'fromIfFailedForward' => null,
			'basecamp' => 'default'
		];
		$this->sessionStorage->offsetSet(self::SESSION_KEY, $this->getDatas());
		return $this;
	}
}
