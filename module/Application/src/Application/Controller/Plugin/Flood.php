<?php
namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Exception;

/**
 *
 *
 */
class Flood extends AbstractPlugin
{

	/**
	 *
	 * @var string
	 */
	const SESSION_KEY = 'antiflood';

	/**
	 *
	 * @var \Zend\Session\Storage\StorageInterface
	 */
	private $sessionStorage;

	/**
	 *
	 * @var array
	 */
	private $datas = [];

	/**
	 * @var array
	 */
	private $config = [];

	/**
	 *
	 * @var string
	 */
	protected $ticket = '';

	/**
	 *
	 * @var array
	 */
	protected $ticketStack = [];

	/**
	 * True to keep validity after supression of session ticket
	 * @var boolean
	 */
	protected $isValid = false;

	/**
	 * 
	 * Config is array with keys :
	 * flood.check
	 * flood.timeinterval
	 * flood.maxrequest
	 * flood.wait
	 *
	 * @param \Zend\Session\Storage\StorageInterface $sessionStorage
	 * @param array $config
	 * @return void
	 */
	public function __construct(\Zend\Session\Storage\StorageInterface $sessionStorage, array $config)
	{
		$this->sessionStorage = $sessionStorage;

		if ( $sessionStorage->offsetExists(self::SESSION_KEY) && is_array($sessionStorage->offsetGet(self::SESSION_KEY)) ) {
			$this->datas = $sessionStorage->offsetGet(self::SESSION_KEY);
		}
		else {
			$this->datas = [
				'timeTracker' => '',
				'floodTracker' => '',
				'ticketStack' => ''
			];
		}

		$this->config = $config;
	}

	/**
	 *
	 */
	public function __destruct()
	{
		$this->sessionStorage->offsetSet(self::SESSION_KEY, $this->getDatas());
	}

	/**
	 *
	 * @return array
	 */
	private function getDatas()
	{
		return $this->datas;
	}

	/**
	 *
	 * @return array
	 */
	private function getData($name)
	{
		if ( isset($this->datas[$name]) ) {
			return $this->datas[$name];
		}
		else {
			return null;
		}
	}

	/**
	 * Anti-Flood V3. Condition existance du ticket en session. Tous les tickets
	 * sont conservés en session jusqu'a leur utilisation.
	 * Inspired by principle described in
	 *   http://www.developpez.net/forums/archive/index.php/t-104783.html
	 * If session jeton is egal to jeton record in form, generate a new jeton.
	 * session_register('jeton');
	 *
	 * @param string
	 * @return boolean
	 *
	 */
	public function checkFlood($ticket)
	{
		if ( $this->isValid ) {
			return true;
		}

		if ( $this->config['flood.check'] == false ) {
			return true;
		}

		$ticketStack = & $this->ticketStack;

		//Return array key of the ticket or false if not a valid ticket
		$ticketKey = array_search($ticket, $ticketStack);

		if ( $ticketKey ) {
			unset($ticketStack[$ticketKey]); // Suppress ticket in session
			$this->setTicket(); // generate a new ticket
			$this->isValid = true; //keep validity
			return true;
		}
		else {
			$this->setTicket(); // generate a new ticket
			return false;
		}
	}

	/**
	 * Check time interval between 2 requests
	 *
	 * @return boolean
	 */
	public function checkTime()
	{
		$timeTracker = $this->getData('timeTracker');
		$floodTracker = $this->getData('floodTracker');

		/* init time tracker */
		if ( !$timeTracker ) {
			$timeTracker = time();
			return true;
		}

		/* init Flood tracker */
		if ( !$floodTracker ) {
			$floodTracker = 1;
			return true;
		}

		$timeInterval = $this->config['flood.timeinterval'];
		$maxRequest = $this->config['flood.maxrequest'];
		$waitTime = $this->config['flood.wait'];

		/* If the page is recall after time < to one second */
		if ( (time() - $timeTracker) < $timeInterval ) {
			$floodTracker++;
			if ( $floodTracker > $maxRequest ) {
				$msg = "You can not doing $maxRequest requests in $timeInterval seconds.Now, wait $waitTime seconds and refresh this page...";
				$floodTracker = time() + $waitTime;
				$this->setData('floodTracker', $floodTracker);
				throw new Exception($msg);
			}
			$this->setData('floodTracker', $floodTracker);
		}
		else {
			$this->setData('timeTracker', time());
			$this->setData('floodTracker', 1);
			return true;
		}
	}

	/** Generate the ticket. If $string is set, generate the ticket from this string
	 *  md5 encode, else, generate a unpredictible random string
	 *
	 * @param String String to encode with md5
	 * @return String Ticket
	 *
	 */
	public function setTicket($string = '')
	{
		$ticketStack = $this->getData('ticketStack');
		if ( !$ticketStack ) {
			$ticketStack = [];
		}

		/* generate a random string if none string */
		if ( !$string ) {
			$this->ticket = md5(uniqid(mt_rand(), true));
		}
		else {
			$this->ticket = md5($string);
			/* check if ticket is yet in stack */
			if ( array_search($this->ticket, $ticketStack) !== false ) {
				return $this->ticket;
			}
		}

		/* add ticket to stack begining */
		array_unshift($ticketStack, $this->ticket);

		/* clean older ticket */
		$maxsize = $this->config['flood.maxsize'];
		if ( count($ticketStack) > $maxsize ) {
			array_pop($ticketStack);
		}

		$this->setData('ticketStack', $ticketStack);
		return $this->ticket;
	}

	/** Return ticket for current request
	 *
	 * @return string
	 */
	public function getTicket()
	{
		return $this->ticket;
	}
}
