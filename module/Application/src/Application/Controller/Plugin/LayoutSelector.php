<?php
namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Set the layout from request layout parameter or from saved layout in sesssion.
 * Layout is save in session with key $pageId, wich must setted in controller.
 * So, a layout must be defined by controller.
 *
 */
class LayoutSelector extends AbstractPlugin
{

	/**
	 *
	 * @var string
	 */
	const SESSION_KEY = 'layoutselector';

	/**
	 *
	 * @var \Zend\Session\Storage\StorageInterface
	 */
	private $sessionStorage;

	/**
	 *
	 * @var array
	 */
	private $datas = [];

	/**
	 *
	 * @param \Zend\Session\Storage\StorageInterface $sessionStorage
	 * @return void
	 */
	public function __construct(\Zend\Session\Storage\StorageInterface $sessionStorage)
	{
		$this->sessionStorage = $sessionStorage;
		$this->datas = [];

		if ( $sessionStorage->offsetExists(self::SESSION_KEY) && is_array($sessionStorage->offsetGet(self::SESSION_KEY)) ) {
			$this->datas = $sessionStorage->offsetGet(self::SESSION_KEY);
		}
		else {
			$this->datas = [];
		}
	}

	/**
	 *
	 */
	public function __destruct()
	{
		$this->save();
	}

	/**
	 *
	 * @return void
	 */
	private function getDatas()
	{
		return $this->datas;
	}

	/**
	 *
	 * @return array
	 */
	private function getData($name)
	{
		if ( isset($this->datas[$name]) ) {
			return $this->datas[$name];
		}
		else {
			return null;
		}
	}

	/**
	 * Set a alternatif layout for application
	 * 
	 * @param string $layout
	 */
	public function setApplicationLayout($layout)
	{
		$this->session->application = $layout;
		return $this;
	}
	
	/**
	 * 
	 */
	public function getLayoutForModule()
	{
		$controller = $this->getController();
		$controllerClass = get_class($controller);
		$moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
		
		/* @var \Zend\Mvc\Controller\AbstractActionController $controller */
		$config = $controller->getEvent()->getApplication()->getServiceManager()->get('Configuration');
		$layoutsConfig = $config['view_manager']['template_map']['module_layouts'];
		
		if ( isset($layoutsConfig[$moduleNamespace]) ) {
			return $layoutsConfig[$moduleNamespace];
		}
		else {
			return $layoutsConfig['Ranchbe'];
		}
	}

	/**
	 * @param \Application\Controller\AbstractController $controller
	 * @return LayoutSelector
	 *
	 */
	public function setLayout()
	{
		$controller = $this->getController();
		$pageId = $controller->pageId;
		$request = $controller->getRequest();
		
		/**/
		$layout = $this->getData($pageId);
		if ( !$layout ) {
			$layout = $this->getData('application');
		}
		
		$layout = $request->getPost('layout', $request->getQuery('layout', $layout));

		/**/
		if ( $layout == 'popup' ) {
			$controller->layout('layout/popup');
		}
		elseif ( $layout == 'fragment' ) {
			$controller->layout('layout/fragment');
		}
		elseif ( $layout == 'dialog' ) {
			$controller->layout('layout/dialog');
		}
		elseif ( $layout == 'viewer' ) {
			$controller->layout('layout/viewer');
		}
		elseif ( $layout == 'ajaxload' ) {
			$controller->layout('layout/ajaxload');
		}
		else{
			$controller->layout($this->getLayoutForModule());
		}
		
		if ( $controller->view ) {
			$controller->view->layout = $layout;
		}

		$this->layout = $layout;
		$this->pageId = $pageId;
		$this->controller = $controller;
		return $this;
	}

	/**
	 * @return LayoutSelector
	 *
	 */
	public function clear($controller = null)
	{
		if ( is_null($controller) ) {
			$this->sessionStorage->clear(self::SESSION_KEY);
		}
		else {
			$pageId = $controller->pageId;
			unset($this->datas[$pageId]);
			$this->save();
		}
		return $this;
	}

	/**
	 * @return LayoutSelector
	 *
	 */
	public function save()
	{
		$this->sessionStorage->offsetSet(self::SESSION_KEY, $this->getDatas());
		return $this;
	}
}
