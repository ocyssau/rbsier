<?php
namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 *
 *
 */
class ColumnSelector extends AbstractPlugin
{

	/**
	 * Array of selected columns
	 *
	 * @var array
	 */
	protected $selected;

	/**
	 * Array of available columns
	 *
	 * @var array
	 */
	protected $availables;

	/**
	 * 
	 * @var \Zend\Session\Container
	 */
	private $sessionContainer;

	/**
	 *
	 * @var string
	 */
	private $namespace;

	/**
	 * @param \Zend\Session\Container $sessionContainer
	 */
	public function __construct(\Zend\Session\Container $sessionContainer, $namespace)
	{
		$this->sessionContainer = $sessionContainer;
		$this->namespace = $namespace;
		$this->selected = [];
	}

	/**
	 * @param array $availableColumns Name of columns candidates to be show 
	 * @param array $requestedColumns Name of columns requested by user tu be show
	 * @return \Application\Controller\Plugin\ColumnSelector
	 */
	public function load($availableColumns, $requestedColumns)
	{
		$this->availables = $availableColumns;

		/* save */
		if ( $requestedColumns ) {
			$this->sessionContainer->offsetSet($this->namespace, $requestedColumns);
		}
		/* load */
		else {
			if ( $this->sessionContainer->offsetExists($this->namespace) ) {
				$data1 = $this->sessionContainer->offsetGet($this->namespace);
			}
			else {
				$data1 = $availableColumns;
			}
			$this->selected = $data1;
		}
		
		return $this;
	}

	/**
	 * 
	 * @return \Application\Controller\Plugin\ColumnSelector
	 */
	public function clean()
	{
		$this->sessionContainer->offsetUnset($this->namespace);
		return $this;
	}

	/**
	 * @return array
	 */
	public function getSelected()
	{
		return $this->selected;
	}

	/**
	 * @return array
	 */
	public function getAvailables()
	{
		return $this->availables;
	}
} /* End of class */
