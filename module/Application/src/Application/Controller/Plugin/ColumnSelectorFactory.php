<?php
namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 *
 *
 */
class ColumnSelectorFactory extends AbstractPlugin
{
	
	const SESSION_KEY = 'columnSelector';
	
	/**
	 * 
	 * @var \Zend\Session\Container
	 */
	private $sessionContainer;
	
	/**
	 * 
	 * @var array
	 */
	private $registry = [];

	/**
	 * 
	 * @param \Zend\Session\ManagerInterface $sessionManager
	 */
	public function __construct(\Zend\Session\ManagerInterface $sessionManager)
	{
		/* @var \Ranchbe\Service\SessionContainerRegistry $sessionManager->containerRegistry */
		$this->sessionContainer = $sessionManager->containerRegistry->get(self::SESSION_KEY);
	}
	
	/**
	 * 
	 * @return \Application\Controller\Plugin\ColumnSelector
	 */
	public function __invoke()
	{
		$namespace = $this->getController()->pageId;
		if( ! isset($this->registry[$namespace])){
			$columnSelector = new ColumnSelector($this->sessionContainer, $namespace);
			$this->registry[$namespace] = $columnSelector;
		}
		return $this->registry[$namespace];
	}
} /* End of class */
