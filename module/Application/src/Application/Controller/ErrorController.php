<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\View\ViewModel;

/**
 *
 *
 */
class ErrorController extends AbstractActionController
{

	/**
	 *
	 */
	public function notauthorizedAction()
	{
		$view = new ViewModel(\Ranchbe::get());
		$response = $this->getResponse();
		$response->setStatusCode($response::STATUS_CODE_403);

		$rightName = $this->params()->fromRoute('rightname', 'undefined');
		$resourceCn = $this->params()->fromRoute('resourcecn', 'undefined');

		$view->setTemplate('error/403');
		$view->message = "You are not authorized to execute this request. May be its time to presents a coffee to your administrator.";
		$view->rightName = $rightName;
		$view->resourceCn = base64_decode($resourceCn);
		$this->layout()->setTemplate('layout/error');
		$view->setTerminal(false);
		return $view;
	}
}
