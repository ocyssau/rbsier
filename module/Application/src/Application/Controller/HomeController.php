<?php
namespace Application\Controller;

use Rbs\Space\Factory as DaoFactory;

/**
 *
 *
 */
class HomeController extends AbstractController
{

	/** @var string */
	public $pageId = 'application_home';

	/** @var string */
	public $defaultSuccessForward = '/home';

	/** @var string */
	public $defaultFailedForward = '/home';

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function init()
	{
		parent::init();
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('home')->activate();
		$this->layout()->tabs = $tabs;
		$this->factory = DaoFactory::get();
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$view = $this->view;
		return $view;
	}

	/**
	 *
	 */
	public function splashAction()
	{
		$view = $this->view;
		return $view;
	}
}
