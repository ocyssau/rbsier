<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractConsoleController;
use Zend\Stdlib\RequestInterface;
use Zend\Stdlib\ResponseInterface;
use Ranchbe;

/**
 *
 * @author olivier
 *
 */
abstract class AbstractCliController extends AbstractConsoleController
{

	/**
	 *
	 */
	protected $application;

	/**
	 *
	 * @var \Rbs\Auth\Storage
	 */
	protected $storage;

	/**
	 *
	 * @var \Zend\Authentication\Adapter\AdapterInterface
	 */
	protected $authservice;

	/**
	 *
	 * @var \Application\Helper\ErrorStackInterface
	 */
	protected $errorStack;

	/**
	 *
	 * @var \Acl\Service\Acl
	 */
	protected $acl;

	/**
	 *
	 * @var string
	 */
	public $resourceCn;

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractConsoleController::dispatch()
	 */
	public function dispatch(RequestInterface $request, ResponseInterface $respons = null)
	{
		$this->init();
		parent::dispatch($request, $respons);
	}

	/**
	 *
	 * @return \Application\Controller\AbstractController
	 */
	abstract public function init();

	/**
	 *
	 * @throws \Exception
	 */
	public function notauthorizedAction()
	{
		throw new \Exception('Not authorized');
	}

	/**
	 * @return AbstractCliController
	 *
	 */
	protected function authIdentityCheck($request)
	{
		$authservice = $this->getAuthService();
		if ( !$authservice->hasIdentity() ) {
			die("none auth");
		}

		return $this;
	}

	/**
	 *
	 * @param \Zend\Console\Request $request
	 */
	protected function authenticate($request)
	{
		$authservice = $this->getAuthService();
		$storage = $authservice->getStorage();

		$username = $request->getParam('user');
		$password = $request->getParam('pwd');

		$authservice->getAdapter()
			->setIdentity($username)
			->setCredential($password);
		$result = $authservice->authenticate();

		if ( $result->getCode() != 1 ) {
			$message = implode(' ', $result->getMessages());
			throw new \Exception($message);
		}

		$storage->write($result->getIdentity());

		/* Init current user, Load session */
		$sm = Ranchbe::get()->getServiceManager();
		$currentUser = $sm->currentUserService();
		$currentUser->setLastLogin(new \DateTime());
	}

	/**
	 * 
	 * @return \Acl\Service\Acl
	 */
	public function getAcl()
	{
		if ( !isset($this->acl) ) {
			$this->acl = new \Acl\Service\Acl($this);
			$this->acl->setBaseCn($this->resourceCn);
		}
		return $this->acl;
	}

	/**
	 * Get the authservice.
	 * Use factory define in Module::getServiceConfig()
	 *
	 * @return \Zend\Authentication\AuthenticationService
	 */
	public function getAuthService()
	{
		if ( !$this->authservice ) {
			$this->authservice = Ranchbe::get()->getServiceManager()->getAuthservice();
		}
		return $this->authservice;
	}
}
