<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\View\ViewModel;
use Rbs\EventDispatcher\EventManager;
use Ranchbe;

/**
 *
 *	To get application instannce :
 *
 * 			$application = $this->getEvent()->getApplication();
 * 
 * Application is too composed with Ranchbe instance :
 * 			$application = Ranchbe::get()->getMvcApplication()
 *
 */
abstract class AbstractController extends AbstractActionController
{

	/**
	 *
	 */
	protected $application;

	/**
	 *
	 * @var \Zend\Authentication\AuthenticationService
	 */
	protected $authservice;

	/**
	 *
	 * @var boolean
	 */
	public $isService = false;

	/**
	 *
	 * @var boolean
	 */
	protected $isAjax = false;

	/**
	 *
	 * @var \Application\Helper\ErrorStackInterface
	 */
	protected $errorStack;

	/**
	 *
	 * @var \Acl\Service\Acl
	 */
	protected $acl;

	/**
	 *
	 * @var ViewModel
	 */
	public $view;

	/**
	 *
	 * @var string
	 */
	public $pageId;

	/**
	 *
	 * @var string
	 */
	public $resourceCn;

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward;

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward;

	/**
	 *
	 * @var string
	 */
	public $ifSuccessForward;

	/**
	 *
	 * @var string
	 */
	public $ifFailedForward;

	/**
	 *
	 * @var EventManager
	 */
	protected $rbEventManager;

	/**
	 *
	 * @var \Ranchbe\ServiceManager
	 */
	protected $rbServiceManager;

	/**
	 *
	 * @return \Application\Controller\AbstractController
	 */
	public function init()
	{
		$this->isService = false;
		$this->isAjax = false;

		$this->maintenancemodeCheck();
		$this->authIdentityCheck();
		
		$this->errorStack = $this->getRbServiceManager()->getErrorStack();

		$this->view = new ViewModel(Ranchbe::get());
		$this->view->isService = $this->isService;
		$this->view->isAjax = $this->isAjax;

		$request = $this->getRequest();
		$this->pageId = $request->getQuery('pageid', $request->getPost('pageid', $this->pageId));
		$this->view->pageid = $this->pageId;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;

		$this->layoutSelector()
			->setLayout($this)
			->save();

		return $this;
	}

	/**
	 * Called by dispatch if is not a AJAX request.
	 * init isService, isAjax property and set to view.
	 * if*Forward properties are not setted.
	 *
	 * If request have a property layout=none, setTerminal(true) to dont display layout.
	 * Else, layout may have value popup, fragment, inline, iframe, ranchbe, none
	 *
	 */
	public function initAjax()
	{
		$this->isService = true;
		$this->isAjax = true;

		$this->maintenancemodeCheck();
		$this->authIdentityCheck();

		$this->errorStack = $this->getRbServiceManager()->getErrorStack();

		$this->view = new ViewModel(Ranchbe::get());
		$this->view->isService = $this->isService;
		$this->view->isAjax = $this->isAjax;
		$this->view->setTerminal(true);
		$this->view->layout = '';

		return $this;
	}

	/**
	 *
	 */
	public function authIdentityCheck()
	{
		if ( !$this->getAuthservice()->hasIdentity() ) {
			$sessionStorage = $this->getRbServiceManager()
				->getSessionManager()
				->getStorage();
			$sessionStorage->offsetSet('previousRequestUri', $this->getEvent()
				->getRouter()
				->getRequestUri()
				->__toString());
			
			$fwd = $this->url()->fromRoute('auth/login');
			$this->redirectTo($fwd);
		}
		else {
			/* init current user */
			$this->getEvent()->getApplication()->ranchbe->getServiceManager()->currentUserService();
		}

		return $this;
	}

	/**
	 *
	 */
	public function maintenancemodeCheck()
	{
		if ( is_file('data/cache/maintenance.mode') ) {
			$this->forward()->dispatch('Admin/Controller/Maintenance', [
				'action' => 'index'
			]);
			return $this->redirect()->toRoute('admin-maintenance-message', []);
		}
		return $this;
	}

	/**
	 * Put ServiceRespons data to FlashMessenger.
	 *
	 * @param \Service\Controller\ServiceRespons $respons
	 * @return AbstractActionController
	 */
	protected function bindServiceRespons(\Service\Controller\ServiceRespons $respons)
	{
		$debug = DEBUG;

		$errorStack = $this->errorStack();

		if ( $respons->hasErrors() ) {
			/** @var \Exception $exception */
			foreach( $respons->getErrors() as $exception ) {
				$data = $exception->getData();
				if ( $data ) {
					$name = $data->getName();
				}
				$message = $exception->getMessage();
				$message .= ' DATA NAME : ' . $name;
				if ( $debug ) {
					$trace = $exception->getTraceAsString();
					$message .= '<pre>' . $trace . '</pre>';
				}

				$errorStack->error($message);

				if ( $debug ) {
					$previous = $exception;
					while( $previous = $previous->getPrevious() ) {
						$message = $previous->getMessage();
						$message .= '<pre>' . $previous->getTraceAsString() . '</pre>';
						$errorStack->error($message);
					}
				}
			}
		}

		if ( $respons->hasFeedbacks() ) {
			foreach( $respons->getFeedbacks() as $exception ) {
				$message = $exception->getMessage();
				$data = $exception->getData();
				if ( $data ) {
					$name = $data->getName();
					$message .= ' DATA NAME : ' . $name;
				}
				$errorStack->feedback($message);
			}
		}

		return $this;
	}

	/**
	 *
	 * @param array $return
	 */
	protected function serviceReturn($return = [])
	{
		http_response_code(200);
		echo json_encode($return);
		die();
	}

	/**
	 *
	 * @return EventManager
	 */
	protected function getRbEventManager()
	{
		if ( !isset($this->rbEventManager) ) {
			$this->rbEventManager = $this->getEvent()->getApplication()->ranchbe->getShareEventManager();
		}
		return $this->rbEventManager;
	}

	/**
	 *
	 * @return \Ranchbe\ServiceManager
	 */
	protected function getRbServiceManager()
	{
		if ( !isset($this->rbServiceManager) ) {
			$this->rbServiceManager = $this->getEvent()->getApplication()->ranchbe->getServiceManager();
			$this->rbServiceManager->isService($this->isService);
		}
		return $this->rbServiceManager;
	}

	/**
	 *
	 * @param
	 *        	\Exception
	 * @return string json
	 */
	public function exceptionToJson($e)
	{
		if ( DEBUG ) {
			$return = [
				'error' => [
					'message' => $e->getMessage(),
					'code' => $e->getCode(),
					'file' => $e->getFile(),
					'line' => $e->getLine()
				]
			];
		}
		else {
			$return = [
				'error' => [
					'message' => $e->getMessage()
				]
			];
		}

		echo json_encode($return);
	}

	/**
	 *
	 * @return \Acl\Service\Acl
	 */
	public function getAcl()
	{
		if ( !isset($this->acl) ) {
			$this->acl = new \Acl\Service\Acl($this);
			$this->acl->setBaseCn($this->resourceCn);
		}
		return $this->acl;
	}

	/**
	 * Get the authservice.
	 * Use factory define in Module::getServiceConfig()
	 *
	 * @return \Zend\Authentication\AuthenticationService
	 */
	public function getAuthService()
	{
		if ( !isset($this->authservice) ) {
			$this->authservice = $this->getRbServiceManager()->getAuthservice();
		}
		return $this->authservice;
	}

	/**
	 *
	 * @return \Application\Helper\ErrorStack
	 */
	public function errorStack()
	{
		return $this->errorStack;
	}

	/**
	 *
	 * @return \Zend\Session\Storage\ArrayStorage
	 */
	public function session()
	{
		if ( !isset($this->session) ) {
			$this->session = \Ranchbe::get()->getServiceManager()
				->getSessionManager()
				->getStorage();
		}
		return $this->session;
	}

	/**
	 *
	 * @param string $fwd
	 * @param array $params
	 */
	public function redirectTo($fwd, $params = null)
	{
		$httpEnv = new \Zend\Http\PhpEnvironment\Request();
		$baseUrl = $httpEnv->getBasePath();
		
		$fwd = strtolower(trim($fwd, '/'));
		$baseUrl = strtolower(trim($baseUrl, '/'));

		if ( substr($fwd, 0, strlen($baseUrl)) != $baseUrl ) {
			$location = '/' . $baseUrl . '/' . $fwd;
		}
		else {
			$location = '/' . $fwd;
		}

		if ( is_array($params) ) {

			$queryStr = http_build_query($params);
			$t = strpos($fwd, '?');

			if ( $t === false ) {
				$location = $location . '?' . $queryStr;
			}
			else {
				$location = $location . '&' . $queryStr;
			}
		}

		/* @todo: implements Zend redirect
		 * return $this->redirect()->toRoute('route-name', ['action'=>'...', 'controller'=>'...'], ['query'=>['param1'=>'value1']]);
		 */

		session_write_close();
		header("Location: $location");
		die();
	}

	/**
	 *
	 * @param array $params
	 */
	public function errorForward($params = null)
	{
		if ( $this->isService ) {
			return $this->successForward($params);
		}
		else {
			$this->errorStack()
				->getFlash()
				->save();
			return $this->redirectTo($this->ifFailedForward, $params);
		}
	}

	/**
	 *
	 * @param array @params
	 */
	public function successForward($params = null)
	{
		if ( $this->isService ) {
			$return = $this->errorStack();
			if ( $return->hasErrors() ) {
				http_response_code(500);
				echo json_encode($return);
			}
			else {
				echo json_encode($return);
			}
			die();
		}
		else {
			return $this->redirectTo($this->ifSuccessForward, $params);
		}
	}
}
