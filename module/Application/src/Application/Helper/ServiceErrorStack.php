<?php
//%LICENCE_HEADER%
namespace Application\Helper;

use Rbplm\Sys\Logger;

/**
 *
 * @author olivier
 *
 */
class ServiceErrorStack implements ErrorStackInterface
{

	/**
	 *
	 * @var ServiceErrorStack
	 */
	private static $instance;

	/**
	 *
	 * @var Logger
	 */
	protected $logger;

	/**
	 *
	 * @var array
	 */
	protected $errors;

	/**
	 *
	 * @var array
	 */
	protected $feedbacks;

	/**
	 *
	 * @var \Exception
	 */
	protected $exception;

	/**
	 *
	 */
	public function __construct(Logger $logger)
	{
		self::$instance = $this;
		
		$this->logger = $logger;
		
		$this->errors = [];
		$this->feedbacks = [];
		$this->exception = null;
	}

	/**
	 * Singleton method.
	 * @return ServiceErrorStack
	 */
	public static function get()
	{
		if ( self::$instance === null ) {
			self::$instance = new ServiceErrorStack();
		}
		return self::$instance;
	}

	/**
	 * @param string $message
	 * @param integer $priority must be an integer >= 0 and < 8
	 */
	public function log($message, $priority = 1)
	{
		if ( $this->logger ) {
			$this->logger->log($message, $priority);
		}
	}

	/**
	 * implements JsonSerializable
	 */
	public function jsonSerialize()
	{
		$ret = [
			'errors' => [],
			'feedbacks' => [],
			'exception' => null
		];
		
		foreach( $this->errors as $error ) {
			$ret['errors'][] = $error->jsonSerialize();
		}
		
		foreach( $this->feedbacks as $fb ) {
			$ret['feedbacks'][] = $fb->jsonSerialize();
		}
		
		$ret['exception'] = ($this->exception) ? self::exceptionToArray($this->exception, JSON_FORCE_OBJECT) : null;

		return $ret;
	}

	/**
	 * @param \Exception $e
	 * @return ServiceErrorStack
	 */
	private static function exceptionToArray(\Exception $e)
	{
		$return = array(
			'message' => $e->getMessage(),
			'code' => $e->getCode(),
			'file' => $e->getFile(),
			'line' => $e->getLine()
		);
		return $return;
	}

	/**
	 * @param \Exception $e
	 * @return ServiceErrorStack
	 */
	public function setException($e)
	{
		$this->exception = $e;
		return $this;
	}

	/**
	 *
	 * @return \Exception
	 */
	public function getException()
	{
		return $this->exception;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::error()
	 */
	public function error($message)
	{
		$this->log($message);
		$error = new \Service\Controller\ServiceError($message, null, null);
		$this->errors[] = $error;
		return $this;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::warning()
	 */
	public function warning($message)
	{
		$this->log($message);
		$error = new \Service\Controller\ServiceFeedback($message, null, null);
		$this->feedbacks[] = $error;
		return $this;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::feedback()
	 */
	public function feedback($message)
	{
		$this->log($message);
		$error = new \Service\Controller\ServiceFeedback($message, null, null);
		$this->feedbacks[] = $error;
		return $this;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::success()
	 */
	public function success($message)
	{
		$this->log($message);
		$error = new \Service\Controller\ServiceFeedback($message, null, null);
		$this->feedbacks[] = $error;
		return $this;
	}

	/**
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 */
	public function getWarning()
	{
		return $this->feedbacks;
	}

	/**
	 */
	public function getFeedbacks()
	{
		return $this->feedbacks;
	}

	/**
	 */
	public function getSuccess()
	{
		return $this->feedbacks;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::hasErrors()
	 */
	public function hasErrors()
	{
		return (count($this->errors));
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::hasFeedbacks()
	 */
	public function hasFeedbacks()
	{
		return (count($this->feedbacks));
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::hasMessages()
	 */
	public function hasMessages()
	{
		return (count($this->feedbacks));
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::hasWarnings()
	 */
	public function hasWarnings()
	{
		return (count($this->feedbacks));
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::hasSuccess()
	 */
	public function hasSuccess()
	{
		return (count($this->feedbacks));
	}
}
