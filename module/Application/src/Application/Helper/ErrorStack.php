<?php
//%LICENCE_HEADER%
namespace Application\Helper;

use Rbplm\Sys\Logger;
use \Zend\Mvc\Controller\Plugin\FlashMessenger as FlashMessenger;

/**
 *
 * @author olivier
 *
 */
class ErrorStack implements ErrorStackInterface
{

	/**
	 *
	 * @var FlashMessenger $flash
	 */
	protected $flash;

	/**
	 *
	 * @var Logger
	 */
	protected $logger;

	/**
	 *
	 * @var ErrorStack
	 */
	private static $instance;

	/**
	 *
	 */
	public function __construct(Logger $logger, FlashMessenger $flash)
	{
		self::$instance = $this;
		
		$this->logger = $logger;
		$this->flash = $flash;
	}

	/**
	 * Singleton method.
	 * @return ErrorStack
	 */
	public static function get()
	{
		if ( self::$instance === null ) {
			throw new \Exception(self::class . 'is not constructed');
		}
		return self::$instance;
	}

	/**
	 * implements JsonSerializable
	 */
	public function jsonSerialize()
	{
		$ret = array(
			'errors' => [],
			'warnings' => [],
			'feedbacks' => [],
			'success' => [],
			'exception' => null
		);
		
		foreach( $this->getErrors() as $msg ) {
			$ret['errors'][] = $msg;
		}
		
		foreach( $this->getWarning() as $msg ) {
			$ret['warnings'][] = $msg;
		}
		
		foreach( $this->getFeedbacks() as $msg ) {
			$ret['feedbacks'][] = $msg;
		}
		
		foreach( $this->getSuccess() as $msg ) {
			$ret['success'][] = $msg;
		}
		
		return $ret;
	}

	/**
	 * @param string $message
	 * @param integer $priority must be an integer >= 0 and < 8
	 */
	public function log($message, $priority = 1)
	{
		if ( $this->logger ) {
			$this->logger->log($message, $priority);
		}
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::error()
	 */
	public function error($message)
	{
		$this->log($message);
		$this->flash->addErrorMessage($message);
		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::warning()
	 *
	 */
	public function warning($message)
	{
		$this->log($message);
		$this->flash->addWarningMessage($message);
		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::feedback()
	 *
	 */
	public function feedback($message)
	{
		$this->log($message);
		$this->flash->addInfoMessage($message);
		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::success()
	 *
	 */
	public function success($message)
	{
		$this->log($message);
		$this->flash->addSuccessMessage($message);
		return $this;
	}

	/**
	 */
	public function getErrors()
	{
		return $this->flash->getErrorMessages();
	}

	/**
	 */
	public function getWarning()
	{
		return $this->flash->getWarningMessages();
	}

	/**
	 */
	public function getFeedbacks()
	{
		return array_merge($this->flash->getWarningMessages(), $this->flash->getSuccessMessages(), $this->flash->getMessages());
	}

	/**
	 */
	public function getSuccess()
	{
		return $this->flash->getSuccessMessages();
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::hasError()
	 *
	 */
	public function hasErrors()
	{
		return $this->flash->hasErrorMessages();
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::hasFeedbacks()
	 *
	 */
	public function hasFeedbacks()
	{
		return ($this->flash->hasMessages() || $this->flash->hasWarningMessages() || $this->flash->hasSuccessMessages());
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::hasMessages()
	 *
	 */
	public function hasMessages()
	{
		return $this->flash->hasMessages();
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::hasWarnings()
	 */
	public function hasWarnings()
	{
		return $this->flash->hasWarningMessages();
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Helper\ErrorStackInterface::hasSuccess()
	 *
	 */
	public function hasSuccess()
	{
		return $this->flash->hasSuccessMessages();
	}

	/**
	 *
	 */
	public function getFlash()
	{
		return $this->flash;
	}
}
