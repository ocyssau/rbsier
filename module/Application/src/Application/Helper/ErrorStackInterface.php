<?php
//%LICENCE_HEADER%
namespace Application\Helper;

/**
 *
 *
 */
interface ErrorStackInterface extends \JsonSerializable
{

	/**
	 * Log only the error
	 *
	 * @param string $message
	 * @return ErrorStackInterface
	 *
	 */
	public function log($message);

	/**
	 * Add a warning error.
	 *
	 * @param string $message
	 * @return ErrorStackInterface
	 *
	 */
	public function error($message);

	/**
	 * Add a warning message
	 *
	 * @param string $message
	 * @return ErrorStackInterface
	 */
	public function warning($message);

	/**
	 * Add a feedback message

	 * @param string $message
	 * @return ErrorStackInterface
	 */
	public function feedback($message);

	/**
	 * Add a success message
	 *
	 * @param string $message
	 * @return ErrorStackInterface
	 */
	public function success($message);

	/**
	 * Return the number of errors
	 * @return integer
	 *
	 */
	public function hasErrors();

	/**
	 * Return the number of feedbacks
	 * @return integer
	 *
	 */
	public function hasFeedbacks();

	/**
	 * Return the number of messages
	 * @return integer
	 *
	 */
	public function hasMessages();

	/**
	 * Return the number of warnings
	 * @return integer
	 *
	 */
	public function hasWarnings();

	/**
	 * Return the number of success messages
	 * @return integer
	 *
	 */
	public function hasSuccess();
}
