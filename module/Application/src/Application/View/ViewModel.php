<?php
namespace Application\View;

/**
 * @property string $charset
 * @property string $shortcutIcon
 * @property string $logo
 * @property string $allowUserPrefs
 * @property string $lang
 * @property string $baseurl
 * @property string $baseJsUrl
 * @property string $baseCssUrl
 * @property string $baseImgUrl
 * @property string $baseCustomImgUrl
 * @property string $doctypeIconBaseUrl
 * @property string $thumbnailBaseurl
 * @property string $iconsFileBaseurl
 * @property string $rbgateServerUrl
 * @property string $shortDateformat
 * @property string $longDateformat
 *
 */
class ViewModel extends \Zend\View\Model\ViewModel
{

	/**
	 * 
	 * @var \Ranchbe
	 */
	public $ranchbe;

	/**
	 * Constructor.
	 *
	 * @param array $config
	 *        	Configuration key-value pairs.
	 */
	public function __construct($ranchbe)
	{
		$this->ranchbe = $ranchbe;
		$ranchbe->setView($this);
		parent::__construct();
	}

	/**
	 * This method is deprecated. Use plugin basePath($str) instead.
	 * 
	 * @return string
	 * @deprecated
	 * @see \Zend\View\Helper\BasePath::__invoke($file = null)
	 */
	public function baseUrl($path)
	{
		$path = trim($path, '/');
		return $this->baseurl . '/' . $path;
	}
}
