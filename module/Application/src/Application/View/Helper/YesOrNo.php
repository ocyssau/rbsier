<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * 
 *
 */
class YesOrNo extends AbstractHelper
{

	/**
	 *
	 * @param integer $typeId
	 * @param boolean $withLabel
	 */
	public function __invoke($bool)
	{
		($bool == 1) ? $e = tra('Yes') : $e = tra('No');
		return $e;
	}
}
