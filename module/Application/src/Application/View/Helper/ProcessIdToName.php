<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * 
 */
class ProcessIdToName extends AbstractHelper
{

	public function __invoke($id)
	{
		$cacheUsualName = & \Ranchbe::$registry;
		if ( isset($cacheUsualName['process'][$id]) ) {
			return $cacheUsualName['process'][$id];
		}

		$table = 'wf_process';

		$conn = \Rbplm\Dao\Connexion::get();
		$sql = "SELECT normalizedName FROM $table WHERE id = '$id'";
		$stmt = $conn->query($sql);
		$ret = $stmt->fetchColumn(0);

		if ( !$ret ) {
			$ret = tra('undefined');
		}

		$cacheUsualName['process'][$id] = $ret;
		return $ret;
	}
}
