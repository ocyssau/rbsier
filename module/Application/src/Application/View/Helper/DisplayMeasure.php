<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class DisplayMeasure extends AbstractHelper
{

	/**/
	public function __invoke($value, $measure)
	{
		switch ($measure) {
			case 'volume':
				return round($value, 4) . ' m3';
			case 'surface':
				return round($value, 4) . ' m2';
			case 'length':
				return round($value, 4) . ' mm';
			case 'weight':
				return round($value, 4) . ' Kg';
			case 'density':
				return round($value, 4) . ' Kg/m3';
			default:
				return $value;
		}
	}
}
