<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class GetPostit extends AbstractHelper
{

	public $postitDao;

	public function __invoke($documentId, $spacename = 'workitem')
	{
		if ( !isset($this->postitDao) ) {
			$this->postitDao = \Rbs\Space\Factory::get($spacename)->getDao(\Rbs\Postit\Postit::$classId);
		}
		$dao = $this->postitDao;

		$postits = $dao->getFromParent($documentId)->fetchAll();
		if ( !$postits ) {
			return;
		}

		$html = '<div class="rb-postit">';

		foreach( $postits as $postit ) {
			$id = $postit['id'];
			$spacename = $postit['spacename'];
			$ownerName = $this->getView()->UserIdToName($postit['ownerId']);
			$body = $postit['body'];

			$html .= sprintf('
				<span class="rb-postit-item" data-id="%s" data-spacename="%s">
				<div class="btn-group">
				<button class="btn btn-warning btn-xs btn-postit"
						title="By"
						data-toggle="popover"
						data-content="%s">
					<span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
					%s
				</button>
				<button class="btn btn-default btn-xs deletepostit-btn">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
				</div>
				</span>', $id, $spacename, $ownerName, $body);
		}
		$html .= '</div>';
		return $html;
	}
}
