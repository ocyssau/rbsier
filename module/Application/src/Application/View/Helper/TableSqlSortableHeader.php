<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class TableSqlSortableHeader extends AbstractHelper
{

	public function __invoke($title, $urlOrderby)
	{
		$request = $this->getView()->getRequest();
		$order = $request->getQuery('order', $request->getPost('order'));
		$orderBy = $request->getQuery('order', $request->getPost('orderby'));
		$urlOrder = $order == 'asc' ? 'desc' : 'asc';
		$url = $this->getView()->url('admin', [
			'controller' => 'doctype'
		]) . '?&orderby='.$orderBy.'&order=' . $urlOrder;
		$glyphicon = '';

		$out = '<a title="Click me to sort" href="' . $url . '">' . $title . '<span class="' . $glyphicon . '"></span></a>';
		return $out;
	}
}
