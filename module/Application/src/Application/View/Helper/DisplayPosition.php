<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class DisplayPosition extends AbstractHelper
{

	public function __invoke($point)
	{
		$html = '<div class="row">';
		$html .= '<div class="col-sm-4">';
		$html .= '<ul class="list-group">
				<li class="list-group-item"><span class="label label-info">Ux</span> ' . round($point[0], 4) . ' mm</li>
				<li class="list-group-item"><span class="label label-info">Uy</span> ' . round($point[1], 4) . ' mm</li>
				<li class="list-group-item"><span class="label label-info">Uz</span> ' . round($point[2], 4) . ' mm</li>

		</ul>';
		$html .= '</div>';

		$html .= '<div class="col-sm-4">';
		$html .= '<ul class="list-group">
				<li class="list-group-item"><span class="label label-warning">Vx</span> ' . round($point[3], 4) . ' mm</li>
				<li class="list-group-item"><span class="label label-warning">Vy</span> ' . round($point[4], 4) . ' mm</li>
				<li class="list-group-item"><span class="label label-warning">Vz</span> ' . round($point[5], 4) . ' mm</li>
					
		</ul>';
		$html .= '</div>';

		$html .= '<div class="col-sm-4">';
		$html .= '<ul class="list-group">
				<li class="list-group-item"><span class="label label-success">Wx</span> ' . round($point[6], 4) . ' mm</li>
				<li class="list-group-item"><span class="label label-success">Wy</span> ' . round($point[7], 4) . ' mm</li>
				<li class="list-group-item"><span class="label label-success">Wz</span> ' . round($point[8], 4) . ' mm</li>
					
		</ul>';
		$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="row">';
		$html .= '<div class="col-sm-12">';
		$html .= '<ul class="list-group">
				<li class="list-group-item"><span class="label label-default">X</span> ' . round($point[9], 4) . ' mm</li>
				<li class="list-group-item"><span class="label label-default">Y</span> ' . round($point[10], 4) . ' mm</li>
				<li class="list-group-item"><span class="label label-default">Z</span> ' . round($point[11], 4) . ' mm</li>
					
		</ul>';
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}
}
