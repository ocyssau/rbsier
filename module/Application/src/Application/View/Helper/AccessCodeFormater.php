<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * 
 *
 */
class AccessCodeFormater extends AbstractHelper
{

	public function __invoke($acode)
	{
		$prefix = '';

		switch ($acode) {
			case 0:
				return $prefix . '<p class="rb-ac-free">' . tra('Free') . '</p>';
				break;
			case 1:
				return $prefix . '<p class="rb-ac-checkout">' . tra('Checkout') . '</p>';
				break;
			case 5:
				return $prefix . '<p class="rb-ac-inwf">' . tra('InWorkflow') . '</p>';
				break;
			case 6:
				return $prefix . '<p class="rb-ac-canceled">' . tra('Canceled') . '</p>';
				break;
			case 10:
				return $prefix . '<p class="rb-ac-validate">' . tra('Validate') . '</p>';
				break;
			case 11:
				return $prefix . '<p class="rb-ac-locked">' . tra('Locked') . '</p>';
				break;
			case 12:
				return $prefix . '<p class="rb-ac-deleted">' . tra('Deleted') . '</p>';
				break;
			case 13:
				return $prefix . '<p class="rb-ac-iteration">' . tra('Iteration') . '</p>';
				break;
			case 15:
				return $prefix . '<p class="rb-ac-version">' . tra('Previous Version') . '</p>';
				break;
			default:
				if ( $acode >= 20 ) {
					return '<p class="rb-ac-archived">' . tra('Archived') . '</p>';
				}
				return $prefix . '[' . $acode . ']';
				break;
		}
	}
}
