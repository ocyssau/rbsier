<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Rbs\Space\Factory as DaoFactory;

class CidToClass extends AbstractHelper
{

	public function __invoke($classId, $spacename = 'default')
	{
		(!$spacename) ? $spacename = 'default' : null;
		$class = DaoFactory::get($spacename)->getModelClass($classId);
		return $class;
	}
}
