<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 *
 *
 */
class ListFormater extends AbstractHelper
{

	/**
	 * @param array $list
	 */
	public function __invoke($list)
	{
		$html = '<ul class="list-group">';
		foreach( $list as $label => $value ) {
			$html .= '<li class="list-group-item"><span class="label label-default">' . $label . '</span> ' . $value . '</li>';
		}
		$html .= '</ul>';
		return $html;
	}
}
