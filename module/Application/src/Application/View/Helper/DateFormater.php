<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 *
 *
 */
class DateFormater extends AbstractHelper
{

	/**
	 * @param string $timestamp
	 * @param string $format
	 */
	public function __invoke($date, $format = null)
	{
		if(!$format){
			$format = $this->getView()->longDateformat;
		}
		
		if ( !$date ) {
			return '';
		}
		elseif ( is_string($date) ) {
			return $date;
		}
		elseif ( is_integer($date) ) {
			$dateTime = new \DateTime();
			$dateTime->setTimestamp($date);
			return $dateTime->format($format);
		}
		elseif ( $date instanceof \DateTime ) {
			return $date->format($format);
		}
		else {
			return;
		}
	}
}
