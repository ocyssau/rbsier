<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * 
 * @author ocyssau
 *
 */
class DisplayExtended extends AbstractHelper
{

	/**
	 *
	 * @param string $value
	 * @param array $properties Properties of extended property
	 */
	public function __invoke($value, $properties)
	{
		$type = $properties['type'];
		$view = $this->getView();

		switch ($type) {
			case 'text':
			case 'long_text':
			case 'html_area':
			case 'integer':
			case 'decimal':
			case 'select':
			case 'selectFromDB':
			case 'partner':
				$ret = $view->stringify($value);
				break;
			case 'doctype':
				$ret = $view->doctypeFormater($value);
				break;

			case 'documentVersion':
			case 'document_indice':
			case 'container_indice':
				$ret = $view->versionFormater($value);
				break;

			case 'user':
				$ret = $view->userIdToName($value);
				break;

			case 'process':
				$ret = $view->processIdToName($value);
				break;

			case 'category':
				$ret = $view->categoryIdToName($value);
				break;

			case 'date':
				$ret = $view->dateFormater($value);
				break;
		}
		return $ret;
	}
}
