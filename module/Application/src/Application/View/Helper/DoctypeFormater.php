<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class DoctypeFormater extends AbstractHelper
{

	/**
	 *
	 * @param integer $typeId
	 * @param boolean $withLabel
	 */
	public function __invoke($typeId, $withLabel = false)
	{
		$cacheUsualName = & \Ranchbe::$registry;
		if ( isset($cacheUsualName['type'][$typeId]) ) {
			return $cacheUsualName['type'][$typeId];
		}
		
		$defaultIconUrl = $this->view->basePath('/img/filetypes32/_default.gif');
		

		/* Docfile */
		if ( $typeId == -1 ) {
			$ret = 'docfile';
			$iconUrl = $defaultIconUrl;
		}
		/* Container */
		elseif ( $typeId == -10 ) {
			$ret = 'container';
			$iconUrl = $this->view->basePath('/img/filetypes32/folder.png');
		}
		/* Project */
		elseif ( $typeId == -15 ) {
			$ret = 'project';
			$iconUrl = $this->view->basePath('/img/filetypes32/folder.png');
		}
		/* Product Version */
		elseif ( $typeId == -20 ) {
			$ret = 'product';
			$iconUrl = $this->view->basePath('/img/filetypes32/CATProduct.gif');
		}
		/* Product Instance */
		elseif ( $typeId == -21 ) {
			$ret = 'productInstance';
			$iconUrl = $this->view->basePath('/img/filetypes32/CATProduct.gif');
		}
		else {
			$conn = \Rbplm\Dao\Connexion::get();
			$sql = "SELECT number FROM doctypes WHERE id = '$typeId'";
			$stmt = $conn->query($sql);
			$ret = $stmt->fetchColumn(0);
			
			if ( !$ret ) {
				$ret = tra('undefined');
				$iconUrl = $defaultIconUrl;
			}
			else {
				$ranchbe = \Ranchbe::get();
				$doctypeIconBaseUrl = $ranchbe->getConfig('icons.doctype.compiled.url');
				$doctypeIconBasePath = $ranchbe->getConfig('icons.doctype.compiled.path');
				$doctypeIconType = $ranchbe->getConfig('icons.doctype.type');

				$iconFile = $doctypeIconBasePath . '/' . $typeId . '.' . $doctypeIconType;
				$iconUrl = $this->view->basePath($doctypeIconBaseUrl . '/' . $typeId . '.' . $doctypeIconType);

				if ( !is_file($iconFile) ) {
					$iconUrl = $this->view->basePath('/img/filetypes32/_default.gif');
				}
			}
		}

		$html = "<img class=\"icon\" src=\"$iconUrl\" title=\"$ret\"/>";
		if ( $withLabel ) {
			$html .= '<label class="doctype-label">' . $ret . '</label>';
		}
		$ret = $html;
		$cacheUsualName['type'][$typeId] = $ret;
		return $ret;
	}
}
