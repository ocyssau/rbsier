<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class FilesizeFormater extends AbstractHelper
{

	public function __invoke($int)
	{
		if ( $int == 0 ) return 0;
		else if ( $int <= 1024 ) $format = $int . "oct";
		else if ( $int <= (10 * 1024) ) $format = sprintf("%.2f ko", ($int / 1024));
		else if ( $int <= (100 * 1024) ) $format = sprintf("%.1f ko", ($int / 1024));
		else if ( $int <= (1024 * 1024) ) $format = sprintf("%d ko", ($int / 1024));
		else if ( $int <= (100 * 1024 * 1024) ) $format = sprintf("%.2f Mo", ($int / (1024 * 1024)));
		else $format = sprintf("%d Go", ($int / (1024 * 1024 * 1024)));
		return $format;
	}
}
