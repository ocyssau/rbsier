<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Helper to display the <img> balise to icon of the file type
 */
class FileIcon extends AbstractHelper
{

	/**
	 *
	 * @param string $type extension of file to iconify
	 * @param string $iconType type of the icon file
	 * @return string
	 */
	public function __invoke($type, $iconType = '.gif')
	{
		$type = trim($type, ".");
		$ranchbe = \Ranchbe::get();

		$imgBasePath = $ranchbe->getConfig('icons.file.path');
		$imgBaseUrl = $ranchbe->getConfig('icons.file.url');
		$iconfile = $imgBasePath . '/' . $type . $iconType;

		if ( !is_file($iconfile) ) {
			$iconUrl = $imgBaseUrl . '/_default.gif';
		}
		else {
			$iconUrl = $imgBaseUrl . '/' . $type . $iconType;
		}

		$url = $this->getView()->basePath($iconUrl);
		return '<img alt="no icon" src="' . $url . '" />';
	}
}
