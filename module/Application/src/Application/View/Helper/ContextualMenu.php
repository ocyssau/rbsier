<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * 
 *
 */
class ContextualMenu extends AbstractHelper
{

	/**
	 * @param integer $userId
	 */
	public function __invoke(array $items, $enabledActions = false)
	{
		$html = '';
		foreach( $items as $item ) {
			if ( isset($item['menu']) ) {
				$html .= sprintf('<li class="%s">%s<ul>%s</ul></li>', $item['class'], $item['menu'], self::__invoke($item['items']));
			}
			else {
				if ( ! is_array($enabledActions) || in_array($item['name'], $enabledActions) ) {
					$html .= sprintf('<li data-assert="%s"><a href="%s" class="%s" title="%s">%s</a></li>', $item['assert'], $item['url'], $item['class'], tra($item['title']), tra($item['label']));
				}
			}
		}
		return $html;
	}
}
