<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 *
 *
 */
class GetUserRole extends AbstractHelper
{

	public $postitDao;

	/**
	 * @param integer $userId
	 */
	public function __invoke($userId)
	{
		$factory = \Rbs\Space\Factory::get();
		$list = $factory->getList(\Acl\Model\UserRole::$classId);
		$filter = $factory->getFilter(\Acl\Model\UserRole::$classId);
		$filter->with(array(
			'table' => 'acl_role',
			'alias' => 'role',
			'lefton' => 'roleId',
			'righton' => 'id'
		));
		$filter->andfind(':userId', 'userId');
		$filter->select(array(
			'user.id as userId',
			'user.login as userName',
			'role.id as roleId',
			'role.name as roleName'
		));
		$list->load($filter, array(
			':userId' => $userId
		));

		return $list;
	}
}
