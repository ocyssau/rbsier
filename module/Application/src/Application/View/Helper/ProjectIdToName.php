<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 *
 */
class ProjectIdToName extends AbstractHelper
{

	public function __invoke($id)
	{
		$cacheUsualName = & \Ranchbe::$registry;
		if ( isset($cacheUsualName['project'][$id]) ) {
			return $cacheUsualName['project'][$id];
		}

		$table = 'projects';

		$conn = \Rbplm\Dao\Connexion::get();
		$sql = "SELECT number FROM $table WHERE id = '$id'";
		$stmt = $conn->query($sql);
		$ret = $stmt->fetchColumn(0);

		if ( !$ret ) {
			$ret = tra('undefined');
		}

		$cacheUsualName['project'][$id] = $ret;
		return $ret;
	}
}
