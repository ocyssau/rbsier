<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class GetThumbnail extends AbstractHelper
{

	public function __invoke($productId)
	{
		$rb = \Ranchbe::get();
		$passphrase = $rb->getConfig('visu.passphrase');
		$path = $rb->getConfig('viewer.reposit.path');
		$thumbfile = \Rbplm\Pdm\Product\File::encodename($productId, 'jpg', $passphrase);
		if ( is_file($path . '/' . $thumbfile) ) {
			$thumbUrl = $this->view->url('viewer-getfile', array(
				'file' => $thumbfile
			));
			return '<img border="0" alt="no thumbs" src="' . $thumbUrl . '" />';
		}
		else {
			return '<!--no thumbs-->';
		}
	}
}
