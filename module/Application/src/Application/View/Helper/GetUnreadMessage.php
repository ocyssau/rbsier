<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Rbplm\Sys\Message;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class GetUnreadMessage extends AbstractHelper
{

	/**
	 * @param integer $userId
	 */
	public function __invoke()
	{
		$factory = \Rbs\Space\Factory::get();
		$filter = $factory->getFilter(Message::$classId);
		$filter->page(1, 5);
		$filter->sort('created', 'DESC');
		$filter->andFind('n', 'isRead', Op::EQUAL); /* Get unread messages */

		$list = $factory->getList(Message::$classId);
		$bind = array();
		$list->load($filter, $bind);
		return $list->toArray();
	}
}
