<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 *
 *
 */
class SecondToConvenientFormat extends AbstractHelper
{

	/**
	 * @param string $timestamp
	 * @param string $format
	 */
	public function __invoke($int)
	{
		if ( $int == 0 ) return 0;
		else if ( $int <= 60 ) $format = $int . " sec";
		else if ( $int < (60 * 60) ) $format = sprintf("%.2f mn", ($int / (60)));
		else if ( $int < (60 * 60 * 24) ) $format = sprintf("%.0f hrs", ($int / (60 * 60)));
		else if ( $int < (60 * 60 * 24 * 7) ) $format = sprintf("%.0f day", ($int / (60 * 60 * 24)));
		else if ( $int < (60 * 60 * 24 * 7 * 4) ) $format = sprintf("%d weeks", ($int / (60 * 60 * 24 * 6)));
		else $format = sprintf("%.0f month", ($int / (60 * 60 * 24 * 6 * 4)));
		return $format;
	}
}
