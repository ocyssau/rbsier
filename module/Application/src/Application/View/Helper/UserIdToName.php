<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Ranchbe;

/**
 */
class UserIdToName extends AbstractHelper
{

	/**
	 * 
	 * @param array|integer $inputs
	 * @return NULL|string
	 */
	public function __invoke($inputs)
	{
		if ( !$inputs ) return null;

		$userNames = [];
		$html = '';

		/* detect json */
		if ( is_array($inputs) ) {
			$userIds = $inputs;
		}
		elseif ( $inputs[0] == '[' ) {
			$inputs = trim($inputs, '#');
			$userIds = json_decode($inputs, true);
		}
		else {
			$inputs = trim($inputs, '#');
			$userIds = [
				$inputs
			];
		}

		/**/
		$cacheUsualName = & Ranchbe::$registry;
		if ( !isset($cacheUsualName['usernameStmt']) ) {
			$conn = \Rbplm\Dao\Connexion::get();
			$sql = "SELECT login FROM acl_user WHERE id = :id";
			$stmt = $conn->prepare($sql);
			$cacheUsualName['usernameStmt'] = $stmt;
		}
		else {
			$stmt = $cacheUsualName['usernameStmt'];
		}

		/**/
		foreach( $userIds as $userId ) {
			if ( isset($cacheUsualName['username'][$userId]) ) {
				$userNames[] = $cacheUsualName['username'][$userId];
			}
			else {
				$stmt->execute(array(
					':id' => $userId
				));
				$ret = $stmt->fetchColumn(0);
				if ( !$ret ) {
					$ret = '#' . $userId;
				}
				$cacheUsualName['username'][$userId] = $ret;
				$userNames[] = $ret;
			}
		}

		/**/
		foreach( $userNames as $userName ) {
			if ( count($userIds) > 1 ) {
				$html .= sprintf('<span class="rb-user-name badge">%s</span>', $userName);
			}
			else {
				$html .= sprintf('<span class="rb-user-name">%s</span>', $userName);
			}
		}

		return $html;
	}
}
