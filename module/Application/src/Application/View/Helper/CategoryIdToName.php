<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Convert category id to his name
 *
 */
class CategoryIdToName extends AbstractHelper
{

	public function __invoke($categoryId, $spacename = 'workitem')
	{
		$cacheUsualName = & \Ranchbe::$registry;
		if ( isset($cacheUsualName['category'][$categoryId]) ) {
			return $cacheUsualName['category'][$categoryId];
		}

		$table = 'categories';

		$conn = \Rbplm\Dao\Connexion::get();
		$sql = "SELECT name FROM $table WHERE id = '$categoryId'";
		$stmt = $conn->query($sql);
		$ret = $stmt->fetchColumn(0);

		if ( !$ret ) {
			$ret = tra('undefined');
		}

		$cacheUsualName['category'][$categoryId] = $ret;
		return $ret;
	}
}
