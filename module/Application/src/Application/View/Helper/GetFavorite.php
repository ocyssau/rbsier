<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Rbs\Org\Container\Favorite;
use Rbplm\People\CurrentUser;

/**
 *
 *
 */
class GetFavorite extends AbstractHelper
{

	/**
	 * @param integer $userId
	 */
	public function __invoke()
	{
		/* list of favorites */
		$factory = \Rbs\Space\Factory::get();
		$currentUser = CurrentUser::get();
		/* @var \Rbs\Org\Container\FavoriteDao $favorites */
		$favorites = $factory->getDao(Favorite::$classId);
		$list = $favorites->getFromUserId($currentUser->getId(), $favorites->getTranslator()
			->getSelectAsApp());
		return $list;
	}
}
