<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class DisplayAddress extends AbstractHelper
{

	public function __invoke($addresses)
	{
		$result = array();
		if ( $addresses instanceof \Zend\Mail\Address ) {
			$mail = $addresses->getEmail();
			echo $mail;
		}
		elseif ( is_array($addresses) ) {
			foreach( $addresses as $address ) {
				if ( $address instanceof \Zend\Mail\Address ) {
					$mail = $address->getEmail();
				}
				elseif ( is_string($address) ) {
					$mail = trim($address, '"');
				}
				$result[] = "<li class=\"list-group-item\">$mail</li>";
			}
		}

		if ( $result ) {
			$html = '<ul class="list-group">' . implode('', $result) . '</ul>';
		}
		else {
			$html = '<ul class="list-group"></ul>';
		}
		return $html;
	}
}
