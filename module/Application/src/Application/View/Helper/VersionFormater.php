<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Ranchbe;

class VersionFormater extends AbstractHelper
{

	public function __invoke($versionId, $iterationId = null)
	{
		$cache = & Ranchbe::$registry;

		if ( !isset($cache['indice'][$versionId]) ) {
			$conn = \Rbplm\Dao\Connexion::get();
			$sql = "SELECT `indice_value` FROM `document_indice` WHERE `indice_id` = '$versionId'";
			$stmt = $conn->query($sql);
			$versionName = $stmt->fetchColumn(0);
			$cache['indice'][$versionId] = $versionName;
		}
		else {
			$versionName = $cache['indice'][$versionId];
		}

		if ( !$versionName ) {
			$versionName = $versionId;
		}

		return ($iterationId) ? $versionName . '.' . $iterationId : $versionId;
	}
}
