<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 */
class Stringify extends AbstractHelper
{

	/**
	 *
	 * @param string $input
	 * @return void|string
	 */
	public function __invoke($inputs)
	{
		if ( is_string($inputs) ) {
			/* detect json */
			if ( $inputs[0] == '[' ) {
				$inputs = json_decode($inputs, true);
			}
			else {
				return $inputs;
			}
		}

		if ( is_array($inputs) ) {
			$html = '';
			foreach( $inputs as $str ) {
				$html .= sprintf('<span class="badge">%s</span>', $str);
			}
			return $html;
		}
		else {
			return $inputs;
		}
	}
}
