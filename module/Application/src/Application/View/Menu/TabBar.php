<?php
namespace Application\View\Menu;

/**
 * Container for NavBar
 * Contains Tabs
 */
class TabBar extends Tab
{

	/**
	 *
	 * @var array
	 */
	protected $tabs = array();

	/**
	 * @param string
	 * @return Tab
	 */
	public function getTab($name)
	{
		return $this->tabs[$name];
	}

	/**
	 * @return array
	 */
	public function getTabs()
	{
		return $this->tabs;
	}

	/**
	 * @param string $tabname
	 * @param string $url
	 * @param string $label
	 */
	public function addTab($tab)
	{
		$this->tabs[$tab->getName()] = $tab;
		$tab->setParent($this);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function render()
	{
		$html = '<ul class="nav navbar-nav">';
		$nextTabs = null;
		foreach( $this->tabs as $tab ) {
			if ( $tab instanceof TabBar ) {
				$html .= $tab->renderAsTab();
				if ( $tab->isActive() ) {
					$nextTabs = $tab;
				}
			}
			else {
				$html .= $tab->render();
			}
		}
		$html .= '</ul>';
		if ( $nextTabs ) {
			$html .= $nextTabs->render();
		}
		return $html;
	}

	/**
	 *
	 * @return string
	 */
	public function renderAsTab()
	{
		return parent::render();
	}
} /* End of class */
