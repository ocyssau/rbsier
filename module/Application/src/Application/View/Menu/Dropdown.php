<?php
namespace Application\View\Menu;

/**
 * Tab with dropdown menu
 *
 */
class Dropdown extends Tab
{

	/**
	 *
	 * @var array
	 */
	public $links;

	/**
	 *
	 * @var string
	 */
	public $glyphicon = 'glyphicon-user';

	/**
	 *
	 * @var boolean
	 */
	public $active = null;

	/**
	 *
	 * @param string $tabname
	 * @param string $url
	 * @param string $label
	 * @param TabBar $father
	 */
	public function __construct($tabname, $links)
	{
		parent::__construct($tabname);
		$this->links = $links;
	}

	/**
	 *
	 * @return string
	 */
	public function render()
	{
		$class = '';
		if ( $this->isActive() ) {
			$class = 'active';
		}

		$html = "<li class=\"dropdown $class\">";
		$html .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
		$html .= $this->label;
		$html .= '<span class="glyphicon ' . $this->glyphicon . '"></span>';
		$html .= '<span class="caret"></span></a>';
		$html .= '<ul class="dropdown-menu" role="menu">';
		foreach( $this->links as $name => $link ) {
			if ( $link == 'separator' ) {
				$html .= '<li class="divider" role="separator"></li>';
			}
			elseif ( $link == "header" ) {
				$html .= '<li class="dropdown-header" role="header">' . tra($name) . '</li>';
			}
			else {
				($name == $this->active) ? $class = 'active' : $class = null;
				$html .= "<li class=\"$class\">$link</li>";
			}
		}
		$html .= '</ul></li>';
		return $html;
	}
} /* End of class */
