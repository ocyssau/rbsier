<?php
namespace Application\View\Menu;

/**
 *
 *
 */
class Tab
{

	/**
	 * name of tab
	 * @var string
	 */
	protected $name;

	/**
	 * url of link
	 * @var string
	 */
	protected $url;

	/**
	 * displayed name
	 * @var string
	 */
	protected $label;

	/**
	 *
	 * @var boolean
	 */
	protected $isActive = false;

	/**
	 * @var TabBar
	 */
	protected $parent;

	/**
	 * level 0 for main bar and +1 for each sub-bar
	 * @var integer
	 */
	protected $level = 0;

	/**
	 * registry of tabs
	 * @var array
	 */
	protected static $_instances = array();

	/**
	 *
	 * @param string $tabname
	 * @param string $url
	 * @param string $label
	 * @param TabBar $parent
	 */
	public function __construct($tabname, $url = null, $label = null, TabBar $parent = null)
	{
		$this->name = $tabname;
		$this->url = $url;
		$this->label = $label;
		$this->parent = $parent;
		self::$_instances[$this->name] = & $this;
	}

	/**
	 * Named singleton
	 * @param string $name
	 */
	public static function get($name)
	{
		if ( isset(self::$_instances[$name]) ) {
			return self::$_instances[$name];
		}
	}

	/**
	 * @param string $name
	 */
	public function activate($name = null)
	{
		$this->active = $name;
		if ( $this->parent ) {
			$this->parent->setUrl($this->url);
			$this->parent->activate();
		}
		$this->isActive = true;
		return $this;
	}

	/**
	 * @param TabBar $parent
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getLevel()
	{
		return $this->level;
	}

	/**
	 *
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 *
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 *
	 * @param string $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @return string
	 */
	public function setLabel($label)
	{
		$this->label = $label;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isActive()
	{
		return $this->isActive;
	}

	/**
	 *
	 * @return string
	 */
	public function render()
	{
		$html = '';
		$class = '';
		if ( $this->isActive() ) {
			$class = ' active';
		}
		$html .= '<li class="rb-navtab ' . $class . '"><a href="' . $this->getUrl() . '">' . $this->getLabel() . '</a></li>';
		return $html;
	}
} /* End of class */
