<?php
namespace Application\View\Menu;

use Rbplm\People\CurrentUser;
use Zend\View\Model\ViewModel;
use Ranchbe;

/**
 * Factory for build main tab bar of application
 *
 * @author olivier
 *
 */
class MainTabBar
{

	/**
	 *
	 * @var MainTabBar
	 */
	private static $_instance;

	/**
	 *
	 * @var ViewModel
	 */
	private $view;

	/**
	 *
	 * @var \Zend\Mvc\Router\Http\TreeRouteStack
	 */
	private $router;

	/**
	 *
	 * @var TabBar
	 */
	private $tabBar;

	/**
	 *
	 * @var array
	 */
	private $tabs;

	/**
	 *
	 * @var array
	 */
	private $config;
	
	/**
	 *
	 * @var \Ranchbe
	 */
	private $ranchbe;

	/**
	 * @param ViewModel $view
	 * @param \Zend\Mvc\Router\Http\TreeRouteStack $router
	 */
	protected function __construct(\Ranchbe $ranchbe ,ViewModel $view, \Zend\Mvc\Router\Http\TreeRouteStack $router)
	{
		$this->view = $view;
		$this->router = $router;
		$this->tabBar = new TabBar('mainTabBar');
		$view->tabs = $this;
		$this->baseUrl = $router->getBaseUrl();
		$this->ranchbe = $ranchbe;
		
		$this->tabs = [
			'myspace' => [
				'url' => $this->getUrlFromRoute('workplace-wildspace'),
				'name' => 'myspace',
				'label' => tra('My space')
			]
		];
	}

	/**
	 * Singleton
	 *
	 * @param array $config
	 */
	public static function get($view = null)
	{
		if ( !self::$_instance ) {
			$ranchbe = Ranchbe::get();
			
			if ( !$view ) {
				$view = $ranchbe->getView();
			}
			
			/* @var \Zend\Mvc\Router\Http\TreeRouteStack $router */
			$router = $ranchbe->getMvcApplication()
				->getServiceManager()
				->get('router');

			$config = $ranchbe->getMvcApplication()->getConfig();
			
			self::$_instance = new self($ranchbe, $view, $router);
			self::$_instance->init($config);
		}

		return self::$_instance;
	}

	/**
	 * 
	 * @param string $routeName
	 * @param array $params
	 */
	private function getUrlFromRoute($routeName, $params = [])
	{
		return $this->router->assemble($params, [
			'name' => $routeName
		]);
	}

	/**
	 * $config['rbp'] = array(
	 * 'mockup'=>true,
	 * 'cadlib'=>true,
	 * 'bookshop'=>true,
	 * 'partner'=>true,
	 * 'admin'=>true,
	 * )
	 *
	 * @return MainTabBar
	 */
	public function init($config)
	{
		$this->config = $config;

		$this->tabBar->addTab(new Tab('home', $this->getUrlFromRoute('home'), tra('Welcome')));
		$this->tabBar->addTab($this->getMyspaceMenu($config['rbp']));

		$this->dropdownFactory($config['main_menu'], $this->tabBar);

		if ( $config['rbp']['module.admin'] ) {
			$this->tabBar->addTab($this->getSettingsMenu());
		}
		$this->tabBar->addTab($this->getUserMenu());
		$this->tabBar->addTab($this->getApplicationMenu());
		return $this;
	}

	/**
	 * $config['rbp'] = array(
	 * 'mockup'=>true,
	 * 'cadlib'=>true,
	 * 'bookshop'=>true,
	 * 'partner'=>true,
	 * 'admin'=>true,
	 * )
	 *
	 * @return MainTabBar
	 */
	private function dropdownFactory($config, $parent)
	{
		foreach( $config as $item ) {
			if ( isset($item['class']) && $item['class'] == 'dropdown' ) {
				$items = [];
				if ( isset($item['items']) ) {
					$items = $item['items'];
				}
				$dropdown = new Dropdown($item['name'], $this->itemFromConfig($items));
				$dropdown->setLabel(tra($item['label']));
				$dropdown->glyphicon = $item['glyphicon'];
				$parent->addTab($dropdown);
			}
			else {
				$url = '#';
				if ( isset($item['route']['options']['name']) ) {
					$url = $this->getUrlFromRoute($item['route']['options']['name'], $item['route']['params']);
				}
				$tab = new Tab($item['name'], $url, tra($item['label']));
				$parent->addTab($tab);
			}
		}
		return $this;
	}

	/**
	 * @param array $item
	 * @return array
	 */
	private function itemFromConfig(array $conf)
	{
		$items = [];
		foreach( $conf as $item ) {
			if ( $item['class'] == 'separator' ) {
				$items[$item['name']] = 'separator';
			}
			else {
				$url = $this->router->assemble($item['route']['params'], $item['route']['options']);
				$items[$item['name']] = sprintf('<a href="%s" class="%s">%s</a>', $url, '%anchor%', $item['label']);
			}
		}
		return $items;
	}

	/**
	 *
	 * @return \Application\View\Menu\Dropdown
	 */
	public function getUserMenu()
	{
		$currentUser = CurrentUser::get()->getLogin();

		$items = [
			'logout' => '<a href="' . $this->getUrlFromRoute('auth/logout') . '">' . tra('Logout') . '</a>',
			'context' => '<a href="' . $this->getUrlFromRoute('session') . '">' . tra('Context') . '</a>',
			'user-settings' => '<a href="' . $this->getUrlFromRoute('settings', [
				'action' => 'editme'
			]) . '">' . tra('Preferences') . '</a>'
		];

		$items = array_merge($items, $this->itemFromConfig($this->config['menu']['user']));
		$dropdown = new Dropdown('User', $items);

		$dropdown->glyphicon = 'glyphicon-user';
		$dropdown->setLabel($currentUser);
		return $dropdown;
	}

	/**
	 *
	 * @return \Application\View\Menu\Dropdown
	 */
	public function getApplicationMenu()
	{
		$this->getUrlFromRoute('application', [
			'action' => 'about'
		]);

		$items = [
			'version' => '<a href="#">' . \Ranchbe::getVersion() . '</a>',
			'website' => '<a href="' . \Ranchbe::$WEBSITE . '" title="RanchBE">' . tra('Web Site') . '</a>',
			'issues' => '<a href="' . \Ranchbe::$ISSUEMANAGER . '" title="Issues">' . tra('Signal a bug') . '</a>',
			'about' => '<a href="' . $this->getUrlFromRoute('application', [
				'action' => 'about'
			]) . '">' . tra('About Ranchbe') . '</a>',
			'help' => '<a href="' . \Ranchbe::$HELPSITE . '">' . tra('Help') . '</a>',
			'apidoc' => '<a href="' . $this->getUrlFromRoute('documentation', [
				'action' => 'api'
			]) . '">' . tra('Api Documentation') . '</a>',
			'devdoc' => '<a href="' . $this->getUrlFromRoute('documentation', [
				'action' => 'developer'
			]) . '">' . tra('Developer Documentation') . '</a>',
			'userdoc' => '<a href="' . $this->getUrlFromRoute('documentation', [
				'action' => 'enduser'
			]) . '">' . tra('User Documentation') . '</a>',
			'syslog' => '<a href="' . $this->getUrlFromRoute('application', [
				'action' => 'syslog'
			]) . '">' . tra('Sys Log') . '</a>',
			'displaymessage' => '<a href="#" class="btn-redisplay-messages">' . tra('Redisplay Last Messages') . '</a>'
		];

		$useWicoti = $this->config['rbp']['wicoti']['isdeployed'];
		if ( $useWicoti ) {
			$items = array_merge([
				'wicoti' => '<a href="' . $this->baseUrl . '/service/wicoti/deploy/download' . '" class="btn-download-wicoti">' . tra('Download Wicoti') . '</a>'
			], $items);
		}

		/**/
		$items = array_merge($items, $this->itemFromConfig($this->config['menu']['application']));

		$dropdown = new Dropdown('Ranchbe', $items);
		$dropdown->glyphicon = 'glyphicon glyphicon-info-sign';
		return $dropdown;
	}

	/**
	 *
	 * @param string $name
	 * @return \Application\View\Menu\Dropdown
	 */
	public function getMyspaceMenu($config)
	{
		$context = $this->ranchbe->getServiceManager()->getContext();
		$containerId = $context->getData('containerId');
		$spacename = $context->getData('spacename');
		$items = [];

		/* wildspace item */
		$url = $this->router->assemble([
			'action' => 'index'
		], [
			'name' => 'workplace-wildspace'
		]);
		$items['wildspace'] = '<a href="' . $url . '" class=\"%anchor%\">' . tra('Wildspace') . '</a>';

		/* basket */
		$url = $this->router->assemble([], [
			'name' => 'ged-document-basket'
		]);
		$items['mybasket'] = '<a href="' . $url . '" class=\"%anchor%\">' . tra('My Basket') . '</a>';
		$items['separator0'] = 'separator';

		/* actif container */
		if ( $containerId ) {
			$items['In Context'] = 'header';
			$url = $this->router->assemble([
				'spacename' => $spacename,
				'id' => $containerId,
				'action' => 'index'
			], [
				'name' => 'ged-document-manager'
			]);
			$items['container'] = '<a href="' . $url . '">' . $context->containerNumber . '</a>';
		}

		/* Get favorites containers 
		 * @todo: cache in session
		 */
		$helper = new \Application\View\Helper\GetFavorite();
		if ( $favorites = $helper() ) {
			$items['separator1'] = 'separator';
			$items['Favorites'] = 'header';
			foreach( $favorites as $favorite ) {
				$id = $favorite['containerId'];
				if ( $id == $containerId ) {
					continue;
				}
				$sn = $favorite['spacename'];
				isset($favorite['containerDescription']) ? $title = $favorite['containerDescription'] : $title = '';
				if ( $id && $sn ) {
					$url = $this->router->assemble([
						'spacename' => $sn,
						'id' => $id
					], [
						'name' => 'ged-container-activate'
					]);
					$items['favorites' . $id] = '<a title="' . $title . '" class="rb-favorite activate-btn" href="' . $url . '">' . $favorite['containerNumber'] . '</a>';
				}
				else {
					$items['favorites' . $id] = '<a class="rb-favorite activate-btn" href="_blank">' . $favorite['containerNumber'] . '</a>';
				}
			}
		}

		/* get spaces */
		$items['separator2'] = 'separator';
		$items['Spaces'] = 'header';

		$url = $this->router->assemble([
			'action' => 'index'
		], [
			'name' => 'ged-project'
		]);
		$items['project'] = '<a href="' . $url . '">' . tra('Projects') . '</a>';

		$url = $this->router->assemble([
			'action' => 'index'
		], [
			'name' => 'ged-workitem'
		]);
		$items['workitem'] = '<a href="' . $url . '">' . tra('WorkItems') . '</a>';

		if ( $config['module.bookshop'] ) {
			$url = $this->router->assemble([
				'action' => 'index'
			], [
				'name' => 'ged-bookshop'
			]);
			$items['bookshop'] = '<a href="' . $url . '">' . tra('Bookshops') . '</a>';
		}

		if ( $config['module.cadlib'] ) {
			$url = $this->router->assemble([
				'action' => 'index'
			], [
				'name' => 'ged-cadlib'
			]);
			$items['cadlib'] = '<a href="' . $url . '">' . tra('Cadlibs') . '</a>';
		}

		if ( $config['module.mockup'] ) {
			$url = $this->router->assemble([
				'action' => 'index'
			], [
				'name' => 'ged-mockup'
			]);
			$items['mockup'] = '<a href="' . $url . '">' . tra('Mockups') . '</a>';
		}

		if ( isset($this->session->displayDocfile) && $this->session->displayDocfile == true ) {
			$items['separator4'] = 'separator';
			$url = $this->getUrlFromRoute('ged-docfile-manager');
			$name = $context->containerNumber . '_' . tra('Files');
			$items['container_files'] = "<a href=\"$url\" class=\"%anchor%\">$name</a>";
		}

		/* Others tools */
		$items['separator5'] = 'separator';
		$items['Tools'] = 'header';

		$items = array_merge($items, $this->itemFromConfig($this->config['menu']['myspace']));

		$dropdown = new Dropdown('myspace', $items);
		$dropdown->glyphicon = 'glyphicon glyphicon-home';
		$dropdown->setLabel(tra('MySpace'));
		return $dropdown;
	}

	/**
	 *
	 * @return MainTabBar
	 */
	public function getUrl($tabName)
	{
		if ( isset($this->session->tabs[$tabName]['url']) ) {
			return $this->session->tabs[$tabName]['url'];
		}
		else {
			return $this->session->tabs[$tabName]['url'];
		}
	}

	/**
	 *
	 * @param string $name
	 * @return Tab
	 */
	public static function getTab($name)
	{
		if ( $name == 'spaces' ) $name = 'myspace';
		if ( $name == 'project' ) $name = 'myspace';
		return Tab::get($name);
	}

	/**
	 *
	 * @return MainTabBar
	 */
	public function getSettingsMenu()
	{
		$items = array(
			'config' => '<a href="' . $this->router->assemble([], [
				'name' => 'admin-configuration'
			]) . '">' . tra('Configuration') . '</a>',
			
			'tools' => '<a href="' . $this->router->assemble([], [
				'name' => 'admin-tools'
			]) . '">' . tra('Tools') . '</a>',
			
			'separator000' => 'separator',
			
			'category' => '<a href="' . $this->router->assemble([], [
				'name' => 'ged-category'
			]) . '">' . tra('Categories') . '</a>',
			
			'doctype' => '<a href="' . $this->router->assemble([], [
				'name' => 'ged-doctype'
			]) . '">' . tra('Doctypes') . '</a>',
			
			'typemime' => '<a href="' . $this->router->assemble([], [
				'name' => 'admin-typemime'
			]) . '">' . tra('TypeMimes') . '</a>',
			
			'separator00' => 'separator',
			
			'workflow' => '<a href="' . $this->router->assemble([], [
				'name' => 'process'
			]) . '">' . tra('Workflow') . '</a>',
			
			'separator0' => 'separator',
			
			'reposit' => '<a href="' . $this->router->assemble([], [
				'name' => 'vault'
			]) . '?layout=ranchbe">' . tra('Reposits') . '</a>',
			
			'distantsite' => '<a href="' . $this->router->assemble([], [
				'name' => 'vault-distant-site'
			]) . '?layout=ranchbe">' . tra('Distant Sites') . '</a>',
			
			'replicated' => '<a href="' . $this->router->assemble([], [
				'name' => 'vault-replicated'
			]) . '?layout=ranchbe">' . tra('Replicated Reposits') . '</a>',
			
			'separator1' => 'separator',
			
			'user' => '<a href="' . $this->router->assemble([], [
				'name' => 'adminuser'
			]) . '">' . tra('Users') . '</a>',

			'role' => '<a href="' . $this->router->assemble([], [
				'name' => 'aclrole'
			]) . '?resourceCn=app">' . tra('Roles For Application') . '</a>',
			
			'separator4' => 'separator',
			
			'trash' => '<a href="' . $this->router->assemble([], [
				'name' => 'admin-trash'
			]) . '">' . tra('Trash') . '</a>'
		);

		/**/
		$items = array_merge($items, $this->itemFromConfig($this->config['menu']['admin']));
		
		/* */
		$dropdown = new Dropdown('Admin', $items);
		$dropdown->setLabel(tra('Settings'));
		$dropdown->glyphicon = 'glyphicon glyphicon-cog';
		return $dropdown;
	}

	/**
	 */
	public function toHtml()
	{
		return $this->tabBar->render();
	}
} /* End of class */
