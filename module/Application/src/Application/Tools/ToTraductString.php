<?php
namespace Application\Tools;

/**
 *
 *
 */
class ToTraductString
{

	/**
	 * 
	 * @var array
	 */
	public $toTraductStrings = array();

	/**
	 * 
	 * @var array
	 */
	public $exists = array();

	/**
	 *
	 * @param string $basePath
	 * @return void
	 */
	public function parseDirectoryTree($basePath)
	{
		$directoryIt = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($basePath));
		foreach( $directoryIt as $file ) {
			$extension = substr($file->getFilename(), strrpos($file->getFilename(), '.'));
			if ( $extension == '.php' || $extension == '.phtml' ) {
				echo str_repeat(' ', $directoryIt->getDepth()) . 'Running extraction on: ' . $file->getFilename() . "\n";
				$this->parseFile($file->openFile());
			}
		}
	}

	/**
	 *
	 * @param
	 *        	$SPLfile
	 * @return void
	 */
	public function parseFile(\SplFileObject $SPLfile)
	{
		while( !$SPLfile->eof() ) {
			$toTraductString = $this->parseStr($SPLfile->fgets());
			if ( $toTraductString ) {
				$toTraductString = strtolower($toTraductString);
				if ( !isset($this->exists[$toTraductString]) ) {
					$this->toTraductStrings[$toTraductString] = $toTraductString;
				}
			}
		}
	}

	/**
	 *
	 * @param string $str
	 * @return string
	 */
	public function parseStr($str)
	{
		$regexGrp1 = 'tra\(';
		$regexGrp2 = "[\"\']{1}";
		$regexGrp3 = "[\w\s\'%]+";
		$regexGrp4 = $regexGrp2;

		// $regexQuote = preg_quote ($regex);
		$regex = "($regexGrp1)($regexGrp2)($regexGrp3)($regexGrp4)";

		$str = trim($str);
		$matches = [];
		preg_match("/$regex/i", $str, $matches);

		if ( isset($matches[3]) ) {
			return $matches[3];
		}
	}
}


