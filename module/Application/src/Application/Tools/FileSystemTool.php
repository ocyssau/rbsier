<?php
namespace Application\Tools;

/**
 *
 *
 */
class FileSystemTool
{

	/**
	 *
	 * @param string $path
	 * @param boolean $recursive
	 * @param boolean $verbose
	 */
	public static function removeDir($path, $recursive = false, $verbose = false)
	{
		/* Add trailing slash to $path if one is not there */
		if ( substr($path, -1, 1) != "/" ) {
			$path .= "/";
		}
		if ( $curdir = opendir($path) ) {
			while( $file = readdir($curdir) ) {
				if ( $file != '.' && $file != '..' ) {
					$file = $path . $file;
					if ( is_file($file) === true ) {
						if ( !unlink($file) ) {
							echo "failed to removed File: " . $file . "\n\r";
							return false;
						}
						else {
							if ( $verbose ) echo "Removed File: " . $file . "\n\r";
						}
					}
					else if ( is_dir($file) === true && $recursive === true ) {
						/* If this Directory contains a Subdirectory and if recursivity is requiered, run this Function on it */
						if ( !removeDir($file, $recursive, $verbose) ) {
							if ( $verbose ) echo "failed to removed File: " . $file . "\n\r";
							return false;
						}
					}
				}
			}
			closedir($curdir);
		}

		/* Remove Directory once Files have been removed (If Exists) */
		if ( is_dir($path) ) {
			if ( @rmdir($path) ) {
				if ( $verbose ) echo "Removed Directory: " . $path . "\n\r";
				return true;
			}
		}
		return false;
	}

	/**
	 * @param string $srcPath Source directory
	 * @param string $dstPath Destination directory
	 * @param array $filter Array of files to ignores
	 * @param \Rbplm\Sys\Logger $logger [OPTIONAL] If set, logs events
	 * @param boolean $recursive [OPTIONAL] Copy files recursively
	 * @param number $mode [OPTIONAL] chmod used for create destinations files and directory. Default is 0777
	 */
	public static function dircopy($srcPath, $dstPath, array $filter = [], \Rbplm\Sys\Logger $logger = null, $recursive = true, $mode = 0755)
	{
		/* create destination directory if not existing */
		if ( !is_dir($dstPath) ) {
			if ( $logger ) $logger->log("create directory " . $dstPath);
			mkdir($dstPath, $mode, true);
		}

		$srcDirectory = new \DirectoryIterator($srcPath);
		foreach( $srcDirectory as $element ) {
			if ( in_array($element->getFileName(), $filter) ) {
				if ( $logger ) $logger->log('Ignore  ' . $element->getPathname());
				continue;
			}
			elseif ( $element->isFile() ) {
				if ( $logger ) $logger->log('copy ' . $element->getPathname() . ' to ' . $dstPath . '/' . $element->getFileName());
				copy($element->getPathname(), $dstPath . '/' . $element->getFileName());
			}
			elseif ( $element->isDir() && !$element->isDot() ) {
				$copyTo = $dstPath . '/' . $element->getFileName();
				self::dircopy($element->getPathname(), $copyTo, $filter, $logger, $recursive, $mode);
			}
		}
		return true;
	}

	/**
	 *
	 * @param string $dir
	 * @param boolean $verbose
	 */
	public static function cleansvn($dir, $verbose = true)
	{
		if ( !is_dir($dir) ) return false;
		if ( $curdir = opendir($dir) ) {
			while( $file = readdir($curdir) ) {
				if ( $file == '.' || $file == '..' ) continue;
				if ( $file == '.svn' || $file == 'Thumbs.db' ) {
					$file = $dir . '/' . $file;
					if ( is_dir("$file") ) {
						if ( removeDir($file, true, true) ) {
							echo "Delete directory $file \n\r";
						}
						else {
							print "Error: $file cant be suppressed \n";
						}
					}
				}
				else {
					$file = $dir . '/' . $file;
					if ( is_dir($file) ) {
						//echo "$file \n\r";
						cleansvn($file, $verbose);
					}
				}
			}
			closedir($curdir);
		}
		return true;
	}

	/**
	 *
	 * @param string $dir
	 * @param boolean $verbose
	 */
	public static function emptyDir($dir, $verbose = true)
	{
		if ( $curdir = opendir($dir) ) while( $file = readdir($curdir) ) {
			if ( $file == '.' || $file == '..' || $file == 'readme' ) continue;
			$file = $dir . '/' . $file;
			if ( is_file($file) === true ) {
				if ( !unlink($file) ) {
					echo "failed to removed File: " . $file . "\n\r";
					return false;
				}
				else {
					if ( $verbose ) echo "Removed File: " . $file . "\n\r";
				}
			}
			if ( is_dir($file) === true ) {
				if ( !removeDir($file, true, true) ) {
					echo "failed to removed directory: " . $file . "\n\r";
					return false;
				}
				else {
					if ( $verbose ) echo "Removed directory: " . $file . "\n\r";
				}
			}
		}
		closedir($curdir);
		return true;
	}

	/**
	 *
	 * @param string $dir
	 * @param string $basepath		base path of application
	 * @param boolean $verbose
	 * @param array $output
	 *
	 */
	public static function md5ofAllDirectoryFiles($dir, $basepath, $verbose = true, $output = array())
	{
		if ( $curdir = opendir($dir) ) while( $file = readdir($curdir) ) {
			if ( $file == '.' || $file == '..' ) continue;
			$file = $dir . '/' . $file;
			if ( is_file($file) === true ) {
				$index = str_replace($basepath, '', $file);
				$output[$index] = md5_file($file);
			}
			if ( is_dir($file) === true ) {
				$md5s = md5ofAllDirectoryFiles($file, $basepath, $verbose);
				$output = array_merge($output, $md5s);
			}
		}
		closedir($curdir);
		return $output;
	}

	/**
	 * @todo : add original directory structure
	 *
	 * @param string $dir
	 * @param \ZipArchive $zip
	 * @param boolean $recursive
	 * @param boolean $verbose
	 */
	public static function zipDir($dir, \ZipArchive $zip, $recursive = true, $verbose = true)
	{
		if ( $curdir = opendir($dir) ) while( $localfile = readdir($curdir) ) {
			if ( $localfile == '.' || $localfile == '..' ) continue;
			if ( $dir == './' || $dir == '.' ) $file = $localfile;
			else $file = $file = $dir . '/' . $localfile;
			if ( is_file($file) === true ) {
				$zip->addFile($file, $file);
				if ( $verbose ) echo "Add File: $file to zip file \n\r";
			}
			if ( is_dir($file) === true ) {
				zipDir($file, $zip, $recursive, $verbose);
			}
		}
		closedir($curdir);
		return true;
	}
}
