<?php
namespace Application\Tools;

/**
 *
 *
 */
class ErrorsCode
{

	public $errorCode = array();

	/**
	 *
	 * @param $basePath
	 * @return void
	 */
	public function parseDirectoryTree($basePath)
	{
		$directoryIt = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($basePath));
		foreach( $directoryIt as $file ) {
			$extension = substr($file->getFilename(), strrpos($file->getFilename(), '.'));
			if ( $extension == '.php' ) {
				echo str_repeat(' ', $directoryIt->getDepth()) . 'Running extraction on: ' . $file->getFilename() . "\n";
				$this->parseFile($file->openFile());
			}
		}
	}

	/**
	 *
	 * @param $SPLfile
	 * @return void
	 */
	public function parseFile(\SplFileObject $SPLfile)
	{
		while( !$SPLfile->eof() ) {
			$errorCode = $this->parseStr($SPLfile->fgets());
			if ( $errorCode ) {
				$this->errorCode[] = $errorCode;
			}
		}
	}

	/**
	 *
	 * @param $str
	 * @return string
	 */
	public function parseStr($str)
	{
		$regex = '(.+Rbplm_Sys_Exception+\s*\({1}\s*[\'"]?){1}([A-Z_a-z0-9 ]+){1}(\s*[\'"]?\s*[,]{1}\s*.+\){1}.+){1}';
		//$regexQuote = preg_quote ($regex);
		$split = preg_split("/^$regex$/i", $str, null, PREG_SPLIT_DELIM_CAPTURE);
		if ( $split[2] ) {
			return $split[2];
		}
	}

	/**
	 *
	 * @return void
	 */
	public function getCleanErrorCodeList()
	{
		$errorCode = array_unique($this->errorCode);
		asort($errorCode);

		return $this->errorCode = $errorCode;
	}

	/**
	 *
	 * @return void
	 */
	public function toConstantDeclaration()
	{
		$constants = array();
		foreach( $this->errorCode as $errorCode ) {
			$constants[] = "define($errorCode, '$errorCode');";
		}
		return implode("\n", $constants);
	}

	/**
	 *
	 * @param string $className
	 * @return void
	 */
	public function toStaticClass($className)
	{
		/*
		 $class = new Zend_CodeGenerator_Php_Class();
		 $class->setName($className);

		 $this->getCleanErrorCodeList();
		 foreach( $this->errorCode as $errorCode ) {
		 $property = new Zend_CodeGenerator_Php_Property();
		 $property->setConst(true);
		 $property->setName($errorCode);
		 $property->setDefaultValue($errorCode);
		 $class->setProperty($property);
		 }

		 $output = str_replace('_', '/', $className) . '.php';
		 //Put result in file
		 if ( is_file($output) ) {
		 rename($output, $output . uniqid() . '.bck');
		 echo 'Output file ' . $output . ' is existing!' . CRLF;
		 }
		 $outputDir = dirname($output);
		 if ( !is_dir($outputDir) ) {
		 mkdir($outputDir, 0777, true);
		 }

		 $classContent = '<?php' . "\n";
		 $classContent .= $class->generate();
		 $classContent .= "\n";

		 file_put_contents($output, $classContent);
		 echo 'See result file ' . $output . CRLF;
		 */
		//return $class;
	}
}


