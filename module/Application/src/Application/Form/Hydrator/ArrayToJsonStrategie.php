<?php
namespace Application\Form\Hydrator;

use Zend\Hydrator\Strategy\StrategyInterface;

/**
 * Strategie to convert array as json string
 *
 */
class ArrayToJsonStrategie implements StrategyInterface
{

	public function extract($value)
	{
		if ( is_array($value) ) {
			return json_encode($value);
		}
		return;
	}

	public function hydrate($value)
	{
		if ( $value ) {
			if ( $value[0] == '{' ) {
				return json_decode($value, true);
			}
		}
		return null;
	}
}
