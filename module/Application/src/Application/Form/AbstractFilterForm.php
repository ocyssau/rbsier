<?php
namespace Application\Form;

use Zend\Form\Form;
use Rbplm\Dao\Filter\Op;
use Application\Form\Filter\ElementFactory;

/**
 */
abstract class AbstractFilterForm extends Form
{

	/**
	 *
	 * @var string
	 */
	const SESSION_KEY = 'filter';

	/**
	 *
	 * @var \Zend\Session\Container
	 */
	protected $sessionContainer;
	
	/**
	 *
	 * @var array
	 */
	protected $sessionDatas;
	
	/**
	 * BE CAREFULL: Prevent name collision with Zend\Form\Fieldset\$Factory var
	 * 
	 * @var \Rbs\Space\Factory
	 */
	protected $daoFactory;

	/**
	 * Name of session var to store filters
	 * 
	 * @var string
	 */
	protected $namespace;

	/**
	 *
	 * @var string
	 */
	protected $template;

	/**
	 *
	 * @var \Application\View\ViewModel
	 */
	protected $view;

	/**
	 *
	 * @var ElementFactory
	 */
	protected $elemtFactory;

	/**
	 * Bind array value to transmit to PDO request
	 *
	 * @var array
	 */
	public $bind = [];

	/**
	 *
	 * @param \Rbs\Space\Factory $factory        	
	 * @param string $name Name of key in the paginator session container where store parameters of this form
	 */
	public function __construct($factory, $name)
	{
		parent::__construct();

		$this->view = new \Zend\View\Model\ViewModel();

		$this->setAttribute('id', 'filterForm')
			->setAttribute('method', 'get')
			->setAttribute('class', 'form-inline');

		/* dao */
		$this->daoFactory = $factory;
		
		/* session */
		$this->sessionContainer = \Ranchbe::get()->getServiceManager()->getSessionManager()->containerRegistry->get(self::SESSION_KEY);
		$this->namespace = $name;
		if ( $this->sessionContainer->offsetExists($name) ) {
			$this->sessionDatas = $this->sessionContainer->offsetGet($name);
		}
		else {
			$this->sessionDatas = [];
		}
	}

	/**
	 * 
	 * @return \Application\Form\Filter\ElementFactory
	 */
	public function getElementFactory()
	{
		if ( !isset($this->elemtFactory) ) {
			$this->elemtFactory = new ElementFactory($this, $this->daoFactory);
		}
		return $this->elemtFactory;
	}

	/**
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter        	
	 * @return \Application\Form\AbstractFilterForm
	 */
	public function bindToFilter($filter)
	{
		foreach( $this->getElements() as $element ) {
			$search = $element->getValue();
			$where = $element->getAttribute('data-where');
			if ( $where ) {
				$op = $element->getAttribute('data-op');
				if ( !$op ) {
					$op = Op::CONTAINS;
				}
				$splitted = explode(' ', $search);
				foreach( $splitted as $word ) {
					$paramName = ':' . uniqid();
					$word = str_replace('*', '%', $word);
					$filter->orFind($paramName, $this->key, $op);
					$this->bind[$paramName] = '%' . $word . '%';
				}
			}
		}
		return $this;
	}

	/**
	 *
	 * @param string $in        	
	 * @return string Formated date
	 */
	public function dateToSys($in)
	{
		if ( $in instanceof \DateTime ) {
			return $in->format(\Rbplm\Dao\Mysql::DATE_FORMAT);
		}
		elseif ( $in == 0 ) {
			return null;
		}
		elseif ( is_string($in) ) {
			$date = new \DateTime($in);
			return $date->format(\Rbplm\Dao\Mysql::DATE_FORMAT);
		}
	}

	/**
	 */
	public function load(array $options = [])
	{
		/* request datas */
		(isset($options['request'])) ? $requestDatas = $options['request'] : $requestDatas = $_GET;

		/* session datas */
		(isset($options['session'])) ? $sessionDatas = $options['session'] : $sessionDatas = $this->sessionDatas;

		/* default datas */
		(isset($options['default'])) ? $defaultDatas = $options['default'] : $defaultDatas = [];

		(isset($requestDatas['resetf'])) ? $resetFilter = $requestDatas['resetf'] : $resetFilter = false;
		if ( $resetFilter ) {
			$this->reset();
			return $this;
		}

		$data = array_merge($defaultDatas, $sessionDatas, $requestDatas);
		$this->setData($data);
		return $this;
	}

	/**
	 * 
	 * @return \Application\Form\AbstractFilterForm
	 */
	public function save()
	{
		$this->sessionContainer->offsetSet($this->namespace, $this->getData());
		return $this;
	}

	/**
	 * 
	 * @return \Application\Form\AbstractFilterForm
	 */
	public function reset()
	{
		$this->sessionContainer->offsetSet($this->namespace, []);
		$this->setData([]);
		return $this;
	}

	/**
	 * @param \Zend\View\Renderer\RendererInterface $renderer        
	 * @return string	
	 */
	public function render($renderer)
	{
		$view = $this->view;
		$view->setTemplate($this->template);
		$view->setTerminal(true);
		$view->filterForm = $this;
		$html = $renderer->render($view);
		return $html;
	}
}
