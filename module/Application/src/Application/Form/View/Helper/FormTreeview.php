<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Application\Form\View\Helper;

use Zend\Form\ElementInterface;
use Application\Form\Element\TreeView;
use Zend\Form\Exception;

/**
 * 
 *
 */
class FormTreeview extends \Zend\Form\View\Helper\AbstractHelper
{

	/**
	 * Invoke helper as functor
	 *
	 * Proxies to {@link render()}.
	 *
	 * @param  ElementInterface|null $element
	 * @return string|FormTreeview
	 */
	public function __invoke(ElementInterface $element = null)
	{
		if ( !$element ) {
			return $this;
		}
		return $this->render($element);
	}

	/**
	 * Render a form <input> element from the provided $element
	 *
	 * @param  ElementInterface $element
	 * @throws Exception\DomainException
	 * @return string
	 */
	public function render(ElementInterface $element)
	{
		$name = $element->getName();
		if ( $name === null || $name === '' ) {
			throw new Exception\DomainException(sprintf('%s requires that the element has an assigned name; none discovered', __METHOD__));
		}
		if ( !$element instanceof TreeView ) {
			throw new Exception\InvalidArgumentException(sprintf('%s requires that the element is of type Zend\Form\Element\MultiCheckbox', __METHOD__));
		}

		$attributes = $element->getAttributes();
		$attributes['name'] = $name;
		$type = $this->getType($element);
		$attributes['type'] = $type;
		$attributes['value'] = $element->getValue();

		return sprintf('<input %s%s', $this->createAttributesString($attributes), $this->getInlineClosingBracket());
	}

	/**
	 * Return input type
	 *
	 * @return string
	 */
	protected function getType()
	{
		return 'treeview';
	}
}
