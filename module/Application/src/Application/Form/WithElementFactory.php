<?php
namespace Application\Form;

/**
 *
 *
 */
trait WithElementFactory
{

	/**
	 * @return ElementFactory
	 */
	public function getElemtFactory($factory = null)
	{
		if ( !isset($this->elemtFactory) ) {
			$this->elemtFactory = new ElementFactory($this, $factory);
		}
		return $this->elemtFactory;
	}
} /* End of class */
