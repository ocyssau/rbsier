<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * 
 *
 */
class AuthForm extends Form
{

	/**
	 * 
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('auth');
		$this->setAttribute('method', 'post');

		$this->add(array(
			'name' => 'logout',
			'type' => 'Zend\Form\Element\Hidden',
			'value' => 0
		));
		$this->add(array(
			'name' => 'layout',
			'type' => 'Zend\Form\Element\Hidden'
		));
		$this->add(array(
			'name' => 'applayout',
			'type' => 'Zend\Form\Element\Hidden'
		));

		/* USERNAME */
		$this->add(array(
			'name' => 'username',
			'type' => 'Zend\Form\Element\Text',
			'required' => true,
			'filters' => array(
				array(
					'name' => 'Zend\Filter\StripTags'
				)
			),
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'User name'
			),
			'options' => array(
				'label' => 'Name'
			)
		));

		/* PASSWORD */
		$this->add(array(
			'name' => 'password',
			'type' => 'Zend\Form\Element\Password',
			'required' => true,
			'filters' => array(
				array(
					'name' => 'Zend\Filter\StripTags'
				)
			),
			'attributes' => array(
				'type' => 'Password',
				'placeholder' => 'Password'
			),
			'options' => array(
				'label' => 'Password'
			)
		));

		/* FAKE USER */
		$this->add(array(
			'name' => 'fakeuser',
			'type' => 'Zend\Form\Element\Text',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'Zend\Filter\StripTags'
				)
			),
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Fake user name'
			),
			'options' => array(
				'label' => 'Fake Name'
			)
		));

		/* REMEMBER ME */
		$this->add(array(
			'name' => 'rememberme',
			'type' => 'Zend\Form\Element\Checkbox',
			'options' => array(
				'label' => 'Remember Me'
			)
		));

		/* BUTTONS */
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Sign in'
			)
		));
	}
}