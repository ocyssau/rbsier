<?php
namespace Application\Form;

use Zend\Stdlib\ArraySerializableInterface;

/**
 * A generic class to be binded to Zend\Form (setObject($binded))
 *
 */
class BindModel implements ArraySerializableInterface
{

	/**
	 * From ArraySerializableInterface
	 */
	public function getArrayCopy()
	{
		$properties = get_object_vars($this);
		return $properties;
	}

	/**
	 * From ArraySerializableInterface
	 *
	 * @param array $properties
	 */
	public function exchangeArray(array $properties)
	{
		foreach( $properties as $name => $value ) {
			$this->$name = $value;
		}
		return $this;
	}
} /* End of class */
