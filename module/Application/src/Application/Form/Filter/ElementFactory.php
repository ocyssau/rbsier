<?php
namespace Application\Form\Filter;

use Rbplm\Dao\Filter\Op;

/**
 * 
 *
 */
class ElementFactory extends \Application\Form\ElementFactory
{

	/**
	 *
	 * @return ElementFactory
	 */
	public function magicElement()
	{
		$this->form->add(array(
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => tra('Search for number or designation...'),
				'class' => 'form-control',
				'size' => 60
			),
			'options' => array(
				'label' => tra('Number Or designation')
			)
		));
		$this->form->getInputFilter()->add(array(
			'name' => 'find_magic',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StringTrim'
				)
			)
		));
		return $this;
	}

	/**
	 *
	 * @return ElementFactory
	 */
	public function numberElement($where = 'number')
	{
		$this->form->add(array(
			'name' => 'find_number',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for number...',
				'title' => 'use * character to define your pattern. If none, * will be add to start and end of the chain',
				'class' => 'form-control',
				'data-where' => $where,
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Number'
			)
		));
		$this->form->getInputFilter()->add(array(
			'name' => 'find_number',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StringTrim'
				)
			)
		));
		return $this;
	}

	/**
	 *
	 * @return ElementFactory
	 */
	public function designationElement($where = 'description')
	{
		$this->form->add(array(
			'name' => 'find_designation',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for designation...',
				'class' => 'form-control',
				'data-where' => $where,
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation'
			)
		));
		$this->form->getInputFilter()->add(array(
			'name' => 'find_designation',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StringTrim'
				)
			)
		));
		return $this;
	}

	/**
	 * @param array $properties
	 * @return ElementFactory
	 */
	public function findInElement(array $properties)
	{
		$properties = array_merge([
			null => tra('...property')
		], $properties);

		$this->form->add(array(
			'name' => 'find',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Find'
			)
		));
		$this->form->getInputFilter()->add(array(
			'name' => 'find',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StringTrim'
				)
			)
		));

		/* Find in field */
		$this->form->add(array(
			'name' => 'find_field',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control selectpicker rb-select',
				'data-width' => '200px'
			),
			'options' => array(
				'label' => tra('In'),
				'value_options' => $properties
			)
		));
		$this->form->getInputFilter()->add(array(
			'name' => 'find_field',
			'required' => false
		));

		return $this;
	}

	/**
	 * @param array actions
	 * @return ElementFactory
	 */
	public function userActionElement(array $actions)
	{
		$actions = array_merge([
			null => tra('Select an action...')
		], $actions);

		$this->form->add(array(
			'name' => 'f_action_field',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control selectpicker rb-select',
				'data-width' => '200px'
			),
			'options' => array(
				'label' => tra('Action'),
				'value_options' => $actions
			)
		));
		$this->form->getInputFilter()->add(array(
			'name' => 'f_action_field',
			'required' => false
		));

		/* Select action by user */
		$this->form->add(array(
			'name' => 'f_action_user_name',
			'type' => 'Application\Form\Element\SelectUser',
			'attributes' => array(
				'class' => 'form-control selectpicker rb-select rb-select-user',
				'data-width' => '200px'
			),
			'options' => array(
				'label' => tra('By'),
				'maybenull' => true,
				'unselected_value' => '...by any users'
			)
		));
		$this->form->getInputFilter()->add(array(
			'name' => 'f_action_user_name',
			'required' => false
		));

		return $this;
	}

	/**
	 *
	 * @return ElementFactory
	 */
	public function advancedCbElement()
	{
		$this->form->add(array(
			'name' => 'f_adv_search_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Advanced'
			)
		));
		$this->form->getInputFilter()->add(array(
			'name' => 'f_adv_search_cb',
			'required' => false
		));
		return $this;
	}

	/**
	 *
	 * @return ElementFactory
	 */
	public function dateAndTimeCbElement()
	{
		/* check box */
		$this->form->add(array(
			'name' => 'f_dateAndTime_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Date And Time'
			)
		));
		$this->form->getInputFilter()->add(array(
			'name' => 'f_dateAndTime_cb',
			'required' => false
		));
		return $this;
	}

	/**
	 *
	 * @return ElementFactory
	 */
	public function dateAndTimeElement($name = 'open', $label)
	{
		/* Open date */
		$cbName = sprintf('f_%s_date_cb', $name);
		$this->form->add(array(
			'name' => $cbName,
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => $label
			)
		));
		/* Superior to date */
		$minName = sprintf('f_%s_date_min', $name);
		$this->form->add(array(
			'name' => $minName,
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Superior to'
			)
		));
		/* Inferior to date */
		$maxName = sprintf('f_%s_date_max', $name);
		$this->form->add(array(
			'name' => $maxName,
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Inferior to'
			)
		));
		$inputFilter = $this->form->getInputFilter();
		$inputFilter->add(array(
			'name' => $cbName,
			'required' => false
		));
		$inputFilter->add(array(
			'name' => $minName,
			'required' => false
		));
		$inputFilter->add(array(
			'name' => $maxName,
			'required' => false
		));
		return $this;
	}
}
