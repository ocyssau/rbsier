<?php
namespace Application\Form;

use Rbplm\Dao\Filter\Op;
use Rbs\Space\Factory as DaoFactory;

/**
 * 
 *
 */
class StdFilterForm extends AbstractFilterForm
{

	public $where;

	public $passThrough = true;

	public $key = 'name';

	public $bind = array();

	public $nameSpace;

	/**
	 *
	 * @param DaoFactory $factory
	 * @param string $name
	 */
	public function __construct(DaoFactory $factory = null, $name = null)
	{
		$this->template = 'application/filter/std-filter-form.phtml';
		parent::__construct($factory, $name);

		$this->add(array(
			'name' => 'stdfilter-searchInput',
			'type' => 'Zend\Form\Element\Text',
			'options' => array(
				'label' => ''
			),
			'attributes' => array(
				'onChange' => '',
				'class' => 'form-control'
			)
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Submit',
			'name' => 'stdfilter-submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Filter',
				'id' => 'stdfilter-submit',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Form.Form::prepare()
	 */
	public function prepare()
	{
		parent::prepare();

		$this->load();

		$search = $this->get('stdfilter-searchInput')->getValue();
		if ( $search ) {
			$this->where = $this->key . " LIKE :search";
			if ( $this->passThrough ) {
				$search = str_replace('*', '%', $search);
				$this->bind[':search'] = '%' . $search . '%';
			}
			else {
				$search = str_replace('*', '%', $search);
				$this->bind[':search'] = $search;
			}
		}
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$search = $this->get('stdfilter-searchInput')->getValue();
		if ( $search ) {
			$filter->andFind(':search', $this->key, Op::OP_CONTAINS);
			if ( $this->passThrough ) {
				$search = str_replace('*', '%', $search);
				$this->bind[':search'] = '%' . $search . '%';
			}
			else {
				$search = str_replace('*', '%', $search);
				$this->bind[':search'] = $search;
			}
		}
		return $this;
	}
}

