<?php
namespace Application\Form\Setting;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class EditmeForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 */
	public function __construct()
	{
		/* */
		parent::__construct('settingEdit');

		/* */
		$this->template = 'application/setting/editform';
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Css */
		$this->add(array(
			'name' => 'cssSheet',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Style'),
				'value_options' => $this->_getCssSheetSet(),
				'multiple' => false
			)
		));

		/* Language */
		$this->add(array(
			'name' => 'lang',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Language'),
				'value_options' => $this->_getLangSet(),
				'multiple' => false
			)
		));

		/* rbgateServerUrl */
		$this->add(array(
			'name' => 'rbgateServerUrl',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control',
				'size' => 60
			),
			'options' => array(
				'label' => tra('RbGate converter Url')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'cssSheet' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'lang' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'rbgateServerUrl' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array(
					array(
						'name' => 'uri',
						'options' => array(
							'allowAbsolute' => true,
							'allowRelative' => false
						)
					)
				)
			)
		);
	}

	/**
	 *
	 * @return string[]
	 */
	protected function _getCssSheetSet()
	{
		return array(
			null => 'default'
		);

		$cssSheetSet[null] = tra('default');
		$cssSheetSet = glob('./public/css/custom/*.css');
		$cssSheetSet = array_combine($cssSheetSet, $cssSheetSet);
	}

	/**
	 *
	 * @return array
	 */
	protected function _getLangSet()
	{
		$langMap = include ('data/lang/langmapping.php');
		return $langMap;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getLongDateFormatSet()
	{
		return array(
			null => tra('default'),
			'%d-%m-%Y %H:%M:%S' => '28-12-1969 13:45:20',
			'%d/%m/%Y %Hh%Mmn%Ss' => '28/12/1969 13h45mn20s',
			'%d/%m/%Y %Hh%M' => '28/12/1969 13h45',
			'%d/%m/%y %Hh%M' => '28/12/69 13h45',
			'%Y-%m-%d %H:%M:%S' => '1969-12-28 13:45:20',
			'%d-%m-%Y' => '29-12-1969',
			'%Y-%m-%d' => '1969-12-28'
		);
	}

	/**
	 *
	 * @return array
	 */
	protected function _getShortDateFormatSet()
	{
		return array(
			null => tra('default'),
			'%d-%m-%Y' => '28-12-1969',
			'%Y-%m-%d' => '1969-12-28',
			'%d-%m-%y' => '28-12-69',
			'%y-%m-%d' => '69-12-28'
		);
	}
} /* End of class */
