<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 */
class PaginatorForm extends Form
{

	/**
	 *
	 * @var string
	 */
	const SESSION_KEY = 'paginator';

	/**
	 *
	 * @var \Zend\Session\Container
	 */
	protected $sessionContainer;
	
	/**
	 *
	 * @var array
	 */
	protected $sessionDatas;
	
	/* @var integer */
	public $maxLimit = 1000;

	/* @var integer */
	public $limit = 50;

	/* @var integer */
	public $page = 1;

	/* @var integer */
	public $maxPage = 2;

	/* @var string */
	public $order = 'asc';

	/* @var string */
	public $orderby = 'id';

	/* @var string */
	public $namespace;

	/* @var array */
	public $limitSet = [
		5 => 5,
		20 => 20,
		50 => 50,
		100 => 100,
		200 => 200,
		500 => 500,
		1000 => 1000
	];

	/**
	 *
	 * @param string $name Name of key in the paginator session container where store parameters of this form
	 * @param string $formId        	
	 */
	public function __construct($name = 'paginator', $formId = 'paginator')
	{
		/* we want to ignore the name passed */
		parent::__construct('paginator');
		$this->setAttribute('id', $formId)
			->setAttribute('method', 'get')
			->setAttribute('class', 'form-inline');
		
		/* session */
		$this->sessionContainer = \Ranchbe::get()->getServiceManager()->getSessionManager()->containerRegistry->get(self::SESSION_KEY);
		$this->namespace = $name;
		if ( $this->sessionContainer->offsetExists($name) ) {
			$this->sessionDatas = $this->sessionContainer->offsetGet($name);
		}
		else {
			$this->sessionDatas = [];
		}
		
		/**/
		$inputFilter = new InputFilter();
		$this->setInputFilter($inputFilter);
		
		/* orderby */
		$this->add([
			'name' => 'orderby',
			'type' => \Zend\Form\Element\Hidden::class
		]);
		$inputFilter->add(array(
			'name' => 'orderby',
			'required' => false
		));
		
		/* order */
		$this->add([
			'name' => 'order',
			'type' => \Zend\Form\Element\Hidden::class,
			'attributes' => array(
				'value' => 'asc'
			)
		]);
		$inputFilter->add(array(
			'name' => 'order',
			'required' => false
		));

		/* paginator-limit */
		$this->add([
			'name' => 'paginator-limit',
			'type' => \Zend\Form\Element\Select::class,
			'options' => [
				'label' => 'Result per page'
			],
			'attributes' => [
				'options' => $this->limitSet,
				'class' => 'form-control form-control-paginator btn-group',
				'id' => 'paginator-limit',
				'title' => 'Select limit by page'
			]
		]);
		$inputFilter->add([
			'name' => 'paginator-limit',
			'required' => false
		]);

		/* paginator-page */
		$this->add(array(
			'name' => 'paginator-page',
			'type' => \Zend\Form\Element\Select::class,
			'options' => [
				'label' => 'Page'
			],
			'attributes' => [
				'options' => [
					1 => 1
				],
				'value' => 1,
				'class' => 'form-control form-control-paginator btn-group',
				'id' => 'paginator-page',
				'title' => 'Page num'
			]
		));
		$inputFilter->add([
			'name' => 'paginator-page',
			'required' => false
		]);

		/* paginator-next */
		$this->add([
			'name' => 'paginator-next',
			'type' => \Zend\Form\Element\Button::class,
			'options' => [
				'label' => '>',
				'value' => 'next'
			],
			'attributes' => [
				'class' => 'btn btn-default btn-paginator-next btn-group',
				'id' => 'paginator-next',
				'title' => 'Next page'
			]
		]);

		/* paginator-prev */
		$this->add([
			'name' => 'paginator-prev',
			'type' => \Zend\Form\Element\Button::class,
			'options' => [
				'label' => '<',
				'value' => 'prev'
			],
			'attributes' => [
				'class' => 'btn btn-default btn-paginator-prev btn-group',
				'id' => 'paginator-prev',
				'title' => 'Previous page'
			]
		]);
	}

	/**
	 *
	 * @param array $set        	
	 * @return \Application\Form\PaginatorForm
	 */
	public function setLimitset(array $set)
	{
		$set = array_combine($set, $set);
		$this->limitSet = $set;
		$this->get('paginator-limit')->setAttribute('options', $set);
		return $this;
	}

	/**
	 *
	 * @param integer $maxLimit
	 * @return \Application\Form\PaginatorForm
	 */
	public function setMaxLimit($maxLimit)
	{
		$this->maxLimit = $maxLimit;
		return $this;
	}

	/**
	 *
	 * @see Zend\Form.Form::prepare()
	 * @return \Application\Form\PaginatorForm
	 */
	public function prepare()
	{
		parent::prepare();

		$pPageElmt = $this->get('paginator-page');
		$limitElmt = $this->get('paginator-limit');

		$this->page = $pPageElmt->getValue();

		$limit = $limitElmt->getValue();
		if ( !$limit ) {
			$limit = $this->limit;
			$limitElmt->setValue($limit);
		}

		$this->lastPage = ceil($this->maxLimit / $limit);
		$lastPage = $this->lastPage;
		$minPage = 1;
		$maxPage = $this->lastPage;

		if ( $maxPage > 200 ) {
			$minPage = $this->page;
			$maxPage = $minPage + 200;
			($maxPage > $lastPage) ? $maxPage = $lastPage : null;
		}

		$options = [
			1 => 'First'
		];

		for ($i = $minPage; $i <= $maxPage; $i++) {
			$options[$i] = $i;
		}
		$options[$lastPage] = 'Last';

		$pPageElmt->setValueOptions($options);
		$order = $this->get('order')->getValue();
		$orderby = $this->get('orderby')->getValue();

		/* Default values */
		if ( $order ) $this->order = $order;
		if ( $orderby ) $this->orderby = $orderby;
		$this->limit = $limit;

		return $this;
	}

	/**
	 *
	 * @param \Zend\View\Model\ViewModel $view
	 * @return string Html string
	 */
	public function render($view)
	{
		$html = $view->form()->openTag($this);
		$html .= $view->formRow($this->get('orderby'));
		$html .= $view->formRow($this->get('order'));
		$html .= '<div class="form-group btn-group">';
		$html .= $view->formElement($this->get('paginator-limit'));
		$html .= $view->formElement($this->get('paginator-prev'));
		$html .= $view->formElement($this->get('paginator-page'));
		$html .= $view->formElement($this->get('paginator-next'));
		$html .= '</div>';
		$html .= $view->form()->closeTag();
		return $html;
	}

	/**
	 * Set view variables from paginator datas
	 *
	 * @param \Zend\View\Model\ViewModel $view
	 * @return \Application\Form\PaginatorForm
	 */
	public function bindToView($view)
	{
		$view->paginator = $this;
		$view->orderby = $this->orderby;
		$view->order = $this->order;
		return $this;
	}

	/**
	 * 
	 * @param \Rbplm\Dao\FilterInterface $filter
	 * @return \Application\Form\PaginatorForm
	 */
	public function bindToFilter(\Rbplm\Dao\FilterInterface $filter)
	{
		if ( !$this->isPrepared ) {
			$this->prepare();
		}

		if ( $this->isValid() ) {
			$filter->page($this->page, $this->limit);
			$filter->sort($this->orderby, $this->order);
			$this->bindFilter = $filter;
		}
		return $this;
	}

	/**
	 * 
	 * @return string
	 */
	public function toSql()
	{
		$sql = "";
		$offset = ($this->page - 1) * $this->limit;
		if ( $offset < 0 ) {
			$offset = 0;
		}
		if ( $this->orderby ) {
			$sql .= " ORDER BY $this->orderby $this->order";
		}
		$sql .= " LIMIT $this->limit OFFSET $offset";
		return $sql;
	}

	/**
	 * 
	 * @return \Application\Form\PaginatorForm
	 */
	public function save()
	{
		$this->sessionContainer->offsetSet($this->namespace, $this->getData());
		return $this;
	}

	/**
	 * 
	 * @return \Application\Form\PaginatorForm
	 * 
	 * @param array $options [default=>[], request=>[], session=>[]]
	 * 
	 */
	public function load(array $options = [])
	{
		/* request datas */
		(isset($options['request'])) ? $requestDatas = $options['request'] : $requestDatas = $_GET;

		/* session datas */
		(isset($options['session'])) ? $sessionDatas = $options['session'] : $sessionDatas = $this->sessionDatas;

		/* default datas */
		(isset($options['default'])) ? $defaultDatas = $options['default'] : $defaultDatas = [
			'orderby' => $this->orderby,
			'order' => $this->order,
			'paginator-limit' => $this->limit,
			'paginator-page' => $this->page
		];

		(isset($requestDatas['resetf'])) ? $resetFilter = $requestDatas['resetf'] : $resetFilter = false;
		if ( $resetFilter ) {
			$this->reset();
			return $this;
		}

		$data = array_merge($defaultDatas, $sessionDatas, $requestDatas);

		$this->setData($data);
		return $this;
	}

	/**
	 * 
	 * @return \Application\Form\PaginatorForm
	 */
	public function reset()
	{
		$this->sessionContainer->offsetSet($this->namespace, []);
		$this->setData([
			'order' => 'asc',
			'paginator-page' => 1,
			'paginator-limit' => 50
		]);
		return $this;
	}
}
