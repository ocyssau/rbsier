<?php
namespace Application\Form;

use Rbs\Space\Factory as DaoFactory;

/**
 * 
 *
 */
class ElementFactory
{

	/**
	 *
	 * @var DaoFactory
	 */
	protected $factory;

	/**
	 *
	 * @var DaoFactory
	 */
	protected $form;

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function __construct($form, $factory = null)
	{
		$this->form = $form;
		$this->factory = $factory;
	}

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function setDaoFactory($factory)
	{
		$this->factory = $factory;
	}

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function getForm()
	{
		return $this->form;
	}

	/**
	 *
	 * @param array $params
	 * @param string $type
	 * @return \Zend\Form\Form
	 */
	protected function _selectElemt($params, $type)
	{
		$form = $this->form;
		$attributes = [];
		$options = [];
		$filters = [];
		$multiple = false;
		$validators = [];

		(isset($params['name'])) ? $name = $params['name'] : $name = 'date';
		(isset($params['appName'])) ? $appName = $params['appName'] : $appName = $name;
		(isset($params['id']) && !empty($params['id'])) ? $attributes['id'] = $params['id'] : null;
		(isset($params['class'])) ? $attributes['class'] = $params['class'] . ' selectpicker' : $attributes['class'] = 'form-control selectpicker';
		(isset($params['size'])) ? $attributes['size'] = $params['size'] : null;
		(isset($params['disabled'])) ? $attributes['disabled'] = $params['disabled'] : null;

		if ( isset($params['multiple']) && $params['multiple'] == true ) {
			$attributes['multiple'] = true;
			$options['disable_inarray_validator'] = true;
			$multiple = true;
		}
		(isset($params['required'])) ? $required = (boolean)$params['required'] : $required = false;

		(isset($params['label'])) ? $options['label'] = $params['label'] : $options['label'] = $name;
		(isset($params['displayBoth'])) ? $options['displayBoth'] = (boolean)$params['displayBoth'] : null;
		(isset($params['maybenull'])) ? $options['maybenull'] = (boolean)$params['maybenull'] : null;
		(isset($params['livesearch'])) ? $attributes['data-live-search'] = (boolean)$params['livesearch'] : $attributes['data-live-search'] = true;
		(isset($params['selectSet'])) ? $options['options'] = $params['selectSet'] : null;

		(isset($params['validators'])) ? $validators = $params['validators'] : null;
		(isset($params['filters'])) ? $filters = $params['filters'] : null;

		($this->factory) ? $options['daoFactory'] = $this->factory : null;

		/* Construct object for normal select */
		$form->add(array(
			'type' => $type,
			'name' => $appName,
			'attributes' => $attributes,
			'options' => $options
		));

		/**/
		if ( !$required && $multiple ) {
			$filters[] = array(
				'name' => 'ToNull'
			);
		}

		/**
		 * getInputFilter not existing on fieldset
		 */
		if ( $form instanceof \Zend\Form\Form ) {
			$form->getInputFilter()->add(array(
				'name' => $appName,
				'required' => $required,
				'filters' => $filters,
				'validators' => $validators
			));
		}
		if ( method_exists($form, 'setInputFilterSpecification') ) {
			$inputFilterSpecification = $form->getInputFilterSpecification();
			$inputFilterSpecification[$appName] = array(
				'required' => $required,
				'filters' => $filters,
				'validators' => $validators
			);
			$form->setInputFilterSpecification($inputFilterSpecification);
		}

		return $form;
	}

	/**
	 * @param array $list
	 * @param array $params
	 */
	public function select($params)
	{
		$list = $params['list'];

		/* in put is string */
		if ( is_string($list) ) {
			$array = explode('#', $list);
			$list = array_combine($array, $array);
		}

		$params['selectSet'] = $list;
		return $this->_selectElemt($params, 'Zend\Form\Element\Select');
	}

	/**
	 *
	 * @param array $params
	 */
	public function selectDocumentVersion($params)
	{
		return $this->_selectElemt($params, 'Application\Form\Element\SelectVersion');
	}

	/**
	 *
	 * @param array $params
	 */
	function selectUser($params)
	{
		return $this->_selectElemt($params, 'Application\Form\Element\SelectUser');
	}

	/**
	 *
	 * @param array $params
	 */
	function selectPartner($params)
	{
		return $this->_selectElemt($params, 'Application\Form\Element\SelectPartner');
	}

	/**
	 *
	 * @param array $params
	 */
	public function selectProcess($params)
	{
		return $this->_selectElemt($params, 'Application\Form\Element\SelectProcess');
	}

	/**
	 *
	 * @param array $params
	 */
	public function selectProject($params)
	{
		return $this->_selectElemt($params, 'Application\Form\Element\SelectProject');
	}

	/**
	 * By default, closed containers are not include in the select.
	 * Set $params['withClosed']=true, to get closed container.
	 *
	 * @param array $params
	 */
	public function selectContainer($params)
	{
		return $this->_selectElemt($params, 'Application\Form\Element\SelectContainer');
	}

	/**
	 *
	 * @param array $params
	 */
	public function selectCategory($params)
	{
		return $this->_selectElemt($params, 'Application\Form\Element\SelectCategory');
	}

	/**
	 *
	 * @param array $params
	 */
	public function selectDoctype($params)
	{
		return $this->_selectElemt($params, 'Application\Form\Element\SelectDoctype');
	}

	/**
	 *
	 * @param array $params
	 */
	public function selectProperty($params)
	{
		return $this->_selectElemt($params, 'Application\Form\Element\SelectProperty');
	}

	/**
	 *
	 * @param array $params
	 */
	public function selectFromDb($params)
	{
		return $this->_selectElemt($params, 'Application\Form\Element\SelectFromDb');
	}

	/**
	 * $params = array(
	 * ["name"]=>string
	 * ["label"]=>string
	 * ["required"]=>string
	 * ["value"]=>string
	 *
	 *
	 * @param array $params
	 */
	public function selectDate($params)
	{
		$form = $this->form;
		$options = [];
		$attributes = [];

		(isset($params['name'])) ? $name = $params['name'] : $name = 'date';
		(isset($params['appName'])) ? $appName = $params['appName'] : $appName = $name;
		(isset($params['label'])) ? $options['label'] = $params['label'] : $options['label'] = $name;
		(isset($params['class'])) ? $attributes['class'] = $params['class'] : $attributes['class'] = 'form-input';
		(isset($params['format'])) ? $options['format'] = $params['format'] : $options['format'] = 'Y-m-d';
		(isset($params['required'])) ? $required = $params['required'] : $required = false;

		$form->add(array(
			'name' => $appName,
			'type' => 'Application\Form\Element\Date',
			'attributes' => $attributes,
			'options' => $options
		));

		/* getInputFilter not existing on fieldset */
		if ( $form instanceof \Zend\Form\Form ) {
			$form->getInputFilter()->add(array(
				'name' => $appName,
				'required' => $required,
				'filters' => array(
					array(
						'name' => 'DateTimeFormatter',
						'options' => array(
							'format' => $options['format']
						)
					)
				),
				'validators' => array()
			));
		}
	}

	/**
	 * $params = array(
	 * ["name"]=>string
	 * ["label"]=>string
	 * ["required"]=>string
	 * ["value"]=>string
	 *
	 *
	 * @param array $params		(isset($params['id'])) ? $attributes['id'] = $params['id'] : $attributes['id'] = $name;

	 */
	public function selectDateTime($params)
	{
		$form = $this->form;
		$options = [];
		$attributes = [];

		(isset($params['name'])) ? $name = $params['name'] : $name = 'date';
		(isset($params['appName'])) ? $appName = $params['appName'] : $appName = $name;
		(isset($params['label'])) ? $options['label'] = $params['label'] : $options['label'] = $name;
		(isset($params['class'])) ? $attributes['class'] = $params['class'] : $attributes['class'] = 'form-input';
		(isset($params['format'])) ? $options['format'] = $params['format'] : $options['format'] = 'Y-m-d H:i:s';
		(isset($params['required'])) ? $required = $params['required'] : $required = false;

		$form->add(array(
			'name' => $appName,
			'type' => 'Application\Form\Element\Date',
			'attributes' => $attributes,
			'options' => $options
		));

		/* getInputFilter not existing on fieldset */
		if ( $form instanceof \Zend\Form\Form ) {
			$form->getInputFilter()->add(array(
				'name' => $appName,
				'required' => $required,
				'filters' => array(
					array(
						'name' => 'DateTimeFormatter',
						'options' => array(
							'format' => $options['format']
						)
					)
				),
				'validators' => array()
			));
		}
	}

	/**
	 *
	 * @param array $property
	 */
	public function getText($params)
	{
		$form = $this->form;
		$validators = [];
		$filters = [];
		$options = [];
		$attributes = [];

		(isset($params['name'])) ? $name = $params['name'] : $name = 'date';
		(isset($params['appName'])) ? $appName = $params['appName'] : $appName = $name;
		(isset($params['id']) && !empty($params['id'])) ? $attributes['id'] = $params['id'] : null;
		(isset($params['label'])) ? $options['label'] = $params['label'] : $options['label'] = $name;
		(isset($params['class'])) ? $attributes['class'] = $params['class'] : $attributes['class'] = 'form-input';
		(isset($params['size'])) ? $attributes['size'] = $params['size'] : $attributes['size'] = 20;
		(isset($params['placeholder'])) ? $attributes['placeholder'] = $params['placeholder'] : $attributes['placeholder'] = tra('Input some text');
		(isset($params['value'])) ? $attributes['value'] = $params['value'] : null;

		(isset($params['disabled']) && $params['disabled']) ? $attributes['disabled'] = 'disabled' : null;
		(isset($params['required'])) ? $required = $params['required'] : $required = false;
		(isset($params['min'])) ? $min = $params['min'] : $min = null;
		(isset($params['max'])) ? $max = $params['max'] : $max = null;
		(isset($params['regex'])) ? $regex = $params['regex'] : $regex = null;
		(isset($params['regexMessage'])) ? $regexMessage = $params['regexMessage'] : $regexMessage = tra('invalid format');

		/* Construct Form Element */
		$form->add(array(
			'name' => $appName,
			'type' => 'Zend\Form\Element\Text',
			'attributes' => $attributes,
			'options' => $options
		));

		/* Add rule with regex */
		if ( $regex ) {
			$validators[] = array(
				'name' => 'Regex',
				'options' => array(
					'pattern' => '/' . $regex . '/i',
					'messages' => array(
						\Zend\Validator\Regex::NOT_MATCH => $regexMessage
					)
				)
			);
		}

		if ( $min && $max ) {
			$validators[] = array(
				'name' => 'StringLength',
				'option' => array(
					'min' => $min,
					'max' => $max
				)
			);
		}
		elseif ( $min ) {
			$validators[] = array(
				'name' => 'StringLength',
				'option' => array(
					'min' => $min
				)
			);
		}
		elseif ( $max ) {
			$validators[] = array(
				'name' => 'StringLength',
				'option' => array(
					'max' => $max
				)
			);
		}

		/* getInputFilter not existing on fieldset */
		if ( $form instanceof \Zend\Form\Form ) {
			$form->getInputFilter()->add(array(
				'name' => $appName,
				'required' => $required,
				'filters' => $filters,
				'validators' => $validators
			));
		}
	}

	/**
	 *
	 * @param array $property
	 */
	public function getLongtext($params)
	{
		$form = $this->form;
		$validators = [];
		$filters = [];
		$options = [];
		$attributes = [];

		(isset($params['name'])) ? $name = $params['name'] : $name = 'date';
		(isset($params['appName'])) ? $appName = $params['appName'] : $appName = $name;
		(isset($params['id']) && !empty($params['id'])) ? $attributes['id'] = $params['id'] : null;
		(isset($params['label'])) ? $options['label'] = $params['label'] : $options['label'] = $name;
		(isset($params['class'])) ? $attributes['class'] = $params['class'] : $attributes['class'] = 'form-input';
		(isset($params['size'])) ? $attributes['size'] = $params['size'] : $attributes['size'] = 20;
		(isset($params['placeholder'])) ? $attributes['placeholder'] = $params['placeholder'] : $attributes['placeholder'] = tra('Input some text');
		(isset($params['value'])) ? $attributes['value'] = $params['value'] : null;

		(isset($params['disabled']) && $params['disabled']) ? $attributes['disabled'] = 'disabled' : null;
		(isset($params['required'])) ? $required = $params['required'] : $required = false;
		(isset($params['min'])) ? $min = $params['min'] : $min = null;
		(isset($params['max'])) ? $max = $params['max'] : $max = null;
		(isset($params['regex'])) ? $regex = $params['regex'] : $regex = null;
		(isset($params['regexMessage'])) ? $regexMessage = $params['regexMessage'] : $regexMessage = tra('invalid format');

		/* Construct Form Element */
		$form->add(array(
			'name' => $appName,
			'type' => 'Zend\Form\Element\Text',
			'attributes' => $attributes,
			'options' => $options
		));

		/* Add rule with regex */
		if ( !empty($regex) ) {
			$validators[] = array(
				'name' => 'Regex',
				'options' => array(
					'pattern' => '/' . $regex . '/i',
					'messages' => array(
						\Zend\Validator\Regex::NOT_MATCH => $regexMessage
					)
				)
			);
		}

		if ( $min && $max ) {
			$validators[] = array(
				'name' => 'StringLength',
				'option' => array(
					'min' => $min,
					'max' => $max
				)
			);
		}
		elseif ( $min ) {
			$validators[] = array(
				'name' => 'StringLength',
				'option' => array(
					'min' => $min
				)
			);
		}
		elseif ( $max ) {
			$validators[] = array(
				'name' => 'StringLength',
				'option' => array(
					'max' => $max
				)
			);
		}

		/* getInputFilter not existing on fieldset */
		if ( $form instanceof \Zend\Form\Form ) {
			$form->getInputFilter()->add(array(
				'name' => $appName,
				'required' => $required,
				'filters' => $filters,
				'validators' => $validators
			));
		}
	}

	/**
	 *
	 * @param array $property
	 */
	public function getCheckbox($params)
	{
		$form = $this->form;
		$validators = [];
		$filters = [];
		$options = [];
		$attributes = [];

		(isset($params['name'])) ? $name = $params['name'] : $name = 'date';
		(isset($params['appName'])) ? $appName = $params['appName'] : $appName = $name;
		(isset($params['id']) && !empty($params['id'])) ? $attributes['id'] = $params['id'] : null;
		(isset($params['label'])) ? $options['label'] = $params['label'] : $options['label'] = $name;
		(isset($params['class'])) ? $attributes['class'] = $params['class'] : $attributes['class'] = 'form-input';
		(isset($params['size'])) ? $attributes['size'] = $params['size'] : $attributes['size'] = 20;
		(isset($params['placeholder'])) ? $attributes['placeholder'] = $params['placeholder'] : $attributes['placeholder'] = tra('Input some text');
		(isset($params['value'])) ? $attributes['value'] = $params['value'] : null;

		(isset($params['disabled']) && $params['disabled']) ? $attributes['disabled'] = 'disabled' : null;
		(isset($params['required'])) ? $required = $params['required'] : $required = false;

		/* Construct Form Element */
		$form->add(array(
			'name' => $appName,
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => $attributes,
			'options' => $options
		));

		/* getInputFilter not existing on fieldset */
		if ( $form instanceof \Zend\Form\Form ) {
			$form->getInputFilter()->add(array(
				'name' => $appName,
				'required' => $required,
				'filters' => $filters,
				'validators' => $validators
			));
		}
	}

	/**
	 *
	 * @param array $property
	 */
	public function getDigit($params)
	{
		$form = $this->form;
		$validators = [];
		$filters = [];
		$options = [];
		$attributes = [];

		(isset($params['name'])) ? $name = $params['name'] : $name = 'date';
		(isset($params['appName'])) ? $appName = $params['appName'] : $appName = $name;
		(isset($params['id']) && !empty($params['id'])) ? $attributes['id'] = $params['id'] : null;
		(isset($params['label'])) ? $options['label'] = $params['label'] : $options['label'] = $name;
		(isset($params['class'])) ? $attributes['class'] = $params['class'] : $attributes['class'] = 'form-input';
		(isset($params['placeholder'])) ? $attributes['placeholder'] = $params['placeholder'] : $attributes['placeholder'] = tra('Input some text');
		(isset($params['value'])) ? $attributes['value'] = $params['value'] : null;
		(isset($params['size'])) ? $attributes['size'] = $params['size'] : $attributes['size'] = 20;
		(isset($params['maxlength'])) ? $attributes['maxlength'] = $params['maxlength'] : $attributes['maxlength'] = $attributes['size'];

		(isset($params['disabled']) && $params['disabled']) ? $attributes['disabled'] = 'disabled' : null;
		(isset($params['required'])) ? $required = $params['required'] : $required = false;
		(isset($params['min'])) ? $min = $params['min'] : $min = null;
		(isset($params['max'])) ? $max = $params['max'] : $max = null;

		/* Construct Form Element */
		$form->add(array(
			'name' => $appName,
			'type' => 'Zend\Form\Element\Text',
			'attributes' => $attributes,
			'options' => $options
		));

		/* Add rule with regex */
		$validators[] = array(
			'name' => 'Digits'
		);

		if ( $min && $max ) {
			$validators[] = array(
				'name' => 'Between',
				'option' => array(
					'min' => $min,
					'max' => $max
				)
			);
		}
		elseif ( $min ) {
			$validators[] = array(
				'name' => 'GreaterThan',
				'option' => array(
					'min' => $min
				)
			);
		}
		elseif ( $max ) {
			$validators[] = array(
				'name' => 'LessThan',
				'option' => array(
					'max' => $max
				)
			);
		}
		/* getInputFilter not existing on fieldset */
		if ( $form instanceof \Zend\Form\Form ) {
			$form->getInputFilter()->add(array(
				'name' => $appName,
				'required' => $required,
				'filters' => $filters,
				'validators' => $validators
			));
		}
	}

	/**
	 *
	 * @param array $property
	 */
	public function element($property)
	{
		switch ($property['type']) {
			case 'text':
			case 'long_text':
				$this->getText($property);
				break;
			case 'html_area':
				$this->getLongText($property);
				break;

			case 'integer':
			case 'decimal':
				$this->getDigit($property);
				break;

			case 'partner':
				$this->selectPartner($property);
				break;

			case 'doctype':
				$this->selectDoctype($property);
				break;

			case 'documentVersion':
			case 'document_indice':
				$this->selectDocumentVersion($property);
				break;

			case 'container_indice':
				break;

			case 'user':
				$this->selectUser($property);
				break;

			case 'process':
				$this->selectProcess($property);
				break;

			case 'category':
				$this->selectCategory($property);
				break;

			case 'date':
				$this->selectDate($property);
				break;

			case 'select':
				$this->select($property);
				break;

			case 'selectFromDB':
				$this->selectFromDb($property);
				break;
			default:
		} /* switch */
	}
}
