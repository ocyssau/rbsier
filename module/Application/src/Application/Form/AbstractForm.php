<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 *
 *
 */
abstract class AbstractForm extends Form
{

	/**
	 *
	 */
	public function setUrlFromCurrentRoute($currentController)
	{
		$route = $currentController->getEvent()->getRouteMatch();
		$url = $currentController->url()->fromRoute($route->getMatchedRouteName(), $route->getParams());
		$this->setAttribute('action', $url);
	}
} /* End of class */
