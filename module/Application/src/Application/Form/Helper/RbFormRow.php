<?php
namespace Application\Form\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * 
 * @author ocyssau
 *
 */
class RbFormRow extends AbstractHelper
{

	/**
	 */
	protected $formLabelViewHelper;

	/**
	 */
	protected $formElementViewHelper;

	/**
	 */
	protected $formElementErrorsViewHelper;

	/**
	 * 
	 * @param \stdClass $formLabelViewHelper
	 * @param \stdClass $formElementViewHelper
	 * @param \stdClass $formElementErrorsViewHelper
	 */
	public function __construct($formLabelViewHelper, $formElementViewHelper, $formElementErrorsViewHelper)
	{
		$this->formLabelViewHelper = $formLabelViewHelper;
		$this->formElementViewHelper = $formElementViewHelper;
		$this->formElementErrorsViewHelper = $formElementErrorsViewHelper;
	}

	/**
	 * 
	 * @param \stdClass $formElement
	 * @return string
	 */
	public function __invoke($formElement)
	{
		$errors = $this->formElementErrorsViewHelper->__invoke($formElement);

		$html = '';
		$html .= '<fieldset class="">';
		$html .= $this->formLabelViewHelper->__invoke($formElement);
		$html .= $this->formElementViewHelper->__invoke($formElement);
		$html .= $errors;
		$html .= '</fieldset>';

		/*
		 $html .= '<fieldset class=" . ($errors ? 'has-danger' : '') . ">';
		 $html .= $this->formLabelViewHelper->__invoke($formElement);
		 $html .= $this->formElementViewHelper->__invoke($formElement);
		 $html .= $errors;
		 $html .= '</fieldset>';
		 */
		return $html;
	}
}