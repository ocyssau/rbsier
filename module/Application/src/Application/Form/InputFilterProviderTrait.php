<?php
namespace Application\Form;

/**
 *
 *
 */
trait InputFilterProviderTrait
{

	/**
	 * 
	 * @var array
	 */
	protected $inputFilterSpecification;

	/**
	 *
	 * @param array $inputFilter
	 * @return InputFilterProviderTrait
	 */
	public function setInputFilterSpecification($inputFilter)
	{
		$this->inputFilterSpecification = $inputFilter;
		return $this;
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return $this->inputFilterSpecification;
	}
} /* End of class */
