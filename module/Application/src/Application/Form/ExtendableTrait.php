<?php
namespace Application\Form;

/**
 *
 *
 */
trait ExtendableTrait
{

	/**
	 * @var array
	 */
	public $extended;

	/**
	 *
	 */
	public function getElemtFactory($factory = null)
	{
		if ( !isset($this->elemtFactory) ) {
			$this->elemtFactory = new ElementFactory($this, $factory);
		}
		return $this->elemtFactory;
	}

	/**
	 * @param array $extended In App sementic
	 */
	public function setExtended(array $extended)
	{
		$hydrator = $this->getHydrator();
		$elemtFactory = $this->getElemtFactory();

		/* check if hydrator is a \Rbs\Model\Hydrator\Extendable */
		if ( !is_callable([
			$hydrator,
			'setExtended'
		]) ) {
			throw new \Exception('hydrator must have a setExtended method');
		}
		$hydrator->setExtended($extended);

		foreach( $extended as $attributes ) {
			$elemtFactory->element($attributes);
			$this->extended[$attributes['name']] = $attributes['appName'];
		}

		return $this;
	}
} /* End of class */
