<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Application\Form\Element\SelectFromDb as BaseElement;
use Rbplm\Ged\Category;

/**
 * 
 *
 */
class SelectCategory extends BaseElement
{

	/**
	 * 
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['class'] = $this->attributes['class'] . ' rb-select-category';

		/* add local default options */
		$this->localOptions['containerId'] = null;
		$this->localOptions['mode'] = 'inherited';
		$this->localOptions['fullname'] = true;
		$this->localOptions['maybenull'] = true;

		/* */
		$this->attributes['data-width'] = "300px";
		$this->attributes['data-live-search'] = true;
		$this->attributes['data-selected-text-format'] = 'count > 1';
		$this->attributes['title'] = tra('select a category...');
		$this->setUnselectedValue('...');
	}

	/**
	 * Set options for an element. Accepted options are:
	 * - label: label to associate with the element
	 * - label_attributes: attributes to use when the label is rendered
	 * - value_options: list of values and labels for the select options
	 * _ empty_option: should an empty option be prepended to the options ?
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$options = $this->options;

		if ( $options['daoFactory'] ) {
			if ( $options['mode'] == 'inherited' ) {
				$this->loadInheritedCategory();
			}
			elseif ( $options['mode'] == 'load' ) {
				$this->load();
			}
			elseif ( $options['mode'] == 'usedbycontainer' ) {
				$this->loadUsedByContainer();
			}
		}
		return $this;
	}

	/**
	 * Select Categories recursivly from project and container settings
	 * @param array $params
	 */
	public function loadInheritedCategory()
	{
		$factory = $this->options['daoFactory'];
		$containerId = $this->options['containerId'];
		$dao = $factory->getDao(\Rbplm\Ged\Category::$classId);

		if ( !$containerId ) {
			$select = array(
				$dao->toSys('id') . ' AS id',
				$dao->toSys('name') . ' AS label',
				$dao->toSys('designation') . ' AS title'
			);
			$list = $factory->getList(Category::$classId);
			$list->select($select);
			$list->load("1=1 ORDER BY label LIMIT 1000");
			$stmt = $list->getStmt();
		}
		else {
			$select = array(
				'DISTINCT child.' . $dao->toSys('id') . ' AS id',
				'child.' . $dao->toSys('name') . ' AS label',
				'child.' . $dao->toSys('designation') . ' AS title'
			);
			$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Category::$classId);
			$stmt = $lnkDao->getInheritsFromParentId($containerId, $select, '1=1 ORDER BY label LIMIT 1000');
		}

		$selectSet = $this->stmtToSelectSet($stmt);

		$this->setValueOptions($selectSet);

		return $this;
	}

	/**
	 *
	 * @param array $params
	 */
	public function load()
	{
		$factory = $this->options['daoFactory'];
		$containerId = $this->options['containerId'];
		$dao = $factory->getDao(\Rbplm\Ged\Category::$classId);

		if ( !$containerId ) {
			$select = array(
				$dao->toSys('id') . ' AS id',
				$dao->toSys('name') . ' AS label',
				$dao->toSys('designation') . ' AS title'
			);
			$list = $factory->getList(Category::$classId);
			$list->select($select);
			$list->load("1=1 ORDER BY label LIMIT 1000");
			$stmt = $list->getStmt();
		}
		else {
			$select = array(
				'child.' . $dao->toSys('id') . ' AS id',
				'child.' . $dao->toSys('name') . ' AS label',
				'child.' . $dao->toSys('designation') . ' AS title'
			);
			$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Category::$classId);
			$stmt = $lnkDao->getChildrenFromId($containerId, $select);
		}

		$selectSet = $this->stmtToSelectSet($stmt);
		$this->setValueOptions($selectSet);
		return $this;
	}

	/**
	 *
	 * @param array $params
	 */
	public function loadUsedByContainer()
	{
		$factory = $this->options['daoFactory'];
		$containerId = $this->options['containerId'];
		$spacename = strtolower($factory->getName());
		$dao = $factory->getDao(Category::$classId);

		$select = array(
			'cat.' . $dao->toSys('id') . ' AS id',
			'cat.' . $dao->toSys('name') . ' AS label',
			'cat.' . $dao->toSys('designation') . ' AS title'
		);
		$stmt = $dao->getUsedByContainer($containerId, $spacename, $select);
		$selectSet = $this->stmtToSelectSet($stmt);
		$this->setValueOptions($selectSet);
		return $this;
	}
}

