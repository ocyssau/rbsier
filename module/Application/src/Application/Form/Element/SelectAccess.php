<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Zend\Form\Element\Select as BaseElement;
use Rbplm\Ged\AccessCode;

/**
 * 
 *
 */
class SelectAccess extends BaseElement
{

	/**
	 *
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['class'] = 'form-control selectpicker rb-select rb-select-access';
		$this->attributes['title'] = tra('select access code...');
	}

	/**
	 * Set options for an element. Accepted options are:
	 * - label: label to associate with the element
	 * - label_attributes: attributes to use when the label is rendered
	 * - value_options: list of values and labels for the select options
	 * _ empty_option: should an empty option be prepended to the options ?
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$options = $this->options;

		/* Access */
		$selectSet = array(
			'free' => tra('Free'), // Not 0 to prevent confusion with null values == problem in filters selection
			AccessCode::CHECKOUT => tra('CheckedOut'),
			AccessCode::INWORKFLOW => tra('InWorkflow'),
			AccessCode::CANCELED => tra('Canceled'),
			AccessCode::VALIDATE => tra('Validated'),
			AccessCode::LOCKED => tra('Locked'),
			AccessCode::SUPPRESS => tra('Marked to suppress'),
			AccessCode::HISTORY => tra('Previous Version'),
			AccessCode::ARCHIVE => tra('Archived')
		);
		$this->setValueOptions($selectSet);

		return $this;
	}
}
