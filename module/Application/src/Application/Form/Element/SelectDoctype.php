<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Application\Form\Element\SelectFromDb as BaseElement;
use Rbplm\Ged\Doctype;

/**
 * 
 *
 */
class SelectDoctype extends BaseElement
{

	/**
	 * 
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['class'] = $this->attributes['class'] . ' rb-select-doctype';

		/* add local default options */
		$this->localOptions['containerId'] = null;
		$this->localOptions['dbsortBy'] = 'name';
		$this->localOptions['dbfilter'] = '1=1';
		$this->localOptions['mode'] = 'inherited';
		$this->localOptions['fullname'] = true;
		$this->localOptions['maybenull'] = true;

		/* */
		$this->attributes['data-width'] = "300px";
		$this->attributes['data-live-search'] = true;
		$this->attributes['data-selected-text-format'] = 'count > 1';
		$this->attributes['title'] = tra('select doctype');
		$this->setUnselectedValue('...');
	}

	/**
	 * Set options for an element. Accepted options are:
	 * - label: label to associate with the element
	 * - label_attributes: attributes to use when the label is rendered
	 * - value_options: list of values and labels for the select options
	 * _ empty_option: should an empty option be prepended to the options ?
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$options = $this->options;

		if ( $options['daoFactory'] ) {
			if ( $options['mode'] == 'load' ) {
				$this->load();
			}
			elseif ( $options['mode'] == 'usedbycontainer' ) {
				$this->loadUsedByContainer();
			}
		}
		return $this;
	}

	/**
	 *
	 * @param array $params
	 */
	public function load()
	{
		$factory = $this->options['daoFactory'];
		$containerId = $this->options['containerId'];
		$trans = $factory->getDao(Doctype::$classId)->getTranslator();
		
		if ( !$containerId ) {
			$select = array(
				$trans->toSys('id') . ' AS id',
				$trans->toSys('name') . ' AS label',
				$trans->toSys('designation') . ' AS title'
			);
			$list = $factory->getList(Doctype::$classId);
			$list->select($select);
			$list->load("1=1 ORDER BY label LIMIT 1000");
			$stmt = $list->getStmt();
			
			
		}
		else {
			$select = array(
				'child.' . $trans->toSys('id') . ' AS id',
				'child.' . $trans->toSys('name') . ' AS label',
				'child.' . $trans->toSys('designation') . ' AS title'
			);
			$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Doctype::$classId);
			$stmt = $lnkDao->getChildrenFromId($containerId, $select);
		}

		$selectSet = $this->stmtToSelectSet($stmt);
		$this->setValueOptions($selectSet);

		return $this;
	}

	/**
	 *
	 * @param array $params
	 */
	public function loadUsedByContainer()
	{
		$factory = $this->options['daoFactory'];
		$containerId = $this->options['containerId'];
		$spacename = strtolower($factory->getName());
		$dao = $factory->getDao(Doctype::$classId);

		$select = array(
			'dt.' . $dao->toSys('id') . ' AS id',
			'dt.' . $dao->toSys('name') . ' AS label',
			'dt.' . $dao->toSys('designation') . ' AS title'
		);
		$stmt = $dao->getUsedByContainer($containerId, $spacename, $select);
		$selectSet = $this->stmtToSelectSet($stmt);
		$this->setValueOptions($selectSet);
		return $this;
	}
}

