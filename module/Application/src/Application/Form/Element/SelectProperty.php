<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

//use Zend\Form\Element\Select as BaseElement;
use Application\Form\Element\SelectFromDb as BaseElement;
use Rbs\Extended\Property;
use Rbplm\Ged\Document;

/**
 * 
 *
 */
class SelectProperty extends BaseElement
{

	/**
	 * 
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['class'] = $this->attributes['class'] . ' rb-select-category';

		/* add local default options */
		$this->localOptions['daoFactory'] = null;
		$this->localOptions['containerId'] = null;
		$this->localOptions['mode'] = 'inherited';
		$this->setUnselectedValue('...');
	}

	/**
	 * Set options for an element. Accepted options are:
	 * - label: label to associate with the element
	 * - label_attributes: attributes to use when the label is rendered
	 * - value_options: list of values and labels for the select options
	 * _ empty_option: should an empty option be prepended to the options ?
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$options = $this->options;

		$this->load($options['daoFactory']);
		return $this;
	}

	/**
	 *
	 * @param array $params
	 */
	public function load()
	{
		$factory = $this->options['daoFactory'];
		$dao = $factory->getDao(Property::$classId);
		$filter = $this->options['dbfilter'];

		$select = array(
			$dao->toSys('id') . ' AS id',
			$dao->toSys('label') . ' AS label',
			$dao->toSys('description') . ' AS title'
		);
		$list = $factory->getList(Property::$classId);
		$list->select($select);

		$documentCid = Document\Version::$classId;
		$filter .= " AND extendedCid='$documentCid'";
		$list->load($filter . ' ORDER BY label ASC LIMIT 1000');
		$stmt = $list->getStmt();

		$selectSet = $this->stmtToSelectSet($stmt);
		$this->setValueOptions($selectSet);
		return $this;
	}
}

