<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Zend\Form\Element\Text as BaseElement;

/**
 * 
 *
 */
class Date extends BaseElement
{

	/**
	 *
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['placeholder'] = tra('Click to select date');
	}

	/**
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$options = $this->options;
		return $this;
	}

	/**
	 */
	public function setAttributes($arrayOrTraversable)
	{
		parent::setAttributes($arrayOrTraversable);
		$this->attributes['class'] = $this->attributes['class'] . ' datepicker';
		return $this;
	}
}
