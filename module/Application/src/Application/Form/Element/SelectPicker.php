<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Zend\Form\Element\Select as BaseElement;

/**
 * 
 *
 */
class SelectPicker extends BaseElement
{

	/**
	 * 
	 * @var array
	 */
	protected $localOptions = [
		'maybenull' => false,
		'livesearch' => true
	];

	/**
	 * 
	 * @var string
	 */
	protected $classAttr = 'form-control selectpicker rb-select';

	/**
	 * 
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['multiple'] = false;
		$this->attributes['class'] = $this->classAttr;
		$this->attributes['data-live-search'] = true;
		$this->attributes['data-selected-text-format'] = 'count > 1';
		$this->attributes['data-size'] = 10;
		$this->setUnselectedValue('...');
	}

	/**
	 * Set options for an element. Accepted options are:
	 * - label: label to associate with the element
	 * - label_attributes: attributes to use when the label is rendered
	 * - value_options: list of values and labels for the select options
	 * _ empty_option: should an empty option be prepended to the options ?
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$this->options = array_merge($this->localOptions, $this->options);
		if ( $this->options['livesearch'] == false ) {
			$this->attributes['data-live-search'] = false;
		}
		else {
			$this->attributes['data-live-search'] = true;
		}
		return $this;
	}

	/**
	 */
	public function setAttributes($arrayOrTraversable)
	{
		parent::setAttributes($arrayOrTraversable);
		if ( isset($arrayOrTraversable['class']) ) {
			$this->attributes['class'] = $this->attributes['class'] . ' ' . $this->classAttr;
		}
		return $this;
	}
}
