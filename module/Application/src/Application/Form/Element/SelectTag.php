<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Application\Form\Element\SelectFromDb as BaseElement;
use Rbplm\Ged\Document;
use Rbs\Dao\Sier\Filter;
use Rbplm\Dao\Filter\Op;

/**
 * 
 *
 */
class SelectTag extends BaseElement
{

	/**
	 * 
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['class'] = $this->attributes['class'] . ' rb-select-tag';

		/* add local default options */
		$this->localOptions['daoFactory'] = null;
		$this->localOptions['containerId'] = null;
		$this->localOptions['mode'] = 'usedbycontainer';
		$this->localOptions['fullname'] = true;
		$this->localOptions['maybenull'] = true;
		$this->localOptions['tags'] = null;

		/* */
		$this->attributes['data-width'] = "120px";
		$this->attributes['data-live-search'] = true;
		$this->attributes['data-selected-text-format'] = 'count > 1';
		$this->attributes['title'] = tra('select tags...');
		$this->setUnselectedValue('...');
	}

	/**
	 * Set options for an element. Accepted options are:
	 * - label: label to associate with the element
	 * - label_attributes: attributes to use when the label is rendered
	 * - value_options: list of values and labels for the select options
	 * _ empty_option: should an empty option be prepended to the options ?
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$options = $this->options;

		if ( $options['daoFactory'] ) {
			$this->load($options['tags']);
		}
		return $this;
	}

	/**
	 * set some tags to be present in documents return
	 * 
	 * @param array $selected
	 */
	public function load($selectedTags = null)
	{
		$factory = $this->options['daoFactory'];
		$containerId = $this->options['containerId'];

		$list = $factory->getList(Document\Version::$classId);
		$filter = new Filter('', false);
		$filter->select(array(
			'id',
			'tags'
		));
		$filter->page(1, 1000);

		/* */
		$filter->andfind('', 'tags', Op::NOTNULL);
		if ( $selectedTags ) {
			foreach( $selectedTags as $selectedTag ) {
				$selectedTag = trim($selectedTag);
				if ( $selectedTag ) {
					$filter->andfind($selectedTag, 'tags', Op::FINDINSET);
				}
			}
		}
		if ( $containerId ) {
			$dao = $factory->getDao(Document\Version::$classId);
			$filter->andfind($containerId, $dao->toSys('parentId'), Op::EQUAL);
		}

		$list->load($filter);

		$filtered = array();
		foreach( $list as $item ) {
			$exploded = explode(',', $item['tags']);
			$filtered = array_merge($filtered, $exploded);
		}
		$filtered = array_unique($filtered);

		$selectSet = [];
		foreach( $filtered as $tag ) {
			$selectSet[$tag] = $tag;
		}
		$this->setValueOptions($selectSet);

		return $this;
	}
}

