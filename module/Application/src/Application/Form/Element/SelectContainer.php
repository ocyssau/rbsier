<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Application\Form\Element\SelectFromDb as BaseElement;

/**
 * 
 *
 */
class SelectContainer extends BaseElement
{

	/**
	 *
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['class'] = $this->attributes['class'] . ' rb-select-container';
		$this->attributes['data-live-search'] = true;
		$this->attributes['data-selected-text-format'] = 'count > 1';
		$this->attributes['title'] = tra('select container');

		/* add local default options */
		$this->localOptions['parentId'] = null;
		$this->localOptions['dbsortBy'] = 'name';
		$this->localOptions['dbfilter'] = '1=1';
		$this->localOptions['withClosed'] = false;
	}

	/**
	 * Set options for an element. Accepted options are:
	 * - label: label to associate with the element
	 * - label_attributes: attributes to use when the label is rendered
	 * - value_options: list of values and labels for the select options
	 * _ empty_option: should an empty option be prepended to the options ?
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$options = $this->options;
		$this->load();
		return $this;
	}

	/**
	 *
	 * @param array $params
	 */
	public function load()
	{
		$factory = $this->options['daoFactory'];
		$projectId = $this->options['parentId'];
		$dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
		$dbtable = $factory->getTable(\Rbplm\Org\Workitem::$classId);
		$bind = [];

		$options = $this->options;
		$dbfilter = $options['dbfilter'];
		$sortBy = $options['dbsortBy'];
		$sortOrder = $options['dbsortOrder'];
		$dblimit = $options['dblimit'];

		if ( $options['withClosed'] == true ) {
			$dbfilter .= ' AND acode=0';
		}

		if ( $projectId ) {
			$dbfilter .= ' AND ' . $dao->toSys('parentId') . '=:parentId';
			$bind[':parentId'] = $projectId;
		}

		$select = implode(',', array(
			$dao->toSys('id') . ' AS id',
			$dao->toSys('number') . ' AS label',
			$dao->toSys('designation') . ' AS title',
			$dao->toSys('number') . ' AS number'
		));

		$connexion = \Rbplm\Dao\Connexion::get();
		$stmt = $connexion->prepare("SELECT $select FROM $dbtable WHERE $dbfilter ORDER BY $sortBy $sortOrder LIMIT $dblimit");
		$stmt->execute($bind);
		$selectSet = $this->stmtToSelectSet($stmt);
		$this->setValueOptions($selectSet);
		return $this;
	}
}
