<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Application\Form\Element\SelectFromDb as BaseElement;
use Rbs\People\Somebody;
use Rbs\Space\Factory as DaoFactory;

/**
 * 
 *
 */
class SelectPartner extends BaseElement
{

	/**
	 *
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['class'] = $this->attributes['class'] . ' rb-select-partner';
		$this->localOptions['dbfilter'] = '1=1';
	}

	/**
	 * Set options for an element. Accepted options are:
	 * - label: label to associate with the element
	 * - label_attributes: attributes to use when the label is rendered
	 * - value_options: list of values and labels for the select options
	 * _ empty_option: should an empty option be prepended to the options ?
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$options = $this->options;
		$this->load();
		return $this;
	}

	/**
	 *
	 * @param array $params
	 */
	public function load()
	{
		(isset($this->options['daoFactory'])) ? $factory = $this->options['daoFactory'] : $factory = DaoFactory::get();
		$list = $factory->getList(Somebody::$classId);
		$dao = $factory->getDao(Somebody::$classId);

		$options = $this->options;
		$dbfilter = $options['dbfilter'];

		$fnKey = $dao->toSys('firstname');
		$lnKey = $dao->toSys('lastname');
		$select = array(
			$dao->toSys('id') . ' AS id',
			"CONCAT(COALESCE($fnKey, ''),' ', COALESCE($lnKey, '')) AS label",
			"'' AS title"
		);

		$list->select($select);
		$list->load($dbfilter . " ORDER BY label ASC LIMIT 1000");
		$stmt = $list->getStmt();

		$selectSet = $this->stmtToSelectSet($stmt);
		$this->setValueOptions($selectSet);
		return $this;
	}
}
