<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Application\Form\Element\SelectPicker as BaseElement;

/**
 * 
 *
 */
class SelectFromDb extends BaseElement
{

	/**
	 * 
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);

		$this->localOptions = array_merge($this->localOptions, [
			'daoFactory' => null,
			'returnName' => false,
			'fullname' => false,
			'displayBoth' => false,
			'dbFieldForName' => '',
			'dbFieldForValue' => '',
			'dbFieldForTitle' => '',
			'dbtable' => '',
			'dbfilter' => '1=1',
			'dbsortBy' => '',
			'dbsortOrder' => 'ASC',
			'dblimit' => 1000
		]);
	}

	/**
	 *
	 * @param array $params
	 */
	public function load()
	{
		$options = $this->options;
		$dbFieldForName = $options['dbFieldForName'];
		$dbFieldForValue = $options['dbFieldForValue'];
		$dbFieldForTitle = $options['dbFieldForTitle'];
		$dbtable = $options['dbtable'];
		$dbfilter = $options['dbfilter'];
		$sortBy = $options['dbsortBy'];
		$sortOrder = $options['dbsortOrder'];
		$dblimit = $options['dblimit'];

		$select = implode(', ', array(
			$dbFieldForValue . ' AS id',
			$dbFieldForName . ' AS label',
			$dbFieldForTitle . ' AS title'
		));

		$connexion = \Rbplm\Dao\Connexion::get();
		$stmt = $connexion->query("SELECT $select FROM $dbtable WHERE $dbfilter ORDER BY $sortBy $sortOrder LIMIT $dblimit");
		$selectSet = $this->stmtToSelectSet($stmt);
		$this->setValueOptions($selectSet);
		return $this;
	}

	/**
	 * 
	 * @param \PDOStatement $stmt
	 */
	protected function stmtToSelectSet(\PDOStatement $stmt)
	{
		$maybenull = $this->options['maybenull'];
		$displayBoth = $this->options['displayBoth'];
		$fullname = $this->options['fullname'];
		$returnName = $this->options['returnName'];
		($maybenull) ? $selectSet = [
			null => $this->getUnselectedValue()
		] : $selectSet = [];

		while( $item = $stmt->fetch(\PDO::FETCH_ASSOC) ) {
			$itemLabel = $item['label'];
			$id = $item['id'];
			if ( $displayBoth ) {
				$itemLabel = $id . '-' . $itemLabel;
			}
			if ( $fullname && isset($item['title']) ) {
				$itemLabel = $itemLabel . ' (' . $item['title'] . ')';
			}
			if ( $returnName ) {
				$selectSet[$itemLabel] = $itemLabel;
			}
			else {
				$selectSet[$id] = $itemLabel;
			}
		}
		return $selectSet;
	}
}

