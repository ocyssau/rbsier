<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Application\Form\Element\SelectFromDb as BaseElement;
use Rbplm\Ged\Category;

/**
 * 
 *
 */
class SelectLifestage extends BaseElement
{

	/**
	 * 
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['class'] = $this->attributes['class'] . ' rb-select-lifestage';

		/* add local default options */
		$this->localOptions['daoFactory'] = null;
		$this->localOptions['containerId'] = null;
		$this->localOptions['mode'] = 'inherited';
		$this->localOptions['maybenull'] = true;

		/* */
		$this->attributes['data-width'] = "120px";
		$this->attributes['data-live-search'] = true;
		$this->attributes['data-selected-text-format'] = 'count > 1';
		$this->attributes['title'] = tra('select a category...');
		$this->setUnselectedValue('...');
	}

	/**
	 * Set options for an element. Accepted options are:
	 * - label: label to associate with the element
	 * - label_attributes: attributes to use when the label is rendered
	 * - value_options: list of values and labels for the select options
	 * _ empty_option: should an empty option be prepended to the options ?
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$options = $this->options;

		if ( $options['daoFactory'] ) {
			if ( $options['mode'] == 'load' ) {
				$this->load($options['daoFactory']);
			}
			elseif ( $options['mode'] == 'usedbycontainer' ) {
				$this->loadUsedByContainer($options['daoFactory']);
			}
		}
		return $this;
	}

	/**
	 *
	 * @param array $params
	 */
	public function load()
	{
		$factory = $this->options['daoFactory'];
		$dao = $factory->getDao(\Rbplm\Ged\Category::$classId);

		$select = array(
			$dao->toSys('id') . ' AS id',
			$dao->toSys('name') . ' AS label',
			$dao->toSys('designation') . ' AS title'
		);
		$list = $factory->getList(Category::$classId);
		$list->select($select);
		$list->load("1=1 ORDER BY label LIMIT 1000");
		$stmt = $list->getStmt();
		$selectSet = $this->stmtToSelectSet($stmt);
		$this->setValueOptions($selectSet);
		return $this;
	}

	/**
	 *
	 * @param array $params
	 */
	public function loadUsedByContainer()
	{
		$factory = $this->options['daoFactory'];
		$containerId = $this->options['containerId'];
		$dao = $factory->getDao(\Rbplm\Ged\Document\Version::$classId);
		$documentTable = $factory->getTable(\Rbplm\Ged\Document\Version::$classId);
		$filter = $dao->toSys('parentId') . '=:containerId ORDER BY ' . $dao->toSys('lifeStage') . ' ASC';
		$lifeStageAsSys = $dao->toSys('lifeStage');
		$select = array(
			'doc.' . $lifeStageAsSys . ' AS id',
			'doc.' . $lifeStageAsSys . ' AS label',
			'doc.' . $lifeStageAsSys . ' AS title'
		);
		$selectStr = implode(',', $select);
		$bind = [
			':containerId' => $containerId
		];
		$sql = "SELECT DISTINCT $selectStr FROM $documentTable AS doc WHERE $filter";
		$stmt = $dao->getConnexion()->prepare($sql);
		$stmt->execute($bind);
		$selectSet = $this->stmtToSelectSet($stmt);
		$this->setValueOptions($selectSet);
		return $this;
	}
}

