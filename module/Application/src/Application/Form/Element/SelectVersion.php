<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
namespace Application\Form\Element;

use Application\Form\Element\SelectFromDb as BaseElement;

/**
 * 
 *
 */
class SelectVersion extends BaseElement
{

	/**
	 *
	 * @param string $name
	 * @param array $options
	 */
	public function __construct($name = null, $options = [])
	{
		parent::__construct($name, $options);
		$this->attributes['class'] = $this->attributes['class'] . ' rb-select-version';
		$this->localOptions['dbsortBy'] = 'indice_id';
		$this->localOptions['dbfilter'] = '1=1';
	}

	/**
	 * Set options for an element. Accepted options are:
	 * - label: label to associate with the element
	 * - label_attributes: attributes to use when the label is rendered
	 * - value_options: list of values and labels for the select options
	 * _ empty_option: should an empty option be prepended to the options ?
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
		$options = $this->options;

		$this->options['dbFieldForValue'] = 'indice_id';
		$this->options['dbFieldForName'] = 'indice_value';
		$this->options['dbFieldForTitle'] = 'indice_value';
		$this->options['dbtable'] = \Rbs\Ged\Document\IndiceDao::$table;
		$this->load();
		return $this;
	}
}
