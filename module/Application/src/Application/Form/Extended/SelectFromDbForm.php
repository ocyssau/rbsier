<?php
namespace Application\Form\Extended;

/**
 * 
 *
 */
class SelectFromDbForm extends PropertyForm
{

	/**
	 *
	 * @param string $name
	 */
	public function __construct($name = 'selectFromDb')
	{
		parent::__construct($name);
		//$this->template = 'application/extendedproperties/selectform.phtml';

		$this->additionalsElements = array(
			'size',
			'dbtable',
			'dbFieldForName',
			'dbFieldForValue',
			'dbfilter',
			'return'
		);

		$this->add(array(
			'name' => 'size',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type' => 'number',
				'step' => '1',
				'min' => '5',
				'placeholder' => 'Size'
			),
			'options' => array(
				'label' => 'Size'
			)
		));

		$this->add(array(
			'name' => 'dbtable',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Db Table',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Db Table'
			)
		));

		$this->add(array(
			'name' => 'dbFieldForName',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'dbFieldForName',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'dbFieldForName'
			)
		));

		$this->add(array(
			'name' => 'dbFieldForValue',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'dbFieldForValue',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'dbFieldForValue'
			)
		));

		$this->add(array(
			'name' => 'dbfilter',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'dbfilter',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'dbfilter'
			)
		));

		$this->add(array(
			'name' => 'return',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type' => 'select',
				'placeholder' => 'return',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'return',
				'value_options' => array(
					'value' => 'The Value',
					'name' => 'The Name'
				),
				'empty_option' => 'Please, select the field to return in application'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'options' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			)
		);
	}
}
