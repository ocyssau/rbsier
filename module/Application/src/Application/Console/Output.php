<?php
namespace Application\Console;

#use Zend\Console\Console;
#use Zend\Console\ColorInterface;

/**
 *
 * Cli tools non dependant of Zend\Console
 * 
 * @see Application\Console\OutputTest
 * 
 * Colors on console :
 * 
 * Black 0;30
 * Blue 0;34
 * Green 0;32
 * Cyan 0;36
 * Red 0;31
 * Purple 0;35
 * Brown 0;33
 * Light Gray 0;37
 * Dark Gray 1;30
 * Light Blue 1;34
 * Light Green 1;32
 * Light Cyan 1;36
 * Light Red 1;31
 * Light Purple 1;35
 * Yellow 1;33
 * White 1;37 
 * 
 * 
 *
 */
class Output
{

	/**
	 *
	 */
	public static function console($string, $eof = CRLF)
	{
		#Console::getInstance()->writeLine($string, ColorInterface::RESET);
		echo "\033[0m" . $string . $eof;
	}

	/**
	 *
	 */
	public static function red($string, $eof = CRLF)
	{
		#Console::getInstance()->writeLine($string, ColorInterface::RED);
		echo "\033[31m" . $string . $eof;
	}

	/**
	 *
	 */
	public static function green($string, $eof = CRLF)
	{
		#Console::getInstance()->writeLine($string, ColorInterface::GREEN);
		echo "\033[32m" . $string . $eof;
	}

	/**
	 *
	 */
	public static function yellow($string, $eof = CRLF)
	{
		#Console::getInstance()->writeLine($string, ColorInterface::YELLOW);
		echo "\033[33m" . $string . $eof;
	}

	/**
	 *
	 */
	public static function blue($string, $eof = CRLF)
	{
		#Console::getInstance()->writeLine($string, ColorInterface::BLUE);
		echo "\033[34m" . $string . $eof;
	}

	/**
	 * @param \Exception $e
	 */
	public static function exception(\Exception $e, $eof = CRLF)
	{
		self::yellow(str_repeat('-', 80), $eof);
		self::red('A EXCEPTION ERROR IS OCCURED :', $eof);
		self::yellow(str_repeat('-', 40), $eof);

		echo sprintf('Message : %s', $e->getMessage()) . $eof;
		echo sprintf('Error Code : %S', $e->getCode()) . $eof;
		echo sprintf('In File : %s', $e->getFile()) . $eof;
		echo sprintf('On Line : %s', $e->getLine()) . $eof;

		self::yellow(str_repeat('-', 40), $eof);
		echo sprintf('Trace : %s', $e->getTraceAsString()) . $eof;

		self::yellow(str_repeat('-', 80), $eof);
	}

	/**
	 * To format header before test function execution
	 * @param string $function
	 */
	public static function headerLine($string)
	{
		self::console('');
		self::yellow(str_repeat('-', 80));
		self::yellow(str_repeat('-', 80));
		self::green(strtoupper($string));
		self::yellow(str_repeat('-', 80));
		self::yellow(str_repeat('-', 80));
	}
}
