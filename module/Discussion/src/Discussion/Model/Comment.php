<?php
// %LICENCE_HEADER%
namespace Discussion\Model;

use Rbplm\Any;
use Rbplm\People;
use Rbplm\Sys\Date as DateTime;
use Rbplm\Dao\MappedInterface;

/**
 * 
 *
 */
class Comment extends Any implements MappedInterface
{
	use \Rbplm\Owned, \Rbplm\LifeControl, \Rbplm\Mapped, \Rbplm\Item;

	static $classId = '568be4fc7a0a8';

	/**
	 *
	 * @var string
	 */
	protected $body;

	/**
	 *
	 * @var string
	 */
	protected $discussionUid = null;

	/**
	 *
	 * @param string $name
	 * @return Any
	 */
	public static function init($name = "", $parent = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		$obj->updated = new DateTime();

		if ( !$name ) {
			$name = uniqid();
		}

		$obj->setName($name);
		$obj->setOwner(People\CurrentUser::get());

		if ( $parent ) {
			$obj->setParent($parent);
		}

		return $obj;
	}

	/**
	 *
	 * @param array $properties
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['discussionUid'])) ? $this->discussionUid = $properties['discussionUid'] : null;
		(isset($properties['parentId'])) ? $this->parentId = $properties['parentId'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid = $properties['parentUid'] : null;
		(isset($properties['ownerId'])) ? $this->ownerId = $properties['ownerId'] : null;
		(isset($properties['updated'])) ? $this->setUpdated(new DateTime($properties['updated'])) : null;
		(isset($properties['body'])) ? $this->body = $properties['body'] : null;
		return $this;
	}

	/**
	 *
	 * @param string $string
	 */
	public function setBody($string)
	{
		$this->body = $string;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * Set the discussion id.
	 *
	 * @param string $discussionUid
	 *
	 */
	public function setDiscussionUid($discussionUid)
	{
		$this->discussionUid = $discussionUid;
		return $this;
	}

	/**
	 * Get the discussion id.
	 *
	 * @return string
	 */
	public function getDiscussionUid()
	{
		return $this->discussionUid;
	}
}
