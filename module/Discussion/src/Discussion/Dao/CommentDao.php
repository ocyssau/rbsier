<?php
//%LICENCE_HEADER%
namespace Discussion\Dao;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT >>

 CREATE TABLE IF NOT EXISTS `discussion_comment` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `cid` CHAR(16) NOT NULL DEFAULT '568be4fc7a0a8',
 `name` VARCHAR(255) NULL DEFAULT NULL,
 `discussion_uid` VARCHAR(255) NOT NULL,
 `owner_id` VARCHAR(255) NULL DEFAULT NULL,
 `parent_id` INT(11) NULL DEFAULT NULL,
 `parent_uid` VARCHAR(255) NULL DEFAULT NULL,
 `updated` DATETIME NOT NULL,
 `body` MEDIUMTEXT NOT NULL,
 PRIMARY KEY (`id`),
 UNIQUE INDEX (`uid` ASC),
 INDEX (`parent_id` ASC),
 INDEX (`parent_uid` ASC),
 INDEX (`discussion_uid` ASC),
 INDEX (`owner_id` ASC),
 INDEX (`updated` ASC)
 )ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;

 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE IF EXISTS `discussion_comment`;
 <<*/

/**
 * 
 *
 */
class CommentDao extends DaoSier
{

	/**
	 * @var \PDOStatement
	 */
	public $deleteFromIdStmt;

	/**
	 * @var string
	 */
	public static $table = 'discussion_comment';

	public static $vtable = 'discussion_comment';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'cid' => 'cid',
		'uid' => 'uid',
		'name' => 'name',
		'body' => 'body',
		'discussion_uid' => 'discussionUid',
		'owner_id' => 'ownerId',
		'parent_id' => 'parentId',
		'parent_uid' => 'parentUid',
		'updated' => 'updated'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'updated' => 'datetime'
	);

	/**
	 * Recursive function
	 * @return CommentDao
	 */
	public function deleteFromId($id, $withChilds = true, $withTrans = null)
	{
		($withTrans === null) ? $withTrans = $this->options['withtrans'] : null;
		if ( $withTrans ) $this->connexion->beginTransaction();

		if ( !$this->deleteFromIdStmt ) {
			$table = $this->_table;
			$sql = 'DELETE FROM ' . $table . ' WHERE `id`=:id';
			$this->deleteFromIdStmt = $this->connexion->prepare($sql);
			$sql = 'SELECT `id` FROM ' . $table . ' WHERE `parent_id`=:id';
			$this->selectToDeleteFromIdStmt = $this->connexion->prepare($sql);
		}

		try {
			$bind = array(
				':id' => $id
			);
			$this->selectToDeleteFromIdStmt->execute($bind);
			$result = $this->selectToDeleteFromIdStmt->fetchAll(\PDO::FETCH_COLUMN);

			if ( $withChilds ) {
				foreach( $result as $childId ) {
					$this->deleteFromId($childId, true, false);
				}
			}

			$this->deleteFromIdStmt->execute($bind);

			if ( $withTrans ) $this->connexion->commit();
		}
		catch( \Exception $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}
} /* End of class */

