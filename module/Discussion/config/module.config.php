<?php
return array(
	'router' => array(
		'routes' => array(
			'discussion' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/discussion',
					'defaults' => array(
						'__NAMESPACE__' => 'Discussion\Controller',
						'controller'=> 'Index',
						'action'=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'=> 'Segment',
						'options' => array(
							'route'=> '/[:controller[/:action[/:id]]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Discussion\Controller\Index' => 'Discussion\Controller\IndexController',
			),
	),
	'view_manager' => array(
		'template_map' => array(
			'layout/discussion'   => 'module/Application/view/layout/ranchbe.phtml',
		),
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	),
);