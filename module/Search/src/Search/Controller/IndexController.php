<?php
namespace Search\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Search\Engine\Search;

/**
 *
 * @author ocyssau
 *
 */
class IndexController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'application_search';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'search/index/index';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'home';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('home')->activate();
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
	}

	/**
	 */
	public function indexAction()
	{
		/* Init some helper */
		$factory = DaoFactory::get();
		$view = $this->view;

		/* Check permission */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* */
		$this->layoutSelector()->clear($this);
		$this->basecamp()->save('search/index', $this->defaultFailedForward, $view);

		/* Filter */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Search\Form\SearchForm($factory, 'searchindex');
		$filterForm->setAttribute('id', 'searchfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter);

		$paginator = new \Application\Form\PaginatorForm('searchindex');
		$paginator->load();

		try {
			$select = [];
			/* @var \Search\Engine\SearchDao $dao */
			$dao = $factory->getDao(Search::$classId);
			$list = $factory->getList(Search::$classId);

			/* update index if older than $indexValidityTime minutes */
			$indexValidityTime = 24 * 60;
			$connexion = $factory->getConnexion();
			$connexion->beginTransaction();
			$connexion->prepare('CALL updateSearch(' . $indexValidityTime . ')')->execute();
			$connexion->commit();

			foreach( array_keys($dao->metaModel) as $asSys ) {
				$select[] = $asSys;
			}
			$filter->select($select);

			/* Set max limit paginator */
			$max = $list->countAll($filter);
			$max = ($max > 1000) ? 1000 : $max;
			$paginator->setMaxLimit($max);
			$paginator->bindToFilter($filter);

			/* Load list */
			$list->load($filter);

			/* Save paginator and filter */
			$paginator->bindToView($view)->save();
			$filterForm->save();
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
			$this->errorForward();
		}

		/* */
		$headers = [];

		/* */
		$view->headers = $headers;
		$view->select = $select;
		$view->list = $list;
		$view->pageTitle = 'Search';
		$view->filter = $filterForm;
		return $view;
	}

	/**
	 *
	 */
	public function fulltextAction()
	{
		/* Check permission */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* */
		$view = $this->view;

		/* */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Search\Form\FulltextSearchForm(DaoFactory::get(), $this->pageId);
		$route = $this->getEvent()->getRouteMatch();
		$url = $this->url()->fromRoute($route->getMatchedRouteName(), array(
			'action' => $route->getParam('action')
		));
		$filterForm->setAttribute('action', $url);
		$filterForm->setAttribute('id', 'fulltextsearchfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter);

		try {
			$fullTextSearchEngine = new \Search\Engine\FullText(DaoFactory::get()->getConnexion());
			$stmt = $fullTextSearchEngine->searchInGed($filter);
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
		}

		/* */
		$headers = array(
			array(
				'id',
				'id'
			),
			array(
				'name',
				'name'
			),
			array(
				'number',
				'number'
			),
			array(
				'description',
				'description'
			),
			array(
				'spacename',
				'spacename'
			),
			array(
				'cid',
				'cid'
			)
		);

		$view->headers = $headers;
		$view->list = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$view->pageTitle = 'FullText Search';
		$view->filter = $filterForm;
		return $view;
	}

	/**
	 *
	 */
	public function fordocumentAction()
	{
		/* */
		$view = $this->view;

		/* Check permission */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* */
		$this->layoutSelector()->clear($this);
		$this->basecamp()->save('search/fordocument', $this->defaultFailedForward, $view);

		/* */
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		$filterForm = new \Search\Form\FulltextSearchForm(DaoFactory::get(), 'searchfordocument');
		$filterForm->setAttribute('action', $this->getRequest()
			->getBaseUrl() . '/search/fordocument');
		$filterForm->setAttribute('id', 'searchfordocumentform');
		$filterForm->load();
		$filterForm->bindToFilter($filter);

		$paginator = new \Application\Form\PaginatorForm('searchfordocument');
		$paginator->reset(); /* ignore session infos */
		$paginator->load([
			'default' => [
				'orderby' => '(score_content*0.1+score_nom*0.9)',
				'order' => 'desc',
				'limit' => 50
			]
		]);

		try {
			$fullTextSearchEngine = new \Search\Engine\SearchInDocument();

			$max = $fullTextSearchEngine->countAll($filter, $filter->getBind());
			$max = ($max > 1000) ? 1000 : $max;
			$paginator->setMaxLimit($max);

			/* Set max limit paginator */
			$paginator->bindToFilter($filter);

			$fullTextSearchEngine->load($filter, $filter->getBind());
			$view->list = $fullTextSearchEngine;

			/* save paginator and filter */
			$paginator->bindToView($view)->save();
			$filterForm->save();
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
		}

		$view->filter = $filterForm;
		$view->headers = '';
		$view->pageId = 'searchfordocument';
		$view->pageTitle = 'FullText Search';
		return $view;
	}

	/**
	 *
	 */
	public function forcontainerAction()
	{
		/* */
		$view = $this->view;

		/* Check permission */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* */
		$this->layoutSelector()->clear($this);
		$this->basecamp()->save('search/forcontainer', $this->defaultFailedForward, $view);

		/* */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Ged\Form\Container\FulltextSearchForm(DaoFactory::get(), 'searchforcontainer');
		$filterForm->setAttribute('action', $this->getRequest()
			->getBaseUrl() . '/search/forcontainer');
		$filterForm->setAttribute('id', 'searchforcontainerform');
		$filterForm->load();
		$filterForm->bindToFilter($filter);

		/* */
		$paginator = new \Application\Form\PaginatorForm('searchforcontainer');
		$paginator->load([
			'default' => [
				'orderby' => 'number',
				'order' => 'asc',
				'limit' => 50
			]
		]);

		try {
			$fullTextSearchEngine = new \Search\Engine\SearchInContainer();

			/* Set max limit paginator */
			$max = $fullTextSearchEngine->countAll($filter);
			$max = ($max > 1000) ? 1000 : $max;
			$paginator->setMaxLimit($max);

			$paginator->bindToFilter($filter);
			$fullTextSearchEngine->load($filter);
			$view->list = $fullTextSearchEngine;

			/* save paginator and filter */
			$paginator->bindToView($view)->save();
			$filterForm->save();
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
		}

		/* */
		$headers = array(
			array(
				'name',
				'name'
			),
			array(
				'designation',
				'designation'
			)
		);

		$view->filter = $filterForm;
		$view->headers = $headers;
		$view->pageTitle = 'FullText Search';
		return $view;
	}

	/**
	 *
	 */
	public function forfileAction()
	{
		/* */
		$view = $this->view;

		/* Check permission */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* */
		$this->layoutSelector()->clear($this);
		$this->basecamp()->save('search/forfile', $this->defaultFailedForward, $view);

		/* */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Ged\Form\Docfile\FulltextSearchForm(DaoFactory::get(), 'searchforfile');
		$filterForm->setAttribute('action', $this->getRequest()
			->getBaseUrl() . '/search/forfile');
		$filterForm->setAttribute('id', 'searchforfile');
		$filterForm->load();
		$filterForm->bindToFilter($filter);

		$paginator = new \Application\Form\PaginatorForm('searchforfile');
		$paginator->load();

		try {
			$fullTextSearchEngine = new \Search\Engine\SearchInFile();

			$max = $fullTextSearchEngine->countAll($filter);
			$max = ($max > 1000) ? 1000 : $max;
			$paginator->setMaxLimit($max);

			/* Set max limit paginator */
			$paginator->bindToFilter($filter);
			$fullTextSearchEngine->load($filter);
			$view->list = $fullTextSearchEngine;

			/* save paginator and filter */
			$paginator->bindToView($view)->save();
			$filterForm->save();
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
		}

		/* */
		$headers = array(
			array(
				'name',
				'name'
			),
			array(
				'designation',
				'designation'
			)
		);

		$view->filter = $filterForm;
		$view->headers = $headers;
		$view->pageTitle = 'FullText Search';
		return $view;
	}

	/**
	 *
	 */
	public function forproductAction()
	{
		/* */
		$view = $this->view;

		/* Check permission */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* */
		$this->layoutSelector()->clear($this);
		$this->basecamp()->save('search/forproduct', $this->defaultFailedForward, $view);

		/* */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Pdm\Form\ProductVersion\FulltextSearchForm(DaoFactory::get(), 'searchforproduct');
		$filterForm->setAttribute('action', $this->getRequest()
			->getBaseUrl() . '/search/forproduct');
		$filterForm->setAttribute('id', 'searchforproduct');
		$filterForm->load();
		$filterForm->bindToFilter($filter);

		$paginator = new \Application\Form\PaginatorForm('searchforproduct');
		$paginator->load();

		try {
			$fullTextSearchEngine = new \Search\Engine\SearchInProduct();

			/* Set max limit paginator */
			$max = $fullTextSearchEngine->countAll($filter);
			$max = ($max > 1000) ? 1000 : $max;
			$paginator->setMaxLimit($max);

			$paginator->bindToFilter($filter);
			$fullTextSearchEngine->load($filter);
			$view->list = $fullTextSearchEngine;

			/* save paginator and filter */
			$paginator->bindToView($view)->save();
			$filterForm->save();
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
		}

		/* */
		$headers = array(
			array(
				'name',
				'Name'
			),
			array(
				'description',
				'Description'
			),
			array(
				'version',
				'Version'
			)
		);

		$view->filter = $filterForm;
		$view->headers = $headers;
		$view->pageTitle = 'FullText Search';
		return $view;
	}
} /* End of class */
