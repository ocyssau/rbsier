<?php
namespace Search\Controller;

use Application\Controller\AbstractController;
use Search\Solr;

/**
 *
 * @author ocyssau
 *
 */
class SolrController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'solr_search';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'search/index/index';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'home';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('home')->activate();
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
	}

	/**
	 */
	public function indexAction()
	{
		$config = $this->getEvent()->getApplication()->getServiceManager()->get('config')['search.solr.config'];
		$solr = new \SolrClient($config);
		
		$doc = new \SolrInputDocument();
		$doc->addField('id', 'test');
		$doc->addField('uid', 'testUid');
		$doc->addField('name', 'My Name');
		$doc->addField('number', 'test001');
		$doc->addField('description', 'un test');
		
		$solr->addDocument($doc);
		var_dump($doc);die;
		
	}
	
} /* End of class */
