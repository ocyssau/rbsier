<?php
namespace Search\Model;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `indexer` (
 `document_id` int(11) NOT NULL,
 `docfile_id` int(11) NOT NULL,
 `spacename` VARCHAR(32) NOT NULL,
 `updated` DATETIME NULL,
 `content` TEXT,
 PRIMARY KEY (`docfile_id`, `spacename`),
 UNIQUE (`spacename` ASC, `docfile_id` ASC),
 KEY (`spacename`),
 KEY (`document_id` ASC, `spacename` ASC),
 FULLTEXT (content)
 ) ENGINE=MyIsam;
 << */

/** SQL_INSERT>>
 << */

/** SQL_ALTER>>
 << */

/** SQL_FKEY>>
 << */

/** SQL_TRIGGER>>
 << */

/** SQL_VIEW>>
 << */

/** SQL_DROP>>
 DROP TABLE `indexer`;
 << */

/**
 * 
 * @author olivier
 *
 */
class IndexerDao extends \Rbs\Dao\Sier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'indexer';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'indexer';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'document_id' => 'documentId',
		'docfile_id' => 'docfileId',
		'spacename' => 'spacename',
		'updated' => 'status'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'updated' => 'datetime'
	);
}
