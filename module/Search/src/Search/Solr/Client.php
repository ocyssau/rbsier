<?php
namespace Search\Solr;

/**
 * 
 * @see http://php.net/manual/class.solrclient.php
 *
 */
class Client extends \SolrClient
{

	/** @var Client */
	protected static $instance;

	/**
	 * 
	 * @return \Search\Solr\Client
	 */
	public static function get()
	{
		if ( !self::$instance ) {
			$config = \Ranchbe::get()->getConfig('search.solr.config');
			self::$instance = new self($config);
		}
		return self::$instance;
	}
} /* End of class */
