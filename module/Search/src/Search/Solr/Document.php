<?php
namespace Search\Solr;

/**
 * 
 * @see http://php.net/manual/class.solrdocument.php
 *
 */
class Document
{

	/**
	 * 
	 * @return \Search\Solr\Document
	 */
	public static function factoryFromRbDocument(\Rbplm\Ged\Document\Version $rbDocument)
	{
		$doc = new \SolrDocument();
		$doc->addField('id', $rbDocument->getId());
		$doc->addField('uid', $rbDocument->getUid());
		$doc->addField('name', $rbDocument->getName());
		$doc->addField('number', $rbDocument->getNumber());
		$doc->addField('description', $rbDocument->description);
		return $doc;
	}
} /* End of class */
