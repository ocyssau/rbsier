<?php
namespace Search\Form;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;
use Search\Engine;
use Rbplm\Ged\AccessCode;

/**
 *
 *
 */
class SearchForm extends AbstractFilterForm
{

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 * @param string $sessionNamespace
	 */
	public function __construct($factory, $sessionNamespace)
	{
		parent::__construct($factory, $sessionNamespace);

		$this->template = 'search/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Spacename */
		$this->add(array(
			'name' => 'find_spacename',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control submitOnChange',
				'id' => 'spacename',
				'multiple' => false
			),
			'options' => array(
				'label' => tra('In'),
				'value_options' => $this->_getSpacenameSet()
			)
		));
		$inputFilter->add(array(
			'name' => 'find_spacename',
			'required' => false
		));

		/* Object class */
		$this->add(array(
			'name' => 'find_class',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control submitOnChange',
				'id' => 'find_class',
				'multiple' => false
			),
			'options' => array(
				'label' => tra('Object Type'),
				'value_options' => $this->_getObjectClassSet()
			)
		));
		$inputFilter->add(array(
			'name' => 'find_class',
			'required' => false
		));

		/* Designation */
		$this->add(array(
			'name' => 'find_designation',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search For Designation...',
				'class' => 'form-control',
				'data-where' => 'description',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_designation',
			'required' => false
		));

		/* Name */
		$this->add(array(
			'name' => 'find_number',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search For Number...',
				'class' => 'form-control',
				'data-where' => 'number',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Number'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_number',
			'required' => false
		));

		/* Advanced search CheckBox */
		$this->add(array(
			'name' => 'f_adv_search_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Advanced'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_adv_search_cb',
			'required' => false
		));

		/* DateAndTime CheckBox */
		$this->add(array(
			'name' => 'f_dateAndTime_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Date And Time'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_dateAndTime_cb',
			'required' => false
		));

		/* CheckOut date */
		$this->add(array(
			'name' => 'f_check_out_date_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'CheckOut date'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_check_out_date_cb',
			'required' => false
		));
		/* Superior to Date */
		$this->add(array(
			'name' => 'f_check_out_date_min',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Superior to'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_check_out_date_min',
			'required' => false
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_check_out_date_max',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Inferior to'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_check_out_date_max',
			'required' => false
		));

		/* Update date selector */
		$this->add(array(
			'name' => 'f_update_date_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Update date'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_update_date_cb',
			'required' => false
		));

		/* Superior to date */
		$this->add(array(
			'name' => 'f_update_date_min',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Superior to'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_update_date_min',
			'required' => false
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_update_date_max',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Inferior to'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_update_date_max',
			'required' => false
		));

		/* Open date */
		$this->add(array(
			'name' => 'f_open_date_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Open date'
			)
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_open_date_min',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Superior to'
			)
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_open_date_max',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Inferior to'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_cb',
			'required' => false
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_min',
			'required' => false
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_max',
			'required' => false
		));

		/* Close date */
		$this->add(array(
			'name' => 'f_close_date_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Close date'
			)
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_close_date_min',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Superior to'
			)
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_close_date_max',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Inferior to'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_cb',
			'required' => false
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_min',
			'required' => false
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_max',
			'required' => false
		));

		/*Create user*/
		$this->add(array(
			'name' => 'f_createby',
			'type' => 'Application\Form\Element\SelectUser',
			'options' => array(
				'label' => tra('Create By')
			)
		));

		/*Update user*/
		$this->add(array(
			'name' => 'f_updateby',
			'type' => 'Application\Form\Element\SelectUser',
			'options' => array(
				'label' => tra('Update By')
			)
		));

		/*Checkout user*/
		$this->add(array(
			'name' => 'f_checkoutby',
			'type' => 'Application\Form\Element\SelectUser',
			'options' => array(
				'label' => tra('Checkout By')
			)
		));
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(Engine\Search::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();
		
		/* NORMALS OPTIONS */
		//NUMBER
		if ( $datas['find_number'] ) {
			$filter->andFind($datas['find_number'], $dao->toSys('number'), Op::CONTAINS);
		}

		//DESIGNATION
		if ( $datas['find_designation'] ) {
			$filter->andFind($datas['find_designation'], $dao->toSys('designation'), Op::CONTAINS);
		}

		//CLASS
		if ( $datas['find_class'] ) {
			$filter->andFind($datas['find_class'], $dao->toSys('cid'), Op::EQUAL);
		}

		//SPACENAME
		if ( $datas['find_spacename'] ) {
			$filter->andFind($datas['find_spacename'], $dao->toSys('spacename'), Op::EQUAL);
		}

		/* ADVANCED SEARCH */
		if ( $datas['f_adv_search_cb'] ) {
			//FIND IN
			if ( $datas['find'] && $datas['find_field'] ) {
				$filter->with(array(
					'table' => $this->daoFactory->getName() . '_categories',
					'on' => 'category_id',
					'alias' => 'category',
					'select' => array(
						'category_number'
					),
					'direction' => 'outer'
				));
				$filter->andFind($datas['find'], $datas['find_field'], Op::CONTAINS);
			}

			//ACTION USER
			if ( $datas['f_createby'] ) {
				$filter->andFind($datas['f_createby'], 'createById', Op::EQUAL);
			}
			if ( $datas['f_updateby'] ) {
				$filter->andFind($datas['f_updateby'], 'updateById', Op::EQUAL);
			}
			if ( $datas['f_checkoutby'] ) {
				$filter->andFind($datas['f_checkoutby'], 'checkoutById', Op::EQUAL);
			}

			//DATE AND TIME
			if ( $datas['f_dateAndTime_cb'] ) {
				//CHECKOUT
				if ( $datas['f_check_out_date_cb'] ) {
					if ( $datas['f_check_out_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_check_out_date_min']), $dao->toSys('locked'), Op::SUP);
						$filter->andFind(AccessCode::CHECKOUT, $dao->toSys('accessCode'), Op::EQUAL);
					}
					if ( $datas['f_check_out_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_check_out_date_min']), $dao->toSys('locked'), Op::INF);
						$filter->andFind(AccessCode::CHECKOUT, $dao->toSys('accessCode'), Op::EQUAL);
					}
				}
				//UPDATE
				if ( $datas['f_update_date_cb'] ) {
					if ( $datas['f_update_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_update_date_min']), $dao->toSys('updated'), Op::SUP);
					}
					if ( $datas['f_update_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_update_date_max']), $dao->toSys('updated'), Op::INF);
					}
				}
				//OPEN
				if ( $datas['f_open_date_cb'] ) {
					if ( $datas['f_open_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_open_date_min']), $dao->toSys('created'), Op::SUP);
					}
					if ( $datas['f_open_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_open_date_max']), $dao->toSys('created'), Op::INF);
					}
				}
				//CLOSE
				if ( $datas['f_close_date_cb'] ) {
					if ( $datas['f_close_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_close_date_min']), $dao->toSys('closed'), Op::SUP);
					}
					if ( $datas['f_close_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_close_date_max']), $dao->toSys('closed'), Op::INF);
					}
				}
				//FORSEEN CLOSE
				if ( $datas['f_fsclose_date_cb'] ) {
					if ( $datas['f_fsclose_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_min']), 'planned_closure', Op::SUP);
					}
					if ( $datas['f_fsclose_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_max']), 'planned_closure', Op::INF);
					}
				}
			}
		}

		return $this;
	}

	/**
	 * @return array
	 */
	protected function _getSpacenameSet()
	{
		return [
			null => '...',
			'cadlib' => tra('cadlib'),
			'bookshop' => tra('bookshop'),
			'mockup' => tra('mockup'),
			'workitem' => tra('workitem')
		];
	}

	/**
	 * @return array
	 */
	protected function _getObjectClassSet()
	{
		return [
			null => '...',
			'569e92709feb6' => tra('Document'),
			'569e92b86d248' => tra('File'),
			'569e94192201a' => tra('Container')
		];
	}
} /* End of class */
