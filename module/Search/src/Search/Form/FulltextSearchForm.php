<?php
namespace Search\Form;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\AccessCode;

/**
 *
 *
 */
class FulltextSearchForm extends AbstractFilterForm
{

	/**
	 * @param string $nameSpace
	 */
	public function __construct($factory, $sessionNamespace)
	{
		parent::__construct($factory, $sessionNamespace);

		$this->template = 'search/fulltextsearchform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Find */
		$this->add(array(
			'name' => 'find_what',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_what',
			'required' => false
		));

		/* displayHistory */
		$this->add(array(
			'name' => 'displayAll',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control submitOnClick',
				'title' => 'Include history, deleted, versions...'
			),
			'options' => array(
				'label' => tra('Display All')
			)
		));
		$inputFilter->add(array(
			'name' => 'displayAll',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();

		if ( !$datas['displayAll'] ) {
			/* dont display h obsolete versions */
			$filter->andFind(AccessCode::HISTORY, 'acode', Op::NOTEQUAL);
			$filter->andFind(AccessCode::VERSION, 'acode', Op::NOTEQUAL);
			$filter->andFind(AccessCode::ITERATION, 'acode', Op::NOTEQUAL);
		}

		/**/
		$bind = $filter->getBind();

		/**/
		$select = $filter->getSelect();
		if ( count($select) == 0 ) {
			$select = [
				'`obj`.`id` AS `id`',
				'`obj`.`uid` AS `uid`',
				'`obj`.`number` AS `number`',
				'`obj`.`name` AS `name`',
				'`obj`.`designation` AS `description`',
				'`obj`.`lock_by_id` AS `lockById`',
				'`obj`.`lock_by_uid` AS `lockByUid`',
				'`obj`.`locked` AS `locked`',
				'`obj`.`updated` AS `updated`',
				'`obj`.`update_by_id` AS `updateById`',
				'`obj`.`update_by_uid` AS `updateByUid`',
				'`obj`.`created` AS `created`',
				'`obj`.`create_by_id` AS `createById`',
				'`obj`.`create_by_uid` AS `createByUid`',
				'`obj`.`life_stage` AS `lifeStage`',
				'`obj`.`version` AS `version`',
				'`obj`.`iteration` AS `iteration`',
				'`obj`.`acode` AS `accessCode`',
				'`obj`.`doctype_id` AS `doctypeId`',
				'`obj`.`category_id` AS `categoryId`'
			];
		}

		//MATCH(obj.number,obj.name,obj.designation) AGAINST("$what") AS score_nom,
		//ORDER BY (score_content*0.1+score_nom*0.9) DESC

		/* WHAT */
		if ( $datas['find_what'] ) {
			$bkm = $datas['find_what'];
			$select[] = 'MATCH(indexer.content) AGAINST("' . $bkm . '") AS score_content';
			$select[] = 'MATCH(obj.number,obj.name,obj.designation, obj.tags, obj.label) AGAINST("' . $bkm . '") AS score_nom';

			$whats = explode(' ', $bkm);
			$i = 1;
			foreach( $whats as $what ) {
				$bkm = ':whatm' . $i;
				$bkl = ':whatl' . $i;
				$bke = ':whate' . $i;

				$bind[$bkm] = $what;
				$bind[$bkl] = '%' . $what . '%';
				$bind[$bke] = $what;

				$subfilter = new $filter('', false);
				$subfilter->orFind($bkm, 'indexer.content', Op::MATCH);
				$subfilter->orFind($bkm, 'obj.number,obj.name,obj.designation, obj.tags, obj.label', Op::MATCH);
				$subfilter->orFind($bkl, 'doctypes.name', Op::LIKE)
					->orFind($bkl, 'doctypes.designation', Op::LIKE)
					->orFind($bkl, 'doctypes.file_extensions', Op::LIKE)
					->orFind($bkl, 'obj.number', Op::LIKE)
					->orFind($bkl, 'obj.name', Op::LIKE)
					->orFind($bkl, 'obj.designation', Op::LIKE)
					->orFind($bke, 'obj.id', Op::EQUAL);
				$filter->suband($subfilter);
				$i++;
			}
		}
		else {
			$select[] = '0 AS score_content';
			$select[] = '0 AS score_nom';
		}

		$filter->select($select);
		$filter->setBind($bind);
		return $this;
	}
}
