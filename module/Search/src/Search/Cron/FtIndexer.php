<?php
//%LICENCE_HEADER%
namespace Search\Cron;

use Rbs\Batch\CallbackInterface;
use Rbplm\Dao\Connexion;
use Rbs\Dao\Sier\MetamodelTranslator;
use Rbs\Converter\Factory as ConverterFactory;

/**
 * CallbackInterface interface
 *
 * Must be implemented by callbock for execution of cronTasks
 *
 */
class FtIndexer implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/* @var int number of files to index by batch */
	protected $limit = 10000;

	/* @var int number of chars to extract of file to index */
	protected $stringLimit = 10000;

	/* @var array list of extensions to index */
	protected $filetypes = null;

	/* @var bool If true reindex files */
	protected $reindex = false;

	/* @var array If populate with names of workitems, restrict to content of this list */
	protected $workitems;

	/* @var array If populate with names of bookshops, restrict to content of this list */
	protected $bookshops;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
		$this->connexion = Connexion::get();

		isset($params['limit']) ? $this->limit = $params['limit'] : null;
		isset($params['filetypes']) ? $this->filetypes = $params['filetypes'] : $this->filetypes = null;
		isset($params['stringLimit']) ? $this->stringLimit = $params['stringLimit'] : null;
		isset($params['reindex']) ? $this->reindex = $params['reindex'] : null;
		isset($params['workitems']) ? $this->workitems = $params['workitems'] : null;
		isset($params['bookshops']) ? $this->bookshops = $params['bookshops'] : null;
	}

	/**
	 *
	 */
	public function run()
	{
		$job = $this->job;
		if ( $job->cron instanceof \Rbs\Batch\CronTask ) {
			$lastExec = $job->cron->getLastExec();
			if ( !$lastExec ) {
				$lastExec = null;
			}
		}

		$docfilesToIndexStmt = $this->getDocfilesToIndex($lastExec);
		echo sprintf('Indexation of %s files', $docfilesToIndexStmt->rowCount()) . "\n\r";

		$this->index($docfilesToIndexStmt, true);

		return $this;
	}

	/**
	 *
	 * @param \DateTime $lastRuntime
	 * @throws \Exception
	 * @return \PDOStatement
	 */
	public function getDocfilesToIndex($lastRuntime = null)
	{
		$connexion = $this->connexion;
		$limit = $this->limit;

		/* get all docfiles modified since the lastIndexerRun */
		try {
			if ( $this->reindex ) {
				$lastRuntime = null;
			}

			if ( $lastRuntime ) {
				$where = "WHERE df.updated > indexer.updated OR indexer.updated IS NULL AND df.acode < 11";
				$bind = array(
					':lastrun' => MetamodelTranslator::datetimeToSys($lastRuntime)
				);
			}
			else {
				$where = "WHERE df.acode < 11";
				$bind = array();
			}

			if ( $this->filetypes ) {
				$filetypes = [];
				foreach( $this->filetypes as $t ) {
					$t = $connexion->quote($t);
					$filetypes[] = $t;
				}
				$filetypes = implode(',', $filetypes);
				$where = $where . " AND df.extension IN ($filetypes)";
			}

			$wiWhere = '1=1';
			$bsWhere = '1=1';

			if ( $this->workitems ) {
				$wiWhere = '';
				foreach( $this->workitems as $t ) {
					$t = $connexion->quote('%/' . $t);
					$wiWhere = $wiWhere . " df.path LIKE $t";
				}
			}
			if ( $this->bookshops ) {
				$bsWhere = '';
				foreach( $this->bookshops as $t ) {
					$t = $connexion->quote('%/' . $t);
					$bsWhere = $bsWhere . " df.path LIKE $t";
				}
			}

			$select = 'df.id, df.name, df.path, df.document_id, df.document_uid, df.updated, df.extension';
			$sql = "SELECT $select, 'workitem' as spacename FROM workitem_doc_files AS df LEFT OUTER JOIN indexer ON indexer.docfile_id=df.id $where AND $wiWhere";
			$sql .= " UNION SELECT $select, 'bookshop' as spacename FROM bookshop_doc_files AS df LEFT OUTER JOIN indexer ON indexer.docfile_id=df.id $where AND $bsWhere";
			$sql = "SELECT * FROM ($sql) AS toup LIMIT $limit";

			$docFilesStmt = $connexion->prepare($sql);
			$docFilesStmt->execute($bind);
			return $docFilesStmt;
		}
		catch( \Exception $e ) {
			throw $e;
		}
	}

	/**
	 * @param \PDOStatement $pdoStmt
	 * @param boolean $verbose
	 */
	public function index(\PDOStatement $pdoStmt, $verbose = false)
	{
		$connexion = $this->connexion;
		$now = new \DateTime();

		/* statement inits for index update and insert */
		$sql = "INSERT INTO indexer (document_id, docfile_id, spacename, updated, content) VALUES (:documentId, :docfileId, :spacename, :updated, :content)";
		$indexInsertStmt = $connexion->prepare($sql);

		$sql = "UPDATE indexer SET updated=:updated, content=:content WHERE docfile_id=:docfileId AND spacename=:spacename";
		$indexUpdateStmt = $connexion->prepare($sql);

		/* insert each file in index */
		while( $fileEntry = $pdoStmt->fetch(\PDO::FETCH_ASSOC) ) {
			$path = $fileEntry['path'];
			$name = $fileEntry['name'];
			$extension = $fileEntry['extension'];
			$documentId = $fileEntry['document_id'];
			$docfileId = $fileEntry['id'];
			$spacename = $fileEntry['spacename'];
			$fullPath = $path . '/' . $name;

			if ( !is_file($fullPath) ) {
				if ( $verbose ) echo "$fullPath is not found \n";
				continue;
			}

			if ( $verbose ) echo "Indexation of $fullPath \n";

			/* extract content of file */
			/* and write in db indexer table */
			try {
				$converter = ConverterFactory::get()->getConverter($extension, 'txt');
				$tmpfile = tempnam('/tmp', 'rbFtIndexer');
				$converter->convert($fullPath, $tmpfile);

				if ( $converter->getResult()->getErrors() ) {
					foreach( $converter->getResult()->getErrors() as $str ) {
						echo $str . "\n";
					}
				}

				/* get stringLimit fst characters */
				$content = '';
				$ofile = new \SplFileObject($tmpfile);
				while( strlen($content) < $this->stringLimit ) {
					$line = self::stringFilter($ofile->current());
					$content = $content . $line;
					$ofile->next();
					if ( $ofile->eof() ) {
						break;
					}
				}
				$ofile = null;
				unlink($tmpfile);

				if ( !$content ) {
					echo "\t None content to index \n";
				}

				try {
					$indexInsertStmt->execute(array(
						':documentId' => $documentId,
						':docfileId' => $docfileId,
						':spacename' => $spacename,
						':updated' => MetamodelTranslator::datetimeToSys($now),
						':content' => $content
					));
					echo "\t Insert new index docfileId $docfileId spacename $spacename \n";
				}
				catch( \Exception $e ) {
					try {
						echo "\t" . $e->getMessage() . "\n";
						if ( strpos($e->getMessage(), 'SQLSTATE[HY000]') === false ) {}
						$indexUpdateStmt->execute(array(
							':docfileId' => $docfileId,
							':spacename' => $spacename,
							':updated' => MetamodelTranslator::datetimeToSys($now),
							':content' => $content
						));
						echo "\t Update existing index docfileId $docfileId spacename $spacename" . "\n";
					}
					catch( \Exception $e ) {
						echo "\t Update index failed :" . $e->getMessage() . "\n";
					}
				}
			}
			catch( \Rbs\Converter\NotMappedException $e ) {
				echo $e->getMessage() . "\n";
				continue;
			}
			catch( \Exception $e ) {
				echo $e->getMessage() . "\n";
				continue;
			}
		}

		return $this;
	}

	/**
	 * 
	 * @param string $string
	 * @return string
	 */
	protected static function stringFilter($string)
	{
		$string = preg_replace('/\s\s+/', '', $string);
		$string = str_replace("\r", " ", $string); /* remove return */
		$string = str_replace("\n", " ", $string); /* remove return */
		$string = str_replace("\t", " ", $string); /* remove tabs */
		$string = preg_replace('/[[:cntrl:]]/', '', $string); /* remove controls chars */
		$string = preg_replace('/[[:^print:]]/', '', $string); /* remove non ascii chars */
		return $string;
	}
}
