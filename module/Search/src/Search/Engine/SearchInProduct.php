<?php
namespace Search\Engine;

/** SQL_SCRIPTS>>
 << */

/** SQL_ALTER>>
 ALTER TABLE `pdm_product_version` ADD FULLTEXT (name, number, description);
 ALTER TABLE `pdm_product_instance` ADD FULLTEXT (name, number, description);
 << */

/**
 *
 *
 */
class SearchInProduct extends \Rbplm\Dao\DaoListAbstract
{

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'name' => 'name',
		'number' => 'number',
		'description' => 'description',
		'of_product_uid' => 'ofProductUid',
		'of_product_id' => 'ofProductId',
		'document_uid' => 'documentUid',
		'document_id' => 'documentId',
		'spacename' => 'spacename',
		'type' => 'type',
		'version' => 'version',
		'materials' => 'materials',
		'weight' => 'weight',
		'volume' => 'volume',
		'wetsurface' => 'wetSurface',
		'density' => 'density',
		'gravitycenter' => 'gravityCenter',
		'inertiacenter' => 'inertiaCenter'
	);

	/**
	 *
	 */
	public function __construct($connexion = null)
	{
		parent::__construct(null, $connexion);
		$this->metaModel = static::$sysToApp;
	}

	/**
	 *
	 * @see \Rbplm\Dao\DaoInterface::load()
	 *
	 * @param
	 *        	string | DaoFilter $filter
	 * @param
	 *        	array
	 * @return SearchInDocument
	 */
	public function load($filter = null, $bind = null)
	{
		if ( $this->isLoaded == true ) {
			$this->_reset();
		}

		$select = $filter->selectToString();
		if ( $select == '*' ) {
			$select = '
			`obj`.`id`  as  `id`,
			`obj`.`uid`  as  `uid`,
			`obj`.`name`  as  `name`,
			`obj`.`number`  as  `number`,
			`obj`.`description`  as  `description`,
			`obj`.`of_product_uid`  as  `ofProductUid`,
			`obj`.`of_product_id`  as  `ofProductId`,
			`obj`.`document_uid`  as  `documentUid`,
			`obj`.`document_id`  as  `documentId`,
			`obj`.`spacename`  as  `spacename`,
			`obj`.`type`  as  `type`,
			`obj`.`version`  as  `version`,
			`obj`.`materials`  as  `materials`,
			`obj`.`weight`  as  `weight`,
			`obj`.`volume`  as  `volume`,
			`obj`.`wetsurface`  as  `wetSurface`,
			`obj`.`density`  as  `density`,
			`obj`.`gravitycenter`  as  `gravityCenter`,
			`obj`.`inertiacenter`  as  `inertiaCenter`';
		}

		$sql = "SELECT * FROM(
		SELECT
		$select,
		'569e972dd4c2c' as `cid`
		FROM pdm_product_version as obj
		LEFT OUTER JOIN indexer ON obj.document_id=indexer.document_id AND obj.spacename=indexer.spacename
		WHERE %filter%) AS products";

		$sql = str_replace('%filter%', $filter->whereToString(), $sql) . $filter->orderToString();
		$this->stmt = $this->connexion->prepare($sql);
		$this->stmt->setFetchMode($this->options['mode']);
		$this->stmt->execute($bind);
		$this->isLoaded = true;
		$this->filter = $filter;
		return $this;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::save()
	 */
	public function save($list)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::delete()
	 */
	public function delete($filter)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::countAll()
	 */
	public function countAll($filter, $bind = null)
	{
		$sql = "SELECT COUNT(*) FROM(
			SELECT id FROM pdm_product_version as obj
			LEFT OUTER JOIN indexer ON obj.document_id=indexer.document_id AND obj.spacename=indexer.spacename
			WHERE %filter%
		) AS products";

		$sql = str_replace('%filter%', $filter->whereToString(), $sql);
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		$count = $stmt->fetch();
		return $count[0];
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::areExisting()
	 */
	public function areExisting($propNameToTest, $list, $returnSelect = null)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}
} /* End of class */
