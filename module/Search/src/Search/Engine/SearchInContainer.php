<?php
namespace Search\Engine;

/** SQL_SCRIPTS>>
 << */

/** SQL_ALTER>>
 ALTER TABLE `workitems` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `cadlibs` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `mockups` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `bookshops` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `projects` ADD FULLTEXT (number,name,designation);
 << */

/**
 *
 *
 */
class SearchInContainer extends \Rbplm\Dao\DaoListAbstract
{

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'number' => 'number',
		'name' => 'name',
		'designation' => 'description',
		'life_stage' => 'lifeStage',
		'acode' => 'accessCode',
		'version' => 'versionId',
		'parent_id' => 'parentId',
		'default_file_path' => 'path',
		'default_process_id' => 'processId',
		'planned_closure' => 'forseenCloseDate',
		'closed' => 'closed',
		'close_by_id' => 'closeById',
		'created' => 'created',
		'create_by_id' => 'createById'
	);

	/**
	 *
	 */
	public function __construct($connexion = null)
	{
		parent::__construct(null, $connexion);
		$this->metaModel = static::$sysToApp;
	}

	/**
	 *
	 * @see \Rbplm\Dao\DaoInterface::load()
	 *
	 * @param
	 *        	string | DaoFilter $filter
	 * @param
	 *        	array
	 * @return SearchInDocument
	 */
	public function load($filter = null, $bind = null)
	{
		if ( $this->isLoaded == true ) {
			$this->_reset();
		}

		$select = $filter->selectToString();
		if ( $select == '*' ) {
			$select = '
			`obj`.`id` as `id`,
			`obj`.`uid` as `uid`,
			`obj`.`number` as `number`,
			`obj`.`name` as `name`,
			`obj`.`designation` as `description`,
			`obj`.`created` as created,
			`obj`.`create_by_id` as createById,
			`obj`.`life_stage` as lifeStage,
			`obj`.`acode` as accessCode,
			`obj`.`parent_id` as parentId,
			`obj`.`default_file_path` as path,
			`obj`.`default_process_id` as processId,
			`obj`.`planned_closure` as forseenCloseDate,
			`obj`.`closed` as closed,
			`obj`.`close_by_id` as closeById';
		}

		$sql = "SELECT * FROM(
		SELECT
		$select,
		'workitem' AS `spacename`,
		'569e94192201a' AS `cid`
		FROM `workitems` as obj
		WHERE %filter%
		UNION
		SELECT
		$select,
		'bookshop' as `spacename`,
		'569e94192201a' AS `cid`
		FROM bookshops as obj
		WHERE %filter%
		UNION
		SELECT
		$select,
		'cadlib' as `spacename`,
		'569e94192201a' AS `cid`
		FROM cadlibs as obj
		WHERE %filter%
		UNION
		SELECT
		$select,
		'mockup' as `spacename`,
		'569e94192201a' AS `cid`
		FROM mockups as obj
		WHERE %filter%) AS containers";

		$sql = str_replace('%filter%', $filter->whereToString(), $sql) . $filter->orderToString();
		$this->stmt = $this->connexion->prepare($sql);
		$this->stmt->setFetchMode($this->options['mode']);
		$this->stmt->execute($bind);
		$this->isLoaded = true;
		$this->filter = $filter;
		return $this;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::countAll()
	 */
	public function countAll($filter, $bind = null)
	{
		$sql = "SELECT COUNT(*) FROM(
			SELECT id FROM workitems as obj
			WHERE %filter%
			UNION
			SELECT id FROM bookshops as obj
			WHERE %filter%
			UNION
			SELECT id FROM cadlibs as obj
			WHERE %filter%
			UNION
			SELECT id FROM mockups as obj
			WHERE %filter%
		) AS containers";

		$sql = str_replace('%filter%', $filter->whereToString(), $sql);
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		$count = $stmt->fetch();
		return $count[0];
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::save()
	 */
	public function save($list)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::delete()
	 */
	public function delete($filter)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::areExisting()
	 */
	public function areExisting($propNameToTest, $list, $returnSelect = null)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}
} /* End of class */
