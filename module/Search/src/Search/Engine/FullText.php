<?php
namespace Search\Engine;

/** SQL_SCRIPTS>>
 SET NAMES 'utf8' COLLATE 'utf8_general_ci';
 DROP PROCEDURE IF EXISTS fulltextSearchInGed;
 DELIMITER $$
 CREATE PROCEDURE `fulltextSearchInGed`(_what VARCHAR(256))
 BEGIN
 SELECT * FROM(
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`number` as `number`,
 `obj`.`name` as `name`,
 `obj`.`designation` as `designation`,
 `obj`.`lock_by_id` as lockById,
 `obj`.`locked` as locked,
 `obj`.`updated` as updated,
 `obj`.`update_by_id` as updateById,
 `obj`.`created` as created,
 `obj`.`create_by_id` as createById,
 "workitem" as `spacename`,
 "569e92709feb6" as `cid`
 FROM workitem_documents as obj
 LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
 WHERE MATCH(number,name,designation) AGAINST(_what IN NATURAL LANGUAGE MODE)
 OR MATCH(indexer.content) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`number` as `number`,
 `obj`.`name` as `name`,
 `obj`.`designation` as `designation`,
 `obj`.`lock_by_id` as lockById,
 `obj`.`locked` as locked,
 `obj`.`updated` as updated,
 `obj`.`update_by_id` as updateById,
 `obj`.`created` as created,
 `obj`.`create_by_id` as createById,
 "bookshop" as `spacename`,
 "569e92709feb6" as `cid`
 FROM bookshop_documents as obj
 LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
 WHERE MATCH(number,name,designation) AGAINST(_what IN NATURAL LANGUAGE MODE)
 OR MATCH(indexer.content) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`number` as `number`,
 `obj`.`name` as `name`,
 `obj`.`designation` as `designation`,
 `obj`.`lock_by_id` as lockById,
 `obj`.`locked` as locked,
 `obj`.`updated` as updated,
 `obj`.`update_by_id` as updateById,
 `obj`.`created` as created,
 `obj`.`create_by_id` as createById,
 "cadlib" as `spacename`,
 "569e92709feb6" as `cid`
 FROM cadlib_documents as obj
 LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
 WHERE MATCH(number,name,designation) AGAINST(_what IN NATURAL LANGUAGE MODE)
 OR MATCH(indexer.content) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`number` as `number`,
 `obj`.`name` as `name`,
 `obj`.`designation` as `designation`,
 `obj`.`lock_by_id` as lockById,
 `obj`.`locked` as locked,
 `obj`.`updated` as updated,
 `obj`.`update_by_id` as updateById,
 `obj`.`created` as created,
 `obj`.`create_by_id` as createById,
 "mockup" as `spacename`,
 "569e92709feb6" as `cid`
 FROM mockup_documents as obj
 LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
 WHERE MATCH(number,name,designation) AGAINST(_what IN NATURAL LANGUAGE MODE)
 OR MATCH(indexer.content) AGAINST(_what IN NATURAL LANGUAGE MODE)
 ) AS document
 -- ## DOCFILES
 UNION
 SELECT * FROM(
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`uid` as `number`,
 `obj`.`name` as `name`,
 "" as `designation`,
 `obj`.`lock_by_id` as `lockById`,
 `obj`.`locked` as `locked`,
 `obj`.`updated` as `updated`,
 `obj`.`update_by_id` as `updateById`,
 `obj`.`created` as `created`,
 `obj`.`create_by_id` as `createById`,
 "workitem" as `spacename`,
 "569e92b86d248" as `cid`
 FROM workitem_doc_files as obj
 WHERE MATCH(name) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`uid` as `number`,
 `obj`.`name` as `name`,
 "" as `designation`,
 `obj`.`lock_by_id` as `lockById`,
 `obj`.`locked` as `locked`,
 `obj`.`updated` as `updated`,
 `obj`.`update_by_id` as `updateById`,
 `obj`.`created` as `created`,
 `obj`.`create_by_id` as `createById`,
 "bookshop" as `spacename`,
 "569e92b86d248" as `cid`
 FROM bookshop_doc_files as obj
 WHERE MATCH(name) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`uid` as `number`,
 `obj`.`name` as `name`,
 "" as `designation`,
 `obj`.`lock_by_id` as `lockById`,
 `obj`.`locked` as `locked`,
 `obj`.`updated` as `updated`,
 `obj`.`update_by_id` as `updateById`,
 `obj`.`created` as `created`,
 `obj`.`create_by_id` as `createById`,
 "cadlib" as `spacename`,
 "569e92b86d248" as `cid`
 FROM cadlib_doc_files as obj
 WHERE MATCH(name) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`uid` as `number`,
 `obj`.`name` as `name`,
 "" as `designation`,
 `obj`.`lock_by_id` as `lockById`,
 `obj`.`locked` as `locked`,
 `obj`.`updated` as `updated`,
 `obj`.`update_by_id` as `updateById`,
 `obj`.`created` as `created`,
 `obj`.`create_by_id` as `createById`,
 "mockup" as `spacename`,
 "569e92b86d248" as `cid`
 FROM mockup_doc_files as obj
 WHERE MATCH(name) AGAINST(_what IN NATURAL LANGUAGE MODE)
 ) AS docfile;
 END$$
 DELIMITER ;


 -- ###################################################
 -- ###################################################
 SET NAMES 'utf8' COLLATE 'utf8_general_ci';
 DROP PROCEDURE IF EXISTS fulltextSearchInContainer;
 DELIMITER $$
 CREATE PROCEDURE `fulltextSearchInContainer`(_what VARCHAR(256))
 BEGIN
 SELECT * FROM(
 SELECT
 `workitems`.`id` AS `id`,
 `workitems`.`uid` AS `uid`,
 `workitems`.`number` AS `number`,
 `workitems`.`name` AS `name`,
 `workitems`.`designation` AS `designation`,
 '' AS `lockById`,
 '' AS `locked`,
 '' AS `updated`,
 '' AS `updateById`,
 `workitems`.`created` AS `created`,
 `workitems`.`create_by_id` AS `createById`,
 'workitem' AS `spacename`,
 '569e94192201a' AS `cid`
 FROM `workitems`
 WHERE MATCH(`name`, `number`, `designation`) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `bookshops`.`id` AS `id`,
 `bookshops`.`uid` AS `uid`,
 `bookshops`.`number` AS `number`,
 `bookshops`.`name` AS `name`,
 `bookshops`.`designation` AS `designation`,
 '' AS `lockById`,
 '' AS `locked`,
 '' AS `updated`,
 '' AS `updateById`,
 `bookshops`.`created` AS `created`,
 `bookshops`.`create_by_id` AS `createById`,
 'bookshop' AS `spacename`,
 '569e94192201a' AS `cid`
 FROM `bookshops`
 WHERE MATCH(`name`, `number`, `designation`) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `cadlibs`.`id` AS `id`,
 `cadlibs`.`uid` AS `uid`,
 `cadlibs`.`number` AS `number`,
 `cadlibs`.`name` AS `name`,
 `cadlibs`.`designation` AS `designation`,
 '' AS `lockById`,
 '' AS `locked`,
 '' AS `updated`,
 '' AS `updateById`,
 `cadlibs`.`created` AS `created`,
 `cadlibs`.`create_by_id` AS `createById`,
 'cadlib' AS `spacename`,
 '569e94192201a' AS `cid`
 FROM `cadlibs`
 WHERE MATCH(`name`, `number`, `designation`) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `mockups`.`id` AS `id`,
 `mockups`.`uid` AS `uid`,
 `mockups`.`number` AS `number`,
 `mockups`.`name` AS `name`,
 `mockups`.`designation` AS `designation`,
 '' AS `lockById`,
 '' AS `locked`,
 '' AS `updated`,
 '' AS `updateById`,
 `mockups`.`created` AS `created`,
 `mockups`.`create_by_id` AS `createById`,
 'mockup' AS `spacename`,
 '569e94192201a' AS `cid`
 FROM `mockups`
 WHERE MATCH(`name`, `number`, `designation`) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `projects`.`id` AS `id`,
 `projects`.`uid` AS `uid`,
 `projects`.`number` AS `number`,
 `projects`.`name` AS `name`,
 `projects`.`designation` AS `designation`,
 '' AS `lockById`,
 '' AS `locked`,
 '' AS `updated`,
 '' AS `updateById`,
 `projects`.`created` AS `created`,
 `projects`.`create_by_id` AS `createById`,
 'default' AS `spacename`,
 '569e93c6ee156' AS `cid`
 FROM `projects`
 WHERE MATCH(`name`, `number`, `designation`) AGAINST(_what IN NATURAL LANGUAGE MODE)
 ) AS container;
 END $$
 DELIMITER ;

 -- ###################################################
 -- ###################################################
 SET NAMES 'utf8' COLLATE 'utf8_general_ci';
 DROP PROCEDURE IF EXISTS fulltextSearchInFiles;
 DELIMITER $$
 CREATE PROCEDURE `fulltextSearchInFiles`(_what VARCHAR(256), _orderBy VARCHAR(64), _order, _pageOffset, _pageLimit)
 BEGIN
 SELECT * FROM (
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`uid` as `number`,
 `obj`.`name` as `name`,
 "" as `designation`,
 `obj`.`lock_by_id` as `lockById`,
 `obj`.`locked` as `locked`,
 `obj`.`updated` as `updated`,
 `obj`.`update_by_id` as `updateById`,
 `obj`.`created` as `created`,
 `obj`.`create_by_id` as `createById`,
 "workitem" as `spacename`,
 "569e92b86d248" as `cid`
 FROM workitem_doc_files as obj
 WHERE MATCH(name) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`uid` as `number`,
 `obj`.`name` as `name`,
 "" as `designation`,
 `obj`.`lock_by_id` as `lockById`,
 `obj`.`locked` as `locked`,
 `obj`.`updated` as `updated`,
 `obj`.`update_by_id` as `updateById`,
 `obj`.`created` as `created`,
 `obj`.`create_by_id` as `createById`,
 "bookshop" as `spacename`,
 "569e92b86d248" as `cid`
 FROM bookshop_doc_files as obj
 WHERE MATCH(name) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`uid` as `number`,
 `obj`.`name` as `name`,
 "" as `designation`,
 `obj`.`lock_by_id` as `lockById`,
 `obj`.`locked` as `locked`,
 `obj`.`updated` as `updated`,
 `obj`.`update_by_id` as `updateById`,
 `obj`.`created` as `created`,
 `obj`.`create_by_id` as `createById`,
 "cadlib" as `spacename`,
 "569e92b86d248" as `cid`
 FROM cadlib_doc_files as obj
 WHERE MATCH(name) AGAINST(_what IN NATURAL LANGUAGE MODE)
 UNION
 SELECT
 `obj`.`id` as `id`,
 `obj`.`uid` as `uid`,
 `obj`.`uid` as `number`,
 `obj`.`name` as `name`,
 "" as `designation`,
 `obj`.`lock_by_id` as `lockById`,
 `obj`.`locked` as `locked`,
 `obj`.`updated` as `updated`,
 `obj`.`update_by_id` as `updateById`,
 `obj`.`created` as `created`,
 `obj`.`create_by_id` as `createById`,
 "mockup" as `spacename`,
 "569e92b86d248" as `cid`
 FROM mockup_doc_files as obj
 WHERE MATCH(name) AGAINST(_what IN NATURAL LANGUAGE MODE)
 ) AS docfile;
 END$$
 DELIMITER ;

 -- ###################################################
 -- ###################################################
 SET NAMES 'utf8' COLLATE 'utf8_general_ci';
 DROP PROCEDURE IF EXISTS fulltextSearchInPdm;
 DELIMITER $$
 CREATE PROCEDURE `fulltextSearchInPdm`(_what VARCHAR(256))
 BEGIN
 SELECT
 `id` as `id`,
 `uid` as `uid`,
 `number` as `number`,
 `name` as `name`,
 `description` as `designation`,
 "" as `lockById`,
 "" as `locked`,
 "" as `updated`,
 "" as `updateById`,
 "" as `created`,
 "" as `createById`,
 `spacename` as `spacename`,
 "569e972dd4c2c" as `cid`
 FROM pdm_product_version
 WHERE MATCH(name, number, description) AGAINST(_what IN NATURAL LANGUAGE MODE)
 -- PRODUCT INSTANCE
 UNION
 SELECT
 `id` as `id`,
 `uid` as `uid`,
 `number` as `number`,
 `name` as `name`,
 `description` as `designation`,
 "" as `lockById`,
 "" as `locked`,
 "" as `updated`,
 "" as `updateById`,
 "" as `created`,
 "" as `createById`,
 'default' as `spacename`,
 "569e972dd4c2c" as `cid`
 FROM pdm_product_instance
 WHERE MATCH(name, number, description) AGAINST(_what IN NATURAL LANGUAGE MODE);
 END$$
 DELIMITER ;
 << */

/** SQL_ALTER>>
 ALTER TABLE `workitem_documents` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `cadlib_documents` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `mockup_documents` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `bookshop_documents` ADD FULLTEXT (number,name,designation);

 ALTER TABLE `workitem_doc_files` ADD FULLTEXT (name);
 ALTER TABLE `cadlib_doc_files` ADD FULLTEXT (name);
 ALTER TABLE `mockup_doc_files` ADD FULLTEXT (name);
 ALTER TABLE `bookshop_doc_files` ADD FULLTEXT (name);

 ALTER TABLE `pdm_product_version` ADD FULLTEXT (name, number, description);
 ALTER TABLE `pdm_product_instance` ADD FULLTEXT (name, number, description);

 ALTER TABLE `workitems` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `cadlibs` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `mockups` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `bookshops` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `projects` ADD FULLTEXT (number,name,designation);
 << */

/**
 *
 *
 */
class FullText
{

	/**
	 *
	 * @var \PDO
	 */
	protected $connexion;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'name' => 'name',
		'number' => 'number',
		'designation' => 'designation',
		'spacename' => 'spacename',
		'lockById' => 'lockById',
		'locked' => 'locked',
		'updateById' => 'updateById',
		'updated' => 'updated',
		'createById' => 'createById',
		'created' => 'created'
	);

	/**
	 *
	 * @param \PDO $connexion
	 */
	public function __construct($connexion)
	{
		$this->connexion = $connexion;
		$this->metaModel = static::$sysToApp;
	}

	/**
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @return \PDOStatement
	 */
	public function searchInGed(\Rbs\Dao\Sier\Filter $filter)
	{
		$sql = "SELECT * FROM(
		SELECT
		`obj`.`id` as `id`,
		`obj`.`uid` as `uid`,
		`obj`.`number` as `number`,
		`obj`.`name` as `name`,
		`obj`.`designation` as `description`,
		`obj`.`lock_by_id` as lockById,
		`obj`.`locked` as locked,
		`obj`.`updated` as updated,
		`obj`.`update_by_id` as updateById,
		`obj`.`created` as created,
		`obj`.`create_by_id` as createById,
		`obj`.`life_stage` as lifeStage,
		`obj`.`version` as version,
		`obj`.`iteration` as iteration,
		`obj`.`acode` as accessCode,
		`obj`.`doctype_id` as doctypeId,
		`obj`.`category_id` as categoryId,
		'workitem' as `spacename`,
		'569e92709feb6' as `cid`
		FROM workitem_documents as obj
		LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
		WHERE %filter%
		UNION
		SELECT
		`obj`.`id` as `id`,
		`obj`.`uid` as `uid`,
		`obj`.`number` as `number`,
		`obj`.`name` as `name`,
		`obj`.`designation` as `description`,
		`obj`.`lock_by_id` as lockById,
		`obj`.`locked` as locked,
		`obj`.`updated` as updated,
		`obj`.`update_by_id` as updateById,
		`obj`.`created` as created,
		`obj`.`create_by_id` as createById,
		`obj`.`life_stage` as lifeStage,
		`obj`.`version` as version,
		`obj`.`iteration` as iteration,
		`obj`.`acode` as accessCode,
		`obj`.`doctype_id` as doctypeId,
		`obj`.`category_id` as categoryId,
		'bookshop' as `spacename`,
		'569e92709feb6' as `cid`
		FROM bookshop_documents as obj
		LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
		WHERE %filter%
		UNION
		SELECT
		`obj`.`id` as `id`,
		`obj`.`uid` as `uid`,
		`obj`.`number` as `number`,
		`obj`.`name` as `name`,
		`obj`.`designation` as `description`,
		`obj`.`lock_by_id` as lockById,
		`obj`.`locked` as locked,
		`obj`.`updated` as updated,
		`obj`.`update_by_id` as updateById,
		`obj`.`created` as created,
		`obj`.`create_by_id` as createById,
		`obj`.`life_stage` as lifeStage,
		`obj`.`version` as version,
		`obj`.`iteration` as iteration,
		`obj`.`acode` as accessCode,
		`obj`.`doctype_id` as doctypeId,
		`obj`.`category_id` as categoryId,
		'cadlib' as `spacename`,
		'569e92709feb6' as `cid`
		FROM cadlib_documents as obj
		LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
		WHERE %filter%
		UNION
		SELECT
		`obj`.`id` as `id`,
		`obj`.`uid` as `uid`,
		`obj`.`number` as `number`,
		`obj`.`name` as `name`,
		`obj`.`designation` as `description`,
		`obj`.`lock_by_id` as lockById,
		`obj`.`locked` as locked,
		`obj`.`updated` as updated,
		`obj`.`update_by_id` as updateById,
		`obj`.`created` as created,
		`obj`.`create_by_id` as createById,
		`obj`.`life_stage` as lifeStage,
		`obj`.`version` as version,
		`obj`.`iteration` as iteration,
		`obj`.`acode` as accessCode,
		`obj`.`doctype_id` as doctypeId,
		`obj`.`category_id` as categoryId,
		'mockup' as `spacename`,
		'569e92709feb6' as `cid`
		FROM mockup_documents as obj
		LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
		WHERE %filter%) AS documents";

		$sql = str_replace('%filter%', $filter->whereToString(), $sql) . $filter->orderToString() . ' LIMIT 200';
		$stmt = $this->connexion->prepare($sql);

		$bind = array();
		$stmt->execute($bind);
		return $stmt;
	}

	/**
	 *
	 * @param string $what
	 * @return \PDOStatement
	 */
	public function searchInPdm($what)
	{
		$sql = "CALL fulltextSearchInPdm(:what)";
		$stmt = $this->connexion->prepare($sql);

		$bind = array(
			':what' => $what
		);
		$stmt->execute($bind);

		return $stmt;
	}

	/**
	 *
	 * @param string $what
	 * @return \PDOStatement
	 */
	public function searchInFiles($what)
	{
		$sql = "CALL fulltextSearchInFiles(:what)";
		$stmt = $this->connexion->prepare($sql);

		$bind = array(
			':what' => $what
		);
		$stmt->execute($bind);

		return $stmt;
	}

	/**
	 *
	 * @param string $what
	 * @return \PDOStatement
	 */
	public function searchInContainer($what)
	{
		$sql = "CALL fulltextSearchInContainer(:what)";
		$stmt = $this->connexion->prepare($sql);

		$bind = array(
			':what' => $what
		);
		$stmt->execute($bind);

		return $stmt;
	}
} /* End of class */
