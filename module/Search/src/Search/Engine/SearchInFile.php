<?php
namespace Search\Engine;

use Rbplm\Dao\DaoListAbstract;

/** SQL_SCRIPTS>>
 << */

/** SQL_ALTER>>
 ALTER TABLE `workitem_doc_files` ADD FULLTEXT (name);
 ALTER TABLE `cadlib_doc_files` ADD FULLTEXT (name);
 ALTER TABLE `mockup_doc_files` ADD FULLTEXT (name);
 ALTER TABLE `bookshop_doc_files` ADD FULLTEXT (name);
 << */

/**
 *
 *
 */
class SearchInFile extends DaoListAbstract
{

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'document_id' => 'parentId',
		'document_uid' => 'parentUid',
		'name' => 'name',
		'iteration' => 'iteration',
		'acode' => 'accessCode',
		'locked' => 'locked',
		'lock_by_id' => 'lockById',
		'created' => 'created',
		'create_by_id' => 'createById',
		'updated' => 'updated',
		'update_by_id' => 'updateById',
		'life_stage' => 'lifeStage',
		'path' => 'path',
		'root_name' => 'rootname',
		'extension' => 'extension',
		'type' => 'type',
		'size' => 'size',
		'mtime' => 'mtime',
		'md5' => 'md5',
		'mainrole' => 'mainrole',
		'roles' => 'roles'
	);

	/**
	 *
	 */
	public function __construct(\PDO $connexion = null)
	{
		parent::__construct(null, $connexion);
		$this->metaModel = static::$sysToApp;
	}

	/**
	 *
	 * @see \Rbplm\Dao\DaoInterface::load()
	 *
	 * @param \Rbplm\Dao\FilterAbstract $filter
	 * @param array $bind
	 * @return SearchInDocument
	 */
	public function load($filter = null, $bind = null)
	{
		if ( $this->isLoaded == true ) {
			$this->_reset();
		}

		$select = $filter->selectToString();
		if ( $select == '*' ) {
			$select = '
			`obj`.`id` as `id`,
			`obj`.`uid` as `uid`,
			`obj`.`name` as `name`,
			`obj`.`iteration` as `iteration`,
			`obj`.`acode` as `accessCode`,
			`obj`.`locked` as `locked`,
			`obj`.`lock_by_id` as `lockById`,
			`obj`.`created` as `created`,
			`obj`.`create_by_id` as `createById`,
			`obj`.`updated` as `updated`,
			`obj`.`update_by_id` as `updateById`,
			`obj`.`life_stage` as `lifeStage`,
			`obj`.`document_id` as `parentId`,
			`obj`.`document_uid` as `parentUid`,
			`obj`.`path` as `path`,
			`obj`.`root_name` as `rootname`,
			`obj`.`extension` as `extension`,
			`obj`.`type` as `type`,
			`obj`.`size` as `size`,
			`obj`.`mtime` as `mtime`,
			`obj`.`md5` as `md5`,
			`obj`.`mainrole` as `mainrole`,
			`obj`.`roles` as `roles`';
		}

		$sql = "SELECT * FROM(
		SELECT
		$select,
		'workitem' AS `spacename`,
		'569e92b86d248' AS `cid`
		FROM `workitem_doc_files` as obj
		WHERE %filter%
		UNION
		SELECT
		$select,
		'bookshop' as `spacename`,
		'569e92b86d248' AS `cid`
		FROM `bookshop_doc_files` as obj
		WHERE %filter%
		UNION
		SELECT
		$select,
		'cadlib' as `spacename`,
		'569e92b86d248' AS `cid`
		FROM `cadlib_doc_files` as obj
		WHERE %filter%
		UNION
		SELECT
		$select,
		'mockup' as `spacename`,
		'569e92b86d248' AS `cid`
		FROM `mockup_doc_files` as obj
		WHERE %filter%) AS containers";

		$sql = str_replace('%filter%', $filter->whereToString(), $sql) . ' ' . $filter->groupByToString() . $filter->orderToString();

		$this->stmt = $this->connexion->prepare($sql);
		$this->stmt->setFetchMode($this->options['mode']);
		$this->stmt->execute($bind);
		$this->isLoaded = true;
		$this->filter = $filter;
		return $this;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::countAll()
	 */
	public function countAll($filter, $bind = null)
	{
		$sql = "SELECT COUNT(*) FROM(
			SELECT id FROM workitem_doc_files as obj
			WHERE %filter%
			UNION
			SELECT id FROM bookshop_doc_files as obj
			WHERE %filter%
			UNION
			SELECT id FROM cadlib_doc_files as obj
			WHERE %filter%
			UNION
			SELECT id FROM mockup_doc_files as obj
			WHERE %filter%
		) AS containers";

		$sql = str_replace('%filter%', $filter->whereToString(), $sql);
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		$count = $stmt->fetch();
		return $count[0];
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::save()
	 */
	public function save($list)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::delete()
	 */
	public function delete($filter)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::areExisting()
	 */
	public function areExisting($propNameToTest, $list, $returnSelect = null)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}
} /* End of class */
