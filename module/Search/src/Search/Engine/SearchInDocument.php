<?php
namespace Search\Engine;

use Rbplm\Dao\Connexion as Connexion;

/** SQL_SCRIPTS>>
 << */

/** SQL_ALTER>>
 ALTER TABLE `workitem_documents` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `cadlib_documents` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `mockup_documents` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `bookshop_documents` ADD FULLTEXT (number,name,designation);
 ALTER TABLE `doctypes` ADD FULLTEXT (name, designation, file_extensions);
 << */

/**
 *
 *
 */
class SearchInDocument extends \Rbplm\Dao\DaoListAbstract
{

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'name' => 'name',
		'number' => 'number',
		'designation' => 'designation',
		'spacename' => 'spacename',
		'lockById' => 'lockById',
		'lockByUid' => 'lockByUid',
		'locked' => 'locked',
		'updateById' => 'updateById',
		'updateByUid' => 'updateByUid',
		'updated' => 'updated',
		'createById' => 'createById',
		'createByUid' => 'createByUid',
		'created' => 'created'
	);

	/**
	 *
	 */
	public function __construct(Connexion $connexion = null)
	{
		parent::__construct(null, $connexion);
		$this->metaModel = static::$sysToApp;
	}

	/**
	 *
	 * @see \Rbplm\Dao\DaoInterface::load()
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param array $bind
	 * @return SearchInDocument
	 */
	public function load($filter = null, $bind = null)
	{
		if ( $this->isLoaded == true ) {
			$this->_reset();
		}

		$sql = "SELECT * FROM(
		SELECT
		%select%,
		'workitem' as `spacename`,
		'569e92709feb6' as `cid`
		FROM workitem_documents as obj
		LEFT OUTER JOIN doctypes ON obj.doctype_id=doctypes.id
		LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
		WHERE %filter%
		UNION
		SELECT
		%select%,
		'bookshop' as `spacename`,
		'569e92709feb6' as `cid`
		FROM bookshop_documents as obj
		LEFT OUTER JOIN doctypes ON obj.doctype_id=doctypes.id
		LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
		WHERE %filter%
		UNION
		SELECT
		%select%,
		'cadlib' as `spacename`,
		'569e92709feb6' as `cid`
		FROM cadlib_documents as obj
		LEFT OUTER JOIN doctypes ON obj.doctype_id=doctypes.id
		LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
		WHERE %filter%
		UNION
		SELECT
		%select%,
		'mockup' as `spacename`,
		'569e92709feb6' as `cid`
		FROM mockup_documents as obj
		LEFT OUTER JOIN doctypes ON obj.doctype_id=doctypes.id
		LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
		WHERE %filter%) AS documents";

		$sql = str_replace('%filter%', $filter->whereToString(), $sql);
		$sql = str_replace('%select%', $filter->selectToString(), $sql);
		$sql = $sql . $filter->orderToString();

		//echo '<pre>'.$sql;die;

		$this->stmt = $this->connexion->prepare($sql);
		$this->stmt->setFetchMode($this->options['mode']);
		$this->stmt->execute($bind);
		$this->isLoaded = true;
		$this->filter = $filter;
		return $this;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::save()
	 */
	public function save($list)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::delete()
	 */
	public function delete($filter)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::countAll()
	 */
	public function countAll($filter, $bind = null)
	{
		$sql = "SELECT COUNT(*) FROM(
			SELECT obj.id FROM workitem_documents as obj
			LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
			LEFT OUTER JOIN doctypes ON obj.doctype_id=doctypes.id
			WHERE %filter%
			UNION
			SELECT obj.id FROM bookshop_documents as obj
			LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
			LEFT OUTER JOIN doctypes ON obj.doctype_id=doctypes.id
			WHERE %filter%
			UNION
			SELECT obj.id FROM cadlib_documents as obj
			LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
			LEFT OUTER JOIN doctypes ON obj.doctype_id=doctypes.id
			WHERE %filter%
			UNION
			SELECT obj.id FROM mockup_documents as obj
			LEFT OUTER JOIN indexer ON obj.id=indexer.document_id AND obj.spacename=indexer.spacename
			LEFT OUTER JOIN doctypes ON obj.doctype_id=doctypes.id
			WHERE %filter%
		) AS documents";

		$sql = str_replace('%filter%', $filter->whereToString(), $sql);
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		$count = $stmt->fetch();
		return $count[0];
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Rbplm\Dao\DaoListAbstract::areExisting()
	 */
	public function areExisting($propNameToTest, $list, $returnSelect = null)
	{
		throw new \BadMethodCallException('This method is not implemented for this class');
	}
} /* End of class */
