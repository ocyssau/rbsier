<?php
namespace Search;

class Module
{
	/**/
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	/**/
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}

	/**
	 *
	 */
	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'Indexer' => function($sm) {
					/* $sm must have a di rbFactory setted with Space\Factory object */
					/* @var \Rbs\Space\Factory $factory */
					$factory = $sm->rbFactory;
					$indexer = new \Search\Model\Indexer($factory);
					$indexer->serviceManager = $sm;
					return $indexer;
				},
			),
			'aliases' => array(),
			'abstract_factories' => array(),
			'invokables' => array(),
			'services' => array(),
			'shared' => array(),
		);
	}
}
