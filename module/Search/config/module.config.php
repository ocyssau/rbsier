<?php
return array(
	'router' => array(
		'routes' => array(
			'search' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/search[/:action]',
					'defaults' => array(
						'controller' => 'Search\Controller\Index',
						'action' => 'index'
					)
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/[:controller[/:action[/:id]]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
							),
							'defaults' => array()
						)
					)
				)
			),
			'solr' => [
				'type' => 'Segment',
				'options' => [
					'route' => '/solr[/:action]',
					'defaults' => [
						'controller' => 'Search\Controller\Solr',
						'action' => 'index'
					]
				]
			]
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Search\Controller\Index' => 'Search\Controller\IndexController',
			'Search\Controller\Solr' => 'Search\Controller\SolrController'
		)
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Search' => __DIR__ . '/../view'
		)
	),
	'search.solr.config' => [
		'secure' => false,
		'hostname' => 'localhost',
		'port' => 8080,
		'path' => 'solr',
		'wt' => 'json',
	]
);
