<?php

/**
 *
 */
return array(
	'router' => array(
		'routes' => array(
			'ged-document' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Manager',
						'action' => 'index'
					)
				)
			),
			'ged-document-manager' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/manager/:spacename/:action[/:id]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Manager',
						'action' => 'index'
					)
				)
			),
			'ged-document-manager-batch' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/manager/batch/:action',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Manager'
					)
				)
			),
			'ged-document-data' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/datamanager[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Datamanager',
						'action' => 'index'
					)
				)
			),
			'ged-document-data-params' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/datamanager/:spacename/:action[/:id]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Datamanager',
						'action' => 'index'
					)
				)
			),
			'ged-document-read' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/read[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Read',
						'action' => 'index'
					)
				)
			),
			'ged-document-lock' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/lockmanager[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Lockmanager',
						'action' => 'index'
					)
				)
			),
			'ged-document-history' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/history/:spacename[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\History\Document',
						'action' => 'index'
					)
				)
			),
			'ged-document-history-ofcontainer' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/history/ofcontainer/:spacename/:containerId[/:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\History\Container',
						'action' => 'index'
					)
				)
			),
			'ged-document-history-batch' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/history/batch/:spacename[/:action[/:uid]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\History\Batch',
						'action' => 'index'
					)
				)
			),
			'ged-document-history-batch-ofcontainer' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/history/batch/ofcontainer/:spacename/:containerId[/:uid]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\History\Batch',
						'action' => 'ofcontainer'
					)
				)
			),
			'ged-document-history-batch-index' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/history/indexofbatch/[:uid][/]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\History\Document',
						'action' => 'indexofbatch'
					)
				)
			),
			'ged-document-version' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/version[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Version',
						'action' => 'index'
					)
				)
			),
			'ged-document-permission' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/permission[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Permission',
						'action' => 'index'
					)
				)
			),
			'ged-document-share' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/share[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Share',
						'action' => 'index'
					)
				)
			),
			'ged-document-permanentlink' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/get/[:spacename]/[:documentId]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Viewer',
						'action' => 'permanent'
					)
				)
			),
			'ged-document-getlast' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/getlast/[:spacename]/[:number]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Viewer',
						'action' => 'getlast'
					)
				)
			),
			'ged-document-publiclink' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/shared/[:uid]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Shared',
						'action' => 'index'
					)
				)
			),
			'ged-document-viewer' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/viewer[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Viewer',
						'action' => 'index'
					)
				)
			),
			'ged-document-basket' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/basket[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Basket',
						'action' => 'index'
					)
				)
			),
			'ged-document-relation' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/relation/:direction/:spacename/:id',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Relation',
						'action' => 'getrelation'
					)
				)
			),
			'ged-document-detail' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/detail/[:spacename]/[:id]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Detail',
						'action' => 'index'
					)
				)
			),
			'ged-document-notification' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/notification[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Notification',
						'action' => 'index'
					)
				)
			),
			'ged-docfile' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/docfile[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Docfile\Manager',
						'action' => 'index'
					)
				)
			),
			'ged-docfile-manager' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/docfile/manager/:spacename/:action[/:id]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Docfile\Manager',
						'action' => 'index'
					)
				)
			),
			'ged-docfile-detail' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/docfile/detail/:spacename/:id[/:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Docfile\Detail',
						'action' => 'index'
					)
				)
			),
			'ged-docfile-iteration' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/docfile/iteration/:spacename/:id[/:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Docfile\Iteration',
						'action' => 'index'
					)
				)
			),
			'ged-docfile-version' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/docfile/version/:spacename/:id/[:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Docfile\Version'
					)
				)
			),
			'ged-doctype' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/doctype[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Doctype\Index',
						'action' => 'index'
					)
				)
			),
			'ged-doctype-callback' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/doctype/callback/[:doctypeId][/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Doctype\Callback',
						'action' => 'index'
					)
				)
			),
			'ged-category' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/category[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Category\Index',
						'action' => 'index'
					)
				)
			),
			'ged-category-ajax-getlist' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/service/category/ajax/getlist'
				)
			),
			'ged-metadata' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/metadata/:spacename[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Metadata\Index',
						'action' => 'index'
					)
				)
			),
			'ged-container-activate' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/session/activate/:spacename/:id',
					'defaults' => array(
						'controller' => 'Ged\Controller\Session',
						'action' => 'activate'
					)
				)
			),
			'ged-container-current' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/session/current',
					'defaults' => array(
						'controller' => 'Ged\Controller\Session',
						'action' => 'current'
					)
				)
			),
			'ged-container-notification' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/container/notification[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Notification',
						'action' => 'index'
					)
				)
			),
			'ged-container-permission' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/container/permission[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Permission',
						'action' => 'assign'
					)
				)
			),
			'ged-workitem' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/workitem[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Workitem',
						'action' => 'index'
					)
				)
			),
			'ged-cadlib' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/cadlib[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Cadlib',
						'action' => 'index'
					)
				)
			),
			'ged-bookshop' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/bookshop[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Bookshop',
						'action' => 'index'
					)
				)
			),
			'ged-mockup' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/mockup[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Mockup',
						'action' => 'index'
					)
				)
			),
			'ged-container-detail' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/container/detail/:spacename/:id[/:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Detail',
						'action' => 'index'
					)
				)
			),
			'ged-container-alias' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/container/alias[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Alias',
						'action' => 'index'
					)
				)
			),
			'ged-container-history' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/container/history[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\History',
						'action' => 'index'
					)
				)
			),
			'ged-container-categories' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/container/categories[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Linkcategory',
						'action' => 'index'
					)
				)
			),
			'ged-container-doctypes' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/container/doctypes[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Linkdoctype',
						'action' => 'index'
					)
				)
			),
			'ged-container-metadatas' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/container/metadatas[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Linkmetadata',
						'action' => 'index'
					)
				)
			),
			'ged-container-processes' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/container/processes[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Linkprocess',
						'action' => 'index'
					)
				)
			),
			'ged-project-detail' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/project/detail/:id[/:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Project\Detail',
						'action' => 'index'
					)
				)
			),
			'ged-project' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/project[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Project\Index',
						'action' => 'index'
					)
				)
			),
			'ged-project-container' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/project/container/[:spacename][/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Project\Linkcontainer',
						'action' => 'index'
					)
				)
			),
			'ged-project-doctypes' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/project/doctypes[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Project\Linkdoctype',
						'action' => 'index'
					)
				)
			),
			'ged-project-categories' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/project/categories[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Project\Linkcategory',
						'action' => 'index'
					)
				)
			),
			'ged-project-processes' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/project/processes[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Project\Linkprocess',
						'action' => 'index'
					)
				)
			),
			'ged-project-permission' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/project/permission[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Project\Permission',
						'action' => 'index'
					)
				)
			),
			'ged-document-smartstore' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/smartstore[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Smartstore',
						'action' => 'store'
					)
				)
			),
			'ged-document-archiver' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/document/archiver[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Archiver\Archiver',
						'action' => 'index'
					)
				)
			),
			'ged-document-archive-media' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/archiver/media[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Archiver\Media',
						'action' => 'index'
					)
				)
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Ged\Controller\Index' => 'Ged\Controller\IndexController',
			'Ged\Controller\Document\Manager' => 'Ged\Controller\Document\ManagerController',
			'Ged\Controller\Document\Datamanager' => 'Ged\Controller\Document\DatamanagerController',
			'Ged\Controller\Document\Lockmanager' => 'Ged\Controller\Document\LockmanagerController',
			'Ged\Controller\Document\Read' => 'Ged\Controller\Document\ReadController',
			'Ged\Controller\Document\Version' => 'Ged\Controller\Document\VersionController',
			'Ged\Controller\Document\Permission' => 'Ged\Controller\Document\PermissionController',
			'Ged\Controller\Docfile\Manager' => 'Ged\Controller\Docfile\ManagerController',
			'Ged\Controller\Docfile\Iteration' => 'Ged\Controller\Docfile\IterationController',
			'Ged\Controller\Docfile\Version' => 'Ged\Controller\Docfile\VersionController',
			'Ged\Controller\Docfile\Detail' => 'Ged\Controller\Docfile\DetailController',
			'Ged\Controller\Document\Viewer' => 'Ged\Controller\Document\ViewerController',
			'Ged\Controller\Document\Basket' => 'Ged\Controller\Document\BasketController',
			'Ged\Controller\Document\Relation' => 'Ged\Controller\Document\RelationController',
			'Ged\Controller\Document\Detail' => 'Ged\Controller\Document\DetailController',
			'Ged\Controller\Document\History\Document' => 'Ged\Controller\Document\History\DocumentController',
			'Ged\Controller\Document\History\Container' => 'Ged\Controller\Document\History\ContainerController',
			'Ged\Controller\Document\History\Batch' => 'Ged\Controller\Document\History\BatchController',
			'Ged\Controller\Document\Share' => 'Ged\Controller\Document\ShareController',
			'Ged\Controller\Document\Shared' => 'Ged\Controller\Document\SharedController',
			'Ged\Controller\Document\Smartstore' => 'Ged\Smartstore\Controller\SmartstoreController',
			'Ged\Controller\Document\Notification' => 'Ged\Controller\Document\NotificationController',
			'Ged\Controller\Doctype\Index' => 'Ged\Controller\Doctype\IndexController',
			'Ged\Controller\Doctype\Callback' => 'Ged\Controller\Doctype\CallbackController',
			'Ged\Controller\Category\Index' => 'Ged\Controller\Category\IndexController',
			'Ged\Controller\Metadata\Index' => 'Ged\Controller\Metadata\IndexController',
			'Ged\Controller\Container\Notification' => 'Ged\Controller\Container\NotificationController',
			'Ged\Controller\Container\Workitem' => 'Ged\Controller\Container\WorkitemController',
			'Ged\Controller\Container\Cadlib' => 'Ged\Controller\Container\CadlibController',
			'Ged\Controller\Container\Bookshop' => 'Ged\Controller\Container\BookshopController',
			'Ged\Controller\Container\Mockup' => 'Ged\Controller\Container\MockupController',
			'Ged\Controller\Container\Alias' => 'Ged\Controller\Container\AliasController',
			'Ged\Controller\Container\Permission' => 'Ged\Controller\Container\PermissionController',
			'Ged\Controller\Container\History' => 'Ged\Controller\Container\HistoryController',
			'Ged\Controller\Container\Linkmetadata' => 'Ged\Controller\Container\LinkmetadataController',
			'Ged\Controller\Container\Linkdoctype' => 'Ged\Controller\Container\LinkdoctypeController',
			'Ged\Controller\Container\Linkcategory' => 'Ged\Controller\Container\LinkcategoryController',
			'Ged\Controller\Container\Linkprocess' => 'Ged\Controller\Container\LinkprocessController',
			'Ged\Controller\Container\Detail' => 'Ged\Controller\Container\DetailController',
			'Ged\Controller\Project\Index' => 'Ged\Controller\Project\IndexController',
			'Ged\Controller\Project\Detail' => 'Ged\Controller\Project\DetailController',
			'Ged\Controller\Project\Permission' => 'Ged\Controller\Project\PermissionController',
			'Ged\Controller\Project\Linkcontainer' => 'Ged\Controller\Project\LinkcontainerController',
			'Ged\Controller\Project\Linkdoctype' => 'Ged\Controller\Project\LinkdoctypeController',
			'Ged\Controller\Project\Linkprocess' => 'Ged\Controller\Project\LinkprocessController',
			'Ged\Controller\Project\Linkcategory' => 'Ged\Controller\Project\LinkcategoryController',
			'Ged\Controller\Archiver\Archiver' => 'Ged\Controller\Archiver\ArchiverController',
			'Ged\Controller\Archiver\Media' => 'Ged\Controller\Archiver\MediaController',
			'Ged\Controller\Session' => 'Ged\Controller\SessionController'
		)
	),
	'view_manager' => [
		'template_path_stack' => [
			'Ged' => __DIR__ . '/../view'
		]
	],
	'contextual_menu' => [
		'document' => [
			'share' => [
				'menu' => 'Share',
				'class' => 'submenu',
				'items' => [
					[
						'name' => 'document-share',
						'assert' => 'multiSelect==false || multiSelect==true',
						'class' => 'document-share-btn',
						'url' => '#',
						'title' => 'Share document',
						'label' => 'Share'
					]
				]
			]
		]
	]
);
