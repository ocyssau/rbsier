<?php
// %LICENCE_HEADER%
namespace Ged\Smartstore;

use Zend\Hydrator\HydratorInterface;

/**
 */
class DocfileHydrator implements HydratorInterface
{

	/**
	 * Hydrate $object with the provided $data.
	 *
	 * @param array $data        	
	 * @param object $object        	
	 * @return object
	 */
	public function hydrate(array $data, $object)
	{
		$object->hydrate($data);
		(isset($data['fsdata'])) ? $object->fsdata = $data['fsdata'] : null;
		(isset($data['document'])) ? $object->document = $data['document'] : null;
		(isset($data['comment'])) ? $object->comment = $data['comment'] : null;
		(isset($data['checkerResult'])) ? $object->checkerResult = $data['checkerResult'] : null;
		(isset($data['deleteAfterStore'])) ? $object->deleteAfterStore = $data['deleteAfterStore'] : null;
		(isset($data['action'])) ? $object->action = $data['action'] : null;
		return $object;
	}

	/**
	 * Extract values from an object
	 *
	 * @param object $object        	
	 * @return array
	 */
	public function extract($object)
	{
		return $object->getArrayCopy();
	}
}
