<?php
// %LICENCE_HEADER%
namespace Ged\Smartstore;

use Zend\Hydrator\HydratorInterface;

/**
 *
 *
 */
class Hydrator implements HydratorInterface
{

	/**
	 * Hydrate Collection of docfiles
	 * Hydrate $object with the provided $data.
	 *
	 * @param $properties array
	 * @param  $object DocfileCollection
	 * @return object
	 */
	public function hydrate(array $properties, $object)
	{
		(isset($properties['comment'])) ? $object->comment = $properties['comment'] : null;
		(isset($properties['validate'])) ? $object->validate = $properties['validate'] : null;
		(isset($properties['cancel'])) ? $object->cancel = $properties['cancel'] : null;
		(isset($properties['containerId'])) ? $object->containerId = $properties['containerId'] : null;
		return $object;
	}

	/**
	 * Extract values from an object
	 *
	 * @param object $object
	 * @return array
	 */
	public function extract($object)
	{
		return $object->getArrayCopy();
	}
}
