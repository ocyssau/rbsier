<?php
namespace Ged\Smartstore;

use Ged\Smartstore\Processor\Respons;
use Exception;
use Ranchbe;
use Rbplm\Ged\AccessCode;
use Rbs\Space\Factory as DaoFactory;
use Zend\Http\Request;

/**
 * 
 * Processor class for process actions on SmartStoreForm validation.
 * When run is called, route to adequats methods.
 * The resons object carries the messages and the results of the processor.
 * 
 * @todo: add events to extend capabilies on demands.
 *
 */
class Processor
{

	/**
	 *
	 * @var \Ged\Smartstore\Processor\Respons
	 */
	public $respons;

	/**
	 *
	 * @var \Rbs\Batch\Batch
	 */
	protected $batch;

	/**
	 *
	 * @var boolean
	 */
	protected $options = array(
		'keepAsCo' => false,
		'deleteAfterStore' => true
	);

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function __construct()
	{
		$this->respons = new Respons();
	}

	/**
	 * @param string $name
	 * @param string $value
	 */
	public function setOption($name, $value)
	{
		$this->options[$name] = $value;
		return $this;
	}

	/**
	 * @param \Rbs\Batch\Batch $batch
	 * @return Processor
	 */
	public function setBatch(\Rbs\Batch\Batch $batch)
	{
		$this->batch = $batch;
		return $this;
	}

	/**
	 *
	 * @return \Rbs\Pdm\Product\Service|\Rbs\Ged\Document\Service
	 */
	protected function getPdmService()
	{
		if ( !isset($this->pdmService) ) {
			$this->pdmService = new \Rbs\Pdm\Product\Service($this->factory);
		}
		return $this->pdmService;
	}

	/**
	 * Run action
	 *
	 * $document->dao must be setted before call this method
	 * $docfile->dao must be setted before call this method
	 * $docfile->fsdata must be setted before call this method
	 *
	 * @param string $todo
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 * @return Processor
	 * @throws Exception
	 *
	 */
	public function run($todo, $document, $docfile)
	{
		if ( isset($this->batch) ) {
			$document->batch = $this->batch;
			$document->comment = $this->batch->getComment();
		}
		else {
			$document->batch = null;
		}

		if ( !isset($document->factory) ) {
			$spacename = $document->spacename;
			$document->factory = DaoFactory::get($spacename);
		}

		if ( !isset($document->comment) ) {
			$document->comment = '';
		}

		switch ($todo) {
			/* Ignore */
			case Checker::TODO_IGNORE:
				return true;
				break;
			case Checker::TODO_CHECKIN:
				$releasing = true;
				$deleteFileInWs = true;
				return $this->checkinAction($document, $docfile, $releasing, $deleteFileInWs);
				break;
			case Checker::TODO_CHECKINANDKEEP:
				$releasing = false;
				$deleteFileInWs = false;
				return $this->checkinAction($document, $docfile, $releasing, $deleteFileInWs);
				break;
			case Checker::TODO_UPDATE:
				$deleteFileInWs = false;
				return $this->updateAction($document, $docfile, $deleteFileInWs);
				break;
			/* Update metadatas only */
			case Checker::TODO_EDIT:
				$document->dao->save($document);
				break;
			/* Add the file to the document */
			case Checker::TODO_ADDFILE:
				try {
					Ranchbe::get()->getServiceManager()
						->getDocumentService($document->factory)
						->addFileToDocument($document, $docfile, $docfile->fsdata);
					$document->dao->save($document);
				}
				catch( \PDOException $e ) {
					throw new \Exception('DbERROR, Unable to associate file to document: ' . $e->getMessage(), E_USER_ERROR, $e);
				}
				catch( \Rbplm\Sys\Exception $e ) {
					throw new \Exception('Unable to associate file to document: ' . $e->getMessage(), E_USER_ERROR, $e);
				}
				catch( \Exception $e ) {
					throw new \Exception('Unable to associate file to document', E_USER_ERROR, $e);
				}

				try {
					$docfile->fsdata->delete();
				}
				catch( \Exception $e ) {
					throw new \Exception('File has been associated but can not be deleted from wildspace', $e->getCode(), $e);
				}
				break;
			/* Create a new document */
			case Checker::TODO_CREATE:
				return $this->createAction($document, $docfile);
				break;
			/* Create a new version and update the files if specifed */
			case Checker::TODO_VERSION:
				/* use the wildspace file as new data for new version */
				Ranchbe::get()->getServiceManager()
					->getDocumentService($document->factory)
					->newVersion($document);
				break;
		} /* End of switch */
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 * @param boolean $releasing  Checkin and unlock the document 
	 * @param boolean $deleteFileInWs delete file in wildspace after checkin
	 * @return Processor
	 * @throws Exception
	 */
	protected function checkinAction($document, $docfile, $releasing = true, $deleteFileInWs = true)
	{
		$comment = $document->comment;

		try {
			$request = new Request();
			$request->setMethod(Request::METHOD_POST)
				->getPost()
				->fromArray([
				'documents' => [
					'document1' => array(
						'object' => $document,
						'comment' => $comment,
						'checkinandkeep' => !$releasing,
						'pdmdata' => null
					)
				],
				'batch' => $this->batch
			]);

			$service = new \Service\Controller\Document\LockmanagerService();
			$return = $service->checkinService($request);
			if ( $return->hasErrors() ) {
				throw current($return->getErrors());
			}
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
			$this->respons->addError($e->getMessage() . ' <p class="rb-debugdata">' . $e->getFile() . ' line:' . $e->getLine() . '</p>');
		}
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 * @return Processor
	 * @throws Exception
	 */
	protected function updateAction($document, $docfile, $deleteFileInWs = true)
	{
		try {
			/* load docfiles */
			$dfDao = $document->factory->getDao(\Rbplm\Ged\Docfile\Version::$classId);
			$dfDao->loadDocfiles($document, $dfDao->toSys('accessCode') . '<> :accessCode', array(
				':accessCode' => AccessCode::CHECKOUT
			));

			/* Update document datas */
			$deleteFileInWs = false;
			$fromWildspace = true;
			$checkAccess = true;
			$service = Ranchbe::get()->getServiceManager()->getDocumentService($document->factory);
			$service->batch = $this->batch;
			$ret = $service->update($document, $deleteFileInWs, $fromWildspace, $checkAccess);
			$this->respons = $ret->respons;
		}
		catch( \Exception $e ) {
			$this->respons->addError($e->getMessage() . ' <p class="rb-debugdata">' . $e->getFile() . ' line:' . $e->getLine() . '</p>');
		}
	}

	/**
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 * @return Processor
	 * @throws Exception
	 */
	protected function createAction($document, $docfile)
	{
		$keepAsCo = $this->options['keepAsCo'];
		$defaultContainer = $document->getParent();

		/* In case of user choice of target different of context */
		if ( $defaultContainer->getId() != $document->getParent(true) ) {
			$containerId = $document->getParent(true);
			$loader = $defaultContainer->loader;
			$container = $loader->loadFromId($containerId, \Rbplm\Org\Workitem::$classId);
			$document->setParent($container);
		}

		/* Create document */
		$this->respons = Ranchbe::get()->getServiceManager()
			->getDocumentService($document->factory)
			->createDocument($document, $keepAsCo)->respons;

		/* Create a new product */
		if ( isset($document->product) ) {
			$document->product->setDocument($document);
			$this->getPdmService()->create($document->product, false);
		}
	}
} /* End of class */
