<?php
namespace Ged\Smartstore\Checker;

use Ged\Smartstore\Checker\Exception as CheckerException;
use Rbplm\People;
use Rbplm\Ged\AccessCode;
use Rbplm\Dao\NotExistingException;
use Rbplm\Ged\Doctype;
use Exception;

/**
 * Helper to check existance of a entity
 */
class Document extends \Ged\Smartstore\Checker
{

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param string $name
	 */
	public function check($document, $name)
	{
		/* */
		$this->getResult($document);

		/* Check if docfile is existing */
		try {
			$this->notExisting($document, $name);
		}
		/* Is exisiting */
		catch( CheckerException $e ) {
			$this->setResult($document, 'feedbacks', 'Document is existing with the same ref in db');
			$this->setResult($document, 'todo', self::TODO_EDIT);

			/* Is exisiting */
			try {
				$this->isLocked($document);
				$this->setResult($document, 'todo', self::TODO_UPDATE);
			}
			catch( CheckerException $e ) {
				$this->setResult($document, 'feedback', $e->getMessage());

				/* checkout by me */
				if ( $e->getCode() == 1020 ) {
					$this->setResult($document, 'todo', self::TODO_CHECKIN);
				}
				/* is locked */
				elseif ( $e->getCode() == 1022 || $e->getCode() == 1023 ) {
					$this->setResult($document, 'todo', self::TODO_VERSION);
				}
				/* is not free or not changed */
				else {
					$this->setResult($document, 'todo', self::TODO_IGNORE);
				}
			}
		}
		/* Is Not exisiting */
		catch( NotExistingException $e ) {
			/* Check the doctype for the new document */
			try {
				$fsdata = $document->docfile->fsdata;
				/* Associate docfile to document, needed by Service::createDocument*/
				$document->addDocfile($document->docfile);
				$this->checkDoctype($document, $fsdata);
			}
			catch( CheckerException $e ) {
				$document->checkerResult = null;
				$this->setResult($document, 'error', $e->getMessage());
				$this->setResult($document, 'todo', self::TODO_IGNORE);
				return true;
			}

			try {
				$this->checkNumberMask($document, $name);
			}
			catch( CheckerException $e ) {
				$this->setResult($document, 'error', $e->getMessage());
				$this->setResult($document, 'todo', self::TODO_IGNORE);
				return true;
			}

			$this->setResult($document, 'todo', self::TODO_CREATE);
		}

		return true;
	}

	/**
	 * 
	 * The number is conform
	 * 
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param string $name
	 */
	public function checkNumberMask($document, $name)
	{
		if ( $document->id ) {
			return $this;
		}

		$normalize = \Ranchbe::get()->getConfig('document.name.normalize');
		if ( $normalize ) {
			$number = \Rbs\Number::normalize($name);
		}
		else {
			$number = $name;
		}

		$regex = '/' . \Ranchbe::get()->getConfig('document.name.mask') . '/';
		$document->nameRegex = $regex;
		if ( preg_match($regex, $number) ) {
			$document->nameMask = true;
		}
		else {
			$document->nameMask = false;
		}

		$document->setName($name)->setNumber($number);

		if ( $document->nameMask == false ) {
			$msg = sprintf('The name of the document is not conform. See the regex %s', $regex);
			throw new CheckerException($msg, 1010);
		}
	}

	/**
	 * Ref is existing ?
	 *
	 * If exisiting, populate property $model->checkerResult['existingModel']
	 * Get only the last version.
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param string $name
	 */
	public function notExisting($document, $name)
	{
		if ( $document->getId() == null ) {
			try {
				$document->dao->loadFromName($document, $name);
			}
			catch( NotExistingException $e ) {
				throw new NotExistingException($e->getMessage(), 100, $e);
			}
			catch( \Exception $e ) {
				throw $e;
			}
		}

		$this->setResult($document, 'existingModel', $document);
		throw new CheckerException('document is existing', 1000);
	}

	/**
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param \Rbplm\Sys\Fsdata $fsdata
	 */
	public function checkDoctype($document, $fsdata)
	{
		try {
			$doctype = new Doctype();
			$doctype->dao = $this->factory->getDao($doctype->cid);

			/**/
			$lnkDao = $this->factory->getDao(\Rbs\Org\Container\Link\Doctype::$classId);
			$fileExtension = $fsdata->getExtension();
			$fileType = $fsdata->getType();

			/* */
			if ( $parent = $document->getParent() ) {
				$lnkDao->loadFromDocument($doctype, $parent->getDn(), $document->getNumber(), $fileExtension, $fileType);
				$document->setDoctype($doctype);
			}
			else {
				throw new Exception('document parent is not loaded');
			}
		}
		catch( \Rbplm\Dao\NotExistingException $e ) {
			throw new CheckerException('Doctype is not founded for this file');
		}
		catch( \Exception $e ) {
			throw new CheckerException('Doctype is not valide for this reason: ' . $e->getMessage());
		}

		return true;
	}

	/**
	 * @param \Rbplm\Ged\Document\Version $document
	 */
	public function isLocked($document)
	{
		$accessCode = $document->checkAccess();

		/* is free */
		if ( $accessCode == AccessCode::FREE ) {
			return true;
		}

		/* Is checkouted */
		if ( $accessCode == AccessCode::CHECKOUT ) {
			/* Checkouted by current user */
			$coUser = $document->lockById;
			if ( $coUser == People\CurrentUser::get()->getId() ) {
				throw new CheckerException(sprintf('Document %s is checkouted by you', $document->getName()), 1020);
			}
			else {
				throw new CheckerException(sprintf('Document %s is checkouted by %s', $document->getName(), $coUser), 1021);
			}
		}

		/* Is in workflow */
		if ( $accessCode >= AccessCode::INWORKFLOW ) {
			throw new CheckerException(sprintf('Document %s is locked by a workflow', $document->getName()), 1024);
		}

		/* Is locked */
		if ( $accessCode >= AccessCode::LOCKEDLEVEL4 ) {
			throw new CheckerException(sprintf('Document %s is locked as "%s"', $document->getName(), AccessCode::getName($accessCode)), 1022);
		}

		/* Others */
		throw new CheckerException(sprintf('Document %s is not free, is locked as "%s"', $document->getName(), AccessCode::getName($accessCode)), 1023);
	}
} /* End of class */
