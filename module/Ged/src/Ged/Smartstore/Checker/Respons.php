<?php
namespace Ged\Smartstore\Checker;

/**
 */
class Respons
{

	/** @var array Some datas */
	protected $datas;

	/** @var array Informatives messages list */
	protected $feedbacks = array();

	/** @var array Errors messages list */
	protected $errors = array();

	/** @var array Alerts messages list */
	protected $alerts = array();

	/** @var \Exception The last exception */
	protected $exception;

	/** @var string */
	protected $todo;

	/** 
	 * public array where values are name of available actions
	 * 
	 * @var array 
	 */
	public $actions = array();

	/** @var \Rbplm\Ged\Document\Version */
	//public $document;

	/** @var \Rbplm\Ged\Docfile\Version */
	//public $docfile;

	/** @var \Rbplm\Sys\Fsdata */
	//public $fsdata;

	/** @var \Rbplm\Org\Workitem */
	//public $container;

	/** @var string */
	//public $file;

	/** @var string */
	//public $spacename;

	/**
	 * 
	 */
	public function __construct()
	{
		$this->datas = [
			'existingModel' => null,
			'md5' => [
				false,
				null,
				null
			]
		];
	}

	/**
	 *
	 * @param \Exception $e
	 * @return Respons
	 */
	public function setException($e)
	{
		$this->exception = $e;
		return $this;
	}

	/**
	 *
	 * @return \Exception
	 */
	public function getException()
	{
		return $this->exception;
	}

	/**
	 * Converted associated exception as array.
	 * 
	 * @param \Exception $e
	 * @return array
	 */
	public static function exceptionToArray(\Exception $e)
	{
		$return = array(
			'message' => $e->getMessage(),
			'code' => $e->getCode(),
			'file' => $e->getFile(),
			'line' => $e->getLine()
		);
		return $return;
	}

	/**
	 * @param string $name
	 */
	public function setTodo($name)
	{
		$this->todo = $name;
		$this->actions[$name] = $name;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getTodo()
	{
		return $this->todo;
	}

	/**
	 *
	 * @param string $name
	 * @param mixed $datas
	 */
	public function setData($name, $datas)
	{
		$this->datas[$name] = $datas;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 *
	 * @return string
	 */
	public function getData($name)
	{
		isset($this->datas) ? $r = $this->datas[$name] : $r = null;
		return $r;
	}

	/**
	 *
	 * @param string $msg
	 * @return Respons
	 */
	public function addError($msg)
	{
		$this->errors[] = $msg;
		return $this;
	}

	/**
	 *
	 * @param string $msg
	 * @return Respons
	 */
	public function hasErrors()
	{
		return (count($this->errors) > 0);
	}

	/**
	 *
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 *
	 * @param string $msg
	 */
	public function addFeedback($msg)
	{
		$this->feedbacks[] = $msg;
		return $this;
	}

	/**
	 *
	 * @param string $index
	 * @return boolean;
	 */
	public function hasFeedbacks()
	{
		return (count($this->feedbacks) > 0);
	}

	/**
	 *
	 * @return array
	 */
	public function getFeedbacks()
	{
		return $this->feedbacks;
	}

	/**
	 *
	 * @param string $msg
	 */
	public function addAlert($msg)
	{
		$this->alerts[] = $msg;
		return $this;
	}

	/**
	 *
	 * @param string $index
	 * @return boolean;
	 */
	public function hasAlerts()
	{
		return (count($this->alerts) > 0);
	}

	/**
	 *
	 * @return array
	 */
	public function getAlerts()
	{
		return $this->alerts;
	}

	/**
	 * 
	 * @param array $properties
	 * @return Respons
	 */
	public function hydrate($properties)
	{
		(isset($properties['feedbacks'])) ? $this->feedbacks = $properties['feedbacks'] : null;
		(isset($properties['alerts'])) ? $this->alerts = $properties['alerts'] : null;
		(isset($properties['errors'])) ? $this->errors = $properties['errors'] : null;
		(isset($properties['exception'])) ? $this->exception = $properties['exception'] : null;
		(isset($properties['todo'])) ? $this->todo = $properties['todo'] : null;
		(isset($properties['actions'])) ? $this->actions = $properties['actions'] : null;
		return $this;
	}
} /* End of class */
