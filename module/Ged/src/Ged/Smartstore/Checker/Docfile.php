<?php
namespace Ged\Smartstore\Checker;

use Ged\Smartstore\Checker\Exception as CheckerException;
use Ged\Smartstore\Checker\Md5Exception as CheckerMd5Exception;
use Rbplm\Dao\NotExistingException;
use Rbplm\People;
use Rbplm\Ged\AccessCode;

/**
 * Helper to check existance of a entity
 */
class Docfile extends \Ged\Smartstore\Checker
{

	/**
	 * Check
	 *
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 * @param string $fileName
	 */
	public function check($docfile, $fileName)
	{
		$this->getResult($docfile);

		/* Check if docfile is existing */
		try {
			$this->notExisting($docfile, $fileName);
		}
		/* Is exisiting */
		catch( CheckerException $e ) {
			$this->setResult($docfile, 'feedbacks', 'Data is existing with the same ref in db');
			try {
				$this->isMd5Equal($docfile);
				$this->isLocked($docfile);
				$this->setResult($docfile, 'feedbacks', '<p class="alert alert-danger"> *** Vaulted data must be updated *** </p>');
				$this->setResult($docfile, 'todo', self::TODO_UPDATE);
			}
			/* widlspace and vault files are equals */
			catch( CheckerMd5Exception $e ) {
				$this->setResult($docfile, 'feedback', $e->getMessage());
				$this->setResult($docfile, 'md5', [
					true,
					$docfile->getData()->md5,
					$docfile->fsdata->getMd5()
				]);
			}
			/* is locked */
			catch( CheckerException $e ) {
				$this->setResult($docfile, 'feedback', $e->getMessage());

				/* checkout by me */
				if ( $e->getCode() == 1020 ) {
					$this->setResult($docfile, 'todo', self::TODO_CHECKIN);
				}
				/* is locked */
				elseif ( $e->getCode() == 1022 || $e->getCode() == 1023 ) {
					$this->setResult($docfile, 'todo', self::TODO_VERSION);
				}
				/* is not free or not changed */
				else {
					$this->setResult($docfile, 'todo', self::TODO_IGNORE);
				}
			}
			$this->setResult($docfile, 'todo', self::TODO_EDIT);
		}
		/* Is Not exisiting */
		catch( NotExistingException $e ) {
			$this->setResult($docfile, 'todo', self::TODO_CREATE);
		}

		return true;
	}

	/**
	 *
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return Docfile
	 */
	public function associateToDocument($docfile, $document)
	{
		/* */
		$this->getResult($docfile);

		/* associate docfile to document */
		$document->docfile = $docfile;
		$document->addDocfile($document->docfile);
		$docfile->setParent($document);

		/* set associate file action */
		$this->setResult($docfile, 'todo', self::TODO_ADDFILE);

		return $this;
	}

	/**
	 * Ref is existing ?
	 *
	 * If exisiting, populate property $model->checkerResult['existingModel'].
	 * Get only the last version.
	 *
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 * @param string $fileName
	 */
	public function notExisting($docfile, $fileName)
	{
		if ( $docfile->getId() == null ) {
			try {
				$documentId = null;
				$iterationId = null;
				$parent = $docfile->getParent();
				if ( $parent ) {
					$documentId = $parent->getId();
				}
				$docfile->dao->loadFromName($docfile, $fileName, $iterationId, $documentId);
			}
			catch( NotExistingException $e ) {
				throw new NotExistingException($e->getMessage(), 100, $e);
			}
			catch( \Exception $e ) {
				throw $e;
			}
		}

		$this->setResult($docfile, 'existingModel', $docfile);
		throw new CheckerException('data is existing', 1000);
	}

	/**
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 */
	public function isMd5Equal($docfile)
	{
		if ( $docfile->getData()->md5 == $docfile->fsdata->getMd5() ) {
			throw new CheckerMd5Exception(sprintf('files %s from wildspace and from vault are strictly identicals', $docfile->fsdata->getName()), 1005);
		}
	}

	/**
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 */
	public function isLocked($docfile)
	{
		$accessCode = $docfile->checkAccess();

		/* is free */
		if ( $accessCode == AccessCode::FREE ) {
			return true;
		}

		/* Is checkouted */
		if ( $accessCode == AccessCode::CHECKOUT ) {
			/* Checkouted by current user */
			$coUser = $docfile->lockById;
			if ( $coUser == People\CurrentUser::get()->getId() ) {
				throw new CheckerException(sprintf('docfile %s is checkouted by you', $docfile->getName()), 1020);
			}
			else {
				throw new CheckerException(sprintf('docfile %s is checkouted by %s', $docfile->getName(), $coUser), 1021);
			}
		}

		/* Is locked */
		if ( $accessCode >= AccessCode::LOCKEDLEVEL4 ) {
			throw new CheckerException(sprintf('docfile %s is locked', $docfile->getName()), 1022);
		}

		/* Others */
		throw new CheckerException(sprintf('docfile %s is not free', $docfile->getName()), 1023);
	}
} /* End of class */
