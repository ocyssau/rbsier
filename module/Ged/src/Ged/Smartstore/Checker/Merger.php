<?php
namespace Ged\Smartstore\Checker;

//use Ged\Smartstore\Checker\Exception as CheckerException;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\AccessCode;
use Ged\Smartstore\Checker\Docfile as DocfileChecker;

/**
 * 
 * Merge result of check on Document and Docfile
 *
 */
class Merger
{

	/**
	 * Check
	 *
	 * @param Docfile\Version $docfile
	 * @param Document\Version $document
	 */
	public function merge(Docfile\Version $docfile, Document\Version $document)
	{
		$result = $docfile->checkerResult;

		$actions = array_merge($docfile->checkerResult->actions, $document->checkerResult->actions);
		$feedbacks = array_merge($docfile->checkerResult->getFeedbacks(), $document->checkerResult->getFeedbacks());
		$errors = array_merge($docfile->checkerResult->getErrors(), $document->checkerResult->getErrors());
		$alerts = array_merge($docfile->checkerResult->getAlerts(), $document->checkerResult->getAlerts());
		$md5IsEqual = $result->getData('md5')[0];

		if ( $md5IsEqual ) {
			$result->setTodo(DocfileChecker::TODO_IGNORE);
			unset($actions['version']);
			unset($actions['update']);
			unset($actions['checkin']);
			$alerts = [
				'File of wildspace is strictly the same than file in vault'
			];
		}
		else {
			/*
			 * Check Document/Docfile couple
			 * Document existings, not docfile, Document is free
			 */
			if ( $document->getId() && !$docfile->getId() ) {
				/* the document can be versionned with new docfile */
				if ( isset($document->checkerResult->actions['version']) ) {
					$feedbacks = [];
					$feedbacks[] = 'Docfile is not existing but document may be versionned';
					$feedbacks[] = 'Create new version of document with new docfile';
					unset($actions[DocfileChecker::TODO_CREATE]);
					$result->setTodo(DocfileChecker::TODO_VERSION);
				}
				else {
					if ( $document->checkAccess() == AccessCode::FREE ) {
						$feedbacks[] = sprintf('File may be associated to document %s.v%s (#%s)', $document->getNumber(), $document->version, $document->getId());
						$result->setTodo(DocfileChecker::TODO_ADDFILE);
						$actions[DocfileChecker::TODO_ADDFILE] = DocfileChecker::TODO_ADDFILE;
					}
					else {
						$feedbacks[] = 'Document is not free';
						$result->setTodo(DocfileChecker::TODO_IGNORE);
					}
					unset($actions[DocfileChecker::TODO_CREATE]);
				}
			}

			/* update only if available on doc and df */
			if ( isset($docfile->checkerResult->actions['update']) && !isset($document->checkerResult->actions['update']) ) {
				unset($actions['update']);
			}
			if ( isset($document->checkerResult->actions['update']) && !isset($docfile->checkerResult->actions['update']) ) {
				unset($actions['update']);
			}
			if ( isset($document->checkerResult->actions['version']) && !isset($docfile->checkerResult->actions['version']) ) {
				unset($actions['version']);
			}
		}

		$result->hydrate([
			'actions' => $actions,
			'feedbacks' => $feedbacks,
			'errors' => $errors,
			'alerts' => $alerts
		]);

		return $result;
	}
} /* End of class */
