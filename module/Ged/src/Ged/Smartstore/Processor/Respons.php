<?php
namespace Ged\Smartstore\Processor;

/**
 *
 */
class Respons
{

	/** @var array Some datas */
	protected $datas;

	/** @var array Informatives messages list */
	protected $feedbacks = array();

	/** @var array Errors messages list */
	protected $errors = array();

	/** @var \Exception The last exception */
	protected $exception;

	/**
	 *
	 * @param \Exception $e
	 * @return Respons
	 */
	public function setException($e)
	{
		$this->exception = $e;
		return $this;
	}

	/**
	 *
	 * @return \Exception
	 */
	public function getException()
	{
		return $this->exception;
	}

	/**
	 * @param \Exception $e
	 */
	public static function exceptionToArray(\Exception $e)
	{
		$return = array(
			'message' => $e->getMessage(),
			'code' => $e->getCode(),
			'file' => $e->getFile(),
			'line' => $e->getLine()
		);
		return $return;
	}

	/**
	 *
	 * @param string $name
	 * @param mixed $datas
	 */
	public function setData($name, $datas)
	{
		$this->datas[$name] = $datas;
	}

	/**
	 * @return array
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 * @param string $msg
	 * @return Respons
	 */
	public function addError($msg)
	{
		$this->errors[] = $msg;
		return $this;
	}

	/**
	 * @param string $msg
	 * @return Respons
	 */
	public function hasErrors()
	{
		return (count($this->errors) > 0);
	}

	/**
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 *
	 * @param string $msg
	 */
	public function addFeedback($msg)
	{
		$this->feedbacks[] = $msg;
		return $this;
	}

	/**
	 *
	 * @param string $index
	 * @return boolean;
	 */
	public function hasFeedbacks()
	{
		return (count($this->feedbacks) > 0);
	}

	/**
	 * @return array
	 */
	public function getFeedbacks()
	{
		return $this->feedbacks;
	}
}
