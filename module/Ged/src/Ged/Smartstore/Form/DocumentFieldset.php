<?php
namespace Ged\Smartstore\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Ged\Smartstore\DocumentHydrator as Hydrator;

/**
 *
 *
 */
class DocumentFieldset extends Fieldset implements InputFilterProviderInterface
{
	use \Application\Form\ExtendableTrait;
	use \Application\Form\InputFilterProviderTrait;

	/**
	 *
	 * @param string $name
	 * @param \Rbs\Space\Factory $daoFactory
	 */
	public function __construct($name, $daoFactory)
	{
		/* we want to ignore the name passed */
		parent::__construct($name);

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setObject(new \Rbplm\Ged\Document\Version());

		/* hidden */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'uid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'isFrozen',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->inputFilterSpecification = array(
			'id' => array(
				'required' => false
			),
			'uid' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			),
			'isFrozen' => array(
				'required' => false
			)
		);

		/* Number */
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control number',
				'size' => 32,
				'readonly' => true
			),
			'options' => array(
				'label' => tra('Number')
			)
		));
		$mask = \Ranchbe::get()->getConfig('document.name.mask');
		$numberValidators = array(
			array(
				'name' => 'StringLength',
				'options' => array(
					'encoding' => 'UTF-8',
					'min' => 1,
					'max' => 255
				)
			)
		);
		if ( $mask ) {
			$help = \Ranchbe::get()->getConfig('document.name.mask.help');
			$numberValidators[] = array(
				'name' => 'Regex',
				'options' => array(
					'pattern' => '/' . $mask . '/i',
					'messages' => array(
						\Zend\Validator\Regex::NOT_MATCH => $help
					)
				)
			);
			$this->numberHelp = $help;
		}
		$this->inputFilterSpecification['number'] = array(
			'required' => true,
			'filters' => array(
				array(
					'name' => 'StripTags'
				),
				array(
					'name' => 'StringTrim'
				)
			),
			'validators' => $numberValidators
		);

		/* Version */
		$this->add(array(
			'name' => 'version',
			'type' => 'Application\Form\Element\SelectVersion',
			'attributes' => array(
				'class' => 'form-control duplicable version',
				'multiple' => false
			),
			'options' => array(
				'label' => tra('Version')
			)
		));
		$this->inputFilterSpecification['version'] = array(
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StringTrim'
				),
				array(
					'name' => 'ToNull'
				)
			)
		);

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'class' => 'form-control duplicable description',
				'size' => 32,
				'rows' => 2,
				'cols' => 32
			),
			'options' => array(
				'label' => tra('Description')
			)
		));
		$this->inputFilterSpecification['description'] = array(
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StripTags'
				),
				array(
					'name' => 'StringTrim'
				)
			),
			'validators' => array(
				array(
					'name' => 'StringLength',
					'options' => array(
						'encoding' => 'UTF-8',
						'min' => 1,
						'max' => 512
					)
				)
			)
		);

		/* Category */
		$this->add(array(
			'name' => 'categoryId',
			'type' => 'Application\Form\Element\SelectCategory',
			'attributes' => array(
				'class' => 'form-control duplicable category',
				'multiple' => false
			),
			'options' => array(
				'label' => tra('Category'),
				'daoFactory' => $daoFactory,
				'maybenull' => true
			)
		));
		$this->inputFilterSpecification['categoryId'] = array(
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StringTrim'
				),
				array(
					'name' => 'ToNull'
				)
			)
		);

		/* Container */
		$this->add(array(
			'name' => 'parentId',
			'type' => 'Application\Form\Element\SelectContainer',
			'attributes' => array(
				'class' => 'form-control duplicable containerId',
				'multiple' => false
			),
			'options' => array(
				'label' => tra('Container'),
				'daoFactory' => $daoFactory
			)
		));
		$this->inputFilterSpecification['parentId'] = array(
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StringTrim'
				),
				array(
					'name' => 'ToNull'
				)
			)
		);

		/* Tags */
		$this->add(array(
			'name' => 'tags',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control rb-tagsinput',
				'size' => 16
			),
			'options' => array(
				'label' => tra('Tags')
			)
		));
		$this->inputFilterSpecification['tags'] = array(
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StringTrim'
				),
				array(
					'name' => 'ToNull'
				)
			)
		);
	}
} /* End of class */
