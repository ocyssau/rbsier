<?php
namespace Ged\Smartstore\Form;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Ged\Smartstore\Hydrator as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class SmartstoreForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($daoFactory)
	{
		/* we want to ignore the name passed */
		parent::__construct('smartstoreLayout');
		$this->template = 'ged/smartstore/storeform';

		$this->setAttribute('method', 'post')
			->setAttribute('class', 'form-inline')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'containerId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'deleteAfterStore',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Set files collection */
		$fileForm = new \Ged\Smartstore\Form\DocfileFieldset('fileForm', $daoFactory);
		$fileForm->setUseAsBaseFieldset(true);
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'docfiles',
			'options' => array(
				'should_create_template' => false,
				'allow_add' => true,
				'target_element' => $fileForm,
				'use_as_base_fieldset'
			)
		));

		/* Send to basket */
		$this->add(array(
			'name' => 'sendToBasket',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'value' => 1,
				'class' => 'form-check-input'
			),
			'options' => array(
				'label' => tra('Put documents in basket')
			)
		));

		/* keepAsCo */
		$this->add(array(
			'name' => 'keepAsCo',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'value' => 0,
				'class' => 'form-check-input'
			),
			'options' => array(
				'label' => 'Keep Checkouted'
			)
		));

		/* Comment */
		$this->add(array(
			'name' => 'comment',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'class' => 'form-control comment',
				'style' => 'resize:horizontal;width:100%;height:60px;'
			),
			'options' => array(
				'label' => tra('Comment')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'button',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success btn-save'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'button',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default btn-cancel'
			)
		));

		/* Prepare data button */
		$this->add(array(
			'name' => 'preparedata',
			'attributes' => array(
				'type' => 'button',
				'value' => tra('Prepare Data'),
				'id' => 'preparebutton',
				'class' => 'btn btn-primary btn-preparedata'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'containerId' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			),
			'files' => array(
				'required' => false
			),
			'comment' => array(
				'required' => true
			)
		);
	}

	/**
	 *
	 * @param \Ged\Smartstore\DocfileCollection $object
	 * @return \Ged\Smartstore\Form\DocfileFieldset
	 */
	public function bind($object, $flags = "")
	{
		/* add some elements before bind */
		$extended = current($object->getIterator())->getParent()->dao->extended;
		$this->get('docfiles')
			->getTargetElement()
			->get('parent')
			->setExtended($extended);

		/* add extended properties */
		$ret = parent::bind($object);
		$actions = array();

		foreach( $object->getIterator() as $name => $item ) {
			$actions = array_merge($item->checkerResult->actions, $actions);
			$docfileFieldset = $this->get('docfiles')->get($name);
			$documentFieldset = $docfileFieldset->get('parent');

			$docfileFieldset->populateActionsSelect($item->checkerResult->actions);

			/* Disable container selector */
			if ( isset($item->lockContainerSelector) && $item->lockContainerSelector ) {
				$documentFieldset->get('parentId')->setAttribute('disabled', true);
			}
		}

		/* for validate, all options must be set to the targetElement */
		$this->get('docfiles')
			->getTargetElement()
			->populateActionsSelect($actions);
		return $ret;
	}
}
