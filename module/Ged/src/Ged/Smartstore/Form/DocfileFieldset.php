<?php
namespace Ged\Smartstore\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Ged\Smartstore\DocfileHydrator as Hydrator;

/**
 *
 *
 */
class DocfileFieldset extends Fieldset implements InputFilterProviderInterface
{
	use \Application\Form\InputFilterProviderTrait;

	/**
	 *
	 * @param string $name
	 * @param \Rbs\Space\Factory $daoFactory
	 */
	public function __construct($name, $daoFactory)
	{
		/* we want to ignore the name passed */
		parent::__construct($name);

		$obj = new \Rbplm\Ged\Docfile\Version();
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setObject($obj);

		/* hidden */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'uid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'isFrozen',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'deleteAfterStore',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* File name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control fileName',
				'size' => 32,
				'readonly' => true
			),
			'options' => array(
				'label' => tra('File')
			)
		));

		/* Document Fieldset */
		$documentFieldset = new DocumentFieldset('parent', $daoFactory);
		$documentFieldset->setName('parent');
		$this->add($documentFieldset);

		/* Actions */
		$this->add(array(
			'name' => 'action',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control duplicable action',
				'multiple' => false
			),
			'options' => array(
				'label' => tra('Action'),
				'options' => array()
			)
		));

		$this->inputFilterSpecification = array(
			'id' => array(
				'required' => false
			),
			'uid' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			),
			'deleteAfterStore' => array(
				'required' => false
			),
			'action' => array(
				'required' => true
			),
			'name' => array(
				'required' => true,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 255
						)
					)
				)
			)
		);
	}

	/**
	 *
	 * @param array $array
	 * @return \Ged\Smartstore\Form\DocfileFieldset
	 */
	public function populateActionsSelect($array)
	{
		$actionDefs = \Ged\Smartstore\Checker::getActionDefinitions();

		foreach( $array as $todo ) {
			$r[$todo] = $actionDefs[$todo];
		}
		$this->get('action')->setValueOptions($r);
		return $this;
	}
} /* End of class */
