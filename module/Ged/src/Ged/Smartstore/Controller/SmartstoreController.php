<?php
namespace Ged\Smartstore\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Sys\Fsdata;
use Rbplm\Org\Workitem;
use Rbs\Extended;
use Rbplm\People;
use Application\Controller\ControllerException as Exception;
use Ged\Smartstore\Checker\Document as DocumentChecker;
use Ged\Smartstore\Checker\Docfile as DocfileChecker;

/**
 * @author ocyssau
 *
 * When user request a store on files collection, call storeAction first.
 * Docfiles from input files are grouped in DocfileCollection.
 * Then, check docfiles and documents (if exists, doctypes, data integrity...). This task is dedicate to Checker class.
 * Checker load docfiles and documents if existing.
 * Checker init a Checker\Respons object, and populate it with actions todos, messages...
 * The Checker\Respons object is attached to Document::checkerResult and Docfile::checkerResult property.
 *
 */
class SmartstoreController extends AbstractController
{

	/** @var string */
	public $pageId = 'document_smartstore';

	/** @var string */
	public $defaultSuccessForward = 'workplace/wildspace/index';

	/** @var string */
	public $defaultFailedForward = 'workplace/wildspace/index';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		$context = \Ranchbe::get()->getServiceManager()->getContext();
		$this->containerId = $context->getData('containerId');
		$this->spacename = $context->getData('spacename');

		/* Rbgate url */
		$this->view->rbgateServerUrl = \Ranchbe::get()->getConfig('rbgate.server.url');

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;

		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * In POST datas :
	 * IN GET datas :
	 * ['spacename'] = string,
	 * ['containerid'] = integer,
	 * ['deleteAfterStore'] => true,
	 * ['keepAsCo'] => true,
	 * ['docfiles'] => array(
	 * 		'file1'=> array(
	 * 			'uid' => string
	 * 			'name' => string /file name/
	 * 			'data'=>array(
	 * 				'data'=>'',
	 * 				'md5'=>'',
	 * 				'base64'=>'',
	 * 			),
	 * 			'action' => string
	 * 			'parent' => array( //document data
	 * 				'description' => int
	 * 				...
	 * 			),
	 * 			'container'=>array(
	 * 				'id'=>integer,
	 * 				'lockContainerSelector'=>boolean, //If true, disable container selector
	 * 			),
	 * 		),
	 * )
	 *
	 *
	 *
	 *
	 * @throws Exception
	 * @return mixed|\Application\View\ViewModel
	 */
	public function storeAction()
	{
		$view = $this->view;

		/** @var \Zend\Http\PhpEnvironment\Request $request */
		$request = $this->getRequest();

		/* Init some work */
		$user = People\CurrentUser::get();
		$wildspace = new People\User\Wildspace($user);
		$wsPath = $wildspace->getPath();
		$dataInputs = [];

		$options = [
			'deleteAfterStore' => false,
			'keepAsCo' => false
		];

		/* is POST */
		if ( $request->isPost() ) {
			$spacename = $request->getPost('spacename', $this->spacename);
			$containerId = $request->getPost('containerId', $this->containerId);
			$checked = $request->getPost('docfiles', []);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}

			if ( !$spacename && $this->spacename ) {
				$spacename = $this->spacename;
				$request->getPost()->set('spacename', $spacename);
				$request->getQuery()->set('spacename', $spacename);
			}

			foreach( $checked as $in ) {
				$dataInputs[] = [
					'fileName' => $in['name'],
					'spacename' => $spacename
				];
			}
		}
		/* is GET */
		elseif ( $request->isGet() ) {
			$options['deleteAfterStore'] = $request->getQuery('deleteAfterStore', $options['deleteAfterStore']);
			$options['keepAsCo'] = $request->getQuery('keepAsCo', $options['keepAsCo']);

			$spacenames = $request->getQuery('spacenames', []);
			$spacename = $request->getQuery('spacename', $this->spacename);
			$containerId = $request->getQuery('containerid', $request->getQuery('containerId', $this->containerId));
			$checked = $request->getQuery('docfiles', null);
			
			if ( $checked ) {
				foreach( $checked as $in ) {
					$filename = $in['name'];
					if ( isset($spacenames[$filename]) ) {
						$mySpacename = $spacenames[$filename];
					}
					else {
						$mySpacename = $spacename;
					}

					$dataInput = array(
						'fileName' => $in['name'],
						'data' => (isset($in['data'])) ? $in['data'] : null,
						'spacename' => $mySpacename,
						'lockContainerSelector' => false
					);

					if ( isset($in['parent']) ) {
						isset($in['parent']['description']) ? $dataInput['parent']['description'] = $in['parent']['description'] : null;
						isset($in['parent']['tags']) ? $dataInput['parent']['tags'] = $in['parent']['tags'] : null;
						isset($in['parent']['id']) ? $dataInput['parent']['id'] = $in['parent']['id'] : null;
					}

					if ( isset($in['container']) ) {
						$dataInput['container'] = $in['container'];
						isset($in['container']['lock']) ? $dataInput['lockContainerSelector'] = $in['container']['lock'] : null;
						isset($in['container']['spacename']) ? $dataInput['spacename'] = $in['container']['spacename'] : null;
					}

					$dataInputs[] = $dataInput;
				}
			}
			else {
				$checked = $request->getQuery('checked', []);
				foreach( $checked as $fileName ) {
					if ( isset($spacenames[$fileName]) ) {
						$mySpacename = $spacenames[$fileName];
					}
					else {
						$mySpacename = $spacename;
					}
					$dataInputs[] = [
						'fileName' => $fileName,
						'lockContainerSelector' => false,
						'spacename' => $mySpacename
					];
				}
			}
			
			/* some files must be select */
			if ( count($dataInputs) == 0 ) {
				$this->errorStack()->error('You must select at least one file');
				$this->errorForward();
			}

			/* limit selection */
			$maxBatchSize = \Ranchbe::get()->getConfig('document.smartstore.maxsize');
			if ( count($dataInputs) > $maxBatchSize ) {
				$chunkSelect = array_chunk($dataInputs, $maxBatchSize, true);
				$dataInputs = $chunkSelect[0];
				unset($chunkSelect);
				$msg = 'Sorry but your selection is too large and has been trunk to %s elements';
				$msg .= 'Change the document.smartstore.maxsize configuration directive to increase this value';
				$this->errorStack()->warning(sprintf($msg, $maxBatchSize));
			}

			unset($checked, $fileName, $chunkSelect);
		} /* End is GET */

		
		if ( !$containerId ) {
			$this->errorStack()->error('None container is set. The containerId param is not set in request');
			return $this->errorForward();
		}
		if(count($dataInputs) == 0){
			return $this->successForward();
		}

		/* Init some helpers */
		$factory = DaoFactory::get($spacename);
		$loader = new \Rbs\Dao\Loader($factory);
		$collection = new \Ged\Smartstore\DocfileCollection();
		$documentChecker = new DocumentChecker($factory);
		$docfileChecker = new DocfileChecker($factory);
		$extendsloader = new Extended\Loader($factory);

		/* Init some work */
		$defaultContainer = $loader->loadFromId($containerId, Workitem::$classId);
		$defaultContainer->loader = $loader;
		
		$count = 0;
		foreach( $dataInputs as $dataInput ) {
			$fileName = $dataInput['fileName'];
			if ( !$fileName ) {
				continue;
			}
			$file = $wsPath . '/' . $fileName;

			/* get data from requests and put it in a file */
			if ( isset($dataInput['data']) && $dataInput['data'] ) {
				$data = $dataInput['data']['data'];
				$md5 = $dataInput['data']['md5'];
				$encode = $dataInput['data']['base64'];
				if ( $encode == 'base64' ) {
					$data = base64_decode($data);
				}
				/* check md5 */
				if ( md5($data) != $md5 ) {
					throw new Exception('Data Md5 checksum error');
				}
				file_put_contents($file, $data);
			}

			/* init Docfile */
			$docfile = Docfile\Version::init(); //522
			$fsdata = new Fsdata($file);
			$docfile->fsdata = new Fsdata($file);
			$docfile->dao = $factory->getDao($docfile->cid);

			/* set options */
			$docfile->deleteAfterStore = (int)$options['deleteAfterStore'];

			/* Check docfile, populate $docfile->checkerResult */
			$docfileChecker->check($docfile, $fileName);

			/* If docfile is existing, get his parent document */
			if ( $docfile->getId() > 0 ) {
				$document = new Document\Version();
				$documentId = $docfile->getParent(true);
				$factory->getDao($document->cid)->loadFromId($document, $documentId);
				$document->docfile = $docfile;
				$documentChecker->check($document, null);
				/* Check permission */
				$this->getAcl()->checkRightFromUid('edit', $document->parentUid);
			}
			else {
				/* */
				$name = $fsdata->getRootname();
				$docfile->setName($fsdata->getName())
					->setNumber($fsdata->getName());

				/* Init Document objects from document id requested */
				if ( isset($dataInput['parent']['id']) ) {
					$document = new Document\Version();
					$documentId = $dataInput['parent']['id'];
					$factory->getDao($document->cid)->loadFromId($document, $documentId);

					/**/
					if ( $document->getParent(true) == $defaultContainer->getId() ) {
						$document->setParent($defaultContainer);
					}
					else {
						$container = $loader->loadFromId($document->getParent(true), Workitem::$classId);
						$container->loader = $loader;
						$document->setParent($container);
					}

					/* Associate docfile to requested document */
					$document->docfile = $docfile;
					$docfileChecker->associateToDocument($docfile, $document);
					$documentChecker->check($document, $name);

					/* Check permission */
					$this->getAcl()->checkRightFromUid('edit', $document->parentUid);
				}
				/* build a new document */
				else {
					$document = Document\Version::init();
					$document->dao = $factory->getDao($document->cid);
					$document->setParent($defaultContainer);
					$document->spacename = $dataInput['spacename'];
					$document->docfile = $docfile;
					$documentChecker->check($document, $name);

					/* set some default properties from request datas */
					if ( isset($dataInput['parent']['description']) ) {
						$document->description = $dataInput['parent']['description'];
					}
					if ( isset($dataInput['parent']['tags']) ) {
						$document->setTags($dataInput['parent']['tags']);
					}
				}

				/* Check permission */
				$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

				if ( $document->checkerResult->hasErrors() ) {
					$docfile->checkerResult = $document->checkerResult;
					goto afterdfcheck;
				}
			}

			/* Merge available actions */
			$checkerMerger = new \Ged\Smartstore\Checker\Merger();
			$docfile->checkerResult = $checkerMerger->merge($docfile, $document);

			/* lock container selector */
			if ( isset($dataInput['lockContainerSelector']) ) {
				$docfile->lockContainerSelector = $dataInput['lockContainerSelector'];
			}
			else {
				unset($docfile->lockContainerSelector);
			}

			/*
			 * Extended properties
			 * @todo : If create, the extends must be set from selected container,
			 * ,add ajax loader to display extends.
			 */
			$extendsloader->loadFromContainerId($defaultContainer->getId(), $document->dao);

			afterdfcheck:
			/* Init new document */
			if ( $document->getId() == null ) {
				$document->setParent($defaultContainer);
			}
			/* If existing document */
			else {
				$containerId = $document->getParent(true);
				/* Load container */
				try {
					$container = $loader->loadFromId($containerId, Workitem::$classId);
				}
				catch( \Rbplm\Dao\NotExistingException $e ) {
					throw new Exception(sprintf('Container #%s is not existing', $containerId), E_ERROR, $e, $this);
				}
				$document->setParent($container);
			}

			/* set document as parent of docfile */
			$docfile->setParent($document);
			$collection->add($docfile);

			$count++;
		} /* foreach end */
		// $sessionCaddie = serialize($collection);

		/* Init form */
		$form = new \Ged\Smartstore\Form\SmartstoreForm($factory);
		$form->setAttribute('action', $this->url()
			->fromRoute('ged-document-smartstore', array(
			'action' => 'store'
		)));

		/* Call extract() from Hydrator and populate value attributes of all form elements, recursively on fieldsets.
		 * And populate actions options from permit options defined by checkers
		 */
		$collection->containerId = $containerId;
		$collection->spacename = $spacename;
		$form->bind($collection);

		/* Submit */
		if ( $request->isPost() && $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$this->errorStack()->getFlash()->clearMessagesFromContainer();

				/* goto next */
				return $this->forward()->dispatch('Ged\Controller\Document\Smartstore', array(
					'action' => 'run',
					'documents' => $collection
				));
			}
			else {
				foreach( $form->getMessages() as $msg ) {
					$msg[] = 'FORM VALIDATION ERROR';
					$this->errorStack()->error(json_encode($msg, true));
				}
			}
		}
		else {
			if ( $request->getQuery('docfiles', false) ) {
				$form->setData($request->getQuery());
			}
		}

		/* Display template */
		$view->setTemplate($form->template);
		$this->containerid = $containerId;
		$this->spacename = $spacename;
		$view->form = $form;
		$view->wildspacePath = $wsPath;
		$view->pageTitle = tra('Store files in') . ' ' . $defaultContainer->getName();
		return $view;
	}

	/**
	 *
	 */
	public function runAction()
	{
		/** @var \Zend\Http\PhpEnvironment\Request $request */
		$request = $this->getRequest();
		$factory = DaoFactory::get($this->spacename);
		$processor = new \Ged\Smartstore\Processor($factory);

		/**/
		$options = [
			'deleteAfterStore' => false,
			'keepAsCo' => false
		];

		/* */
		$user = People\CurrentUser::get();
		$wildspace = $user->wildspace;
		$processor->wildspace = $wildspace;

		/* get forwarded extracted model */
		$collection = $this->params()->fromRoute('documents');
		$comment = $collection->comment;

		/* Read inputs if is POST */
		if ( $request->isPost() ) {
			$sendToBasket = $request->getPost('sendToBasket', null);
			$options['deleteAfterStore'] = $request->getPost('deleteAfterStore', $options['deleteAfterStore']);
			$options['keepAsCo'] = $request->getPost('keepAsCo', $options['keepAsCo']);
		}

		if ( $sendToBasket ) {
			$sendToBasketInput = array(
				'documents' => array()
			);
		}

		/* set options */
		$processor->setOption('keepAsCo', $options['keepAsCo']);
		$processor->setOption('deleteAfterStore', $options['deleteAfterStore']);

		/* Batch init */
		$batch = new \Service\Batch\StoreBatch();
		$batch->newUid()->setOwner(People\CurrentUser::get());
		$batch->setComment($comment);
		DaoFactory::get()->getDao($batch::$classId)->save($batch);

		/* batch is put in processor to be send to services */
		$processor->setBatch($batch);

		/** @var \Zend\EventManager $eventManager */
		$eventManager = $this->getEvent()
			->getApplication()
			->getEventManager();

		$index = 1;
		foreach( $collection->getIterator() as $docfile ) {
			/* @var \Rbplm\Ged\Document\Version $document */
			$document = $docfile->getParent();
			$todo = $docfile->action;

			/* Run processor */
			try {
				$processor->run($todo, $document, $docfile);
				if ( $processor->respons->hasErrors() ) {
					foreach( $processor->respons->getErrors() as $msg ) {
						$this->errorStack()->error($msg);
					}
				}
				else {
					if ( $sendToBasket ) {
						$sendToBasketInput['document' . $index] = array(
							'id' => $document->getId(),
							'spacename' => $document->getParent()->spacename
						);
					}
				}
				if ( $processor->respons->hasFeedbacks() ) {
					foreach( $processor->respons->getFeedbacks() as $msg ) {
						$this->errorStack()->feedback($msg);
					}
				}
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
			$index++;
		}

		/* Send to basket if require */
		if ( $sendToBasket ) {
			try {
				$basketRequest = clone ($request);
				$basketRequest->setMethod($basketRequest::METHOD_POST)
					->getPost()
					->fromArray($sendToBasketInput);
				$service = new \Service\Controller\Document\BasketService();
				$service->addService($basketRequest);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
			}
		}

		// return $this->redirect()->toRoute('wildspace'); //lost of flash messanger, whyyyyyyy!!!!
		return $this->redirectTo('workplace/wildspace/index');
	}
} /* End of class */