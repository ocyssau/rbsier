<?php
// %LICENCE_HEADER%
namespace Ged\Smartstore;

use Rbplm\Any;

/**
 */
class DocumentCollection extends Any implements \Countable
{

	static $classId = 'documentcollection';

	/**
	 *
	 * @var array
	 */
	public $documents = array();

	/**
	 *
	 * @param array $properties
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['documents'])) ? $this->documents = $properties['documents'] : null;
		return $this;
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $doc
	 */
	public function add($doc)
	{
		$this->documents[$doc->getUid()] = $doc;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function count()
	{
		return count($this->documents);
	}

	/**
	 * @return array
	 */
	public function getIterator()
	{
		return $this->documents;
	}
}
