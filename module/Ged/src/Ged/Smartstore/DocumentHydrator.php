<?php
// %LICENCE_HEADER%
namespace Ged\Smartstore;

use Zend\Hydrator\HydratorInterface;

/**
 */
class DocumentHydrator extends \Rbs\Model\Hydrator\Extendable implements HydratorInterface
{

	/**
	 * Hydrate $object with the provided $data.
	 *
	 * @param array $data
	 * @param object $object
	 * @return object
	 */
	public function hydrate(array $data, $object)
	{
		parent::hydrate($data, $object);
		(isset($data['fsdata'])) ? $object->fsdata = $data['fsdata'] : null;
		(isset($data['docfile'])) ? $object->docfile = $data['docfile'] : null;
		(isset($data['checkerResult'])) ? $object->checkerResult = $data['checkerResult'] : null;
		(isset($data['spacename'])) ? $object->spacename = $data['spacename'] : null;
		return $object;
	}

	/**
	 * Extract values from an object
	 *
	 * @param object $object
	 * @return array
	 */
	public function extract($object)
	{
		return parent::extract($object);
	}
}
