<?php
namespace Ged\Smartstore;

use Ged\Smartstore\Checker\Respons;

/**
 * Helper to check existance of a entity.
 * Load Document if existing
 * Load Docfile if exisiting
 * Populate the actions valids for the situation
 * The Checker\Respons object is attached to Document::checkerResult and Docfile::checkerResult property
 */
abstract class Checker
{

	/* create a new document */
	const TODO_CREATE = 'create';

	/* edit and update metadatas only */
	const TODO_EDIT = 'edit';

	/* update metadatas and vaulted files of not checkouted docfile or document */
	const TODO_UPDATE = 'update';

	/* do nothing */
	const TODO_IGNORE = 'ignore';

	/* checkin */
	const TODO_CHECKIN = 'checkin';

	/* update metadatas and vaulted files */
	const TODO_CHECKINANDKEEP = 'checkinandkeep';

	/* create new version */
	const TODO_VERSION = 'version';

	/* add the file to the existing document */
	const TODO_ADDFILE = 'addfile';

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
	}

	/**
	 *
	 */
	public static function getActionDefinitions()
	{
		return array(
			self::TODO_CREATE => 'New document',
			self::TODO_UPDATE => 'Update the document and files',
			self::TODO_ADDFILE => 'Associate file to document',
			self::TODO_IGNORE => 'Ignore',
			self::TODO_VERSION => 'New version',
			self::TODO_CHECKIN => 'Checkin',
			self::TODO_CHECKINANDKEEP => 'Checkin And Keep',
			self::TODO_EDIT => 'Edit Properties only'
		);
	}

	/**
	 */
	abstract public function check($model, $name);

	/**
	 * 
	 * @param \Rbplm\Any $model
	 * @return Respons
	 */
	public function getResult($model)
	{
		if ( !isset($model->checkerResult) ) {
			$model->checkerResult = new Respons();
			$model->checkerResult->setTodo(self::TODO_IGNORE);
		}
		return $model->checkerResult;
	}

	/**
	 * @param \Rbplm\Any $model
	 * @param string $type
	 *        	errors|feedbacks|alerts|todo
	 * @param string $msg
	 * @return Checker
	 */
	public function setResult($model, $type, $msg)
	{
		$result = $this->getResult($model);
		switch ($type) {
			case 'existingModel':
				$result->setData('existingModel', $msg);
				break;
			case 'error':
			case 'errors':
				$result->addError($msg);
				break;
			case 'feedback':
			case 'feedbacks':
				$result->addFeedback($msg);
				break;
			case 'alert':
			case 'alerts':
				$result->addAlert($msg);
				break;
			case 'todo':
				$result->setTodo($msg);
				break;
			case 'md5':
				/* $msg is array[isequalBoolean, md5OfWsFile, md5OfVaultFile] */
				$result->setData('md5', $msg);
				break;
		}
		return $this;
	}
} /* End of class */
