<?php
// %LICENCE_HEADER%
namespace Ged\Smartstore;

/**
 */
class DocfileCollection implements \Countable
{

	static $classId = 'docfilecollection';

	/**
	 *
	 * @var integer
	 */
	public $containerId;

	/**
	 *
	 * @var string
	 */
	public $comment;

	/**
	 *
	 * @var array
	 */
	protected $items = array();

	/**
	 *
	 * @param array $properties
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['docfiles'])) ? $this->items = $properties['docfiles'] : null;
		(isset($properties['items'])) ? $this->items = $properties['items'] : null;
		(isset($properties['containerId'])) ? $this->containerId = $properties['containerId'] : null;
		(isset($properties['comment'])) ? $this->comment = $properties['comment'] : null;
		return $this;
	}

	/**
	 * Alias for hydrate
	 *
	 * @param array $properties
	 */
	public function populate(array $properties)
	{
		return $this->hydrate($properties);
	}

	/**
	 * Alias for __serialize; implement arrayObject interface
	 * @return 	array
	 */
	public function getArrayCopy()
	{
		return array(
			'docfiles' => $this->items,
			'containerId' => $this->containerId
		);
	}

	/**
	 * method of Serializable interface
	 * @return array
	 */
	public function __sleep()
	{
		return array(
			'items'
		);
	}

	/**
	 *
	 * @param \Rbplm\Ged\Docfile\Version $df
	 */
	public function add($df)
	{
		$this->items[$df->getName()] = $df;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function count()
	{
		return count($this->items);
	}

	/**
	 *
	 * @return array
	 */
	public function getIterator()
	{
		return $this->items;
	}
}
