<?php
namespace Ged\Controller\Category;

use Application\Controller\AbstractController;
use Rbplm\Ged\Category;
use Rbs\Space\Factory as DaoFactory;

/**
 *
 *
 */
class IndexController extends AbstractController
{

	public $pageId = 'category_manage';

	public $defaultSuccessForward = 'ged/category/index';

	public $defaultFailedForward = 'ged/category/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$this->defaultSuccessForward = 'ged/category/index';
		$this->defaultFailedForward = 'ged/category/index';
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('category');
		$this->layout()->tabs = $tabs;

		$this->resourceCn = \Acl\Model\Resource\Ged::$appCn;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Ged::$appCn;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$request = $this->getRequest();
		$view = $this->view;
		$view->setTemplate('ged/category/index/index');
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Init some helpers*/
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Category::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Ged\Form\Category\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'categoryfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Category::$classId);
		$select = [];
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->orderby = 'name';
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Categories Manager';
		return $view;
	}

	/**
	 *
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$category = Category::init('Category');
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Category::$classId);

		/**/
		$form = new \Ged\Form\Category\EditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($category);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($category);
				return $this->successForward();
			}
		}

		/* */
		$view->pageTitle = 'Create Category';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$categoryIds = $request->getQuery('checked', []);
			$categoryId = $categoryIds[0];
			$validate = false;
		}
		if ( $request->isPost() ) {
			$categoryId = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$category = new Category();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Category::$classId);
		$dao->loadFromId($category, $categoryId);

		$form = new \Ged\Form\Category\EditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($category);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($category);
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Category ' . $category->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$categoryIds = $request->getQuery('checked', []);
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Category::$classId);

		foreach( $categoryIds as $categoryId ) {
			try {
				$dao->deleteFromId($categoryId);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $categoryId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}
} /* End of class */
