<?php
namespace Ged\Controller\Archiver;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Org\Workitem;
use Rbplm\Ged\AccessCode;
use Ged\Archiver\Archiver;
use Exception;
use Rbs\Dao\Sier\Filter;
use Rbplm\Dao\Filter\Op;
use Rbplm\People;
use Ranchbe;

/**
 */
class ArchiverController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'document_archiver';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'ged/document/manager/index';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'ged/document/manager/index';

	/**
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		/* */
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;

		/* */
		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 */
	public function indexAction()
	{}

	/**
	 * Archive a selection of documents
	 */
	function documentAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename');
			$documentIds = $request->getQuery('checked', array());
			$documentId = $request->getQuery('id', null);
			($documentId) ? $documentIds[] = $documentId : null;
			$validate = false;
			if ( is_array($spacename) ) {
				$spacename = $spacename[$documentId];
			}
		}
		if ( $request->isPost() ) {
			$spacename = $request->getPost('spacename');
			$documentIds = $request->getPost('checked', array());
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* load helpers */
		$factory = DaoFactory::get($spacename);
		/** @var \Rbs\Ged\Document\VersionDao $dao */
		$dao = $factory->getDao(Document\Version::$classId);

		/* display confirmation form */
		if ( !$validate ) {
			$form = new \Ged\Form\Document\ConfirmForm($factory, $view);
			$form->setUrlFromCurrentRoute($this);

			foreach( $documentIds as $documentId ) {
				$document = new Document\Version();
				$dao->loadFromId($document, $documentId);
				$form->addDocument($document);
			}

			$view->pageTitle = "Archive Confirmation";
			$view->warning = "Are you sure that you want archive this documents?";
			$view->help = "";
			$view->spacename = $spacename;
			$view->setTemplate($form->template);
			$view->form = $form;
			return $view;
		}
		else {
			$archiver = Ranchbe::get()->getServiceManager()->getArchiverService($factory);
			$loader = $factory->getLoader();
			$dfDao = $factory->getDao(Docfile\Version::$classId);

			/* */
			foreach( $documentIds as $documentId ) {
				try {
					/* LOAD DOCUMENT */
					$document = new Document\Version();
					$dao->loadFromId($document, $documentId);

					if ( $document->checkAccess() < Archiver::$accessCode ) {
						/* IF NOT ARCHIVE, LOAD PARENT AND DOCFILES */
						$container = $loader->loadFromId($document->getParent(true), Workitem::$classId);
						$document->setParent($container);
						$dfDao->loadDocfiles($document, $dfDao->toSys('accessCode') . '<' . Archiver::$accessCode);

						/* RUN ARCHIVER */
						$archiver->archive($document, true);
					}
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
					continue;
				}
			}
		}

		$this->successForward();
	}

	/**
	 * Archive all documens of a container
	 */
	function containerAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$containerIds = $request->getQuery('checked', array());
			$containerId = $request->getQuery('id', null);
			($containerId) ? $containerIds[] = $containerId : null;
			$spacename = $request->getQuery('spacename', null);
			$spacenames = $request->getQuery('spacenames', array());
			$validate = false;
		}
		if ( $request->isPost() ) {
			$containerIds = $request->getPost('checked', array());
			$spacename = $request->getPost('spacename', null);
			$spacenames = $request->getPost('spacenames', array());
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* display confirmation form */
		if ( !$validate ) {
			$factory = DaoFactory::get($spacename);
			$form = new \Ged\Form\Archiver\ConfirmForm($factory, $view);
			$form->template = 'ged/archiver/confirmform';
			$form->setUrlFromCurrentRoute($this);

			foreach( $containerIds as $containerId ) {
				$sn = $spacenames[$containerId];

				/* load helpers */
				$factory = DaoFactory::get($sn);
				$dao = $factory->getDao(Workitem::$classId);

				$container = $factory->getModel(Workitem::$classId);
				$dao->loadFromId($container, $containerId);
				$form->toConfirm($container, $container->getName());
			}

			$view->pageTitle = "Confirmation";
			$view->warning = "Are you sure that you want archive this containers?";
			$view->help = "";
			$view->spacename = $spacename;
			$view->setTemplate($form->template);
			$view->form = $form;
			return $view;
		}
		else {
			foreach( $containerIds as $containerId ) {
				try {
					$spacename = $spacenames[$containerId];
					$factory = DaoFactory::get($spacename);
					$archiver = Ranchbe::get()->getServiceManager()->getArchiverService($factory);
					$dao = $factory->getDao(Document\Version::$classId);
					$dfDao = $factory->getDao(Docfile\Version::$classId);

					/* LOAD CONTAINER */
					$container = $factory->getModel(Workitem::$classId);
					$factory->getDao($container->cid)->loadFromId($container, $containerId);

					if ( $container->checkAccess() < Archiver::$accessCode ) {
						/* check if all documents are checkin */
						$list = $factory->getList(Document\Version::$classId);
						$filter = new Filter('', false);
						$filter->andfind($containerId, $dao->toSys('parentId'), Op::EQUAL);
						$filter->andfind(AccessCode::CHECKOUT, $dao->toSys('accessCode'), Op::EQUAL);
						$count = $list->countAll($filter);
						if ( $count > 0 ) {
							throw new Exception('Some document are currently checkouted by some users. Let users finish her works before.');
						}

						/* get list of document to archive */
						$list = $factory->getList(Document\Version::$classId);
						$filter = new Filter('', false);
						$filter->andfind($containerId, $dao->toSys('parentId'), Op::EQUAL);
						$filter->andfind(Archiver::$accessCode, $dao->toSys('accessCode'), Op::INF);
						// $filter->select($dao->getTranslator()->getSelectAsApp());
						$list->load($filter);

						/* archive each document */
						foreach( $list as $entry ) {
							$document = new Document\Version();
							$dao->hydrate($document, $entry);
							$document->dao = $dao;
							$document->setParent($container);
							$dfDao->loadDocfiles($document, $dfDao->toSys('accessCode') . '<' . Archiver::$accessCode);
							/* RUN ARCHIVER */
							$archiver->archive($document, true);
						}

						/* lock project */
						$container->close(Archiver::$accessCode, People\CurrentUser::get());
						$container->lifeStage = Archiver::STATUS_ARCHIVED;

						$factory->getDao($container->cid)->save($container);
					}
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
					continue;
				}
			}
		}
		$this->successForward();
	}
} /* End of class */
