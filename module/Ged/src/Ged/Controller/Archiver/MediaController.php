<?php
namespace Ged\Controller\Archiver;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Ged\Archiver\Media;

/**
 *
 *
 */
class MediaController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'archiver_media';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'ged/document/manager/index';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'ged/document/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;

		$this->basecamp()->setForward($this);

		/**/
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$request = $this->getRequest();
		$view = $this->view;
		$view->setTemplate('ged/archiver/media/index/index');

		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* init helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Media::$classId);
		$list = $factory->getList(Media::$classId);

		/* init filters */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Ged\Form\Archiver\Media\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();
		$filter->select($dao->getTranslator()
			->getSelectAsApp());

		/* init paginator */
		$list->countAll = $list->countAll($filter);
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 100;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		/* load datas from db in list */
		$list->load($filter, $filterForm->bind);

		/* Display the template */
		$view->list = $list;
		$view->filter = $filterForm;
		$view->headers = $dao->metaModel;
		$view->pageTitle = 'Media Manager';
		return $view;
	}

	/**
	 *
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();

		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}
		else {
			$validate = false;
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* init helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Media::$classId);

		/* init model */
		$model = Media::init('Media' . uniqid());

		/* load form */
		$form = new \Ged\Form\Archiver\Media\EditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($model);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($model);
				return $this->successForward();
			}
		}
		$view->pageTitle = 'Create Media';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', []);
			$id = $ids[0];
			$validate = false;
		}
		if ( $request->isPost() ) {
			$id = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* init helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Media::$classId);

		/* init model */
		$model = new Media();
		$dao->loadFromId($model, $id);

		/* load form */
		$form = new \Ged\Form\Archiver\Media\EditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($model);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($model);
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Media ' . $model->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', []);
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* init helpers*/
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Media::$classId);

		foreach( $ids as $id ) {
			try {
				$dao->deleteFromId($id);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $id));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}
} /* End of class */
