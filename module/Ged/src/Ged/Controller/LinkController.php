<?php
namespace Ged\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Application\Controller\ControllerException as Exception;

/**
 *
 * Abstract controller from manager linked obects to org objects (Projects, Containers)
 * The concret class must implements methods :
 *
 * 	getLinkModel => return the Link object,
 *  getParentModel => return the parent object,
 *  getChildModel => return the child object,
 *  getAddLinkform => return the Zend form used to select children to link.
 *
 *
 *
 */
abstract class LinkController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $linkClassId;

	/**
	 *
	 * @var string
	 */
	public $childClassId;

	/**
	 *
	 * @var string
	 */
	public $parentClassId;

	/**
	 *
	 * @var string
	 */
	public $addTemplate;

	/**
	 *
	 * @var string
	 */
	public $displayTemplate;

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* */
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
		$this->spacename = $this->params()->fromRoute('spacename', 'default');
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * @return \Rbs\Org\Project\Link\Doctype
	 */
	protected abstract function getLinkModel();

	/**
	 * @param \Rbs\Space\Factory $factory
	 */
	protected abstract function getParentModel($factory);

	/**
	 * @param \Rbs\Space\Factory $factory
	 */
	protected abstract function getChildModel($factory);

	/**
	 * Factory for the add link form
	 */
	protected abstract function getAddLinkform($view, $factory);

	/**
	 * @param \Rbplm\Org\Project $parent
	 * @param \Rbs\Space\Factory $factory
	 * @return \PDOStatement
	 */
	protected abstract function getLinks($parent, $factory);

	/**
	 * @param string $spacename
	 */
	protected function setSpacename($spacename)
	{}

	/**
	 */
	public function indexAction($list = null)
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename', $this->spacename);
			$parentId = $request->getQuery('id', $request->getQuery('parentid', null));
			$parentIds = $request->getQuery('checked', array());
			($parentIds) ? $parentId = $parentIds[0] : null;
			$parentUid = $request->getQuery('uid', null);
			if ( is_array($spacename) ) {
				$spacename = $spacename[$parentId];
			}
		}
		else {
			throw new \BadMethodCallException('Must be called by http-GET methods');
		}

		/**/
		$this->setSpacename($spacename);

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* return to this page after actions */
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		if ( !$list ) {
			/* Some helpers */
			$factory = DaoFactory::get($spacename);
			$dao = $factory->getDao($this->childClassId);

			/* Get uid */
			if ( !$parentUid ) {
				$parentCid = $this->parentClassId;
				$parentUid = $factory->getDao($parentCid)->getUidFromId($parentId);
			}

			/* Build select*/
			$select = [];
			foreach( $dao->metaModel as $asSys => $asApp ) {
				$select[] = "`child`.`$asSys` AS `$asApp`";
			}
			$lnkDao = $factory->getDao($this->linkClassId);

			/*
			 * $filter = new \Rbs\Dao\Sier\Filter('', false);
			 * $filter->andfind($parentId, 'link.'.$dao->toSys('parentId'));
			 * $filter->andfind($parentCid, 'link.'.$dao->toSys('parentCid'));
			 * $filter->andfind($childCid, 'link.'.$dao->toSys('childCid'));
			 * $stmt = $lnkDao->getChildrenStmt($select, $filter->__toString());
			 */

			$list = $lnkDao->getChildrenFromUid($parentUid, $select);
		}

		/* Display the template */
		$view->list = $list;
		$template = $this->displayTemplate;
		$view->setTemplate($template);
		$view->spacename = $spacename;
		$view->parentid = $parentId;
		$view->parentuid = $parentUid;
		$view->number = $parentUid;
		return $view;
	}

	/**
	 *
	 * @param \Rbplm\Dao\FilterAbstract $filter
	 * @param array $bind
	 * @return \Application\View\ViewModel
	 */
	public function addlinkAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$parentId = $request->getQuery('parentid', null);
			$spacename = $request->getQuery('spacename', $this->spacename);
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$parentId = $request->getPost('parentId', null);
			$checked = $request->getPost('children', array());
			$spacename = $request->getPost('spacename', $this->spacename);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		/* */
		$factory = DaoFactory::get($spacename);
		$this->factory = $factory;
		$childDao = $factory->getDao($this->childClassId);

		/* @var \Rbs\Org\Project\Link\ContainerDao $lnkDao */
		$lnkDao = $factory->getDao($this->linkClassId);

		/* */
		$form = $this->getAddLinkform($view, $factory);

		/** @var \Rbplm\Link $link */
		$link = $this->getLinkModel();
		$parent = $this->getParentModel($factory);
		$factory->getDao($parent::$classId)->loadFromId($parent, $parentId);
		$link->setParent($parent);
		$link->childCid = $this->childClassId;

		/* */
		if ( $validate ) {
			$form->bind($link);
			$form->setData($request->getPost());

			$filter = $lnkDao->getTranslator()->toSys('parentUid') . '=:parentUid';
			$filter .= ' AND ' . $lnkDao->getTranslator()->toSys('childCid') . '=:childCid';
			$bind = [
				':parentUid' => $parent->getUid(),
				':childCid' => $this->childClassId
			];

			/* cleanup existing links... */
			$lnkDao->delete($filter, $bind);

			/* ...and rewrite it */
			foreach( $checked as $childId ) {
				try {
					$uid = $childDao->getUidFromId($childId);
					$link->childId = $childId;
					$link->childUid = $uid;
					$lnkDao->insert($link);
				}
				catch( \PDOException $e ) {
					if ( strpos($e->getMessage(), 'Integrity constraint violation: 1062') == false ) {
						$this->errorStack()->error($e->getMessage());
					}
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
			}
			$this->successForward(array(
				'id' => $parentId,
				'spacename' => $spacename
			));
		}
		else {
			$form->setData($request->getQuery());
			$link->children = $this->getLinks($parent, $factory)->fetchAll(\PDO::FETCH_COLUMN);
			$form->bind($link);

			$view->setTemplate($this->addTemplate);
			$view->pageTitle = sprintf('Add link to %s', $parent->getName());
			$view->parentUid = $parent->getUid();
			$view->form = $form;
			return $view;
		}
	}

	/**
	 *
	 * @throws Exception
	 */
	public function unlinkAction()
	{
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$spacename = $request->getPost('spacename', $this->spacename);
			$parentId = $request->getPost('parentid', null);
			$checked = $request->getPost('checked', array());
		}

		/* */
		if ( !$parentId ) {
			throw new Exception('parentId is not setted');
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$factory = DaoFactory::get($spacename);
		/** @var \Rbs\Dao\Sier\Link $lnkDao */
		$lnkDao = $factory->getDao($this->linkClassId);
		$parentCid = $this->parentClassId;
		$childrenCid = $this->childClassId;

		foreach( $checked as $childId ) {
			$lnkDao->deleteFromParentAndChild($parentId, $childId, $parentCid, $childrenCid);
		}

		$this->successForward();
	}
} /* End of class */
