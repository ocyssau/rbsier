<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\Document;
use Rbplm\People;

/**
 */
class ViewerController extends AbstractController
{

	/** @var string */
	public $pageId = 'document_viewer';

	/** @var string */
	public $defaultSuccessForward = 'ged/document/manager/index';

	/** @var string */
	public $defaultFailedForward = 'ged/document/manager/index';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Proxy to service/document/viewer/download
	 */
	public function permanentAction()
	{
		$documentId = $this->params()->fromRoute('documentId');
		$spacename = $this->params()->fromRoute('spacename');
		return $this->redirectTo('service/document/viewer/download', array(
			'id' => $documentId,
			'spacename' => $spacename
		));
	}

	/**
	 * Proxy to service/document/viewer/getlast
	 */
	public function getlastAction()
	{
		$number = $this->params()->fromRoute('number');
		$spacename = $this->params()->fromRoute('spacename');
		return $this->redirectTo('service/document/viewer/getlast', array(
			'number' => $number,
			'spacename' => $spacename
		));
	}

	/**
	 * Display instance of all process
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function threedAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/viewer/babylonjs');
		$documentId = $this->params()->fromQuery('documentId', null);
		$spacename = $this->params()->fromQuery('spacename', null);

		if ( !$documentId || !$spacename ) return $view;
		$factory = DaoFactory::get($spacename);

		/* Check permissions */
		$this->getAcl()->checkRightFromIdAndCid('read', $documentId, \Rbplm\Ged\Document\Version::$classId);

		/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
		$dfDao = $factory->getDao(Docfile\Version::$classId);

		/** @var \Rbs\Dao\Sier\Filter $filter */
		$filter = $factory->getFilter(Docfile\Version::$classId);

		try {
			$threedDf = new Docfile\Version();
			$filter->andfind(Docfile\Role::VISU, $dfDao->toSys('mainrole'), Op::EQUAL);
			$filter->andfind($documentId, $dfDao->toSys('parentId'), Op::EQUAL);
			$dfDao->load($threedDf, $filter);
			$view->threedDf = $threedDf;
		}
		catch( \Exception $e ) {
			$view->threedDf = null;
		}
		return $view;
	}

	/**
	 *
	 * @return \Application\View\ViewModel|string
	 */
	function assocvisuAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename');
			$documentIds = $request->getQuery('checked', array());
			$documentId = $request->getQuery('id', null);
			(is_null($documentId)) ? $documentId = current($documentIds) : null;
			$fileName = null;
			$role = null;
			$checkName = \Ranchbe::get()->getConfig('document.assocfile.checkname');
		}
		if ( $request->isPost() ) {
			$documentId = $request->getPost('id', null);
			$spacename = $request->getPost('spacename', null);
			$role = trim($request->getPost('mainrole', null));
			$fileName = trim($request->getPost('file', null));
			$cancel = $request->getPost('cancel', null);
			if ( $cancel || !$documentId ) {
				$this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromIdAndCid('read', $documentId, \Rbplm\Ged\Document\Version::$classId);

		$view->spacename = $spacename;
		$filter = new \Rbplm\Sys\Filesystem\Filter();

		if ( !$fileName ) {
			/* load document */
			$factory = DaoFactory::get($spacename);
			$dao = $factory->getDao(Document\Version::$classId);
			$document = new Document\Version();
			$dao->loadFromId($document, $documentId);

			/* set some vars */
			$number = $document->getNumber();
			$uid = $document->getUid();

			/* ASSOC_FILE_CHECK_NAME is set in ranchbe_setup.php */
			/* Limit list of files to files with the document name in the name */
			$filter = new \Rbplm\Sys\Filesystem\Filter('', false);
			$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
			if ( $checkName == true ) {
				$filter->andFind($number, 'name', Op::CONTAINS);
			}
			$filter->andFind('(.*[.jpg|.jpeg|.png|.gif])$', 'name', Op::REGEX);
			$form = new \Ged\Form\Document\Datamanager\AssociateFileForm($wildspace, $filter);
			$form->setAttribute('action', $request->getUriString());
			$binded = new \stdClass();
			$binded->documentid = $documentId;
			$binded->spacename = $spacename;
			$binded->mainrole = \Rbplm\Ged\Docfile\Role::PICTURE;
			$form->bind($binded);

			/* Display the template */
			$view->pageTitle = "Select the Visu to associate to document $uid";
			$view->documentUid = $uid;
			$view->form = $form;
			$view->setTemplate($form->template);
			return $view;
		}
		else {
			/* structure of input data defined in DatamanagerService */
			$input = array(
				'documents' => array(
					'document1' => array(
						'id' => $documentId,
						'spacename' => $spacename,
						'files' => array(
							'file1' => array(
								'name' => $fileName,
								'role' => $role,
								'data' => array(
									'data' => 'FROM_WS',
									'name' => $fileName
								)
							)
						)
					)
				)
			);
			try {
				$service = new \Service\Controller\Document\DatamanagerService();
				$service->init()->assocfileService($input);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				$this->errorForward();
			}

			$this->successForward();
		}
	}
}

