<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Dao\Filter\Op;
use Rbplm\People;
#use Zend\Stdlib\Parameters;
use Zend\Http\Request;

/**
 * Ajax enabled
 *
 * @author olivier
 *
 */
class DatamanagerController extends AbstractController
{

	/* */
	public $pageId = 'document_datamanager';

	/* */
	public $defaultSuccessForward = 'home';

	/* */
	public $defaultFailedForward = 'home';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		$context = \Ranchbe::get()->getServiceManager()->getContext();
		$this->ifSuccessForward = $this->url()->fromRoute('ged-document-manager', [
			'id' => $context->getData('containerId'),
			'spacename' => $context->getData('spacename')
		]);
		$this->ifFailedForward = $this->ifSuccessForward;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Single selection
	 *
	 * @return \Application\View\ViewModel
	 */
	function assocfileAction()
	{
		$view = $this->view;
		$request = $this->getRequest();

		$spacename = $this->params()->fromRoute('spacename');
		$documentId = $this->params()->fromRoute('id');

		if ( $request->isGet() ) {
			$filenames = null;
			$role = null;
		}
		elseif ( $request->isPost() ) {
			$role = $request->getPost('mainrole', null);
			$filenames = $request->getPost('file', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}
		$checkName = \Ranchbe::get()->getConfig('document.assocfile.checkname');

		if ( !$filenames ) {
			/* load document */
			$factory = DaoFactory::get($spacename);
			$dao = $factory->getDao(Document\Version::$classId);
			$dfdao = $factory->getDao(Docfile\Version::$classId);
			$document = new Document\Version();
			$dao->loadFromId($document, $documentId);
			$dfdao->loadDocfiles($document);

			/* Check permissions */
			$this->getAcl()->checkRightFromUid('create', $document->parentUid);

			/* set some vars */
			$name = $document->getName();

			/* ASSOC_FILE_CHECK_NAME is set in ranchbe_setup.php */
			/* Limit list of files to files with the document name in the name */
			$filter = new \Rbplm\Sys\Filesystem\Filter('', false);
			$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
			if ( $checkName == true ) {
				$filter->andFind($name, 'name', Op::CONTAINS);
			}

			$form = new \Ged\Form\Document\Datamanager\AssociateFileForm($wildspace, $filter);
			$form->setAttribute('action', $request->getUriString());
			$binded = new \stdClass();
			$binded->id = $documentId;
			$binded->spacename = $spacename;
			if ( count($document->getDocfiles()) > 0 ) {
				$binded->mainrole = \Rbplm\Ged\Docfile\Role::ANNEX;
			}
			else {
				$binded->mainrole = \Rbplm\Ged\Docfile\Role::MAIN;
			}
			$form->bind($binded);
		}
		else {
			/* structure of input data defined in DatamanagerService */
			$documents = array(
				'document1' => array(
					'id' => $documentId,
					'spacename' => $spacename,
					'files' => []
				)
			);

			$i = 1;
			foreach( $filenames as $filename ) {
				$documents['document1']['files']['file' . $i] = [
					'name' => $filename,
					'role' => $role,
					'data' => array(
						'data' => 'FROM_WS',
						'name' => $filename
					)
				];
				$i++;
			}

			try {
				$request = new Request();
				$request->setMethod(Request::METHOD_POST)
					->getPost()
					->set('documents', $documents);
				$service = new \Service\Controller\Document\DatamanagerService();
				$this->bindServiceRespons($service->init()
					->assocfileService($request));
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				$this->errorForward();
			}

			$this->successForward();
		}

		/* Display the template */
		$view->pageTitle = sprintf(tra('Select the file to associate to %s'), $document->getName());
		$view->documentUid = $document->getUid();
		$view->setTemplate($form->template);
		$view->form = $form;

		return $view;
	}

	/**
	 * Single selection
	 *
	 * @return \Application\View\ViewModel
	 */
	function copydocumentAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacenames = $request->getQuery('spacenames');
			$documentIds = $request->getQuery('checked', array());
			$documentId = $request->getQuery('id', null);
			(is_null($documentId)) ? $documentId = current($documentIds) : null;
			if ( !isset($spacenames[$documentId]) ) {
				throw new \Exception('spacename is not set for document ' . $documentId);
			}
			$spacename = $spacenames[$documentId];
			$toName = null;
			$toContainerId = null;
			$toVersion = null;
		}
		if ( $request->isPost() ) {
			$documentId = $request->getPost('id', null);
			$toContainerId = trim($request->getPost('tocontainerid', null));
			$toName = trim($request->getPost('newname', null));
			$toVersion = trim($request->getPost('version', 1));
			$spacename = $request->getPost('spacename');
			$cancel = $request->getPost('cancel', null);

			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromIdAndCid('read', $documentId, Document\Version::$classId);

		if ( !($toName && $toContainerId) ) {
			$factory = DaoFactory::get($spacename);
			$dao = $factory->getDao(Document\Version::$classId);

			/* load document */
			$document = new Document\Version();
			$dao->loadFromId($document, $documentId);

			/* Check permissions */
			$containerId = $document->getParent(true);
			$this->getAcl()->checkRightFromIdAndCid('create', $toContainerId, \Rbplm\Org\Workitem::$classId);

			/* load form */
			$form = new \Ged\Form\Document\Datamanager\CopyForm($factory);
			$form->setAttribute('action', $request->getUriString());
			$bindObj = new \stdClass();
			$bindObj->spacename = $spacename;
			$bindObj->id = $documentId;
			$bindObj->containerid = $containerId;
			$bindObj->newname = 'copyof_' . $document->getNumber();
			$bindObj->version = 1;
			$form->bind($bindObj);
		}
		else {
			$documents = [];
			$documents = array(
				'document1' => array(
					'id' => $documentId,
					'spacename' => $spacename,
					'tocontainer' => $toContainerId,
					'toname' => $toName,
					'toversion' => $toVersion
				)
			);
			try {
				$request = new Request();
				$request->setMethod(Request::METHOD_POST)
					->getPost()
					->set('documents', $documents);
				$service = new \Service\Controller\Document\DatamanagerService();
				$this->bindServiceRespons($service->init()
					->copydocumentService($request));
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				$this->errorForward();
			}

			$this->successForward();
		}

		/* Display the template */
		$view->pageTitle = sprintf(tra('Copy Document %s'), $document->getName());
		$view->documentUid = $document->getUid();
		$view->setTemplate($form->template);
		$view->form = $form;

		return $view;
	}

	/**
	 * Move a set of document to another vault.
	 * Multi-selection enable only on one spacename
	 * 
	 * With form
	 *
	 * @return \Application\View\ViewModel
	 */
	function movedocumentAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$documentIds = $request->getQuery('checked', []);
			$spacenames = $request->getQuery('spacenames');
			/* first selected spacename is adopt as user intention */
			$spacename = current($spacenames);
			$validate = false;
			$toContainerId = false;
		}
		if ( $request->isPost() ) {
			$documentIds = $request->getPost('checked', []);
			$spacename = $request->getPost('spacename');
			$toContainerId = trim($request->getPost('tocontainerid', null));
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);

			if ( $cancel ) {
				$this->successForward();
			}
		}

		if ( !($validate && $toContainerId) ) {
			/* init some helpers */
			$factory = DaoFactory::get($spacename);

			/* load form */
			$form = new \Ged\Form\Document\Datamanager\MoveForm($factory);
			$form->setAttribute('action', $request->getUriString());
			$bindObj = new \stdClass();
			$bindObj->spacename = $spacename;
			$form->bind($bindObj);

			/* @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);

			foreach( $documentIds as $documentId ) {
				if ( $spacenames[$documentId] != $spacename ) {
					$sn = $spacenames[$documentId];
					$this->errorStack()->log(sprintf('document id %s has a spacename %s not conform with spacename %s of current selection. Document is ignored.', $documentId, $sn, $spacename));
					unset($documentIds[$documentId]);
					continue;
				}
				$document = new Document\Version();
				$dao->loadFromId($document, $documentId);

				/* Check permissions */
				$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

				$form->addDocument($document);
			}
		}
		/* Validate the form */
		else {
			$i = 1;
			$documents = array();
			foreach( $documentIds as $documentId ) {
				$documents['document' . $i] = array(
					'id' => $documentId,
					'spacename' => $spacename,
					'tocontainer' => $toContainerId
				);
				$i++;
			}
			try {
				$request = new Request();
				$request->setMethod(Request::METHOD_POST)
					->getPost()
					->set('documents', $documents);
				$service = new \Service\Controller\Document\DatamanagerService();
				$this->bindServiceRespons($service->movedocumentService($request));
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				$this->errorForward();
			}

			$this->successForward();
		}

		/* Display the template */
		$view->pageTitle = tra('Move Documents');
		$view->setTemplate($form->template);
		$view->form = $form;

		return $view;
	}
} /* End of class */
