<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractPublicController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Ged\Document\Share\PublicUrl;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Document;

/**
 */
class SharedController extends AbstractPublicController
{

	/* */
	public $pageId = 'document_shared';

	/* */
	public $defaultSuccessForward = 'shared';

	/* */
	public $defaultFailedForward = 'shared';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$view = $this->view;
		/* @var \Zend\Http\PhpEnvironment\Request $request */
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$uid = $this->params()->fromRoute('uid');
		}

		/* Init some helpers */
		$factory = DaoFactory::get();

		/* Load link */
		try {
			$publicShare = new PublicUrl();
			$factory->getDao($publicShare->cid)->loadFromUid($publicShare, $uid);

			/* Get document */
			$documentId = $publicShare->documentId;
			$spacename = $publicShare->spacename;
			$factory = DaoFactory::get($spacename);
			$document = new Document\Version();
			$factory->getDao($document->cid)->loadFromId($document, $documentId);

			/* @var \Rbs\Ged\Docfile\VersionDao $dfDao */
			$dfDao = $factory->getDao(Docfile\Version::$classId);
			$dfDao->loadDocfiles($document, $dfDao->toSys('mainrole') . '=' . Docfile\Role::MAIN);
			$docfiles = $document->getDocfiles();
			$mainDf = array_shift($docfiles);
			$fsdata = $mainDf->getData()->getFsdata();
			$fileName = $document->getName() . '.v' . $document->version . '.' . $document->iteration . $fsdata->getExtension();

			\Service\Controller\Document\ViewerService::download($fileName, $fsdata->getMimetype(), $fsdata->getSize(), $fsdata->getFullpath());
			die();
		}
		catch( \Rbplm\Dao\NotExistingException $e ) {
			throw $e;
		}

		return $view;
	}
}
