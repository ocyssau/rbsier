<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Ged\Document\BasketDao;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\People\CurrentUser;
use Rbplm\Dao\NotExistingException;
use Rbs\Dao\Sier\Filter;
use Zend\Http\Request;

/**
 */
class BasketController extends AbstractController
{

	/**
	 * @var string
	 */
	public $pageId = 'document_basket';

	/**
	 * 
	 * @var string
	 */
	public $defaultSuccessForward = 'ged/document/basket/index';

	/**
	 * 
	 * @var string
	 */
	public $defaultFailedForward = 'ged/document';

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('mybasket');
		$this->layout()->tabs = $tabs;

		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename', null);
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$factory = DaoFactory::get($spacename);

		$defaultFwd = $this->defaultSuccessForward . '?spacename=' . $spacename;
		$this->basecamp()->save($defaultFwd, $this->ifFailedForward, $view);

		try {
			$basket = new BasketDao(CurrentUser::get(), $factory);
			$filter = new Filter('', false);
			$paginator = new \Application\Form\PaginatorForm($this->pageId);
			$paginator->load(['default'=>[
				'orderby'=>'number',
				'order'=>'asc',
				'limit' => 50
			]]);
			$paginator->setMaxLimit($basket->countAll());
			$paginator->bindToFilter($filter)
				->bindToView($view)
				->save();

			$list = $basket->get($filter);
			$view->list = $list;
			$view->pageTitle = tra('My Documents Basket');
		}
		catch( NotExistingException $e ) {
			$this->errorStack()->error('Basket is empty');
		}

		$view->spacename = $spacename;
		$view->setTemplate('ged/document/basket/index');
		return $view;
	}

	/**
	 * 
	 */
	public function addAction()
	{
		$request = $this->getRequest();

		if ( $request->isGet() ) {
			$spacenames = $request->getQuery('spacenames', null);
			$documentId = $request->getQuery('documentid', null);
			$spacename = $request->getQuery('spacename', null);
			$documentIds = $request->getQuery('checked', []);
			($documentId) ? $documentIds[] = $documentId : null;
			($documentId && $spacename) ? $spacenames[$documentId] = $spacename : null;

			unset($documentId, $spacename);
		}

		try {
			$idx = 1;
			$documents = [];
			foreach( $documentIds as $id ) {
				/* set spacename */
				isset($spacenames[$documentId]) ? $spacename = $spacenames[$documentId] : $spacename = false;
				if ( !$spacename ) {
					throw (new \Exception('spacename is not set for document ' . $documentId));
				}

				$documents['document' + $idx] = array(
					'id' => $id,
					'spacename' => $spacename
				);
				$idx++;
			}

			$request = new Request();
			$request->setMethod(Request::METHOD_POST)
				->getPost()
				->set('documents', $documents);
			$service = new \Service\Controller\Document\BasketService();
			$this->bindServiceRespons($service->addService($request));
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
			$this->errorForward();
		}
		$this->successForward();
	}

	/**
	 * 
	 */
	public function removeAction()
	{
		$request = $this->getRequest();

		if ( $request->isGet() ) {
			$spacenames = $request->getQuery('spacenames', null);
			$documentId = $request->getQuery('documentid', null);
			$spacename = $request->getQuery('spacename', null);
			$documentIds = $request->getQuery('checked', []);
			($documentId) ? $documentIds[] = $documentId : null;
			($documentId && $spacename) ? $spacenames[$documentId] = $spacename : null;

			unset($documentId, $spacename);
		}

		try {
			$idx = 1;
			$documents = [];
			foreach( $documentIds as $id ) {
				/* set spacename */
				isset($spacenames[$documentId]) ? $spacename = $spacenames[$documentId] : $spacename = false;
				if ( !$spacename ) {
					throw (new \Exception('spacename is not set for document ' . $documentId));
				}

				$documents['document' + $idx] = array(
					'id' => $id,
					'spacename' => $spacename
				);
				$idx++;
			}

			$request = new Request();
			$request->setMethod(Request::METHOD_POST)
				->getPost()
				->set('documents', $documents);
			$service = new \Service\Controller\Document\BasketService();
			$this->bindServiceRespons($service->removeService($request));
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
			$this->errorForward();
		}
		$this->successForward();
	}

	/**
	 * 
	 */
	public function cleanAction()
	{
		try {
			$service = new \Service\Controller\Document\BasketService();
			$this->bindServiceRespons($service->cleanService());
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
			$this->errorForward();
		}
		$this->successForward();
	}

	/**
	 * Returnto document manager
	 */
	public function exitAction()
	{
		$context = \Ranchbe::get()->getServiceManager()->getContext();
		$spacename = $context->getData('spacename');
		$containerId = $context->getData('containerId');
		if ( $spacename && $containerId ) {
			$url = $this->url()->fromRoute('ged-document-manager', [
				'spacename' => $spacename,
				'id' => $containerId,
				'action' => 'index'
			]);
		}
		else {
			$url = $this->url()->fromRoute('home');
		}
		return $this->redirect()->toUrl($url);
	}
} /* End of class */
