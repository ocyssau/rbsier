<?php
namespace Ged\Controller\Document;

//use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Dao\Filter\Op;

/**
 * 
 */
class RelationController extends ManagerController
{

	/* @var string */
	public $pageId = 'document_relation';

	/* @var string */
	public $defaultSuccessForward = 'ged/document/manager';

	/* @var string */
	public $defaultFailedForward = 'ged/document/manager';

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		\Application\Controller\AbstractController::init();

		/* Set commons vars */
		$context = \Ranchbe::get()->getServiceManager()->getContext();
		$containerId = $context->getData('containerId');
		$spacename = $context->getData('spacename');

		/**/
		$this->ifSuccessForward = $this->defaultSuccessForward . '/' . $spacename . '/' . $containerId;
		$this->ifFailedForward = $this->defaultFailedForward . '/' . $spacename . '/' . $containerId;
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		\Application\Controller\AbstractController::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 */
	public function indexAction()
	{}

	/**
	 */
	public function exitAction()
	{
		return $this->successForward();
	}

	/**
	 */
	function getrelationAction()
	{
		$view = $this->view;

		$view->setTemplate('ged/document/manager/index');
		$spacename = $this->params()->fromRoute('spacename', null);
		$id = $this->params()->fromRoute('id', null);
		$direction = $this->params()->fromRoute('direction', 'parent');

		if ( !$spacename ) {
			throw new \Application\Controller\ControllerException('spacename is not set');
		}

		/**/
		$this->pageId = $spacename . '_' . $this->pageId;

		$this->layoutSelector()->clear($this);
		$url = $this->url()->fromRoute('ged-document-relation', array(
			'direction' => $direction,
			'spacename' => $spacename,
			'id' => $id
		));
		$this->basecamp()->save($url, $url, $view);

		/* */
		$factory = DaoFactory::get($spacename);

		/* load parent document */
		$document = new Document\Version();
		$document->dao = $factory->getDao(Document\Version::$classId);
		$document->dao->loadFromId($document, $id);
		$containerId = $document->getParent(true);

		/* Init link dao */
		/* @var \Rbs\Ged\Document\LinkDao $linkDao */
		$linkDao = $factory->getDao(Document\Link::$classId);

		/* */
		if ( $containerId ) {
			/* Load container */
			$container = $factory->getModel(\Rbplm\Org\Workitem::$classId);
			$container->dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
			$container = $container->dao->loadFromId($container, $containerId);

			/* Check permissions */
			$this->getAcl()->checkRightFromUid('read', $container->getUid());

			/* Load extended properties */
			$this->loadExtended($document->dao, $containerId);
			$view->extended = $document->dao->extended;
		}

		/* */
		$filterForm = $this->getFilter($factory, $containerId, $direction + $spacename + $id);
		/* @var   \Rbplm\Dao\FilterAbstract $filter */
		$filter = $filterForm->daoFilter;
		$filter->setAlias('doc');
		$filter->setBind($filterForm->bind);

		/* get extended metadatas */
		$childMetamodel = array_merge($document->dao->metaModel, $document->dao->extendedModel);
		$select = [];
		if ( $childMetamodel ) {
			foreach( $childMetamodel as $asSys => $asApp ) {
				$select[] = 'doc.' . $asSys . ' AS ' . $asApp;
			}
		}

		$filter->select($select);

		/* */
		$filterForm->paginator->setMaxLimit(1000)
			->bindToFilter($filter)
			->bindToView($view)
			->save();

		/* */
		if ( $direction == 'children' ) {
			$filter->andfind($id, 'lt.' . $linkDao->toSys('parentId'), Op::EQUAL);
			$stmt = $linkDao->getChildren($filter, $filter->getBind());
			$view->pageTitle = sprintf(tra('Relations Children of %s'), $document->getName());
		}
		else {
			$filter->andfind($id, 'lt.' . $linkDao->toSys('childId'), Op::EQUAL);
			$stmt = $linkDao->getParents($filter, $filter->getBind());
			$view->pageTitle = sprintf(tra('Relation Parent of %s'), $document->getName());
		}

		/* get fixed document */
		$this->fixedDocument($view);

		/* Display */
		$view->list = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$view->filter = $filterForm;
		$view->columns = $filterForm->columnSelector->getSelected();
		$view->displayThumbs = $filterForm->get('displayThumbs')->getValue();
		$view->documentid = $id;
		$view->spacename = $spacename;

		return $view;
	}
} /* End of class */
