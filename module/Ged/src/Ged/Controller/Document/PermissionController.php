<?php
namespace Ged\Controller\Document;

use Acl\Controller\Permission\IndexController as ParentController;
use Rbs\Space\Factory as DaoFactory;
use Acl\Model\Resource;
use Rbplm\Ged\Document;

/**
 */
class PermissionController extends ParentController
{

	public $pageId = 'ged_document_permission';

	public $defaultSuccessForward = 'ged/document/manager/index';

	public $defaultFailedForward = 'ged/document/manager/index';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultSuccessForward;
	}

	/**
	 *
	 */
	public function assignAction()
	{
		/* @var \Zend\Http\Request $request */
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', array());
			$id = $request->getQuery('id', current($ids));

			/**/
			$cid = Document\Version::$classId;
			$resourceCn = DaoFactory::get()->getDao(Resource::$classId)->getResourceCnFromReferIdAndCid($id, $cid);
		}

		return parent::assignAction($resourceCn);
	}
} /* End of class*/
