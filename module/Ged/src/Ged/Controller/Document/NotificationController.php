<?php
namespace Ged\Controller\Document;

use Ged\Controller\AbstractNotificationController as AbstractController;
use Rbplm\Ged\Document;
use Rbs\Ged\Document\Event;

/**
 */
class NotificationController extends AbstractController
{

	/** @var string */
	public $pageId = 'ged_document_notification';

	/** @var string */
	public $defaultSuccessForward = 'ged/document/notification/index';

	/** @var string */
	public $defaultFailedForward = 'ged/document/notification/index';

	/** @var \Rbplm\Ged\Document\Version */
	protected $reference;

	/**
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * @return array
	 */
	protected function _getEvents()
	{
		return array(
			Event::POST_CHECKIN => tra('checkin'),
			Event::POST_CHECKOUT => tra('checkout'),
			Event::POST_CANCELCHECKOUT => tra('cancel checkout'),
			Event::POST_DELETE => tra('delete'),
			Event::POST_MOVE => tra('move'),
			Event::POST_NEWVERSION => tra('new version'),
			Event::POST_COPY => tra('copy')
		);
	}

	/**
	 * 
	 * @return \Rbplm\Ged\Document\Version
	 */
	protected function _newReference()
	{
		return new Document\Version();
	}
}
