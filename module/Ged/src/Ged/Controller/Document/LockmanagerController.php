<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\AccessCode;
use Rbplm\People;
use Zend\Http\Request;

/**
 */
class LockmanagerController extends AbstractController
{

	/* @var string */
	public $pageId = 'document_lockmanager';

	/* @var string */
	public $defaultSuccessForward = 'home';

	/* @var string */
	public $defaultFailedForward = 'home';

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
		
		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax()
	{
		parent::initAjax();
		/* Set commons vars */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 */
	public function resetdocAction()
	{
		$service = new \Service\Controller\Document\LockmanagerService();
		$this->bindServiceRespons($service->resetService($this->getRequest()));
	}

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function checkoutAction()
	{
		/* set inputs */
		$view = $this->view;
		//$session = $this->session();
		
		/* @var \Zend\Http\Request $request */
		$request = $this->getRequest();
		
		if ( $request->isGet() ) {
			$documentIds = $request->getQuery('checked', []);
			$spacenames = $request->getQuery('spacenames', null);

			$documentId = $request->getQuery('id', null);
			$spacename = $request->getQuery('spacename', null);
			($documentId) ? $documentIds[] = $documentId : null;
			($documentId && $spacename) ? $spacenames[$documentId] = $spacename : null;

			unset($documentId, $spacename);
			$validate = false;
		}
		else {
			$spacenames = $request->getPost('spacenames', null);
			$documentIds = $request->getPost('checked', []);
			$ifExistmodes = $request->getPost('ifExistmode', []);
			$validate = $request->getPost('validate', false);
			$cancel = $request->getPost('cancel', false);

			if ( $cancel ) {
				$this->successForward();
			}
		}

		$wildspace = new People\User\Wildspace(People\CurrentUser::get());

		/* init form */
		$form = new \Ged\Form\Document\Lockmanager\Checkout\CheckoutForm();
		$form->setAttribute('action', $request->getUriString());

		/* display form */
		if ( !$validate ) {
			/* Documents to validate */
			$toValidate = [];
			$documents = [];
			$i = 1;
			foreach( $documentIds as $documentId ) {

				/* set spacename */
				isset($spacenames[$documentId]) ? $spacename = $spacenames[$documentId] : $spacename = false;
				if ( !$spacename ) {
					throw (new \Exception('spacename is not set for document ' . $documentId));
				}

				/* init helpers */
				$factory = DaoFactory::get($spacename);
				/** @var \Rbs\Ged\Document\VersionDao $dao */
				$dao = $factory->getDao(Document\Version::$classId);
				/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
				$dfDao = $factory->getDao(Docfile\Version::$classId);

				/* load document */
				$document = new Document\Version();
				$dao->loadFromId($document, $documentId);

				/* populate input to use if no validation is require */
				$documents['document' . $i] = array(
					'id' => $documentId,
					'spacename' => $spacename,
					'replace' => false,
					'getfile' => true
				);

				/* check access to document */
				$aCode = $document->checkAccess();
				if ( ($aCode > AccessCode::FREE) || ($aCode < AccessCode::FREE || $aCode === AccessCode::CHECKOUT) ) {
					$this->errorStack()->feedback(tra('this document is locked with code %acode%, checkOut is not permit'), [
						'acode' => $aCode
					]);
				}

				/* Checkout only MAIN, ANNEX, FRAGMENT and all other with role < 16 */
				$dfDao->loadDocfiles($document, $dfDao->getTranslator()->toSys('mainrole') . ' < ' . Docfile\Role::VISU);
				$confirmNeed = false;
				foreach( $document->getDocfiles() as $docfile ) {
					if ( $docfile->checkAccess() !== AccessCode::FREE ) {
						continue;
					}

					$docfile->setParent($document);

					/* check if file is exisiting in ws */
					$fileName = $docfile->getName();
					$wsFile = $wildspace->getPath() . '/' . $fileName;

					/* If File Exist in Wildspace */
					if ( is_file($wsFile) || is_dir($wsFile) ) {
						$form->addToConfirm($docfile);
						$toValidate[$documentId] = $documentId;
						$confirmNeed = true;
					}
				} /* foreach docfiles */

				if ( $confirmNeed == false ) {
					$form->addToCo($document);
				}

				$i++;
			} /* foreach document */

			if ( $toValidate ) {
				//$session->checkoutToValidate = $toValidate;
				$view->setTemplate($form->template);
				$view->form = $form;
				return $view;
			}
			/* no files existing in ws, co documents with replace option */
			else {
				try {
					$request = new Request();
					$request->setMethod(Request::METHOD_POST)
						->getPost()
						->set('documents', $documents);
					$service = new \Service\Controller\Document\LockmanagerService();
					$this->bindServiceRespons($service->checkoutService($request));
					$this->successForward();
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
					$this->errorForward();
				}
			}
		}
		/* validate */
		else {
			$i = 1;
			$documents = [];
			foreach( $documentIds as $documentId ) {
				if ( isset($ifExistmodes[$documentId]) ) {
					$ifExistmode = $ifExistmodes[$documentId];
					switch ($ifExistmode) {
						case 'keep':
							$getfile = false;
							$replace = false;
							break;
						case 'replace':
							$getfile = true;
							$replace = true;
							break;
						case 'ignore':
							continue;
							break;
					}
				}
				else {
					$getfile = true;
					$replace = false;
				}

				isset($spacenames[$documentId]) ? $spacename = $spacenames[$documentId] : $spacename = false;
				if ( !$spacename ) {
					throw (new \Exception('spacename is not set for document ' . $documentId));
				}

				$documents['document' . $i] = [
					'id' => $documentId,
					'spacename' => $spacename,
					'replace' => $replace,
					'getfile' => $getfile
				];
				$i++;
			}
			try {
				$request = new Request();
				$request->setMethod(Request::METHOD_POST)
					->getPost()
					->set('documents', $documents);
				$service = new \Service\Controller\Document\LockmanagerService();
				$this->bindServiceRespons($service->checkoutService($request));
				//$session->checkoutToValidate = null;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				$this->errorForward();
			}
			
			$this->successForward();
		}
	}

	/**
	 * Accept HTTP POST and GET request.
	 * 
	 * If GET request, requested data mmust have structure like this :
	 * 		'checked[]' = int [id of document 1]
	 * 		'checked[]' = int [id of document 2]
	 * 		'spacenames[id of document 1]' = string
	 * 		'spacenames[id of document 2]' = string
	 * 
	 * If POST request, requested data mmust have structure like this :
	 * 		documents = [
	 * 			document1=>[
	 * 				id => integer,
	 * 				spacename => string
	 * 			],
	 * 			...
	 * 		]
	 *
	 * @return string|\Application\View\ViewModel
	 */
	public function checkinAction()
	{
		/* init form */
		$form = new \Ged\Form\Document\Lockmanager\Checkin\CheckinForm();
		$form->setAttribute('method', 'post');
		$form->setAttribute('action', $this->url()
			->fromRoute('ged-document-lock', [
			'action' => 'checkin'
		]));
		$collection = $form->getObject();

		/* set inputs */
		$view = $this->view;
		$request = $this->request;
		if ( $request->isGet() ) {
			$spacenames = $request->getQuery('spacenames', null);
			$documentIds = $request->getQuery('checked', []);

			$documentId = $request->getQuery('id', null);
			$spacename = $request->getQuery('spacename', null);

			($documentId) ? $documentIds[] = $documentId : null;
			($documentId && $spacename) ? $spacenames[$documentId] = $spacename : null;

			unset($documentId, $spacename);

			$ciAndKeep = (boolean)($request->getQuery('checkinandkeep', false));
			$comment = trim($request->getQuery('comment', null));

			$documents = $request->getQuery('documents', null);

			if ( !$documents ) {
				foreach( $documentIds as $documentId ) {
					/* set spacename */
					isset($spacenames[$documentId]) ? $spacename = $spacenames[$documentId] : $spacename = false;
					if ( !$spacename ) {
						throw (new \Exception('spacename is not set for document ' . $documentId));
					}

					$documents[] = [
						'id' => $documentId,
						'spacename' => $spacename
					];
				}
			}

			$collection->populate([
				'checkinandkeep' => $ciAndKeep,
				'comment' => $comment
			]);

			$validate = false;
		}
		else {
			$documents = $request->getPost('documents', []);
			$inputPdmData = $request->getPost('pdmdata', '[]');

			$validate = $request->getPost('validate', false);
			$cancel = $request->getPost('cancel', false);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		foreach( $documents as $data ) {
			$documentId = (int)$data['id'];
			$spacename = $data['spacename'];

			/* init helpers */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);
			/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
			$dfDao = $factory->getDao(Docfile\Version::$classId);

			/* load document */
			$document = new Document\Version();
			$dao->loadFromId($document, $documentId);

			$aCode = $document->checkAccess();
			$coBy = $document->lockByUid;
			$coById = $document->lockById;

			if ( ($aCode != AccessCode::CHECKOUT) ) {
				$this->errorStack()->error(sprintf('Document %s is not checkout', $document->getNumber()));
				continue;
			}

			if ( ($coById != People\CurrentUser::get()->getId()) ) {
				$this->errorStack()->error(sprintf('Document %s is locked By %s', $document->getNumber(), $coBy));
				continue;
			}

			$document->hydrate(array_intersect_key($data, [
				'description' => 0,
				'tags' => 1
			]));

			$dfDao->loadDocfiles($document, $dfDao->getTranslator()->toSys('mainrole') . '=' . Docfile\Role::MAIN);
			$collection->add($document);
		}

		if ( $collection->count() == 0 ) {
			$this->errorStack()->error('Nothing to checkin');
			return $this->successForward();
		}
		$form->bind($collection);

		/* display form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$i = 1;
				$documents = [];
				foreach( $collection->getIterator() as $document ) {
					$documents['document' . $i] = array(
						'object' => $document,
						'comment' => $collection->comment,
						'checkinandkeep' => $collection->checkinandkeep,
						'pdmdata' => $inputPdmData
					);
					$i++;
				}

				try {
					$request = new Request();
					$request->setMethod(Request::METHOD_POST)
						->getPost()
						->set('documents', $documents);
					$service = new \Service\Controller\Document\LockmanagerService();
					$this->bindServiceRespons($service->checkinService($request));
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
					$this->errorForward();
				}
				$this->successForward();
			}
		}

		/* Display the template */
		$view->pageTitle = 'Checkin this documents.';
		$view->setTemplate($form->template);
		$view->form = $form;
		return $view;
	}

	/**
	 */
	public function lockAction($code = AccessCode::LOCKED)
	{
		/* set inputs */
		$view = $this->view;
		$request = $this->request;

		/**/
		if ( $request->isGet() ) {
			$spacenames = $request->getQuery('spacenames', []);
			$documentIds = $request->getQuery('checked', []);

			$documentId = $request->getQuery('documentid', null);
			$spacename = $request->getQuery('spacename', null);

			($documentId) ? $documentIds[] = $documentId : null;
			($documentId && $spacename) ? $spacenames[$documentId] = $spacename : null;
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$documentIds = $request->getPost('documentIds', []);
			$spacenames = $request->getPost('spacenames', null);

			$comment = trim($request->getPost('comment', null));
			$accessCode = $request->getPost('accessCode', []);
			$validate = $request->getPost('validate', false);
			$cancel = $request->getPost('cancel', false);

			if ( $cancel ) {
				$this->successForward();
			}
		}

		/**/
		if ( !$documentIds ) {
			$this->successForward();
		}

		/* init form */
		$form = new \Ged\Form\Document\Lockmanager\Lock\LockForm($view);
		$form->setAttribute('method', 'post');
		$form->setAttribute('action', $request->getUriString());

		/** @var \Rbs\Ged\Document\VersionDao $dao */

		foreach( $documentIds as $documentId ) {
			$spacename = $spacenames[$documentId];

			/* Init helpers */
			$factory = DaoFactory::get($spacename);
			$document = new Document\Version();
			$factory->getDao(Document\Version::$classId)->loadFromId($document, $documentId);
			try {
				$form->addDocument($document);
			}
			catch( \Rbplm\Ged\AccessCodeException $e ) {
				$this->errorStack()->error(tra("You cant lock this document"));
			}
		}

		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$i = 1;
				$documents = [];
				foreach( $form->documents as $document ) {
					$documents['document' . $i] = [
						'object' => $document
					];
					$i++;
				}

				try {
					$request = new Request();
					$request->setMethod(Request::METHOD_POST)
						->getPost()
						->fromArray([
						'documents' => $documents,
						'comment' => $comment
					]);

					$service = new \Service\Controller\Document\LockmanagerService();
					$this->bindServiceRespons($service->lockService($request, $accessCode));
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
					$this->errorForward();
				}
				$this->successForward();
			}
		}

		/* Display the template */
		$view->pageTitle = 'Lock this documents';
		$view->setTemplate($form->template);
		$view->form = $form;
		return $view;
	}
} /* End of class */

