<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Extended;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Discussion\Model\Comment;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class DetailController extends AbstractController
{

	/** @var string */
	public $pageId = 'document_detail';

	/** @var string */
	public $defaultSuccessForward = 'ged/document/detail/index';

	/** @var string */
	public $defaultFailedForward = 'ged/document/detail/index';

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$spacename = $this->params()->fromRoute('spacename', null);
		$documentId = $this->params()->fromRoute('id', null);

		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$tab = $request->getQuery('tab', null);
		}

		/* save this page in basecamp to return here after actions */
		$currentUri = $request->getUri()->getPath();
		$this->basecamp()->save($currentUri, $currentUri, $view);

		/* init some helpers */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Document\Version::$classId);

		/* Load extended properties in Dao */
		$loader = new Extended\Loader($factory);

		/* @var \Rbplm\Ged\Document\Version $document */
		try {
			$loader->loadFromDocumentId($documentId, $dao);
			$document = $dao->loadFromId(new Document\Version(), $documentId);
		}
		catch( \PDOException $e ) {
			if ( substr($e->getMessage(), 0, 15) == 'SQLSTATE[42S02]' ) {
				/* context is probably no setted */
				return $this->redirect()->toRoute('home');
			}
			else {
				throw $e;
			}
		}
		catch( \Rbplm\Dao\NotExistingException $e ) {
			/* document is not existing */
			$this->errorStack()->warning('Document has been deleted, you are redirected to home');
			return $this->redirect()->toRoute('ged-container-current');
		}

		/**/
		$parent = new \Rbplm\Org\Workitem();
		$factory->getDao($parent->cid)->loadFromId($parent, $document->parentId);
		$document->setParent($parent);

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('read', $document->parentUid);

		/* send extended properties to view */
		$extended = $dao->extended;
		$view->extended = $extended;
		$view->document = $document;
		$view->threedfile = '';

		if ( $tab == 'files' ) {
			$list = $this->_getAssocFiles($document);
			$view->associatedfiles = $list->toArray();
			$view->setTemplate('document/detail/associatedfiles.phtml');
		}
		elseif ( $tab == 'product' ) {
			$product = $this->_getProduct($document);
			if ( $product ) {
				$view->product = $list->getArrayCopy();
				$view->setTemplate('document/detail/product.phtml');
			}
		}
		elseif ( $tab == 'children' ) {
			$stmt = $this->_getChildren($document);
			$view->doclink_list = $stmt->fetchAll();
			$view->setTemplate('document/detail/relation.phtml');
		}
		elseif ( $tab == 'docparent' ) {
			$list = $this->_getParents($document);
			$view->father_documents = $list->toArray();
			$view->setTemplate('document/detail/parent.phtml');
		}
		elseif ( $tab == '' ) {
			$view->associatedfiles = $this->_getAssocFiles($document)->toArray();
			$view->children = $this->_getChildren($document)->fetchAll();
			$view->parents = $this->_getParents($document)->fetchAll();
			$view->discussion = $this->_getDiscussion($document);

			$product = $this->_getProduct($document);
			if ( $product ) {
				$view->product = $product;
				$view->pdmchildren = $this->_getPdmChildren($document)->fetchAll(\PDO::FETCH_ASSOC);
				if ( isset($product->threedfile) ) {
					$view->threedfile = $product->threedfile;
					$view->threedurl = $this->url()->fromRoute('viewer-getfile');
				}
			}
			$view->setTemplate('ged/document/detail/index.phtml');
		}

		/**/
		$view->spacename = $spacename;
		$view->documentid = $documentId;

		return $view;
	}

	/**
	 * Associated files
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return \Rbplm\Dao\ListInterface
	 */
	protected function _getAssocFiles($document)
	{
		/* get all file associated to the selected document */
		$documentId = $document->getId();
		$factory = $document->dao->factory;
		$list = $factory->getList(Docfile\Version::$classId);
		$dao = $factory->getDao(Docfile\Version::$classId);

		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = $asSys . ' as ' . $asApp;
		}
		$list->select($select);

		$list->load($dao->toSys('parentId') . '=:parentId', array(
			':parentId' => $documentId
		));

		/* Build roles list for select */
		$roles = \Rbplm\Ged\Docfile\Role::toArray();
		$this->view->roles = $roles;

		return $list;
	}

	/**
	 * Relationships
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return \PDOStatement
	 */
	protected function _getChildren($document)
	{
		$documentId = $document->getId();
		$factory = $document->dao->factory;
		$linkDao = $factory->getDao(Document\Link::$classId);
		$docDao = $factory->getDao($document->cid);
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$select = [];

		/* BUILD SELECT */
		foreach( $docDao->metaModel as $asSys => $asApp ) {
			$select[] = 'doc.' . $asSys . ' AS ' . $asApp;
		}
		foreach( $linkDao->metaModel as $asSys => $asApp ) {
			$select[] = 'lt.' . $asSys . ' AS link_' . $asApp;
		}
		$filter->select($select);
		$filter->andFind($documentId, 'lt.' . $linkDao->toSys('parentId'), Op::EQUAL);

		$stmt = $linkDao->getChildren($filter);
		return $stmt;
	}

	/**
	 * Relationships
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return \PDOStatement
	 */
	protected function _getParents($document)
	{
		$documentId = $document->getId();
		$factory = $document->dao->factory;
		$linkDao = $factory->getDao(Document\Link::$classId);
		$docDao = $factory->getDao($document->cid);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		/* BUILD SELECT */
		foreach( $docDao->metaModel as $asSys => $asApp ) {
			$select[] = 'doc.' . $asSys . ' AS ' . $asApp;
		}
		foreach( $linkDao->metaModel as $asSys => $asApp ) {
			$select[] = 'lt.' . $asSys . ' AS link_' . $asApp;
		}
		$filter->select($select);
		$filter->andFind($documentId, 'lt.' . $linkDao->toSys('childId'), Op::EQUAL);

		$stmt = $linkDao->getParents($filter);
		return $stmt;
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return \Rbplm\Pdm\Product\Version|NULL
	 */
	protected function _getProduct($document)
	{
		$documentId = $document->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = $document->dao->factory;

		/** @var \Rbs\Pdm\Product\VersionDao $dao */
		$dao = $factory->getDao(\Rbplm\Pdm\Product\Version::$classId);

		$filter = $dao->toSys('documentId') . "=:documentId";
		$bind = array(
			':documentId' => $documentId
		);
		$product = new \Rbplm\Pdm\Product\Version();

		try {
			$dao->load($product, $filter, $bind);
			$product->spacename = $document->spacename;
			$passphrase = \Ranchbe::get()->getConfig('visu.passphrase');
			$path = \Ranchbe::get()->getConfig('viewer.reposit.path');
			$threedfile = \Rbplm\Pdm\Product\File::encodename($product->getId(), 'stl', $passphrase);
			if ( is_file($path . '/' . $threedfile) ) {
				$product->threedfile = $threedfile;
			}
			return $product;
		}
		catch( \Exception $e ) {
			return null;
		}
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 */
	protected function _getDiscussion($document)
	{
		/** @var \Rbs\Space\Factory $factory */
		$factory = $document->dao->factory;
		$discussionUid = $document->getUid();
		$dao = $factory->getDao(Comment::$classId);

		$list = $factory->getList(Comment::$classId);
		$filter = $factory->getFilter(Comment::$classId);
		$filter->andFind($discussionUid, $dao->toSys('discussionUid'), Op::EQUAL);
		$filter->select($dao->getSelectAsApp());
		$list->load($filter);
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Pdm\Product\Version $product
	 * @return \PDOStatement
	 */
	protected function _getPdmChildren($document)
	{
		$documentId = $document->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = $document->dao->factory;

		/** @var \Rbs\Ged\Document\VersionDao $documentDao */
		$documentDao = $factory->getDao(Document\Version::$classId);
		/** @var \Rbs\Ged\Document\PdmLinkDao $linkDao */
		$linkDao = $factory->getDao(Document\PdmLink::$classId);
		/** @var \Rbs\Pdm\Product\VersionDao $productDao */
		$productDao = $factory->getDao(\Rbplm\Pdm\Product\Version::$classId);

		try {
			/** @var \PDOStatement $stmt */
			$filter = $factory->getFilter(Document\PdmLink::$classId);
			$filter->select(array(
				'inst.*',
				'doc.' . $documentDao->toSys('id') . ' as documentId',
				'doc.' . $documentDao->toSys('uid') . ' as documentUid',
				'doc.' . $documentDao->toSys('name') . ' as documentName',
				'doc.' . $documentDao->toSys('number') . ' as documentNumber',
				'doc.' . $documentDao->toSys('description') . ' as documentDescription',
				'doc.' . $documentDao->toSys('spacename') . ' as spacename'
			));
			$filter->andFind($documentId, 'parent.' . $productDao->toSys('documentId'), Op::EQUAL);
			$stmt = $linkDao->getChildren($filter);
			return $stmt;
		}
		catch( \Exception $e ) {
			throw $e;
			return null;
		}
	}

	/**
	 */
	public function _getDetail()
	{}
} /* End of class */

