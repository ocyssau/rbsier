<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Zend\Form\FormInterface;

/**
 */
class VersionController extends AbstractController
{

	public $pageId = 'document_version_manager';

	public $defaultSuccessForward = 'ged/document/manager/index';

	public $defaultFailedForward = 'ged/document/manager/index';

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * Upgrade indice of document
	 */
	public function newversionAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$documentIds = $request->getQuery('checked', []);
			$spacenames = $request->getQuery('spacenames', null);

			$documentId = $request->getQuery('id', null);
			$spacename = $request->getQuery('spacename', null);

			($documentId) ? $documentIds[] = $documentId : null;
			($documentId && $spacename) ? $spacenames[$documentId] = $spacename : null;

			$spacename = null;
			$documentId = null;
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$spacenames = $request->getPost('spacenames', []);
			$documentIds = $request->getPost('checked', []);

			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);

			if ( $cancel ) {
				$this->successForward();
			}
		}

		/**/
		if ( !$documentIds ) {
			$this->successForward();
		}

		/* init form */
		$form = new \Ged\Form\Document\Version\NewversionForm();
		$form->setAttribute('method', 'post');
		$form->setAttribute('action', $request->getUriString());

		/* display form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$datas = $form->getData(FormInterface::VALUES_AS_ARRAY);
				$comment = $datas['comment'];
				$toVersion = $datas['version'];
				$toContainerId = $datas['tocontainerid'];

				$documents = [];
				$i = 1;
				foreach( $documentIds as $documentId ) {
					$spacename = $spacenames[$documentId];
					$documents['document' . $i] = [
						'id' => $documentId,
						'spacename' => $spacename
					];
					$i++;
				}
				try {
					$request->getPost()->set('documents', $documents);
					$request->getPost()->set('tocontainerid', $toContainerId);
					$request->getPost()->set('version', $toVersion);
					$request->getPost()->set('comment', $comment);
					$service = new \Service\Controller\Document\VersionService();
					$this->bindServiceRespons($service->newversionService($request));
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
					$this->errorForward();
				}
				$this->successForward();
			}
		}
		else {
			foreach( $documentIds as $documentId ) {
				$spacename = $spacenames[$documentId];

				/**/
				$factory = DaoFactory::get($spacename);
				/** @var \Rbs\Ged\Document\VersionDao $dao */
				$dao = $factory->getDao(Document\Version::$classId);

				$document = new Document\Version();
				$dao->loadFromId($document, $documentId);
				//$document->dao = $dao;

				try {
					/* Check permissions */
					$this->getAcl()->checkRightFromUid('edit', $document->parentUid);
					$form->addDocument($document);
				}
				catch( \Rbplm\Ged\AccessCodeException $e ) {
					$this->errorStack()->error(sprintf(tra("You cant lock document %s : %s"), $document->getName(), $e->getMessage()));
				}
			}
		}

		/* Display the template */
		$view->pageTitle = tra('New Version of Documents');
		$view->setTemplate($form->template);
		$view->form = $form;
		return $view;
	}
} /* End of class */
