<?php
// %LICENCE_HEADER%
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
//use Rbplm\Ged\AccessCode;
use Rbplm\Ged\Document;
use Rbplm\People;
//use Zend\Stdlib\Parameters;
//use Zend\Http\Request;
use Application\Controller\ControllerException;
//use Rbs\Sys\Session;
use Rbplm\Sys\Fsdata;

/**
 * @package 
 */
class ReadController extends AbstractController
{

	/** @var string */
	public $pageId = 'document_read';

	/** @var string */
	public $defaultSuccessForward = '/home';

	/** @var string */
	public $defaultFailedForward = '/home';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * @return string|\Application\View\ViewModel
	 */
	public function indexAction()
	{
		/* set inputs */
		$view = $this->view;
		$request = $this->request;
		if ( $request->isGet() ) {
			$spacenames = $request->getQuery('spacenames', null);
			$documentIds = $request->getQuery('checked', []);

			$documentId = $request->getQuery('id', null);
			$spacename = $request->getQuery('spacename', null);

			($documentId) ? $documentIds[] = $documentId : null;
			($documentId && $spacename) ? $spacenames[$documentId] = $spacename : null;
			unset($documentId, $spacename);

			$documents = $request->getQuery('documents', null);
			if ( !$documents ) {
				foreach( $documentIds as $documentId ) {
					/* set spacename */
					isset($spacenames[$documentId]) ? $spacename = $spacenames[$documentId] : $spacename = false;
					if ( !$spacename ) {
						throw (new \Exception('spacename is not set for document ' . $documentId));
					}

					$documents[] = [
						'id' => $documentId,
						'spacename' => $spacename
					];
				}
			}

			$validate = false;
		}
		else {
			$documents = $request->getPost('documents', []);
			$validate = $request->getPost('validate', false);
			$cancel = $request->getPost('cancel', false);
			if ( $cancel ) {
				return;
			}
		}

		if ( !$documents ) {
			throw new ControllerException('None documents selected');
		}

		/* init form */
		$form = new \Ged\Form\Document\Read\ReadForm();
		$form->setAttribute('method', 'post');
		$form->setAttribute('action', $this->url()
			->fromRoute('ged-document-read', [
			'action' => 'index'
		]));
		$collection = $form->getObject();

		/* load documents and populate collection */
		foreach( $documents as $data ) {
			$documentId = (int)$data['id'];
			$spacename = $data['spacename'];

			/* init helpers */
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);
			/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
			$dfDao = $factory->getDao(Docfile\Version::$classId);

			/* Check permission */
			//$this->getAcl()->checkRightFromUid('read', $document->parentUid);

			/* load document */
			$document = new Document\Version();
			$dao->loadFromId($document, $documentId);

			$dfDao->loadDocfiles($document);
			$collection->add($document);
		}

		/* nothing in collection */
		if ( $collection->count() == 0 ) {
			$this->errorStack()->error('Nothing to read');
			return $this->successForward();
		}

		/* init default value of form */
		$collection->getmode = $form::MODE_ALL;
		$collection->putInWilspace = true;
		$collection->zip = false;
		$collection->withVersion = true;

		/* bind */
		$form->bind($collection);

		/* display form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				/* if create subfolder is requested */
				if ( $collection->putInWilspace ) {
					$wildspace = People\User\Wildspace::get(People\CurrentUser::get());
					$path = $wildspace->getPath();
					if ( $collection->inSubDirectory ) {
						if ( $collection->subDirectoryName ) {
							$path = $path . '/' . $collection->subDirectoryName;
							if ( !is_dir($path) ) {
								mkdir($path);
							}
						}
					}
				}
				else {
					$path = false;
				}

				/* open zip file if required*/
				if ( $collection->zip ) {
					$zipfile = '';
					try {
						$zipfile = tempnam(sys_get_temp_dir(), 'rb') . '.zip';
						$zip = new \ZipArchive();
						$zip->open($zipfile, \ZipArchive::CREATE);
						$this->errorStack()->log(sprintf('open temp zip file %s', $zipfile));
					}
					catch( \Throwable $e ) {
						$msg = sprintf('unable to open zip file %s, may be no write permission, or ZipArchive is not installed', $zipfile);
						$msg = $msg . ' >> ' . $e->getMessage();
						$this->errorStack()->log($msg);
						throw new ControllerException($msg);
					}
				}

				foreach( $collection->getIterator() as $document ) {
					try {
						$docfiles = $document->getDocfiles();

						/* filter belong mode */
						if ( $collection->getmode == $form::MODE_MAIN ) {
							$mainDf = current($docfiles);
							$docfiles = [
								$mainDf
							];
						}
						else if ( $collection->getmode == $form::MODE_VISU ) {
							$t = [];
							foreach( $docfiles as $docfile ) {
								if ( $docfile->getRole() == Docfile\Role::VISU ) {
									$t = [
										$docfile
									];
								}
							}
							if ( count($t) == 0 ) {
								rewind($docfiles);
								$t[] = current($docfiles);
							}
							$docfiles = $t;
							unset($t);
						}

						/* set name of file send to user */
						foreach( $docfiles as $docfile ) {
							/* @var \Rbplm\Sys\Datatype\File $fsdata */
							$fsdata = $docfile->getData()->getFsdata();

							/* if add iteration-version to filename */
							if ( $collection->withVersion ) {
								$docfile->userfilename = $fsdata->getRootname() . '.v' . $document->version . '.' . $document->iteration . $fsdata->getExtension();
							}
							else {
								$docfile->userfilename = $fsdata->getName();
							}

							$docfile->todir = $path;

							if ( $collection->putInWilspace ) {
								/* path to ws file */
								$targetfile = $docfile->todir . '/' . $docfile->userfilename;

								/* replace target file only if name is versionned */
								if ( $collection->withVersion ) {
									$replace = true;
									if ( is_file($targetfile) ) {
										$this->errorStack()->log(sprintf('file %s will be replace', $targetfile));
									}
								}
								else {
									$replace = false;
								}

								/* copy vault file to target file */
								$fsdata->copy($targetfile, 0666, $replace);
							}

							/* if zip */
							if ( $collection->zip ) {
								$this->errorStack()->log(sprintf('add file %s to zip archive', $fsdata->getFullpath()));
								$zip->addFile($fsdata->getFullpath(), $docfile->userfilename);
							}
						}
					}
					catch( \Throwable $e ) {
						$this->errorStack()->error($e->getMessage());
						$this->errorForward();
					}
				} /* foreach */

				/* download zip file */
				if ( $collection->zip ) {
					$zip->close();
					$fsdata = new Fsdata($zipfile);
					$fsdata->download('ranchbe-read.zip');
					unlink($zipfile);
					die();
				}
			} /* for is valid */
		} /* is validate */

		/* Display the template */
		$view->pageTitle = 'Read this documents.';
		$view->setTemplate($form->template);
		$view->form = $form;
		return $view;
	}
}
