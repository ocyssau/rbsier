<?php
namespace Ged\Controller\Document\History;

use Application\Controller\AbstractController;
use Application\Controller\ControllerException;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Filter\Op;
use Rbs\Ged\Document\History;

/**
 */
class BatchController extends AbstractController
{

	/** @var string */
	public $pageId = 'document_batch_history';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$this->layout()->tabs = $tabs;
	}

	/**
	 * @param \Zend\View\Model\ViewModel $view
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param \Rbs\Space\Factory $factory
	 */
	protected function _index($view, $filter, $factory)
	{
		/* init some vars */
		$this->layoutSelector()->clear($this);
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* init some helpers */
		$spacename = $view->spacename;

		/* @var \Rbs\Space\Workitem\Document\History\BatchDao $dao */
		$dao = $factory->getDao(History\Batch::$classId);
		$table = $factory->getTable(History\Batch::$classId);
		$list = $factory->getList(History\Batch::$classId);

		/* Select main table */
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = "`$table`.`$asSys` AS `$asApp`";
		}
		$select[] = "'$spacename' AS spacename";
		$filter->select($select);

		/* Filter Form */
		$filterForm = new \Ged\Form\Document\History\Batch\FilterForm($factory);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(History\Batch::$classId);
		$list->countAll = $list->countAll($filter, $filter->getBind());

		/* Paginator */
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($this->getRequest()
			->getPost())
			->setData($this->getRequest()
			->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		try {
			$list->load($filter, $filter->getBind());
		}
		catch( \PDOException $e ) {
			$query = $list->stmt->queryString;
			$message = $e->getMessage() . ' - SQL QUERY: ' . $query;
			throw new \Application\Controller\ControllerException($message);
		}

		$view->list = $list->toArray();
		$view->filter = $filterForm;

		return $view;
	}

	/**
	 * Get documents history involved by batch
	 *
	 * @return \Application\View\ViewModel
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/document/history/batch/index');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$batchUid = $this->params()->fromRoute('uid', null);
			$spacename = $this->params()->fromRoute('spacename', null);
		}
		else {
			throw new \Application\Controller\ControllerException('This request must be GET only');
		}

		/* */
		if ( $batchUid ) {
			$params = array(
				'uid' => $batchUid,
				'spacename' => $spacename
			);
		}
		else {
			$params = array(
				'spacename' => $spacename
			);
		}
		$url = $this->url()->fromRoute('ged-document-history-batch', $params);
		$this->defaultSuccessForward = $url;
		$this->defaultFailedForward = $url;
		$this->basecamp()->setForward($this);

		/* Check permissions */
		//$this->getAcl()->checkRightFromIdAndCid('read', $containerId, \Rbplm\Org\Workitem::$classId);

		/* init some helpers */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(History\Batch::$classId);

		/* set filter */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->andfind('', $dao->toSys('uid'), Op::NOTNULL);
		$filter->isDistinct(true);
		/* set filter */
		if ( $batchUid ) {
			$filter->andFind(':uid', $dao->toSys('uid'), Op::EQUAL);
			$bind = array(
				':uid' => $batchUid
			);
			$filter->setBind($bind);
		}

		$view->spacename = $spacename;
		$view->pageTitle = 'Batchs ' . $batchUid;
		$view->uid = $batchUid;
		$view->url = $url;

		return $this->_index($view, $filter, $factory);
	}

	/**
	 * Get documents history involved by batch
	 *
	 * @return \Application\View\ViewModel|string
	 */
	public function ofcontainerAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/document/history/batch/index');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$containerId = $this->params()->fromRoute('containerId', null);
			$spacename = $this->params()->fromRoute('spacename', null);
		}
		else {
			throw new ControllerException('This request must be GET only');
		}

		/* */
		$url = $this->url()->fromRoute('ged-document-history-batch-ofcontainer', array(
			'containerId' => $containerId,
			'spacename' => $spacename
		));
		$this->defaultSuccessForward = $url;
		$this->defaultFailedForward = $url;
		$this->basecamp()->setForward($this);

		/* Check permissions */
		$this->getAcl()->checkRightFromIdAndCid('read', $containerId, \Rbplm\Org\Workitem::$classId);

		/**/
		$factory = DaoFactory::get($spacename);

		/* set filter */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		if ( $containerId ) {
			$joinTable = $spacename . '_documents_history';
			$filter->with(array(
				'table' => $joinTable,
				'lefton' => 'uid',
				'righton' => 'action_batch_uid',
				'alias' => 'doc'
			));
			$filter->andfind(':containerId', 'doc.container_id', Op::EQUAL);
			$filter->setBind([
				':containerId' => $containerId
			]);
		}

		$view->spacename = $spacename;
		$view->url = $url;
		$view->containerId = $containerId;
		$view->uid = null;
		$view->pageTitle = 'Batches Of ' . $spacename . '/' . $containerId;

		return $this->_index($view, $filter, $factory);
	}
} /* End of class */
