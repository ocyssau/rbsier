<?php
namespace Ged\Controller\Document\History;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Ged\Document\History;
use Zend\View\Model\ViewModel;
use Rbs\Dao\Sier\Filter as DaoFilter;

/**
 */
class AbstractController extends \Application\Controller\AbstractController
{

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * @param ViewModel $view
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param DaoFactory $factory
	 */
	protected function _index(ViewModel $view, DaoFilter $filter, DaoFactory $factory)
	{
		/* init some vars */
		$request = $this->getRequest();
		$spacename = $view->spacename;
		$this->layoutSelector()->clear($this);
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* init some helpers */
		$history = new History();
		/** @var \Rbs\Dao\Sier\MetamodelTranslator $trans */
		$table = $factory->getTable($history->cid);
		$trans = $factory->getDao($history->cid)->getTranslator();

		/* Select main table */
		$select = $trans->getSelectAsApp($table);
		
		$select[] = "'$spacename' AS data_spacename";
		$filter->select($select);

		/* Filter Form */
		$filterForm = new \Ged\Form\Document\History\FilterForm($factory);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		/* @var \Rbs\Dao\Sier\DaoList $list */
		$list = $factory->getList($history->cid);
		try {
			$list->countAll = $list->countAll($filter, $filter->getBind());
		}
		catch( \PDOException $e ) {
			$query = $list->stmt->queryString;
			$message = $e->getMessage() . ' - SQL QUERY: ' . $query;
			throw new \Application\Controller\ControllerException($message);
		}

		/* Paginator */
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		
		/* 		Paginator default values */
		$paginator->limit = 50;
		$paginator->orderby = $trans->toSys('action_id');
		
		/* 		Paginator populate */
		$paginator->setData($request->getPost())->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		try {
			$list->load($filter, $filter->getBind());
		}
		catch( \PDOException $e ) {
			$message = $e->getMessage() . ' - SQL QUERY: ' . $list->stmt->queryString;
			throw new \Application\Controller\ControllerException($message);
		}

		$view->list = $list->toArray();
		$view->filter = $filterForm;

		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$spacename = $request->getPost('spacename', $this->params()
				->fromRoute('spacename', null));
			$documentId = $request->getPost('documentid', null);
			$checked = $request->getPost('checked', []);
		}
		else {
			throw new \Application\Controller\ControllerException('This request must be POST only');
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(History::$classId);

		foreach( $checked as $historyOrder ) {
			try {
				$dao->deleteFromId($historyOrder);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}

		return $this->successForward([
			'documentid' => $documentId,
			'spacename' => $spacename
		]);
	}
} /* End of class */
