<?php
namespace Ged\Controller\Document\History;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Filter\Op;

/**
 */
class ContainerController extends AbstractController
{

	/** @var string */
	public $pageId = 'document_ofcontainer_history';

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/document/history/index');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $this->params()->fromRoute('spacename', null);
			$containerId = $this->params()->fromRoute('containerId', null);
		}
		else {
			throw new \Application\Controller\ControllerException('This request must be GET only');
		}

		/* */
		$url = $this->url()->fromRoute('ged-document-history-ofcontainer', array(
			'containerId' => $containerId,
			'spacename' => $spacename
		));
		$this->defaultSuccessForward = $url;
		$this->defaultFailedForward = $url;
		$this->basecamp()->setForward($this);

		/* Check permissions */
		$this->getAcl()->checkRightFromIdAndCid('read', $containerId, \Rbplm\Org\Workitem::$classId);

		/**/
		$factory = DaoFactory::get($spacename);

		/* set filter */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		if ( $containerId ) {
			$joinTable = $spacename . '_documents';
			$filter->with(array(
				'table' => $joinTable,
				'lefton' => 'document_id',
				'righton' => 'id',
				'alias' => 'doc'
			));
			$filter->andfind(':containerId', 'doc.container_id', Op::EQUAL);
			$filter->setBind([
				':containerId' => $containerId
			]);
		}

		$view->spacename = $spacename;
		$view->url = $url;
		$view->containerId = $containerId;
		$view->pageTitle = 'History Of ' . $spacename;

		return $this->_index($view, $filter, $factory);
	}
} /* End of class */
