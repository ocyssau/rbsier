<?php
namespace Ged\Controller\Document\History;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Filter\Op;
use Application\Controller\ControllerException;
use Rbplm\Ged\Document;
use Rbplm\Org\Workitem;

/**
 */
class DocumentController extends AbstractController
{

	/** @var string */
	public $pageId = 'document_history';

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/document/history/index');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $this->params()->fromRoute('spacename', null);
			$documentId = $this->params()->fromRoute('id', null);
		}
		else {
			throw new ControllerException('This request must be GET only');
		}

		/* */
		$url = $this->url()->fromRoute('ged-document-history', array(
			'id' => $documentId,
			'spacename' => $spacename
		));
		$this->defaultSuccessForward = $url;
		$this->defaultFailedForward = $url;
		$this->basecamp()->setForward($this);

		/**/
		$factory = DaoFactory::get($spacename);

		/* load document */
		$document = new Document\Version();
		$document->dao = $factory->getDao(Document\Version::$classId);
		$document->dao->loadFromId($document, $documentId);

		/* Check permissions */
		$this->getAcl()->checkRightFromIdAndCid('read', $document->parentId, Workitem::$classId);

		/* set filter */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		if ( $documentId ) {
			$table = $spacename . '_documents';
			$filter->andfind('@SQL(SELECT number FROM ' . $table . ' WHERE id=:documentId)', 'number', Op::EQUAL);
			$filter->setBind([
				':documentId' => $documentId
			]);
		}

		$view->spacename = $spacename;
		$view->url = $url;
		$view->pageTitle = 'History Of ' . $factory->getName();
		$view->documentId = $documentId;

		return $this->_index($view, $filter, $factory);
	}

	/**
	 * Get documents history involved by batch
	 *
	 * @return \Application\View\ViewModel|string
	 */
	public function indexofbatchAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/document/history/index');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$batchUid = $this->params()->fromRoute('uid', null);
			$spacename = 'workitem';
		}
		else {
			throw new \Application\Controller\ControllerException('This request must be GET only');
		}

		/* */
		$url = $this->url()->fromRoute('ged-document-history-batch-index', array(
			'uid' => $batchUid
		));
		$this->defaultSuccessForward = $url;
		$this->defaultFailedForward = $url;
		$this->basecamp()->setForward($this);

		/**/
		$factory = DaoFactory::get($spacename);

		/* set filter */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		if ( $batchUid ) {
			$filter->andfind(':batchUid', 'action_batch_uid', Op::EQUAL);
			$filter->setBind([
				':batchUid' => $batchUid
			]);
		}

		$view->spacename = $spacename;
		$view->url = $url;
		$view->pageTitle = 'History Of ' . $factory->getName();
		$view->documentId = null;

		return $this->_index($view, $filter, $factory);
	}
} /* End of class */
