<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbs\Dao\Sier\Filter;
use Rbplm\Dao\Filter\Op;
use Rbs\Extended;
use Zend\Form\FormInterface;
use Zend\Stdlib\Parameters;

/**
 */
class ManagerController extends AbstractController
{

	/** @var string */
	public $pageId = 'document_manager';

	/** @var string */
	public $defaultSuccessForward = 'ged/document';

	/** @var string */
	public $defaultFailedForward = 'ged/document';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		$this->spacename = $this->params()->fromRoute('spacename', null);
		$this->containerId = $this->params()->fromRoute('id', null);
		
		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		$toUrl = $this->url()->fromRoute('ged-document-manager', [
			'spacename' => $this->spacename,
			'id' => $this->containerId,
			'action' => 'index'
		]);

		$this->ifSuccessForward = $toUrl;
		$this->ifFailedForward = $toUrl;
		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();

		$this->spacename = $this->params()->fromRoute('spacename', null);
		$this->containerId = $this->params()->fromRoute('id', null);
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Read contents of the constainer actually activate in context
	 */
	public function indexofcurrentAction()
	{
		$spacename = $this->spacename;
		$containerId = $this->containerId;
		$this->forward()->dispatch('\Ged\Controller\Document\Manager', array(
			'action' => 'index',
			'id' => $containerId,
			'spacename' => $spacename
		));
		return $this->view;
	}

	/**
	 *
	 * @return string|\Application\View\ViewModel
	 */
	public function indexAction()
	{
		$view = $this->view;

		$view->setTemplate('ged/document/manager/index');
		$spacename = $this->spacename;
		$containerId = $this->containerId;

		if ( !$spacename ) {
			throw new \Application\Controller\ControllerException('spacename is not set');
		}
		if ( !$containerId ) {
			throw new \Application\Controller\ControllerException('containerId is not set');
		}

		/**/
		$this->pageId = $spacename . '_' . $this->pageId;

		/* */
		$this->layoutSelector()->clear($this);
		$url = $this->url()->fromRoute('ged-document-manager', [
			'action' => 'index',
			'spacename' => $spacename,
			'id' => $containerId
		]);
		$this->basecamp()->save($url, $url, $view);

		/* */
		$factory = DaoFactory::get($spacename);

		/* */
		/* Load container */
		$container = $factory->getModel(\Rbplm\Org\Workitem::$classId);
		$container->dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
		$container = $container->dao->loadFromId($container, $containerId);

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('read', $container->getUid());

		/* Load extended properties */
		$dao = $factory->getDao(Document\Version::$classId);
		$this->loadExtended($dao, $containerId);
		$view->extended = $dao->extended;

		$view->pageTitle = sprintf(tra('Documents Manager Of %s'), $container->getNumber());
		$view->pageSubTitle = $container->description;

		/* */
		$filterForm = $this->getFilter($factory, $containerId);
		$filter = $filterForm->daoFilter;
		($containerId) ? $filter->andfind($containerId, $dao->getTranslator()->toSys('parentId'), Op::EQUAL) : null;
		$list = $this->getList($filter, $factory);

		$list->countAll = $list->countAll($filter);
		$filterForm->paginator->setMaxLimit($list->countAll)
			->bindToFilter($filter)
			->bindToView($view)
			->save();

		/* */
		$list->load($filter, $filterForm->bind);

		/* get fixed document */
		$this->fixedDocument($view);

		/* Display */
		$view->list = $list->toArray();
		$view->filter = $filterForm;
		$view->columns = $filterForm->columnSelector->getSelected();
		$view->displayThumbs = $filterForm->get('displayThumbs')->getValue();
		$view->containerid = $containerId;
		$view->spacename = $spacename;
		return $view;
	}

	/**
	 * Load Extended Properties
	 */
	protected function loadExtended($dao, $containerId)
	{
		$loader = new Extended\Loader($dao->factory);
		//$loader->load(Document\Version::$classId, $dao);
		$loader->loadFromContainerId($containerId, $dao, array(
			'name',
			'appName',
			'label',
			'type'
		));
		$dao->loader = $loader;
	}

	/**
	 *
	 */
	protected function fixedDocument($view)
	{
		$context = \Ranchbe::get()->getServiceManager()->getContext();
		if ( $context->getData('fixedDocumentId') ) {
			$factory = DaoFactory::get($context->getData('fixedDocumentSpacename'));
			$documentId = $context->getData('fixedDocumentId');
			$dao = $factory->getDao(Document\Version::$classId);
			$fixedDocument = $dao->loadFromId(new Document\Version(), $documentId);
			$view->fixedDocument = $fixedDocument;
		}
	}

	/**
	 * 
	 */
	protected function getFilter($factory, $containerId, $sessionId = null)
	{
		/* */
		$filter = new Filter('', false);
		$filter->setAlias('doc');

		/* */
		(!$sessionId) ? $sessionId = $this->pageId : null;
		$filterForm = new \Ged\Form\Document\FilterForm($factory, $containerId, $sessionId);
		$filterForm->setAttribute('id', 'documentfilterform');
		$filterForm->load();
		$dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
		if ( isset($dao->extended) ) {
			$filterForm->setExtended($dao->extended);
		}

		/* */
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->load(['default'=>[
			'orderby'=>'number',
			'order'=>'asc',
			'limit' => 50
		]]);
		
		/* */
		$columnSelector = $this->columnSelector()->load($dao->extendedModel, $this->getRequest()->getQuery('columns', null));

		/* */
		$filterForm->paginator = $paginator;
		$filterForm->columnSelector = $columnSelector;
		$filterForm->daoFilter = $filter;
		$filterForm->bindToFilter($filter)->save();
		return $filterForm;
	}

	/**
	 *
	 * @param \Rbplm\Dao\FilterAbstract $filter
	 * @param \Rbs\Space\Factory $factory
	 * @param \Ged\Form\Document\FilterForm $filterForm
	 */
	public function getList($filter, $factory, $filterForm = null)
	{
		$pdo = $factory->getConnexion();
		$dao = $factory->getDao(Document\Version::$classId);
		$list = $factory->getList(Document\Version::$classId);
		$list->dao = $dao;
		$select = array();
		$filter->setAlias('doc');
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = 'doc.' . $asSys . ' AS ' . $asApp;
		}

		/* Load Extended Properties */
		$loader = new Extended\Loader($factory);
		$extendedMetadatas = $loader->load(Document\Version::$classId, $dao);
		if ( is_array($dao->extendedModel) ) {
			foreach( $dao->extendedModel as $asSys => $asApp ) {
				$select[] = $asSys . ' AS ' . $pdo->quote($asApp);
			}
		}

		$filter->select($select);
		if ( $filterForm ) {
			$filterForm->setExtended($extendedMetadatas);
		}

		return $list;
	}

	/**
	 * FOR HOMOGENIOUS SPACENAME ONLY
	 * 
	 */
	public function editmultiAction()
	{
		$view = $this->view;

		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$documentIds = $request->getQuery('checked', array());
			$spacenames = $request->getQuery('spacenames', []);
			$validate = false;
			$cancel = false;
		}
		elseif ( $request->isPost() ) {
			$documentIds = $request->getPost('checked', null);
			$spacenames = $request->getPost('spacenames', []);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		$documentId = current($documentIds);
		$spacename = $spacenames[$documentId];

		/* init some helpers */
		$factory = DaoFactory::get($spacename);

		/* load first document, and assume that all other documents are in same OU */
		$document = new Document\Version();
		$dao = $factory->getDao(Document\Version::$classId);

		/* Construct the form */
		$form = new \Ged\Form\Document\EditMultiForm();
		$form->setUrlFromCurrentRoute($this);

		$containerId = null;
		foreach( $documentIds as $documentId ) {
			/* Unselect document with other spacename */
			if ( $spacenames[$documentId] != $spacename ) {
				$sn = $spacenames[$documentId];
				$this->errorStack()->log(sprintf('document id %s has a spacename %s not conform with spacename %s of current selection. Document is ignored.', $documentId, $sn, $spacename));
				unset($documentIds[$documentId]);
				continue;
			}

			$document = new Document\Version();
			$dao->loadFromId($document, $documentId);

			/* ignore documents from other space */
			if ( isset($spacenames[$documentId]) ) {
				if ( $spacenames[$documentId] != $spacename ) {
					continue;
				}
			}

			/* ignore documents from other container */
			if ( $containerId ) {
				if ( $containerId != $document->parentId ) {
					continue;
				}
			}

			$containerId = $document->parentId;

			/* Check permissions */
			$this->getAcl()->checkRightFromUid('edit', $document->parentUid);
			$form->addDocument($document);
		}
		$form->selectCategory($containerId, $factory);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$values = $form->getData(FormInterface::VALUES_AS_ARRAY);
				$description = $values['description'];
				$categoryId = $values['categoryId'];
				foreach( $form->documents as $document ) {
					$toSave = false;
					if ( $description ) {
						$document->description = $description;
						$toSave = true;
					}
					if ( $categoryId ) {
						$document->categoryId = $categoryId;
						$toSave = true;
					}
					if ( $toSave ) {
						$document->dao->save($document);
					}
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = tra(sprintf('Edit Documents'));
		$view->form = $form;
		$view->containerid = $containerId;
		$view->spacename = $spacename;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 * 
	 */
	public function editAction()
	{
		$view = $this->view;

		$documentId = $this->params()->fromRoute('id');
		$spacename = $this->params()->fromRoute('spacename');

		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}
		else {
			$validate = false;
			$cancel = false;
		}

		/* init some helpers */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Document\Version::$classId);

		/* Load extended properties */
		$loader = new Extended\Loader($factory);
		$loader->loadFromDocumentId($documentId, $dao);

		/* Load document */
		$document = new Document\Version();
		$dao->loadFromId($document, $documentId);
		$containerId = $document->parentId;

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

		/* Construct the form */
		$form = new \Ged\Form\Document\EditForm($factory);
		$form->selectCategory($containerId);
		$form->setUrlFromCurrentRoute($this);

		/* put extended properties in form */
		if ( $dao->extended ) {
			$form->setExtended($dao->extended);
		}
		$form->bind($document);
		$form->setMode('edit');

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($document);
				return $this->successForward();
			}
		}

		$view->pageTitle = tra(sprintf('Edit Document %s', $document->getName()));
		$view->form = $form;
		$view->containerid = $containerId;
		$view->spacename = $spacename;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function createAction()
	{
		$spacename = $this->params()->fromRoute('spacename');

		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
			$cancel = false;
			$containerId = $request->getQuery('containerid', $this->containerId);
		}
		if ( $request->isPost() ) {
			$containerId = $request->getPost('parentId', $this->containerId);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);

			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* init some helpers */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Document\Version::$classId);

		/* Check permissions */
		$container = $factory->getDao(\Rbplm\Org\Workitem::$classId)->loadFromId($factory->getModel(\Rbplm\Org\Workitem::$classId), $containerId);
		$this->getAcl()->checkRightFromUid('create', $container->getUid());

		/* Load extended properties */
		$loader = new Extended\Loader($factory);
		$loader->loadFromContainerId($containerId, $dao);

		/* Init document */
		$document = Document\Version::init();

		/* Construct the form */
		$form = new \Ged\Form\Document\EditForm($factory);
		$form->selectCategory($containerId);
		$form->setUrlFromCurrentRoute($this);
		/* put extended properties in form */
		if ( $dao->extended ) {
			$form->setExtended($dao->extended);
		}
		$form->bind($document);
		$form->setMode('create');

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($document);
				return $this->successForward();
			}
		}

		$view->pageTitle = tra('Create New Document');
		$view->form = $form;
		$view->containerid = $containerId;
		$view->spacename = $spacename;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 * 
	 * FOR HOMOGENIOUS SPACENAME ONLY
	 * 
	 * @throws \Exception
	 * @return \Zend\Mvc\Controller\Plugin\Redirect
	 */
	public function deleteAction()
	{
		$spacename = $this->params()->fromRoute('spacename', $this->spacename);

		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$documentIds = $request->getQuery('checked', []);
			$spacenames = $request->getQuery('spacenames', []);
			$validate = false;
		}
		if ( $request->isPost() ) {
			$documentIds = $request->getPost('checked', []);
			$spacenames = $request->getPost('spacenames', []);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* display form */
		if ( !$validate ) {
			$factory = DaoFactory::get($spacename);
			/** @var \Rbs\Ged\Document\VersionDao $dao */
			$dao = $factory->getDao(Document\Version::$classId);

			$form = new \Ged\Form\Document\ConfirmForm();
			$form->setUrlFromCurrentRoute($this);

			foreach( $documentIds as $documentId ) {
				/* Unselect document with other spacename */
				if ( isset($spacenames[$documentId]) && $spacenames[$documentId] != $spacename ) {
					$sn = $spacenames[$documentId];
					$this->errorStack()->log(sprintf('document id %s has a spacename %s not conform with spacename %s of current selection. Document is ignored.', $documentId, $sn, $spacename));
					unset($documentIds[$documentId]);
					continue;
				}

				$document = new Document\Version();
				$dao->loadFromId($document, $documentId);

				/* Check permissions */
				$this->getAcl()->checkRightFromUid('delete', $document->parentUid);

				$form->addDocument($document);
			}

			$view->pageTitle = "Suppression Confirmation";
			$view->warning = "Are you sure that you want suppress this documents?";
			$view->help = "After deletion, documents files are placed in trash. In case of error, you can see your admin system to get back trashed files";
			$view->setTemplate($form->template);
			$view->form = $form;
			return $view;
		}
		/* validate */
		else {
			$i = 1;
			$documents = [];
			foreach( $documentIds as $documentId ) {
				if ( !isset($spacenames[$documentId]) ) {
					throw new \Exception('spacename is not set for document ' . $documentId);
				}
				$spacename = $spacenames[$documentId];
				$documents['document' . $i] = array(
					'id' => $documentId,
					'spacename' => $spacename
				);
				$i++;
			}
			try {
				$request->setPost(new Parameters([
					'documents' => $documents
				]));
				$service = new \Service\Controller\Document\ManagerService();
				$this->bindServiceRespons($service->deleteService($request));
				return $this->successForward();
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				$this->errorForward();
			}
		}
	}

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function excelAction()
	{
		$view = $this->view;
		$containerId = $this->params()->fromRoute('id');
		$spacename = $this->params()->fromRoute('spacename');

		/* Check permissions */
		$this->getAcl()->checkRightFromIdAndCid('read', $containerId, \Rbplm\Org\Workitem::$classId);

		$factory = DaoFactory::get($spacename);
		$filter = new Filter('', false);

		$list = $this->getList($filter, $factory);
		$dao = $list->dao;
		$filter->sort($dao->toSys('uid'), 'ASC');
		$filter->andfind($containerId, $dao->toSys('parentId'), Op::EQUAL);
		$list->load($filter);

		$view->setTemplate('ged/document/manager/excel');
		$view->list = $list;
		$view->title = $spacename;
		return $view;
	}

	/**
	 */
	public function resetdoctypeAction()
	{
		$request = $this->getRequest();

		if ( $request->isGet() ) {
			$spacenames = $request->getQuery('spacenames', null);
			$documentIds = $request->getQuery('checked', []);

			$documentId = $request->getQuery('id', null);
			$spacename = $request->getQuery('spacename', null);

			($documentId) ? $documentIds[] = $documentId : null;
			($documentId && $spacename) ? $spacenames[$documentId] = $spacename : null;

			unset($documentId, $spacename);
		}

		$input = [];
		foreach( $documentIds as $documentId ) {
			/* set spacename */
			isset($spacenames[$documentId]) ? $spacename = $spacenames[$documentId] : $spacename = false;
			if ( !$spacename ) {
				throw (new \Exception('spacename is not set for document ' . $documentId));
			}

			$input['documents'][$documentId] = array(
				'id' => $documentId,
				'spacename' => $spacename
			);
		}

		try {
			$service = new \Service\Controller\Document\DoctypeService();
			$service->resetService($input);
		}
		catch( \Exception $e ) {
			$this->errorStack()->error($e->getMessage());
		}

		return $this->successForward();
	}
}
