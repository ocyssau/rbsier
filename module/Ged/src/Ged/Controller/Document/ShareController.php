<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;

/**
 */
class ShareController extends AbstractController
{

	/* */
	public $pageId = 'document_share';

	/* */
	public $defaultSuccessForward = 'ged/document/share';

	/* */
	public $defaultFailedForward = 'ged/document/share';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * @return string|\Application\View\ViewModel
	 */
	public function indexAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$documentIds = $request->getQuery('checked', null);
			$spacenames = $request->getQuery('spacenames', null);

			$documentId = $request->getQuery('id', null);
			$spacename = $request->getQuery('spacename', null);

			(!$documentId) ? $documentId = current($documentIds) : null;
			(!$spacename) ? $spacename = $spacenames[$documentId] : null;

			unset($documentIds, $spacenames);
		}
		else {
			$spacename = $request->getPost('spacename', null);
			$documentId = $request->getPost('id', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* Load document */
		$factory = DaoFactory::get($spacename);

		$document = new Document\Version();
		$document->dao = $factory->getDao(Document\Version::$classId);
		$document->dao->loadFromId($document, $documentId);

		/* */
		$view->setTemplate('ged/document/share/index');

		/* Base URI */
		$baseUri = $request->getUri()->getScheme() . '://' . $request->getUri()->getHost() . '/';

		/* Public URI */
		$publicBaseUri = trim($this->url()->fromRoute('ged-document-publiclink'), '/');

		/* Permanent link */
		$permanentUri = trim($this->url()->fromRoute('ged-document-permanentlink', array(
			'documentId' => $documentId,
			'spacename' => $spacename
		)), '/');

		/* Load share */
		$publicShare = new \Rbs\Ged\Document\Share\PublicUrl();
		try {
			$factory->getDao($publicShare->cid)->loadFromDocumentIdAndSpacename($documentId, $spacename, $publicShare);
			$publicShare->setUrl($baseUri . $publicBaseUri . '/' . $publicShare->getUid());
			$document->publicShare = $publicShare;
			$document->isShared = true;
		}
		catch( \Rbplm\Dao\NotExistingException $e ) {
			$document->isShared = false;
			$publicShare->newUid();
		}

		/* Construct the form */
		$form = new \Ged\Form\Document\Share\ShareForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($document);

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

		/* send to view */
		$view->form = $form;
		$view->document = $document;
		$view->spacename = $spacename;
		$view->baseUri = $baseUri;
		$view->permanentUri = $baseUri . $permanentUri;
		$view->publicBaseUri = $baseUri . $publicBaseUri;
		$view->pageTitle = sprintf('Share');
		return $view;
	}
}
