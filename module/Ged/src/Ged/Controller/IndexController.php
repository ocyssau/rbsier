<?php
namespace Ged\Controller;

use Application\Controller\AbstractController;

class IndexController extends AbstractController
{

	protected $pageId = 'ged_index';

	//(string)
	protected $ifSuccessForward = 'ged/document/index/index';

	protected $ifFailedForward = 'ged/document/index/index';

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		return $view;
	}
}

