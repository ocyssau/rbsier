<?php
namespace Ged\Controller;

use Application\Controller\AbstractController;

/**
 */
class SessionController extends AbstractController
{

	/**
	 */
	public function activateAction()
	{
		$containerId = null;
		$request = $this->getRequest();
		
		$context = \Ranchbe::get()->getServiceManager()->getContext();
		$session = \Ranchbe::get()->getServiceManager()->getSessionManager()->getStorage();
		$session->displayDocfile = false;

		/* */
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename', $this->params()
				->fromRoute('spacename'));
			$containerId = $request->getQuery('id', $this->params()
				->fromRoute('id'));
		}

		/* */
		if ( $spacename ) {
			$this->container = $context->activate($containerId, $spacename);
			$this->spacename = $spacename;
			if($context->isSaved() == false){
				$context->getDao()->save($context);
			}
		}

		if ( isset($this->container->fileOnly) && $this->container->fileOnly == 1 ) {
			return $this->redirectTo('recordfile/manager/display', [
				'spacename' => $spacename,
				'containerid' => $containerId
			]);
		}
		else {
			return $this->redirect()->toRoute('ged-document-manager', [
				'spacename' => $spacename,
				'id' => $containerId
			]);
		}
	}

	/**
	 * Redirect to the current activate container manager. Current container is the container set in session.
	 * @return \Zend\Http\Response
	 */
	public function currentAction()
	{
		$context = \Ranchbe::get()->getServiceManager()->getContext();
		$containerId = $context->getData('containerId');
		$spacename = $context->getData('spacename');

		if ( !$containerId ) {
			return $this->redirectTo('home', array());
		}
		if ( !$spacename ) {
			return $this->redirectTo('home', array());
		}

		/**/
		$factory = \Rbs\Space\Factory::get($spacename);
		$container = $factory->getModel(\Rbplm\Org\Workitem::$classId);
		$contDao = $factory->getDao($container->cid);
		$contDao->loadFromId($container, $containerId);

		/**/
		if ( isset($container->fileOnly) && $container->fileOnly == 1 ) {
			return $this->redirectTo('recordfile/manager/display', array(
				'spacename' => $spacename,
				'containerid' => $containerId
			));
		}
		else {
			return $this->redirect()->toRoute('ged-document-manager', array(
				'spacename' => $spacename,
				'id' => $containerId
			));
		}
	}
} /* End of class */
