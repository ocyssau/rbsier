<?php
namespace Ged\Controller\Docfile;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Document;

/**
 * 
 *
 */
class DetailController extends AbstractController
{

	/**
	 * @var string
	 */
	public $pageId = 'docfile_detail';

	/** @var string */
	public $defaultSuccessForward = 'ged/document/detail';

	/** @var string */
	public $defaultFailedForward = 'ged/document/detail';

	/**
	 */
	public function init()
	{
		parent::init();

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/docfile/detail/index');
		$spacename = $this->params()->fromRoute('spacename', null);
		$docfileId = $this->params()->fromRoute('id', null);
		$request = $this->getRequest();

		/* save this page in basecamp to return here after actions */
		$currentUri = $request->getUri()->getPath();
		$this->basecamp()->save($currentUri, $currentUri, $view);

		/* init some helpers */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Docfile\Version::$classId);

		/* @var \Rbplm\Ged\Docfile\Version $docfile */
		try {
			$docfile = $dao->loadFromId(new Docfile\Version(), $docfileId);
			if ( $docfile->parentId ) {
				$document = new Document\Version();
				$document->dao = $factory->getDao(Document\Version::$classId);
				$document->dao->loadFromId($document, $docfile->parentId);
				$docfile->setParent($document);
			}
		}
		catch( \PDOException $e ) {
			if ( substr($e->getMessage(), 0, 15) == 'SQLSTATE[42S02]' ) {
				/* context is probably no setted */
				return $this->redirect()->toRoute('home');
			}
			else {
				throw $e;
			}
		}

		//$url = sprintf('/ged/document/detail/%s/%s', $spacename, $docfile->parentId).'#docfiles';
		//return $this->redirectTo($url);
		$view->docfile = $docfile;
		$view->spacename = $spacename;
		return $view;
	}
} /* End of class */

