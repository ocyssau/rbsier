<?php
namespace Ged\Controller\Docfile;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Document;
use Rbplm\People\CurrentUser;
use Rbs\Ged\Document\Event;

/**
 */
class VersionController extends AbstractController
{

	/** @var string */
	public $pageId = 'docfile_version';

	/** @var string */
	public $defaultSuccessForward = 'ged/docfile/manager#docfiles';

	/** @var string */
	public $defaultFailedForward = 'ged/docfile/manager#docfiles';

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$this->layout()->tabs = $tabs;
		$this->layoutSelector()->clear($this);

		/**/
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
	}

	/**
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 */
	public function replaceAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/docfile/version/replace');
		$request = $this->getRequest();

		$spacename = $this->params()->fromRoute('spacename', null);
		$fileId = $this->params()->fromRoute('id', null);

		if ( $request->isGet() ) {
			$validate = false;
			$cancel = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		$factory = DaoFactory::get($spacename);

		/* @var \Rbs\Ged\Docfile\VersionDao $dao */
		$dao = $factory->getDao(Docfile\Version::$classId);

		/** @var Docfile\Iteration $docfile */
		$docfile = $dao->loadFromId(new Docfile\Version(), $fileId);
		$docfile->spacename = $spacename;
		$currentUser = CurrentUser::get();

		/* Load parent document */
		$document = new Document\Version();
		$document->dao = $factory->getDao(Document\Version::$classId);
		$document->dao->loadFromId($document, $docfile->parentId);
		$docfile->setParent($document);

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('edit', $document->getParentUid());

		/* init form */
		$form = new \Ged\Form\Docfile\ReplaceForm($factory, $view);
		$form->setAttribute('action', $request->getUriString());
		$form->bind($docfile);

		if ( $validate ) {
			$form->setData($request->getPost());
			$form->setData($_FILES);
			if ( $form->isValid() ) {
				$event = new Event(Event::PRE_REPLACEFILE, $document, $factory);
				$this->getRbEventManager()->trigger($event);

				$d = $form->get('uploadFile')->getValue();
				$fileName = $d['name'];
				$tmpFile = $d['tmp_name'];

				/* set comment for history */
				$comment = sprintf('replace file %s by %s', $docfile->getName(), $fileName);

				/* check if uploaded file name is existing in db
				 = a file with same name that original, can not be used */
				try {
					$df2 = $dao->loadFromName(new Docfile\Iteration(), $fileName);
					$e = new \Rbplm\Dao\ExistingException('File is existing', 7000);
					$e->datas = $df2;
					throw $e;
				}
				catch( \Rbplm\Dao\NotExistingException $e ) {
					null;
				}

				/* Record uploaded file in vaut 
				 and attach new record to existing docfile, update his datas */
				/* @var \Rbplm\Vault\Record $record */
				$record = $docfile->getData();
				/* @var \Rbplm\Sys\Fsdata $fsData */
				$fsdata = new \Rbplm\Sys\Fsdata($tmpFile);
				$reposit = \Rbplm\Vault\Reposit::init($docfile->getData()->path);
				$vault = new \Rbs\Vault\Vault($reposit, $factory->getDao(\Rbplm\Vault\Record::$classId));
				$vault->record($record, $fsdata, $fileName);
				$docfile->setData($record);
				$docfile->setUpdateBy($currentUser);
				$docfile->setUpdated(new \DateTime());
				$docfile->iteration = $docfile->iteration + 1;
				$docfile->dao->save($docfile);

				/* Original vault data must be keep? or deleted? */

				$event = new Event(Event::POST_REPLACEFILE, $document, $factory);
				$event->setComment($comment);
				$this->getRbEventManager()->trigger($event);

				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Replace file "%s"'), $docfile->getName());
		$view->form = $form;
		return $view;
	}
}
