<?php
namespace Ged\Controller\Docfile;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Document;

/**
 */
class IterationController extends AbstractController
{

	/** @var string */
	public $pageId = 'docfile_iteration';

	/** @var string */
	public $defaultSuccessForward = 'ged/docfile/manager';

	/** @var string */
	public $defaultFailedForward = 'ged/docfile/manager';

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$this->layout()->tabs = $tabs;
		$this->layoutSelector()->clear($this);

		/**/
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
	}

	/**
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/docfile/iteration/index');
		$request = $this->getRequest();

		$spacename = $this->params()->fromRoute('spacename', null);
		$fileId = $this->params()->fromRoute('id', null);

		if ( $request->isGet() ) {
			$orderBy = $request->getQuery('orderby', 'name');
			$order = $request->getQuery('order', 'asc');
		}

		$factory = DaoFactory::get($spacename);
		$list = $factory->getList(Docfile\Iteration::$classId);
		/* @var \Rbs\Dao\Sier\MetamodelTranslator $trans */
		$trans = $factory->getDao(Docfile\Iteration::$classId)->getTranslator();
		
		try {
			$docfile = new Docfile\Version();
			$factory->getDao($docfile->cid)->loadFromId($docfile, $fileId);

			/* Check permissions */
			//$this->getAcl()->checkRightFromUid('read', $docfile->getParentUid());

			/* get all file versions */
			$select = $trans->getSelectAsApp();
			$select[] = "'$spacename' as spacename";
			
			$list->select($select);
			$sqlOrder = "ORDER BY $orderBy $order";
			$list->load($trans->toSys('ofDocfileId') . '=:parentId ' . $sqlOrder, [
				':parentId' => $fileId
			]);
			$view->list = $list->toArray();
		}
		catch( \Throwable $e ) {
			$this->errorStack()->error($e->getMessage());
			$view->list = [];
		}

		$view->fileid = $fileId;
		$view->spacename = $spacename;
		$view->basecamp = '';
		$view->orderby = $orderBy;
		$view->order = $order;
		$view->pageTitle = 'Iterations Of ' . $docfile->getName() . ' (#' . $docfile->getId() . ')';
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename', null);
			$checked = $request->getQuery('checked', array());
			$fileId = $request->getQuery('fileid', null);
			if ( $fileId ) $checked[] = $fileId;
		}

		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Docfile\Iteration::$classId);
		$docDao = $factory->getDao(Document\Version::$classId);
		$service = new \Rbs\Ged\Docfile\Iteration\Service($factory);

		foreach( $checked as $fileId ) {
			$docfile = $dao->loadFromId(new Docfile\Iteration(), $fileId);

			/* Load parent document */
			$document = new Document\Version();
			$docDao->loadFromId($document, $docfile->parentId);

			/* Check permissions */
			$this->getAcl()->checkRightFromUid('delete', $document->getParentUid());

			/* */
			$service->delete($docfile, true);
		}

		return $this->redirect($this->ifSuccessForward, array(
			'spacename' => $this->spacename,
			'fileid' => $fileId
		));
	}
}
