<?php
namespace Ged\Controller\Docfile;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Dao\Filter\Op;

/**
 */
class ManagerController extends AbstractController
{

	/** @var string */
	public $pageId = 'docfile_manager';

	/** @var string */
	public $defaultSuccessForward = '/home';

	/** @var string */
	public $defaultFailedForward = '/home';

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		$this->context = \Ranchbe::get()->getServiceManager()->getContext();
		$containerId = $this->context->getData('containerId');
		$spacename = $this->context->getData('spacename');
		
		$url = $this->url()->fromRoute('ged-docfile-manager', [
			'spacename' => $spacename,
			'id' => $containerId,
			'action' => 'index'
		]);
		
		$this->defaultSuccessForward = $url;
		$this->defaultFailedForward = $url;
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		
		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		$this->basecamp()->setForward($this);

		/**/
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax($view = null, $errorStack = null)
	{
		parent::initAjax($view, $errorStack);
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
		$this->context = \Ranchbe::get()->getServiceManager()->getContext();
	}

	/**
	 *
	 */
	public function hideAction()
	{
		$containerId = $this->context->getData('containerId');
		$spacename = $this->context->getData('spacename');
		
		$session = \Ranchbe::get()->getServiceManager()->getSessionManager()->getStorage();
		$session->displayDocfile = false;
		
		if ( $spacename && $containerId ) {
			$url = $this->url()->fromRoute('ged-document-manager', [
				'spacename' => $spacename,
				'id' => $containerId,
				'action' => 'index'
			]);
		}
		else {
			$url = $this->url()->fromRoute('home');
		}
		
		return $this->redirect()->toUrl($url);
	}

	/**
	 *
	 * @param \Rbs\Dao\Sier\DaoList $list
	 * @param string $template
	 * @return \Application\View\ViewModel
	 */
	public function indexAction($list = null, $template = null)
	{
		$view = $this->view;
		$request = $this->getRequest();

		$containerId = $this->params()->fromRoute('id');
		$spacename = $this->params()->fromRoute('spacename');

		if ( $request->isGet() == false ) {
			throw new \Exception(__NAMESPACE__ . '\\' . __FUNCTION__ . ' accept HTTP GET request only');
		}

		($template == null) ? $template = 'ged/docfile/manager/index' : null;
		$view->setTemplate($template);

		$this->layoutSelector()->clear($this);
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Load container */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
		$container = $dao->loadFromId($factory->getModel(\Rbplm\Org\Workitem::$classId), $containerId);

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('read', $container->getUid());

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->setAlias('df');
		$filterForm = new \Ged\Form\Docfile\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $this->getList($filter, $factory, $containerId);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->load(['default'=>[
			'orderby'=>'number',
			'order'=>'asc',
			'limit' => 50
		]]);
		$paginator->setMaxLimit($list->countAll);
		$paginator->bindToFilter($filter)
			->bindToView($view)
			->save();

		$list->load($filter, $filterForm->bind);
		$view->list = $list;
		$view->filter = $filterForm;

		$view->pageTitle = sprintf(tra('Docfiles Manager Of %s'), $container->getNumber());
		$view->containerid = $containerId;
		$view->spacename = $spacename;
		return $view;
	}

	/**
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param \Rbs\Space\Factory $factory
	 * @param \Ged\\Form\Docfile\FilterForm $filterForm
	 * @return \Rbs\Dao\Sier\DaoList
	 */
	public function getList($filter, $factory, $containerId = null)
	{
		$dao = $factory->getDao(Docfile\Version::$classId);
		$trans = $factory->getDao(Document\Version::$classId)->getTranslator();
		$list = $factory->getList(Docfile\Version::$classId);
		$filter->with([
			'table' => $factory->getTable(Document\Version::$classId),
			'lefton' => 'document_id',
			'righton' => 'id',
			'alias' => 'document'
		]);
		$filter->setAlias('df');

		$select = $dao->getTranslator()->getSelectAsApp('df');
		$select[] = $factory->getConnexion()->quote(strtolower($factory->getName())) . ' as spacename';

		$parentIdAsSys = $trans->toSys('parentId');
		$select[] = "document.$parentIdAsSys AS containerId";
		$select[] = 'document.' . $trans->toSys('number') . ' AS parentNumber';
		$select[] = 'document.' . $trans->toSys('version') . ' AS parentVersion';
		$select[] = 'document.' . $trans->toSys('iteration') . ' as parentIteration';
		$filter->select($select);

		if ( $containerId ) {
			$filter->andfind($containerId, 'document.' . $parentIdAsSys, Op::OP_EQUAL);
		}

		return $list;
	}

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function getfatherdocAction()
	{
		$view = $this->view;
		return $view;
	}

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function excelAction()
	{
		$view = $this->view;
		$containerId = $this->params()->fromRoute('id');
		$spacename = $this->params()->fromRoute('spacename');

		/* Check permissions */
		$this->getAcl()->checkRightFromIdAndCid('read', $containerId, \Rbplm\Org\Workitem::$classId);

		$factory = DaoFactory::get($spacename);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		$list = $this->getList($filter, $factory, $containerId);
		$dao = $list->dao;
		$filter->sort($dao->toSys('name'), 'ASC');
		$list->load($filter);

		$view->setTemplate('ged/docfile/manager/excel');
		$view->list = $list;
		$view->title = $spacename;
		return $view;
	}
}

