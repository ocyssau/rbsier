<?php
namespace Ged\Controller\Metadata;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Extended\Property;
use Rbplm\Dao\Filter\Op;
use Application\Controller\ControllerException;

/**
 */
class IndexController extends AbstractController
{

	/** @var string */
	public $pageId = 'metadata_manager';

	/** @var string */
	public $defaultSuccessForward = 'ged/metadata/index';

	/** @var string */
	public $defaultFailedForward = 'ged/metadata/index';

	/**
	 *
	 * @var string
	 */
	protected $extendedCid;

	/**
	 */
	public function init()
	{
		parent::init();

		$spacename = $this->params()->fromRoute('spacename');

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('spaces')->activate($spacename);
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Ged::$appCn;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Ged::$appCn;
	}

	/**
	 */
	public function indexAction()
	{
		$request = $this->getRequest();
		if ( !$request->isGet() ) {
			throw new ControllerException('Accept only HTTP GET mothod');
		}

		$spacename = $this->params()->fromRoute('spacename');
		$extendedCid = $request->getQuery('class');

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$view = $this->view;
		$view->setTemplate('ged/metadata/index/index');
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Property::$classId);
		$extendedCid = $this->extendedCid;

		$this->spacename = $spacename;
		$this->class = $extendedCid;

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Ged\Form\Metadata\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Property::$classId);
		$select = [];
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);

		if ( $extendedCid ) {
			$filter->andfind(':extendedCid', $dao->toSys('extendedCid'), Op::EQUAL);
		}

		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Metadata Manager';
		$view->spacename = $spacename;
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$spacename = $this->params()->fromRoute('spacename');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$propertyIds = $request->getQuery('checked', []);
		}
		elseif ( $request->isPost() ) {
			$propertyIds = $request->getPost('checked', []);
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* init dao */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Property::$classId);

		foreach( $propertyIds as $propertyId ) {
			try {
				$dao->deleteFromId($propertyId);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
			}
		}

		return $this->redirect()->toRoute('ged-metadata', [
			'spacename' => $spacename
		]);
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$spacename = $this->params()->fromRoute('spacename');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$propertyIds = $request->getQuery('checked', array());
			$propertyId = $propertyIds[0];
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$propertyId = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->redirect()->toRoute('ged-metadata', [
					'spacename' => $spacename
				]);
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		$property = new Property();
		$property->spacename = $spacename;
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Property::$classId);
		$dao->loadFromId($property, $propertyId);

		$form = new \Ged\Form\Metadata\EditForm($view);
		$form->setUrlFromCurrentRoute($this);
		$form->remove('extendedCid')
			->getInputFilter()
			->remove('extendedCid')
			->remove('type');
		$form->get('type')->setAttribute('disabled', true);
		$form->bind($property);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$dao->update($property);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect()->toRoute('ged-metadata', [
					'spacename' => $spacename
				]);
			}
		}

		$view->pageTitle = 'Edit Property ' . $property->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$spacename = $this->params()->fromRoute('spacename');
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$extendedCid = $request->getQuery('extendedCid', null);
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$extendedCid = $request->getPost('extendedCid', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->redirect()->toRoute('ged-metadata', [
					'spacename' => $spacename
				]);
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* Set default values */
		$property = Property::init();
		$property->setAppName('')->newUid();
		$property->setExtended($extendedCid);
		$property->setType('text');
		$property->description = '';
		$property->label = '';
		$property->multiple = 0;
		$property->return = 0;
		$property->hide = 0;
		$property->attributes = [];
		$property->min = -1;
		$property->max = -1;
		$property->step = 1;
		$property->spacename = $spacename;

		/* helpers */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Property::$classId);

		/* init form */
		$form = new \Ged\Form\Metadata\EditForm($view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($property);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$property->setAppName($property->getAppName());
					$extendTable = $factory->getTable($extendedCid);
					$dao->setExtendedTable($extendTable);
					$dao->insert($property);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect()->toRoute('ged-metadata', [
					'spacename' => $spacename
				]);
			}
		}

		$view->pageTitle = 'Create Property';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}
} /* End of class */
