<?php
namespace Ged\Controller;

use Application\Controller\AbstractController;
use Rbs\Notification;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\People;

/**
 * 
 * @author ocyssau
 *
 */
abstract class AbstractNotificationController extends AbstractController
{

	/** @var \Rbplm\Ged\Document\Version */
	protected $reference;

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		$factory = DaoFactory::get();
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$dao = $factory->getDao(Notification\Notification::$classId);
		$list = $factory->getList(Notification\Notification::$classId);
		$select = [];
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$list->load($filter);
		$view->list = $list;

		$view->PageTitle = tra('My notifications');
		return $this->view;
	}

	/**
	 * 
	 * @return \Application\View\ViewModel
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$referenceIds = $request->getQuery('checked', []);
			$spacenames = $request->getQuery('spacenames', []);

			$referenceId = $request->getQuery('referenceId', null);
			$spacename = $request->getQuery('spacename', null);

			($referenceId) ? $referenceIds[] = $referenceId : null;
			($referenceId && $spacename) ? $spacenames[$referenceId] = $spacename : null;

			$referenceUid = null;
			$referenceId = null;
			$spacename = null;
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$spacenames = $request->getPost('spacenames', null);
			$referenceIds = [];
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		/* set helpers */
		$owner = People\CurrentUser::get();

		$form = new \Ged\Form\Notification\EditForm($view, $this->_getEvents());
		$form->setUrlFromCurrentRoute($this);
		$collection = new \Ged\Form\Notification\Collection();

		foreach( $referenceIds as $referenceId ) {
			$spacename = $spacenames[$referenceId];

			/* set helper */
			$factory = DaoFactory::get($spacename);
			$dao = $factory->getDao(Notification\Notification::$classId);

			$reference = $this->_newReference();
			$factory->getDao($reference->cid)->loadFromId($reference, $referenceId);
			$referenceUid = $reference->getUid();

			/* try to load the existing notification. if failed, get in creation mode */
			try {
				$notification = new Notification\Notification();
				$dao->loadFromReferenceUidAndOwnerUid($notification, $referenceUid, $owner->getLogin());
				$notification->dao = $dao;
				$view->pageTitle = 'Edit Notification ' . $notification->getName();
				$notification->hydrate(array(
					'reference' => $reference
				));
			}
			catch( \Rbplm\Dao\NotExistingException $e ) {
				$notification = Notification\Notification::init();
				$notification->setName($referenceUid . '-' . $owner->getUid());
				$notification->setReference($reference);
				$notification->spacename = $spacename;
				$notification->setOwner($owner);
				$notification->dao = $dao;
				$view->pageTitle = 'Create Notification';
			}

			$collection->add($notification);
		}

		$form->bind($collection);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				foreach( $collection->getIterator() as $notification ) {
					$notification->setOwner($owner);
					$dao = DaoFactory::get($notification->spacename)->getDao($notification::$classId);
					$dao->save($notification);
				}
				return $this->successForward();
			}
		}

		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 * @return array
	 */
	abstract protected function _getEvents();

	/**
	 * 
	 * @return \Rbplm\Ged\Document\Version
	 */
	abstract protected function _newReference();
}
