<?php
namespace Ged\Controller\Doctype;

use Application\Controller\AbstractController;
use Rbs\Observers\DoctypeCallbackFactory as Callback;
use Rbs\Space\Factory as DaoFactory;

/**
 */
class CallbackController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'doctype_callback';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'ged/doctype/callback/index';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'ged/doctype/index';

	/**
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('doctype');
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Ged::$appCn;
	}

	/**
	 */
	public function indexAction()
	{
		$request = $this->getRequest();
		$view = $this->view;
		$view->setTemplate('ged/doctype/callback/index/index');
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);
		$doctypeId = $this->params()->fromRoute('doctypeId');

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Callback::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Admin\Form\Callback\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'callbackfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Callback::$classId);
		$select = [];
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$filter->andfind($doctypeId, $dao->toSys('doctype_id'));
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);

		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Callback Manager';
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		$doctypeId = $this->params()->fromRoute('doctypeId');

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$callback = Callback::init();
		$callback->doctypeId = $doctypeId;
		$factory = DaoFactory::get();
		$dao = $factory->getDao($callback->cid);

		$form = new \Ged\Form\Doctype\Callback\EditForm($view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($callback);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($callback);
				return $this->successForward();
			}
		}

		$view->pageTitle = tra('Create Callback');
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$callbackIds = $request->getQuery('checked', []);
			$callbackId = $callbackIds[0];
			$validate = false;
		}
		if ( $request->isPost() ) {
			$callbackId = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$callback = new Callback();
		//$callback->doctypeId = $doctypeId;
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Callback::$classId);
		$dao->loadFromId($callback, $callbackId);

		$form = new \Ged\Form\Doctype\Callback\EditForm($view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($callback);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($callback);
				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Edit Callback %s'), $callback->getName());
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$callbackIds = $request->getQuery('checked', []);
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Callback::$classId);

		foreach( $callbackIds as $callbackId ) {
			try {
				$dao->deleteFromId($callbackId);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $callbackId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}
}
