<?php
namespace Ged\Controller\Doctype;

use Application\Controller\AbstractController;
use Rbplm\Ged\Doctype;
use Rbs\Space\Factory as DaoFactory;

/**
 */
class IndexController extends AbstractController
{

	/** @var string */
	public $pageId = 'doctype_manage';

	/** @var string */
	public $defaultSuccessForward = 'ged/doctype/index';

	/** @var string */
	public $defaultFailedForward = 'ged/doctype/index';

	/**
	 */
	public function init()
	{
		parent::init();
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('doctype');
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Ged::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Ged::$appCn;
	}

	/**
	 */
	public function indexAction()
	{
		$request = $this->getRequest();
		$view = $this->view;
		$view->setTemplate('ged/doctype/index/index');
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Doctype::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Ged\Form\Doctype\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Doctype::$classId);
		$select = [];
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->orderby = 'name';
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);

		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Doctypes Manager';
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$doctypeIds = $request->getQuery('checked', []);
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Doctype::$classId);

		foreach( $doctypeIds as $doctypeId ) {
			try {
				$dao->deleteFromId($doctypeId);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $doctypeId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$doctypeIds = $request->getQuery('checked', []);
			$doctypeId = $doctypeIds[0];
			$validate = false;
		}
		if ( $request->isPost() ) {
			$doctypeId = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$doctype = new Doctype();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Doctype::$classId);
		$dao->loadFromId($doctype, $doctypeId);

		$form = new \Ged\Form\Doctype\EditForm($view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($doctype);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($doctype);
				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Edit Doctype %s'), $doctype->getName());
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$doctype = Doctype::init()->setName('')->setNumber(uniqid());
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Doctype::$classId);

		$form = new \Ged\Form\Doctype\EditForm($view);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($doctype);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($doctype);
				return $this->successForward();
			}
		}

		$view->pageTitle = tra('Create Doctype');
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function csvimport()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$this->doctype->ImportDoctypesCsv($_FILES['csvlist']['tmp_name'], $_REQUEST['overwrite']);
		return $this->displayAction();
	}

	/**
	 * 
	 */
	public function buildiconsAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$factory = DaoFactory::get();
		$list = $factory->getList(Doctype::$classId);
		$list->load();
		$translator = $factory->getTranslator($list->dao);

		/**/
		$ranchbe = \Ranchbe::get();
		$sourcePath = $ranchbe->getConfig('icons.doctype.source.path');
		$compiledPath = $ranchbe->getConfig('icons.doctype.compiled.path');

		/**/
		foreach( $list as $entry ) {
			try {
				$doctype = $translator->toApp($entry);
				$srcIconFile = $sourcePath . '/' . $doctype['icon'];
				self::compileIcon($srcIconFile, $compiledPath, $doctype['id']);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
			}
		}

		$this->errorStack()->success('Compilation success');
		return $this->successForward();
	}

	/**
	 * Create a icon file in "C" dir for icon to associate to Doctype.
	 * The copy in C is renamed with the id of the doctype.
	 *
	 * @param string $iconPath	Source icon file path.
	 * @param string $iconUid	Uid or any other uniq identifier for icon.
	 * @return string Path to compiled icon file or false
	 * @throws \Rbplm\Ged\Exception
	 */
	public static function compileIcon($iconPath, $compiledIconDir, $iconUid)
	{
		if ( !is_file($iconPath) ) {
			throw new \Rbplm\Ged\Exception(sprintf('INVALID SOURCE %s', $iconPath));
		}

		if ( filesize($iconPath) > 10240 ) {
			throw new \Rbplm\Ged\Exception(sprintf('INVALID_DATA_SIZE FOR %s %s', $iconPath, '10240bits max'));
		}

		$pathParts = pathinfo($iconPath);
		if ( !is_dir($compiledIconDir) ) {
			if ( !@mkdir($compiledIconDir, 0755) ) {
				throw new \Rbplm\Ged\Exception(sprintf('UNABLE_TO_CREATE_DIR %s', $compiledIconDir));
			}
		}

		$compiledIconFile = $compiledIconDir . '/' . $iconUid . '.' . $pathParts['extension'];
		if ( copy($iconPath, $compiledIconFile) ) {
			return $compiledIconFile;
		}
		else {
			throw new \Rbplm\Ged\Exception(sprintf('COPY_ERROR FROM %s TO %s', $iconPath, $compiledIconFile));
		}
	}

	/**
	 *
	 */
	public function recalculateallAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		$factory = DaoFactory::get('workitem');
		$list = $factory->getList(\Rbplm\Ged\Document\Version::$classId);
		$list->load();
		$service = new \Service\Controller\Document\DoctypeService();
		/**/
		foreach( $list as $docEntry ) {
			try {
				$in = [
					'documents' => [
						'document1' => [
							'id' => $docEntry['id'],
							'spacename' => 'workitem'
						]
					]
				];
				$service->resetService($in);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
			}
		}

		$this->errorStack()->success('Completed');
		return $this->successForward();
	}
} /* End of class */
