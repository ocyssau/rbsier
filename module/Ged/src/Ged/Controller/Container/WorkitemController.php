<?php
namespace Ged\Controller\Container;

/**
 */
class WorkitemController extends ManagerController
{

	/** @var string */
	public $pageId = 'container_workitem';

	/** @var string */
	public $defaultSuccessForward = 'ged/workitem/index';

	/** @var string */
	public $defaultFailedForward = 'ged/workitem/index';

	/** @var string */
	public $spacename = 'workitem';

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 * @return \Ged\Form\Container\Workitem\EditForm
	 */
	protected function getEditForm($factory)
	{
		if ( !$this->editForm ) {
			$this->editForm = new \Ged\Form\Container\Workitem\EditForm($factory);
		}
		return $this->editForm;
	}
}
