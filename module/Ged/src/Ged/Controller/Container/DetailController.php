<?php
namespace Ged\Controller\Container;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Extended;
use Rbplm\Org\Workitem as Container;
use Discussion\Model\Comment;
use Rbplm\Dao\Filter\Op;

/**
 */
class DetailController extends AbstractController
{

	/** @var string */
	public $pageId = 'container_detail';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		$this->defaultSuccessForward = $this->getRequest()
			->getUri()
			->getPath();
		$this->defaultFailedForward = $this->defaultSuccessForward;
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax($view = null, $errorStack = null)
	{
		parent::initAjax($view, $errorStack);
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/container/detail/index.phtml');

		$spacename = $this->params()->fromRoute('spacename', null);
		$containerId = $this->params()->fromRoute('id', null);

		/* save this page in basecamp to return here after actions */
		$uri = $this->getRequest()->getRequestUri();
		$this->basecamp()->save($uri, $uri, $view);

		/* init some helpers */
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao(Container::$classId);

		/* Load extended properties in Dao */
		$loader = new Extended\Loader($factory);
		$extended = $loader->load(Container::$classId, $dao);

		/* @var \Rbplm\Org\Workitem $container */
		$container = $dao->loadFromId(new Container(), $containerId);

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('read', $container->getUid());

		/* Load parent, if existing */
		if ( $container->parentId ) {
			$parent = new \Rbplm\Org\Project();
			$factory->getDao($parent->cid)->loadFromId($parent, $container->parentId);
			$container->setParent($parent);
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('read', $container->getUid());

		/* send extended properties to view */
		$extended = $dao->extended;
		$view->extended = $extended;
		$view->spacename = $spacename;
		$view->container = $container;
		$view->id = $containerId;

		$view->project = $container;
		$view->links = array(
			'doctype' => $this->_getDoctypeLink($container),
			'process' => $this->_getProcessLink($container),
			'category' => $this->_getCategoryLink($container),
			'metadata' => $this->_getMetadataLink($container)
		);

		$view->discussion = $this->_getDiscussion($container);

		return $view;
	}

	/**
	 *
	 * @param \Rbplm\Org\Workitem $project
	 * @return \PDOStatement
	 */
	protected function _getDoctypeLink($parent)
	{
		$parentId = $parent->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get($parent->spacename);

		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Ged\Doctype::$classId);
		$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Doctype::$classId);
		$list = $lnkDao->getChildrenFromId($parentId, $dao->getSelectAsApp('child'));
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Org\Workitem $project
	 * @return \PDOStatement
	 */
	protected function _getProcessLink($parent)
	{
		$parentId = $parent->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get($parent->spacename);

		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Workflow\Model\Wf\Process::$classId);
		$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Process::$classId);
		$list = $lnkDao->getChildrenFromId($parentId, $dao->getSelectAsApp('child'));
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Org\Workitem $project
	 * @return \PDOStatement
	 */
	protected function _getCategoryLink($parent)
	{
		$parentId = $parent->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get($parent->spacename);

		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Ged\Category::$classId);
		$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Category::$classId);
		$list = $lnkDao->getChildrenFromId($parentId, $dao->getSelectAsApp('child'));
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Org\Workitem $project
	 * @return \PDOStatement
	 */
	protected function _getMetadataLink($parent)
	{
		$parentId = $parent->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get($parent->spacename);

		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbs\Extended\Property::$classId);
		$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Property::$classId);
		$list = $lnkDao->getChildrenFromId($parentId, $dao->getSelectAsApp('child'));
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Org\Workitem $container
	 */
	protected function _getDiscussion(\Rbplm\Org\Project $container)
	{
		/** @var \Rbs\Space\Factory $factory */
		$factory = $container->dao->factory;
		$discussionUid = $container->getId();

		$list = $factory->getList(Comment::$classId);
		$filter = $factory->getFilter(Comment::$classId);
		$filter->andFind($discussionUid, 'discussion_uid', Op::EQUAL);
		$list->load($filter->__toString());
		return $list;
	}
} /* End of class */

