<?php
namespace Ged\Controller\Container;

//use Application\Controller\AbstractController;
use Ged\Controller\AbstractNotificationController as AbstractController;
use Rbplm\Ged\Document;

/**
 */
class NotificationController extends AbstractController
{

	/** @var string */
	public $pageId = 'ged_container_notification';

	/** @var string */
	public $defaultSuccessForward = 'ged/container/notification/edit';

	/** @var string */
	public $defaultFailedForward = 'ged/container/notification/edit';

	/**
	 */
	public function init()
	{
		parent::init();

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Ged::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Ged::$appCn;
	}

	/**
	 * @return array
	 */
	protected function _getEvents()
	{
		return [
			Document\Version::SIGNAL_POST_CHECKIN => tra('document checkin'),
			Document\Version::SIGNAL_POST_CHECKOUT => tra('document checkout'),
			Document\Version::SIGNAL_POST_CANCELCHECKOUT => tra('cancel checkout'),
			Document\Version::SIGNAL_POST_DELETE => tra('document delete'),
			Document\Version::SIGNAL_POST_MOVE => tra('document move'),
			Document\Version::SIGNAL_POST_NEWVERSION => tra('document new version'),
			Document\Version::SIGNAL_POST_COPY => tra('document copy')
		];
	}

	/**
	 *
	 * @return \Rbplm\Ged\Document\Version
	 */
	protected function _newReference()
	{
		return new \Rbplm\Org\Workitem();
	}
}
