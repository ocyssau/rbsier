<?php
namespace Ged\Controller\Container;

// use Acl\Controller\Permission\IndexController as ParentController;
use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Acl\Model\Resource;
use Acl\Model\Right;
use Acl\Model\Rule;

/**
 */
class PermissionController extends AbstractController
{

	/** @var string */
	public $pageId = 'ged_container_permission';

	/** @var string */
	public $defaultSuccessForward = 'ged/container/index';

	/** @var string */
	public $defaultFailedForward = 'ged/container/index';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 */
	public function assignAction()
	{
		$view = $this->view;
		/* @var \Zend\Http\Request $request */
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', array());
			$id = $request->getQuery('id', current($ids));
			$spacename = $request->getQuery('spacename', null);
			if ( is_array($spacename) ) {
				$spacename = $spacename[$id];
			}

			/* */
			$cid = \Rbplm\Org\Workitem::$classId;
			$resourceCn = DaoFactory::get()->getDao(Resource::$classId)->getResourceCnFromReferIdAndCid($id, $cid);
			$roleId = $request->getQuery('roleId');
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$resourceCn = $request->getPost('resourceCn');
			$roleId = $request->getPost('roleId', null);
			$rights = $request->getPost('rights', array());
			$spacename = $request->getPost('spacename', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward(array(
					'roleId' => $roleId,
					'resourceCn' => $resourceCn
				));
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* */
		$factory = DaoFactory::get($spacename);

		/* */
		$form = new \Acl\Form\Permission\AssignForm($factory, $resourceCn);
		$form->setUrlFromCurrentRoute($this);

		/* */
		$ruleDao = $factory->getDao(Rule::$classId);
		$rule = new Rule($resourceCn, $roleId);
		$rule->spacename = $spacename;
		$form->bind($rule);

		/* */
		if ( $validate ) {
			foreach( $rights as $rightId => $ruleCode ) {
				$rule->addRule($rightId, $ruleCode);
			}
			$ruleDao->save($rule);
			return $this->successForward(array(
				'roleId' => $roleId,
				'resourceCn' => $resourceCn
			));
		}

		/* Get the permissions of the role */
		$roleRightsStmt = $ruleDao->getFromRoleAndResource($roleId, $resourceCn);
		$roleRights = $roleRightsStmt->fetchAll(\PDO::FETCH_ASSOC);
		$view->list = $roleRights;
		$view->rights = Right::enum();

		/* Display the template */
		$view->pageTitle = 'Assign Permission To Role ' . $roleId;
		$view->form = $form;
		$view->roleId = $roleId;
		$view->resourceCn = $resourceCn;
		$view->setTemplate($form->template);
		return $view;
	}
} /* End of class*/
