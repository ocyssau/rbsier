<?php
namespace Ged\Controller\Container;

use Rbs\Org\Container\Link\Category as Link;
use Rbplm\Org\Workitem as ParentClass;
use Rbplm\Ged\Category as ChildClass;
use Ged\Controller\LinkController;

/**
 */
class LinkcategoryController extends LinkController
{

	/** @var string */
	public $pageId = 'container_linkcategory';

	/** @var string */
	public $defaultSuccessForward = 'ged/container/categories';

	/** @var string */
	public $defaultFailedForward = 'ged/container/categories';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		/* */
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;

		$this->addTemplate = 'ged/container/link/addcategory';
		$this->displayTemplate = 'ged/container/link/categories';
		$this->linkClassId = Link::$classId;
		$this->parentClassId = ParentClass::$classId;
		$this->childClassId = ChildClass::$classId;
		return $this;
	}

	/**
	 * @param string $spacename
	 */
	protected function setSpacename($spacename)
	{
		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('spaces')->activate($spacename);
		$this->layout()->tabs = $tabs;
	}

	/**
	 *
	 * @return Link
	 */
	protected function getLinkModel()
	{
		$link = new Link();
		return $link;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Ged\Controller\LinkController::getParentModel()
	 */
	protected function getParentModel($factory)
	{
		$model = new ParentClass();
		return $model;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Ged\Controller\LinkController::getChildModel()
	 */
	protected function getChildModel($factory)
	{
		$model = new ChildClass();
		return $model;
	}

	/**
	 * Factory for the add link form
	 */
	protected function getAddLinkform($view, $factory)
	{
		if ( !isset($this->addLinkform) ) {
			$this->addLinkform = new \Ged\Form\Project\AddCategoryLinkForm($view, $factory);
		}
		return $this->addLinkform;
	}

	/**
	 * @param \Rbplm\Org\Workitem $parent
	 * @param \Rbs\Space\Factory $factory
	 * @return \PDOStatement
	 */
	protected function getLinks($parent, $factory)
	{
		$lnkDao = $factory->getDao($this->linkClassId);

		$select = array(
			'link.' . $lnkDao->getTranslator()->toSys('childId') . ' as childId',
			'link.' . $lnkDao->getTranslator()->toSys('parentId') . ' as parentId'
		);

		$stmt = $lnkDao->getChildrenFromUid($parent->getUid(), $select);
		return $stmt;
	}
}
