<?php
namespace Ged\Controller\Container;

/**
 *
 *
 */
class CadlibController extends ManagerController
{

	/** @var string */
	public $pageId = 'container_cadlib';

	/** @var string */
	public $defaultSuccessForward = 'ged/cadlib/index';

	/** @var string */
	public $defaultFailedForward = 'ged/cadlib/index';

	/** @var string */
	public $spacename = 'cadlib';

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 * @return \Ged\Form\Container\Workitem\EditForm
	 */
	protected function getEditForm($factory)
	{
		if ( !$this->editForm ) {
			$this->editForm = new \Ged\Form\Container\Cadlib\EditForm($factory);
		}
		return $this->editForm;
	}
}
