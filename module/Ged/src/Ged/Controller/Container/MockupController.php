<?php
namespace Ged\Controller\Container;

/**
 */
class MockupController extends ManagerController
{

	/** @var string */
	public $pageId = 'container_mockup';

	/** @var string */
	public $defaultSuccessForward = 'ged/mockup/index';

	/** @var string */
	public $defaultFailedForward = 'ged/mockup/index';

	/** @var string */
	public $spacename = 'mockup';

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 * @return \Ged\Form\Container\Mockup\EditForm
	 */
	protected function getEditForm($factory)
	{
		if ( !$this->editForm ) {
			$this->editForm = new \Ged\Form\Container\Mockup\EditForm($factory);
		}
		return $this->editForm;
	}
}
