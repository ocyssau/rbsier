<?php
namespace Ged\Controller\Container;

/**
 *
 *
 */
class BookshopController extends ManagerController
{

	/** @var string */
	public $pageId = 'container_bookshop';

	/** @var string */
	public $defaultSuccessForward = 'ged/bookshop/index';

	/** @var string */
	public $defaultFailedForward = 'ged/bookshop/index';

	/** @var string */
	public $spacename = 'bookshop';

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 * @return \Ged\Form\Container\Bookshop\EditForm
	 */
	protected function getEditForm($factory)
	{
		if ( !$this->editForm ) {
			$this->editForm = new \Ged\Form\Container\Bookshop\EditForm($factory);
		}
		return $this->editForm;
	}
}
