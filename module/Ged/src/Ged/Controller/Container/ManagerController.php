<?php
namespace Ged\Controller\Container;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Dao\Sier\Filter;
use Rbs\Extended;
use Rbplm\Org\Workitem;
use Rbplm\People;
use Rbplm\Vault;
use DateTime;
use Zend\Form\FormInterface;

/**
 */
abstract class ManagerController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $spacename = 'container';

	/**
	 *
	 * @var \Ged\Form\Container\EditForm
	 */
	protected $editForm;

	/**
	 * Run controller
	 */
	public function init()
	{
		/**/
		parent::init();
		
		/**/
		$spacename = $this->spacename;
		$this->factory = DaoFactory::get($spacename);
		$this->view->spacename = $spacename;

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('spaces')->activate($spacename);
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();

		$spacename = $this->spacename;
		$this->factory = DaoFactory::get($spacename);
		$this->view->spacename = $spacename;
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * 
	 * @param DaoFactory $factory
	 * @return \Ged\Form\Container\EditForm
	 */
	abstract protected function getEditForm($factory);

	/**
	 */
	public function indexAction()
	{
		/* Check rights from current resource to last children leaf. If one child is permit, permit on current resource */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* */
		$view = $this->view;
		$view->setTemplate('ged/container/manager/index');
		$this->layoutSelector()->clear($this);
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Init some helpers */
		$filter = new Filter('', false);
		$factory = $this->factory;
		$spacename = $this->spacename;

		/* Get model and dao */
		$container = $factory->getModel(Workitem::$classId);
		$dao = $factory->getDao($container->cid);
		$list = $factory->getList($container->cid);
		$pdo = $factory->getConnexion();

		/* init paginator */
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->load(['default'=>[
			'orderby'=>'number',
			'order'=>'asc',
			'limit' => 50
		]]);
		$count = $list->countAll($filter);
		$count = (1.2 * $count);
		$paginator->setMaxLimit($count);

		/* get table */
		$table = $factory->getTable($container->cid);
		$aliasTable = $factory->getTable(\Rbs\Org\Container\Alias::$classId);
		$aliasTableAs = 'alias';
		$tableAs = 'wi';

		/* Select main table */
		$select = $dao->getSelectAsApp($tableAs);
		$select['file_only'] = "file_only AS file_only";
		$select['aliasOfId'] = '"" AS aliasOfId';
		$select['aliasOfName'] = '"" AS aliasOfName';

		$loader = new Extended\Loader($factory);
		$extended = $loader->load($container->cid, $dao);
		if ( is_array($dao->extendedModel) ) {
			foreach( $dao->extendedModel as $asSys => $asApp ) {
				$select[$asApp] = $asSys . ' AS ' . $pdo->quote($asApp);
			}
		}

		/* select in alias table */
		$select2 = $select;
		$select2['id'] = "$aliasTableAs.alias_id as id";
		$select2['uid'] = str_replace($tableAs, $aliasTableAs, $select['uid']); /* uid */
		$select2['name'] = str_replace($tableAs, $aliasTableAs, $select['name']); /* name */
		$select2['number'] = str_replace($tableAs, $aliasTableAs, $select['number']); /* number */
		$select2['description'] = str_replace($tableAs, $aliasTableAs, $select['description']); /* description */
		$select2['cid'] = str_replace($tableAs, $aliasTableAs, $select['cid']); /* cid */
		$select2['created'] = str_replace($tableAs, $aliasTableAs, $select['created']); /* created */
		$select2['createById'] = str_replace($tableAs, $aliasTableAs, $select['createById']); /* createById */
		$select2['createByUid'] = str_replace($tableAs, $aliasTableAs, $select['createByUid']); /* createByUid */
		$select2['aliasOfId'] = "`$aliasTableAs`.`container_id` as `aliasOfId`";
		$select2['aliasOfName'] = "`$tableAs`.`number` as `aliasOfName`";

		/* init filter form */
		$filterForm = new \Ged\Form\Container\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'containerfilterform');
		$filterForm->load();
		if ( isset($dao->extended) ) {
			$filterForm->setExtended($dao->extended);
		}
		$filterForm->bindToFilter($filter)->save();
		$paginator->bindToFilter($filter)
			->bindToView($view)
			->save();

		/* */
		$sql1 = "SELECT " . implode(',', $select) . " FROM $table AS $tableAs";
		$idSysName = $dao->toSys('id');
		$sql2 = "SELECT " . implode(',', $select2) . " FROM $aliasTable AS $aliasTableAs JOIN $table AS wi ON alias.container_id=wi.$idSysName";
		$sql = "SELECT * FROM ($sql1 UNION $sql2) AS uni";
		$sql .= " WHERE " . $filter->__toString();

		/* */
		try {
			$list->loadFromSql($sql);
		}
		catch( \Exception $e ) {
			$this->errorStack()->error('An error is occured during  database request. Records messages below and contact your administrator.');
			$this->errorStack()->error($e->getMessage());
			$this->errorStack()->error($list->getStmt()->queryString);
		}

		/* */
		$view->list = $list->toArray();
		$view->filter = $filterForm;
		$view->extended = $extended;
		$view->randWindowName = 'container_' . uniqid();
		$view->pageTitle = tra($spacename . ' manager');

		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename');
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$spacename = $request->getPost('spacename');
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check right */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$factory = $this->factory;
		$currentUser = People\CurrentUser::get();

		/** @var Workitem $container */
		$init = $factory->getModel(Workitem::$classId);
		$container = $init::init(uniqid('CONTAINER'));
		$container->setCreateBy($currentUser)->setUpdateBy($currentUser);
		$container->spacename = $spacename;
		$dao = $factory->getDao(Workitem::$classId);
		$extendedLoader = new Extended\Loader($factory);
		$extendedLoader->load($container->cid, $dao);

		$forseenCloseDate = new \DateTime();
		$forseenCloseDate->add(new \DateInterval('P5Y')); // 5 years
		$container->setForseenCloseDate($forseenCloseDate);
		$container->newUid();
		$container->description = 'New workitem';

		$form = $this->getEditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->setEditMode(false);

		/* SET EXTENDED TO FORM */
		if ( $dao->extended ) {
			$form->setExtended($dao->extended, $view);
			$view->extended = $dao->extended;
		}
		else {
			$view->extended = array();
		}

		$form->bind($container);

		/* @todo: add sql transaction and rollback if failed */

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$wiName = $container->getNumber();
				$container->setName($wiName);

				$datas = $form->getData(FormInterface::VALUES_AS_ARRAY);
				$createPrivateVault = $datas['createprivatevault'];
				$repositId = $datas['vault'];

				/* create a new vault if required */
				if ( $createPrivateVault == true ) {
					try {
						$reposit = Vault\Reposit::init()->setName($wiName)
							->setNumber($wiName)
							->setDescription('Private vault for container ' . $wiName)
							->setCreated(new DateTime())
							->setCreateBy(People\CurrentUser::get())
							->setSpacename($spacename);

						$basePath = \Ranchbe::get()->getConfig('path.reposit.' . $spacename);
						$path = $basePath . '/' . $wiName;
						Vault\Reposit::initPath($path);
						$reposit->setPath($path);
						$factory->getDao(Vault\Reposit::$classId)->save($reposit);
						$container->path = $path;
						$container->setReposit($reposit);
					}
					catch( Vault\ExistingDirectoryException $e ) {
						$this->errorStack()->error("Creation of reposit FAILED :" . $e->getMessage());
					}
					catch( \Exception $e ) {
						$this->errorStack()->error("Creation of reposit FAILED :" . $e->getMessage());
					}
				}
				else {
					/* Load and set reposit */
					$reposit = new Vault\Reposit();
					$factory->getDao(Vault\Reposit::$classId)->loadFromId($reposit, $repositId);
					$path = $reposit->getPath();
					$container->setReposit($reposit);
					$container->path = $reposit->getPath();
				}

				/* Save container */
				try {
					$dao->save($container);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}

				return $this->successForward();
			}
		}

		$view->pageTitle = 'Create Container';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename');
			$containerId = $request->getQuery('id', null);
			$containerIds = $request->getQuery('checked', null);
			if ( $containerIds ) {
				$containerId = current($containerIds);
			}
			$validate = false;
		}
		if ( $request->isPost() ) {
			$containerId = $request->getPost('id', null);
			$spacename = $request->getPost('spacename');
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel || !$containerId ) {
				$this->successForward();
			}
		}

		/* init some helpers */
		$factory = $this->factory;

		/* Check right */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		/* init some working class */
		$container = $factory->getModel(Workitem::$classId);
		$container->spacename = $spacename;
		$dao = $factory->getDao(Workitem::$classId);
		$extendedLoader = new Extended\Loader($factory);
		$extendedLoader->load($container->cid, $dao);
		$dao->loadFromId($container, $containerId);

		/* load form */
		$form = $this->getEditForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->setEditMode(true);

		/* SET EXTENDED TO FORM */
		if ( $dao->extended ) {
			$form->setExtended($dao->extended, $view);
			$view->extended = $dao->extended;
		}
		else {
			$view->extended = array();
		}

		/* */
		$form->bind($container);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$dao->save($container);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		/* */
		$view->pageTitle = 'Edit Container ' . $container->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$containerIds = $request->getQuery('checked', []);
			$containerId = $request->getQuery('id', null);
			($containerId) ? $containerIds[] = $containerId : null;
		}
		if ( $request->isPost() ) {
			$containerIds = $request->getPost('checked', []);
		}

		if ( empty($containerIds) ) {
			$this->errorStack()->error(tra('You must select at least one item'));
			$this->errorForward();
		}

		/* init some helpers */
		$factory = $this->factory;

		$container = $factory->getModel(Workitem::$classId);
		/** @var \Rbs\Org\WorkitemDao $dao */
		$dao = $factory->getDao(Workitem::$classId);

		foreach( $containerIds as $containerId ) {
			try {
				/* Check right */
				$this->getAcl()->checkRightFromIdAndCid('delete', $containerId, Workitem::$classId);
				/* */
				$dao->deleteFromId($containerId);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $containerId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
			}
		}
		return $this->successForward();
	}

	/**
	 *
	 * @return boolean
	 */
	public function renameAction()
	{
		die("NOT IMPLEMENTED FOR PRODUCTION");
		
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename');
			$containerIds = $request->getQuery('checked', array());
			$containerId = $containerIds[0];
		}
		if ( $request->isPost() ) {
			$containerId = $request->getPost('id', null);
			$spacename = $request->getPost('spacename');
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
		}

		if ( $cancel || !$containerId ) {
			return $this->successForward();
		}

		$factory = $this->factory;
		$container = $factory->getModel(Workitem::$classId);
		$dao = $factory->getDao(Workitem::$classId);
		$dao->loadFromId($container, $containerId);

		/* Check right */
		$this->getAcl()->checkRightFromUid('delete', $container->getUid());

		$form = new \Ged\Form\Container\RenameForm($view, $factory);
		$binded = new \Application\Form\BindModel();
		$binded->id = $containerId;
		$binded->spacename = $spacename;
		$form->bind($binded);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					/* Get Current Reposit */
					$reposit = \Rbplm\Vault\Reposit::init($container->path);

					/* Move and Rename Reposit */
					$fromPath = $reposit->getPath();
					$dirname = dirname($fromPath);
					$toPath = $dirname . '/' . $binded->number;

					/* Update path of each records */
					try {
						//$recordDao = $factory->getDao();
					}
					catch( \Exception $e ) {}

					$directory = new \Rbplm\Sys\Directory();
					$directory->rename($reposit->getPath(), $toPath);

					/* Rename Container */
					$path = '';
					$container->path = $path;
					$container->setName($binded->number);

					/* Save Container */
					$dao->save($container);

					try {}
					catch( \Exception $e ) {}
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Rename Container ' . $container->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}
} /* End of class */
