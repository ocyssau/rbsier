<?php
namespace Ged\Controller\Container;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Org\Container\Alias;
use Rbplm\People\CurrentUser;

/**
 */
class AliasController extends AbstractController
{

	/** @var string */
	public $pageId = 'container_alias';

	/** @var string */
	public $defaultSuccessForward = 'ged/container/alias/{$spacename}';

	/** @var string */
	public $defaultFailedForward = 'ged/container/alias/{$spacename}';

	/**
	 *
	 * @var string
	 */
	public $spacename = 'container';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();
		$this->defaultSuccessForward = 'ged/container/alias';
		$this->defaultFailedForward = 'ged/container/alias';
		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * @param string $spacename
	 */
	protected function setSpacename($spacename)
	{
		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('spaces')->activate($spacename);
		$this->layout()->tabs = $tabs;
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('/ged/container/alias/index');
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename', null);
			$validate = false;
		}
		if ( $request->isPost() ) {
			$spacename = $request->getPost('spacename', $this->spacename);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		$this->setSpacename($spacename);

		/* */
		$alias = new Alias();
		$alias->spacename = $spacename;
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao($alias->cid);
		$alias->newUid();
		$alias->setCreateBy(CurrentUser::get());
		$alias->setCreated(new \DateTime());

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		/* */
		$form = new \Ged\Form\Container\Alias\EditForm($view, $factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($alias);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$alias->setName($alias->getNumber());
					$dao->save($alias);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Create Alias';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename');
			$aliasIds = $request->getQuery('checked', array());
			$aliasId = $aliasIds[0];
			$validate = false;
		}
		if ( $request->isPost() ) {
			$aliasId = $request->getPost('id', null);
			$spacename = $request->getPost('spacename', $this->spacename);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/**/
		$alias = new Alias();
		$alias->spacename = $spacename;
		$factory = DaoFactory::get($spacename);
		$dao = $factory->getDao($alias->cid);
		$dao->loadFromId($alias, $aliasId);

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('edit', $alias->getUid());

		/**/
		$form = new \Ged\Form\Container\Alias\EditForm($view, $factory);
		$form->setEditMode(true);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($alias);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($alias);
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Alias ' . $alias->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename', $this->spacename);
			$aliasIds = $request->getQuery('checked', array());
			$aliasId = $request->getQuery('id', null);
			($aliasId) ? $aliasIds[] = $aliasId : null;
		}

		$factory = DaoFactory::get($spacename);
		$aliasDao = $factory->getDao(Alias::$classId);

		if ( empty($aliasIds) ) {
			throw new \Exception('You must select at least one item');
		}

		foreach( $aliasIds as $aliasId ) {
			try {
				/* Check permissions */
				$uid = $aliasDao->query(array(
					$aliasDao->toSys('uid')
				), $aliasDao->toSys('id') . '=:id', array(
					':id' => $aliasId
				))
					->fetchColumn(0);
				$this->getAcl()->checkRightFromUid('edit', $uid);
				$aliasDao->deleteFromId($aliasId);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}
} /* End of class*/
