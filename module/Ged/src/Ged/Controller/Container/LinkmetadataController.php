<?php
namespace Ged\Controller\Container;

use Rbs\Org\Container\Link\Property as Link;
use Rbplm\Org\Workitem as ParentClass;
use Rbs\Extended\Property as ChildClass;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\Document;
use Rbs\Dao\Sier\Filter;
use Ged\Controller\LinkController;

/**
 */
class LinkmetadataController extends LinkController
{

	/** @var string */
	public $pageId = 'container_linkmetadata';

	/** @var string */
	public $defaultSuccessForward = 'ged/container/metadatas';

	/** @var string */
	public $defaultFailedForward = 'ged/container/metadatas';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		/* */
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;

		/* */
		$this->addTemplate = 'ged/container/link/addmetadata';
		$this->displayTemplate = 'ged/container/link/metadatas';
		$this->linkClassId = Link::$classId;
		$this->parentClassId = ParentClass::$classId;
		$this->childClassId = ChildClass::$classId;
		return $this;
	}

	/**
	 * @param string $spacename
	 */
	protected function setSpacename($spacename)
	{
		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('spaces')->activate($spacename);
		$this->layout()->tabs = $tabs;
	}

	/**
	 *
	 * @return Link
	 */
	protected function getLinkModel()
	{
		$link = new Link();
		return $link;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see LinkController::_getModel()
	 */
	protected function getParentModel($factory)
	{
		$model = new ParentClass();
		return $model;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see LinkController::_getModel()
	 */
	protected function getChildModel($factory)
	{
		$model = new ChildClass();
		return $model;
	}

	/**
	 * Factory for the add link form
	 */
	protected function getAddLinkform($view, $factory)
	{
		if ( !isset($this->addLinkform) ) {
			$this->addLinkform = new \Ged\Form\Project\AddPropertyLinkForm($view, $factory);
		}
		return $this->addLinkform;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Ged\Controller\LinkController::addlinkAction()
	 */
	public function __addlinkAction($filter = null, $bind = null)
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename', $this->spacename);
		}
		if ( $request->isPost() ) {
			$spacename = $request->getPost('spacename', $this->spacename);
		}
		$this->setSpacename($spacename);

		$extendedCid = Document\Version::$classId;
		$dao = DaoFactory::get($spacename)->getDao($extendedCid);
		$filter = new Filter('', false);
		$filter->andfind(':extendedCid', $dao->toSys('extendedCid'), Op::EQUAL);
		$bind = array(
			':extendedCid' => $extendedCid
		);
		return parent::addlinkAction($filter, $bind);
	}

	/**
	 * @param \Rbplm\Org\Workitem $parent
	 * @param \Rbs\Space\Factory $factory
	 * @return \PDOStatement
	 */
	protected function getLinks($parent, $factory)
	{
		$lnkDao = $factory->getDao($this->linkClassId);

		$select = array(
			'link.' . $lnkDao->getTranslator()->toSys('childId') . ' as childId',
			'link.' . $lnkDao->getTranslator()->toSys('parentId') . ' as parentId'
		);

		$stmt = $lnkDao->getChildrenFromUid($parent->getUid(), $select);
		return $stmt;
	}
}
