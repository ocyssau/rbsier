<?php
namespace Ged\Controller\Project;

use Rbs\Org\Project\Link\Container as Link;
use Rbplm\Org\Project as ParentClass;
use Ged\Controller\LinkController;

/**
 */
class LinkcontainerController extends LinkController
{

	/** @var string */
	public $pageId = 'project_linkcontainer';

	/** @var string */
	public $defaultSuccessForward = 'ged/project/container/index';

	/** @var string */
	public $defaultFailedForward = 'ged/project/container/index';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		/* */
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
		$this->addTemplate = 'ged/project/link/addcontainer';
		$this->displayTemplate = 'ged/project/link/container';
		$this->linkClassId = Link::$classId;
		$this->parentClassId = ParentClass::$classId;
		$this->spacename = $this->params()->fromRoute('spacename', 'default');
		$this->setChildClassId($this->spacename);
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
		$this->addTemplate = 'ged/project/link/addcontainer';
		$this->displayTemplate = 'ged/project/link/container';
		$this->linkClassId = Link::$classId;
		$this->parentClassId = ParentClass::$classId;
		$this->spacename = $this->params()->fromRoute('spacename', 'default');
		$this->setChildClassId($this->spacename);
	}

	/**
	 * 
	 * @param string $spacename
	 */
	private function setChildClassId($spacename)
	{
		switch ($spacename) {
			case 'default':
				$this->childClassId = \Rbplm\Org\Project::$classId;
				break;
			case 'workitem':
				$this->childClassId = \Rbplm\Org\Workitem::$classId;
				break;
			case 'cadlib':
				$this->childClassId = \Rbplm\Org\Cadlib::$classId;
				break;
			case 'mockup':
				$this->childClassId = \Rbplm\Org\Mockup::$classId;
				break;
			case 'bookshop':
				$this->childClassId = \Rbplm\Org\Bookshop::$classId;
				break;
		}
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Ged\Controller\LinkController::getLinkModel()
	 */
	protected function getLinkModel()
	{
		$link = new Link();
		$link->spacename = strtolower($this->factory->getName());
		return $link;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Ged\Controller\LinkController::getParentModel()
	 */
	protected function getParentModel($factory)
	{
		$model = $factory->getModel($this->parentClassId);
		return $model;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Ged\Controller\LinkController::getChildModel()
	 */
	protected function getChildModel($factory)
	{
		$model = $factory->getModel($this->childClassId);
		return $model;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Ged\Controller\LinkController::getAddLinkform()
	 */
	protected function getAddLinkform($view, $factory)
	{
		if ( !isset($this->addLinkform) ) {
			$form = new \Ged\Form\Project\AddContainerLinkForm($view, $factory);
			$this->addLinkform = $form;
		}

		return $this->addLinkform;
	}

	/**
	 * @param \Rbplm\Org\Project $parent
	 * @param \Rbs\Space\Factory $factory
	 * @return \PDOStatement
	 */
	protected function getLinks($parent, $factory)
	{
		$lnkDao = $factory->getDao($this->linkClassId);

		$select = array(
			'link.' . $lnkDao->getTranslator()->toSys('childId') . ' as childId',
			'link.' . $lnkDao->getTranslator()->toSys('parentId') . ' as parentId'
		);

		$spacename = strtolower($factory->getName());

		$stmt = $lnkDao->getChildrenFromUid($parent->getUid(), $select, "link.spacename=:spacename", array(
			':spacename' => $spacename
		));
		return $stmt;
	}
}
