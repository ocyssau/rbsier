<?php
namespace Ged\Controller\Project;

use Application\Controller\AbstractController;
use Rbplm\Org\Project;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Filter\Op;

/**
 */
class DetailController extends AbstractController
{

	/* @var string */
	public $pageId = 'project_detail';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$this->defaultSuccessForward = $this->getRequest()
			->getUri()
			->getPath();
		$this->defaultFailedForward = $this->defaultSuccessForward;
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;

		/* init tabs */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('project')->activate('project');
		$this->layout()->tabs = $tabs;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/project/detail/index');

		$projectId = $this->params()->fromRoute('id', null);

		/* save this page in basecamp to return here after actions */
		$uri = $this->getRequest()->getRequestUri();
		$this->basecamp()->save($uri, $uri, $view);

		/* inits helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Project::$classId);

		/* @var Project $project */
		$project = $dao->loadFromId(new Project(), $projectId);

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('read', $project->getUid());

		$view->project = $project;
		$view->links = array(
			'cadlib' => $this->_getCadlibLink($project),
			'bookshop' => $this->_getBookshopLink($project),
			'mockup' => $this->_getMockupLink($project),
			'doctype' => $this->_getDoctypeLink($project),
			'process' => $this->_getProcessLink($project),
			'category' => $this->_getCategoryLink($project)
		);

		$view->children = array(
			'workitem' => $this->_getWorkitems($project),
			'cadlib' => $this->_getCadlibs($project),
			'bookshop' => $this->_getBookshops($project),
			'mockup' => $this->_getMockups($project)
		);

		$view->pageTitle = tra('Project Manager');
		return $view;
	}

	/**
	 *
	 * @param \Rbplm\Org\Project $project
	 * @return \PDOStatement
	 */
	protected function _getWorkitems($project)
	{
		$projectId = $project->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get('workitem');

		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
		/** @var \Rbs\Dao\Sier\DaoList $list */
		$list = $factory->getList(\Rbplm\Org\Workitem::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->select($dao->getSelectAsApp());
		$filter->andfind($projectId, $dao->toSys('parentId'), Op::EQUAL);

		$list->load($filter);

		return $list->stmt;
	}

	/**
	 *
	 * @param \Rbplm\Org\Project $project
	 * @return \PDOStatement
	 */
	protected function _getCadlibs($project)
	{
		$projectId = $project->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get('cadlib');

		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
		/** @var \Rbs\Dao\Sier\DaoList $list */
		$list = $factory->getList(\Rbplm\Org\Workitem::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->select($dao->getSelectAsApp());
		$filter->andfind($projectId, $dao->toSys('parentId'), Op::EQUAL);
		$list->load($filter);

		return $list->stmt;
	}

	/**
	 *
	 * @param \Rbplm\Org\Project $project
	 * @return \PDOStatement
	 */
	protected function _getBookshops($project)
	{
		$projectId = $project->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get('bookshop');

		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
		/** @var \Rbs\Dao\Sier\DaoList $list */
		$list = $factory->getList(\Rbplm\Org\Workitem::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->select($dao->getSelectAsApp());
		$filter->andfind($projectId, $dao->toSys('parentId'), Op::EQUAL);
		$list->load($filter);

		return $list->stmt;
	}

	/**
	 *
	 * @param \Rbplm\Org\Project $project
	 * @return \PDOStatement
	 */
	protected function _getMockups($project)
	{
		$projectId = $project->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get('mockup');

		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
		/** @var \Rbs\Dao\Sier\DaoList $list */
		$list = $factory->getList(\Rbplm\Org\Workitem::$classId);

		/**/
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->select($dao->getSelectAsApp());
		$filter->andfind($projectId, $dao->toSys('parentId'), Op::EQUAL);
		$list->load($filter);

		return $list->stmt;
	}

	/**
	 * @param \Rbplm\Org\Project $project
	 * @return \PDOStatement
	 */
	protected function _getCadlibLink($project)
	{
		$projectId = $project->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get('cadlib');
		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Org\Cadlib::$classId);
		/** @var \Rbs\Org\Project\Link\ContainerDao */
		$lnkDao = $factory->getDao(\Rbs\Org\Project\Link\Container::$classId);

		$list = $lnkDao->getChildrenFromId($projectId, $dao->getSelectAsApp('child'), "link.spacename=:spacename", array(
			':spacename' => 'cadlib'
		));
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Org\Project $project
	 * @return \PDOStatement
	 */
	protected function _getBookshopLink($project)
	{
		$projectId = $project->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get('bookshop');
		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Org\Bookshop::$classId);
		$lnkDao = $factory->getDao(\Rbs\Org\Project\Link\Container::$classId);
		$list = $lnkDao->getChildrenFromId($projectId, $dao->getSelectAsApp('child'), "link.spacename=:spacename", array(
			':spacename' => 'bookshop'
		));
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Org\Project $project
	 * @return \PDOStatement
	 */
	protected function _getMockupLink($project)
	{
		$projectId = $project->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get('mockup');
		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Org\Mockup::$classId);
		$lnkDao = $factory->getDao(\Rbs\Org\Project\Link\Container::$classId);
		$list = $lnkDao->getChildrenFromId($projectId, $dao->getSelectAsApp('child'), "link.spacename=:spacename", array(
			':spacename' => 'mockup'
		));
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Org\Project $project
	 * @return \PDOStatement
	 */
	protected function _getDoctypeLink($project)
	{
		$projectId = $project->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get();
		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Ged\Doctype::$classId);
		$lnkDao = $factory->getDao(\Rbs\Org\Project\Link\Doctype::$classId);
		$list = $lnkDao->getChildrenFromId($projectId, $dao->getSelectAsApp('child'));
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Org\Project $project
	 * @return \PDOStatement
	 */
	protected function _getProcessLink($project)
	{
		$projectId = $project->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get();

		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Workflow\Model\Wf\Process::$classId);
		$lnkDao = $factory->getDao(\Rbs\Org\Project\Link\Process::$classId);
		$list = $lnkDao->getChildrenFromId($projectId, $dao->getSelectAsApp('child'));
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Org\Project $project
	 * @return \PDOStatement
	 */
	protected function _getCategoryLink($project)
	{
		$projectId = $project->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = DaoFactory::get();

		/** @var \Rbs\Dao\Sier $dao */
		$dao = $factory->getDao(\Rbplm\Ged\Category::$classId);
		$lnkDao = $factory->getDao(\Rbs\Org\Project\Link\Category::$classId);
		$list = $lnkDao->getChildrenFromId($projectId, $dao->getSelectAsApp('child'));
		return $list;
	}
}
