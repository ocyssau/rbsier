<?php
namespace Ged\Controller\Project;

use Rbs\Org\Project\Link\Doctype as Link;
use Rbplm\Org\Project as ParentClass;
use Rbplm\Ged\Doctype as ChildClass;
use Ged\Controller\LinkController;

/**
 */
class LinkdoctypeController extends LinkController
{

	/** @var string */
	public $pageId = 'project_linkdoctype';

	/** @var string */
	public $defaultSuccessForward = 'ged/project/doctypes/index';

	/** @var string */
	public $defaultFailedForward = 'ged/project/doctypes/index';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		/* */
		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
		$this->addTemplate = 'ged/project/link/adddoctype';
		$this->displayTemplate = 'ged/project/link/doctypes';
		$this->linkClassId = Link::$classId;
		$this->parentClassId = ParentClass::$classId;
		$this->childClassId = ChildClass::$classId;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
		$this->addTemplate = 'ged/project/link/adddoctype';
		$this->displayTemplate = 'ged/project/link/doctypes';
		$this->linkClassId = Link::$classId;
		$this->parentClassId = ParentClass::$classId;
		$this->childClassId = ChildClass::$classId;
	}

	/**
	 *
	 * @return Link
	 */
	protected function getLinkModel()
	{
		$link = new Link();
		return $link;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Ged\Controller\LinkController::getParentModel()
	 */
	protected function getParentModel($factory)
	{
		$model = new ParentClass();
		return $model;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Ged\Controller\LinkController::getChildModel()
	 */
	protected function getChildModel($factory)
	{
		$model = new ChildClass();
		return $model;
	}

	/**
	 * Factory for the add link form
	 */
	protected function getAddLinkform($view, $factory)
	{
		if ( !isset($this->addLinkform) ) {
			$this->addLinkform = new \Ged\Form\Project\AddDoctypeLinkForm($view, $factory);
		}
		return $this->addLinkform;
	}

	/**
	 * @param \Rbplm\Org\Project $parent
	 * @param \Rbs\Space\Factory $factory
	 * @return \PDOStatement
	 */
	protected function getLinks($parent, $factory)
	{
		$lnkDao = $factory->getDao($this->linkClassId);

		$select = array(
			'link.' . $lnkDao->getTranslator()->toSys('childId') . ' as childId',
			'link.' . $lnkDao->getTranslator()->toSys('parentId') . ' as parentId'
		);

		$stmt = $lnkDao->getChildrenFromUid($parent->getUid(), $select);
		return $stmt;
	}
}
