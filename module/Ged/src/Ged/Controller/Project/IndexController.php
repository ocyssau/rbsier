<?php
namespace Ged\Controller\Project;

use Application\Controller\AbstractController;
use Rbplm\Org\Project;
use Rbs\Space\Factory as DaoFactory;

/**
 */
class IndexController extends AbstractController
{

	/* @var string */
	public $pageId = 'project_index';

	/* @var string */
	public $defaultSuccessForward = 'ged/project/index';

	/* @var string */
	public $defaultFailedForward = 'ged/project/index';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('project')->activate('project');
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultSuccessForward;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 */
	public function indexAction($list = null)
	{
		/* */
		$view = $this->view;
		$this->ifSuccessForward = '/home';
		$this->ifFailedForward = '/home';
		$this->basecamp()->save($this->ifSuccessForward, $this->ifFailedForward, $view);
		$this->basecamp()->setForward($this);
		$view->setTemplate('ged/project/index/index');
		$this->layoutSelector()->clear($this);

		/* return to this page after actions */
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Check rights from current resource to last children leaf. If one child is permit, permit on current resource */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* set some helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Project::$classId);
		$list = $factory->getList(Project::$classId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		/* init filter form */
		$filterForm = new \Ged\Form\Project\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'projectfilterform');
		$filterForm->load();

		/* init paginator */
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->load([
			'default' => [
				'orderby' => 'uid',
				'order' => 'asc',
				'limit' => 50
			]
		]);
		$count = $list->countAll($filter);
		$count = (1.1 * $count);
		$paginator->setMaxLimit($count);

		/* Select main table */
		$select = $dao->getTranslator()->getSelectAsApp();
		$filter->select($select);

		/* */
		$filterForm->bindToFilter($filter)->save();
		$paginator->bindToFilter($filter)
			->bindToView($view)
			->save();
		$list->load($filter);

		/* */
		$view->list = $list->toArray();
		$view->filter = $filterForm;
		$view->pageTitle = tra('Project Manager');
		return $view;
	}

	/**
	 *
	 * @return \Application\View\ViewModel|string
	 *
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}
		else {
			$validate = false;
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$project = Project::init();
		$project->setName('Project' . uniqid());
		$forseenCloseDate = new \DateTime();
		$forseenCloseDate->add(new \DateInterval('P5Y')); // 5 years
		$project->setForseenCloseDate($forseenCloseDate);

		$factory = DaoFactory::get();
		$form = new \Ged\Form\Project\EditForm($view, $factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($project);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$project->setNumber(\Rbs\Number::normalize($project->getName()));
					$factory->getDao($project->cid)->save($project);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = tra('Create a project');
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$projectIds = $request->getQuery('checked', array());
			$projectId = $request->getQuery('id', null);
			(!$projectId) ? $projectId = current($projectIds) : null;
			$validate = false;
		}
		if ( $request->isPost() ) {
			$projectId = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* */
		$project = new Project();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Project::$classId);
		$dao->loadFromId($project, $projectId);

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('edit', $project->getUid());

		/* */
		$form = new \Ged\Form\Project\EditForm($view, $factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($project);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$project->dao->save($project);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Edit project %s'), $project->getName());
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$projectIds = $request->getQuery('checked', array());
			$projectId = $request->getQuery('id', null);
			($projectId) ? $projectIds[] = $projectId : null;
		}

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Project::$classId);

		foreach( $projectIds as $projectId ) {
			try {
				/* Check permissions */
				$this->getAcl()->checkRightFromIdAndCid('delete', $projectId, Project::$classId);

				/* */
				$dao->deleteFromId($projectId);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $projectId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
			}
		}

		$this->successForward();
	}
}
