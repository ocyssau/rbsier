<?php
namespace Ged\Archiver;

use Rbplm\AnyObject;
use Rbplm\Sys\Date as DateTime;
use Rbplm\Dao\MappedInterface;

/**
 * Implement Any->AnyObject->use(LifeControl, Owned, Bewitched)->Mapped->MappedInterface
 *
 *
 */
class Media extends AnyObject implements MappedInterface
{
	use \Rbplm\Mapped;

	/**
	 * @var string
	 */
	static $classId = 'archivermedia';

	/**
	 * @param string $name
	 * @param \Rbplm\People\User $by
	 * @return \Rbplm\Any
	 */
	public static function init($name, $by = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		$obj->updated = new DateTime();
		$obj->created = new DateTime();
		$obj->setName($name);
		if ( $by ) {
			$obj->setOwner($by);
			$obj->setCreateBy($by);
			$obj->setUpdateBy($by);
		}
		return $obj;
	}

	/**
	 *
	 * @param array $properties
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['number'])) ? $this->number = $properties['number'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
		(isset($properties['type'])) ? $this->type = $properties['type'] : null;
		(isset($properties['emplacement'])) ? $this->emplacement = $properties['emplacement'] : null;
		(isset($properties['ofObjectId'])) ? $this->ofObjectId = $properties['ofObjectId'] : null;
		(isset($properties['ofObjectUid'])) ? $this->ofObjectUid = $properties['ofObjectUid'] : null;
		(isset($properties['ofObjectCid'])) ? $this->ofObjectCid = $properties['ofObjectCid'] : null;
		(isset($properties['spacename'])) ? $this->spacename = $properties['spacename'] : null;
		(isset($properties['notice'])) ? $this->notice = $properties['notice'] : null;
		return $this;
	}
}