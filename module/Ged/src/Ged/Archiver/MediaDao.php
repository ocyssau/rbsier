<?php
//%LICENCE_HEADER%
namespace Ged\Archiver;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>

 CREATE TABLE IF NOT EXISTS `archiver_media` (
 `id` int(11) NOT NULL,
 `uid` VARCHAR(64) NOT NULL,
 `cid` VARCHAR(64) NOT NULL DEFAULT 'archivermedia',
 `name` VARCHAR(128) DEFAULT NULL,
 `number` VARCHAR(128) DEFAULT NULL,
 `designation` text,
 `type` VARCHAR(64) DEFAULT NULL,
 `emplacement` VARCHAR(128) DEFAULT NULL,
 `created` DATETIME DEFAULT NULL,
 `create_by_id` int(11) DEFAULT NULL,
 `owner_id` int(11) DEFAULT NULL,
 `notice` text,
 PRIMARY KEY (`id`),
 UNIQUE KEY `UC_archiver_media_1` (`uid`),
 UNIQUE KEY `UC_archiver_media_2` (`number`),
 INDEX `INDEX_archiver_media_1` (`name`)
 ) ENGINE=InnoDB;

 CREATE TABLE IF NOT EXISTS `archiver_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=MyISAM AUTO_INCREMENT=10;
 INSERT INTO archiver_seq SET id=10;

 CREATE TABLE IF NOT EXISTS `archiver_media_object_rel` (
 `uid` VARCHAR(64) NOT NULL,
 `media_id` int(11) NOT NULL,
 `ofObjectId` int(11) DEFAULT NULL,
 `ofObjectUid` VARCHAR(64) DEFAULT NULL,
 `ofObjectCid` VARCHAR(64) DEFAULT NULL,
 `spacename` VARCHAR(64) DEFAULT NULL,
 PRIMARY KEY (`uid`),
 INDEX `INDEX_archiver_media_2` (`ofObjectId`),
 INDEX `INDEX_archiver_media_3` (`ofObjectUid`),
 INDEX `INDEX_archiver_media_4` (`ofObjectCid`),
 INDEX `INDEX_archiver_media_5` (`spacename`)
 ) ENGINE=InnoDB;


 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class MediaDao extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table = 'archiver_media';

	public static $vtable = 'archiver_media';

	public static $sequenceName = 'archiver_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'name' => 'name',
		'number' => 'number',
		'designation' => 'description',
		'type' => 'type',
		'emplacement' => 'emplacement',
		'created' => 'created',
		'create_by_id' => 'createById',
		'owner_id' => 'ownerId',
		'notice' => 'notice'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'created' => 'datetime',
		'updated' => 'datetime'
	);
}
