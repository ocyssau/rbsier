<?php
namespace Ged\Archiver;

use Rbplm\Ged\Document;
use Rbs\Vault\Vault;
use Rbs\EventDispatcher\EventManager;
use Rbs\Ged\Document\Event;

/**
 * Service class to archive documents
 *
 *      Emited signals :
 *      - self::SIGNAL_PRE_ARCHIVE
 *      - self::SIGNAL_POST_ARCHIVE
 *
 *      @verbatim
 *      @endverbatim
 *
 */
class Archiver implements \Rbs\EventDispatcher\EventManagerAwareInterface
{

	/**/
	const SIGNAL_PRE_ARCHIVE = 'archive.pre';

	/**/
	const SIGNAL_POST_ARCHIVE = 'archive.post';

	/**/
	const STATUS_ARCHIVED = 'archived';

	/**
	 * @var \Rbs\Space\Factory
	 */
	protected $factory;

	/**
	 * @var string
	 */
	protected $basePath;

	/**
	 * @var string
	 */
	protected $repositFolder;

	/**
	 * Value of the offset to apply to access_code
	 *
	 * @var integer
	 */
	public static $accessCode = 100;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 * @param string $basePath
	 */
	public function __construct($factory, $basePath)
	{
		$this->factory = $factory;
		$this->basePath = $basePath;
	}

	/**
	 *
	 * @param EventManager $eventManager
	 */
	public function setEventManager(EventManager $eventManager)
	{
		$this->eventManager = $eventManager;
		return $this;
	}

	/**
	 *
	 * @return EventManager
	 */
	public function getEventManager()
	{
		if ( !isset($this->eventManager) ) {
			$this->eventManager = new EventManager();
		}
		return $this->eventManager;
	}

	/**
	 * Get the archive reposit from the container
	 *
	 * @param \Rbplm\Org\Workitem $container
	 * @param string $spacename
	 */
	public function getReposit($container)
	{
		$spacename = strtolower($this->factory->getName());
		$basePath = $this->basePath;

		if ( !is_dir($basePath) ) {
			throw new Exception(sprintf("Archive directory %s is not existing. Check [path.reposit.$spacename.archive] config key", $basePath));
		}
		if ( !is_writable($basePath) ) {
			throw new Exception(sprintf("Archive directory %s is not writable. Check permissions.", $basePath));
		}

		$wiName = $container->getUid();
		$path = $basePath . '/' . $wiName;
		$reposit = \Rbs\Vault\Reposit\Archive::init($path);

		return $reposit;
	}

	/**
	 * All Docfiles of document are move in archiving reposit.*
	 * Set the accessCode to current access code of docfile + self::$accessCode value
	 *
	 * The docfiles must be loaded in document before.
	 * The parent container must be loaded in document before.
	 * The document->dao must be set
	 *
	 * If $cleaniterations=true, delete iterations files
	 *
	 * @param Document\Version $document
	 * @param bool $cleaniterations
	 *
	 */
	public function archive($document, $cleaniterations = true)
	{
		$docfiles = $document->getDocfiles();
		$factory = $this->factory;
		$container = $document->getParent();
		$archiveReposit = $this->getReposit($container);
		$vault = new Vault($archiveReposit);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		/* Notiy observers on event */
		$document->archiver = $this;

		$event = new Event(self::SIGNAL_PRE_ARCHIVE, $document, $factory);
		$this->getEventManager()->trigger($event);

		/**/
		foreach( $docfiles as $docfile ) {
			if ( $docfile->checkAccess() > self::$accessCode ) {
				continue;
			}

			$archivedRecord = $vault->depriveToArchive($docfile->getData(), $archiveReposit);
			$docfile->setData($archivedRecord);
			$docfile->lock($docfile->checkAccess() + self::$accessCode);
			$docfile->dao->save($docfile);

			/* Delete the iterations of file */
			if ( $cleaniterations ) {
				try {
					$dfService->deleteIterations($docfile);
				}
				catch( \Exception $e ) {
					continue;
				}
			}
		}

		/**/
		$accessCode = $document->checkAccess() + self::$accessCode;
		$document->lock($accessCode);
		$document->dao->save($document);

		/* Notiy observers on event */
		$this->getEventManager()->trigger($event->setName(self::SIGNAL_POST_ARCHIVE));

		/* Clean history */
		$historyDao = $factory->getDao(\Rbs\Ged\Document\History::$classId);
		$historyDao->deleteFromDocumentId($document->getId());

		/* Close processes and deletes processes instances for documents */
		$documentProcessLinkDao = $factory->getDao(\Rbs\Wf\Instance\DocumentLink::$classId);
		/* Delete links with process instances */
		$documentProcessLinkDao->deleteFromDocumentId($document->getId(), '%');

		return $this;
	}
}
