<?php
//%LICENCE_HEADER%
namespace Ged\Cron;

use Rbs\Batch\CallbackInterface;
use Rbs\Space\Factory as DaoFactory;

/**
 * 
 */
class CleanShared implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 *
	 */
	public function run()
	{
		echo date(DATE_RFC822) . "\n";
		echo 'Cleanup of obsolete documents shares' . "\n";

		$dao = DaoFactory::get()->getDao(\Rbs\Ged\Document\Share\PublicUrl::$classId);
		$dao->cleanup();
		echo $dao->lastStmt->queryString . "\n";
		var_export($dao->lastBind) . "\n";

		echo "End at " . date(DATE_RFC822) . "\n";
		return $this;
	}
}
