<?php
namespace Ged\Form\Category;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author olivier
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/* @var string */
	public $template;

	/* @var \Ranchbe */
	public $ranchbe;

	/* @var array */
	protected $scriptsSelect;

	/* @var array */
	protected $iconsSelect;

	/* @var array */
	protected $fileExtensionSelect;

	/* @var array */
	protected $visufileExtensionSelect;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('categoryEdit');

		$this->template = 'ged/category/editform';
		$this->ranchbe = \Ranchbe::get();

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden',
				'value' => strtolower($factory->getName())
			)
		));

		/* Label */
		$this->add(array(
			'name' => 'nodelabel',
			'type' => \Zend\Form\Element\Text::class,
			'attributes' => array(
				'placeholder' => 'No spaces, no special chars',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Label')
			)
		));

		/* ParentId */
		$this->add(array(
			'name' => 'parentId',
			'type' => \Application\Form\Element\SelectCategory::class,
			'attributes' => array(
				'placeholder' => 'As child of',
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1',
				'multiple' => false
			),
			'options' => array(
				'label' => tra('As child of'),
				'daoFactory' => $factory,
				'load' => 'inherited',
				'maybenull' => true,
				'returnName' => false,
				'fullname' => true,
				'displayBoth' => false
			)
		));

		/*
		 $this->add(array(
		 'name' => 'parentId',
		 'type' => 'Zend\Form\Element\Select',
		 'attributes' => array(
		 'placeholder' => '',
		 'class' => 'form-control category-ajax-selector',
		 'size' => 1
		 ),
		 'options' => array(
		 'label' => tra('As child of'),
		 'value_options' => array(),
		 'multiple' => false,
		 'disable_inarray_validator' => true
		 )
		 ));
		 */

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => \Zend\Form\Element\Textarea::class,
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Icon */
		$this->add(array(
			'name' => 'icon',
			'type' => \Zend\Form\Element\Select::class,
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control icon-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Icon'),
				'value_options' => array(),
				'multiple' => false
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$mask = \Ranchbe::get()->getConfig('category.name.mask');
		if ( $mask ) {
			$help = \Ranchbe::get()->getConfig('category.name.mask.help');
			$nameValidators = array(
				array(
					'name' => 'Regex',
					'options' => array(
						'pattern' => '/' . $mask . '/i',
						'messages' => array(
							\Zend\Validator\Regex::NOT_MATCH => $help
						)
					)
				)
			);
		}
		else {
			$nameValidators = array();
		}

		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => false
			),
			'nodelabel' => array(
				'required' => true,
				'validators' => $nameValidators
			),
			'parentId' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Digits'
					)
				),
				'filters' => array(
					array(
						'name' => 'ToNull'
					)
				)
			),
			'icon' => array(
				'required' => false
			)
		);
	}
}
