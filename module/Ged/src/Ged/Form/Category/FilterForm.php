<?php
namespace Ged\Form\Category;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 * 
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @param \RBs\Space\Factory $factory
	 * @param string $nameSpace
	 */
	public function __construct($factory, $nameSpace)
	{
		parent::__construct($factory, $nameSpace);

		$this->template = 'ged/category/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$this->view->spacename = strtolower($factory->getName());

		/* Magic */
		$this->add(array(
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control',
				'title' => 'Search in name, number, designation, File extension'
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_magic',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(\Rbplm\Ged\Category::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* MAGIC */
		if ( $datas['find_magic'] ) {
			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, '-')", $dao->toSys('name'), $dao->toSys('description'));
			$filter->andFind($datas['find_magic'], $concat, Op::OP_CONTAINS);
		}

		return $this;
	}
}
