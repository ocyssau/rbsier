<?php
namespace Ged\Form\Document;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Rbs\Model\Hydrator\Extendable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author olivier
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{
	use \Application\Form\ExtendableTrait;

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @var array
	 */
	public $documentIds = array();

	/**
	 *
	 * @param string $name
	 */
	public function __construct($factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('documentEdit');

		$this->template = 'ged/document/version/editform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$this->daoFactory = $factory;
		$spacename = $factory->getName();

		/* Add hidden fields */
		$this->add([
			'name' => 'id',
			'attributes' => [
				'type' => 'hidden'
			]
		]);
		/* Add hidden fields */
		$this->add([
			'name' => 'spacename',
			'attributes' => [
				'type' => 'hidden',
				'value' => $spacename
			]
		]);

		/* Name */
		$this->add([
			'name' => 'name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => [
				'placeholder' => '',
				'class' => 'form-control'
			],
			'options' => [
				'label' => tra('Name')
			]
		]);

		/* Tags */
		$this->add([
			'name' => 'tags',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => [
				'placeholder' => '',
				'class' => 'form-control rb-tagsinput',
				'size' => 60
			],
			'options' => [
				'label' => tra('Tags')
			]
		]);

		/* As template */
		$this->add([
			'name' => 'asTemplate',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => [
				'class' => 'form-control'
			],
			'options' => [
				'label' => tra('As Template'),
				'use_hidden_element' => true,
				'checked_value' => 1,
				'unchecked_value' => 0
			]
		]);

		/* Number */
		$this->add([
			'name' => 'number',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => [
				'placeholder' => '',
				'class' => 'form-control'
			],
			'options' => [
				'label' => tra('Number')
			]
		]);

		/* Label */
		$this->add([
			'name' => 'label',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => [
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 50
			],
			'options' => [
				'label' => tra('Label'),
				'size' => 25
			]
		]);

		/* Description */
		$this->add([
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => [
				'placeholder' => '',
				'class' => 'form-control'
			],
			'options' => [
				'label' => tra('Description')
			]
		]);

		/* File */
		$this->add([
			'name' => 'file',
			'type' => 'Zend\Form\Element\File',
			'attributes' => [
				'placeholder' => 'Select A File',
				'class' => ''
			],
			'options' => [
				'label' => tra('File')
			]
		]);

		/* Submit button */
		$this->add([
			'name' => 'validate',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			]
		]);

		/* Cancel button */
		$this->add([
			'name' => 'cancel',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			]
		]);
	}

	/**
	 * Category
	 */
	public function selectCategory($containerId)
	{
		$this->add([
			'name' => 'categoryId',
			'type' => 'Application\Form\Element\SelectCategory',
			'attributes' => [
				'multiple' => false,
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1'
			],
			'options' => [
				'label' => tra('Category'),
				'load' => 'inherited',
				'daoFactory' => $this->daoFactory,
				'containerId' => $containerId,
				'returnName' => false,
				'fullname' => true,
				'displayBoth' => false,
				'maybenull' => true
			]
		]);
		return $this;
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$mask = \Ranchbe::get()->getConfig('document.name.mask');
		$numberValidators = [
			[
				'name' => 'StringLength',
				'options' => [
					'encoding' => 'UTF-8',
					'min' => 1,
					'max' => 255
				]
			]
		];
		if ( $mask ) {
			$help = \Ranchbe::get()->getConfig('document.name.mask.help');
			$numberValidators[] = [
				'name' => 'StringLength',
				'options' => [
					'encoding' => 'UTF-8',
					'min' => 1,
					'max' => 255
				]
			];
			$this->numberHelp = $help;
		}

		return array(
			'id' => [
				'required' => false
			],
			'spacename' => [
				'required' => true
			],
			'description' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'StripTags'
					],
					[
						'name' => 'StringTrim'
					]
				],
				'validators' => [
					[
						'name' => 'StringLength',
						'options' => [
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 512
						]
					]
				]
			],
			'name' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'StripTags'
					],
					[
						'name' => 'StringTrim'
					]
				],
				'validators' => [
					[
						'name' => 'StringLength',
						'options' => [
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 255
						]
					]
				]
			],
			'number' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'StripTags'
					],
					[
						'name' => 'StringTrim'
					]
				],
				'validators' => $numberValidators
			],
			'categoryId' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'Int'
					],
					[
						'name' => 'ToNull'
					]
				]
			],
			'file' => [
				'required' => false
			],
			'asTemplate' => [
				'required' => false
			]
		);
	}

	/**
	 *
	 * @param string $mode
	 */
	public function setMode($mode = 'edit')
	{
		if ( $mode == 'edit' ) {
			$this->remove('number');
			$this->remove('name');
		}
	}

	/**
	 * In case of multi edit, add a document to edit
	 */
	public function addDocument($document)
	{
		$id = $document->getId();
		$this->documentIds[] = $id;

		/* Add hidden fields */
		$this->add([
			'name' => 'checked[' . $id . ']',
			'attributes' => [
				'type' => 'hidden',
				'value' => $document->getId()
			]
		]);

		return $this;
	}
}

