<?php
namespace Ged\Form\Document\Version;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Rbplm\Ged\AccessCode;
use Exception;

/**
 */
class NewversionForm extends Form implements InputFilterProviderInterface
{

	/**
	 * Count number of documents to version
	 * @var integer
	 */
	public $count = 0;

	/**
	 * List of document ids to versionned
	 * @var array
	 */
	public $documentIds = array();

	/**
	 * @var string
	 */
	public $spacename = null;

	/**
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('newversion');

		$this->template = 'ged/document/version/newversionform.phtml';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* VERSION */
		$this->add(array(
			'name' => 'version',
			'type' => 'Application\Form\Element\SelectVersion',
			'attributes' => array(
				'class' => 'form-control',
				'data-live-search' => true,
				'data-size' => 10
			),
			'options' => array(
				'label' => tra('Version'),
				'maybenull' => true
			)
		));

		/* Comments */
		$this->add(array(
			'name' => 'comment',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'rows' => 5,
				'cols' => 80
			),
			'options' => array(
				'label' => tra('Comment')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'tocontainerid' => array(
				'required' => false
			),
			'version' => array(
				'required' => false
			),
			'comment' => array(
				'required' => false
			)
		);
	}

	/**
	 * @param \Rbplm\Ged\Document $document
	 */
	public function addDocument($document)
	{
		/* Check status */
		$aCode = $document->checkAccess();
		$msg = "";
		if ( $aCode != AccessCode::FREE && $aCode < AccessCode::LOCKEDLEVEL1 ) {
			$msg = sprintf(tra('Document %s is not in right state. To be versionned, a document must be FREE or LOCKED'), $document->getNumber());
			throw new Exception($msg);
		}

		if ( $this->spacename ) {
			if ( $this->spacename != $document->spacename ) {
				$msg = sprintf(tra('heterogeneous space detected in selection. Document %s is rejected'), $document->getNumber());
				throw new Exception($msg);
			}
		}

		/* set the container selector from space defined by first document selected */
		if ( !$this->has('tocontainerid') ) {
			$factory = $document->dao->factory;
			/* TARGET CONTAINER */
			$this->add(array(
				'name' => 'tocontainerid',
				'type' => 'Application\Form\Element\SelectContainer',
				'attributes' => array(
					'class' => 'form-control',
					'data-live-search' => true,
					'data-size' => 10
				),
				'options' => array(
					'label' => tra('Target Container'),
					'daoFactory' => $factory,
					'maybenull' => true
				)
			));
		}

		$id = $document->getId();
		$this->spacename = $document->spacename;
		$this->documentIds[] = $id;
		$this->count++;

		$label = $document->getName() . ' v' . $document->version . '.' . $document->iteration;

		/* Keep */
		$elemtName = 'checked[' . $id . ']';
		$this->add(array(
			'name' => $elemtName,
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control tovalidate-cb',
				'checked' => true
			),
			'options' => array(
				'label' => $label,
				'use_hidden_element' => false,
				'checked_value' => $id
			)
		));
		$this->getInputFilter()->add([
			'required' => false
		], $elemtName);

		/* Add hidden fields */
		$elemtName = 'spacenames[' . $id . ']';
		$this->add([
			'name' => $elemtName,
			'attributes' => [
				'type' => 'hidden',
				'value' => $this->spacename
			]
		]);
		$this->getInputFilter()->add([
			'required' => false
		], $elemtName);
	}
}

