<?php
namespace Ged\Form\Document;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Rbs\Model\Hydrator\Extendable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author olivier
 *
 */
class EditMultiForm extends AbstractForm implements InputFilterProviderInterface
{
	use \Application\Form\ExtendableTrait;

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @var array
	 */
	public $documents = array();

	/**
	 *
	 * @param string $name
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('documentEditmulti');

		$this->template = 'ged/document/version/editmultiform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => 'Let blank to no change',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * 
	 */
	public function selectCategory($containerId, $daoFactory)
	{
		$this->add(array(
			'name' => 'categoryId',
			'type' => 'Application\Form\Element\SelectCategory',
			'attributes' => array(
				'multiple' => false,
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1'
			),
			'options' => array(
				'label' => tra('Category'),
				'mode' => 'inherited',
				'daoFactory' => $daoFactory,
				'containerId' => $containerId,
				'returnName' => false,
				'fullname' => true,
				'displayBoth' => false,
				'maybenull' => true
			)
		));
		return $this;
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'checked' => array(
				'required' => false
			),
			'spacenames' => array(
				'required' => false
			),
			'description' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 512
						)
					)
				)
			),
			'categoryId' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'Int'
					),
					array(
						'name' => 'ToNull'
					)
				)
			)
		);
	}

	/**
	 * In case of multi edit, add a document to edit
	 */
	public function addDocument($document)
	{
		$id = $document->getId();
		$this->documents[] = $document;

		/* Add hidden fields */
		$this->add(array(
			'name' => 'checked[' . $id . ']',
			'attributes' => array(
				'type' => 'hidden',
				'value' => $document->getId()
			)
		));

		/* Add name */
		$this->add(array(
			'name' => 'name[' . $id . ']',
			'attributes' => array(
				'type' => 'text',
				'value' => $document->getName()
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacenames[' . $id . ']',
			'attributes' => array(
				'type' => 'hidden',
				'value' => $document->spacename
			)
		));

		return $this;
	}
}
