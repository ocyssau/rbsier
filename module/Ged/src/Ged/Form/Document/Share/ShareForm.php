<?php
namespace Ged\Form\Document\Share;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class ShareForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 * Count number of documents to version
	 * @var integer
	 */
	public $count = 0;

	/**
	 * List of document ids to versionned
	 * @var array
	 */
	public $documentIds = array();

	/**
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('shareDocumentForm');
		$this->setAttribute('id', 'shareDocumentForm');
		$this->template = 'ged/document/share/publicurlform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Share/Unshare Checkbox */
		$this->add(array(
			'name' => 'isShared',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-check-input',
				'id' => 'isSharedCb'
			),
			'options' => array(
				'label' => tra('Is Shared')
			)
		));

		/* Set Share form */
		$this->add(array(
			'name' => 'publicShare',
			'type' => '\Ged\Form\Document\Share\PublicUrlFieldset'
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Close'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'spacename' => array(
				'required' => false
			),
			'id' => array(
				'required' => false
			)
		);
	}
}

