<?php
namespace Ged\Form\Document\Share;

use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Form\Fieldset;

/**
 */
class PublicUrlFieldset extends Fieldset implements InputFilterProviderInterface
{

	/**
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 * Count number of documents to version
	 * @var integer
	 */
	public $count = 0;

	/**
	 * List of document ids to versionned
	 * @var array
	 */
	public $documentIds = array();

	/**
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('publicShareForm');

		$this->template = 'ged/document/share/publicurlfieldset';

		$this->setAttribute('method', 'post')
			->setAttribute('id', 'publicurlForm')
			->setHydrator(new Hydrator(false))
			->setObject(new \Rbs\Ged\Document\Share\PublicUrl());

		/* Hiddens */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'uid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Link url*/
		$this->add(array(
			'name' => 'url',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'http://server.domain.ext',
				'class' => 'form-control',
				'size' => 80,
				'readonly' => true
			),
			'options' => array(
				'label' => tra('Public Url')
			)
		));

		/* Validity */
		$this->add(array(
			'name' => 'expirationDate',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control datepicker'
			),
			'options' => array(
				'label' => tra('Expiration Date')
			)
		));

		/* Comments */
		$this->add(array(
			'name' => 'comment',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'rows' => 5,
				'cols' => 80
			),
			'options' => array(
				'label' => tra('Comment')
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'spacename' => array(
				'required' => false
			),
			'documentId' => array(
				'required' => true
			),
			'id' => array(
				'required' => false
			),
			'uid' => array(
				'required' => false
			),
			'url' => array(
				'required' => false
			),
			'expirationDate' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'DateTimeFormatter',
						'options' => array(
							'format' => 'Y-m-d H:i:s'
						)
					)
				)
			),
			'comment' => array(
				'required' => false
			)
		);
	}
}
