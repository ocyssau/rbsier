<?php
namespace Ged\Form\Document\History\Batch;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @var string
	 */
	public $spacename;

	/**
	 * 
	 * @param \Rbs\Space\Factory $factory
	 * @param string $name
	 */
	public function __construct($factory = null, $name = null)
	{
		parent::__construct($factory, $name);

		$this->template = 'ged/document/history/batch/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$dao = $factory->getDao(\Rbs\Ged\Document\History\Batch::$classId);
		$this->dao = $dao;

		/* Hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'type' => 'Zend\Form\Element\Hidden'
		));
		$this->add(array(
			'name' => 'documentid',
			'type' => 'Zend\Form\Element\Hidden'
		));
		$inputFilter->add(array(
			'name' => 'spacename',
			'required' => false
		));
		$inputFilter->add(array(
			'name' => 'documentid',
			'required' => false
		));

		/* Magic */
		$this->add(array(
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control',
				'title' => 'Search in action name and by, uid'
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_magic',
			'filters' => array(
				array(
					'name' => 'StringTrim'
				)
			),
			'required' => false
		));

		/* Advanced search CheckBox */
		$this->add(array(
			'name' => 'f_adv_search_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Advanced'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_adv_search_cb',
			'required' => false
		));

		/* Action date */
		$this->add(array(
			'name' => 'f_action_date_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Action date'
			)
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_action_date_min',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Superior to'
			)
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_action_date_max',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Inferior to'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_action_date_cb',
			'required' => false
		));
		$inputFilter->add(array(
			'name' => 'f_action_date_min',
			'required' => false
		));
		$inputFilter->add(array(
			'name' => 'f_action_date_max',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();
		/* @var \Rbs\Space\Workitem\Document\HistoryDao $dao */
		$dao = $this->dao;
		$translator = $dao->getTranslator();

		/* MAGIC */
		if ( isset($datas['find_magic']) && $datas['find_magic'] ) {
			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, `%s`, '-')", $translator->toSys('callbackMethod'), $translator->toSys('ownerUid'), $translator->toSys('uid'));
			$filter->andFind($datas['find_magic'], $concat, Op::OP_CONTAINS);
		}

		/* ACTION DATE */
		if ( $datas['f_action_date_cb'] ) {
			if ( $datas['f_action_date_min'] ) {
				$filter->andFind($translator->datetimeToSys($datas['f_action_date_min']), $translator->toSys('created'), Op::OP_SUP);
			}
			if ( $datas['f_action_date_max'] ) {
				$filter->andFind($translator->datetimeToSys($datas['f_action_date_max']), $translator->toSys('created'), Op::OP_INF);
			}
		}
		return $this;
	}
} /* End of class */
