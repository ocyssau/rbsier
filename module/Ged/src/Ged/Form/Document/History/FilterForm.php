<?php
namespace Ged\Form\Document\History;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @var string
	 */
	public $spacename;

	/**
	 * @var \Rbs\History\HistoryDao
	 */
	public $dao;

	/**
	 * 
	 * @param \Rbs\Space\Factory $factory
	 * @param string $name
	 */
	public function __construct($factory = null, $name = null)
	{
		parent::__construct($factory, $name);

		$this->template = 'ged/document/history/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$dao = $factory->getDao(\Rbs\Ged\Document\History::$classId);
		$this->dao = $dao;

		/* Hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'type' => 'Zend\Form\Element\Hidden'
		));
		$this->add(array(
			'name' => 'documentid',
			'type' => 'Zend\Form\Element\Hidden'
		));
		$inputFilter->add(array(
			'name' => 'spacename',
			'required' => false
		));
		$inputFilter->add(array(
			'name' => 'documentid',
			'required' => false
		));

		/* Magic */
		$this->add(array(
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control',
				'title' => 'Search in action name and by, document number, description'
			),
			'options' => array(
				'label' => 'Document Number'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_magic',
			'filters' => array(
				array(
					'name' => 'StringTrim'
				)
			),
			'required' => false
		));

		/* Find what */
		$this->add(array(
			'name' => 'find',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Find',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Find'
			)
		));
		$inputFilter->add(array(
			'name' => 'find',
			'required' => false
		));

		/* Find in field */
		$this->add(array(
			'name' => 'find_in',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'In',
				'class' => 'form-control rb-select',
				'size' => 1,
				'multiple' => false
			),
			'options' => array(
				'label' => 'In',
				'disable_inarray_validator' => true,
				'value_options' => array(
					null => 'In...',
					$dao->toSys('data_name') => 'Number',
					$dao->toSys('data_status') => 'State',
					$dao->toSys('data_indice') => 'Indice'
				)
			)
		));
		$inputFilter->add(array(
			'name' => 'find_in',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'ToNull'
				)
			)
		));

		/* Advanced search CheckBox */
		$this->add(array(
			'name' => 'f_adv_search_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Advanced'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_adv_search_cb',
			'required' => false
		));

		/* Action date */
		$this->add(array(
			'name' => 'f_action_date_cb',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control optionSelector'
			),
			'options' => array(
				'label' => 'Action date'
			)
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_action_date_min',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Superior to'
			)
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_action_date_max',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Inferior to'
			)
		));
		$inputFilter->add(array(
			'name' => 'f_action_date_cb',
			'required' => false
		));
		$inputFilter->add(array(
			'name' => 'f_action_date_min',
			'required' => false
		));
		$inputFilter->add(array(
			'name' => 'f_action_date_max',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();
		/* @var \Rbs\Space\Workitem\Document\HistoryDao $dao */
		$dao = $this->dao;
		$translator = $dao->getTranslator();
		$table = $this->dao->getTable();
		$alias = $table;

		/* MAGIC */
		if ( isset($datas['find_magic']) && $datas['find_magic'] ) {
			$concat = sprintf("CONCAT_WS('-', %s, %s, %s, %s, '-')", $alias . '.' . $translator->toSys('action_name'), $alias . '.' . $translator->toSys('action_ownerUid'), $alias . '.' . $translator->toSys('data_number'), $alias . '.' . $translator->toSys('data_description'));
			$filter->andFind($datas['find_magic'], $concat, Op::OP_CONTAINS);
		}

		/* ADVANCED SEARCH */
		if ( $datas['f_adv_search_cb'] ) {
			/* FIND IN */
			if ( $datas['find'] && $datas['find_in'] ) {
				$filter->andFind($datas['find'], $datas['find_in'], Op::CONTAINS);
			}

			/* ACTION DATE */
			if ( $datas['f_action_date_cb'] ) {
				if ( $datas['f_action_date_min'] ) {
					if ( $datas['f_action_date_min'] ) {
						$filter->andFind($translator->datetimeToSys($datas['f_action_date_min']), $translator->toSys('action_created'), Op::OP_SUP);
					}
					if ( $datas['f_action_date_max'] ) {
						$filter->andFind($translator->datetimeToSys($datas['f_action_date_max']), $translator->toSys('action_created'), Op::OP_INF);
					}
				}
			}
		}

		return $this;
	}
} /* End of class */
