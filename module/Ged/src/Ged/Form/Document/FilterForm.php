<?php
namespace Ged\Form\Document;

use Rbplm\Dao\Filter\Op;
use Rbs\Dao\Sier\Filter as DaoFilter;
use Application\Form\AbstractFilterForm;
use Rbplm\People;
use Rbplm\Ged\Document;
use Rbplm\Ged\AccessCode;

/**
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 *
	 * @var array
	 */
	public $extended;

	/**
	 * @param \Rbs\Space\Factory $factory
	 * @param integer $containerId
	 */
	public function __construct($factory = null, $containerId, $sessionId)
	{
		parent::__construct($factory, $sessionId);

		/* */
		$this->template = 'ged/document/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$elemtFactory = $this->getElementFactory();

		/* Spacename */
		$this->add(array(
			'name' => 'spacename',
			'type' => 'Zend\Form\Element\Hidden',
			'attributes' => array(
				'value' => strtolower($factory->getName())
			)
		));

		/* Document id */
		$this->add(array(
			'name' => 'documentid',
			'type' => 'Zend\Form\Element\Hidden'
		));

		/* Container id */
		$this->add(array(
			'name' => 'containerid',
			'type' => 'Zend\Form\Element\Hidden',
			'attributes' => array(
				'value' => $containerId
			)
		));

		/* Magic */
		$elemtFactory->magicElement();

		/* Number */
		$elemtFactory->numberElement();

		/* Designation */
		$elemtFactory->designationElement();

		/* Tags */
		$this->add(array(
			'name' => 'find_tags',
			'type' => 'Application\Form\Element\SelectTag',
			'attributes' => array(
				'placeholder' => tra('Search for tags...'),
				'class' => 'bootstrap-select selectpicker rb-select form-control',
				'title' => 'select tags',
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1',
				'data-size' => 10,
				'multiple' => true
			),
			'options' => array(
				'label' => tra('Tags'),
				'mode' => 'usedbycontainer',
				'daoFactory' => $this->daoFactory,
				'containerId' => $containerId
			)
		));
		$inputFilter->add(array(
			'name' => 'find_tags',
			'required' => false
		));

		/* Category */
		$this->add(array(
			'name' => 'find_category',
			'type' => 'Application\Form\Element\SelectCategory',
			'attributes' => array(
				'placeholder' => tra('Search for category...'),
				'multiple' => true,
				'class' => 'bootstrap-select selectpicker rb-select form-control rb-select-category',
				'title' => 'select category',
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1',
				'data-size' => 10,
				'multiple' => true
			),
			'options' => array(
				'label' => tra('Category'),
				'mode' => 'usedbycontainer',
				'daoFactory' => $this->daoFactory,
				'containerId' => $containerId
			)
		));
		$inputFilter->add(array(
			'name' => 'find_category',
			'required' => false
		));

		/* Doctype */
		$this->add(array(
			'name' => 'find_doctype',
			'type' => 'Application\Form\Element\SelectDoctype',
			'attributes' => array(
				'placeholder' => tra('Search for doctype...'),
				'class' => 'bootstrap-select selectpicker rb-select form-control rb-select-doctype',
				'title' => 'select doctype',
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1',
				'data-size' => 10,
				'multiple' => true
			),
			'options' => array(
				'label' => tra('Doctype'),
				'mode' => 'usedbycontainer',
				'daoFactory' => $this->daoFactory,
				'containerId' => $containerId
			)
		));
		$inputFilter->add(array(
			'name' => 'find_doctype',
			'required' => false
		));

		/* Life Stage */
		$this->add(array(
			'name' => 'find_state',
			'type' => 'Application\Form\Element\SelectLifestage',
			'attributes' => array(
				'placeholder' => tra('Life Stage Status'),
				'class' => 'bootstrap-select selectpicker rb-select form-control rb-select-lifestage',
				'title' => 'select lifestage',
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1',
				'data-size' => 10,
				'multiple' => true
			),
			'options' => array(
				'label' => tra('Status'),
				'size' => 8,
				'value_options' => [
					'' => '...'
				],
				'mode' => 'usedbycontainer',
				'daoFactory' => $this->daoFactory,
				'containerId' => $containerId,
				'maybenull' => true
			)
		));
		$inputFilter->add(array(
			'name' => 'find_state',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StringTrim'
				)
			)
		));

		/* Access Code */
		$this->add(array(
			'name' => 'find_access_code',
			'type' => 'Application\Form\Element\SelectAccess',
			'attributes' => array(
				'multiple' => true,
				'data-live-search' => false,
				'data-selected-text-format' => 'count > 1'
			),
			'options' => array(
				'label' => tra('Access')
			)
		));
		$inputFilter->add(array(
			'name' => 'find_access_code',
			'required' => false
		));
		$this->get('find_access_code')->setAttribute('data-selected-text-format', 'count > 1');

		/* displayTrash */
		$this->add(array(
			'name' => 'displayTrash',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control submitOnClick resetOnClick'
			),
			'options' => array(
				'label' => tra('Display trash')
			)
		));
		$inputFilter->add(array(
			'name' => 'displayTrash',
			'required' => false
		));

		/* onlyMy */
		$this->add(array(
			'name' => 'onlyMy',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control submitOnClick resetOnClick'
			),
			'options' => array(
				'label' => tra('My documents only')
			)
		));
		$inputFilter->add(array(
			'name' => 'onlyMy',
			'required' => false
		));

		/* checkByMe */
		$this->add(array(
			'name' => 'checkByMe',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control submitOnChange resetOnChange'
			),
			'options' => array(
				'label' => tra('Checkouted by me')
			)
		));
		$inputFilter->add(array(
			'name' => 'checkByMe',
			'required' => false
		));

		/* displayThumbs */
		$this->add(array(
			'name' => 'displayThumbs',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control submitOnClick'
			),
			'options' => array(
				'label' => tra('Display thumbnails')
			)
		));
		$inputFilter->add(array(
			'name' => 'displayHistory',
			'required' => false
		));

		/* withHistory */
		$this->add(array(
			'name' => 'withHistory',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control submitOnClick'
			),
			'options' => array(
				'label' => tra('With Previous Versions')
			)
		));
		$inputFilter->add(array(
			'name' => 'withHistory',
			'required' => false
		));

		/* Find what */
		$elemtFactory->findInElement([
			'version' => tra('Version'),
			'iteration' => tra('Iteration')
		]);

		/* Select action */
		$elemtFactory->userActionElement([
			'lock_by_id' => 'Checkout By',
			'update_by_id' => 'Update By',
			'create_by_id' => 'Created By'
		]);

		/* Advanced search CheckBox */
		$elemtFactory->advancedCbElement();

		/* DateAndTime CheckBox */
		$elemtFactory->dateAndTimeCbElement();

		/* CheckOut date */
		$elemtFactory->dateAndTimeElement('check_out', 'CheckOut Date');

		/* Update date selector */
		$elemtFactory->dateAndTimeElement('update', 'Update Date');

		/* Open date */
		$elemtFactory->dateAndTimeElement('open', 'Open Date');

		/* Close date */
		$elemtFactory->dateAndTimeElement('close', 'Close Date');

		/* FsClose date */
		$elemtFactory->dateAndTimeElement('fsclose', 'Forseen Close Date');
	}

	/**
	 *
	 * @param array $optionalFields        	
	 * @return FilterForm
	 */
	public function setExtended(array $optionalFields)
	{
		$this->extended = $optionalFields;
		$element = $this->get('find_field');
		$options = $element->getValueOptions();
		foreach( $optionalFields as $val ) {
			$options[$val['name']] = $val['label'];
		}
		$element->setValueOptions($options);
		return $this;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(Document\Version::$classId);
		$me = People\CurrentUser::get()->getid();
		$this->prepare()->isValid();
		$datas = $this->getData();
		$alias = $filter->getAlias();
		($alias) ? $alias = $alias . '.' : $alias = null;

		/* NORMALS OPTIONS */
		// ONLY ME
		if ( $datas['onlyMy'] ) {
			$subFilter = new DaoFilter('', false);
			$subFilter->orFind($me, $alias . $dao->toSys('lockById'), Op::EQUAL);
			$subFilter->orFind($me, $alias . $dao->toSys('updateById'), Op::EQUAL);
			$subFilter->orFind($me, $alias . $dao->toSys('createById'), Op::EQUAL);
			$filter->subor($subFilter);
		}

		// CHECKOUT BY ME
		if ( !empty($datas['checkByMe']) ) {
			$filter->andFind($me, $alias . $dao->toSys('lockById'), Op::EQUAL);
			$filter->andFind(AccessCode::CHECKOUT, $alias . $dao->toSys('accessCode'), Op::EQUAL);
		}

		// DISPLAY TRASH
		if ( isset($datas['displayTrash']) && $datas['displayTrash'] == 1 ) {
			$filter->andfind(AccessCode::SUPPRESS, $alias . $dao->toSys('accessCode'), Op::EQUAL);
		}
		else {
			$filter->andfind(AccessCode::SUPPRESS, $alias . $dao->toSys('accessCode'), Op::NOTEQUAL);
		}

		// DISPLAY THUMBNAILS
		if ( isset($datas['displayThumbs']) && $datas['displayThumbs'] == true ) {}

		// NUMBER
		if ( !empty($datas['find_number']) ) {
			$what = $datas['find_number'];
			$what = str_replace('  ', ' ', $what);

			if ( stristr($what, '*') ) {
				$what = str_replace('*', '%', $what);
			}
			else {
				$what = '%' . $what . '%';
			}

			$filter->andFind($what, $alias . $dao->toSys('number'), Op::LIKE);
		}

		// DESIGNATION
		if ( !empty($datas['find_designation']) ) {
			$what = $datas['find_designation'];
			$what = str_replace('  ', ' ', $what);

			if ( stristr($what, '*') ) {
				$what = str_replace('*', '%', $what);
			}
			else {
				$what = '%' . $what . '%';
			}

			$filter->andFind($what, $alias . $dao->toSys('description'), Op::LIKE);
		}

		// MAGIC
		if ( !empty($datas['find_magic']) ) {
			$what = $datas['find_magic'];
			$what = str_replace('  ', ' ', $what);
			$what = str_replace('*', '%', $what);
			$whats = explode(' AND ', $what);

			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, `%s`, `%s`, '-')", $dao->toSys('id'), $dao->toSys('name'), $dao->toSys('description'), $dao->toSys('number'));
			foreach( $whats as $what ) {
				$filter->andFind($what, $concat, Op::LIKE);
			}
		}

		// Access code
		if ( is_array($datas['find_access_code']) ) {
			$subFilter = new DaoFilter('', false);
			foreach( $datas['find_access_code'] as $acode ) {
				$acode = (int)$acode;
				if ( $acode == 20 ) {
					$subFilter->orFind($acode, $alias . $dao->toSys('accessCode'), Op::EQUALSUP);
					continue;
				}

				if ( $acode == 'free' ) {
					$acode = 0;
				}
				$subFilter->orFind($acode, $alias . $dao->toSys('accessCode'), Op::EQUAL);
			}
			// WITH HISTORY
			if ( isset($datas['withHistory']) && $datas['withHistory'] == 1 ) {
				$subFilter->orFind(AccessCode::HISTORY, $alias . $dao->toSys('accessCode'), Op::EQUAL);
			}
			$filter->subor($subFilter);
		}
		else {
			// WITH HISTORY
			if ( !isset($datas['withHistory']) || $datas['withHistory'] == 0 ) {
				$filter->andfind(AccessCode::HISTORY, $alias . $dao->toSys('accessCode'), Op::NOTEQUAL);
			}
		}

		// LifeStage
		if ( is_array($datas['find_state']) ) {
			$subFilter = new DaoFilter('', false);
			$asSys = $dao->toSys('lifeStage');
			foreach( $datas['find_state'] as $lifeStage ) {
				if ( $lifeStage ) {
					$subFilter->orFind($lifeStage, $alias . $asSys, Op::EQUAL);
				}
			}
			$filter->subor($subFilter);
		}

		// Doctype plain
		if ( !empty($datas['find_doctype_plain']) ) {
			$filter->with(array(
				'table' => 'doctypes',
				'lefton' => 'doctype_id',
				'righton' => 'id',
				'alias' => 'doctype',
				'select' => array(
					'number'
				),
				'direction' => 'outer'
			));
			$what = $datas['find_doctype'];
			$filter->andFind($what, 'doctype.number', Op::CONTAINS);
		}

		// Doctype select
		if ( is_array($datas['find_doctype']) ) {
			$subFilter = new DaoFilter('', false);
			foreach( $datas['find_doctype'] as $dtId ) {
				if ( $dtId ) {
					$subFilter->orFind($dtId, $alias . $dao->toSys('doctypeId'), Op::EQUAL);
				}
			}
			$filter->subor($subFilter);
		}

		// Category
		if ( is_array($datas['find_category']) ) {
			$subFilter = new DaoFilter('', false);
			foreach( $datas['find_category'] as $cat ) {
				if ( $cat ) {
					$subFilter->orFind($cat, $alias . $dao->toSys('categoryId'), Op::EQUAL);
				}
			}
			$filter->subor($subFilter);
		}

		// Tags
		if ( is_array($datas['find_tags']) ) {
			$subFilter = new DaoFilter('', false);
			$toSys = $dao->toSys('tags');
			foreach( $datas['find_tags'] as $tag ) {
				$tag = trim($tag);
				if ( $tag ) {
					$subFilter->orFind($tag, $alias . $toSys, Op::FINDINSET);
				}
			}
			$filter->subor($subFilter);
		}

		// ADVANCED SEARCH
		if ( $datas['f_adv_search_cb'] ) {
			// FIND IN
			if ( $datas['find'] && $datas['find_field'] ) {
				$filter->andFind($datas['find'], $alias . $datas['find_field'], Op::CONTAINS);
			}

			// ACTION USER
			if ( $datas['f_action_field'] ) {
				if ( $datas['f_action_user_name'] == '' ) {
					$filter->andFind($datas['f_action_user_name'], $alias . $datas['f_action_field'], Op::NOTNULL);
				}
				else {
					$filter->andFind($datas['f_action_user_name'], $alias . $datas['f_action_field'], Op::EQUAL);
				}
			}

			// DATE AND TIME
			if ( $datas['f_dateAndTime_cb'] ) {
				// CHECKOUT
				if ( $datas['f_check_out_date_cb'] ) {
					if ( $datas['f_check_out_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_check_out_date_min']), $alias . $dao->toSys('locked'), Op::SUP);
						$filter->andFind(AccessCode::CHECKOUT, $alias . $dao->toSys('accessCode'), Op::EQUAL);
					}
					if ( $datas['f_check_out_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_check_out_date_min']), $alias . $dao->toSys('locked'), Op::INF);
						$filter->andFind(AccessCode::CHECKOUT, $alias . $dao->toSys('accessCode'), Op::EQUAL);
					}
				}
				// UPDATE
				if ( $datas['f_update_date_cb'] ) {
					if ( $datas['f_update_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_update_date_min']), $alias . $dao->toSys('updated'), Op::SUP);
					}
					if ( $datas['f_update_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_update_date_max']), $alias . $dao->toSys('updated'), Op::INF);
					}
				}
				// OPEN
				if ( $datas['f_open_date_cb'] ) {
					if ( $datas['f_open_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_open_date_min']), $alias . $dao->toSys('created'), Op::SUP);
					}
					if ( $datas['f_open_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_open_date_max']), $alias . $dao->toSys('created'), Op::INF);
					}
				}
				// CLOSE
				if ( $datas['f_close_date_cb'] ) {
					if ( $datas['f_close_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_close_date_min']), $alias . $dao->toSys('closed'), Op::SUP);
					}
					if ( $datas['f_close_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_close_date_max']), $alias . $dao->toSys('closed'), Op::INF);
					}
				}
				// FORSEEN CLOSE
				if ( $datas['f_fsclose_date_cb'] ) {
					if ( $datas['f_fsclose_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_min']), 'planned_closure', Op::SUP);
					}
					if ( $datas['f_fsclose_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_max']), 'planned_closure', Op::INF);
					}
				}
			}
		}

		// EXTENDED
		if ( is_array($this->extended) ) {
			foreach( $this->extended as $val ) {
				$asApp = $val['appName'];
				if ( isset($datas[$asApp]) ) {
					$what = trim($datas[$asApp]);
					$filter->andFind($what, $alias . $asApp, Op::CONTAINS);
				}
			}
		}

		return $this;
	}
}
