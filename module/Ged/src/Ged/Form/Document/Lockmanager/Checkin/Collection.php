<?php
// %LICENCE_HEADER%
namespace Ged\Form\Document\Lockmanager\Checkin;

/**
 */
class Collection implements \Countable
{

	/**
	 *
	 * @var array
	 */
	protected $documents = [];

	public $checkinandkeep;

	public $comment;

	public $spacename;

	/**
	 *
	 * @param array $properties
	 */
	public function populate(array $properties)
	{
		(isset($properties['documents'])) ? $this->documents = $properties['documents'] : null;
		(isset($properties['checkinandkeep'])) ? $this->checkinandkeep = $properties['checkinandkeep'] : null;
		(isset($properties['comment'])) ? $this->comment = $properties['comment'] : null;
		(isset($properties['spacename'])) ? $this->spacename = $properties['spacename'] : null;
		return $this;
	}

	/**
	 *
	 * @param array $properties
	 */
	public function getArrayCopy()
	{
		return [
			'documents' => $this->documents,
			'checkinandkeep' => $this->checkinandkeep,
			'comment' => $this->comment,
			'spacename' => $this->spacename
		];
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $doc
	 */
	public function add($doc)
	{
		$this->documents[$doc->getId()] = $doc;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function count()
	{
		return count($this->documents);
	}

	/**
	 * @return array
	 */
	public function getIterator()
	{
		return $this->documents;
	}
}
