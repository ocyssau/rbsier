<?php
namespace Ged\Form\Document\Lockmanager\Checkin;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;

/**
 */
class CheckinForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @param string $name
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('checkinDocument');
		$this->template = 'ged/document/lockmanager/checkinform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter())
			->setObject(new Collection());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Comments */
		$this->add(array(
			'name' => 'comment',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'textarea',
				'placeholder' => '',
				'class' => 'form-control',
				'rows' => 5,
				'cols' => 50
			),
			'options' => array(
				'label' => tra('Comment')
			)
		));

		/* Keep */
		$this->add(array(
			'name' => 'checkinandkeep',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Checkin and keep')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));

		/* Prepare data button */
		$this->add(array(
			'name' => 'preparedata',
			'attributes' => array(
				'type' => 'button',
				'value' => tra('Prepare Data'),
				'id' => 'databutton',
				'class' => 'btn btn-info'
			)
		));

		/* Set files collection */
		$documentFieldset = new DocumentFieldset('documents');
		$documentFieldset->setUseAsBaseFieldset(true);
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'documents',
			'options' => array(
				'should_create_template' => false,
				'allow_add' => true,
				'target_element' => $documentFieldset,
				'use_as_base_fieldset'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'containerId' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			),
			'files' => array(
				'required' => false
			),
			'comment' => array(
				'required' => true
			)
		);
	}
}
