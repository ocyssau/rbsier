<?php
namespace Ged\Form\Document\Lockmanager\Checkout;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class CheckoutForm extends Form implements InputFilterProviderInterface
{

	/**
	 *
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	public $documentIds = [];

	public $docfileIds = [];

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('checkoutDocument');

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$this->template = 'ged/document/lockmanager/checkoutform';

		/* init list of permit actions */
		$this->selectSet = array(
			'keep' => tra('Keep file in wildspace'),
			'replace' => tra('Replace file in wildspace'),
			'ignore' => tra('Dont Checkout')
		);

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'documentid' => [
				'required' => false
			],
			'docfileid' => [
				'required' => false
			],
			'spacename' => [
				'required' => false
			],
			'spacenames' => [
				'required' => false
			],
			'checked' => [
				'required' => false
			]
		);
	}

	/**
	 * Parent document of docfile must be set before
	 *
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 * @return CheckoutForm
	 */
	public function addToConfirm($docfile)
	{
		$document = $docfile->getParent();
		$label = $document->getUid() . ' v' . $document->version . '.' . $document->iteration;
		$id = $document->getId();
		$spacename = $document->spacename;

		if ( !in_array($id, $this->documentIds) ) {
			$this->documentIds[] = $id;

			/* Add hidden fields */
			$this->add(array(
				'name' => 'checked[' . $id . ']',
				'attributes' => array(
					'type' => 'hidden',
					'value' => $id
				)
			));

			/* Add hidden fields */
			$this->add(array(
				'name' => 'spacenames[' . $id . ']',
				'attributes' => array(
					'type' => 'hidden',
					'value' => $spacename
				)
			));

			/* select action */
			$fileName = $docfile->getName();
			$message = sprintf(tra('What do want to do with file %s of document %s?'), $fileName, $label);
			$this->add(array(
				'name' => 'ifExistmode[' . $id . ']',
				'type' => 'Zend\Form\Element\Select',
				'attributes' => array(
					'class' => 'form-control',
					'id' => 'ifExistmode[' . $id . ']'
				),
				'options' => array(
					'label' => $message,
					'options' => $this->selectSet
				)
			));
		}
		return $this;
	}

	/**
	 * Document not require confirmation
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return CheckoutForm
	 */
	public function addToCo($document)
	{
		$id = $document->getId();
		$spacename = $document->spacename;

		if ( !in_array($id, $this->documentIds) ) {
			$this->documentIds[] = $id;
			/* Add hidden fields */
			$this->add(array(
				'name' => 'checked[' . $id . ']',
				'attributes' => array(
					'type' => 'hidden',
					'value' => $id
				)
			));

			/* Add hidden fields */
			$this->add(array(
				'name' => 'spacenames[' . $id . ']',
				'attributes' => array(
					'type' => 'hidden',
					'value' => $spacename
				)
			));
		}

		return $this;
	}
}
