<?php
namespace Ged\Form\Document\Lockmanager\Lock;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Rbplm\Ged\AccessCode;

/**
 */
class LockForm extends Form implements InputFilterProviderInterface
{

	/**
	 * Collection of documents
	 * @var array
	 */
	public $documents = array();

	/**
	 *
	 * @param \Rbs\Space\Factory $factory        	
	 * @param \Application\View\ViewModel $view        	
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('lockDocument');

		/* init view var */
		$view->documentIds = array();
		$this->view = $view;

		$this->template = 'ged/document/lockmanager/lockform.phtml';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* access code set */
		$this->add(array(
			'name' => 'accessCode',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control',
				'id' => 'accessCode'
			),
			'options' => array(
				'label' => 'Lock Type',
				'options' => $this->getAccessCodeSet()
			)
		));

		/* Comments */
		$this->add(array(
			'name' => 'comment',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'textarea',
				'placeholder' => '',
				'class' => 'form-control',
				'rows' => 5,
				'cols' => 50
			),
			'options' => array(
				'label' => tra('Comment')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'accessCode' => array(
				'required' => true
			),
			'comment' => array(
				'required' => true
			)
		);
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getAccessCodeSet()
	{
		return array(
			11 => 'Locked (All others reasons)',
			6 => 'Canceled (The document is canceled)'
		);
	}

	/**
	 * Add a document to selection
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 */
	public function addDocument($document)
	{
		$id = $document->getId();
		$spacename = $document->spacename;
		$this->documents[] = $document;

		$label = $document->getNumber() . ' v' . $document->version . '.' . $document->iteration;
		$document->label = $label;

		$aCode = $document->checkAccess();
		if ( $aCode > AccessCode::FREE ) {
			throw new \Rbplm\Ged\AccessCodeException('Document % is not free');
		}

		/* recap list with checkbox */
		$elemtName = 'documentIds[' . $id . ']';
		$this->add(array(
			'name' => $elemtName,
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => [
				'class' => 'form-control tovalidate-cb',
				'checked' => true
			],
			'options' => [
				'label' => $label,
				'use_hidden_element' => false,
				'checked_value' => $id
			]
		));
		$this->getInputFilter()->add([
			'required' => false
		], $elemtName);

		/* Add hidden fields */
		$elemtName = 'spacenames[' . $id . ']';
		$this->add([
			'name' => $elemtName,
			'attributes' => [
				'type' => 'hidden',
				'value' => $spacename
			]
		]);
		$this->getInputFilter()->add([
			'required' => false
		], $elemtName);
	}
}
