<?php
namespace Ged\Form\Document\Datamanager;

use Rbplm\Ged\Docfile\Role;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class AssociateFileForm extends Form implements InputFilterProviderInterface
{

	/**
	 * 
	 * @param \Rbplm\People\User\Wildspace $wildspace
	 * @param \Rbplm\Dao\FilterAbstract $filter
	 */
	public function __construct($wildspace, $filter = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('associateFile');

		$this->template = 'ged/document/datamanager/associatefileform.phtml';
		$this->wildspace = $wildspace;

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* ROLES */
		$selectSet = Role::toArray();
		$this->add(array(
			'name' => 'mainrole',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control selectpicker',
				'data-width' => '200px',
				'title' => tra('Selected role is applied on all files selected')
			),
			'options' => array(
				'label' => tra('Select Role'),
				'options' => $selectSet
			)
		));

		/* FILES */
		$path = $wildspace->getPath();
		$list = $wildspace::getDatas($path, $filter);
		$filesSet = array();
		foreach( $list as $file ) {
			$filesSet[$file['name']] = $file['name'];
		}
		$this->add(array(
			'name' => 'file',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control selectpicker rb-select-file',
				'data-width' => '400px',
				'title' => tra('You can select one or many files to associate to document. Only valid candidates are displayed'),
				'multiple' => true
			),
			'options' => array(
				'label' => tra('Select Files'),
				'options' => $filesSet
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'documentid' => array(
				'required' => false
			),
			'file' => array(
				'required' => false
			),
			'mainrole' => array(
				'required' => false
			)
		);
	}
}
