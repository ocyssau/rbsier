<?php
namespace Ged\Form\Document\Datamanager;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class MoveForm extends Form implements InputFilterProviderInterface
{

	/**
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('moveDocument');

		$this->template = 'ged/document/datamanager/movedocumentform.phtml';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		//TARGET CONTAINER
		$this->add(array(
			'name' => 'tocontainerid',
			'type' => 'Application\Form\Element\SelectContainer',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Target Container'),
				'daoFactory' => $factory
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'checked' => array(
				'required' => false
			),
			'tocontainerid' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			)
		);
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return \Ged\Form\Document\Datamanager\MoveForm
	 */
	public function addDocument($document)
	{
		$documentId = $document->getId();
		$i = $documentId;

		/* recap list with checkbox */
		$label = $document->getNumber() . ' - v' . $document->version;
		$this->add(array(
			'name' => 'checked[' . $i . ']',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control',
				'checked' => true
			),
			'options' => array(
				'label' => $label,
				'use_hidden_element' => false,
				'checked_value' => $documentId
			)
		));

		$this->documentIds[] = $documentId;
		return $this;
	}
}

