<?php
namespace Ged\Form\Document\Datamanager;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class CopyForm extends Form implements InputFilterProviderInterface
{

	/**
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('copyDocument');

		$this->template = 'ged/document/datamanager/copydocumentform.phtml';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* TARGET CONTAINER */
		$this->add(array(
			'name' => 'tocontainerid',
			'type' => 'Application\Form\Element\SelectContainer',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Target Container'),
				'daoFactory' => $factory
			)
		));

		/* NEW NAME */
		$this->add(array(
			'name' => 'newname',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('New name')
			)
		));

		/* VERSION */
		$this->add(array(
			'name' => 'version',
			'type' => 'Application\Form\Element\SelectVersion',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Version')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'button',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'button',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'documentid' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			),
			'newname' => array(
				'required' => true
			),
			'tocontainerid' => array(
				'required' => false
			),
			'version' => array(
				'required' => false
			)
		);
	}
}

