<?php
namespace Ged\Form\Document\Read;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;

/**
 */
class ReadForm extends AbstractForm implements InputFilterProviderInterface
{

	const MODE_MAIN = 'main';

	const MODE_VISU = 'visu';

	const MODE_ALL = 'all';

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @param string $name
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('readDocument');
		$this->template = 'ged/document/read/form';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter())
			->setObject(new Collection());

		/* withVersion */
		$this->add(array(
			'name' => 'withVersion',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('With iteration and version suffix')
			)
		));

		/* Put in wildspace or download */
		$this->add(array(
			'name' => 'putInWilspace',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Put In Wildspace')
			)
		));

		/* getMode */
		$this->add(array(
			'name' => 'getmode',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Get files mode'),
				'value_options' => [
					self::MODE_MAIN => 'Main file only',
					self::MODE_VISU => 'Visualisation file only, or if not, main file',
					self::MODE_ALL => 'All files'
				]
			)
		));

		/* CreateSubDirectory */
		$this->add(array(
			'name' => 'inSubDirectory',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Create and out in a Sub Directory')
			)
		));

		/* SubDirectoryName */
		$this->add(array(
			'name' => 'subDirectoryName',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Sub Directory Name')
			)
		));

		/* Download as Zip */
		$this->add(array(
			'name' => 'zip',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Download As Zipfile')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));

		/* Set files collection */
		$documentFieldset = new DocumentFieldset('documents');
		$documentFieldset->setUseAsBaseFieldset(true);
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'documents',
			'options' => array(
				'should_create_template' => false,
				'allow_add' => true,
				'target_element' => $documentFieldset,
				'use_as_base_fieldset'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'withVersion' => array(
				'required' => false
			),
			'putInWilspace' => array(
				'required' => false
			),
			'getmode' => array(
				'required' => false
			),
			'inSubDirectory' => array(
				'required' => false
			),
			'subDirectoryName' => array(
				'required' => false,
				'validators' => [
					[
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/[A-Za-z0-9_-]?/i',
							'messages' => array(
								\Zend\Validator\Regex::INVALID => 'Unvalid name for folder'
							)
						)
					]
				]
			),
			'zip' => array(
				'required' => false
			)
		);
	}
}
