<?php
namespace Ged\Form\Document\Read;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ObjectProperty as Hydrator;

/**
 *
 *
 */
class DocumentFieldset extends Fieldset implements InputFilterProviderInterface
{
	use \Application\Form\ExtendableTrait;
	use \Application\Form\InputFilterProviderTrait;

	/**
	 *
	 * @param string $name
	 * @param \Rbs\Space\Factory $daoFactory
	 */
	public function __construct($name)
	{
		/* we want to ignore the name passed */
		parent::__construct($name);

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setObject(new \Rbplm\Ged\Document\Version());

		/* hidden */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->inputFilterSpecification = array(
			'id' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			)
		);

		/* Set files collection */
		/*
		 $docfileFieldset = new DocfileFieldset('docfileForm', $daoFactory);
		 $docfileFieldset->setName('docfiles');
		 $this->add($docfileFieldset);
		 */

		$docfileFieldset = new DocfileFieldset('docfiles');
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'docfiles',
			'options' => array(
				'should_create_template' => false,
				'allow_add' => true,
				'target_element' => $docfileFieldset
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'class' => 'form-control duplicable description',
				'size' => 32,
				'rows' => 2,
				'cols' => 32
			),
			'options' => array(
				'label' => tra('Description')
			)
		));
		$this->inputFilterSpecification['description'] = array(
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StripTags'
				),
				array(
					'name' => 'StringTrim'
				)
			),
			'validators' => array(
				array(
					'name' => 'StringLength',
					'options' => array(
						'encoding' => 'UTF-8',
						'min' => 1,
						'max' => 512
					)
				)
			)
		);

		/* Number */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control name',
				'size' => 32,
				'readonly' => true
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Category */
		/*
		 $this->add(array(
		 'name' => 'categoryId',
		 'type' => 'Application\Form\Element\SelectCategory',
		 'attributes' => array(
		 'class' => 'form-control duplicable category',
		 'multiple' => false,
		 ),
		 'options' => array(
		 'label' => tra('Category'),
		 'daoFactory'=>$daoFactory,
		 'maybenull' => true,
		 )
		 ));
		 $this->inputFilterSpecification['categoryId'] = array(
		 'required' => false,
		 'filters' => array(
		 array(
		 'name' => 'StringTrim'
		 ),
		 array(
		 'name' => 'ToNull'
		 )
		 )
		 );
		 */

		/* Tags */
		$this->add(array(
			'name' => 'tags',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control rb-tagsinput',
				'size' => 16
			),
			'options' => array(
				'label' => tra('Tags')
			)
		));
		$this->inputFilterSpecification['tags'] = array(
			'required' => false,
			'filters' => array(
				array(
					'name' => 'StringTrim'
				),
				array(
					'name' => 'ToNull'
				)
			)
		);
	}
} /* End of class */
