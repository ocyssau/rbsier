<?php
// %LICENCE_HEADER%
namespace Ged\Form\Document\Read;

/**
 */
class Collection implements \Countable
{

	/**
	 *
	 * @var array
	 */
	protected $documents = [];

	public $withVersion = true;

	public $putInWilspace = false;

	public $getmode;

	public $inSubDirectory = false;

	public $subDirectoryName;

	public $zip = false;

	/**
	 *
	 * @param array $properties
	 */
	public function populate(array $properties)
	{
		(isset($properties['documents'])) ? $this->documents = $properties['documents'] : null;
		(isset($properties['withVersion'])) ? $this->withVersion = (bool)$properties['withVersion'] : null;
		(isset($properties['putInWilspace'])) ? $this->putInWilspace = (bool)$properties['putInWilspace'] : null;
		(isset($properties['getmode'])) ? $this->getmode = $properties['getmode'] : null;
		(isset($properties['inSubDirectory'])) ? $this->inSubDirectory = (bool)$properties['inSubDirectory'] : null;
		(isset($properties['subDirectoryName'])) ? $this->subDirectoryName = $properties['subDirectoryName'] : null;
		(isset($properties['zip'])) ? $this->zip = (bool)$properties['zip'] : null;
		return $this;
	}

	/**
	 *
	 * @param array $properties
	 */
	public function getArrayCopy()
	{
		return [
			'documents' => $this->documents,
			'withVersion' => $this->withVersion,
			'putInWilspace' => $this->putInWilspace,
			'getmode' => $this->getmode,
			'inSubDirectory' => $this->inSubDirectory,
			'subDirectoryName' => $this->subDirectoryName,
			'zip' => $this->zip
		];
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $doc
	 */
	public function add($doc)
	{
		$this->documents[$doc->getId()] = $doc;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function count()
	{
		return count($this->documents);
	}

	/**
	 * @return array
	 */
	public function getIterator()
	{
		return $this->documents;
	}
}
