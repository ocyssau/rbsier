<?php
namespace Ged\Form\Document\Read;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ObjectProperty as Hydrator;

/**
 *
 *
 */
class DocfileFieldset extends Fieldset implements InputFilterProviderInterface
{
	use \Application\Form\InputFilterProviderTrait;

	/**
	 *
	 * @param string $name
	 * @param \Rbs\Space\Factory $daoFactory
	 */
	public function __construct($name)
	{
		/* we want to ignore the name passed */
		parent::__construct($name);

		$obj = new \Rbplm\Ged\Docfile\Version();
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setObject($obj);

		/* hidden */
		$this->add([
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		]);

		/* File name */
		$this->add([
			'name' => 'name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'class' => 'form-control rb-fileName label',
				'size' => 32,
				'readonly' => true
			),
			'options' => array(
				'label' => tra('File')
			)
		]);

		$this->inputFilterSpecification = [
			'id' => array(
				'required' => false
			)
		];
	}
} /* End of class */
