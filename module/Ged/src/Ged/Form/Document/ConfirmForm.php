<?php
namespace Ged\Form\Document;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class ConfirmForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 * 
	 * @var integer
	 */
	public $count = 0;

	/**
	 *
	 * @var integer
	 */
	public $documentIds = [];

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 * @param \Application\View\ViewModel $view
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('confirmation');

		$this->template = 'ged/document/confirmform';
		$this->documentIds = [];

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'checked' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			)
		);
	}

	/**
	 * @param \Rbplm\Ged\Document\Version $document
	 */
	public function addDocument($document)
	{
		$label = $document->getName() . ' v' . $document->version . '.' . $document->iteration;
		$id = $document->getId();
		$this->documentIds[] = $id;

		/* recap list with checkbox */
		$this->add(array(
			'name' => 'checked[' . $id . ']',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control',
				'checked' => true
			),
			'options' => array(
				'label' => $label,
				'use_hidden_element' => false,
				'checked_value' => $id
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacenames[' . $id . ']',
			'attributes' => array(
				'type' => 'hidden',
				'value' => $document->spacename
			)
		));

		$this->count++;
	}
}
