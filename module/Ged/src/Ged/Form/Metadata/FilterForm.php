<?php
namespace Ged\Form\Metadata;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @param \Rbs\Space\Factory $factory
	 * @param string $nameSpace
	 */
	public function __construct($factory, $nameSpace)
	{
		parent::__construct($factory, $nameSpace);

		$this->template = 'ged/metadata/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$this->view->spacename = strtolower($factory->getName());

		/* Name */
		$this->add(array(
			'name' => 'find_name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for name...',
				'class' => 'form-control',
				'data-where' => 'name',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Name'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_name',
			'required' => false
		));

		/* Designation */
		$this->add(array(
			'name' => 'find_designation',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for designation...',
				'class' => 'form-control',
				'data-where' => 'description',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_designation',
			'required' => false
		));

		/* Label */
		$this->add(array(
			'name' => 'find_label',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for label...',
				'class' => 'form-control',
				'data-where' => 'label',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Label'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_label',
			'required' => false
		));

		/* Extendedcid */
		$selectSet = array(
			null => 'All Extended Class',
			'569e92709feb6' => 'Extend Document',
			'569e94192201a' => 'Extend Workitem'
		);
		$this->add(array(
			'name' => 'find_extendedcid',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control submitOnChange',
				'data-where' => 'extendedCid',
				'data-op' => Op::EQUAL
			),
			'options' => array(
				'label' => 'Extended Class',
				'value_options' => $selectSet
			)
		));
		$inputFilter->add(array(
			'name' => 'find_extendedcid',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(\Rbs\Extended\Property::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* NAME */
		if ( $datas['find_name'] ) {
			$filter->andFind($datas['find_name'], $dao->toSys('name'), Op::OP_CONTAINS);
		}

		/* DESIGNATION */
		if ( $datas['find_designation'] ) {
			$filter->andFind($datas['find_designation'], $dao->toSys('description'), Op::OP_CONTAINS);
		}

		/* LABEL */
		if ( $datas['find_label'] ) {
			$filter->andFind($datas['find_label'], $dao->toSys('label'), Op::OP_CONTAINS);
		}

		/* CLASS */
		if ( $datas['find_extendedcid'] ) {
			$filter->andFind($datas['find_extendedcid'], $dao->toSys('extendedCid'), Op::OP_EQUAL);
		}

		return $this;
	}
}
