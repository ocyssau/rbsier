<?php
namespace Ged\Form\Metadata;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/* */
	public $template;

	/* */
	public $ranchbe;

	/* */
	protected $inputFilter;

	/**
	 *
	 * @var array $optionalFields
	 */
	public $optionalFields = array();

	/**
	 *
	 * @var string edit | create
	 */
	public $mode;

	/**
	 *
	 * @var array $fieldTypes
	 */
	public $fieldTypes = array(
		'text',
		'longtext',
		'htmlarea',
		'partner',
		'doctype',
		'user',
		'version',
		'process',
		'category',
		'date',
		'integer',
		'decimal',
		'select',
		'selectFromDB',
		'liveSearch'
	);

	/**
	 *
	 * @var array $extendedCidSet
	 */
	public $extendedCidSet = array(
		'569e92709feb6' => 'Document',
		'569e94192201a' => 'Workitem'
	);

	/**
	 *
	 * @param \Application\View\ViewModel $view
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('metadataEdit');

		$this->template = 'ged/metadata/editform';
		$this->ranchbe = \Ranchbe::get();

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'layout',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'appName',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Label */
		$this->add(array(
			'name' => 'label',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Label')
			)
		));

		/* Type */
		$this->add(array(
			'name' => 'type',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Type'),
				'value_options' => array_combine($this->fieldTypes, $this->fieldTypes),
				'multiple' => false
			)
		));

		/* Extend Class */
		$this->add(array(
			'name' => 'extendedCid',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Extend Class'),
				'value_options' => $this->extendedCidSet,
				'multiple' => false
			)
		));

		/* Regex */
		$this->add(array(
			'name' => 'regex',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 40
			),
			'options' => array(
				'label' => tra('Regex')
			)
		));

		/* List */
		$this->add(array(
			'name' => 'list',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 40
			),
			'options' => array(
				'label' => tra('List')
			)
		));

		/* Required CB */
		$this->add(array(
			'name' => 'required',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control checkbox'
			),
			'options' => array(
				'label' => tra('Required')
			)
		));

		/* Multiple CB */
		$this->add(array(
			'name' => 'multiple',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control checkbox'
			),
			'options' => array(
				'label' => tra('Multiple')
			)
		));

		/* May be null */
		$this->add(array(
			'name' => 'maybenull',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control checkbox'
			),
			'options' => array(
				'label' => tra('May be null')
			)
		));

		/* Return Name CB */
		$this->add(array(
			'name' => 'returnName',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control checkbox'
			),
			'options' => array(
				'label' => tra('Return Name')
			)
		));

		/* Size */
		$this->add(array(
			'name' => 'size',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 10
			),
			'options' => array(
				'label' => tra('Size')
			)
		));

		/* Db Table Name */
		$this->add(array(
			'name' => 'dbtable',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 40
			),
			'options' => array(
				'label' => tra('Db Table Name')
			)
		));

		/* For Value */
		$this->add(array(
			'name' => 'forValue',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 40
			),
			'options' => array(
				'label' => tra('For Value')
			)
		));

		/* For Display */
		$this->add(array(
			'name' => 'forDisplay',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 40
			),
			'options' => array(
				'label' => tra('For Display')
			)
		));

		/* Date Format */
		$this->add(array(
			'name' => 'dateFormat',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 40
			),
			'options' => array(
				'label' => tra('Date Format')
			)
		));

		/* Min Value */
		$this->add(array(
			'name' => 'min',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 20
			),
			'options' => array(
				'label' => tra('Min Value')
			)
		));

		/* Max Value */
		$this->add(array(
			'name' => 'max',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 20
			),
			'options' => array(
				'label' => tra('Max Value')
			)
		));

		/* Step between value */
		$this->add(array(
			'name' => 'step',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 20
			),
			'options' => array(
				'label' => tra('Step between value')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => true
			),
			'size' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Digits',
						'options' => array(
							'messages' => array(
								\Zend\Validator\Digits::INVALID => 'Should be numeric'
							)
						)
					)
				)
			),
			'appName' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'messages' => array(
								\Zend\Validator\Regex::INVALID => 'Name should contains lower letters and/or _ only'
							),
							'pattern' => '/^[a-z_]+$/'
						)
					)
				)
			),
			'returnName' => array(
				'required' => false,
				'filter' => array(
					array(
						'name' => 'ToInt'
					)
				)
			),
			'maybenull' => array(
				'required' => false,
				'filter' => array(
					array(
						'name' => 'ToInt'
					)
				)
			),
			'required' => array(
				'required' => true
			),
			'type' => array(
				'required' => true
			),
			'extendedCid' => array(
				'required' => true
			),
			'regex' => array(
				'required' => false
			),
			'dbtable' => array(
				'required' => false
			),
			'forDisplay' => array(
				'required' => false
			),
			'forValue' => array(
				'required' => false
			),
			'list' => array(
				'required' => false
			),
			'dateFormat' => array(
				'required' => false
			),
			'getter' => array(
				'required' => false
			),
			'setter' => array(
				'required' => false
			),
			'label' => array(
				'required' => false
			),
			'min' => array(
				'required' => false
			),
			'max' => array(
				'required' => false
			),
			'step' => array(
				'required' => false
			),
			'dbfilter' => array(
				'required' => false
			),
			'dbquery' => array(
				'required' => false
			),
			'attributes' => array(
				'required' => false
			)
		);
	}
}
