<?php
namespace Ged\Form\Project;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class AddProcessLinkForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view, $factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('addProcessLink');
		$this->view = $view;
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'parentId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		$this->add(array(
			'name' => 'layout',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Process */
		$this->add(array(
			'name' => 'children',
			'type' => \Application\Form\Element\SelectProcess::class,
			'attributes' => array(
				'multiple' => true,
				'data-live-search' => true,
				'data-selected-text-format' => 'count > 1'
			),
			'options' => array(
				'label' => tra('Available Process'),
				'daoFactory' => $factory,
				'fullname' => true
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'parentId' => array(
				'required' => true
			),
			'layout' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			),
			'childId' => array(
				'required' => true
			)
		);
	}
} //End of class
