<?php
namespace Ged\Form\Project;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	public $template;

	public $ranchbe;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view, $factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('projectEdit');
		$this->template = 'ged/project/editform';
		$this->view = $view;
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'layout',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 40
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Forseen Close Date */
		$this->add(array(
			'name' => 'forseenCloseDate',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Click to select date',
				'class' => 'form-control datepicker'
			),
			'options' => array(
				'label' => 'Forseen Close Date'
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$rbConfig = \Ranchbe::get()->getConfig();
		
		$mask = $rbConfig['project.name.mask'];
		if ( $mask ) {
			$help = $rbConfig['project.name.mask.help'];
			$nameValidators = array(
				array(
					'name' => 'Regex',
					'options' => array(
						'pattern' => '/' . $mask . '/i',
						'messages' => array(
							\Zend\Validator\Regex::NOT_MATCH => $help
						)
					)
				)
			);
			$this->view->numberHelp = $help;
		}
		else {
			$nameValidators = array();
		}

		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => true
			),
			'name' => array(
				'required' => true,
				'validators' => $nameValidators
			),
			'forseenCloseDate' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Date',
						'options' => array(
							'format' => $rbConfig['view.shortdate.format']
						)
					)
				)
			)
		);
	}
} //End of class
