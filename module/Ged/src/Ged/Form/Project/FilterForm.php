<?php
namespace Ged\Form\Project;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * 
	 * @param \Rbs\Space\Factory $factory
	 * @param string $name
	 */
	public function __construct($factory = null, $name = null)
	{
		parent::__construct($factory, $name);

		$this->template = 'ged/project/filterform.phtml';
		$this->setAttribute('id', 'projectFilterForm');
		$elemtFactory = $this->getElementFactory();

		/* Number */
		$elemtFactory->numberElement();

		/* Designation */
		$elemtFactory->designationElement();

		/* Find what */
		$elemtFactory->findInElement([
			'id' => 'Id',
			'description' => 'Designation'
		]);

		/* Select action */
		$elemtFactory->userActionElement([
			'closeById' => 'Close By',
			'createById' => 'Created By'
		]);

		/* Advanced checkbox */
		$elemtFactory->advancedCbElement();
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(\Rbplm\Org\Project::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		//NUMBER
		if ( $datas['find_number'] ) {
			$filter->andFind($datas['find_number'], $dao->toSys('number'), Op::CONTAINS);
		}

		//DESIGNATION
		if ( $datas['find_designation'] ) {
			$filter->andFind($datas['find_designation'], $dao->toSys('description'), Op::CONTAINS);
		}

		//ADVANCED SEARCH
		if ( $datas['f_adv_search_cb'] ) {
			//FIND IN
			if ( $datas['find'] && $datas['find_field'] ) {
				$filter->andFind($datas['find'], $dao->toSys($datas['find_field']), Op::CONTAINS);
			}

			//ACTION USER
			if ( $datas['f_action_field'] && $datas['f_action_user_name'] ) {
				$filter->andFind($datas['f_action_user_name'], $dao->toSys($datas['f_action_field']), Op::EQUAL);
			}
		}

		return $this;
	}
}
