<?php
namespace Ged\Form\Archiver\Media;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 * 
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		parent::__construct($factory, null);

		$this->template = 'ged/archiver/media/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Search */
		$this->add(array(
			'name' => 'find_generic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_generic',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(\Ged\Archiver\Media::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* NAME */
		if ( $datas['find_generic'] ) {
			$keys = array(
				"''",
				$dao->toSys('uid'),
				$dao->toSys('name'),
				$dao->toSys('number'),
				$dao->toSys('description'),
				$dao->toSys('emplacement'),
				$dao->toSys('notice'),
				"''"
			);
			$key = 'CONCAT_WS(' . implode(',', $keys) . ')';

			$searchStr = $datas['find_generic'];
			$hash = explode(' ', $searchStr);
			foreach( $hash as $word ) {
				if ( $word ) {
					$filter->andFind(trim($word), $key, Op::CONTAINS);
				}
			}
		}
		return $this;
	}
} /* End of class */
