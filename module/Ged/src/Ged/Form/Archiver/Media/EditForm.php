<?php
namespace Ged\Form\Archiver\Media;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author olivier
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	public $template;

	public $ranchbe;

	protected $inputFilter;

	protected $scriptsSelect;

	protected $iconsSelect;

	protected $fileExtensionSelect;

	protected $visufileExtensionSelect;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('mediaEdit');

		$this->template = 'ged/archiver/media/editform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Uid */
		$this->add(array(
			'name' => 'uid',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Uid')
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Number */
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Notice */
		$this->add(array(
			'name' => 'notice',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'textarea',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Notice')
			)
		));

		/* Type */
		$this->add(array(
			'name' => 'type',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control type-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Type'),
				'value_options' => $this->_getTypes(),
				'multiple' => false
			)
		));

		/* Emplacement */
		$this->add(array(
			'name' => 'emplacement',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Emplacement'),
				'value_options' => $this->_getEmplacement(),
				'multiple' => false
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'uid' => array(
				'required' => true
			),
			'name' => array(
				'required' => true
			),
			'number' => array(
				'required' => true
			),
			'type' => array(
				'required' => true
			),
			'description' => array(
				'required' => false
			),
			'notice' => array(
				'required' => false
			)
		);
	}

	protected function _getTypes()
	{
		return array(
			'dat' => 'bande dat',
			'cd' => 'cd',
			'dvd' => 'dvd',
			'service-externe' => 'Service Fournisseur Externe',
			'ddexterne' => 'disque dur externe',
			'nas' => 'Nas',
			'ddserver' => 'Disque dur server'
		);
	}

	protected function _getEmplacement()
	{
		return array(
			'lt' => 'Local technique',
			'server1' => 'Server1',
			'server2' => 'server2'
		);
	}
}
