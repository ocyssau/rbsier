<?php
namespace Ged\Form\Archiver;

use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Application\Form\AbstractForm;

/**
 */
class ConfirmForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 *
	 * @var integer
	 */
	public $count = 0;

	/**
	 * @var array
	 */
	public $checked;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 * @param \Application\View\ViewModel $view
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('confirmform');

		/* init view var */
		$view->checked = array();
		$this->view = $view;

		$this->template = 'ged/archiver/confirmform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'checked' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			),
			'spacenames' => array(
				'required' => false
			)
		);
	}

	/**
	 * @param \Rbplm\Ged\Document\Version $document
	 */
	public function toConfirm($object, $label = null)
	{
		if ( !$label ) {
			$label = $object->getUid();
		}

		$id = $object->getId();
		$spacename = $object->spacename;
		$this->checked[] = $id;

		/* recap list with checkbox */
		$this->add(array(
			'name' => 'checked[' . $id . ']',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control',
				'checked' => true
			),
			'options' => array(
				'label' => $label,
				'use_hidden_element' => false,
				'checked_value' => $id
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacenames[' . $id . ']',
			'attributes' => array(
				'type' => 'hidden',
				'value' => $spacename
			)
		));

		$this->count++;
	}
}
