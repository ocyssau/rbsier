<?php
namespace Ged\Form\Docfile;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class ReplaceForm extends Form implements InputFilterProviderInterface
{

	/**
	 *
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('replaceForm');
		$this->template = 'ged/docfile/iteration/replace';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter())
			->setAttribute('enctype', 'multipart/form-data')
			->setAttribute('method', 'post');

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* File select */
		$this->add(array(
			'name' => 'uploadFile',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'id' => 'upload-file',
				'class' => ''
			),
			'options' => array(
				'label' => tra('Select File')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'spacename' => array(
				'required' => false
			),
			'id' => array(
				'required' => false
			),
			'uploadFile' => array(
				'required' => false
			)
		);
	}
}
