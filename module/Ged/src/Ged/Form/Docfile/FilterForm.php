<?php
namespace Ged\Form\Docfile;

use Rbplm\Dao\Filter\Op;
use Application\Form\AbstractFilterForm;
use Rbplm\Ged\Document;
use Rbplm\Ged\AccessCode;

/**
 * 
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 */
	public function __construct($factory = null, $name = null)
	{
		parent::__construct($factory, $name);

		$this->template = 'ged/docfile/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$elemtFactory = $this->getElementFactory();

		/* spacename */
		$this->add(array(
			'name' => 'spacename',
			'type' => 'Zend\Form\Element\Hidden'
		));

		/* Number */
		$elemtFactory->numberElement();

		/* Find what */
		$elemtFactory->findInElement([
			'extension' => 'Extensions',
			'path' => 'Path',
			'createByUid' => 'Created by',
			'created' => 'Created date',
			'updateByUid' => 'Last update by',
			'updated' => 'Last update date',
			'accessCode' => 'Access',
			'lifeStage' => 'Life Stage',
			'iteration' => 'Iteration',
			'type' => 'Type',
			'size' => 'Size'
		]);

		/* Select action */
		$elemtFactory->userActionElement([
			'lockByUid' => 'Checkout By',
			'updateByUid' => 'Update By',
			'createByUid' => 'Created By'
		]);

		/* State */
		$this->add(array(
			'name' => 'find_state',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for life stage...',
				'class' => 'form-control',
				'data-where' => '',
				'data-op' => ''
			),
			'options' => array(
				'label' => 'Status',
				'size' => 8
			)
		));
		$inputFilter->add(array(
			'name' => 'find_state',
			'required' => false
		));

		/* select access */
		$this->add(array(
			'name' => 'find_access_code',
			'type' => 'Application\Form\Element\SelectAccess',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Access')
			)
		));
		$inputFilter->add(array(
			'name' => 'find_access_code',
			'required' => false
		));

		/* Advanced search CheckBox */
		$elemtFactory->advancedCbElement();

		/* DateAndTime CheckBox */
		$elemtFactory->dateAndTimeCbElement();

		/* CheckOut date */
		$elemtFactory->dateAndTimeElement('check_out', 'CheckOut Date');

		/* Update date selector */
		$elemtFactory->dateAndTimeElement('update', 'Update Date');

		/* Open date */
		$elemtFactory->dateAndTimeElement('open', 'Open Date');

		/* Close date */
		$elemtFactory->dateAndTimeElement('close', 'Close Date');

		/* FsClose date */
		$elemtFactory->dateAndTimeElement('fsclose', 'Forseen Close Date');
	}

	/**
	 *
	 * @param array $optionalFields
	 *
	 */
	public function setExtended($optionalFields, $view)
	{
		if ( is_array($optionalFields) ) {
			foreach( $optionalFields as $val ) {
				/* Inferior to date */
				$this->add(array(
					'name' => $val['appName'],
					'type' => 'Zend\Form\Element\Text',
					'attributes' => array(
						'placeholder' => 'Click to select date',
						'class' => 'form-control',
						'size' => $val['size']
					),
					'options' => array(
						'label' => $val['description']
					)
				));
			}
		}
		$view->extended = $optionalFields;
		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(Document\Version::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();
		$docAlias = 'document.';
		if ( $filter->getAlias() ) {
			$dfAlias = $filter->getAlias() . '.';
		}
		else {
			$dfAlias = '';
		}

		/* NORMALS OPTIONS */
		//NUMBER
		if ( $datas['find_number'] ) {
			$toSys = $docAlias . $dao->toSys('number');
			$filter->andFind($datas['find_number'], $toSys, Op::OP_CONTAINS);
		}

		//ACCESS CODE
		if ( $datas['find_access_code'] ) {
			if ( $datas['find_access_code'] == 'free' ) {
				$acode = 0;
			}
			else {
				$acode = $datas['find_access_code'];
			}
			$toSys = $docAlias . $dao->toSys('accessCode');
			$filter->andFind($acode, $toSys, Op::OP_EQUAL);
		}

		//STATE
		if ( $datas['find_state'] ) {
			$toSys = $docAlias . $dao->toSys('lifeStage');
			$filter->andFind($datas['find_state'], $toSys, Op::OP_EQUAL);
		}

		//ADVANCED SEARCH
		if ( $datas['f_adv_search_cb'] ) {
			//FIND IN
			if ( $datas['find'] && $datas['find_field'] ) {
				$toSys = $dfAlias . $dao->toSys($datas['find_field']);
				$filter->andFind($datas['find'], $toSys, Op::CONTAINS);
			}

			//ACTION USER
			if ( $datas['f_action_field'] && $datas['f_action_user_name'] ) {
				$toSys = $dfAlias . $dao->toSys($datas['f_action_field']);
				$filter->andFind($datas['f_action_user_name'], $toSys, Op::CONTAINS);
			}

			//DATE AND TIME
			if ( $datas['f_dateAndTime_cb'] ) {
				//CHECKOUT
				if ( $datas['f_check_out_date_cb'] ) {
					if ( $datas['f_check_out_date_min'] ) {
						$toSys = $dfAlias . $dao->toSys('locked');
						$filter->andFind($this->dateToSys($datas['f_check_out_date_min']), $toSys, Op::SUP);

						$toSys = $dfAlias . $dao->toSys('accessCode');
						$filter->andFind(AccessCode::CHECKOUT, $toSys, Op::EQUAL);
					}
					if ( $datas['f_check_out_date_max'] ) {
						$toSys = $dfAlias . $dao->toSys('locked');
						$filter->andFind($this->dateToSys($datas['f_check_out_date_min']), $toSys, Op::INF);

						$toSys = $dfAlias . $dao->toSys('accessCode');
						$filter->andFind(AccessCode::CHECKOUT, $toSys, Op::EQUAL);
					}
				}
				//UPDATE
				if ( $datas['f_update_date_cb'] ) {
					if ( $datas['f_update_date_min'] ) {
						$toSys = $dfAlias . $dao->toSys('updated');
						$filter->andFind($this->dateToSys($datas['f_update_date_min']), $toSys, Op::SUP);
					}
					if ( $datas['f_update_date_max'] ) {
						$toSys = $dfAlias . $dao->toSys('updated');
						$filter->andFind($this->dateToSys($datas['f_update_date_max']), $toSys, Op::INF);
					}
				}
				//OPEN
				if ( $datas['f_open_date_cb'] ) {
					if ( $datas['f_open_date_min'] ) {
						$toSys = $dfAlias . $dao->toSys('created');
						$filter->andFind($this->dateToSys($datas['f_open_date_min']), $toSys, Op::SUP);
					}
					if ( $datas['f_open_date_max'] ) {
						$toSys = $dfAlias . $dao->toSys('created');
						$filter->andFind($this->dateToSys($datas['f_open_date_max']), $toSys, Op::INF);
					}
				}
				//CLOSE
				if ( $datas['f_close_date_cb'] ) {
					$toSys = $dfAlias . $dao->toSys('closed');
					if ( $datas['f_close_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_close_date_min']), $toSys, Op::SUP);
					}
					if ( $datas['f_close_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_close_date_max']), $toSys, Op::INF);
					}
				}
				//FORSEEN CLOSE
				if ( $datas['f_fsclose_date_cb'] ) {
					$toSys = $dfAlias . $dao->toSys('planned_closure');
					if ( $datas['f_fsclose_date_min'] ) {
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_min']), $toSys, Op::SUP);
					}
					if ( $datas['f_fsclose_date_max'] ) {
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_max']), $toSys, Op::INF);
					}
				}
			}
		}

		return $this;
	}
}
