<?php
namespace Ged\Form\Notification;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * 
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/** @var string */
	public $template;

	/** @var array */
	public $referenceIds;

	/** @var InputFilter */
	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view, $events = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('notificationEdit');

		$this->template = 'ged/notification/editform';
		$this->view = $view;

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		if ( $events ) {
			$this->eventSet = $events;
		}

		/* Set notifications collection */
		$notificationFieldset = new \Ged\Form\Notification\NotificationFieldset('notificationFieldset', $events);
		$notificationFieldset->setUseAsBaseFieldset(true);
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'notifications',
			'options' => array(
				'should_create_template' => false,
				'allow_add' => true,
				'target_element' => $notificationFieldset,
				'use_as_base_fieldset'
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array();
	}
}
