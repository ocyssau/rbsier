<?php
namespace Ged\Form\Notification;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Rbplm\Ged\Document;

/**
 *
 *
 */
class NotificationFieldset extends Fieldset implements InputFilterProviderInterface
{

	/** @var array */
	protected $eventSet;

	/**
	 *
	 * @param string $name
	 * @param array $events
	 */
	public function __construct($name, $events = null)
	{
		/* we want to ignore the name passed */
		parent::__construct($name);

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setObject(new \Rbs\Notification\Notification());

		if ( $events ) {
			$this->eventSet = $events;
		}

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'referenceId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'referenceCid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'referenceUid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Event */
		$this->add(array(
			'name' => 'events',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'selectpicker form-control',
				'multiple' => 'multiple',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Events'),
				'value_options' => $this->_getEvents()
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			),
			'referenceId' => array(
				'required' => false
			),
			'referenceCid' => array(
				'required' => false
			),
			'referenceUid' => array(
				'required' => false
			),
			'events' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'ToNull'
					)
				)
			)
		);
	}

	/**
	 * @return array
	 */
	protected function _getEvents()
	{
		if ( !$this->eventSet ) {
			$this->eventSet = array(
				Document\Version::SIGNAL_POST_CHECKIN => tra('checkin'),
				Document\Version::SIGNAL_POST_CHECKOUT => tra('checkout'),
				Document\Version::SIGNAL_POST_CANCELCHECKOUT => tra('cancel checkout'),
				Document\Version::SIGNAL_POST_DELETE => tra('delete'),
				Document\Version::SIGNAL_POST_MOVE => tra('move'),
				Document\Version::SIGNAL_POST_NEWVERSION => tra('new version'),
				Document\Version::SIGNAL_POST_COPY => tra('copy')
			);
		}
		return $this->eventSet;
	}
} /* End of class */
