<?php
// %LICENCE_HEADER%
namespace Ged\Form\Notification;

/**
 */
class Collection implements \Countable
{

	static $classId = 'referencecollection';

	/**
	 *
	 * @var array
	 */
	protected $items = array();

	/**
	 *
	 * @param array $properties
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['notifications'])) ? $this->items = $properties['notifications'] : null;
		(isset($properties['items'])) ? $this->items = $properties['items'] : null;
		return $this;
	}

	/**
	 * Alias for hydrate
	 *
	 * @param array $properties
	 */
	public function populate(array $properties)
	{
		return $this->hydrate($properties);
	}

	/**
	 * Alias for __serialize; implement arrayObject interface
	 * @return 	array
	 */
	public function getArrayCopy()
	{
		return array(
			'notifications' => $this->items
		);
	}

	/**
	 * method of Serializable interface
	 * @return array
	 */
	public function __sleep()
	{
		return array(
			'items'
		);
	}

	/**
	 *
	 * @param $notifications
	 */
	public function add($item)
	{
		$this->items[$item->getUid()] = $item;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function count()
	{
		return count($this->items);
	}

	/**
	 *
	 * @return array
	 */
	public function getIterator()
	{
		return $this->items;
	}
}
