<?php
namespace Ged\Form\Doctype;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Rbs\Space\Factory as DaoFactory;

/**
 *
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/** @var string */
	public $template;

	public $ranchbe;

	/** @var string */
	protected $inputFilter;

	/** @var array */
	protected $scriptsSelect;

	/** @var array */
	protected $iconsSelect;

	/** @var array */
	protected $fileExtensionSelect;

	/** @var array */
	protected $visufileExtensionSelect;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('doctypeEdit');

		$this->template = 'ged/doctype/editform';
		$this->ranchbe = \Ranchbe::get();

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$inputFilter = $this->getInputFilter();

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Number */
		$mask = '/' . $this->ranchbe->getConfig('doctype.name.mask') . '/i';
		$help = $this->ranchbe->getConfig('doctype.name.mask.help');
		$view->numberHelp = $help;
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number')
			)
		));
		$inputFilter->add(array(
			'name' => 'number',
			'required' => true,
			'validators' => array(
				array(
					'name' => 'Regex',
					'options' => array(
						'pattern' => $mask,
						'messages' => array(
							\Zend\Validator\Regex::INVALID => $help
						)
					)
				)
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Set A Convinient Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));
		$inputFilter->add(array(
			'name' => 'name',
			'required' => true,
			'validators' => array(
				array(
					'name' => 'Regex',
					'options' => array(
						'pattern' => '/[a-zA-Z0-9 -_]/i',
						'messages' => array(
							\Zend\Validator\Regex::INVALID => 'Use only letters and numbers, space, -,_'
						)
					)
				)
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'textarea',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Regex */
		$this->add(array(
			'name' => 'recognitionRegexp',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'textarea',
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 60
			),
			'options' => array(
				'label' => tra('Regex')
			)
		));

		/* May be composite */
		$this->add(array(
			'name' => 'mayBeComposite',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 1
			),
			'options' => array(
				'label' => tra('May Be Composite'),
				'value_options' => array(
					0 => 'No',
					1 => 'Yes'
				),
				'multiple' => false
			)
		));

		/* Priority */
		$this->add(array(
			'name' => 'priority',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Priority'),
				'value_options' => array(
					0 => 0,
					1 => 1,
					2 => 2,
					3 => 3,
					4 => 4,
					5 => 5,
					6 => 6,
					7 => 7,
					8 => 8,
					9 => 9,
					10 => 10
				),
				'multiple' => false
			)
		));

		/* Pre Store Class */
		$this->add(array(
			'name' => 'preStoreClass',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Pre Store Class',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Pre Store Class')
			)
		));

		/* Pre Store Method */
		$this->add(array(
			'name' => 'preStoreMethod',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Pre Store Method',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Pre Store Method')
			)
		));

		/* Post Store Class */
		$this->add(array(
			'name' => 'postStoreClass',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Post Store Class',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Post Store Class')
			)
		));

		/* Post Store Method */
		$this->add(array(
			'name' => 'postStoreMethod',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Post Store Method',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Post Store Method')
			)
		));

		/* Pre Update Class */
		$this->add(array(
			'name' => 'preUpdateClass',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Pre Update Class',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Pre Update Class')
			)
		));

		/* Pre Update Method */
		$this->add(array(
			'name' => 'preUpdateMethod',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Pre Update Method',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Pre Update Method')
			)
		));

		/* Post Update Class */
		$this->add(array(
			'name' => 'postUpdateClass',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Post Update Class',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Post Update Class')
			)
		));

		/* Post Update Method */
		$this->add(array(
			'name' => 'postUpdateMethod',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Post Update Method',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Post Update Method')
			)
		));

		/* Number Generator Class */
		$this->add(array(
			'name' => 'numberGeneratorClass',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Number Generator Class',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number Generator Class')
			)
		));

		/* Icon */
		$this->add(array(
			'name' => 'icon',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control icon-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Icon'),
				'value_options' => $this->_getIcons(),
				'multiple' => false
			)
		));

		/* File Extension */
		$this->add(array(
			'name' => 'fileExtensions',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'selectpicker fileext-select',
				'multiple' => 'multiple',
				'data-size' => 10,
				'data-live-search' => 'true'
			),
			'options' => array(
				'label' => tra('File Extension'),
				'value_options' => $this->_getFileExtensions()
			)
		));
		$inputFilter->add(array(
			'name' => 'fileExtensions',
			'required' => true,
			'filters' => array(
				array(
					'name' => 'Null',
					'options' => array(
						'type' => 'all'
					)
				)
			)
		));

		/* Visu File Extension */
		$this->add(array(
			'name' => 'visuFileExtension',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control visufile-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Visu File Type'),
				'value_options' => $this->_getVisuFileExtensions(),
				'multiple' => false
			)
		));
		$inputFilter->add(array(
			'name' => 'fileExtension',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'Null',
					'options' => array(
						'type' => 'all'
					)
				)
			)
		));

		/* File type */
		$fileTypes = $this->ranchbe->getFileTypes();
		$this->add(array(
			'name' => 'fileType',
			'type' => 'Zend\Form\Element\Select',
			'options' => array(
				'label' => tra('Has Associated Files'),
				'value_options' => $fileTypes,
				'multiple' => true
			),
			'attributes' => array(
				'value' => 'yes',
				'class' => 'form-control'
			)
		));

		/* Temlate */
		$this->add(array(
			'name' => 'templateId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control template-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Template'),
				'value_options' => $this->_getTemplates(),
				'multiple' => false
			)
		));

		/* Docseeder */
		$this->add(array(
			'name' => 'docseeder',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-check-input docseeder-cb'
			),
			'options' => array(
				'label' => tra('Valid for docseeder'),
				'use_hidden_element' => true,
				'checked_value' => '1',
				'unchecked_value' => '0'
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$classValidator = array(
			array(
				'name' => 'Regex',
				'options' => array(
					'pattern' => '/^\\\\[a-z0-9\\\\]*$/i',
					'messages' => array(
						\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, and \\. Must Start with \\'
					)
				)
			)
		);
		$methodValidator = array(
			array(
				'name' => 'Regex',
				'options' => array(
					'pattern' => '/^[a-z]*$/i',
					'messages' => array(
						\Zend\Validator\Regex::NOT_MATCH => 'Use only letters'
					)
				)
			)
		);

		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => false
			),
			'priority' => array(
				'required' => false
			),
			'recognitionRegexp' => array(
				'required' => false
			),
			'mayBeComposite' => array(
				'required' => false
			),
			'preStoreClass' => array(
				'required' => false,
				'validators' => $classValidator
			),
			'preStoreMethod' => array(
				'required' => false,
				'validators' => $methodValidator
			),
			'postStoreClass' => array(
				'required' => false,
				'validators' => $classValidator
			),
			'postStoreMethod' => array(
				'required' => false,
				'validators' => $methodValidator
			),
			'preUpdateClass' => array(
				'required' => false,
				'validators' => $classValidator
			),
			'preUpdateMethod' => array(
				'required' => false,
				'validators' => $methodValidator
			),
			'postUpdateClass' => array(
				'required' => false,
				'validators' => $classValidator
			),
			'postUpdateMethod' => array(
				'required' => false,
				'validators' => $methodValidator
			),
			'numberGeneratorClass' => array(
				'required' => false,
				'validators' => $classValidator
			),
			'icon' => array(
				'required' => false
			),
			'visuFileExtension' => array(
				'required' => false
			),
			'fileType' => array(
				'required' => false
			),
			'templateId' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'ToInt'
					),
					array(
						'name' => 'ToNull'
					)
				)
			),
			'docseeder' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'ToInt'
					)
				)
			)
		);
	}

	/**
	 *
	 * @return array
	 */
	protected function _getIcons()
	{
		if ( !isset($this->iconsSelect) ) {
			$iconsReposit = $this->ranchbe->getConfig('icons.doctype.source.path');
			$iconsList = glob($iconsReposit . '/*.gif');
			$iconsList = array_merge($iconsList, glob($iconsReposit . '/*.png'));
			sort($iconsList);
			$selectSet = array(
				0 => null
			);
			foreach( $iconsList as $icon ) {
				$icon = basename($icon);
				$selectSet[$icon] = $icon;
			}
			$this->iconsSelect = $selectSet;
		}

		return $this->iconsSelect;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getFileExtensions()
	{
		if ( !isset($this->fileExtensionSelect) ) {
			$fileExtensions = $this->ranchbe->getFileExtensions();
			$fileExtSelectSet = array_combine($fileExtensions['file'], $fileExtensions['file']);
			array_unshift($fileExtSelectSet, 'select one or many extension');
			$this->fileExtensionSelect = $fileExtSelectSet;
		}

		return $this->fileExtensionSelect;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getVisuFileExtensions()
	{
		if ( !isset($this->visuFileExtensionSelect) ) {
			$visuFileExtensions = $this->ranchbe->getVisuFileExtensions();
			$vfileExtSelectSet = array_combine($visuFileExtensions, $visuFileExtensions);
			array_unshift($vfileExtSelectSet, 'select one extension');
			$this->visufileExtensionSelect = $vfileExtSelectSet;
		}

		return $this->visufileExtensionSelect;
	}

	/**
	 * Get the data from the form and format it
	 */
	protected function _formgetValues($values)
	{
		$fileExtensions = \Ranchbe::getFileExtensions();

		/* unselect file extension list if type file is not selected */
		if ( !isset($values['file_type']['file']) ) $values['file_extension'] = array();

		/* if type file is selected check if file_extension is set */
		if ( isset($values['file_type']['file']) && !is_array($values['file_extension']) ) {
			print(tra('Extension is required'));
			return false;
		}

		/* Reformat the array send by form from the checkbox selection for file_type */
		$values['file_type'] = array_keys($values['file_type']);

		/* Add valid extension to file_extension list for type cadds, ps ...etc */
		foreach( $values['file_type'] as $filetype ) {
			if ( $filetype != 'file' ) {
				$values['file_extension'] = array_merge($values['file_extension'], array_values($fileExtensions[$filetype]));
			}
		}
		return $values;
	}

	/**
	 * Get type from db
	 *
	 * @return array
	 */
	protected function _getTemplates()
	{
		$daoFactory = DaoFactory::get('Bookshop');
		if ( !isset($this->templateSelect) ) {
			try {
				$list = $daoFactory->getList(\Rbplm\Ged\Document\Version::$classId);
				$list->select(array(
					'id',
					'name'
				));
				$list->load("as_template=1 LIMIT 1000");
				$selectSet[null] = 'Select one...';
				foreach( $list as $item ) {
					$selectSet[$item['id']] = $item['name'];
				}
				$this->templateSelect = $selectSet;
			}
			catch( \Exception $e ) {
				throw $e;
			}
		}
		return $this->templateSelect;
	}
}
