<?php
namespace Ged\Form\Doctype\Callback;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	public $template;

	public $ranchbe;

	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('callbackEdit');
		$this->template = 'ged/doctype/callback/editform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'doctypeId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Set A Convenient Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Events */
		$this->add(array(
			'name' => 'events',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control rb-select selectpicker',
				'multiple' => true
			),
			'options' => array(
				'label' => 'Select Events',
				'value_options' => $this->_getEvents()
			)
		));

		/* Callback Class */
		$this->add(array(
			'name' => 'callbackClass',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Callback Class')
			)
		));

		/* Callback Method */
		$this->add(array(
			'name' => 'callbackMethod',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Callback Method')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z0-9\s]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, numbers, space, and -,_'
							)
						)
					)
				)
			),
			'name' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z0-9-_]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, numbers, space, and -,_'
							)
						)
					)
				)
			),
			'events' => array(
				'required' => false
			),
			'callbackClass' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z\\\\]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, and \\'
							)
						)
					)
				)
			),
			'callbackMethod' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters'
							)
						)
					)
				)
			)
		);
	}

	/**
	 *
	 * @return array
	 */
	protected function _getEvents()
	{
		return array(
			'checkout.pre' => 'checkout.pre',
			'checkout.post' => 'checkout.post',
			'checkin.pre' => 'checkin.pre',
			'checkin.post' => 'checkin.post'
		);
	}
}
