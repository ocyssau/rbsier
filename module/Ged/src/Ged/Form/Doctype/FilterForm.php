<?php
namespace Ged\Form\Doctype;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 * 
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * 
	 * @param \RBs\Space\Factory $factory
	 * @param string $nameSpace
	 */
	public function __construct($factory, $nameSpace)
	{
		parent::__construct($factory, $nameSpace);

		$this->template = 'ged/doctype/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Magic */
		$this->add(array(
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control',
				'title' => 'Search in name, number, designation, File extension'
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_magic',
			'required' => false
		));

		/* Docseeder */
		$this->add(array(
			'name' => 'find_docseeder',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control submitOnClick'
			),
			'options' => array(
				'label' => 'Docseeder Type'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_docseeder',
			'required' => false
		));

		/* Name */
		$this->add(array(
			'name' => 'find_name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for name...',
				'class' => 'form-control',
				'data-where' => 'name',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Name'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_name',
			'required' => false
		));

		/* Designation */
		$this->add(array(
			'name' => 'find_designation',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for designation...',
				'class' => 'form-control',
				'data-where' => 'description',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_designation',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(\Rbplm\Ged\Doctype::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* MAGIC */
		if ( $datas['find_magic'] ) {
			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, `%s`, `%s`, '-')", $dao->toSys('name'), $dao->toSys('description'), $dao->toSys('number'), $dao->toSys('fileExtensions'));
			$filter->andFind($datas['find_magic'], $concat, Op::OP_CONTAINS);
		}

		/* DOCSEEDER */
		if ( $datas['find_docseeder'] ) {
			$filter->andFind($datas['find_docseeder'], $dao->toSys('docseeder'), Op::EQUAL);
		}

		/* NAME */
		if ( $datas['find_name'] ) {
			$filter->andFind($datas['find_name'], $dao->toSys('name'), Op::OP_CONTAINS);
		}

		/* DESIGNATION */
		if ( $datas['find_designation'] ) {
			$filter->andFind($datas['find_designation'], $dao->toSys('description'), Op::OP_CONTAINS);
		}

		return $this;
	}
}
