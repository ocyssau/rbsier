<?php
namespace Ged\Form\Container;

use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Form\Form;

/**
 *
 *
 */
class RenameForm extends Form implements InputFilterProviderInterface
{

	public $template;

	public $ranchbe;

	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view, $factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('containerEdit');

		$this->template = 'ged/container/renameform';
		$this->ranchbe = \Ranchbe::get();
		$this->daoFactory = $factory;

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'layout',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* New Number */
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('New Number')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$spacename = strtolower($this->daoFactory->getName());
		return array(
			'id' => array(
				'required' => false
			),
			'number' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/' . $this->ranchbe->getConfig($spacename . '.name.mask') . '/i',
							'messages' => array(
								\Zend\Validator\Regex::INVALID => $this->ranchbe->getConfig($spacename . '.name.mask.help')
							)
						)
					)
				)
			)
		);
	}
} /* End of class */
