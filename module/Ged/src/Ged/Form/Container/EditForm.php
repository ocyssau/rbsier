<?php
namespace Ged\Form\Container;

use Zend\InputFilter\InputFilter;
use Rbs\Model\Hydrator\Extendable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Rbs\Space\Factory as DaoFactory;

/**
 *
 *
 */
class EditForm extends \Application\Form\AbstractForm implements InputFilterProviderInterface
{
	use \Application\Form\ExtendableTrait;

	/**
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @var \Ranchbe
	 */
	public $ranchbe;

	/**
	 *
	 * @var DaoFactory
	 */
	public $daoFactory;

	/** 
	 * @var boolean 
	 */
	public $editMode = false;

	/**
	 *
	 * @var InputFilter
	 */
	protected $inputFilter;

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function __construct($factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('containerEdit');
		$this->daoFactory = $factory;

		$this->template = 'ged/container/editform';
		$this->ranchbe = \Ranchbe::get();
		$elemtFactory = $this->getElemtFactory($factory);

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'layout',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Number */
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 40
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Vault */
		$this->add(array(
			'name' => 'vault',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Vault'),
				'value_options' => $this->_getVault($factory),
				'multiple' => false
			)
		));

		/* Ceate new Vault */
		$this->add(array(
			'name' => 'createprivatevault',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-check-input',
				'id' => 'createprivatevault'
			),
			'options' => array(
				'label' => tra('Create Private Vault')
			)
		));

		/* Forseen Close Date */
		$params = array(
			'name' => 'forseenCloseDate',
			'label' => tra('Forseen close date'),
			'class' => 'form-control'
		);
		$elemtFactory->selectDate($params, $this);

		/* Parent */
		$params = array(
			'name' => 'parentId',
			'label' => tra('Parent project'),
			'fullname' => true,
			'size' => 1,
			'multiple' => false,
			'required' => true
		);
		$elemtFactory->selectProject($params);

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$spacename = $this->daoFactory->getName();

		return [
			'id' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'Null'
					],
					[
						'name' => 'ToInt'
					]
				],
				'validators' => [
					[
						'name' => 'Digits'
					]
				]
			],
			'layout' => [
				'required' => false
			],
			'spacename' => [
				'required' => true,
				'validators' => [
					[
						'name' => 'InArray',
						'options' => [
							'haystack' => \Rbs\Space\Space::getNames()
						]
					]
				]
			],
			'createprivatevault' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'ToInt'
					]
				],
				'validators' => [
					[
						'name' => 'Digits'
					]
				]
			],
			'vault' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'ToInt'
					]
				],
				'validators' => [
					[
						'name' => 'Digits'
					]
				]
			],
			'parentId' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'ToInt'
					]
				],
				'validators' => [
					[
						'name' => 'Digits'
					]
				]
			],
			'description' => [
				'required' => false,
				'filters' => [
					[
						'name' => 'HtmlEntities'
					],
					[
						'name' => 'StringTrim'
					]
				]
			],
			'forseenCloseDate' => [
				'required' => true,
				'validators' => [
					[
						'name' => 'Date'
					]
				]
			],
			'number' => [
				'required' => true,
				'validators' => [
					[
						'name' => 'Regex',
						'options' => [
							'pattern' => '/' . $this->ranchbe->getConfig($spacename . '.name.mask') . '/i',
							'messages' => [
								\Zend\Validator\Regex::INVALID => $this->ranchbe->getConfig($spacename . '.name.mask.help')
							]
						]
					]
				]
			]
		];
	}

	/**
	 *
	 * @param boolean $bool
	 * @return EditForm
	 */
	public function setEditMode($bool)
	{
		$this->editMode = $bool;
		if ( $bool ) {
			$this->get('number')->setAttribute('disabled', true);
			$this->get('vault')->setAttribute('disabled', true);
			$this->get('createprivatevault')->setAttribute('disabled', true);
			$this->getInputFilter()
				->remove('number')
				->remove('vault')
				->remove('createprivatevault');
		}
		return $this;
	}

	/**
	 * @param DaoFactory $factory
	 * @return array
	 */
	protected function _getVault($factory)
	{
		$spacename = strtolower($factory->getName());
		$selectSet = array();
		$list = $factory->getList(\Rbplm\Vault\Reposit::$classId);
		$dao = $factory->getDao(\Rbplm\Vault\Reposit::$classId);
		$select = array(
			$dao->toSys('id') . ' AS id',
			$dao->toSys('name') . ' AS label',
			$dao->toSys('description') . ' AS title'
		);
		$list->select($select);
		$list->load(sprintf('actif=1 AND spacename="%s" ORDER BY label LIMIT 1000', $spacename));

		foreach( $list as $item ) {
			$itemLabel = $item['label'];
			$itemLabel = $itemLabel . ' (' . $item['title'] . ')';
			$selectSet[$item['id']] = $itemLabel;
		}

		return $selectSet;
	}
}
