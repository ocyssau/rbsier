<?php
namespace Ged\Form\Container;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * 
	 * @var array
	 */
	public $extended;

	/**
	 *
	 * @var string
	 */
	public $spacename;

	/**
	 * 
	 * @param \Rbs\Space\Factory $factory
	 * @param string $name
	 */
	public function __construct($factory = null, $name = null)
	{
		parent::__construct($factory, $name);

		$this->template = 'ged/container/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$elemtFactory = $this->getElementFactory();

		/* spacename */
		$this->add(array(
			'name' => 'spacename',
			'type' => 'Zend\Form\Element\Hidden'
		));

		/* Magic */
		$elemtFactory->magicElement();

		/* Number */
		$elemtFactory->numberElement();

		/* Designation */
		$elemtFactory->designationElement();

		/* Project */
		$this->add(array(
			'name' => 'find_project',
			'type' => 'Application\Form\Element\SelectProject',
			'attributes' => array(
				'placeholder' => tra('Of project...'),
				'class' => 'btn submitOnChange',
				'data-width' => '150px'
			),
			'options' => array(
				'label' => tra('Project'),
				'daoFactory' => $factory,
				'maybenull' => true,
				'unselected_value' => 'All project'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_project',
			'required' => false
		));

		/* Closed */
		$this->add(array(
			'name' => 'find_closed',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-check-input submitOnChange'
			),
			'options' => array(
				'label' => tra('Display closed'),
				'use_hidden_element' => true,
				'checked_value' => '1',
				'unchecked_value' => '0'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_closed',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'Int'
				)
			)
		));

		/* Find what */
		$elemtFactory->findInElement([
			'id' => 'id'
		]);

		/* Select action */
		$elemtFactory->userActionElement([
			'closeById' => 'Close By',
			'createById' => 'Created By'
		]);

		/* Advanced search CheckBox */
		$elemtFactory->advancedCbElement();

		/* DateAndTime CheckBox */
		$elemtFactory->dateAndTimeCbElement();

		/* Open date */
		$elemtFactory->dateAndTimeElement('open', 'Open Date');

		/* Close date */
		$elemtFactory->dateAndTimeElement('close', 'Close Date');

		/* FsClose date */
		$elemtFactory->dateAndTimeElement('fsclose', 'Forseen Close Date');
	}

	/**
	 *
	 * @param array $optionalFields
	 *
	 */
	public function setExtended($optionalFields)
	{
		$this->extended = $optionalFields;
		$element = $this->get('find_field');
		$options = $element->getValueOptions();
		foreach( $optionalFields as $val ) {
			$options[$val['appName']] = $val['label'];
		}
		$element->setValueOptions($options);
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 * 
	 * IMPORTANT : Filter build request from asApp properties. So ensure that request is build with a select close to translate sysName to appName
	 * with SQL request as SELECT * FROM ( select asSys AS asApp from ...)...
	 * 
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();

		// MAGIC
		if ( !empty($datas['find_magic']) ) {
			$what = $datas['find_magic'];
			$what = str_replace('*', ' ', $what);
			$what = str_replace('  ', ' ', $what);
			$whats = explode(' ', $what);

			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, '-')", 'description', 'number');
			foreach( $whats as $what ) {
				$filter->andFind($what, $concat, Op::CONTAINS);
			}
		}

		//NUMBER
		if ( $datas['find_number'] ) {
			$filter->andfind($datas['find_number'], 'number', Op::CONTAINS);
		}

		//DESIGNATION
		if ( $datas['find_designation'] ) {
			$filter->andfind($datas['find_designation'], 'description', Op::CONTAINS);
		}

		//PROJECT ID
		if ( $datas['find_project'] ) {
			$filter->andfind($datas['find_project'], 'parentId', Op::EQUAL);
		}

		//CLOSED
		if ( !$datas['find_closed'] ) {
			$filter->andfind('', 'closed', Op::ISNULL);
		}

		//ADVANCED SEARCH
		if ( $datas['f_adv_search_cb'] ) {
			//FIND IN
			if ( $datas['find'] && $datas['find_field'] ) {
				$what = str_replace('*', '%', $datas['find']);
				$filter->andfind($what, $datas['find_field'], Op::LIKE);
			}

			//ACTION USER
			if ( $datas['f_action_field'] && $datas['f_action_user_name'] ) {
				$filter->andfind($datas['f_action_user_name'], $datas['f_action_field'], Op::CONTAINS);
			}

			//DATE AND TIME
			if ( $datas['f_dateAndTime_cb'] ) {
				//OPEN
				if ( $datas['f_open_date_cb'] ) {
					if ( $datas['f_open_date_min'] ) {
						$filter->andfind($this->dateToSys($datas['f_open_date_min']), 'created', Op::SUP);
					}
					if ( $datas['f_open_date_max'] ) {
						$filter->andfind($this->dateToSys($datas['f_open_date_max']), 'created', Op::INF);
					}
				}
				//CLOSE
				if ( $datas['f_close_date_cb'] ) {
					if ( $datas['f_close_date_min'] ) {
						$filter->andfind($this->dateToSys($datas['f_close_date_min']), 'closed', Op::SUP);
					}
					if ( $datas['f_close_date_max'] ) {
						$filter->andfind($this->dateToSys($datas['f_close_date_max']), 'closed', Op::INF);
					}
				}
				//FORSEEN CLOSE
				if ( $datas['f_fsclose_date_cb'] ) {
					if ( $datas['f_fsclose_date_min'] ) {
						$filter->andfind($this->dateToSys($datas['f_fsclose_date_min']), 'forseenCloseDate', Op::SUP);
					}
					if ( $datas['f_fsclose_date_max'] ) {
						$filter->andfind($this->dateToSys($datas['f_fsclose_date_max']), 'forseenCloseDate', Op::INF);
					}
				}
			}
		}

		//EXTENDED
		if ( is_array($this->extended) ) {
			foreach( $this->extended as $val ) {
				$fieldName = $val['appName'];
				if ( isset($datas[$fieldName]) ) {
					$filter->andfind($datas[$fieldName], $fieldName, Op::CONTAINS);
				}
			}
		}

		return $this;
	}
} /* End of class */
