<?php
namespace Ged\Form\Container\Alias;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * 
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/** @var string */
	public $template;

	/** @var boolean */
	public $editMode = false;

	/**
	 *
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function __construct($view, $factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('aliasEdit');
		$this->template = 'ged/container/alias/editform';

		/* */
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		$this->add(array(
			'name' => 'spacename',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Number */
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number')
			)
		));

		/* Alias Of */
		$this->add(array(
			'name' => 'aliasOfId',
			'type' => 'Application\Form\Element\SelectContainer',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Alias of'),
				'daoFactory' => $factory
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => false
			),
			'number' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-zA-Z0-9]+/',
							'messages' => array(
								\Zend\Validator\Regex::INVALID => 'Only string characters r - and _, without spaces'
							)
						)
					)
				)
			)
		);
	}

	/**
	 *
	 * @param boolean $bool
	 * @return EditForm
	 */
	public function setEditMode($bool)
	{
		$this->editMode = $bool;
		if ( $bool ) {
			$this->get('number')->setAttribute('disabled', true);
			$this->getInputFilter()->remove('number');
		}
		return $this;
	}
} /* End of class */
