<?php
namespace Ged\Form\Container;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class FulltextSearchForm extends AbstractFilterForm
{

	/**
	 * @param string $nameSpace
	 */
	public function __construct($factory, $nameSpace)
	{
		parent::__construct($factory, $nameSpace);

		$this->template = 'search/fulltextsearchform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Find */
		$this->add(array(
			'name' => 'find_what',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_what',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* WHAT */
		if ( $datas['find_what'] ) {
			$whats = explode(' ', $datas['find_what']);
			foreach( $whats as $what ) {
				$subfilter = new $filter('', false);
				$subfilter->orFind($what, 'number', Op::CONTAINS)
					->orFind($what, 'name', Op::CONTAINS)
					->orFind($what, 'designation', Op::CONTAINS)
					->orFind($what, 'id', Op::EQUAL);
				$filter->suband($subfilter);
			}
		}
		return $this;
	}
}
