<?php
namespace Message\Form;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 * 
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @param \Rbs\Space\Factory $factory
	 * @param string $nameSpace
	 */
	public function __construct($factory, $nameSpace)
	{
		parent::__construct($factory, $nameSpace);
		$this->template = 'message/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Find */
		$this->add(array(
			'name' => 'find',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Input Search here',
				'class' => 'form-control',
				'data-where' => 'body',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find',
			'required' => false
		));

		/* Mail Priority */
		$this->add(array(
			'name' => 'find_priority',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control submitOnChange',
				'id' => 'find_mailprio'
			),
			'options' => array(
				'label' => tra('Priority'),
				'value_options' => array(
					0 => 'all',
					1 => '1 - Very Low',
					2 => '2 - Low',
					3 => '3 - Normal',
					4 => '4 - High',
					5 => '5 - Very High'
				)
			)
		));
		$inputFilter->add(array(
			'name' => 'find_priority',
			'required' => false
		));

		/* Mail Priority */
		$this->add(array(
			'name' => 'find_flags',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control submitOnChange',
				'id' => 'find_flags'
			),
			'options' => array(
				'label' => tra('Flag'),
				'value_options' => array(
					0 => 'all',
					'isRead' => 'Is Read',
					'isFlagged' => 'Is Flagged',
					'isNotRead' => 'Is Not Read',
					'isNotFlagged' => 'Is Not Flagged'
				)
			)
		));
		$inputFilter->add(array(
			'name' => 'find_flags',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(\Rbplm\Ged\Doctype::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* FIND */
		if ( $datas['find'] ) {
			$filter->andFind($datas['find'], 'CONCAT(' . $dao->toSys('subject') . ',' . $dao->toSys('body') . ')', Op::CONTAINS);
		}

		/* PRIO */
		if ( $datas['find_priority'] ) {
			$filter->andFind($datas['find_priority'], $dao->toSys('priority'), Op::EQUAL);
		}

		/* FLAG */
		if ( $datas['find_flags'] ) {
			$filter->andFind($datas['find_flags'], $dao->toSys('isFlagged'), Op::EQUAL);
		}

		return $this;
	}
}
