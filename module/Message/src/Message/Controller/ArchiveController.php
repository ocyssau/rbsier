<?php
namespace Message\Controller;

use Rbplm\Sys\Message;
use Application\Controller\AbstractController;

class ArchiveController extends MailboxController
{

	public $pageId = 'message_archive';

	public $defaultSuccessForward = 'message/archive/index';

	public $defaultFailedForward = 'message/archive/index';

	/**
	 * (non-PHPdoc)
	 *
	 * @see Controller.Controller::init()
	 */
	public function init()
	{
		AbstractController::init();
		$this->message = new Message\Archived(Message::TYPE_ARCHIVE);
		$this->indexTemplate = 'message/archive/index.phtml';

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('home')->activate();
		$this->layout()->tabs = $tabs;
	}

	/**
	 */
	public function indexAction()
	{
		$view = MailboxController::indexAction();
		$view->pageTitle = 'Archive Mails';
		return $view;
	}
} //End of class

