<?php
namespace Message\Controller;

use Rbplm\Sys\Message;
use Application\Controller\AbstractController;

class SentController extends MailboxController
{

	public $pageId = 'message_sent';

	public $defaultSuccessForward = 'message/sent/index';

	public $defaultFailedForward = 'message/sent/index';

	/**
	 */
	public function init()
	{
		AbstractController::init();
		$this->message = new Message\Sent(Message::TYPE_SEND);
		$this->indexTemplate = 'message/sent/index.phtml';

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('home')->activate();
		$this->layout()->tabs = $tabs;
	}

	/**
	 */
	public function indexAction()
	{
		$view = MailboxController::indexAction();
		$view->pageTitle = 'Sent Mails';
		return $view;
	}
} //End of class

