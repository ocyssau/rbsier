<?php
namespace Message\Controller;

use Application\Controller\AbstractController;
use Rbplm\Sys\Message;
use Rbplm\People;
use Rbs\Space\Factory as DaoFactory;

/**
 * 
 *
 */
class ComposeController extends AbstractController
{

	public $pageId = 'message_compose';

	public $defaultSuccessForward = 'message/mailbox/index';

	public $defaultFailedForward = 'message/mailbox/index';

	/**
	 * (non-PHPdoc)
	 *
	 * @see Controller.Controller::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('home')->activate();
		$this->layout()->tabs = $tabs;
	}

	/**
	 */
	public function indexAction()
	{}

	/**
	 * Strip Re:Re:Re: from subject
	 */
	public function replyAction()
	{
		$msgId = $this->getRequest()->getParam('msgId');
		$reply = $this->getRequest()->getParam('reply');
		$replyall = $this->getRequest()->getParam('replyall');

		$currentUser = People\CurrentUser::get();
		$message = new Message(Message::TYPE_MAILBOX);

		$msg = $message->getMessage($currentUser->getId(), $msgId);
		$subject = $msg['subject'];

		// Strip Re:Re:Re: from subject
		if ( $reply || $replyall ) {
			$subject = tra("Re:") . ereg_replace("^(" . tra("Re:") . ")+", "", $subject);
		}
	}

	/**
	 */
	public function sendAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$byMessage = $request->getPost('bymessage', true);
			$byMail = $request->getPost('bymail', false);
			$toAll = $request->getPost('toall', null);

			#$subject = $request->getPost('subject', null);
			#$body = $request->getPost('body', null);
			#$priority = $request->getPost('priority', null);

			$cancel = $request->getPost('cancel', null);
			$validate = $request->getPost('validate', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		$factory = DaoFactory::get();
		$currentUser = People\CurrentUser::get();

		$message = new Message(Message::TYPE_SEND);
		$message->setOwner($currentUser);
		$message->setFrom($currentUser->getLogin(), $currentUser->getLogin());
		$message->bymail = $byMail;
		$message->bymessage = $byMessage;
		$message->toall = $toAll;

		$form = new \Message\Form\ComposeForm($view);
		$form->bind($message);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				if ( $toAll ) {
					$users = $factory->getList(People\User::$classId);
					$users->load();
					foreach( $users as $user ) {
						$to = $user['login'];
						$message->addTo($to, $user['login']);
					}
				}

				if ( $byMail ) {
					try {
						$userDao = $factory->getDao(People\User::$classId);
						$mailMessage = new \Rbplm\Sys\Mail();
						$from = $currentUser->getMail();
						if ( $from ) {
							$mailMessage->setFrom($currentUser->getMail(), $currentUser->getLogin());
						}
						else {
							throw new \Exception('Current user mail is not defined');
						}

						foreach( $message->getTo() as $login ) {
							try {
								$user = new People\User();
								$userDao->loadFromLogin($user, $login);
								$toMail = $user->getMail();
								if ( $toMail ) {
									$mailMessage->addTo($toMail, $user->getFullName());
								}
							}
							catch( \Exception $e ) {}
						}
						foreach( $message->getCc() as $login ) {
							try {
								$user = new People\User();
								$userDao->loadFromLogin($user, $login);
								$toMail = $user->getMail();
								if ( $toMail ) {
									$mailMessage->addCc($toMail, $user->getFullName());
								}
							}
							catch( \Exception $e ) {}
						}
						foreach( $message->getBcc() as $login ) {
							try {
								$user = new People\User();
								$userDao->loadFromLogin($user, $login);
								$toMail = $user->getMail();
								if ( $toMail ) {
									$mailMessage->addBcc($toMail, $user->getFullName());
								}
							}
							catch( \Exception $e ) {}
						}
						$mailMessage->setSubject($message->getSubject());
						$mailMessage->setBody($message->getBody());
						$mailMessage->send();
						$this->errorStack()->feedback('Message is sent');
					}
					catch( \Exception $e ) {
						$this->errorStack()->error($e->getMessage());
					}
				}
				if ( $byMessage ) {
					try {
						$dao = $factory->getDao(Message::$classId);
						$dao->save($message);
						$dao = $factory->getDao(Message\Sent::$classId);
						$dao->save(clone ($message));
						$this->errorStack()->feedback('Message is sent');
					}
					catch( \Exception $e ) {
						$this->errorStack()->error($e->getMessage());
					}
				}
			}
		}

		$view->setTemplate($form->template);
		$view->pageTitle = 'Compose Message';
		$view->form = $form;
		return $view;
	}
} /* End of class */
