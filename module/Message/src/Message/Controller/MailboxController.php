<?php
namespace Message\Controller;

use Application\Controller\AbstractController;
use Rbplm\Sys\Message;
use Rbplm\People;
use Rbs\Dao\Sier\Filter;
use Rbplm\Dao\Filter\Op;

/**
 * 
 */
class MailboxController extends AbstractController
{

	/**/
	public $pageId = 'message_mailbox';

	/**/
	public $defaultSuccessForward = 'message/mailbox/index';

	/**/
	public $defaultFailedForward = 'message/mailbox/index';

	/**
	 * @var Message
	 */
	public $message;

	/**
	 */
	public function init()
	{
		parent::init();
		$this->message = new Message(Message::TYPE_MAILBOX);
		$this->indexTemplate = 'message/mailbox/index.phtml';

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('home')->activate();
		$this->layout()->tabs = $tabs;
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate($this->indexTemplate);

		$ownerUid = People\CurrentUser::get()->getLogin();
		$message = $this->message;

		/* Init some helpers*/
		$request = $this->getRequest();
		$factory = \Rbs\Space\Factory::get();
		$ranchbe = \Ranchbe::get();
		$list = $factory->getList($message::$classId);
		$dao = $factory->getDao($message::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->andfind($ownerUid, $dao->toSys('ownerUid'), Op::EQUAL);
		$filterForm = new \Message\Form\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* set data for gauge */
		$cellsize = 200;
		$percentage = 1;
		$mailboxMaxSize = $ranchbe->getConfig('message.mailbox.maxsize');
		if ( $mailboxMaxSize > 0 ) {
			$currentSize = $paginator->maxLimit;
			$percentage = ($currentSize / $mailboxMaxSize) * 100;
			$cellsize = round($percentage / 100 * 200);
			if ( $currentSize > $mailboxMaxSize ) {
				$cellsize = 200;
			}
			if ( $cellsize < 1 ) {
				$cellsize = 1;
			}
			$percentage = round($percentage);
		}

		$view->mailboxSize = $currentSize;
		$view->mailboxMaxsize = $mailboxMaxSize;
		$view->cellsize = $cellsize;
		$view->percentage = $percentage;
		$view->pageTitle = 'Mail Box';

		return $view;
	}

	/**
	 *
	 */
	public function readAction()
	{
		$view = $this->view;
		$view->setTemplate('message/read/index.phtml');

		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$priority = $request->getQuery('priority', null);
			$sortMode = $request->getQuery('sort_mode', null);
			$offset = $request->getQuery('offset', null);
			$find = $request->getQuery('find', null);
			$id = $request->getQuery('id', null);
		}
		if ( !$id ) {
			return $this->successForward();
		}

		/* @var Message $message */
		$message = $this->message;
		$factory = \Rbs\Space\Factory::get();
		$dao = $factory->getDao($message);
		$dao->loadFromId($message, $id);

		$view->flag = null;
		$view->priority = $priority;
		$view->flagval = null;
		$view->offset = $offset;
		$view->sort = $sortMode;
		$view->find = $find;

		/* Mark the message as read in the receivers mailbox */
		$message->isRead(true);
		$dao->save($message);

		/* Get the message and assign its data to template vars */
		$view->message = $message;
		return $view;
	}

	/**
	 * Mark messages if the mark button was pressed
	 */
	public function markAction()
	{
		$factory = \Rbs\Space\Factory::get();
		$dao = $factory->getDao(Message::$classId);
		if ( $this->msgIds ) {
			foreach( array_keys($this->msgIds) as $msgId ) {
				$message = new Message(Message::TYPE_MAILBOX);
				$dao->loadFromId($message, $msgId);
				$message->isFlagged = true;
				$dao->save($message);
			}
		}
		return $this->successForward();
	}

	/**
	 * Delete messages if the delete button was pressed
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$msgIds = $request->getQuery('checked', []);
		}
		elseif ( $request->isPost() ) {
			$msgIds = $request->getPost('checked', []);
		}

		$factory = \Rbs\Space\Factory::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);

		if ( $msgIds ) {
			foreach( $msgIds as $msgId ) {
				$dao->deleteFromId($msgId);
			}
		}
		return $this->successForward();
	}

	/**
	 * Archive messages if the archive button was pressed
	 */
	public function archiveAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$checked = $request->getQuery('checked', []);
		}
		elseif ( $request->isPost() ) {
			$checked = $request->getPost('checked', []);
		}

		$ownerUid = People\CurrentUser::get()->getLogin();

		$factory = \Rbs\Space\Factory::get();
		$ranchbe = \Ranchbe::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);
		$list = $factory->getList(Message\Archived::$classId);

		$filter = new Filter('', false);
		$filter->andfind($ownerUid, $dao->toSys('ownerUid'), Op::EQUAL);

		$count = $list->countAll($filter);
		$maxArchiveSize = $ranchbe->getConfig('message.archive.maxsize');

		if ( $maxArchiveSize > 0 && $count >= $maxArchiveSize ) {
			$msg = tra("Archive is full. Delete some messages from archive first.");
			$this->errorStack()->error($msg);
			$this->successForward();
		}

		if ( $checked ) {
			$filter = "`id`=:id";
			foreach( $checked as $id ) {
				$bind = array(
					':id' => $id
				);
				$dao->archive($filter, $bind);
			}
		}
		return $this->successForward();
	}

	/**
	 * Archive messages if the archive button was pressed
	 */
	public function autoarchiveAction()
	{
		$days = (int)$this->params()->fromQuery('days');

		$userUid = People\CurrentUser::get()->getLogin();
		$factory = \Rbs\Space\Factory::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);

		if ( $days < 1 ) {
			return $this->successForward();
		}

		$age = date("U") - ($days * 3600 * 24);

		// @todo: only move as much msgs into archive as there is space left in there
		$filter = "`ownerUid`=:user AND `date`<=:age";
		$bind = array(
			':ownerUid' => $userUid,
			':age' => $age
		);
		$dao->archive($filter, $bind);

		return $this->successForward();
	}

	/**
	 * Delete old messages
	 */
	public function autodeleteAction()
	{
		$userUid = People\CurrentUser::get()->getUid();
		$factory = \Rbs\Space\Factory::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);

		$days = (int)$this->params()->fromQuery('days');

		if ( $days < 1 ) {
			return $this->successForward();
		}

		$age = date("U") - ($days * 3600 * 24);

		// @todo: only move as much msgs into archive as there is space left in there
		$filter = "`ownerUid`=:user AND `date`<=:age";
		$bind = array(
			':ownerUid' => $userUid,
			':age' => $age
		);

		$dao->delete($filter, $bind);

		return $this->successForward();
	}

	/**
	 * Download messages if the download button was pressed
	 */
	public function downloadAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$checked = $request->getQuery('checked', []);
		}
		elseif ( $request->isPost() ) {
			$checked = $request->getPost('checked', []);
		}

		$factory = \Rbs\Space\Factory::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);

		/* if message ids are handed over, use them: */
		if ( $checked ) {
			foreach( $checked as $id ) {
				$m = clone ($message);
				$dao->loadFromId($m, $id);
				$items[] = $m;
			}
		}
		$this->view->items = $items;

		$fileName = 'mailbox-' . time("U") . '.txt';
		header("Content-disposition: attachment; filename=$fileName");
		header("Content-Type: application/txt");
		header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		die();
	}
} //End of class
