<?php
namespace Message\Controller;

use Zend\Mail\Address;
use Zend\Mail\Message;
use Zend\Mail\Transport\TransportInterface;
use RuntimeException;

/**
 * SMTP connection object
 *
 * Loads an instance of Zend\Mail\Protocol\Smtp and forwards smtp transactions
 */
class Bdd implements TransportInterface
{

	/**
	 * @var array
	 */
	protected $options;

	/**
	 * Constructor.
	 *
	 */
	public function __construct()
	{}

	/**
	 * Set options
	 *
	 * @param  array $options
	 * @return Bdd
	 */
	public function setOptions(array $options)
	{
		$this->options = $options;
		return $this;
	}

	/**
	 * Get options
	 *
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * Send an email via the SMTP connection protocol
	 *
	 * The connection via the protocol adapter is made just-in-time to allow a
	 * developer to add a custom adapter if required before mail is sent.
	 *
	 * @param Message $message
	 * @throws \RuntimeException
	 */
	public function send(Message $message)
	{
		// Prepare message
		#$from = $this->prepareFromAddress($message);
		$recipients = $this->prepareRecipients($message);
		$headers = $this->prepareHeaders($message);
		$body = $this->prepareBody($message);

		if ( (count($recipients) == 0) && (!empty($headers) || !empty($body)) ) {
			// Per RFC 2821 3.3 (page 18)
			throw new \RuntimeException(sprintf('%s transport expects at least one recipient if the message has at least one header or body', __CLASS__));
		}
	}

	/**
	 * Retrieve email address for envelope FROM
	 *
	 * @param  Message $message
	 * @throws \RuntimeException
	 * @return string
	 */
	protected function prepareFromAddress(Message $message)
	{
		if ( $this->getEnvelope() && $this->getEnvelope()->getFrom() ) {
			return $this->getEnvelope()->getFrom();
		}

		$sender = $message->getSender();
		if ( $sender instanceof Address\AddressInterface ) {
			return $sender->getEmail();
		}

		$from = $message->getFrom();
		if ( !count($from) ) {
			// Per RFC 2822 3.6
			throw new \RuntimeException(sprintf('%s transport expects either a Sender or at least one From address in the Message; none provided', __CLASS__));
		}

		$from->rewind();
		$sender = $from->current();
		return $sender->getEmail();
	}

	/**
	 * Prepare array of email address recipients
	 *
	 * @param  Message $message
	 * @return array
	 */
	protected function prepareRecipients(Message $message)
	{
		if ( $this->getEnvelope() && $this->getEnvelope()->getTo() ) {
			return (array)$this->getEnvelope()->getTo();
		}

		$recipients = [];
		foreach( $message->getTo() as $address ) {
			$recipients[] = $address->getEmail();
		}
		foreach( $message->getCc() as $address ) {
			$recipients[] = $address->getEmail();
		}
		foreach( $message->getBcc() as $address ) {
			$recipients[] = $address->getEmail();
		}

		$recipients = array_unique($recipients);
		return $recipients;
	}

	/**
	 * Prepare header string from message
	 *
	 * @param  Message $message
	 * @return string
	 */
	protected function prepareHeaders(Message $message)
	{
		$headers = clone $message->getHeaders();
		$headers->removeHeader('Bcc');
		return $headers->toString();
	}

	/**
	 * Prepare body string from message
	 *
	 * @param  Message $message
	 * @return string
	 */
	protected function prepareBody(Message $message)
	{
		return $message->getBodyText();
	}
}
