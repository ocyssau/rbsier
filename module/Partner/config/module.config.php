<?php
return array(
	'router' => array(
		'routes' => array(
			'partner' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/partner',
					'defaults' => array(
						'__NAMESPACE__' => 'Partner\Controller',
						'controller' => 'Index',
						'action' => 'display'
					)
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/[:controller[/:action[/:id]]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
							),
							'defaults' => array()
						)
					)
				)
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Partner\Controller\Index' => 'Partner\Controller\IndexController'
		)
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Partner' => __DIR__ . '/../view'
		)
	),
	'menu' => [
		'admin' => [
			'partner' => [
				'name' => 'partner',
				'route' => [
					'params' => [
						'action' => 'index'
					],
					'options' => [
						'name' => 'partner'
					]
				],
				'class' => '%anchor%',
				'label' => 'Partner'
			]
		]
	]
);
