<?php
namespace Partner\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\People\Somebody;

/**
 */
class IndexController extends AbstractController
{

	/** @var string */
	public $pageId = 'category_manage';

	/** @var string */
	public $defaultSuccessForward = 'partner/index/display';

	/** @var string */
	public $defaultFailedForward = 'partner/index/display';

	/**
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('partner');
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::initAjax()
	 */
	public function initAjax($view = null, $errorStack = null)
	{
		parent::initAjax($view, $errorStack);
		$this->resourceCn = \Acl\Model\Resource\Application::$appCn;
	}

	/**
	 */
	public function displayAction()
	{
		/* Check permission */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$request = $this->getRequest();
		$view = $this->view;
		$view->setTemplate('partner/index/index');
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		$factory = DaoFactory::get();
		$trans = $factory->getDao(Somebody::$classId)->getTranslator();

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Partner\Form\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'partnerfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Somebody::$classId);
		$select = $trans->getSelectAsApp();
		
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->orderby = 'lastname';
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);

		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Partner Manager';
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		$partnerIds = $request->getQuery('checked', []);

		/* Check permission */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Somebody::$classId);

		foreach( $partnerIds as $partnerId ) {
			try {
				$dao->deleteFromId($partnerId);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$partnerId = $request->getQuery('id', null);
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$partnerId = $request->getPost('id', null);
			$validate = $request->getPost('validate', false);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permission */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Somebody::$classId);
		$partner = new Somebody();
		$dao->loadFromId($partner, $partnerId);

		$form = new \Partner\Form\EditForm($view, $factory);
		$form->setAttribute('action', '#');
		$form->bind($partner);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->validate() ) {
				try {
					$dao->save($partner);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Partner ' . $partner->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$validate = $request->getPost('validate', false);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permission */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Somebody::$classId);
		$partner = Somebody::init();

		$form = new \Partner\Form\EditForm($view, $factory);
		$form->setAttribute('action', '#');
		$form->bind($partner);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->validate() ) {
				try {
					$dao->save($partner);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Create New Partner';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function exportAction()
	{
		/* Check permission */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* */
		$list = $this->partner->GetAllPartners();
		$this->view->list = $list;

		header("Content-type: application/csv ");
		header("Content-Disposition: attachment; filename=export_partners.csv ");
	}
} /* End of class */
