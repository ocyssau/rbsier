<?php
namespace Partner\Form;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @param \Rbs\Space\Factory $factory
	 * @param string $nameSpace
	 */
	public function __construct($factory, $nameSpace)
	{
		parent::__construct($factory, $nameSpace);

		$this->template = 'partner/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Find */
		$list = array(
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'partner_type' => 'Type',
			'adress' => 'Adress',
			'city' => 'City',
			'zip_code' => 'Zip Code',
			'phone' => 'Phone',
			'cell_phone' => 'Cell Phone',
			'mail' => 'Mail',
			'web_site' => 'Web Site',
			'activity' => 'Activity',
			'company' => 'Company'
		);
		$this->add(array(
			'name' => 'find_field',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'Search for first name...',
				'class' => 'form-control submitOnChange'
			),
			'options' => array(
				'label' => tra('In'),
				'value_options' => $list
			)
		));
		$inputFilter->add(array(
			'name' => 'find_field',
			'required' => false
		));

		/* Find */
		$this->add(array(
			'name' => 'find',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search...',
				'class' => 'form-control',
				'data-where' => 'name',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Search'
			)
		));
		$inputFilter->add(array(
			'name' => 'find',
			'required' => false
		));

		/* Number */
		$this->add(array(
			'name' => 'find_number',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for number...',
				'class' => 'form-control',
				'data-where' => 'name',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Number'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_number',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(\Rbs\People\Somebody::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* NAME */
		if ( $datas['find_number'] ) {
			$filter->andFind($datas['find_number'], $dao->toSys('number'), Op::CONTAINS);
		}
		/* NAME */
		if ( $datas['find'] ) {
			$filter->andFind($datas['find'], $dao->toSys($datas['find_field']), Op::CONTAINS);
		}

		return $this;
	}
}
