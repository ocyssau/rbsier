<?php
/**
 *
 */
return array(
	'router' => array(
		'routes' => array(
			'workplace-wildspace' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/workplace/wildspace[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Workplace\Controller\Wildspace',
						'action' => 'index',
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Workplace\Controller\Wildspace'=>'Workplace\Controller\WildspaceController',
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Workplace'=>__DIR__ . '/../view',
		),
	),
);
