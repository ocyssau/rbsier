<?php
namespace Workplace\Controller;

use Application\Controller\FilemanagerController;
use Rbplm\People;
use Rbplm\People\User\Wildspace;
use Rbplm\Sys\Filesystem;
use Rbs\Space\Factory as DaoFactory;
use Application\Controller\ControllerException;

/**
 *
 * @author olivier
 *
 */
class WildspaceController extends FilemanagerController
{

	/**
	 *
	 * @var string $pageId
	 */
	public $pageId = 'workplace_wildspace';

	/**
	 *
	 * @var string $defaultSuccessForward
	 */
	public $defaultSuccessForward = 'workplace/wildspace/index';

	/**
	 *
	 * @var string $defaultFailedForward
	 */
	public $defaultFailedForward = 'workplace/wildspace/index';

	/**
	 *
	 * @var \Rbplm\People\User\Wildspace
	 */
	public $wildspace;

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('wildspace');
		$this->layout()->tabs = $tabs;

		$this->basecamp()->setForward($this);
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\FilemanagerController::getDirectory()
	 */
	public function getDirectory(People\User $user)
	{
		return new Wildspace($user);
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\FilemanagerController::getFileService()
	 */
	public function getFileService()
	{
		return new \Service\Controller\Wildspace\FileService();
	}

	/**
	 *
	 * @return \Application\View\ViewModel|string
	 */
	public function indexAction()
	{
		$view = $this->view;
		$this->view->basecamp = $this->defaultSuccessForward;
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward);
		$user = People\CurrentUser::get();
		$wildspace = $this->getDirectory($user);

		/**
		 * Filter and paginations
		 */
		$filter = new Filesystem\Filter('', false);
		$filter->displayMd5 = false;
		$filterForm = new \Workplace\Form\Wildspace\FilterForm($wildspace, null, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		/* */
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->load([
			'default' => [
				'orderby' => 'name',
				'order' => 'asc',
				'limit' => 50
			]
		])
			->setLimitset([
			20,
			50,
			100,
			200
		])
			->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		/**
		 * get infos on files
		 */
		$path = $wildspace->getPath();

		/** @var array $list */
		$list = $wildspace::getDatas($path, $filter);

		$view->list = $list;
		$view->checkoutIndex = DaoFactory::get()->getDao('checkoutindex');
		$view->filter = $filterForm;
		$view->displayMd5 = $filter->displayMd5;
		$view->pageTitle = sprintf(tra('Wildspace Of %s'), $user->getLogin());
		$view->wildspacePath = $path;
		return $view;
	}

	/**
	 *
	 * @return \Application\View\ViewModel|string
	 */
	public function initAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$id = $request->getQuery('id', null);
			$checked = $request->getQuery('checked', null);
			if ( is_array($checked) ) {
				$id = current($checked);
			}
		}
		else {
			throw new \Exception('Must be a HTTP GET Request');
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', \Acl\Model\Resource\Acl::$appCn);

		try {
			$factory = DaoFactory::get();
			$user = new People\User();
			$factory->getDao(People\User::$classId)->loadFromId($user, $id);
			$wildspace = $this->getDirectory($user);
			$wildspace->init(false);
		}
		catch( \Rbplm\Dao\NotExistingException $e ) {
			throw new ControllerException(sprintf('User Id %s is not existing', $id));
		}

		$view->wildspace = $wildspace;
		$view->path = $wildspace->getPath();
		$view->user = $user;
		$view->pageTitle = sprintf('Wildspace of user %s', $user->getLogin());
		return $view;
	}
}
