<?php
namespace Workplace\Form\Wildspace;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{
	/**
	 * 
	 * @var boolean
	 */
	public $needUpdate = false;
	
	/**
	 * 
	 * @var boolean
	 */
	public $displayNew = false;
	
	/**
	 * 
	 * @var boolean
	 */
	public $displayMd5 = false;
	
	/**
	 * @param \Rbplm\People\User\Wildspace $wildspace
	 * @param \Rbs\Space\Factory $factory
	 * @param string $namespace
	 */
	public function __construct($wildspace, $factory, $namespace)
	{
		parent::__construct($factory, $namespace);

		$this->template = 'workplace/wildspace/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$this->wildspace = $wildspace;

		/* Name */
		$this->add(array(
			'name' => 'find_name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Input Name here',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Name'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_name',
			'required' => false
		));

		/* displayMd5 */
		$this->add(array(
			'name' => 'displayMd5',
			'type' => \Zend\Form\Element\Checkbox::class,
			'attributes' => array(
				'class' => 'form-control submitOnClick',
				'id' => 'displayMd5'
			),
			'options' => array(
				'label' => 'Display Md5'
			)
		));
		$inputFilter->add(array(
			'name' => 'displayMd5',
			'required' => false
		));
		
		
		/* needUpdate */
		$this->add(array(
			'name' => 'needUpdate',
			'type' => \Zend\Form\Element\Checkbox::class,
			'attributes' => array(
				'class' => 'form-control',
				'id' => 'needUpdate',
				'title' => 'display only files with ranchbe document and require to be checkin in vault'
			),
			'options' => array(
				'label' => 'Need Update Only'
			)
		));
		$inputFilter->add(array(
			'name' => 'needUpdate',
			'required' => false
		));
		
		/* displayNew */
		$this->add(array(
			'name' => 'displayNew',
			'type' => \Zend\Form\Element\Checkbox::class,
			'attributes' => array(
				'class' => 'form-control submitOnClick',
				'id' => 'displayNew'
			),
			'options' => array(
				'label' => 'Display Only New'
			)
		));
		$inputFilter->add(array(
			'name' => 'displayNew',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 * @param \Rbplm\Sys\Filesystem\Filter $filter
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* NAME */
		if ( $datas['find_name'] ) {
			$name = $datas['find_name'];
			$filter->andFind($name, 'name', Op::OP_CONTAINS);
		}

		if ( $datas['displayMd5'] ) {
			$filter->displayMd5 = true;
		}

		if ( $datas['displayNew'] ) {
			$filter->displayNew = true;
		}
		
		if ( $datas['needUpdate'] ) {
			$filter->needUpdate = true;
		}
		
		$filter->sort('name', 'asc');

		return $this;
	}
}

