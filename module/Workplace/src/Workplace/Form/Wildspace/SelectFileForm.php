<?php
namespace Workplace\Form\Wildspace;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @author olivier
 *
 */
class SelectFileForm extends Form implements InputFilterProviderInterface
{

	public $template;

	public $wildspace;

	/**
	 * @param \Rbplm\People\User\Wildspace $wildspace
	 * @param \Rbplm\Sys\Filesystem\Filter [OPTIONAL] $filter
	 */
	public function __construct($wildspace, $filter = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('selectFile');

		$this->template = 'workplace/wildspace/selectfileform.phtml';
		$this->wildspace = $wildspace;

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'documentid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* FILES */
		$path = $wildspace->getPath();
		$list = $wildspace::getDatas($path, $filter);
		$selectSet = [];
		foreach( $list as $file ) {
			$selectSet[$file['name']] = $file['name'];
		}
		$this->add(array(
			'name' => 'file',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'data-live-search' => true,
				'class' => 'form-control selectpicker rb-select rb-select-file'
			),
			'options' => array(
				'label' => tra('Select File'),
				'options' => $selectSet
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'documentid' => array(
				'required' => false
			),
			'file' => array(
				'required' => false
			)
		);
	}
}
