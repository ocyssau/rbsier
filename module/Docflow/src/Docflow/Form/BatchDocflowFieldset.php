<?php
namespace Docflow\Form;

use Zend\Form\Fieldset;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class BatchDocflowFieldset extends Fieldset implements InputFilterProviderInterface
{

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('batchform');
		$this->setHydrator(new Hydrator(false));

		/* */
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'items',
			'options' => array(
				'label' => '',
				'count' => 0,
				'should_create_template' => true,
				'allow_add' => true,
				'target_element' => array(
					'type' => 'Docflow\Form\BatchItemFieldset'
				)
			)
		));

		$this->add(array(
			'type' => 'text',
			'name' => 'process',
			'attributes' => array(
				'class' => 'form-control',
				'disabled' => 'disabled',
				'size' => '120'
			),
			'options' => array(
				'label' => 'Process'
			)
		));

		$this->add(array(
			'type' => 'hidden',
			'name' => 'spacename'
		));

		$this->add(array(
			'type' => 'hidden',
			'name' => 'containerId'
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Select',
			'name' => 'activityId',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Next activity')
			)
		));
	}

	/**
	 *
	 * @param \Docflow\Model\BatchDocflow $batch
	 * @return array
	 */
	public function getActivitiesOptions($batch)
	{
		$activities = null;
		$ret = array();
		$process = $batch->getProcess();
		$processDao = $batch->factory->getDao(\Workflow\Model\Wf\Process::$classId);
		$item = $batch->getItem(0);
		if ( !$item ) {
			return $ret;
		}
		$instance = $item->getInstance();

		/* A instance of process is runnings */
		if ( $instance ) {
			$docLinkDao = $batch->factory->getDao(\Rbs\Wf\Instance\DocumentLink::$classId);
			$activities = $docLinkDao->getNextActivitiesFromDocumentId($item->getDocument()
				->getId());
		}
		/* ...else an instance must be started */
		else {
			$activities = $processDao->getActivities($process->getId(), 'start');
		}

		/* Generate select activity */
		if ( $activities ) {
			foreach( $activities as $activity ) {
				$ret[$activity['id']] = $activity['title'];
			}
		}

		/* Generate select standalone activity */
		$standalones = $processDao->getActivities($process->getId(), 'standalone');
		if ( $standalones ) {
			foreach( $standalones as $activity ) {
				$ret[$activity['id']] = $activity['title'];
			}
		}

		return $ret;
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'items' => array(
				'required' => false
			),
			'activityId' => array(
				'required' => false
			),
			'containerId' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => false
			)
		);
	}
}