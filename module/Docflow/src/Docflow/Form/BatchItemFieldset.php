<?php
namespace Docflow\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;

/**
 *
 *
 */
class BatchItemFieldset extends Fieldset implements InputFilterProviderInterface
{

	/**
	 * 
	 */
	public function __construct()
	{
		parent::__construct('itemform');
		$this->setHydrator(new Hydrator(false));
		$this->setObject(new \Docflow\Model\BatchDocflow\Item()); /* because bug https://github.com/zendframework/zendframework/issues/3373 */

		/* Hiddens */
		$this->add(array(
			'type' => 'hidden',
			'name' => 'documentId'
		));

		/* Number */
		$this->add(array(
			'type' => 'text',
			'name' => 'fullId',
			'attributes' => array(
				'class' => 'form-control',
				'disabled' => 'disabled',
				'size' => 60
			),
			'options' => array(
				'label' => 'Number'
			)
		));

		/* Life Stage */
		$this->add(array(
			'type' => 'text',
			'name' => 'lifeStage',
			'attributes' => array(
				'class' => 'form-control',
				'disabled' => 'disabled',
				'size' => 15
			),
			'options' => array(
				'label' => 'Life Stage'
			)
		));

		/* Feedbacks */
		$this->add(array(
			'type' => 'textArea',
			'name' => 'result',
			'attributes' => array(
				'class' => 'form-control',
				'disabled' => 'disabled',
				'rows' => 1,
				'cols' => 50
			),
			'options' => array(
				'label' => 'Feedbacks'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'documentId' => array(
				'required' => false
			),
			'fullId' => array(
				'required' => false
			),
			'lifeStage' => array(
				'required' => false
			),
			'result' => array(
				'required' => false
			)
		);
	}
}
