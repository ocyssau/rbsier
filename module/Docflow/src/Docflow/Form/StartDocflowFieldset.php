<?php
namespace Docflow\Form;

use Zend\Form\Fieldset;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Workflow\Model\Wf;

/**
 *
 *
 */
class StartDocflowFieldset extends Fieldset implements InputFilterProviderInterface
{

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 * 
	 * @var array
	 */
	public $processes = array();

	/**
	 * @param \Rbs\Space\Factory $daoFactory
	 */
	public function __construct($daoFactory)
	{
		/* we want to ignore the name passed */
		parent::__construct('batchform');
		$this->setHydrator(new Hydrator(false));

		/* */
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'items',
			'options' => array(
				'label' => '',
				'count' => 0,
				'should_create_template' => false,
				'allow_add' => true,
				'target_element' => array(
					'type' => 'Docflow\Form\BatchItemFieldset'
				)
			)
		));

		/* Construct object for normal select */
		$this->add(array(
			'name' => 'processId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control process-selector',
				'multiple' => false
			),
			'options' => array(
				'label' => 'Process',
				'value_options' => $this->getProcessSet($daoFactory)
			)
		));

		/* */
		$this->add(array(
			'type' => 'hidden',
			'name' => 'spacename'
		));

		/**/
		$this->add(array(
			'type' => 'hidden',
			'name' => 'containerId'
		));
	}

	/**
	 * @return array
	 */
	public function getProcessSet($factory)
	{
		/* Get context */
		$containerId = \Ranchbe::get()->getServiceManager()->getContext()->getData('containerId');

		/* init vars */
		$selectSet = array(
			null => '...'
		);
		$processes = array();

		if ( !$containerId ) {
			$list = $factory->getList(Wf\Process::$classId)->load("isActive=1 LIMIT 1000");
			foreach( $list as $process ) {
				$name = $process['name'] . ' ' . $process['version'] . ' - ' . $process['title'];
				$selectSet[$process['id']] = $name;
				$processes[$process['id']] = $process;
			}
		}
		/* if context is set, get process from projet/container inherits tree */
		else {
			/* @var \Rbs\Wf\ProcessDao $processDao */
			$processDao = $factory->getDao(Wf\Process::$classId);

			/* @var \Rbs\Space\Workitem\Container\Link\ProcessDao $linkProcessDao */
			$linkProcessDao = $factory->getDao(\Rbs\Org\Container\Link\Process::$classId);
			$filter = sprintf('child.%s=1 AND child.%s=1', $processDao->toSys('isValid'), $processDao->toSys('isActive'));

			/* @var \PDOStatement $stmt */
			$stmt = $linkProcessDao->getInheritsFromParentId($containerId, array(
				'child.*'
			), $filter);

			while( $process = $stmt->fetch() ) {
				$name = $process['name'] . ' ' . $process['version'] . ' - ' . $process['title'];
				$selectSet[$process['id']] = $name;
				$processes[$process['id']] = $process;
			}
		}

		$this->processes = $processes;
		return $selectSet;
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'items' => array(
				'required' => false
			),
			'processId' => array(
				'required' => true
			),
			'containerId' => array(
				'required' => false
			),
			'spacename' => array(
				'required' => true
			)
		);
	}
}