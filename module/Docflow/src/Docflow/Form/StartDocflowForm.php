<?php
namespace Docflow\Form;

use Zend\Form\Form;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * 
 *
 */
class StartDocflowForm extends Form implements InputFilterProviderInterface
{

	/**
	 *
	 * @param \Rbs\Space\Factory $daoFactory
	 */
	public function __construct($daoFactory)
	{
		/* we want to ignore the name passed */
		parent::__construct('docflowform');
		$this->setAttribute('method', 'post')->setHydrator(new Hydrator(false));
		$this->template = 'docflow/form/startdocflowform';

		/* construct batchform Fieldset */
		$batchform = new StartDocflowFieldset($daoFactory);
		$batchform->setUseAsBaseFieldset(true);
		$this->add($batchform);

		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Start',
				'class' => 'btn btn-success'
			)
		));

		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Cancel',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array();
	}
} /* End of class */
