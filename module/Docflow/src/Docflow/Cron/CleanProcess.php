<?php
//%LICENCE_HEADER%
namespace Docflow\Cron;

use Rbs\Batch\CallbackInterface;
use Workflow\Model\Wf;
use Rbplm\Dao\Connexion;
use Rbs\Dao\Sier\MetamodelTranslator;

/**
 *
 */
class CleanProcess implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/** @var integer Time in days before delete completed process instances */
	protected $retentionDelay = 365;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;

		$config = \Ranchbe::get()->getConfig();
		$delay = (int)$config['workflow.instance.retention.delay'];

		isset($params['retentionDelay']) ? $this->retentionDelay = $params['retentionDelay'] : $this->retentionDelay = $delay;
	}

	/**
	 *
	 */
	public function run()
	{
		$delay = $this->retentionDelay;
		$older = new \DateTime();
		$older->sub(new \DateInterval('P' . $delay . 'D'));

		echo 'Cleanup of obsolete ended process instance older than ' . $delay . " days \n";

		self::cleanUp($older);
		return $this;
	}

	/**
	 * @param string $path
	 * @param integer $older Timestamp
	 */
	public static function cleanUp($older)
	{
		$factory = \Rbs\Space\Factory::get();
		$connexion = Connexion::get();

		$table = $factory->getTable(Wf\Instance::$classId);

		$sql = "DELETE FROM $table WHERE (status='completed' OR status='aborted') AND ended < :older";
		$bind = array(
			':older' => MetamodelTranslator::datetimeToSys($older)
		);

		echo 'SQL QUERY USED :' . "\n";
		echo $sql . "\n";
		echo 'BINDED PARAMETERS :' . "\n";
		var_export($bind) . "\n";

		$stmt = $connexion->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}
}
