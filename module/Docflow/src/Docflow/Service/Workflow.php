<?php
namespace Docflow\Service;

use Workflow\Model\Wf;
use Docflow\Service\Workflow\Activity\Code as ActivityCode;

/**
 *
 * @author olivier
 *        
 */
class Workflow extends \Workflow\Service\Workflow
{

	/**
	 * @var \Application\Helper\ErrorStack
	 */
	public $errorStack;

	/**
	 *
	 * @var string
	 */
	public $url;

	/**
	 *
	 * @param string $url        	
	 * @return \Docflow\Service\Workflow
	 */
	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	/**
	 *
	 * @param Wf\Process $process        	
	 * @param Wf\Activity $activity        	
	 * @return ActivityCode
	 */
	public function getCode(Wf\Process $process, Wf\Activity $activity)
	{
		$code = new ActivityCode($process, $activity);
		return $code;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Workflow\Service\Workflow::export()
	 */
	public function export(Wf\Process $process)
	{
		$return = parent::export($process);

		$activityDao = $this->getFactory()->getDao(Wf\Activity::$classId);

		foreach( $return['activities'] as $k => $act ) {
			$activityId = $act['id'];

			$activity = new Wf\Activity();
			$activityDao->loadFromId($activity, $activityId);

			$code = new ActivityCode($process, $activity);

			$return['activities'][$k]['scripts'] = [
				'form' => [
					'file' => $code->getFormFile(),
					'content' => ''
				],
				'model' => [
					'file' => $code->getModelFile(),
					'content' => ''
				],
				'template' => [
					'file' => $code->getTemplateFile(),
					'content' => ''
				]
			];

			$scripts = $return['activities'][$k]['scripts'];

			foreach( $scripts as $script ) {
				$file = $script['file'];

				if ( is_readable($file) ) {
					$script['content'] = base64_encode(file_get_contents($file));
					$script['file'] = basename($file);
				}
				else {
					$this->errorStack->log("Script file $file is not readable");
				}
			}

			$return['activities'][$k]['scripts'] = $scripts;
		}

		return $return;
	}

	/**
	 *
	 * @param string $data Json
	 * @param string $version
	 * @param string $name
	 * @return \Workflow\Model\Wf\Process
	 */
	public function import($data, $version = null, $name = null)
	{
		$process = parent::import($data, $version, $name);

		/* init scripts directories */
		$code = new \Docflow\Service\Workflow\Code($process);
		$code->init();
		$code = null;

		$data = json_decode($data, true);

		/* @var \Workflow\Model\Wf\Activity $activity */
		foreach( $process->getActivities() as $activity ) {
			$name = $activity->getName();

			/* search name in input data */
			foreach( $data['activities'] as $actEntity ) {
				if ( $actEntity['name'] == $name ) {

					if ( !isset($actEntity['scripts']) ) {
						break;
					}

					$scripts = $actEntity['scripts'];

					$code = new ActivityCode($process, $activity);

					$file = $code->getFormFile();
					$content = base64_decode($scripts['form']['content']);
					file_put_contents($file, $content);

					$file = $code->getModelFile();
					$content = base64_decode($scripts['model']['content']);
					file_put_contents($file, $content);

					$file = $code->getTemplateFile();
					$content = base64_decode($scripts['template']['content']);
					file_put_contents($file, $content);
				}
			}
		}

		return $process;
	}
}
