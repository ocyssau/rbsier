<?php
namespace Docflow\Service\Workflow;

use Workflow\Model\Wf\Activity;

class Shape
{

	/**
	 * shape of graphviz
	 * @see http://www.graphviz.org/doc/info/shapes.html
	 */
	const TYPE_START = 'house';

	const TYPE_END = 'invhouse';

	const TYPE_ABSTRACT = 'box';

	const TYPE_ACTIVITY = 'folder';

	const TYPE_SPLIT = 'trapezium';

	const TYPE_JOIN = 'invtrapezium';

	const TYPE_STANDALONE = 'note';

	const TYPE_SWITCH = 'diamond';

	public static function getShapeFromType($type)
	{
		switch ($type) {
			case Activity::TYPE_ACTIVITY:
				return self::TYPE_ACTIVITY;
				break;
			case Activity::TYPE_END:
				return self::TYPE_END;
				break;
			case Activity::TYPE_JOIN:
				return self::TYPE_JOIN;
				break;
			case Activity::TYPE_SPLIT:
				return self::TYPE_SPLIT;
				break;
			case Activity::TYPE_STANDALONE:
				return self::TYPE_STANDALONE;
				break;
			case Activity::TYPE_START:
				return self::TYPE_START;
				break;
			case Activity::TYPE_SWITCH:
				return self::TYPE_SWITCH;
				break;
			default:
				return self::TYPE_ABSTRACT;
				break;
		}
	}
}
