<?php
// %LICENCE_HEADER%
namespace Docflow\Service\Workflow\Activity;

use Workflow\Model\Wf;
use Docflow\Service\Workflow;
use Exception;
use Zend\Code\Reflection;
use Zend\Code\Generator;

/**
 *
 *
 */
class Code
{

	/**
	 *
	 * @var string
	 */
	protected $basePath;

	/**
	 *
	 * @var string
	 */
	protected $templateBasePath;

	/**
	 *
	 * @var string
	 */
	protected $template;

	/**
	 *
	 * @var string
	 */
	protected $namespace;

	/**
	 *
	 * @var string
	 */
	protected $modelClassName;

	/**
	 *
	 * @var string
	 */
	protected $modelClassFile;

	/**
	 *
	 * @var string
	 */
	protected $formClassName;

	/**
	 *
	 * @var string
	 */
	protected $formClassFile;

	/**
	 *
	 * @var string
	 */
	protected $templateFile;

	/**
	 *
	 * @var Wf\Activity
	 */
	protected $activity;

	/**
	 *
	 * @var Wf\Process
	 */
	protected $process;

	/**
	 * 
	 * @param Wf\Process $process
	 * @param Wf\Activity $activity
	 */
	public function __construct(Wf\Process $process, Wf\Activity $activity)
	{
		$this->process = $process;
		$this->activity = $activity;
		$this->basePath = Workflow\Code::getBasePath($process);
		$this->templateBasePath = Workflow\Code::getTemplatePath($process);

		/* to prevent the first characters of class name as number */
		$actPrefix = 'Act';
		$formPrefix = 'Form';

		$normalizedName = str_replace('_', '', $activity->getNormalizedName());

		$this->modelClassFile = $this->basePath . '/' . $actPrefix . ucfirst($normalizedName) . '.php';
		$this->formClassFile = $this->basePath . '/' . $formPrefix . ucfirst($normalizedName) . '.php';
		$this->templateFile = $this->templateBasePath . '/' . $normalizedName . '.phtml';

		/* string length of the processPath */
		$strLen = strlen(Workflow\Code::$processPath);
		$rootNamespace = Workflow\Code::$processNamespace;

		/* Model Class */
		$modelClassName = substr($this->modelClassFile, $strLen, -4);
		$this->modelClassName = $rootNamespace . str_replace('/', '\\', $modelClassName);

		/* Namespace */
		$this->namespace = trim($rootNamespace, '\\') . '\\' . str_replace('/', '\\', trim(dirname($modelClassName), '/'));

		/* Formulaires Class */
		$formClassName = substr($this->formClassFile, $strLen, -4);
		$this->formClassName = $rootNamespace . str_replace('/', '\\', $formClassName);

		$template = substr($this->templateFile, $strLen, -6);
		$this->template = trim($template, '/');
	}

	/**
	 */
	public function delete()
	{
		if ( is_file($this->modelClassFile) ) {
			unlink($this->modelClassFile);
		}
		if ( is_file($this->formClassFile) ) {
			unlink($this->formClassFile);
		}
		if ( is_file($this->templateFile) ) {
			unlink($this->templateFile);
		}
		return $this;
	}

	/**
	 */
	public function generateModelCode()
	{
		$classFile = $this->modelClassFile;

		if ( !is_file($classFile) ) {
			$activityName = $this->activity->getNormalizedName();
			$processName = str_replace('_', '', $this->process->getNormalizedName());
			$currentUserName = '';
			$namespace = $this->namespace;

			$class = Generator\ClassGenerator::fromReflection(new Reflection\ClassReflection('Docflow\Service\Workflow\Callback\Activity'));

			$docblock = new Generator\DocBlockGenerator();
			$docblock->setShortDescription('A class for activity ' . $activityName);
			$date = new \DateTime();

			$tag = new Generator\DocBlock\Tag\GenericTag();
			$docblock->setTag($tag->setName('package')
				->setContent($processName));

			$tag = new Generator\DocBlock\Tag\GenericTag();
			$docblock->setTag($tag->setName('generated')
				->setContent($date->format(\DateTime::ISO8601)));

			$tag = new Generator\DocBlock\Tag\GenericTag();
			$docblock->setTag($tag->setName('author')
				->setContent($currentUserName));

			$className = explode('\\', $this->modelClassName);
			$class->setName(end($className))
				->setExtendedClass('AbstractActivity')
				->addUse('Docflow\Service\Workflow\Callback\AbstractActivity')
				->setDocblock($docblock)
				->setNamespaceName($namespace);

			$file = new Generator\FileGenerator();
			file_put_contents($classFile, $file->setClass($class)->generate());
			chmod($classFile, 0777);
		}
	}

	/**
	 */
	public function generateFormCode()
	{
		$classFile = $this->formClassFile;

		if ( !is_file($classFile) ) {
			$baseDir = dirname($classFile);
			if ( !is_dir($baseDir) ) {
				throw new Exception("Directory $baseDir is not existing");
			}

			$activityName = $this->activity->getNormalizedName();
			$processName = str_replace('_', '', $this->process->getNormalizedName());
			$currentUserName = '';
			$namespace = $this->namespace;

			$class = Generator\ClassGenerator::fromReflection(new Reflection\ClassReflection('Docflow\Service\Workflow\Callback\ActivityForm'));
			$docblock = new Generator\DocBlockGenerator();
			$docblock->setShortDescription('A class for activity form ' . $activityName);
			$date = new \DateTime();

			$tag = new Generator\DocBlock\Tag\GenericTag();
			$docblock->setTag($tag->setName('package')
				->setContent($processName));

			$tag = new Generator\DocBlock\Tag\GenericTag();
			$docblock->setTag($tag->setName('generated')
				->setContent($date->format(\DateTime::ISO8601)));

			$tag = new Generator\DocBlock\Tag\GenericTag();
			$docblock->setTag($tag->setName('author')
				->setContent($currentUserName));

			$className = explode('\\', $this->formClassName);
			$class->setName(end($className))
				->setDocblock($docblock)
				->setExtendedClass('AbstractForm')
				->addUse('Docflow\Service\Workflow\Callback\AbstractForm')
				->setNamespaceName($namespace);

			$file = new Generator\FileGenerator();

			$ok = file_put_contents($classFile, $file->setClass($class)->generate());
			if ( !$ok ) {
				throw new Exception(sprintf('Unable to write in file %s'), $classFile);
			}
			chmod($classFile, 0777);
		}

		$templateFile = $this->templateFile;
		if ( !is_file($templateFile) ) {
			$baseDir = dirname($templateFile);
			if ( !is_dir($baseDir) ) {
				throw new Exception("Directory $baseDir is not existing");
			}
			copy('module/Docflow/src/Docflow/Service/Workflow/Callback/view/activity.phtml', $templateFile);
		}
	}

	/**
	 *
	 * @return string
	 */
	public function getModelFile()
	{
		return $this->modelClassFile;
	}

	/**
	 *
	 * @return string
	 */
	public function getModelClass()
	{
		return $this->modelClassName;
	}

	/**
	 *
	 * @return string
	 */
	public function getFormFile()
	{
		return $this->formClassFile;
	}

	/**
	 *
	 * @return string
	 */
	public function getFormClass()
	{
		return $this->formClassName;
	}

	/**
	 *
	 * @return string
	 */
	public function getTemplateFile()
	{
		return $this->templateFile;
	}

	/**
	 *
	 * @return string
	 */
	public function getTemplate()
	{
		return $this->template;
	}
}
