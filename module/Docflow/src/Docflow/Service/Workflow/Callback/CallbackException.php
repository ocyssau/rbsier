<?php
namespace Docflow\Service\Workflow\Callback;

/**
 * Exception for callback.
 * Throw when a callback failed.
 * 
 * @author ocyssau
 *
 */
class CallbackException extends \Exception
{
}
