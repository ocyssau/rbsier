<?php
namespace Docflow\Service\Workflow\Callback;

use Workflow\Model\Event;

/**
 */
class Activity extends AbstractActivity
{

	/**
	 *
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(Event $event)
	{
		return parent::trigger($event);
	}
}
