<?php
namespace Docflow\Service\Workflow\Callback;

/**
 * 
 *
 */
class ActivityWithViewException extends \Exception
{

	public $view;

	/**
	 *
	 */
	public function __construct($view, $message = null)
	{
		$this->view = $view;
		$this->message = $message;
	}
} /* End of class */