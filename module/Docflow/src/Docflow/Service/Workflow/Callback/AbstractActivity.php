<?php
namespace Docflow\Service\Workflow\Callback;

use Zend\View\Model\ViewModel;
use Workflow\Model\Event;

/**
 * Generic callback class.
 * Must be extends by processes callbacks classes.
 */
abstract class AbstractActivity
{

	/**
	 *
	 * @var \Docflow\Service\Workflow
	 */
	public $workflow;

	/**
	 *
	 * @var \Zend\ServiceManager\ServiceManager
	 */
	public $serviceManager;

	/**
	 *
	 * @var \Zend\Mvc\Controller\AbstractController
	 */
	public $controller;

	/**
	 *
	 * @var \Zend\View\Model\ViewModel
	 */
	public $view;

	/**
	 *
	 * @var \Docflow\Service\Workflow\Callback\ActivityForm
	 * 
	 */
	public $form;

	/**
	 *
	 * @param \Docflow\Service\Workflow $workflow
	 */
	public function __construct($workflow)
	{
		$this->workflow = $workflow;
		$this->serviceManager = $workflow->serviceManager;
		$this->id = $workflow->lastActivity->getActivity(true);
	}

	/**
	 *
	 * @param \Zend\View\Model\ViewModel $view
	 */
	public function setView($view)
	{
		$this->view = $view;
	}

	/**
	 *
	 * @var Event $event
	 * @return $event;
	 */
	public function trigger(Event $event)
	{
		return $event;
	}

	/**
	 * @param Event $event
	 * @param \Docflow\Service\Workflow\Callback\AbstractForm $form
	 * @param string $template
	 */
	public function runForm(Event $event, $form, $template)
	{
		$view = new ViewModel();
		$view->setTemplate($template);
		$view->form = $form;
		$view->title = 'Select Next For Switch Activity';
		$isComment = $this->workflow->lastActivity->isComment();

		/* @var \Zend\Http\PhpEnvironment\Request $request */
		$request = $this->serviceManager->get('request');
		$validate = $request->getPost($form->id, false);

		$this->form = $form;
		if ( $isComment ) {
			$form->addComment();
		}
		$form->bind($this);

		if ( $request->isPost() && $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$nextTransition = $request->getPost('next');
				$comment = $request->getPost('comment', '');
				$this->workflow->setNextTransition($nextTransition);
				$this->workflow->lastActivity->setComment($comment);
				$this->trigger($event);
				return $view;
			}
		}
		else {
			throw new ActivityWithViewException($view, 'This activity require to display a view');
		}

		return $view;
	}

	/**
	 *
	 * @param ViewModel $view
	 */
	public function render($view)
	{
		die('RENDER IS NOT VALID');
		$viewRender = $this->serviceManager->get('ViewRenderer');
		$layout = new ViewModel();
		$layout->setTemplate('layout/ranchbe');
		$layout->setVariable('content', $viewRender->render($view));
		echo $viewRender->render($layout);
	}

	/**
	 * Implement arrayObject interface
	 *
	 * @param array $properties
	 */
	public function getArrayCopy()
	{
		$return = array();
		foreach( $this as $name => $value ) {
			if ( $value instanceof \DateTime ) {
				$return[$name] = $value->format('d-m-Y');
			}
			elseif ( is_scalar($value) ) {
				$return[$name] = $value;
			}
		}
		return $return;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 *        	\PDO fetch result to load
	 * @return AbstractActivity
	 */
	public function populate(array $properties)
	{
		foreach( $properties as $name => $value ) {
			$this->$name = $value;
		}
		return $this;
	}
}
