<?php
namespace Docflow\Service\Workflow\Callback;

/**
 */
class End extends AbstractActivity
{

	/**
	 *
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\Activity::trigger()
	 */
	public function trigger()
	{
		return parent::trigger();
	}
}
