<?php
namespace Docflow\Service\Workflow\Callback;

/**
 * Exception for callback.
 * Throw for informe about Callback execution
 * 
 * @author ocyssau
 *
 */
class InformationException extends \Exception
{
}
