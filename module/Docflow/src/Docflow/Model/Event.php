<?php
namespace Docflow\Model;

use Rbs\Space\Factory as DaoFactory;
use Rbs\History\HistoryEventInterface;
use Rbs\EventDispatcher\StoppableEventInterface;
use Rbs\EventDispatcher\EventInterface;

/**
 * VERY generic to see if a task can be stopped
 */
class Event extends \Workflow\Model\Event implements EventInterface, HistoryEventInterface, StoppableEventInterface
{
	use \Rbs\History\EventTrait;
	use \Rbs\EventDispatcher\StoppableEventTrait;

	/**
	 *
	 * @return DaoFactory
	 */
	public function getFactory(): DaoFactory
	{
		try {
			if ( isset($this->emitter->factory) ) {
				return $this->emitter->factory;
			}
			elseif ( isset($this->emitter->document->dao->factory) ) {
				return $this->emitter->document->dao->factory;
			}
		}
		catch( \Throwable $e ) {
			throw new \RuntimeException('emitter is not composed with factory');
		}
	}
}
