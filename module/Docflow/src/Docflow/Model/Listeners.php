<?php
namespace Docflow\Model;

use Rbplm\People;
use Acl\Model\UserRole;
use Exception as EventException;
use Docflow\Service\Workflow;
use Rbs\Ged\Document\History\Listener as HistoryListener;
use Docflow\Service\Workflow\Activity\Code as ActivityCode;
use Docflow\Service\Workflow\Code as ProcessCode;
use Rbs\Dao\Loader as DaoLoader;
use Workflow\Model\Wf;
#use Rbs\Ged\Document\Event as DocumentEvent;
use Workflow\Model\Event;

/**
 * listener methods used by event manager for workflows
 *
 */
class Listeners
{

	/**
	 * Instanciate and call callback class define on running Activity
	 * 
	 * @param Event $event
	 * @return Event
	 */
	public static function onRunActivity(Event $event)
	{
		$wfService = $event->getEmitter();
		$actInst = $wfService->lastActivity;
		$instanceOwner = $wfService->instance->getOwner(true);
		$currentUser = People\CurrentUser::get();
		$currentUserLogin = $currentUser->getLogin();
		$factory = $wfService->getFactory();

		/* check role */
		if ( $currentUserLogin != $instanceOwner ) {
			$roles = array_filter($actInst->getRoles());
			if ( count($roles) > 0 ) {
				$userRoles = $factory->getDao(UserRole::$classId)->getRolesOfUser($currentUser->getId());
				if ( $userRoles ) {
					$userRoles = $userRoles->fetchAll(\PDO::FETCH_ASSOC);
				}
				$ok = false;
				while( $role = array_pop($roles) && $ok == false ) {
					if ( $currentUserLogin == $role ) {
						$ok = true;
					}
					elseif ( $userRoles ) {
						foreach( $userRoles as $userRole ) {
							if ( $userRole['name'] == $role ) {
								$ok = true;
							}
						}
					}
				}
				if ( $ok == false ) {
					throw new EventException(sprintf('Current user is not authorized to execute this activity. Check roles for activity "%s"', $actInst->getName()));
				}
			}
		}

		$attr = $actInst->actAttributes;

		(isset($attr['callbackclass'])) ? $callbackclass = $attr['callbackclass'] : $callbackclass = '';
		(isset($attr['callbackmethod'])) ? $callbackmethod = $attr['callbackmethod'] : $callbackmethod = '';

		if ( !isset($wfService->process) ) {
			throw new EventException('Process is not setted');
		}

		if ( $callbackclass == '' ) {
			$code = new \Docflow\Service\Workflow\Activity\Code($wfService->process, $wfService->lastActivity);
			$callbackclass = $code->getModelClass();
			$callbackmethod = 'trigger';
			$formName = $code->getFormClass();
			$template = $code->getTemplate();
		}

		if ( class_exists($callbackclass, true) ) {
			$model = new $callbackclass($wfService);

			/* get associated document */
			$model->document = $wfService->document;

			/* */
			if ( $actInst->isInteractive() ) {

				if ( class_exists($formName, true) ) {
					$form = new $formName($wfService);
					$form->setAttribute('action', $wfService->url);
					$model->runForm($event, $form, $template); // return $view
				}
				else {
					throw EventException("Form class $formName NOT FOUND!");
				}
			}
			else {
				return $model->$callbackmethod($event);
			}
		}
		else {
			throw new EventException("Callback class $callbackclass NOT FOUND!");
		}

		return $event;
	}

	/**
	 *
	 * @param Event $event
	 * @return Event
	 */
	public static function onRunActivityPost(Event $event)
	{
		/* @var Workflow $workflow */
		$workflow = $event->getEmitter();

		$event->setBatch($workflow->batch);
		if ( $workflow->lastActivity->getComment() ) {
			$event->setComment($workflow->lastActivity->getComment());
		}

		/* define action name as displayed in history */
		$actionName = 'runwf:';
		$actionName .= ':' . $workflow->process->getId();
		if ( isset($workflow->instance) ) {
			$actionName .= ':' . $workflow->instance->getId();
		}
		$actionName .= ':' . $workflow->lastActivity->getName();

		$historyEvent = new \Rbs\Ged\Document\Event($event->getName(), $workflow->document, $workflow->getFactory());
		$historyEvent->actionName = $actionName;
		$historyEvent = HistoryListener::onEvent($historyEvent);
		$event->historyEvent = $historyEvent;

		return $event;
	}

	/**
	 *
	 * @param Event $event
	 * @return Event
	 */
	public static function onTranslateActivity(Event $event)
	{
		/* @var Workflow $workflow */
		$workflow = $event->getEmitter();

		if ( isset($workflow->nextStatus) ) {
			$workflow->document->lifeStage = $workflow->nextStatus;
			$workflow->document->dao->updateFromArray($workflow->document->getId(), array(
				'lifeStage' => $workflow->nextStatus
			));
		}

		return $event;
	}

	/**
	 * Trigger
	 * Init and create folders.
	 *
	 * @param Event $process
	 * @return Event
	 */
	public static function onSaveProcessPre(Event $event)
	{
		$worflow = $event->getEmitter();
		try {
			if ( isset($event->processCode) ) {
				$code = $event->processCode;
			}
			else {
				$code = new ProcessCode($worflow->process);
				$event->processCode = $code;
			}
			$code->init();
		}
		catch( \Throwable $e ) {
			throw new EventException($e->getMessage());
		}
		return $event;
	}

	/**
	 * Trigger
	 * Save graph image
	 *
	 * @param Event $event
	 * @return Event
	 */
	public static function onSaveProcessPost(Event $event)
	{
		$worflow = $event->getEmitter();
		try {
			if ( isset($event->processCode) ) {
				$code = $event->processCode;
			}
			else {
				$code = new ProcessCode($worflow->process);
				$event->processCode = $code;
			}
			$code->saveGraph();
		}
		catch( \Throwable $e ) {
			throw new EventException($e->getMessage());
		}
		return $event;
	}

	/**
	 * Trigger
	 * @param Event $event
	 * @return Event
	 */
	public static function onDeleteProcess(Event $event)
	{
		$worflow = $event->getEmitter();
		if ( isset($worflow->deleteWithCode) && $worflow->deleteWithCode == true ) {
			$code = new ProcessCode($worflow->process);
			$code->delete();
			$event->processCode = $code;
		}
		return $event;
	}

	/**
	 *
	 * @param Event $event
	 * @return Event
	 */
	public static function onDeleteActivity(Event $event)
	{
		$worflow = $event->getEmitter();
		$activity = $worflow->activity;

		if ( isset($event->activityCode) ) {
			$code = $event->activityCode;
		}
		else {
			$process = DaoLoader::get()->loadFromId($activity->getProcess(true), Wf\Process::$classId);
			$code = new ActivityCode($process, $activity);
			$event->activityCode = $code;
		}

		$code->delete();

		return $event;
	}

	/**
	 *
	 * @param Event $event
	 * @return Event
	 */
	public static function onSaveActivity(Event $event)
	{
		$worflow = $event->getEmitter();

		try {
			if ( isset($event->activityCode) ) {
				$code = $event->activityCode;
			}
			else {
				$code = new ActivityCode($worflow->process, $event->activity);
				$event->activityCode = $code;
			}
			$code->generateFormCode();
			$code->generateModelCode();
		}
		catch( \Throwable $e ) {
			$event->error = $e->getMessage();
			throw $e;
		}
		return $event;
	}
}
