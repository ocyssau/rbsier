<?php
namespace Docflow\Model\BatchDocflow;

/**
 *
 */
class Result
{

	protected $datas;

	protected $feedbacks;

	protected $errors;

	/**
	 *
	 * @param string $name
	 * @param mixed $datas
	 */
	public function setData($name, $datas)
	{
		$this->datas[$name] = $datas;
	}

	/**
	 *
	 * @param string $msg
	 */
	public function error($msg)
	{
		$this->errors[] = $msg;
	}

	/**
	 *
	 * @param string $msg
	 */
	public function feedback($msg)
	{
		$this->feedbacks[] = $msg;
	}

	/**
	 * @return array
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @return array
	 */
	public function getFeedback()
	{
		return $this->feedbacks;
	}
}
