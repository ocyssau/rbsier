<?php
namespace Docflow\Model\BatchDocflow;

use Workflow\Model\Wf;
use Docflow\Model\IntruderException;

/**
 * A item for BatchDocflow collection
 * 
 * @author ocyssau
 *
 */
class Item
{

	/**
	 * @var Result
	 */
	public $result;

	/**
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 * @var \Rbplm\Ged\Document\Version
	 */
	protected $document;

	/**
	 * @var \Workflow\Model\Wf\Instance
	 */
	protected $instance;

	/**
	 * @var \Workflow\Model\Wf\Process
	 */
	protected $process;

	/**
	 * @var \Docflow\Service\Workflow
	 */
	protected $workflow;

	/**
	 * @param \Docflow\Model\BatchDocflow $batch
	 */
	public function __construct($batch = null)
	{
		$this->result = new Result();
		if ( $batch ) {
			$this->batch = $batch;
			$this->factory = $batch->factory;
		}
	}

	/**
	 *
	 */
	public function getArrayCopy()
	{
		return array(
			'documentId' => $this->document->getId(),
			'uid' => $this->document->getUid(),
			'lifeStage' => $this->document->getLifeStage(),
			'name' => $this->document->getName(),
			'number' => $this->document->getNumber(),
			'version' => $this->document->version,
			'iteration' => $this->document->iteration,
			'owner' => $this->document->getOwner(true),
			'accessCode' => $this->document->checkAccess(),
			'fullId' => $this->document->getNumber() . ' v' . $this->document->version . '.' . $this->document->iteration,
			'feedbacks' => $this->result->getFeedback(),
			'errors' => $this->result->getErrors(),
			'instance' => $this->instance,
			'document' => $this->document
		);
	}

	/**
	 * @return array
	 */
	public function populate($datas)
	{
		foreach( $datas as $name => $value ) {
			$this->$name = $value;
		}
	}

	/**
	 *
	 */
	public function validate()
	{
		$batch = $this->batch;
		$instance = $this->instance;
		$document = $this->document;
		$previous = $batch->getItem(0);

		try {
			/* if a previous instance is set and nothing on current */
			if ( $previous && isset($previous->instance) && !$instance ) {
				throw new IntruderException('Other elements has running processes');
			}
			/* if a previous instance is set and nothing on current */
			else if ( $previous && !isset($previous->instance) && $instance ) {
				throw new IntruderException('Other elements has none running processes');
			}
			/* If a workflow instance is running */
			if ( $instance ) {
				/* Check that activity is identical to previous selected document process : to ensure coherence between selected documents */
				if ( $previous && $previous->instance ) {
					$activities = $instance->getRunningActivities();
					$runningActivity = reset($activities)['activityId'];
					$pActivities = $previous->instance->getRunningActivities();
					$previousActivity = reset($pActivities)['activityId'];
					if ( $runningActivity != $previousActivity ) {
						$msg = 'document %s have not same state that %s. it is not selected';
						$e1 = $document->getName();
						$e2 = $previous->getDocument()->getName();
						$msg = sprintf(tra($msg), $e1, $e2);
						throw new IntruderException($msg);
					}
				}
				$batch->setProcess($instance->getProcess());
			}
			/* If no instances get the process and display activities */
			else {
				/* Check that this item has access free */
				$accessCode = $document->checkAccess();
				if ( !($accessCode == 0 || ($accessCode > 1 && $accessCode <= 5)) ) {
					$this->result->error(tra('This item is not free'));
					return false;
				}
			}
		}
		catch( IntruderException $e ) {
			$this->result->error($e->getMessage());
			return false;
		}

		return true;
	}

	/**
	 * @param \Rbplm\Ged\Document\Version $document
	 */
	public function setDocument($document)
	{
		$this->document = $document;
		$documentId = $document->getId();
		$batch = $this->batch;

		$factory = $this->factory;
		$instanceDao = $factory->getDao(Wf\Instance::$classId);
		$docLinkDao = $factory->getDao(\Rbs\Wf\Instance\DocumentLink::$classId);

		/* get instance associated to document */
		$instanceDefinition = $docLinkDao->getInstanceFromDocumentId($documentId, 'running')->current();

		/* */
		if ( $instanceDefinition ) {
			$this->isRunning = true;
			$instance = new Wf\Instance();
			$instanceDao->hydrate($instance, $instanceDefinition);
			$runningActivities = $instanceDao->getRunningActivities($instance->getId());
			$instance->setRunningActivities($runningActivities);
			$this->setInstance($instance);
			if ( !isset($batch->runningActivities) ) {
				$batch->runningActivities = $runningActivities;
			}

			$process = new Wf\Process();
			$this->factory->getDao(Wf\Process::$classId)->loadFromId($process, $instance->getProcess(true));
			$instance->setProcess($process);
			$this->setInstance($instance);
			$this->setProcess($process);
		}

		return $this;
	}

	/**
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function getDocument()
	{
		return $this->document;
	}

	/**
	 * @return \Workflow\Model\Wf\Instance
	 */
	public function getInstance()
	{
		return $this->instance;
	}

	/**
	 * A working process instance is running, set to Item
	 *
	 * @param \Workflow\Model\Wf\Instance $instance
	 */
	public function setInstance($instance)
	{
		$this->instance = $instance;
		return $this;
	}

	/**
	 * @param \Workflow\Model\Wf\Process $process
	 */
	public function setProcess($process)
	{
		$this->process = $process;
		return $this;
	}

	/**
	 * @return \Workflow\Model\Wf\Process
	 */
	public function getProcess()
	{
		return $this->process;
	}
}
