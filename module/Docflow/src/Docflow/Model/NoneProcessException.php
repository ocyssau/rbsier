<?php
namespace Docflow\Model;

/**
 * Exception for when none process is defined
 *
 */
class NoneProcessException extends \Exception
{
}