<?php
namespace Docflow\Model;

/**
 * Factory to create form definition of workflow to run on document.
 */
class BatchDocflow extends \Rbs\Batch\Batch
{

	/**
	 * @var string
	 */
	public $spacename;

	/**
	 * @var string
	 */
	public $containerId;

	/**
	 * @var array
	 */
	public $items = array();

	/**
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 * @var \Docflow\Model\BatchDocflow\Result
	 */
	protected $result;

	/**
	 * @var \Docflow\Service\Workflow
	 */
	protected $workflow;

	/**
	 * @var \Workflow\Model\Wf\Process
	 */
	protected $process;

	/**
	 * @var array
	 */
	public $runningActivities;

	/**
	 * @var array
	 */
	public $activityId;

	/** @var boolean */
	public $isRunning = false;

	/**
	 *
	 * @param \Docflow\Service\Workflow $workflow
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($workflow, $factory)
	{
		parent::__construct();
		$this->workflow = $workflow;
		$workflow->batch = $this;
		$this->factory = $factory;
		$this->result = new BatchDocflow\Result();
		$this->spacename = $factory->getName();
	}

	/**
	 * @return array
	 */
	public function getArrayCopy()
	{
		return array(
			'id' => $this->id,
			'uid' => $this->uid,
			'ownerId' => $this->ownerId,
			'ownerUid' => $this->ownerUid,
			'comment' => $this->comment,
			'items' => $this->items,
			'processId' => (isset($this->process)) ? $this->process->getId() : null,
			'process' => (isset($this->process)) ? $this->process->getName() . '-v' . $this->process->getVersion() . ' - ' . $this->process->getTitle() : '',
			'activityId' => $this->activityId,
			'spacename' => $this->spacename
		);
	}

	/**
	 * @param array $properties
	 * @return BatchDocflow
	 */
	public function hydrate(array $properties)
	{
		foreach( $properties as $name => $value ) {
			$this->$name = $value;
		}
		return $this;
	}

	/**
	 * @param \Docflow\Model\BatchDocflow\Item $item
	 * @return \Docflow\Model\BatchDocflow
	 */
	public function add($item)
	{
		$this->items[] = $item;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @return \Docflow\Model\BatchDocflow\Item
	 * @param integer $key
	 */
	public function getItem($key)
	{
		if ( isset($this->items[$key]) ) {
			return $this->items[$key];
		}
		return null;
	}

	/**
	 * @return \Docflow\Service\Workflow
	 */
	public function getDocflow()
	{
		return $this->workflow;
	}

	/**
	 * @param \Workflow\Model\Wf\Process $process
	 * @return \Docflow\Model\BatchDocflow
	 */
	public function setProcess($process)
	{
		$this->process = $process;
		return $this;
	}

	/**
	 * @return \Workflow\Model\Wf\Process
	 */
	public function getProcess()
	{
		return $this->process;
	}

	/**
	 * @return boolean
	 */
	public function isStarted()
	{
		foreach( $this->items as $item ) {
			if ( $item->getInstance() ) {
				return true;
			}
		}
		return false;
	}
}
