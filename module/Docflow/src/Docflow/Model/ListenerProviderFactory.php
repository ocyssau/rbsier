<?php
namespace Docflow\Model;

use Rbs\EventDispatcher\ListenerProvider;
use Workflow\Model\Event as Event;
use Docflow\Model\Listeners as WfListener;

/**
 */
class ListenerProviderFactory
{

	/**
	 * @return ListenerProvider
	 */
	public static function get()
	{
		$listenerProvider = new ListenerProvider();

		$listenerProvider->addListener(function (Event $event) {
			if ( $event->getName() == Event::RUNACTIVITY_PRE ) {
				$event = WfListener::onRunActivity($event);
			}
			return $event;
		});
		$listenerProvider->addListener(function (Event $event) {
			if ( $event->getName() == Event::RUNACTIVITY_POST ) {
				$event = WfListener::onRunActivityPost($event);
			}
			return $event;
		});
		$listenerProvider->addListener(function (Event $event) {
			if ( $event->getName() == Event::TRANSLATE_POST ) {
				$event = WfListener::onTranslateActivity($event);
			}
			return $event;
		});
		$listenerProvider->addListener(function (Event $event) {
			if ( $event->getName() == Event::SAVEPROCESS_PRE ) {
				WfListener::onSaveProcessPre($event);
			}
			return $event;
		});
		$listenerProvider->addListener(function (Event $event) {
			if ( $event->getName() == Event::SAVEPROCESS_POST ) {
				WfListener::onSaveProcessPost($event);
			}
			return $event;
		});
		$listenerProvider->addListener(function (Event $event) {
			if ( $event->getName() == Event::DELETEPROCESS_POST ) {
				WfListener::onDeleteProcess($event);
			}
			return $event;
		});
		$listenerProvider->addListener(function (Event $event) {
			if ( $event->getName() == Event::SAVEACTIVITY_POST ) {
				WfListener::onSaveActivity($event);
			}
			return $event;
		});
		$listenerProvider->addListener(function (Event $event) {
			if ( $event->getName() == Event::DELETEACTIVITY_POST ) {
				WfListener::onDeleteActivity($event);
			}
			return $event;
		});

		return $listenerProvider;
	}
}
