<?php
namespace Docflow\Model;

/**
 * Exception for when none process is defined
 *
 */
class IntruderException extends \Exception
{
}