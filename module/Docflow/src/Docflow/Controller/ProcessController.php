<?php
namespace Docflow\Controller;

use Zend\View\Model\ViewModel;
use Workflow\Model\Wf;
use Application\Controller\ControllerException;
use Application\Controller\AbstractController;
use Docflow\Service\Workflow\Activity\Code as ActivityCode;

/**
 * 
 *
 */
class ProcessController extends AbstractController
{

	/**
	 */
	public function savetemplateAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		return new ViewModel();
	}

	/**
	 */
	public function getscriptsrcAction()
	{
		$processId = (int)$this->params()->fromPost('processid', null);
		$activityId = (int)$this->params()->fromPost('activityid', null);

		$request = $this->getRequest();
		$view = new ViewModel();
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		$workflow = $this->getEvent()->getApplication()->getServiceManager()->get('workflow');
		$factory = $workflow->getFactory();

		$process = new Wf\Process();
		$process->dao = $factory->getDao($process->cid);
		$process->dao->loadFromId($process, $processId);

		$activity = new Wf\Activity();
		$activity->dao = $factory->getDao($activity->cid);
		$activity->dao->loadFromId($activity, $activityId);

		$code = new ActivityCode($process, $activity);
		
		$modelFile = $code->getModelFile();
		$tplFile = $code->getTemplateFile();
		$formFile = $code->getFormFile();

		$view->modelFile = $modelFile;
		$view->templateFile = $tplFile;
		$view->formFile = $formFile;

		$view->modelSrc = file_get_contents($modelFile, true);
		$view->templateSrc = file_get_contents($tplFile, true);
		$view->formSrc = file_get_contents($formFile, true);

		return $view;
	}

	/**
	 */
	public function savescriptAction()
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$request = $this->getRequest();
		$view = new ViewModel();
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		$activityId = (int)$this->params()->fromPost('activityid', null);
		$processId = (int)$this->params()->fromPost('processid', null);
		$src = trim($this->params()->fromPost('scriptsrc', null));

		if ( $src != "" ) {
			$workflow = $this->getWorkflowService();
			$factory = $workflow->getFactory();

			$process = new Wf\Process();
			$process->dao = $factory->getDao($process->cid);
			$process->dao->loadFromId($process, $processId);

			$activity = new Wf\Activity();
			$activity->dao = $factory->getDao($activity->cid);
			$activity->dao->loadFromId($activity, $activityId);

			$code = new ActivityCode($process, $activity);
			
			$modelFile = $code->getModelFile();
			if ( is_writable($modelFile) ) {
				file_put_contents($modelFile, $src);
			}
			else {
				throw new ControllerException(sprintf('src code file %s is not writable', $modelFile));
			}
		}

		return $view;
	}
} /* End of class */
