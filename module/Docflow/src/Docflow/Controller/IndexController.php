<?php
namespace Docflow\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Org\Workitem;
use Workflow\Model\Wf;
use Rbplm\Dao\NotExistingException;
use Docflow\Service\Workflow\Callback\ActivityWithViewException;
use Docflow\Service\Workflow\Callback\CallbackException;
use Workflow\Model\Exception as WorkflowException;
use Application\Controller\ControllerException;

/**
 */
class IndexController extends AbstractController
{

	/* */
	public $pageId = 'docflow_index';

	/* */
	public $ifSuccessForward = '/docflow/index';

	/* */
	public $ifFailedForward = '/docflow/index';

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		$this->workflow = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Workflow');

		/* */
		//$this->session = \Ranchbe::get()->getServiceManager()->getSessionManager()->getStorage();

		/* */
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('document');
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		parent::initAjax();

		$this->workflow = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('Workflow');

		/* */
		//$this->session = \Ranchbe::get()->getServiceManager()->getSessionManager();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		/* Inits */
		$view = $this->view;
		$request = $this->getRequest();

		if ( $request->isGet() ) {
			$spacename = $request->getQuery('spacename', $this->session()->spacename);
			$documentIds = $request->getQuery('checked', []);
			$documentId = $request->getQuery('id', false);
			if ( $documentId ) $documentIds[] = $documentId;
			$validate = false;
			$processId = null;
		}
		/* validate form */
		elseif ( $request->isPost() ) {
			$spacename = $request->getPost('spacename', $this->session()->spacename);
			$documentIds = $request->getPost('checked', array());
			$documentId = $request->getPost('documentid', false);
			$processId = $request->getPost('processId', false);
			$cancel = $request->getPost('cancel', false);
			$validate = $request->getPost('validate', false);
			if ( $documentId ) $documentIds[] = $documentId;

			if ( $validate ) {
				$batchFormReturn = $request->getPost('batchform');
				foreach( $batchFormReturn['items'] as $item ) {
					$documentIds[] = $item['documentId'];
				}

				(isset($batchFormReturn['processId'])) ? $processId = $batchFormReturn['processId'] : null;
				(isset($batchFormReturn['spacename'])) ? $spacename = $batchFormReturn['spacename'] : null;
			}

			/* */
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* */
		$this->session()->spacename = $spacename;

		/* Inits helpers */
		$factory = DaoFactory::get($spacename);
		$workflow = $this->workflow->setFactory($factory);
		$workflow->setUrl($this->url()
			->fromRoute('docflow-act-validation'));

		/* to retrieve previous selection -- nice. */
		if ( !$documentIds && $this->session()->caddie ) {
			foreach( $this->session()->caddie as $elemt ) {
				$documentIds[] = $elemt['documentId'];
			}
		}

		/*
		 * Get processes and instances attached to each documents
		 * Populate $bacth with Items
		 */
		$batch = new \Docflow\Model\BatchDocflow($workflow, $factory);
		$this->_loadBatch($batch, $documentIds);
		$process = $batch->getProcess();
		$item0 = $batch->getItem(0);
		if ( $item0 == null ) {
			$this->errorStack()->error('None candidates to run');
			header('Location: ' . $this->url()->fromRoute('home'));
		}
		//$firstContainer = $item0->getDocument()->getParent();

		/* Display start form if none process is currently started on documents of selection */
		if ( $batch->isRunning == false && !$process ) {
			return $this->_displayStartForm($batch, $processId);
		}

		/* */
		if ( $batch->getProcess() == null ) {
			throw new \Exception('None process is found');
		}

		/* init form */
		/* Form */
		$form = new \Docflow\Form\BatchDocflowForm();
		$form->setAttribute('action', $this->url()
			->fromRoute('docflow'));

		/* @var \Docflow\Form\BatchDocflowFieldset $batchForm */
		$batchForm = $form->get('batchform');
		$batchForm->setObject($batch);
		$batchForm->get('activityId')->setValueOptions($batchForm->getActivitiesOptions($batch));
		$form->bind($batch);
		$view->form = $form;

		/**/
		if ( $validate ) {
			if ( $request->isPost() ) {
				$form->setData($request->getPost());
				if ( $form->isValid() ) {
					$ret = $this->_runBatch($batch, $factory);
					if ( $ret instanceof \Zend\View\Model\ViewModel ) {
						return $ret;
					}

					/* save batch uid and id in db */
					$factory->getDao($batch->cid)->save($batch);

					return $this->successForward();
				}
			}
		}

		/* */
		$process = $batch->getProcess();

		/* Get graph Url */
		$graphType = \Docflow\Service\Workflow\GraphViz::$imageType;
		$graph = \Docflow\Service\Workflow\Code::getGraphPath($process) . '/' . $process->getNormalizedName() . '.' . $graphType;
		$graphUrl = str_replace(getcwd() . '/public/', '', $graph);

		/* init view */
		$view->setTemplate($form->template);
		$view->form = $form;
		$view->pageTitle = 'Start Process';
		$view->graphImg = $graphUrl;
		$view->graphBasePath = '';
		$view->graphType = $graphType;

		return $view;
	}

	/**
	 * @param \Docflow\Model\BatchDocflow $batch
	 * @param DaoFactory $factory
	 * @return IndexController
	 */
	protected function _runBatch($batch, $factory)
	{
		$activityInstDao = $factory->getDao(Wf\Instance\Activity::$classId);
		$activityId = $batch->activityId;
		$workflow = $batch->getDocflow();
		$process = $batch->getProcess();
		$workflow->process = $process;
		$workflow->batch = $batch;
		//$wfDocLinkDao = $factory->getDao(\Rbs\Wf\Instance\DocumentLink::$classId);
		$connexion = $factory->getConnexion();
		$this->session()->activityId = $activityId;

		foreach( $batch->getItems() as $item ) {
			$document = $item->getDocument();
			$instance = $item->getInstance();
			$workflow->document = $document;

			/* Start PDO transaction */
			$connexion->beginTransaction();

			/* process is not started */
			if ( $instance == null ) {
				throw new \RuntimeException(sprintf('Instance is not started for document %s', $document->getNumber()));
			}
			/* else run the activity */
			else {
				try {
					$instanceId = $instance->getId();
					$filter = 'obj.' . $activityInstDao->toSys('activityId') . '=:activityId AND obj.' . $activityInstDao->toSys('instanceId') . '=:instanceId';
					$bind = array(
						':activityId' => $activityId,
						':instanceId' => $instanceId
					);
					$actInstance = $activityInstDao->loadWithActivity(new Wf\Instance\Activity(), $filter, $bind);
					$actInstance->setInstance($instance);
					$workflow->runActivity($actInstance)->lastActivity;
				}
				catch( NotExistingException $exception ) {
					/* if none instance its a standalone activity */
					$activity = new Wf\Activity();
					$factory->getDao($activity->cid)->loadFromId($activity, $activityId);
					try {
						$workflow->runStandalone($activity, $instance);
					}
					catch( ActivityWithViewException $exception ) {
						$view = $exception->view;
						$connexion->commit();
						return $view;
					}
					catch( CallbackException $exception ) {
						$this->errorStack()->error($exception->getMessage());
					}
				}
				catch( ActivityWithViewException $exception ) {
					$view = $exception->view;
					$connexion->commit();
					return $view;
				}
				catch( CallbackException $exception ) {
					$this->errorStack()->error($exception->getMessage());
				}
				catch( WorkflowException $exception ) {
					$this->errorStack()->log($exception);
					throw $exception;
				}
				catch( \Throwable $exception ) {
					$this->errorStack()->log($exception);
					throw $exception;
				}
			}
			$connexion->commit();
		}

		return $this;
	}

	/**
	 * @param \Zend\Stdlib\RequestInterface $request
	 * @param array $documentIds
	 * @return IndexController
	 */
	protected function _loadBatch($batch, $documentIds)
	{
		/* get some helpers */
		$factory = $batch->factory;
		//$workflow = $batch->getDocflow();

		/* load DAOs*/
		$documentDao = $factory->getDao(Document\Version::$classId);
		$containerDao = $factory->getDao(Workitem::$classId);
		//$activityInstDao = $factory->getDao(Wf\Instance\Activity::$classId);

		$documentCaddie = array();
		//$validInstance = null;
		$batchItem = null;
		foreach( $documentIds as $documentId ) {
			$document = new Document\Version();
			$documentDao->loadFromId($document, $documentId);

			/* Check permissions */
			$this->getAcl()->checkRightFromUid('edit', $document->parentUid);

			$container = new Workitem();
			$containerDao->loadFromId($container, $document->parentId);
			$document->setParent($container);

			/* Set document and retrieve process and instance linked */
			$batchItem = new \Docflow\Model\BatchDocflow\Item($batch);
			$batchItem->setDocument($document);

			/* validate and filter selection */
			if ( $batchItem->validate() ) {
				$batch->add($batchItem);
				$instance = $batchItem->getInstance();
				$itemCaddie = array(
					'documentId' => $documentId
				);
				if ( $instance ) {
					$itemCaddie['instanceId'] = $instance->getId();
				}
				$documentCaddie[] = $itemCaddie;

				/* running */
				if ( $instance ) {
					$batch->isRunning = true;
				}
			}
			else {
				$msg = implode(' - ', $batchItem->result->getErrors());
				$this->errorStack()->error($msg);
			}
		}

		$batch->newUid()->setOwner(\Rbplm\People\CurrentUser::get());
		$this->session()->caddie = $documentCaddie;
		return $this;
	}

	/**
	 * Set process and display start form
	 * @param \Docflow\Model\BatchDocflow $batch
	 */
	protected function _displayStartForm($batch, $defaultProcessId)
	{
		$factory = $batch->factory;
		$view = $this->view;

		/* Form */
		$form = new \Docflow\Form\StartDocflowForm($factory);
		$form->setAttribute('action', $this->url()
			->fromRoute('docflow-start'));

		/* populate the form */
		$batchForm = $form->get('batchform');

		/* load a default process from first selected document */
		//$batchItem = $batch->getItem(0);
		$process = $batch->getProcess();

		/* If batch have process */
		if ( $process ) {
			$batchForm->get('processId')->setValue($process->getId());
		}
		/* load process from requested */
		else if ( $defaultProcessId ) {
			$process = new Wf\Process();
			$batch->factory->getDao(Wf\Process::$classId)->loadFromId($process, $defaultProcessId);
			$batch->setProcess($process);
		}
		$batchForm->setObject($batch);
		$form->bind($batch);

		$graphType = \Docflow\Service\Workflow\GraphViz::$imageType;
		$graphBasePath = trim(str_replace('public/', '', \Docflow\Service\Workflow\Code::$publicImg), '/');

		if ( $process ) {
			$graph = \Docflow\Service\Workflow\Code::getGraphPath($process) . '/' . $process->getNormalizedName() . '.' . $graphType;
			/* Get graph Url */
			$graphUrl = str_replace(getcwd() . '/public/', '', $graph);
		}
		else {
			$graphUrl = '';
		}

		/* Init view */
		$view->setTemplate($form->template);
		$view->form = $form;
		$view->pageTitle = 'Start Process';
		$view->graphBasePath = $graphBasePath;
		$view->graphType = $graphType;
		$view->graphImg = $graphUrl;

		return $view;
	}

	/**
	 *
	 */
	public function activityformvalidationAction()
	{
		/* Inits */
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$activityId = $request->getPost('id', false);
			$cancel = $request->getPost('cancel', false);
			if ( $cancel ) {
				return $this->successForward();
			}
		}
		else {
			$activityId = $request->getQuery('id', false);
			//throw new \Exception('Http Post only');
		}

		/* Inits helpers */
		$spacename = $this->session()->spacename;
		$factory = DaoFactory::get($spacename);
		$connexion = $factory->getConnexion();
		$documentIds = [];

		/* to retrieve previous selection -- nice. */
		if ( $this->session()->caddie ) {
			foreach( $this->session()->caddie as $elemt ) {
				$documentIds[] = $elemt['documentId'];
			}
		}
		else {
			throw new \Exception('None documents in selection');
		}

		/**
		 * Get processes and instances attached to each documents
		 * Populate $bacth with Items
		 */
		$workflow = $this->workflow->setFactory($factory);
		$workflow->setUrl($this->url()
			->fromRoute('docflow-act-validation'));
		$batch = new \Docflow\Model\BatchDocflow($workflow, $factory);
		$this->_loadBatch($batch, $documentIds);
		$process = $batch->getProcess();

		/* */
		if ( $process == null ) {
			throw new \Exception('None process is found');
		}
		$workflow->process = $process;

		/* load DAOs */
		$activityInstDao = $factory->getDao(Wf\Instance\Activity::$classId);

		/* apply activity on each items */
		$view = false;
		foreach( $batch->getItems() as $item ) {
			/* Start transaction */
			$connexion->beginTransaction();

			$document = $item->getDocument();
			$workflow->document = $document;
			$instance = $item->getInstance();
			$workflow->instance = $instance;

			$instanceId = $instance->getId();
			$filter = 'obj.' . $activityInstDao->toSys('activityId') . '=:activityId AND obj.' . $activityInstDao->toSys('instanceId') . '=:instanceId';
			$bind = array(
				':activityId' => $activityId,
				':instanceId' => $instanceId
			);

			/**/
			$actInstance = $activityInstDao->loadWithActivity(new Wf\Instance\Activity(), $filter, $bind);

			/* Run activity */
			try {
				$workflow->runActivity($actInstance)->lastActivity;
			}
			catch( ActivityWithViewException $exception ) {
				$view = $exception->view;
			}
			$connexion->commit();
		}

		/* save batch uid and id in db */
		$factory->getDao($batch->cid)->save($batch);

		/* */
		if ( $workflow->nextActivityInstance && $workflow->nextActivityInstance->isAutomatic() ) {
			$activityId = $workflow->nextActivityInstance->getActivity(true);
			return $this->redirectTo($this->url()
				->fromRoute('docflow-act-validation'), array(
				'id' => $activityId
			));
		}

		/* if a view is defined, return it */
		if ( $view ) {
			return $view;
		}

		return $this->successForward();
	}

	/**
	 * Post only
	 * Start a process
	 */
	public function startAction()
	{
		/* Inits */
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$spacename = $request->getPost('spacename', $this->session()->spacename);
			$documentIds = $request->getPost('checked', array());
			$documentId = $request->getPost('documentid', false);
			$processId = $request->getPost('processId', false);
			$cancel = $request->getPost('cancel', false);
			$validate = $request->getPost('validate', false);
			if ( $documentId ) $documentIds[] = $documentId;

			if ( $validate ) {
				$batchFormReturn = $request->getPost('batchform');
				foreach( $batchFormReturn['items'] as $item ) {
					$documentIds[] = $item['documentId'];
				}

				(isset($batchFormReturn['processId'])) ? $processId = $batchFormReturn['processId'] : null;
				(isset($batchFormReturn['spacename'])) ? $spacename = $batchFormReturn['spacename'] : null;
			}

			/* */
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Inits helpers */
		$factory = DaoFactory::get($spacename);
		$workflow = $this->workflow->setFactory($factory);
		$workflow->setUrl($this->url()
			->fromRoute('docflow-start'));
		$connexion = $factory->getConnexion();

		/* Init DAOs */
		$wfDocLinkDao = $factory->getDao(\Rbs\Wf\Instance\DocumentLink::$classId);

		/* to retrieve previous selection -- nice. */
		if ( !$documentIds && $this->session()->caddie ) {
			foreach( $this->session()->caddie as $elemt ) {
				$documentIds[] = $elemt['documentId'];
			}
		}

		/**
		 * Get processes and instances attached to each documents
		 * Populate $bacth with Items
		 */
		$batch = new \Docflow\Model\BatchDocflow($workflow, $factory);
		$this->_loadBatch($batch, $documentIds);

		/* Display start form if none process is currently started on documents of selection */
		$view = $this->_displayStartForm($batch, $processId);
		$form = $view->form;

		/* set process to workflow */
		$workflow->process = $batch->getProcess();

		if ( $validate ) {
			if ( $request->isPost() ) {
				$form->setData($request->getPost());
				if ( $form->isValid() ) {
					/**/
					foreach( $batch->getItems() as $item ) {
						$document = $item->getDocument();
						$instance = $item->getInstance();
						$workflow->document = $document;

						/* Start PDO transaction */
						$connexion->beginTransaction();

						/* process must be started */
						if ( $instance == null ) {
							try {
								/* start process */
								$startActInst = $workflow->startProcess()->startInstance;

								/* translate to next activities */
								$workflow->translateActivity($startActInst);

								/* Now process instance is set in workflow : */
								$instance = $workflow->instance;

								/* create link between process instance and workunit object */
								$wfDocLinkDao->insertFromArray([
									'documentId' => $document->getId(),
									'documentUid' => $document->getUid(),
									'instanceId' => $instance->getId(),
									'instanceUid' => $instance->getUid(),
									'spacename' => strtolower($factory->getName())
								]);
							}
							catch( \Throwable $e ) {
								$connexion->rollBack();
								$msg = sprintf('change state failed for document %s: ', $document->getName());
								$msg .= $e->getMessage();
								$this->errorStack()->error($msg);
								throw new ControllerException($msg, 65000, $e);
							}
						}
						$connexion->commit();
					}

					/* save batch uid and id in db */
					$factory->getDao($batch->cid)->save($batch);

					/**/
					if ( $workflow->nextActivityInstance && $workflow->nextActivityInstance->isAutomatic() ) {
						//$this->_loadBatch($batch, $documentIds);
						//$this->_runBatch($batch, $factory);
					}
				}
			}
		}
		return $this->successForward();
	}
}
