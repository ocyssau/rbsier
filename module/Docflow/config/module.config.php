<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'docflow' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/docflow',
					'defaults' => array(
						'__NAMESPACE__' => 'Docflow\Controller',
						'controller'=> 'Index',
						'action'=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'=> 'Segment',
						'options' => array(
							'route'=> '/[:controller[/:action]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
			'docflow-start' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/docflow/start',
					'defaults' => array(
						'__NAMESPACE__' => 'Docflow\Controller',
						'controller'=> 'Index',
						'action'=> 'start',
					),
				),
			),
			'docflow-act-validation'=> array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/docflow/activity/validation',
					'defaults' => array(
						'__NAMESPACE__' => 'Docflow\Controller',
						'controller'=> 'Index',
						'action'=> 'activityformvalidation',
					),
				),
			),
		), //routes
	), //router
	'controllers' => array(
		'invokables' => array(
			'Docflow\Controller\Index' => 'Docflow\Controller\IndexController',
		),
	), //controller
	'view_manager' => array(
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	), //view_manager
);
