<?php
namespace Docflow;

use Ranchbe;

/**
 * 
 *
 */
class Module
{

	/**
	 *
	 */
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	/**
	 */
	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'workflow' => function ($sm) {
					return Ranchbe::get()->getServiceManager()->getWorkflowService();
				}
			)
		);
	}

	/**
	 *
	 */
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
				)
			)
		);
	}
}
