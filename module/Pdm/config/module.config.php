<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
	'router' => array(
		'routes' => array(
			'pdm-product-version' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/pdm/productversion/:action[/:id]',
					'defaults' => array(
						'controller' => 'Pdm\Controller\ProductVersion\Index'
					)
				)
			),
			'pdm-product-instance' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/pdm/productinstance/:action[/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z0-9]+'
					),
					'defaults' => array(
						'controller' => 'Pdm\Controller\ProductInstance\Index'
					)
				)
			),
			'pdm-product-instance-detail' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/pdm/productinstance/detail/:action[/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z0-9]+'
					),
					'defaults' => array(
						'controller' => 'Pdm\Controller\ProductInstance\Detail'
					)
				)
			),
			'pdm-product-version-detail' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/pdm/productversion/detail/:action[/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z0-9]+'
					),
					'defaults' => array(
						'controller' => 'Pdm\Controller\ProductVersion\Detail'
					)
				)
			),
			'pdm-product-explorer' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/pdm/explorer/:action[/:id]',
					'defaults' => array(
						'controller' => 'Pdm\Controller\Explorer\Index'
					)
				)
			),
			'pdm-document' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/pdm/document/:action/:spacename/:id',
					'defaults' => array(
						'controller' => 'Pdm\Controller\Document'
					)
				)
			),
			'pdm-document-relation' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/pdm/document/relation/:direction/:spacename/:id',
					'defaults' => array(
						'controller' => 'Pdm\Controller\Document',
						'action' => 'getrelation'
					)
				)
			),
			'pdm-product-relation' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/pdm/product/relation/:action/:id',
					'defaults' => array(
						'controller' => 'Pdm\Controller\ProductVersion\Relation'
					)
				)
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Pdm\Controller\Index' => 'Pdm\Controller\IndexController',
			'Pdm\Controller\Product' => 'Pdm\Controller\ProductController',
			'Pdm\Controller\Explorer\Index' => 'Pdm\Controller\Explorer\IndexController',
			'Pdm\Controller\Document' => 'Pdm\Controller\DocumentController',
			'Pdm\Controller\ProductVersion\Index' => 'Pdm\Controller\ProductVersion\IndexController',
			'Pdm\Controller\ProductVersion\Detail' => 'Pdm\Controller\ProductVersion\DetailController',
			'Pdm\Controller\ProductVersion\Relation' => 'Pdm\Controller\ProductVersion\RelationController',
			'Pdm\Controller\ProductInstance\Index' => 'Pdm\Controller\ProductInstance\IndexController',
			'Pdm\Controller\ProductInstance\Detail' => 'Pdm\Controller\ProductInstance\DetailController',
			'Pdm\Controller\ProductContext\Index' => 'Pdm\Controller\ProductContext\IndexController',
			'Pdm\Controller\ProductConcept\Index' => 'Pdm\Controller\ProductConcept\IndexController'
		)
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Pdm' => __DIR__ . '/../view'
		)
	),
	'main_menu' => [
		[
			'name' => 'pdm',
			'class' => 'dropdown',
			'glyphicon' => 'glyphicon glyphicon-tree-deciduous',
			'label' => 'Pdm',
			'items' => [
				'product' => [
					'name' => 'product',
					'route' => [
						'params' => [
							'action' => 'index'
						],
						'options' => [
							'name' => 'pdm-product-version'
						]
					],
					'class' => '%anchor%',
					'label' => 'Products'
				]
			]
		]
	],
	'contextual_menu' => [
		'document' => [
			'pdm' => [
				'menu' => 'Pdm',
				'class' => 'submenu',
				'items' => [
					[
						'name' => 'getpdmchildren',
						'assert' => 'multiSelect==false || multiSelect==true',
						'class' => 'getpdmchildren-btn',
						'url' => '#',
						'title' => 'Get the children documents',
						'label' => 'Get Children'
					],
					[
						'name' => 'getpdmparent',
						'assert' => 'multiSelect==false || multiSelect==true',
						'class' => 'getpdmparent-btn',
						'url' => '#',
						'title' => 'Get the parents documents',
						'label' => 'Get Parent'
					],
					[
						'name' => 'getpdmexplorer',
						'assert' => 'multiSelect==false || multiSelect==true',
						'class' => 'getpdmexplorer-btn',
						'url' => '#',
						'title' => 'Get the product instance',
						'label' => 'Open in Explorer'
					],
					[
						'name' => 'initproduct',
						'assert' => 'multiSelect==false || multiSelect==true',
						'class' => 'initproduct-btn',
						'url' => '#',
						'title' => 'Create a product from this document',
						'label' => 'Init Product'
					]
				]
			]
		]
	]
);
