<?php
namespace Pdm\Callback;

use Rbs\EventDispatcher\Event;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;
use Pdm\Input\PdmData;
use Rbs\Observers\CallbackException;

/**
 * Import Pdm data set
 *
 */
class ImportPdmdataSet extends \Rbs\Observers\Callback
{

	const DATASET_FILE_SUFFIX = '.pdmdataset.json';

	/**
	 */
	public function post(Event $event)
	{
		$document = $event->getEmitter();
		if ( !($document instanceof \Rbplm\Ged\Document\Version) ) {
			throw new CallbackException('Emitter is not a \Rbplm\Ged\Document\Version');
		}

		$mainDf = $document->getMainDocfile();
		if ( $mainDf ) {
			$filename = $mainDf->getName();
			$pdmdatasetFilename = $filename . self::DATASET_FILE_SUFFIX;
			$wildspace = Wildspace::get(CurrentUser::get());
			$workingDir = $wildspace->getPath();

			/* set input file path */
			$pdmdatasetFilepath = $workingDir . '/' . $pdmdatasetFilename;

			if ( file_exists($pdmdatasetFilepath) ) {
				$jsonStr = file_get_contents($pdmdatasetFilepath);

				/** @var \Pdm\Input\PdmData\Element $inputElmt */
				$inputElmt = PdmData\Factory::getElement($jsonStr, 'json');

				/* set dir where are put temporaries datas */
				$inputElmt->setWorkingDir($workingDir);

				/* set data from pdm data to document */
				$setter = new PdmData\Setter($event->getFactory());
				$product = $setter->setDocument($document, $inputElmt);
				$document->description = $product->description;
				$document->dao->save($document);

				/* remove temp file */
				unlink($pdmdatasetFilepath);
			}
		}
	}

	/**
	 */
	public function pre()
	{}
}
