<?php
//%LICENCE_HEADER%
namespace Pdm\Cron;

use Rbs\Batch\CallbackInterface;
use Rbs\Space\Factory as DaoFactory;

/**
 * 
 */
class ReconciliateDoclink implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/** @var \PDO */
	protected $connexion;

	/** @var array */
	protected $spacenames = [];

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
		$this->connexion = DaoFactory::get()->getConnexion();
		isset($params['spacenames']) ? $this->spacenames = $params['spacenames'] : $this->spacenames = [
			'workitem',
			'cadlib',
			'mockup'
		];
	}

	/**
	 *
	 */
	public function run()
	{
		echo date(DATE_RFC822) . "\n";

		foreach( $this->spacenames as $parentSpacename ) {
			foreach( $this->spacenames as $childSpacename ) {
				echo "Reconciliate doclinks of $parentSpacename to $childSpacename" . "\n";
				$stmt = $this->reconciliate($parentSpacename, $childSpacename);
				echo $stmt->queryString . "\n";
			}
		}

		echo "End at " . date(DATE_RFC822) . "\n";
		return $this;
	}

	/**
	 * Populate designation of product from associated document designation
	 *
	 * @param string $filter Sql filter
	 * @param array $bind PDO bind array for parameter in filter
	 * @return \PDOStatement
	 */
	public function reconciliate($parentSpacename, $childSpacename)
	{
		$doctable = $parentSpacename . '_documents';
		$reltable = $parentSpacename . '_doc_rel';
		$dftable = $childSpacename . '_doc_files';

		$filter = ' doclink.child_id IS NULL';

		$sql = "UPDATE $reltable AS doclink";
		$sql .= " JOIN $dftable AS df ON doclink.name = df.name";
		$sql .= " JOIN $doctable AS doc ON df.document_id = doc.id";
		$sql .= ' SET';
		$sql .= ' doclink.child_id = df.document_id,';
		$sql .= ' doclink.child_uid = df.document_uid,';
		$sql .= ' doclink.child_space_id = ' . DaoFactory::get()->getIdFromName($parentSpacename);
		$sql .= " WHERE $filter";

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute();
		return $stmt;
	}
}
