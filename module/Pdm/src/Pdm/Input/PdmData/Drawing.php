<?php
namespace Pdm\Input\PdmData;

/**
 * \Pdm\Input\PdmData\Drawing is a class to bind datas from CAD datas to ranchbe.
 *
 *
 *
 * To build a input :
 * $input = new Drawing($datas, $key)
 * where $datas is array as:
 * 'name'=>string
 * 'path'=>string
 * 'readonly'=>boolean
 * 'saved'=>boolean
 * 'format'=>boolean
 * 'orientation'=>boolean
 */
class Drawing extends Element
{

	/**
	 * @param array $datas
	 * @param string $key
	 */
	function __construct($datas, $key)
	{
		parent::__construct($datas, $key);
		$this->type = 'drawing';
	}
}
