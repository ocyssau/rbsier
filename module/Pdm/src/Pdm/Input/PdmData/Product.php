<?php
namespace Pdm\Input\PdmData;

/**
 * \Pdm\Input\PdmData\Product is a class to bind datas from CAD datas to ranchbe.
 *
 *
 *
 * To build a input :
 * $input = new Product($datas, $key)
 * where $datas is array as:
 * 'fullName'=>string,
 * 'name'=>string,
 * 'path'=>string,
 * 'readonly'=>boolean,
 * 'saved'=>boolean,
 * 'product'=>array(
 * * 'name'=>string,
 * * 'number'=>string,
 * * 'nomenclature'=>string,
 * * 'version'=>$this->string,
 * * 'definition'=>string,
 * * 'description'=>string,
 * ),
 */
class Product extends Element
{

	/**
	 * @param array $datas
	 * @param string $key
	 */
	function __construct($datas, $key)
	{
		parent::__construct($datas, $key);
		$this->type = 'product';
	}

	/**
	 *
	 */
	public function getChildren()
	{
		if ( isset($this->datas['children']) ) {
			return $this->datas['children'];
		}
	}
}

