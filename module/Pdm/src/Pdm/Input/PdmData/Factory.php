<?php
namespace Pdm\Input\PdmData;

/**
 */
class Factory
{

	const IN_FORMAT_JSON = 'json';

	const IN_FORMAT_ARRAY = 'array';

	/**
	 *
	 * @param string $inputStr,
	 *        	JSON encoded string or array, must br in accordance with $format parameter
	 * @param string $format,
	 *        	self::IN_FORMAT_*
	 */
	public static function getElement($inputStr, $format = 'json')
	{
		if ( $format == self::IN_FORMAT_JSON ) {
			$pdmDatas = json_decode($inputStr, true);
		}
		else if ( $format == self::IN_FORMAT_ARRAY ) {
			throw new \Exception('NOT YET IMPLEMENTED');
		}

		$key = 0;
		switch ($pdmDatas['fromType']) {
			case 'product':
			case 'catproduct':
				return new Product($pdmDatas, $key);
				break;
			case 'part':
			case 'catpart':
				return new Part($pdmDatas, $key);
				break;
			case 'drawing':
			case 'catdrawing':
				return new Drawing($pdmDatas, $key);
				break;
			default:
				return new Element($pdmDatas, $key);
				break;
		}
	}
}

