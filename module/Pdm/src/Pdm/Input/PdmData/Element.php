<?php
namespace Pdm\Input\PdmData;

/**
 *
 *
 */
class Element
{

	/**
	 *
	 * @var array
	 */
	protected $datas;

	/**
	 *
	 * @var string
	 */
	protected $key;

	/**
	 *
	 * @var string
	 */
	protected $workingDir;

	/**
	 * @param array $datas Datas from converter.
	 * @param string $key key of the element as defined by the PdmData collection
	 */
	function __construct($datas, $key)
	{
		$this->datas = $datas;
		$this->key = $key;
		$this->type = $datas['fromType'];
	}

	/**
	 *
	 * @param string $path
	 */
	public function setWorkingDir($path)
	{
		$this->workingDir = rtrim($path, '/');
		return $this;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return array
	 */
	public function getFeedback()
	{
		if ( isset($this->datas['feedback']) ) {
			return $this->datas['feedback'];
		}
	}

	/**
	 * @return array
	 */
	public function getErrors()
	{
		if ( isset($this->datas['errors']) ) {
			return $this->datas['errors'];
		}
	}

	/**
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * @return array
	 */
	public function getProperties()
	{
		return $this->datas['properties'];
	}

	/**
	 * @return string
	 */
	public function getPicture()
	{
		if ( isset($this->datas['picture']) ) {
			return $this->workingDir . '/' . trim(basename($this->datas['picture']['file']), '/');
		}
		return false;
	}

	/**
	 * @return string
	 */
	public function getThreedim()
	{
		if ( isset($this->datas['3d']) ) {
			return $this->workingDir . '/' . trim(basename($this->datas['3d']['file']), '/');
		}
		return false;
	}

	/**
	 * @return string
	 */
	public function getConvert()
	{
		if ( isset($this->datas['convert']) ) {
			return $this->workingDir . '/' . trim(basename($this->datas['convert']['file']), '/');
		}
		return false;
	}

	/**
	 *
	 */
	public function getLinks()
	{
		if ( isset($this->datas['links']) ) {
			return $this->datas['links'];
		}
	}
}
