<?php
namespace Pdm\Input\PdmData;

/**
 * \Pdm\Input\PdmData\Part is a class to bind datas from CAD datas to ranchbe.
 *
 * To build a input :
 * $input = new Part($datas, $key)
 * where $datas is array as:
 * 'fullName'=>string,
 * 'name'=>string,
 * 'path'=>string,
 * 'readonly'=>boolean,
 * 'saved'=>boolean,
 * 'product'=>array(
 * * 'name'=>string,
 * * 'number'=>string,
 * * 'nomenclature'=>string,
 * * 'version'=>$this->string,
 * * 'definition'=>string,
 * * 'description'=>string,
 * ),
 * 'physicalProperties'=>array(
 * * 'weight'=>decimal <Mass in grammes>
 * * 'density'=>decimal <Density Kg/m3>
 * * 'wetArea'=>decimal <Surface M2>
 * * 'volume'=>decimal <Volume mm3>
 * ),
 * 'gravityCenter'=>array <X,Y,Z in mm>
 * 'inertiaMatrix'=>array <Ixx,Ixy,Ixz,Iyx,Iyy,Iyz,Izx,Izy,Izz in mm>
 * 'materials'=>array <list of materials names>
 * 'shape'=>array(
 * * name=>string,
 * * partNumber=>string
 * )
 */
class Part extends Element
{

	/**
	 *
	 * @param array $datas
	 * @param string $key
	 */
	function __construct($datas, $key)
	{
		parent::__construct($datas, $key);
		$this->type = 'part';
	}
}
