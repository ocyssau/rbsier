<?php
namespace Pdm\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ArraySerializable as Hydrator;

/**
 *
 *
 */
class MaterialFieldset extends Fieldset implements InputFilterProviderInterface
{

	/**
	 */
	public function __construct()
	{
		parent::__construct('materialform');
		$this->setHydrator(new Hydrator(false));
		$this->setObject(new \Rbplm\Pdm\Product\Material()); /* because bug https://github.com/zendframework/zendframework/issues/3373 */

		/**/
		$this->add(array(
			'type' => 'hidden',
			'name' => 'id'
		));

		/**/
		$this->add(array(
			'type' => 'text',
			'name' => 'name',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Name'
			)
		));

		/**/
		$this->add(array(
			'type' => 'number',
			'name' => 'density',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Density'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'name' => array(
				'required' => false
			),
			'density' => array(
				'required' => false
			)
		);
	}
}
