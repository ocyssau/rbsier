<?php
namespace Pdm\Form\ProductVersion;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 * 
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @param \Rbs\Space\Factory $factory
	 * @param string $name
	 */
	public function __construct($factory = null, $name = null)
	{
		parent::__construct($factory, $name);

		$this->template = 'pdm/product-version/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		$this->add([
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'options' => [
				'label' => 'Search...'
			],
			'attributes' => [
				'onChange' => '',
				'class' => 'form-control'
			]
		]);
		$inputFilter->add([
			'name' => 'find_magic',
			'required' => false
		]);

		$this->add([
			'name' => 'filter',
			'type' => 'Zend\Form\Element\Button',
			'options' => [
				'label' => 'Filter'
			],
			'attributes' => [
				'class' => 'btn btn-default btn-filter submitOnClick'
			]
		]);

		$this->add([
			'name' => 'resetf',
			'type' => 'Zend\Form\Element\Button',
			'options' => [
				'label' => 'Reset'
			],
			'attributes' => [
				'class' => 'btn btn-default btn-resetf'
			]
		]);
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(\Rbplm\Pdm\Product\Version::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* MAGIC */
		if ( $datas['find_magic'] ) {
			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, `%s`, `%s`, '-')", $dao->toSys('id'), $dao->toSys('uid'), $dao->toSys('name'), $dao->toSys('description'));
			$search = $datas['find_magic'];
			/*
			 if($search){
			 $splitted = explode(' ', $search);
			 foreach($splitted as $word){
			 $paramName = ':'.uniqid();
			 $filter->orFind($paramName, $concat, Op::LIKE);
			 $this->bind[$paramName] = '%'.$word.'%';
			 }
			 }
			 */
			$filter->andFind($search, $concat, Op::OP_CONTAINS);
		}
		return $this;
	}
}
