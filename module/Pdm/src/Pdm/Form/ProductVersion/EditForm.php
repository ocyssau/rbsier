<?php
namespace Pdm\Form\ProductVersion;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Rbplm\Pdm\Product\Version as ProductVersion;

/**
 *
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('productversionEdit');
		$this->template = 'pdm/product-version/editform';

		/**/
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/**/
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/**/
		$this->add(array(
			'name' => 'parentId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		$this->add(array(
			'type' => 'Pdm\Form\ProductVersion\PhysicalPropertiesFieldset',
			'options' => array(
				'use_as_base_fieldset' => false
			)
		));

		/**/
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Name'
			)
		));

		/**/
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Number',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Number'
			)
		));

		/**/
		$this->add(array(
			'name' => 'version',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'placeholder' => 'Version',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Version'
			)
		));

		/**/
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Description',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Description'
			)
		));

		/**/
		$this->add(array(
			'name' => 'contextId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'Select Context',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Context',
				'value_options' => $this->_getContextOptions()
			)
		));

		/**/
		$this->add(array(
			'name' => 'type',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'Select Type',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Type',
				'value_options' => $this->_getTypeOptions()
			)
		));

		/**/
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/**/
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Cancel',
				'id' => 'submitbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'parentId' => array(
				'required' => false
			),
			'name' => array(
				'required' => true
			),
			'number' => array(
				'required' => true
			),
			'version' => array(
				'required' => true
			),
			'description' => array(
				'required' => false
			),
			'contextId' => array(
				'required' => false
			),
			'type' => array(
				'required' => false
			)
		);
	}

	/**
	 *
	 */
	protected function _getContextOptions()
	{
		return array(
			1 => 'Mechanical',
			2 => 'Electric',
			3 => 'Buizness'
		);
	}

	/**
	 *
	 */
	protected function _getTypeOptions()
	{
		return array(
			ProductVersion::TYPE_PART => 'Part',
			ProductVersion::TYPE_PRODUCT => 'Product',
			ProductVersion::TYPE_DRAWING => 'Drawing',
			ProductVersion::TYPE_ROOT => 'Root'
		);
	}
}
