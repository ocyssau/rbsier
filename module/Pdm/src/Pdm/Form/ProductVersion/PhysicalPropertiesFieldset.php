<?php
namespace Pdm\Form\ProductVersion;

use Zend\Form\Fieldset;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class PhysicalPropertiesFieldset extends Fieldset implements InputFilterProviderInterface
{

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('physicalpropertiesform');
		$this->template = 'pdm/product-version/physicalpropertiesform';

		/**/
		$this->setAttribute('method', 'post')->setHydrator(new Hydrator(false));

		/**/
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/**/
		$this->add(array(
			'name' => 'weight',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'placeholder' => 'Weight',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Weight'
			)
		));

		/**/
		$this->add(array(
			'name' => 'volume',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'placeholder' => 'Volume',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Volume'
			)
		));

		/**/
		$this->add(array(
			'name' => 'wetsurface',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'placeholder' => 'Wetsurface',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Wetsurface'
			)
		));

		/**/
		$this->add(array(
			'name' => 'density',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'placeholder' => 'Density',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Density'
			)
		));

		/**/
		$this->add(array(
			'name' => 'gravityCenter',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Gravity Center',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Gravity Center'
			)
		));

		/**/
		$this->add(array(
			'name' => 'inertiaCenter',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Inertia Center',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Inertia Center'
			)
		));

		/**/
		$this->add(array(
			'type' => 'Zend\Form\Element\Collection',
			'name' => 'materials',
			'options' => array(
				'label' => 'Material',
				'count' => 0,
				'should_create_template' => true,
				'allow_add' => true,
				'target_element' => array(
					'type' => 'Pdm\Form\MaterialFieldset'
				)
			)
		));

		/**/
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
				'class' => 'btn btn-default'
			)
		));

		/**/
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Cancel',
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'weight' => array(
				'required' => false
			),
			'volume' => array(
				'required' => false
			),
			'wetsurface' => array(
				'required' => false
			),
			'density' => array(
				'required' => false
			),
			'gravityCenter' => array(
				'required' => false
			),
			'inertiaCenter' => array(
				'required' => false
			),
			'materials' => array(
				'required' => false
			)
		);
	}
} /* End of class */
