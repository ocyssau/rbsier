<?php
namespace Pdm\Form\ProductInstance;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class SetPositionForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('setpositionForm');

		$this->template = 'pdm/product-instance/setpositionform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* PRODUCT INSTANCE ID*/
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		$this->add(array(
			'name' => 'tx',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'TX',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'TX'
			)
		));

		$this->add(array(
			'name' => 'ty',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'TY',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'TY'
			)
		));

		$this->add(array(
			'name' => 'tz',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'TZ',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'TZ'
			)
		));

		$this->add(array(
			'name' => 'rx',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Rx',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Rx'
			)
		));

		$this->add(array(
			'name' => 'ry',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Ry',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Ry'
			)
		));

		$this->add(array(
			'name' => 'rz',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Rz',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Rz'
			)
		));

		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbtn',
				'class' => 'btn btn-success'
			)
		));

		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Cancel',
				'id' => 'cancelbtn',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'tx' => array(
				'required' => false
			),
			'ty' => array(
				'required' => false
			),
			'tz' => array(
				'required' => false
			),
			'rx' => array(
				'required' => false
			),
			'ry' => array(
				'required' => false
			),
			'rz' => array(
				'required' => false
			)
		);
	}
}
