<?php
namespace Pdm\Form\ProductInstance;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class SetReferenceForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 * @param string $name
	 */
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('setreferenceForm');

		$this->template = 'pdm/product-instance/setreferenceform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* PRODUCT INSTANCE ID*/
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/**/
		$this->add(array(
			'name' => 'mode',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'Create Mode',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Mode',
				'value_options' => array(
					'new' => 'New Product',
					'existing' => 'From Existing Product'
				)
			)
		));

		/**/
		$this->add(array(
			'name' => 'ofProduct',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'input number',
				'class' => 'productnumber-autocomplet form-control'
			),
			'options' => array(
				'label' => 'Of Product Number'
			)
		));

		/**/
		$this->add(array(
			'name' => 'ofProductId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/**/
		$this->add(array(
			'name' => 'ofProductNumber',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'product01',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Product Number'
			)
		));

		/**/
		$this->add(array(
			'name' => 'ofProductName',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'my product',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Product Name'
			)
		));

		/**/
		$this->add(array(
			'name' => 'ofProductDescription',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'a simple description',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Product Description'
			)
		));

		/**/
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbtn'
			)
		));

		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Cancel',
				'id' => 'cancelbtn'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => true
			),
			'mode' => array(
				'required' => false
			),
			'ofProduct' => array(
				'required' => false
			),
			'ofProductId' => array(
				'required' => false
			),
			'ofProductNumber' => array(
				'required' => false
			),
			'ofProductName' => array(
				'required' => false
			),
			'ofProductDescription' => array(
				'required' => false
			)
		);
	}
}
