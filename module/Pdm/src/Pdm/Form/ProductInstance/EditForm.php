<?php
namespace Pdm\Form\ProductInstance;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @param string $name
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($view, $factory = null)
	{
		/* We want to ignore the name passed */
		parent::__construct('productinstanceEdit');
		$this->template = 'pdm/product-instance/editform';
		$this->view = $view;
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Layout */
		$this->add(array(
			'name' => 'layout',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Number */
		$this->add(array(
			'name' => 'number',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Number')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 40
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Nomenclature */
		$this->add(array(
			'name' => 'nomenclature',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Nomenclature')
			)
		));

		/* ContextId */
		$this->add(array(
			'name' => 'contextId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'Select Context',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Context',
				'value_options' => $this->_getContextOptions()
			)
		));

		/* Quantity */
		$this->add(array(
			'name' => 'quantity',
			'type' => 'Zend\Form\Element\Number',
			'attributes' => array(
				'min' => '0',
				'max' => '10000',
				'step' => '1', /* default step interval is 1 */
				'type' => 'text',
				'placeholder' => 'Quantity',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Quantity'
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'name' => array(
				'required' => true
			),
			'number' => array(
				'required' => true
			),
			'description' => array(
				'required' => false
			),
			'nomenclature' => array(
				'required' => false
			),
			'quantity' => array(
				'required' => false
			)
		);
	}

	/**
	 *
	 */
	protected function _getContextOptions()
	{
		return array(
			1 => 'Mechanical',
			2 => 'Electric',
			3 => 'Buizness'
		);
	}
} /* End of class */
