<?php
namespace Pdm\Controller\ProductInstance;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Pdm\Product;
use Rbplm\Dao\NotExistingException;

/**
 * 
 *
 */
class DetailController extends \Pdm\Controller\AbstractController
{

	/**
	 * Display instance of all process
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('pdm/product-instance/detail/index');

		$instanceId = $this->params()->fromRoute('id', null);
		$factory = DaoFactory::get();

		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$productDao = $factory->getDao(Product\Version::$classId);

		$instance = null;
		$product = null;
		$document = null;
		$threedfile = null;

		/* Load Instance */
		try {
			if ( $instanceId ) {
				$instance = new Product\Instance();
				$instanceDao->loadFromId($instance, $instanceId);
				$productId = $instance->ofProductId;
			}
		}
		catch( NotExistingException $e ) {}

		/* Load Product */
		try {
			if ( $productId ) {
				$product = new Product\Version();
				$productDao->loadFromId($product, $productId);
				if ( $instance ) {
					$instance->setOfProduct($product);
				}
				$passphrase = \Ranchbe::get()->getConfig('visu.passphrase');
				$path = \Ranchbe::get()->getConfig('viewer.reposit.path');
				$threedfile = \Rbplm\Pdm\Product\File::encodename($product->getId(), 'stl', $passphrase);
				if ( !is_file($path . '/' . $threedfile) ) {
					$threedfile = null;
				}
			}
		}
		catch( NotExistingException $e ) {}

		/* Load Document */
		try {
			if ( $product ) {
				$documentId = $product->documentId;
				if ( $documentId ) {
					$spacename = $product->spacename;
					$factory = DaoFactory::get($spacename);
					$documentDao = $factory->getDao(\Rbplm\Ged\Document\Version::$classId);
					$document = new \Rbplm\Ged\Document\Version();
					$documentDao->loadFromId($document, $documentId);
					if ( $product ) {
						$product->setDocument($document);
					}
				}
			}
		}
		catch( NotExistingException $e ) {}

		$view->threedfile = $threedfile;
		$view->threedurl = $this->url()->fromRoute('viewer-getfile');
		$view->instance = $instance;
		$view->product = $product;
		$view->document = $document;
		$view->pageTitle = 'Product Instance Details';
		return $view;
	}
}
