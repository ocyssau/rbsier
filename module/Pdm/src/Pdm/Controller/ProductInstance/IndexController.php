<?php
namespace Pdm\Controller\ProductInstance;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Pdm;
use Rbplm\Pdm\Product;

/**
 */
class IndexController extends \Pdm\Controller\AbstractController
{

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Pdm\Controller\AbstractController::init()
	 */
	public function init($view = null, $errorStack = null)
	{
		parent::init();
		\Application\View\Menu\MainTabBar::get()->getTab('product')->activate();
	}

	/**
	 * Display instance of all process
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		return $view;
	}

	/**
	 * Display instance of all process
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();

		$id = $this->params()->fromRoute('id');

		if ( $request->isGet() ) {
			$validate = false;
		}
		else if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* init helpers */
		$instance = new Product\Instance();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Instance::$classId);
		$dao->loadFromId($instance, $id);

		/* Check permissions */
		// $this->getAcl()->checkRightFromUid('edit', $instance->getUid());

		$form = new \Pdm\Form\ProductInstance\EditForm($view, $factory);
		$url = $this->url()->fromRoute('pdm-product-instance', array(
			'action' => 'edit',
			'id' => $id
		));
		$form->setAttribute('action', $url);
		$form->bind($instance);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$instance->dao->save($instance);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Product Instance ' . $instance->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function setpositionAction()
	{
		$view = $this->view;
		$request = $this->getRequest();

		$id = $this->params()->fromRoute('id');

		if ( $request->isPost() ) {
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* init helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Instance::$classId);

		/* load objects */
		$instance = new Product\Instance();
		$dao->loadFromId($instance, $id);

		/* Form */
		$form = new \Pdm\Form\ProductInstance\SetPositionForm();
		$url = $this->url()->fromRoute('pdm-product-instance', array(
			'action' => 'setposition',
			'id' => $id
		));
		$form->setAttribute('action', $url);
		$form->bind($instance);
		$view->setTemplate($form->template);

		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$instance->dao->save($instance);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->form = $form;
		$view->pageTitle = 'Edit Position Of ' . $instance->getName();
		return $view;
	}

	/**
	 */
	public function setreferenceAction()
	{
		$view = $this->view;
		$request = $this->getRequest();

		$id = $this->params()->fromRoute('id');

		if ( $request->isGet() ) {
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$mode = $request->getPost('mode', null);
			$ofProductId = $request->getPost('ofProductId', null);
			$ofProductNumber = $request->getPost('ofProductNumber', null);
			$ofProductName = $request->getPost('ofProductName', null);
			$ofProductDescription = $request->getPost('ofProductDescription', null);

			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* init factory with space */
		$factory = DaoFactory::get();

		/* load objects */
		$instance = new Product\Instance();
		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$instanceDao->loadFromId($instance, $id);

		/* Form */
		$form = new \Pdm\Form\ProductInstance\SetReferenceForm();
		$url = $this->url()->fromRoute('pdm-product-instance', array(
			'action' => 'setreference',
			'id' => $id
		));
		$form->setAttribute('action', $url);
		$form->bind($instance);
		$view->setTemplate($form->template);
		$datas = array();

		/* IF REFERENCE IS SET, LOAD IT */
		if ( $validate == false && $instance->ofProductId ) {
			/* LOAD REFERENCE PRODUCT */
			$productDao = $factory->getDao(Product\Version::$classId);
			$product = new Product\Version();
			$productDao->loadFromId($product, $instance->ofProductId);
			$datas['ofProductId'] = $product->getId();
			$datas['ofProduct'] = $product->getNumber() . '.v' . $product->version;
			$form->get('mode')->setValue('existing');
			$form->setData($datas);
		}

		if ( $request->isPost() ) {
			$form->setData($request->getPost());

			if ( $form->isValid() ) {
				if ( $mode == 'new' ) {
					/* instanciate new product and save it */
					$product = Product\Version::init();
					$product->hydrate(array(
						'name' => $ofProductName,
						'number' => $ofProductNumber,
						'description' => $ofProductDescription,
						'type' => $instance->type
					));
					$productDao = $factory->getDao(Product\Version::$classId);
					$productDao->save($product);

					/* set to instance and save */
					$instance->setOfProduct($product);
					$instance->dao->save($instance);
				}
				elseif ( $mode == 'existing' ) {
					/* load existing product */
					$productDao = $factory->getDao(Product\Version::$classId);
					$product = new Product\Version();
					$productDao->loadFromId($product, $ofProductId);

					/* set to instance and save */
					$instance->setOfProduct($product);
					$instance->dao->save($instance);
				}

				if ( $this->isAjax ) {
					$return = array(
						'instance' => $instance->getArrayCopy(),
						'product' => $product->getArrayCopy()
					);
					return $this->serviceReturn($return);
				}
				else {
					return $this->successForward();
				}
			}
		}

		$view->form = $form;
		$view->pageTitle = 'Set Product Relation Of ' . $instance->getName();
		return $view;
	}

	/**
	 *
	 * @return \Zend\Http\Response|\Application\View\ViewModel
	 */
	public function reconciliateAction()
	{
		$view = $this->view;
		$request = $this->getRequest();

		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', array());
			$id = $request->getQuery('id', null);
			(!$id) ? $id = current($ids) : null;
		}
		else if ( $request->isPost() ) {
			$id = $request->getPost('id', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* init helpers */
		$factory = DaoFactory::get();
		$productDao = $factory->getDao(Product\Version::$classId);
		$instanceDao = $factory->getDao(Product\Instance::$classId);

		/* load objects */
		$product = new Product\Version();
		$productDao->loadFromId($product, $id);
		$instance = new Product\Instance();
		$instanceDao->loadFromId($instance, $id);

		return $view;
	}
}
