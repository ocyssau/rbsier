<?php
namespace Pdm\Controller;

class AbstractController extends \Application\Controller\AbstractController
{

	/**
	 *
	 */
	public function init($view = null, $errorStack = null)
	{
		\Application\Controller\AbstractController::init();

		$this->resourceCn = \Acl\Model\Resource\Pdm::$appCn;

		// Record url for page and Active the tab
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('pdm')->activate('product');
		$this->layout()->tabs = $tabs;
	}
}
