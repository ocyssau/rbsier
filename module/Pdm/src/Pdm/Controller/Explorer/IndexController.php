<?php
namespace Pdm\Controller\Explorer;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Pdm\Product;
use Application\Form\PaginatorForm;
use Rbplm\Dao\NotExistingException;

/**
 */
class IndexController extends \Pdm\Controller\AbstractController
{

	/* @var string */
	public $pageId = 'product_explorer';

	/* @var string */
	public $defaultSuccessForward = '/pdm/productexplorer/index';

	/* @var string */
	public $defaultFailedForward = '/pdm/productexplorer/index';

	/**
	 */
	public function init($view = null, $errorStack = null)
	{
		parent::init();
		\Application\View\Menu\MainTabBar::get()->getTab('product')->activate();
	}

	/**
	 * Display instance of all process
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;

		$productId = $this->params()->fromRoute('id');
		$factory = DaoFactory::get();

		/* Load Root Product */
		$rootProduct = new Product\Version();
		$rootProduct->dao = $factory->getDao(Product\Version::$classId);
		$rootProduct->dao->loadFromId($rootProduct, $productId);

		$view->rootProduct = $rootProduct;
		$view->pageTitle = 'Product Explorer';
		$view->setTemplate('pdm/product-explorer/index');
		return $view;
	}

	/**
	 *
	 * @param string $direction
	 * @param DaoFactory $factory
	 * @param integer $productId
	 */
	protected function _list($direction = 'children', $factory, $productId)
	{
		$request = $this->getRequest();
		$select = [];
		$headers = [];

		$filter = new \Rbs\Dao\Sier\Filter('', false);

		$filterForm = new \Pdm\Form\ProductVersion\FilterForm($factory, $this->pageId);
		$filterForm->setData($request->getPost());
		$filterForm->key = 'CONCAT(name,description)';
		$filterForm->passThrough = true;
		$filterForm->prepare()
			->bindToFilter($filter)
			->save();
		$filterStr = $filter->__toString();

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit(1000);
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()
			->bindToView($this->view)
			->bindToFilter($filter)
			->save();

		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$productDao = $factory->getDao(Product\Version::$classId);
		if ( $direction == 'children' ) {
			/* LOAD PRODUCT PROPERTIES ONLY */
			foreach( $productDao->metaModel as $asSys => $asApp ) {
				$select[] = 'child.' . $asSys . ' AS ' . $asApp;
				$headers[$asApp] = $asSys;
			}
			$stmt = $instanceDao->getChildren($productId, $select, $filterStr);
		}
		else {
			/* LOAD PRODUCT PROPERTIES ONLY */
			foreach( $productDao->metaModel as $asSys => $asApp ) {
				$select[] = 'parent.' . $asSys . ' AS ' . $asApp;
				$headers[$asSys] = $asApp;
			}
			$stmt = $instanceDao->getParents($productId, $select, $filterStr);
		}

		$list = $stmt->fetchAll();
		$this->view->headers = $headers;
		$this->view->list = $list;
		$this->view->filter = $filterForm;
	}

	/**
	 */
	function getchildrenproductAction()
	{
		$view = $this->view;
		$spacename = $this->params()->fromQuery('spacename');
		$documentId = $this->params()->fromQuery('documentid');
		$productId = $this->params()->fromQuery('productid');
		$factory = DaoFactory::get($spacename);

		$this->view->spacename = $spacename;

		/* Load product of document */
		if ( !$productId && $documentId ) {
			$productDao = $factory->getDao(Product\Version::$classId);
			$productId = $productDao->query(array(
				'id'
			), $productDao->toSys('documentId') . '=:documentId', array(
				':documentId' => $documentId
			))
				->fetchColumn(0);
		}

		/* To Saved in basecamp in postDispatch */
		$this->defaultSuccessForward = 'pdm/productexplore/getchildrenproduct?documentid=' . $documentId . '&spacename=' . $spacename;
		$this->basecamp()->save($this->defaultSuccessForward, $this->ifFailedForward);

		$this->_list('children', $factory, $productId);

		$view->pageTitle = tra('Get Children');
		$view->setTemplate('pdm/product-explorer/getchildren');
		return $view;
	}

	/**
	 * Ajax exclusive method
	 *
	 * Inputs:
	 *
	 * @param
	 *        	s integer $parentid Id of the parent product instance, if=#, then its the root product
	 * @param
	 *        	s integer $productId Id of the root product to load
	 */
	public function loadtreeAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$factory = DaoFactory::get();

		/* @var integer $productId Id of the root product to load */
		$productId = $request->getQuery('productid', null);

		/* @var integer $parentId Id of the parent product */
		$parentId = $request->getQuery('parentid', null);

		/* @var integer $nodeId Id jstree node , definde as the path of instance Ids as [root.instanceId].[product1.instanceId].[part1.instanceId]... */
		$nodePath = $request->getQuery('nodeid', null);

		/* Load Root Product */
		if ( $parentId == '#' ) {
			$rootProduct = new Product\Version();
			$factory->getDao(Product\Version::$classId)->loadFromId($rootProduct, $productId);
			$view->parentNodePath = '#';
			$view->rootProduct = $rootProduct;
		}
		else {
			$dao = $factory->getDao(Product\Instance::$classId);
			$stmt = $dao->getChildrenFromParentId($parentId);
			$list = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$view->parentNodePath = $nodePath;
			$view->tree = $list;
		}

		$view->setTemplate('pdm/product-explorer/loadtree');
		return $view;
	}

	/**
	 */
	public function savetreeAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$tree = $request->getPost('tree');
		}

		$factory = DaoFactory::get();

		if ( isset($tree['instances']) && count($tree['instances']) > 0 ) {
			$this->_saveInstanceNode($tree['instances'], null, $factory);
		}
		if ( isset($tree['products']) && count($tree['products']) > 0 ) {
			$this->_saveProductNode($tree['products'], null, $factory);
		}

		$view->tree = $tree;
		$view->setTemplate('pdm/product-explorer/savetree');
		return $view;
	}

	/**
	 *
	 * @param array $node
	 * @param integer|Product\Instance $parent
	 * @param DaoFactory $factory
	 * @return NULL|string
	 */
	protected function _saveInstanceNode($instances, $parent = null, $factory)
	{
		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$productDao = $factory->getDao(Product\Version::$classId);

		/* ------------- parse the links from input ------------- */
		for ($i = 1, $index = 'instance' . $i; isset($instances[$index]); $index = 'instance' . ++$i) {
			/* document1 is the current entry in input data */
			$instance1 = $instances[$index];

			/* set vars */
			(isset($instance1['id'])) ? $instanceId = $instance1['id'] : $instanceId = null;
			(isset($instance1['children'])) ? $children = $instance1['children'] : $children = null;

			/* Instance is existing */
			if ( $instanceId ) {
				(isset($instance1['parentId'])) ? $parentId = $instance1['parentId'] : $parentId = null;
				(isset($instance1['type'])) ? $type = $instance1['type'] : $type = null;
				(isset($instance1['name'])) ? $name = $instance1['name'] : $name = null;

				/* Load, Populate */
				$instance = new Product\Instance();
				$instanceDao->loadFromId($instance, $instanceId);
				$instance->type = $type;
				$instance->setName($name);

				/* Load parent */
				if ( $parentId ) {
					$parent = new Product\Instance();
					$productDao->loadFromId($parent, $parentId);
					$instance->setParent($parent);
				}

				/* Save instance */
				$instanceDao->save($instance);

				/* save children */
				if ( $children ) {
					$this->_saveInstanceNode($children, $instance, $factory);
				}
			}
			/* Instance is new and must be created */
			else {
				(isset($instance1['name'])) ? $name = $instance1['name'] : $name = null;
				(isset($instance1['number'])) ? $number = $instance1['number'] : $number = null;
				(isset($instance1['type'])) ? $type = $instance1['type'] : $type = null;

				/* try to load product from name */
				try {
					/* init some objects */
					$product = new Product\Version();
					$productDao->loadFromNumber($product, $number);
				}
				catch( NotExistingException $e ) {
					$product = Product\Version::init($number);
					$productDao->save($product);
				}

				/* Set parent */
				if ( $parent instanceof Product\Instance ) {
					$parentId = $parent->getId();
				}
				elseif ( is_integer($parent) ) {
					$parentId = $parent;
					$parent = new Product\Instance();
					$productDao->loadFromId($parent, $parentId);
				}
				else {
					$parent = new Product\Instance();
				}

				/* Load and save */
				$instance = Product\Instance::init($number);
				$instance->setOfProduct($product);
				$instance->setParent($parent);
				$instance->type = $type;
				$instanceDao->save($instance);

				/* save children */
				if ( $children ) {
					$this->_saveInstanceNode($children, $parent, $factory);
				}
			}
		} /* forend */
	}

	/**
	 *
	 * @param array $products
	 *        	Array of product product defintion.
	 * @param integer|Product\Version $parent
	 * @param DaoFactory $factory
	 * @return NULL|string
	 */
	protected function _saveProductNode($products, $parent = null, $factory)
	{
		$productDao = $factory->getDao(Product\Version::$classId);

		/* ------------- parse the links from input ------------- */
		for ($i = 1, $index = 'product' . $i; isset($products[$index]); $index = 'product' . ++$i) {
			/* document1 is the current entry in input data */
			$product1 = $products[$index];

			/* set vars */
			(isset($product1['id'])) ? $productId = $product1['id'] : $productId = null;
			(isset($product1['children'])) ? $children = $product1['children'] : $children = null;

			/* Product is existing */
			if ( $productId ) {
				/* Load, Populate */
				$product = new Product\Version();
				$productDao->loadFromId($product, $productId);

				/* save children */
				if ( $children ) {
					$this->_saveInstanceNode($children, $product, $factory);
				}
			}
			else {
				throw new \Exception(sprintf('Product %s is not saved', $product->getName()));
			}
		} /* forend */
	}
}

