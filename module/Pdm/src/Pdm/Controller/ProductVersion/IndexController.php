<?php
namespace Pdm\Controller\ProductVersion;

use Zend\View\Model\ViewModel;
use Rbs\Space\Factory as DaoFactory;
use Application\Form\PaginatorForm;
use Rbplm\Pdm\Product;

/**
 */
class IndexController extends \Pdm\Controller\AbstractController
{

	/** @var string */
	public $pageId = 'product_version';

	/** @var string */
	public $defaultSuccessForward = '/pdm/productversion/index';

	/** @var string */
	public $defaultFailedForward = '/pdm/productversion/index';

	/**
	 * Display instance of all process
	 * 
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		$factory = DaoFactory::get();

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/**/
		$request = $this->getRequest();

		/** @var \Rbs\Dao\Sier\DaoList $list */
		$list = $factory->getList(Product\Version::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Pdm\Form\ProductVersion\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();
		$list->countAll = $list->countAll($filter, $filterForm->bind);

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll);
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->prepare()
			->bindToView($this->view)
			->bindToFilter($filter)
			->save();
		$list->load($filter, $filterForm->bind);

		$view->headers = array(
			[
				'uid',
				'Uid'
			],
			[
				'name',
				'Name'
			],
			[
				'description',
				'Description'
			],
			[
				'version',
				'Version'
			]
		);

		$view->list = $list;
		$view->filter = $filterForm;
		$view->pageTitle = 'Products Manager';
		$view->setTemplate('pdm/product-version/index');
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', []);
			$id = $request->getQuery('id', null);
			$parentId = $request->getQuery('parentId', null);
			(!$id) ? $id = current($ids) : null;
		}
		elseif ( $request->isPost() ) {
			$id = $request->getPost('id', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
			$parentId = $request->getPost('parentId', null);
			;
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		/* Init helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Version::$classId);
		$productVersion = Product\Version::init();

		/* init instance */
		if ( $parentId ) {
			$productInstance = Product\Instance::init();
			/* load parent */
			$parentProduct = $dao->loadFromId(new Product\Version(), $parentId);
			$productInstance->setParent($parentProduct);
			$productVersion->parentId = $parentId;
			$productInstance->dao = $factory->getDao(Product\Instance::$classId);
		}

		/* */
		$form = new \Pdm\Form\ProductVersion\EditForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($productVersion);

		/* Try to validate the form */
		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$productVersion->setNumber(\Rbs\Number::normalize($productVersion->getName()));
				$dao->save($productVersion);

				/* create a instance of this product version */
				if ( isset($productInstance) ) {
					$productInstance->setOfProduct($productVersion);
					$productInstance->setNumber(uniqid());
					$productInstance->setName($productVersion->getName() . '.' . $productInstance->getNumber());
					$productInstance->dao->save($productInstance);
				}

				$this->successForward();
			}
		}

		$view->setTemplate($form->template);
		$view->pageTitle = 'New Product Version';
		$view->form = $form;
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();

		$id = $this->params()->fromRoute('id');

		if ( $request->isPost() ) {
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				$this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromIdAndCid('edit', $id, Product\Version::$classId);

		/* Init helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Version::$classId);
		$productVersion = Product\Version::init();
		$dao->loadFromId($productVersion, $id);

		/* Form */
		$form = new \Pdm\Form\ProductVersion\EditForm($view, $factory);
		$url = $this->url()->fromRoute('pdm-product-version', [
			'action' => 'edit',
			'id' => $id
		]);
		$form->setAttribute('action', $url);
		$form->bind($productVersion);

		/* Try to validate the form */
		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($productVersion);
				$this->successForward();
			}
		}

		$view->setTemplate($form->template);
		$view->form = $form;
		$view->pageTitle = 'Edit Product Version';
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isXmlHttpRequest() ) {
			$view = new ViewModel();
			$view->setTerminal(true);
		}
		else {
			$view = $this->view;
		}
		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', []);
			$id = $request->getQuery('id', null);
			($id) ? $ids[] = $id : null;
		}

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Version::$classId);

		/* Check permissions */
		$this->getAcl()->checkRightFromIdAndCid('read', $id, Product\Version::$classId);

		foreach( $ids as $id ) {
			$dao->deleteFromId($id);
		}

		return $this->successForward();
	}
}
