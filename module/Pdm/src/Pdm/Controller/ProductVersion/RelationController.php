<?php
namespace Pdm\Controller\ProductVersion;

use Zend\View\Model\ViewModel;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Pdm\Product;
use Rbplm\Pdm\Usage;

/**
 * 
 *
 */
class RelationController extends \Pdm\Controller\AbstractController
{

	/** @var string */
	public $pageId = 'product_version_relation';

	/** @var string */
	public $defaultSuccessForward = '/pdm/productversion/index';

	/** @var string */
	public $defaultFailedForward = '/pdm/productversion/index';

	/**
	 * 
	 */
	public function usedbyAction()
	{
		$request = $this->getRequest();
		if ( $request->isXmlHttpRequest() ) {
			$view = new ViewModel();
			$view->setTerminal(true);
		}
		else {
			$view = $this->view;
		}

		/* get param */
		$id = $this->params()->fromRoute('id');

		/* Check permissions */
		//$this->getAcl()->checkRightFromIdAndCid('read', $id, Product\Version::$classId);

		/* init helpers */
		$factory = DaoFactory::get();

		/* Load product */
		try {
			$product = new Product\Version();
			$dao = $factory->getDao(Product\Version::$classId);
			$dao->loadFromId($product, $id);
		}
		catch( \Exception $e ) {}

		/* Init link dao */
		/* @var \Rbs\Pdm\UsageDao $linkDao */
		$linkDao = $factory->getDao(Usage::$classId);

		/**/
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$select = $dao->getSelectAsApp('product');
		$filter->select($select);
		$stmt = $linkDao->getUsedBy($id, $filter);

		/**/
		$view->headers = array(
			array(
				'uid',
				'Uid'
			),
			array(
				'name',
				'Name'
			),
			array(
				'description',
				'Description'
			),
			array(
				'version',
				'Version'
			)
		);

		/**/
		$view->list = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$view->pageTitle = sprintf('Product "%s" Is Used By', $product->getName());
		$view->setTemplate('pdm/product-version/relation/usedby');
		return $view;
	}

	/**
	 *
	 */
	public function childrenAction()
	{
		$request = $this->getRequest();
		if ( $request->isXmlHttpRequest() ) {
			$view = new ViewModel();
			$view->setTerminal(true);
		}
		else {
			$view = $this->view;
		}

		/* get param */
		$id = $this->params()->fromRoute('id');

		/* Check permissions */
		//$this->getAcl()->checkRightFromIdAndCid('read', $id, Product\Version::$classId);

		/* init helpers */
		$factory = DaoFactory::get();

		/* Load product */
		try {
			$product = new Product\Version();
			$dao = $factory->getDao(Product\Version::$classId);
			$dao->loadFromId($product, $id);
		}
		catch( \Exception $e ) {}

		/* Init link dao */
		/* @var \Rbs\Pdm\UsageDao $linkDao */
		$linkDao = $factory->getDao(Usage::$classId);

		/**/
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$select = $dao->getSelectAsApp('product');
		$filter->select($select);
		$stmt = $linkDao->getChildren($id, $filter);

		/**/
		$view->headers = array(
			array(
				'uid',
				'Uid'
			),
			array(
				'name',
				'Name'
			),
			array(
				'description',
				'Description'
			),
			array(
				'version',
				'Version'
			)
		);

		/**/
		$view->list = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$view->pageTitle = sprintf('Product Children Of "%s"', $product->getName());
		$view->setTemplate('pdm/product-version/relation/children');
		return $view;
	}
}
