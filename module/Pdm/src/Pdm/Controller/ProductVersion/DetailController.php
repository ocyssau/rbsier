<?php
namespace Pdm\Controller\ProductVersion;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Pdm\Product;
use Rbplm\Dao\NotExistingException;

/**
 * 
 *
 */
class DetailController extends \Pdm\Controller\AbstractController
{

	/**
	 * Display instance of all process
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('pdm/product-instance/detail/index');

		$productId = $this->params()->fromRoute('id', null);
		$factory = DaoFactory::get();
		$productDao = $factory->getDao(Product\Version::$classId);

		$product = null;
		$document = null;

		/* Load Product */
		try {
			if ( $productId ) {
				$product = new Product\Version();
				$productDao->loadFromId($product, $productId);
			}
		}
		catch( NotExistingException $e ) {
			throw new \Application\Controller\ControllerException(sprintf('The product %s is not existing', $productId));
		}

		/* Load Document */
		try {
			if ( $product ) {
				$documentId = $product->documentId;
				if ( $documentId ) {
					$spacename = $product->spacename;
					$factory = DaoFactory::get($spacename);
					$documentDao = $factory->getDao(\Rbplm\Ged\Document\Version::$classId);
					$document = new \Rbplm\Ged\Document\Version();
					$documentDao->loadFromId($document, $documentId);
					if ( $product ) {
						$product->setDocument($document);
					}
				}
			}
		}
		catch( NotExistingException $e ) {}

		$view->instance = null;
		$view->product = $product;
		$view->document = $document;
		$view->pageTitle = 'Product Version Details';
		return $view;
	}
}
