<?php
namespace Pdm\Controller;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Pdm\Product;
use Rbplm\Dao\Filter\Op;

/**
 * 
 */
class DocumentController extends \Ged\Controller\Document\ManagerController
{

	/* @var string */
	public $pageId = 'document_relation';

	/* @var string */
	public $defaultSuccessForward = 'ged/document/manager';

	/* @var string */
	public $defaultFailedForward = 'ged/document/manager';

	/**
	 * 
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		\Application\Controller\AbstractController::init();

		$context = \Ranchbe::get()->getServiceManager()->getContext();
		$containerId = $context->getData('containerId');
		$spacename = $context->getData('spacename');

		/**/
		$this->ifSuccessForward = $this->defaultSuccessForward . '/' . $spacename . '/' . $containerId;
		$this->ifFailedForward = $this->defaultFailedForward . '/' . $spacename . '/' . $containerId;
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function initAjax()
	{
		\Application\Controller\AbstractController::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Project::$appCn;
	}

	/**
	 */
	public function indexAction()
	{}

	/**
	 */
	public function exitAction()
	{
		return $this->successForward();
	}

	/**
	 * 
	 * @throws \Application\Controller\ControllerException
	 * @return \Application\View\ViewModel
	 */
	function getrelationAction()
	{
		$view = $this->view;

		$view->setTemplate('ged/document/manager/index');
		$spacename = $this->params()->fromRoute('spacename', null);
		$id = $this->params()->fromRoute('id', null);
		$direction = $this->params()->fromRoute('direction', null);

		if ( !$spacename ) {
			throw new \Application\Controller\ControllerException('spacename is not set');
		}

		/**/
		$this->pageId = $spacename . '_' . $this->pageId;

		$this->layoutSelector()->clear($this);
		$url = $this->url()->fromRoute('pdm-document-relation', array(
			'direction' => $direction,
			'spacename' => $spacename,
			'id' => $id
		));
		$this->basecamp()->save($url, $url, $view);

		/* */
		$factory = DaoFactory::get($spacename);

		/* Load parent document */
		try {
			$document = new Document\Version();
			$dao = $factory->getDao(Document\Version::$classId);
			$dao->loadFromId($document, $id);
			$containerId = $document->getParent(true);
		}
		catch( \Exception $e ) {}

		/* Load product of document */
		try {
			$product = new Product\Version();
			$dao = $factory->getDao(Product\Version::$classId);
			$dao->load($product, $dao->toSys('documentId') . '=:documentId', array(
				':documentId' => $id
			));
			$product->setDocument($document);
		}
		catch( \Exception $e ) {}

		/* Load container */
		try {
			$container = $factory->getModel(\Rbplm\Org\Workitem::$classId);
			$dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
			$container = $dao->loadFromId($container, $containerId);

			/* Load extended properties */
			$this->loadExtended($document->dao, $containerId);
			$view->extended = $document->dao->extended;
		}
		catch( \Exception $e ) {}

		/* Check permissions */
		$this->getAcl()->checkRightFromUid('read', $container->getUid());

		/* */
		$filterForm = $this->getFilter($factory, $containerId);
		$filter = $filterForm->daoFilter;
		$filter->setAlias('doc');
		$filter->andfind($id, 'doc.' . $document->dao->toSys('id'), Op::EQUAL);
		$filter->setBind($filterForm->bind);

		/* Init link dao */
		/* @var \Rbs\Ged\Document\PdmLinkDao $linkDao */
		$linkDao = $factory->getDao(Document\PdmLink::$classId);

		/* Build Select */
		$childMetamodel = array_merge($document->dao->metaModel, $document->dao->extendedModel);
		if ( $childMetamodel ) {
			foreach( $childMetamodel as $asSys => $asApp ) {
				$select[] = 'doc.' . $asSys . ' AS ' . $asApp;
			}
		}
		$filter->select($select);

		/**/
		$filterForm->paginator->setMaxLimit(1000)
			->bindToFilter($filter)
			->bindToView($view)
			->save();

		if ( $direction == 'children' ) {
			$stmt = $linkDao->getChildren($filter, $filter->getBind());
			$view->pageTitle = sprintf(tra('Pdm Children of %s'), $document->getName());
		}
		else {
			$stmt = $linkDao->getParents($filter, $filter->getBind());
			$view->pageTitle = sprintf(tra('Pdm Parent of %s'), $document->getName());
		}

		/* get fixed document */
		$this->fixedDocument($view);

		/* Display */
		$view->list = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$view->filter = $filterForm;
		$view->columns = $filterForm->columnSelector->getSelected();
		$view->displayThumbs = $filterForm->get('displayThumbs')->getValue();
		$view->documentid = $id;
		$view->spacename = $spacename;
		return $view;
	}

	/**
	 * HTTP GET AJAX METHO
	 * 
	 * Load or init a new product from a document
	 */
	public function initproductAction()
	{
		$documentId = $this->params()->fromRoute('id');
		$spacename = $this->params()->fromRoute('spacename');

		/* */
		$service = new \Service\Controller\Pdm\DocumentversionService();
		$return = $service->initproductService(array(
			'documents' => array(
				'document1' => array(
					'id' => $documentId,
					'spacename' => $spacename
				)
			)
		));

		/* Get index action */
		$product = $return->getFeedback('document1')->getData();
		return $this->redirect()->toRoute('pdm-product-explorer', array(
			'action' => 'index',
			'id' => $product->getId()
		));
	}
} /* End of class */
