<?php
namespace Ranchbe\Service;

use Zend\Session\SessionManager;
use Zend\Session\Container;

/** 
 * @author olivier
 * 
 */
class SessionContainerRegistry
{

	/**
	 * @var SessionManager
	 */
	private $sessionManager;

	/**
	 * Registry for containers instances
	 * @var array
	 */
	private $registry = [];

	/**
	 * 
	 */
	public function __construct(SessionManager $sessionManager)
	{
		$this->sessionManager = $sessionManager;
	}

	/**
	 * Create and return a named container (v2 usage).
	 *
	 * @param  string $name
	 * @return Container
	 */
	public function get($name)
	{
		$key = self::normalizeContainerName($name);
		if ( !isset($this->registry[$key]) ) {
			$container = new \Zend\Session\Container($name, $this->getSessionManager());
			$this->registry[$key] = $container;
		}
		return $this->registry[$key];
	}

	/**
	 * Create and return a named container (v2 usage).
	 *
	 * @param  string $name
	 * @return SessionContainerRegistry
	 */
	public function remove($name)
	{
		$key = self::normalizeContainerName($name);
		if ( isset($this->registry[$key]) ) {
			/* @var \Zend\Session\Storage\SessionArrayStorage $storage */
			$container = $this->registry[$key];
			$container->getManager()
				->getStorage()
				->clear($name);
			unset($this->registry[$key]);
		}
		return $this;
	}

	/**
	 * Retrieve the session manager instance, if any
	 *
	 * @return null|SessionManager
	 */
	private function getSessionManager()
	{
		return $this->sessionManager;
	}

	/**
	 * Normalize the container name in order to perform a lookup
	 *
	 * @param  string $name
	 * @return string
	 */
	private static function normalizeContainerName($name)
	{
		return strtolower($name);
	}
}

