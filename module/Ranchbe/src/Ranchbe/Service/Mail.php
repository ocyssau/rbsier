<?php
namespace Ranchbe\Service;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Exception;

/**
 */
class Mail
{

	/**
	 *
	 * @var \Zend\Mail\Transport\Smtp
	 */
	protected $smtpTransport;

	/**
	 *
	 */
	protected $rbTransport;

	/**
	 *
	 * @var array
	 */
	protected $options;

	/**
	 *
	 */
	public function __construct()
	{}

	/**
	 * Factory method for message
	 *
	 * @return \Zend\Mail\Message
	 */
	public static function messageFactory()
	{
		return new \Zend\Mail\Message();
	}

	/**
	 *
	 */
	public function send($message)
	{
		try {
			if ( $this->smtpTransport ) {
				$this->smtpTransport->send($message);
			}
			if ( $this->rbTransport ) {
				$this->rbTransport->send($message);
			}
		}
		catch( \Exception $e ) {
			throw new Exception('Send of mail failed with reason : ' . $e->getMessage(), 6000, $e);
		}
	}

	/**
	 *
	 * @return SmtpTransport
	 */
	public function getSmtpTransport()
	{
		return $this->smtpTransport;
	}

	/**
	 * @param SmtpTransport $transport
	 * @return Mail
	 */
	public function setSmtpTransport($transport)
	{
		$this->smtpTransport = $transport;
		return $this;
	}

	/**
	 *
	 */
	public function getRbTransport()
	{
		return $this->rbTransport;
	}

	/**
	 *
	 * @return Mail
	 */
	public function setRbTransport($transport)
	{
		$this->rbTransport = $transport;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}
}
