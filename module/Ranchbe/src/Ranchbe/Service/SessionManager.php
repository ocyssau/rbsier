<?php
namespace Ranchbe\Service;

/** 
 * Only to document properties not builtin \Zend\Session\SessionManager
 * 
 */
class SessionManager extends \Zend\Session\SessionManager
{
	/**
	 * @var SessionContainerRegistry
	 */
	public $containerRegistry;
}
