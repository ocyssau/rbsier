<?php
namespace Ranchbe;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Ged\Document\Service as DocumentService;
use Rbs\Ged\Docfile\Service as DocfileService;
use Ged\Archiver\Archiver as ArchiverService;
use Zend\Authentication\AuthenticationService;
use Rbs\Auth;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Docflow\Service\Workflow\Code as ActivityCode;
use Rbplm\People;
use Rbs\People\User\Preference as UserPreference;
use Rbplm\Sys\Filesystem;
use Application\Helper\ErrorStack;
use Application\Helper\ServiceErrorStack;
#use Application\Controller\Plugin\FlashMessenger;
use Rbplm\Dao\Connexion;

//use Zend\ServiceManager\ServiceManager as mvcServiceManager;

/**
 * 
 * Service Manager class
 *
 */
class ServiceManager
{

	/**
	 * 
	 * @var array
	 */
	protected $services;

	/**
	 * 
	 * @var \Ranchbe
	 */
	protected $ranchbe;

	
	/**
	 *
	 * @var bool
	 */
	protected $isService;
	
	/**
	 *
	 * @param \Ranchbe $sm
	 */
	public function __construct(\Ranchbe $ranchbe, $isService = false)
	{
		$this->services = [];
		$this->ranchbe = $ranchbe;
		$this->isService = $isService;
	}
	
	/**
	 * 
	 * @param bool $bool
	 * @return boolean|\Ranchbe\ServiceManager
	 */
	public function isService($bool = null)
	{
		if($bool === null){
			return $this->isService; 
		}
		else{
			$this->isService = (bool)$bool;
			return $this;
		}
	}
	
	/**
	 * @return \Ranchbe\Service\SessionManager
	 */
	public function getSessionManager()
	{
		if ( !isset($this->services['sessionManager']) ) {
			$options = $this->ranchbe->getConfig();
			$config = new \Zend\Session\Config\SessionConfig();
			$config->setOptions([
				'use_cookies' => 1,
				'use_only_cookies' => 1,
				'name' => $options['session.name'],
				'save_path' => $options['session.path'],
				'remember_me_seconds' => 600,
				'gc_maxlifetime' => 32400,
				'cookie_lifetime' => 32400,
				'gc_probability' => 1,
				'gc_divisor' => 100
			]);
			$manager = new \Zend\Session\SessionManager($config);
			$factory = new \Ranchbe\Service\SessionContainerRegistry($manager);
			$manager->containerRegistry = $factory;
			if ( !$manager->sessionExists() ) {
				$manager->start();
			}
			$this->services['sessionManager'] = $manager;
		}
		return $this->services['sessionManager'];
	}
	
	/**
	 * @return \Ranchbe\Service\SessionManager
	 */
	public function getMvcApplication()
	{
		if ( !isset($this->services['mvcApplication']) ) {
			$appConfig = [];
			$application = \Zend\Mvc\Application::init($appConfig);
			$this->ranchbe->setMvcApplication($application);
		}
		return $this->services['mvcApplication'];
	}
	
	

	/**
	 * @return \Zend\Session\SessionManager
	 */
	public function getDaoService()
	{
		if ( !isset($this->services['daoservice']) ) {
			if ( is_file('config/autoload/objectdaomap.local.php') ) {
				$map = include 'config/autoload/objectdaomap.local.php';
			}
			else {
				$map = include 'config/dist/objectdaomap.config.php';
			}

			Connexion::setConfig($this->ranchbe->getConfig('db'));
			$connexion = Connexion::get();

			\Rbs\Space\Factory::setMap($map);
			\Rbs\Space\Factory::setConnexion($connexion);
			\Rbs\Space\Factory::setOption('listclass', '\Rbs\Dao\Sier\DaoList');
			\Rbs\Space\Factory::setOption('filterclass', '\Rbs\Dao\Sier\Filter');
			$this->services['daoservice'] = \Rbs\Space\Factory::get();
		}
		return $this->services['daoservice'];
	}

	/**
	 * @return \Rbs\People\User\Context
	 */
	public function getContext()
	{
		if ( !isset($this->services['context']) ) {
			$sessionStorage = $this->getSessionManager()->getStorage();
			$context = new \Rbs\People\User\Context();
			$context->setSessionStorage($sessionStorage);
			$context->setDao(DaoFactory::get()->getDao($context->cid));
			$userId = $this->currentUserService()->getId();
			$context->setUserId($userId);

			$this->services['context'] = $context;

			/* if session is not yet loaded, load datas from db */
			if ( $context->loadFromSession()->getData('isLoaded') == false ) {
				$context->getDao()->loadFromUserId($context, $userId);
				$context->setData('isLoaded', 1);
				$context->saveInSession();
			}
		}
		return $this->services['context'];
	}

	/**
	 *
	 * @return \Application\Helper\ErrorStack
	 */
	public function getErrorStack()
	{
		if ( !isset($this->services['errorStack']) ) {
			if($this->isService()){
				$errorStack = new ServiceErrorStack($this->getLogger());
			}
			else{
				$errorStack = new ErrorStack($this->getLogger(), $this->getFlashMessenger());
			}
			$this->services['errorStack'] = $errorStack;
		}
		
		return $this->services['errorStack'];
	}

	/**
	 * 
	 * @return \Rbplm\Sys\Logger
	 */
	public function getLogger()
	{
		if ( !isset($this->services['logger']) ) {
			$config = $this->ranchbe->getConfig();
			$logger = \Rbplm\Sys\Logger::singleton();
			$logfile = $config['path.log'];
			if ( $logfile == 'syslog' ) {
				$logger->initSyslog('Ranchbe');
			}
			else {
				$apacheLog = new \Zend\Log\Writer\Stream($logfile);
				$logger->addWriter($apacheLog);
			}
			$this->services['logger'] = $logger;
		}
		return $this->services['logger'];
	}

	/**
	 *
	 * @return \Zend\Mvc\Controller\Plugin\FlashMessenger
	 */
	public function getFlashMessenger()
	{
		if ( !isset($this->services['flashMessenger']) ) {
			
			$flashMessenger = new \Zend\Mvc\Controller\Plugin\FlashMessenger();
			$flashMessenger->setSessionManager($this->getSessionManager());
			$flashMessenger->setNamespace('flashmessages');
			
			$this->services['flashMessenger'] = $flashMessenger;
		}
		
		return $this->services['flashMessenger'];
	}
	

	/**
	 * @param DaoFactory $factory
	 * @return ArchiverService
	 */
	public function getArchiverService(DaoFactory $factory)
	{
		if ( !isset($factory->services['archiverService']) ) {
			$config = $this->ranchbe->getConfig();
			$spacename = strtolower($factory->getName());
			$basePath = $config['path.reposit.' . $spacename . '.archive'];
			$archiver = new ArchiverService($this, $basePath);

			/* set event manager */
			$archiver->setEventManager($this->ranchbe->getShareEventManager());

			$factory->services['archiverService'] = $archiver;
		}
		return $factory->services['archiverService'];
	}

	/**
	 *
	 * @param DaoFactory $factory
	 * @return DocumentService
	 */
	public function getDocumentService(DaoFactory $factory): DocumentService
	{
		if ( !isset($factory->services['documentService']) ) {
			$service = new DocumentService($factory);

			/* set event manager */
			$service->setEventManager($this->ranchbe->getShareEventManager());

			$factory->services['documentService'] = $service;
		}
		return $factory->services['documentService'];
	}

	/**
	 *
	 * @param DaoFactory $factory
	 * @return DocfileService
	 */
	public function getDocfileService(DaoFactory $factory): DocfileService
	{
		if ( !isset($factory->services['docfileService']) ) {
			$service = new DocfileService($factory);

			/* set event manager */
			$service->setEventManager($this->ranchbe->getShareEventManager());

			$factory->services['docfileService'] = $service;
		}
		return $factory->services['docfileService'];
	}

	/**
	 * @return \Zend\Authentication\AuthenticationService
	 */
	public function getAuthService()
	{
		if ( !isset($this->services['auth']) ) {
			$config = $this->ranchbe->getConfig();
			/* @var \Rbs\Auth\Ldap | \Rbs\Auth\DbAdapter $authAdapter */
			$authAdapter = Auth\Factory::getAdapter($config['auth']);
			$authService = new AuthenticationService();
			$authService->setAdapter($authAdapter);
			$sessionManager = $this->getSessionManager();
			$storage = new Auth\Storage('auth', 'memberName', $sessionManager);
			$authService->setStorage($storage);
			$this->services['auth'] = $authService;
		}
		return $this->services['auth'];
	}

	/**
	 * @return People\CurrentUser
	 */
	public function currentUserService()
	{
		if ( !isset($this->services['currentUser']) ) {
			$authservice = $this->getAuthService();
			$storage = $authservice->getStorage();
			$config = $this->ranchbe->getConfig();
			
			if ( $authservice->hasIdentity() ) {
				$userProperties = $storage->read();
				
				if ( !isset($userProperties['id']) ) {
					throw new \Exception('User id is not set');
				}

				$user = new People\CurrentUser;
				$user->hydrate($userProperties);
				if ( isset($userProperties['preferences']) ) {
					$preference = UserPreference::get($user);
					$preference->hydrate($userProperties['preferences']);
				}
				
				$logger = $this->getLogger();
				if ( $logger ) {
					$logger->setPrefix('[' . $user->getLogin() . ']');
				}
				
				$this->userPreferences($user);

				$user->setLastLogin(new \DateTime());
				$this->services['currentUser'] = $user;
			}
			/* Anonymous user */
			else {
				//throw new \Exception('None identity');
				$userProperties = [
					'id' => $config['user.anonymous.id'],
					'uid' => $config['user.anonymous.uid'],
					'lastLogin' => '',
					'firstname' => 'anonymous',
					'lastname' => '',
					'mail' => $config['user.anonymous.mail'],
					'login' => $config['user.anonymous.login'],
					'isActive' => $config['user.anonymous.isactive'],
					'authFrom' => 'db',
					'loaded' => true,
					'preferences' => [
						'lang' => $config['view.lang'],
						'cssSheet' => $config['view.stylesheet.custom']
					]
				];

				$user = new \Rbs\People\Anonymous();
				$user->hydrate($userProperties);
				if ( isset($userProperties['preferences']) ) {
					$preference = UserPreference::get($user);
					$preference->hydrate($userProperties['preferences']);
				}

				People\CurrentUser::set($user);
				$this->services['currentUser'] = $user;
			}

			People\User\Wildspace::$basePath = $config['path.reposit.wildspace'];
			$wildspace = People\User\Wildspace::get($user);
			Filesystem::addAuthorized($wildspace->getPath());
			$wildspace->init();
		}
		
		return $this->services['currentUser'];
	}

	/**
	 * 
	 * @param People\CurrentUser $user
	 */
	private function userPreferences($user)
	{
		$ranchbe = $this->ranchbe;
		$config = $ranchbe->getConfig();
		$preference = UserPreference::get($user);
		$preference->isEnable($config['user.allowprefs']);
		if ( $preference->isEnable() ) {
			if ( $preference->isLoaded() == false ) {
				try {
					$this->getDaoService()
						->getDao($preference->cid)
						->loadFromOwnerId($preference, $user->getId());
				}
				catch( \Rbplm\Dao\NotExistingException $e ) {
					return;
				}
			}
			$preference->setDefault([
				'cssSheet' => $config['view.stylesheet'],
				'lang' => $config['view.lang'],
				'longDateFormat' => $config['view.longdate.format'],
				'shortDateFormat' => $config['view.shortdate.format'],
				'hourFormat' => $config['view.hour.format'],
				'timeZone' => $config['view.timezone'],
				'rbgateServerUrl' => $config['rbgate.server.url']
			]);
			$prefs = $preference->getPreferences();
			if ( $prefs['lang'] ) {
				\Ranchbe::$lang = $prefs['lang'];
				$config['view.lang'] = $prefs['lang'];
			}
			($prefs['cssSheet']) ? $config['view.stylesheet'] = $prefs['cssSheet'] : null;
			($prefs['longDateFormat']) ? $config['view.longdate.format'] = $prefs['longDateFormat'] : null;
			($prefs['shortDateFormat']) ? $config['view.shortdate.format'] = $prefs['shortDateFormat'] : null;
			($prefs['hourFormat']) ? $config['view.hour.format'] = $prefs['hourFormat'] : null;
			($prefs['timeZone']) ? $config['view.timezone'] = $prefs['timeZone'] : null;
			($prefs['rbgateServerUrl']) ? $config['rbgate.server.url'] = $prefs['rbgateServerUrl'] : null;
			$ranchbe->setConfig($config);
		}
	}

	/**
	 * @return \Ranchbe\Service\Mail
	 */
	public function getMailService()
	{
		if ( !isset($this->services['mail']) ) {
			$config = $this->ranchbe->getConfig();
			$smtpConfig = $config['mail.smtp'];
			$mailEnable = $config['message.mail.enable'];
			$messageEnable = $config['message.enable'];
			$mail = new \Ranchbe\Service\Mail();

			if ( $mailEnable == true ) {
				$mail->setSmtpTransport(new SmtpTransport(new SmtpOptions($smtpConfig)));
			}
			if ( $messageEnable == true ) {
				// $mail->setRbTransport(new RbTransport());
			}

			$this->services['mail'] = $mail;
		}
		return $this->services['mail'];
	}

	/**
	 * @throws \Exception
	 * @return \Docflow\Service\Workflow
	 */
	public function getWorkflowService()
	{
		if ( !isset($this->services['workflow']) ) {
			$config = $this->ranchbe->getConfig();

			/*@var \Zend\ServiceManager\ServiceManager $sm */
			$sm = $this->ranchbe->getMvcApplication()->getServiceManager();

			$path = $config['path.workflow.process'];
			$namespace = trim($config['namespace.workflow.process'], '\\');

			if ( realpath($path) == false ) {
				throw new \Exception("Path $path is not existing");
			}

			/** @var \Zend\View\Resolver\TemplatePathStack $templatePathResolver */
			$templatePathResolver = $sm->get('Zend\View\Resolver\TemplatePathStack');
			$templatePathResolver->addPath(realpath($path));

			$this->ranchbe->getAutoloader()->set($namespace . '\\', substr($path, 0, strrpos($path, '/')));
			ActivityCode::$processPath = $path;
			ActivityCode::$processNamespace = $namespace;
			$workflow = new \Docflow\Service\Workflow();
			$workflow->serviceManager = $sm;
			$workflow->errorStack = $this->getErrorStack();
			
			$workflow->setEventManager($this->ranchbe->getShareEventManager());
			$workflow->setEventFactory(function ($name, $emitter) {
				return new \Docflow\Model\Event($name, $emitter);
			});
			$this->services['workflow'] = $workflow;
		}

		return $this->services['workflow'];
	}
}
