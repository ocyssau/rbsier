<?php
use Application\Helper\ErrorStackInterface;
use Ranchbe\ServiceManager;
use Rbs\EventDispatcher\Dispatcher;
use Rbs\EventDispatcher\EventManager;

/**
 *
 *
 */
class Ranchbe
{

	/**
	 * Current version of Ranchbe
	 * @var string
	 */
	const VERSION = '1.1';

	/**
	 * Build number of Ranchbe
	 * @var string
	 */
	const BUILD = '%BUILD-ID%';

	/**
	 * @var string
	 */
	const GITID = '%GIT-ID%';

	/**
	 * Copyright
	 * @var string
	 */
	const COPYRIGHT = '&#169;2007-%s By Olivier CYSSAU For SIER';

	/**
	 * Web Site
	 * @var string
	 */
	public static $WEBSITE = 'https://bitbucket.org/ocyssau/rbsier';

	/**
	 * Web Site Bug manager
	 * @var string
	 */
	public static $ISSUEMANAGER = 'https://bitbucket.org/ocyssau/rbsier/issues';

	/**
	 * Web Site Bug manager
	 * @var string
	 */
	public static $HELPSITE = 'https://bitbucket.org/ocyssau/rbsier/wiki/Home';

	/**
	 *
	 * @var \Rbplm\Sys\Logger
	 */
	private $logger = null;

	/**
	 *
	 * @var \Application\View\ViewModel
	 */
	private $view = null;

	/**
	 * OBSOLETE
	 */
	private static $db = null;

	/**
	 *
	 * @var ErrorStackInterface
	 */
	private $errorStack = null;

	/**
	 *
	 * @var \Zend\Authentication\Adapter\AdapterInterface $auth
	 */
	private $auth = null;

	/**
	 *
	 */
	private $autoloader = null;

	/**
	 *
	 * @var array
	 */
	protected $fileTypes;

	/**
	 * authorized extensions
	 * @var array
	 */
	protected $fileExtensions = null;

	/**
	 * authorized file extensions for visu files
	 * @var array
	 */
	protected $visuFileExtensions = null;

	/**
	 *
	 * @var string
	 */
	public static $lang = 'en';

	/**
	 *
	 * @var array
	 */
	public static $traductions;

	/**
	 *
	 * @var array
	 */
	public static $registry = [];

	/**
	 * @var Ranchbe
	 */
	protected static $instance;

	/**
	 * @var array
	 */
	protected $config;

	/**
	 * @var ServiceManager
	 */
	protected $serviceManager;

	/**
	 * @var EventManager
	 */
	protected $eventManager;

	/**
	 * Current Zend Mvc Application instance
	 *
	 * @var \Zend\Mvc\Application
	 */
	protected $mvcApplication;

	/**
	 *
	 */
	public function __construct()
	{
		if ( self::$instance == null ) {
			self::$instance = $this;
		}
		else {
			throw new Exception("Try to instanciate Ranchbe twice. Use static method get() instead.");
		}
	}

	/**
	 *
	 * @return Ranchbe
	 */
	public static function get()
	{
		if ( !isset(self::$instance) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * @param array $array
	 * @return Ranchbe
	 */
	public function setConfig($array)
	{
		$this->config = $array;
		return $this;
	}

	/**
	 * Return the value of configuration constant value.
	 * $name may be under form prefix.value in lower case
	 *
	 * @return array
	 */
	public function getConfig($name = null)
	{
		if ( $name ) {
			if ( isset($this->config[$name]) ) {
				return $this->config[$name];
			}
			else {
				$constName = strtoupper(str_replace('.', '_', $name));
				if ( defined($constName) ) {
					return constant($constName);
				}
			}
		}
		else {
			return $this->config;
		}
	}

	/**
	 * @param \Zend\Mvc\Application
	 * @return Ranchbe
	 */
	public function setMvcApplication(\Zend\Mvc\Application $application)
	{
		$this->mvcApplication = $application;
		$application->ranchbe = $this;
		return $this;
	}

	/**
	 * @return \Zend\Mvc\Application
	 */
	public function getMvcApplication()
	{
		return $this->mvcApplication;
	}

	/**
	 * Return the service manager instance
	 * 
	 * @return \Ranchbe\ServiceManager
	 */
	public function getServiceManager()
	{
		if ( !isset($this->serviceManager) ) {
			$this->serviceManager = new ServiceManager($this);
		}
		return $this->serviceManager;
	}

	/**
	 *
	 * @return \Rbs\EventDispatcher\EventManager
	 */
	public function getShareEventManager()
	{
		if ( !isset($this->eventManager) ) {
			$this->eventManager = new EventManager(new Dispatcher([
				Rbs\Ged\Document\ListenerProviderFactory::get(),
				Rbs\Ged\Docfile\ListenerProviderFactory::get(),
				Docflow\Model\ListenerProviderFactory::get()
			]));
		}
		return $this->eventManager;
	}

	/**
	 * @return string
	 */
	public static function getVersion($ver = true, $build = true, $cr = true)
	{
		$ret = array();
		($ver) ? $ret[] = 'RanchBE ver:' . self::VERSION : null;
		($build) ? $ret[] = 'Build:' . self::BUILD : null;
		($cr) ? $ret[] = self::COPYRIGHT : null;
		return implode(' ', $ret);
	}

	/**
	 * @return \Rbplm\Sys\Logger
	 */
	public function getLogger()
	{
		return $this->logger;
	}

	/**
	 *
	 * @param \Rbplm\Sys\Logger $logger
	 */
	public function setLogger($logger)
	{
		$this->logger = $logger;
		return $this;
	}

	/**
	 *
	 * @param string $msg
	 */
	public function log($msg)
	{
		if ( $this->logger ) {
			$this->logger->log($msg);
		}
	}

	/**
	 * @return \Application\View\ViewModel
	 */
	public function getView()
	{
		return $this->view;
	}

	/**
	 *
	 * @param \View\View | \Application\View\ViewModel $view
	 */
	public function setView($view)
	{
		$this->view = $view;
	}

	/**
	 * @return ErrorStackInterface
	 */
	public function getErrorStack()
	{
		return $this->errorStack;
	}

	/**
	 *
	 * @param ErrorStackInterface $errorStack
	 */
	public function setErrorStack(ErrorStackInterface $errorStack)
	{
		$this->errorStack = $errorStack;
		return $this;
	}

	/**
	 *
	 */
	public function setAutoloader($loader)
	{
		$this->autoloader = $loader;
		return $this;
	}

	/**
	 *
	 */
	public function getAutoloader()
	{
		return $this->autoloader;
	}

	/**
	 * Define path mapping
	 * $path_mapping is used to translate path from server to path use by the client.
	 * You can use %user% wich will be replaced by the username.
	 */
	public function getPathMap()
	{
		switch (CLIENT_OS) {
			// Windows
			case 'WINDOWS':
				$path_mapping = array(
					DEFAULT_MOCKUP_DIR => MOCKUP_WINDOWS_PATH_MAPPING,
					DEFAULT_BOOKSHOP_DIR => BOOKSHOP_WINDOWS_PATH_MAPPING,
					DEFAULT_CADLIB_DIR => CADLIB_WINDOWS_PATH_MAPPING,
					DEFAULT_WORKITEM_DIR => WORKITEM_WINDOWS_PATH_MAPPING,
					DEFAULT_WILDSPACE_DIR => WILSPACE_WINDOWS_PATH_MAPPING
				);
				break;
			// Mac
			case 'MACINTOSH':
				$path_mapping = array(
					DEFAULT_MOCKUP_DIR => MOCKUP_MAC_PATH_MAPPING,
					DEFAULT_BOOKSHOP_DIR => BOOKSHOP_MAC_PATH_MAPPING,
					DEFAULT_CADLIB_DIR => CADLIB_MAC_PATH_MAPPING,
					DEFAULT_WORKITEM_DIR => WORKITEM_MAC_PATH_MAPPING,
					DEFAULT_WILDSPACE_DIR => WILSPACE_MAC_PATH_MAPPING
				);
				break;
			// Unix
			case 'UNIX':
				$path_mapping = array(
					DEFAULT_MOCKUP_DIR => MOCKUP_UNIX_PATH_MAPPING,
					DEFAULT_BOOKSHOP_DIR => BOOKSHOP_UNIX_PATH_MAPPING,
					DEFAULT_CADLIB_DIR => CADLIB_UNIX_PATH_MAPPING,
					DEFAULT_WORKITEM_DIR => WORKITEM_UNIX_PATH_MAPPING,
					DEFAULT_WILDSPACE_DIR => WILSPACE_UNIX_PATH_MAPPING
				);
				break;
		}
		return $path_mapping;
	}

	/**
	 *
	 */
	public static function tra($txt)
	{
		if ( is_null(self::$traductions) ) {
			$lg = self::$lang;
			self::$traductions = include ("data/lang/$lg/language.php");
		}
		$key = strtolower($txt);
		if ( isset(self::$traductions[$key]) ) {
			return self::$traductions[$key];
		}
		else {
			return $txt;
		}
	}

	/**
	 * Set Authorized files types.
	 * 	Extract list of type from mimes.conf file and populate
	 *  static array var self::$fileTypes and self::$fileExtensions
	 *
	 * @return void
	 */
	protected function _loadFilesTypes()
	{
		$dao = new \Rbs\Typemime\TypemimeDao(\Rbplm\Dao\Connexion::get());
		$typesmimes = $dao->getTypes();
		$validFileExt = array(); //init var

		/**/
		foreach( $typesmimes as $typesmime ) {
			$extensions = explode(' ', $typesmime['extensions']);
			$validFileExt = array_merge($validFileExt, $extensions);
		}

		/* Valid extension for type file */
		$this->fileTypes['file'] = 'file';
		$this->fileExtensions['file'] = array_combine($validFileExt, $validFileExt);
		sort($this->fileExtensions['file']);

		/* Define extension to determine no file type */
		$this->fileTypes['nofile'] = 'nofile'; //Never change this value

		/* Valid extension for type nofile */
		$this->fileExtensions['nofile'] = array(
			'NULL' => ''
		);

		$visuFileExtensions = $this->getConfig('visu.fileExtension');
		$this->visuFileExtensions = array_combine($visuFileExtensions, $visuFileExtensions);
		sort($this->visuFileExtensions);
	}

	/**
	 * Get file type from _getFilesTypes() method
	 *
	 * @return array
	 */
	public function getFileTypes()
	{
		if ( $this->fileTypes === null ) {
			$this->_loadFilesTypes();
		}
		return $this->fileTypes;
	}

	/**
	 * Get file extensions from _getFilesTypes() method
	 *
	 * @return array
	 */
	public function getFileExtensions()
	{
		if ( $this->fileExtensions === null ) {
			$this->_loadFilesTypes();
		}
		return $this->fileExtensions;
	}

	/**
	 * Get visu file extensions list from _getFilesTypes() method
	 *
	 * @return array
	 */
	public function getVisuFileExtensions()
	{
		if ( $this->visuFileExtensions === null ) {
			$this->_loadFilesTypes();
		}
		return $this->visuFileExtensions;
	}
}


