<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'adminuser' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/acl/user/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Acl\Controller\User\Index',
						'action' => 'index',
					),
				),
			),
			'aclrole' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/acl/role[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Acl\Controller\Role\Index',
						'action' => 'index',
					),
				),
			),
			'permission' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/acl/permission[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Acl\Controller\Permission\Index',
						'action' => 'index',
					),
				),
			),
		), //routes
	), //router
	'controllers' => array(
		'invokables' => array(
			'Acl\Controller\User\Index'=>'Acl\Controller\User\IndexController',
			'Acl\Controller\Role\Index' => 'Acl\Controller\Role\IndexController',
			'Acl\Controller\Permission\Index' => 'Acl\Controller\Permission\IndexController',
		),
	), //controller
	'view_manager' => array(
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	), //view_manager
);
