	Acls are build on Zend\Acl paradigmes :
	Resources, Role, Rule.

	Resource is what the Acl is apply
	Role is the actor which want access to Resource.
	Right is the action that the Role want do on Resource.
	Rule define the right level for the Right on Resource for User = accept or deny
	UserRole is link between User, Resource, Role.

	Resources :
	Resources are identified by a string composed with uid of parent concat with a uid for current resource as :
	'app/dt/1' = resource of doctype 1
	
	The base resources as define as a tree like :
		Application, resourceId='app'
			| Admin, resourceId='app/admin'
			|		| Doctype resourceId='app/admin/dt',
			|		| Workflow resourceId='app/admin/wf',
			|		| People and Acl resourceId='app/admin/acl',
			| Ged, resourceId='app/ged'
			|		| Project, resourceId=='app/ged/[uid du project]'
			|				|Document, resourceId='app/ged/[uid du project]/[uid du document]'
			| CurrentUser, resourceId='app/owner'
				| Wildspace, resourceId='app/owner/ws'
