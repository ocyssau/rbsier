<?php
// %LICENCE_HEADER%
namespace Acl\Service;

use Acl\Model\Right;
use Acl\Model\Rule;
use Acl\Model\Resource;
use Rbplm\People;
use Rbs\Space\Factory as DaoFactory;
use Acl\AccessRestrictionException;
use Rbplm\Dao\NotExistingException;

/**
 * Helper for Acls
 */
class Acl
{

	/**
	 * Base Canonical Name
	 *
	 * @var string
	 */
	protected $baseCn;

	/**
	 * @var \Zend\Mvc\Controller\AbstractActionController
	 */
	protected $controller;

	/**
	 *
	 * @var \Zend\Permissions\Acl\Acl
	 */
	//protected $acl;

	/**
	 * @param \Zend\Mvc\Controller\AbstractActionController $controller
	 */
	public function __construct($controller = null)
	{
		$this->controller = $controller;
		//$this->acl = new \Zend\Permissions\Acl\Acl();
	}

	/**
	 *
	 * @param string $cn
	 */
	public function setBaseCn($cn)
	{
		$this->baseCn = $cn;
		return $this;
	}

	/**
	 * @param string $rightName
	 * @param string $resourceCn
	 * @return boolean
	 * @throws AccessRestrictionException
	 */
	public function checkRightFromCn($rightName, $resourceCn)
	{
		/* @var \Acl\Model\RuleDao $dao */
		$dao = DaoFactory::get()->getDao(Rule::$classId);
		$userId = (int)People\CurrentUser::get()->getId();
		$controller = $this->controller;

		/* all rights for system admin */
		if ( $userId == 1 ) {
			return true;
		}
		$rightId = Right::getIdFromName($rightName);

		/**/
		try {
			if ( !$resourceCn ) {
				$resourceCn = 'undefined';
				throw new AccessRestrictionException('$resourceCn is undefined');
			}
			if ( !$rightName ) {
				$rightName = 'undefined';
				throw new AccessRestrictionException('$rightName is undefined');
			}
			if ( !$rightId ) {
				$rightId = 0;
				throw new AccessRestrictionException('$rightId is undefined');
			}
			try {
				$rule = Rule::DENY;

				/* @var \PDOStatement $stmt */
				$stmt = $dao->check($userId, $rightId, $resourceCn);

				while( $row = $stmt->fetch() ) {
					if ( $row['rule'] == Rule::ALLOW ) {
						$rule = Rule::ALLOW;
					}
					elseif ( $row['rule'] == Rule::DENY ) {
						$rule = Rule::DENY;
					}
				}
			}
			catch( NotExistingException $e ) {
				throw new AccessRestrictionException('check rights failed for this credentials');
			}
			if ( $rule != Rule::ALLOW ) {
				throw new AccessRestrictionException('check rights failed for this credentials');
			}
		}
		catch( AccessRestrictionException $e ) {
			if ( $controller instanceof \Zend\Mvc\Controller\AbstractActionController ) {
				$request = $controller->getRequest();
				if ( !$request instanceof \Zend\Console\Request ) {
					$response = $controller->redirect()->toRoute('notauthorized', [
						'rightname' => $rightName,
						'resourcecn' => base64_encode($resourceCn)
					]);
					$response->setStatusCode(307);
					return $response;
				}
			}
			throw $e;
		}
		return true;
	}

	/**
	 *
	 * @param string $rightName
	 * @param string $referUid
	 * @return boolean
	 * @throws AccessRestrictionException
	 */
	public function checkRightFromUid($rightName, $referUid)
	{
		try {
			$resourceCn = DaoFactory::get()->getDao(Resource::$classId)->getResourceCnFromReferUid($referUid);
		}
		catch( \Exception $e ) {
			$resource = new Resource($this->baseCn);
			$resourceCn = $resource->getCn();
		}
		return $this->checkRightFromCn($rightName, $resourceCn);
	}

	/**
	 * @param string $rightName
	 * @param string $referCid
	 * @param string $referId
	 * @return boolean
	 * @throws AccessRestrictionException
	 */
	public function checkRightFromIdAndCid($rightName, $referId, $referCid)
	{
		try {
			$resourceCn = DaoFactory::get()->getDao(Resource::$classId)->getResourceCnFromReferIdAndCid($referId, $referCid);
		}
		catch( \Exception $e ) {
			$resource = new Resource($this->baseCn);
			$resourceCn = $resource->getCn();
		}
		return $this->checkRightFromCn($rightName, $resourceCn);
	}
} /* End of class */
