<?php
namespace Acl\Model;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;
use Rbplm\Dao\NotExistingException;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `acl_rule` (
 `roleId` int(11) NOT NULL,
 `resource` VARCHAR(128) NOT NULL,
 `resourceId` INT(11) DEFAULT NULL,
 `rightId` int(11) NOT NULL,
 `rule` VARCHAR(16) DEFAULT 'allow',
 PRIMARY KEY (`roleId`,`rightId`, `resource`),
 KEY role_idx(`roleId`),
 KEY right_idx(`rightId`),
 KEY resource_idx(`resource`)
 ) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 ALTER TABLE `acl_rule` 
 ADD CONSTRAINT `FK_acl_rule_resource`
 FOREIGN KEY (`resource`)
 REFERENCES `acl_resource` (`cn`)
 ON DELETE CASCADE
 ON UPDATE CASCADE,

 ADD CONSTRAINT `FK_acl_rule_rightid`
 FOREIGN KEY (`rightId`)
 REFERENCES `acl_right` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE,

 ADD CONSTRAINT `FK_acl_rule_roleid`
 FOREIGN KEY (`roleId`)
 REFERENCES `acl_role` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE;
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 */
class RuleDao extends DaoSier
{

	/** @var string */
	public static $table = 'acl_rule';

	/** @var string */
	public static $vtable = 'acl_rule';

	/** @var string */
	public static $sequenceName = '';

	/** @var string */
	public static $sequenceKey = '';

	/**
	 * @var \Rbs\Dao\Ltree
	 */
	public $ltree;

	/**@var \PDOStatement */
	protected $checkStmt;

	/**@var \PDOStatement */
	protected $insertStmt;

	/**@var \PDOStatement */
	protected $updateStmt;

	/**@var \PDOStatement */
	protected $getFromRoleStmt;

	/**@var \PDOStatement */
	protected $checkWithChildrenStmt;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'resource' => 'resourceCn',
		'roleId' => 'roleId',
		'rightId' => 'rightId',
		'rule' => 'rule'
	);

	/**
	 * Constructor
	 *
	 * @param
	 *        	\PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
	}

	/**
	 *
	 * @return \Rbs\Dao\Ltree
	 */
	public function getLtree()
	{
		if ( !isset($this->ltree) ) {
			$this->ltree = new \Rbs\Dao\Ltree($this);
			$this->ltree->metaModel = array(
				'resource' => 'canonicalName'
			);
		}
		return $this->ltree;
	}

	/**
	 *
	 * @param MappedInterface $mapped        	
	 * @param array $options        	
	 */
	public function save(MappedInterface $mapped)
	{
		$this->initStatement($this->metaModel);

		($this->connexion->inTransaction() == true) ? $withTrans = false : $withTrans = true;
		if ( $withTrans ) $this->connexion->beginTransaction();

		$bind = [
			':roleId' => $mapped->getRoleId(),
			':resourceCn' => $mapped->getResourceCn()
		];

		try {
			foreach( $mapped->getRules() as $rule ) {
				$bind[':rightId'] = $rule['rightId'];
				$bind[':rule'] = $rule['rule'];
				$this->lastBind = $bind;
				try {
					$this->insertStmt->execute($bind);
					$this->lastStmt = $this->insertStmt;
				}
				catch( \PDOException $e ) {
					if ( strstr($e->getMessage(), ' 1062 Duplicate entry ') ) {
						$this->updateStmt->execute($bind);
						$this->lastStmt = $this->updateStmt;
					}
					else {
						throw $e;
					}
					continue;
				}
			}
			if ( $withTrans ) $this->connexion->commit();
			$mapped->isLoaded(true);
			$mapped->isSaved(true);
		}
		catch( \Throwable $e ) {
			if ( $withTrans ) $this->connexion->rollBack();
			throw $e;
		}
		return $mapped;
	}

	/**
	 *
	 * @return \PDOStatement
	 */
	protected function initStatement($sysToApp)
	{
		/* Init statement */
		$table = $this->_table;

		foreach( $sysToApp as $asSys => $asApp ) {
			$sysSet[] = $asSys;
			$appSet[] = ':' . $asApp;
			$set[] = $asSys . '=:' . $asApp;
		}

		$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
		$this->insertStmt = $this->connexion->prepare($sql);

		$sql = "UPDATE $table SET " . implode(',', $set) . ' WHERE resource=:resourceCn AND rightId=:rightId AND roleId=:roleId;';
		$this->updateStmt = $this->connexion->prepare($sql);
		return $this->updateStmt;
	}

	/**
	 *
	 * @return \PDOStatement
	 */
	public function getFromRoleAndResource($roleId, $resourceCn)
	{
		$ltree = $this->getLtree();

		$bind = array(
			':roleId' => $roleId,
			':resourceCn' => $resourceCn
		);

		$filter = 'roleId=:roleId AND resource=:resourceCn ORDER BY resource ASC';
		#$select = 'resource, rightId, CASE ISNULL(roleId) WHEN 1 THEN "inherit" WHEN 0 THEN node.rule END AS rule';
		return $ltree->getAncestors($resourceCn, $filter, '*', $bind);

		if ( !$this->getFromRoleStmt ) {
			$ruleTable = $this->_table;

			/* get rule for resource and his ancestors */
			$sql = "SELECT resource, rightId," . " CASE ISNULL(roleId) WHEN 1 THEN \"inherit\" WHEN 0 THEN aclrule.rule END AS rule" . "FROM $ruleTable AS aclrule" . "WHERE roleId=:roleId AND resource=:resourceCn" . "ORDER BY resource ASC";
			$this->getFromRoleStmt = $this->connexion->prepare($sql);
			$this->getFromRoleStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}

		$bind = array(
			':roleId' => $roleId,
			':resourceCn' => $resourceCn
		);
		$this->getFromRoleStmt->execute($bind);
		return $this->getFromRoleStmt;
	}

	/**
	 *
	 * @param int $userId        	
	 * @param int $rightId        	
	 * @param string $resourceCn        	
	 *
	 * @return \PDOStatement
	 *
	 * @todo : if user is owner, bypass permissions
	 *      
	 */
	public function check($userId, $rightId, $resourceCn)
	{
		if ( !$this->checkStmt ) {
			$sql = 'SELECT rule.resource, rule.rule';
			$sql .= ' FROM acl_user_role AS userRole';
			$sql .= ' LEFT OUTER JOIN acl_rule AS rule ON userRole.roleId=rule.roleId AND rule.resource LIKE CONCAT(userRole.resourceCn,\'%\')';
			$sql .= ' WHERE (userRole.userId=:userId OR 1=:userId)' . ' AND rule.rightId = :rightId' . ' AND :resourceCn LIKE CONCAT(rule.resource, \'%\') ORDER BY rule.resource ASC';
			// ' OR resource.ownerId=:userId';
			$this->checkStmt = $this->connexion->prepare($sql);
			$this->checkStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}

		$bind = array(
			':userId' => $userId,
			':rightId' => $rightId,
			':resourceCn' => $resourceCn
		);

		$this->checkStmt->execute($bind);
		if ( $this->checkStmt->rowCount() == 0 ) {
			throw new NotExistingException('check rights failed for this credentials', 10001);
		}

		return $this->checkStmt;
	}

	/**
	 *
	 * @param int $userId        	
	 * @param int $rightId        	
	 * @param string $resourceCn        	
	 *
	 * @return \PDOStatement
	 */
	public function checkChildren($userId, $rightId, $resourceCn)
	{
		if ( !$this->checkWithChildrenStmt ) {
			$sql = 'SELECT rule.resource, rule.rule' . ' FROM acl_user_role AS userRole' . ' LEFT OUTER JOIN acl_rule AS rule ON userRole.roleId=rule.roleId AND rule.resource LIKE CONCAT(userRole.resourceCn,\'%\')' . ' WHERE (userRole.userId=:userId OR 1=:userId)' . ' AND rule.rightId = :rightId' . ' AND rule.rule = 1' . ' AND rule.resource LIKE CONCAT(:resourceCn, \'%\')' . ' ORDER BY rule.resource ASC';
			$this->checkWithChildrenStmt = $this->connexion->prepare($sql);
			$this->checkWithChildrenStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}

		$bind = array(
			':userId' => $userId,
			':rightId' => $rightId,
			':resourceCn' => $resourceCn
		);

		$this->checkWithChildrenStmt->execute($bind);
		if ( $this->checkWithChildrenStmt->rowCount() == 0 ) {
			throw new NotExistingException('check rights failed for this credentials', 10002);
		}

		return $this->checkWithChildrenStmt;
	}
}
