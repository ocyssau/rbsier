<?php
namespace Acl\Model;

use Zend\Permissions\Acl\Resource\ResourceInterface;

/**
 * Resource is the what the acl is apply.
 * The $cn must be defined with a begin "/" and a ending "/"
 * 
 *
 */
class Resource extends \Rbplm\Any implements \Rbplm\Dao\MappedInterface, ResourceInterface
{
	use \Rbplm\Mapped;

	public static $classId = 'aclresource60';

	/**
	 * Canonical Name
	 *
	 * @var string
	 */
	public $cn;

	/**
	 * Distinguished Name
	 *
	 * @var string
	 */
	public $dn;

	/**
	 *
	 * @var \Rbplm\Any
	 */
	protected $referTo;

	/**
	 *
	 * @var string
	 */
	public $referToCid;

	/**
	 *
	 * @var string
	 */
	public $referToId;

	/**
	 *
	 * @var string
	 */
	public $referToUid;

	/**
	 *
	 * @var integer
	 */
	public $ownerId;

	/**
	 *
	 * @var string
	 */
	public $ownerUid;

	/**
	 * @var string $cn Distinguished name in canonical format
	 * @return void
	 */
	public function __construct($dn = null)
	{
		$this->cid = static::$classId;
		$this->dn = $dn;
		$this->cn = $dn;
	}

	/**
	 * @return string
	 */
	public function getCn()
	{
		return $this->cn;
	}

	/**
	 * @return string
	 */
	public function getDn()
	{
		return $this->dn;
	}

	/**
	 * Defined by ResourceInterface; returns the Resource identifier
	 *
	 * @return string
	 */
	public function getResourceId()
	{
		return $this->cn;
	}

	/**
	 * Defined by ResourceInterface; returns the Resource identifier
	 * Proxies to getResourceId()
	 *
	 * @return string
	 */
	public function __toString()
	{
		return $this->cn;
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.MappedInterface::hydrate()
	 */
	public function hydrate(array $properties)
	{
		/* Any */
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['cn'])) ? $this->cn = $properties['cn'] : null;
		(isset($properties['dn'])) ? $this->dn = $properties['dn'] : null;
		(isset($properties['ownerId'])) ? $this->ownerId = $properties['ownerId'] : null;
		(isset($properties['ownerUid'])) ? $this->ownerUid = $properties['ownerUid'] : null;
		(isset($properties['referToId'])) ? $this->referToId = $properties['referToId'] : null;
		(isset($properties['referToUid'])) ? $this->referToUid = $properties['referToUid'] : null;
		(isset($properties['referToCid'])) ? $this->referToCid = $properties['referToCid'] : null;
		return $this;
	}

	/**
	 * @param \Rbplm\Any
	 * @return Resource
	 */
	public function setReferTo($any)
	{
		$this->referTo = $any;
		$this->referToId = $any->getId();
		$this->referToUid = $any->getUid();
		$this->referToCid = $any::$classId;
		return $this;
	}

	/**
	 * @return \Rbplm\Any
	 */
	public function getReferTo()
	{
		return $this->referTo;
	}
}
