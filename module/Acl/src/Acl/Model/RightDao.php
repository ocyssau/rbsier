<?php
namespace Acl\Model;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `acl_right` (
 `id` int(11) NOT NULL,
 `name` VARCHAR(32) NOT NULL,
 `description` VARCHAR(64) DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY (`name`)
 ) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 INSERT INTO `acl_right`
 (`id`,`name`,`description`)
 VALUES
 ('1', 'READ', 'Read'),
 ('2', 'CREATE', 'Create'),
 ('3', 'EDIT', 'Edit'),
 ('4', 'DELETE', 'Delte'),
 ('5', 'ADMIN', 'Admin');
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class RightDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'acl_right';

	public static $vtable = 'acl_right';

	public static $sequenceName = '';

	public static $sequenceKey = '';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'name' => 'name',
		'description' => 'description'
	);
}