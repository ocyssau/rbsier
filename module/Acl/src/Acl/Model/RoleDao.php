<?php
//%LICENCE_HEADER%
namespace Acl\Model;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `acl_role` (
 `id` INT(11) NOT NULL,
 `uid` VARCHAR(64) NOT NULL,
 `cid` VARCHAR(64) NOT NULL DEFAULT 'aclrole3zc547',
 `name` VARCHAR(32) DEFAULT NULL,
 `parentId` INT(11) DEFAULT NULL,
 `parentUid` VARCHAR(64) DEFAULT NULL,
 `description` TEXT DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY (`uid`),
 UNIQUE KEY (`name`),
 KEY (`parentId`),
 KEY (`parentUid`)
 ) ENGINE=InnoDB;


 CREATE TABLE IF NOT EXISTS `acl_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=100;
 INSERT INTO acl_seq (id) VALUES (100);
 <<*/

/** SQL_INSERT>>
 -- Builtin protected users id < 50
 -- Builtin protected roles id < 50
 -- Builtin others users id < 100 > 50
 -- Builtin others roles id < 100 > 50
 -- None conflicts of id withs users ids is permits

 INSERT INTO `acl_role`
 (`id`,`name`,`uid`,`description`)
 VALUES
 (2,"administrateur","administrateur","Administrateur"),
 (20,"reader","reader","Read only access"),
 (50,"concepteurs","concepteurs","Concepteurs CAO"),
 (51,"groupLeader","groupleader","chef de groupe de conception"),
 (52,"Leader","leader","Chargé d'affaire"),
 (53,"checkers","checkers","verificateurs des projets CAO");
 <<*/

/**
 *
 */
class RoleDao extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table = 'acl_role';

	public static $vtable = 'acl_role';

	public static $sequenceName = 'acl_seq';

	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'name' => 'name',
		'parentId' => 'parentId',
		'parentUid' => 'parentUid',
		'description' => 'description'
	);
} /* End of class */

