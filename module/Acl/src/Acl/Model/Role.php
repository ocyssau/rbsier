<?php
//%LICENCE_HEADER%
namespace Acl\Model;

/**
 * @brief A rule apply to a user/role pair.
 *
 */
class Role extends \Rbplm\Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Mapped;

	public static $classId = 'aclrole3zc547';

	/**
	 *
	 * @var string
	 */
	protected $description;

	/**
	 * @param string $name
	 * @return \Acl\Model\Role
	 */
	public static function init($name)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->setUid($name);
		$obj->setName($name);
		return $obj;
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.MappedInterface::hydrate()
	 */
	public function hydrate(array $properties)
	{
		/* Any */
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['parentId'])) ? $this->parentId = $properties['parentId'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid = $properties['parentUid'] : null;
		/* Role */
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
	}

	/**
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}
} /* End of class */
