<?php
namespace Acl\Model\Resource;

/**
 *
 *
 */
class Owner extends \Acl\Model\Resource
{

	public static $classId = 'resourceowner';

	/**
	 * Canonical name
	 * @var string
	 */
	public static $appCn = '/app/owner/';

	/**
	 * Distinguished name
	 * @var string
	 */
	public static $appDn = '/app/owner/';

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
		$this->uid = 'app/owner';
		$this->name = 'app/owner';
		$this->cn = '/app/owner/';
	}
}

