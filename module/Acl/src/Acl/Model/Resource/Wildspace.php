<?php
namespace Acl\Model\Resource;

/**
 * My wildspace resource
 *
 */
class Wildspace extends \Acl\Model\Resource
{

	public static $classId = 'resourceownws';

	/**
	 * Canonical name
	 * @var string
	 */
	public static $appCn = '/app/owner/ws/';

	/**
	 * Distinguished name
	 * @var string
	 */
	public static $appDn = '/app/owner/ws/';

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
		$this->uid = 'app/owner/ws';
		$this->name = 'app/owner/ws';
		$this->cn = '/app/owner/ws/';
	}
}

