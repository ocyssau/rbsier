<?php
namespace Acl\Model\Resource;

/**
 * ged manager resource
 * Edit some configuration around Ged objects
 *
 */
class GedManager extends \Acl\Model\Resource
{

	public static $classId = 'resourgedmana';

	/**
	 * Canonical name
	 * @var string
	 */
	public static $appCn = '/app/gedmanager/';

	/**
	 * Distinguished name
	 * @var string
	 */
	public static $appDn = '/app/gedmanager/';

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
		$this->uid = 'app/gedmanager';
		$this->name = 'app/gedmanager';
		$this->cn = '/app/gedmanager/';
	}
}
