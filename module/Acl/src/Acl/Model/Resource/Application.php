<?php
namespace Acl\Model\Resource;

/**
 * Resource to appliccation
 *
 */
class Application extends \Acl\Model\Resource
{

	public static $classId = 'resourceappli';

	/**
	 * Canonical name
	 * @var string
	 */
	public static $appCn = '/app/';

	/**
	 * Distinguished name
	 * @var string
	 */
	public static $appDn = '/app/';

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
		$this->uid = 'app';
		$this->name = 'app';
		$this->cn = '/app/';
	}
}

