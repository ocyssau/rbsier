<?php
namespace Acl\Model\Resource;

/**
 * ged objects resource
 *
 */
class Pdm extends \Acl\Model\Resource
{

	public static $classId = 'resourceb3pdm';

	/**
	 * Canonical name
	 * @var string
	 */
	public static $appCn = '/app/ged/pdm/';

	/**
	 * Distinguished name
	 * @var string
	 */
	public static $appDn = '/app/ged/pdm/';

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
		$this->uid = 'app/ged/pdm';
		$this->name = 'app/ged/pdm';
		$this->cn = '/app/ged/pdm/';
	}
}
