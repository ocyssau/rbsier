<?php
namespace Acl\Model\Resource;

/**
 * The workflow admin
 *
 */
class Workflow extends \Acl\Model\Resource
{

	public static $classId = 'resourceadmwf';

	/**
	 * Canonical name
	 * @var string
	 */
	public static $appCn = '/app/admin/wf/';

	/**
	 * Distinguished name
	 * @var string
	 */
	public static $appDn = '/app/admin/wf/';

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
		$this->uid = 'app/admin/wf';
		$this->name = 'app/admin/wf';
		$this->cn = '/app/admin/wf/';
	}
}
