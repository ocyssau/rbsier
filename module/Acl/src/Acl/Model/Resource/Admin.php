<?php
namespace Acl\Model\Resource;

/**
 * Resource to administration functions
 *
 */
class Admin extends \Acl\Model\Resource
{

	public static $classId = 'resourceadmin';

	/**
	 * Canonical name
	 * @var string
	 */
	public static $appCn = '/app/admin/';

	/**
	 * Distinguished name
	 * @var string
	 */
	public static $appDn = '/app/admin/';

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
		$this->uid = 'app/admin';
		$this->name = 'app/admin';
		$this->cn = '/app/admin/';
	}
}
