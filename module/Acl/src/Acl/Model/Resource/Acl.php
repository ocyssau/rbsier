<?php
namespace Acl\Model\Resource;

/**
 * Resource to acl definition
 *
 */
class Acl extends \Acl\Model\Resource
{

	public static $classId = 'resourceadaclf';

	/**
	 * Canonical name
	 * @var string
	 */
	public static $appCn = '/app/admin/acl/';

	/**
	 * Distinguished name
	 * @var string
	 */
	public static $appDn = '/app/admin/acl/';

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
		$this->uid = 'app/admin/acl';
		$this->name = 'app/admin/acl';
		$this->cn = '/app/admin/acl/';
	}
}

