<?php
namespace Acl\Model\Resource;

/**
 * ged objects resource
 *
 */
class Ged extends \Acl\Model\Resource
{

	public static $classId = 'resourcea5ged';

	/**
	 * Canonical name
	 * @var string
	 */
	public static $appCn = '/app/ged/';

	/**
	 * Distinguished name
	 * @var string
	 */
	public static $appDn = '/app/ged/';

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
		$this->uid = 'app/ged';
		$this->name = 'app/ged';
		$this->cn = '/app/ged/';
	}
}
