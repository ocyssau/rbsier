<?php
namespace Acl\Model\Resource;

/**
 * Project resource
 *
 */
class Project extends \Acl\Model\Resource
{

	public static $classId = 'resourproject';

	/**
	 * Canonical name
	 * @var string
	 */
	public static $appCn = '/app/ged/project/';

	/**
	 * Distinguished name
	 * @var string
	 */
	public static $appDn = '/app/ged/project/';

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->cid = static::$classId;
		$this->uid = 'app/ged/project';
		$this->name = 'app/ged/project';
		$this->cn = '/app/ged/project/';
	}

	/**
	 * @return void
	 */
	public function getFromId($id)
	{
		return self::$appCn . '/' . $id;
	}
}

