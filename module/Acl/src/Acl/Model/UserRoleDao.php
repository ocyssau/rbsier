<?php
//%LICENCE_HEADER%
namespace Acl\Model;

use Rbs\Dao\Sier\Filter;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `acl_user_role` (
 `uid` VARCHAR(64) NOT NULL,
 `cid` char(13) NOT NULL DEFAULT 'acluserrole59',
 `userId` int(11) NOT NULL,
 `roleId` int(11) NOT NULL,
 `resourceCn` VARCHAR(255) NOT NULL,
 PRIMARY KEY `id_i_idx` (`userId`,`roleId`,`resourceCn`),
 KEY (`userId`),
 KEY (`roleId`),
 KEY (`resourceCn`)
 ) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>

 ALTER TABLE `acl_user_role` 
 ADD CONSTRAINT `FK_acl_user_role_acl_user1` FOREIGN KEY (`userId`) REFERENCES `acl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_acl_user_role_acl_role1` FOREIGN KEY (`roleId`) REFERENCES `acl_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_acl_user_role_acl_resource1` FOREIGN KEY (`resourceCn`) REFERENCES `acl_resource` (`cn`) ON DELETE CASCADE ON UPDATE CASCADE;

 <<*/

/** SQL_TRIGGER>>
 <<*/
/** TESTS>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class UserRoleDao extends \Rbs\Dao\Sier
{

	/**
	 * @var string
	 */
	public static $vtable = 'acl_user_role';

	public static $table = 'acl_user_role';

	public static $parentTable = 'acl_user';

	public static $parentForeignKey = 'id';

	public static $childTable = 'acl_groups';

	public static $childForeignKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'userId' => 'userId',
		'roleId' => 'roleId',
		'resourceCn' => 'resourceCn'
	);

	/**
	 *
	 * @param integer $userId
	 * @param integer $roleId
	 * @param string $resourceCn Resource canonical name
	 */
	public function assign($userId, $roleId, $resourceCn)
	{
		$table = $this->_table;
		$sql = "INSERT INTO $table (uid, userId, roleId, resourceCn) VALUES (:uid,:userId,:roleId,:resourceCn)";
		$stmt = $this->connexion->prepare($sql);

		$this->connexion->beginTransaction();
		$bind = array(
			':uid' => uniqid($resourceCn),
			':userId' => $userId,
			':roleId' => $roleId,
			':resourceCn' => $resourceCn
		);

		$stmt->execute($bind);
		$this->connexion->commit();
		return $this;
	}

	/**
	 * @param integer $userId
	 * @param integer $roleId
	 */
	public function unassign($userId, $roleId, $resourceCn = null)
	{
		$table = $this->_table;
		$sql = "DELETE FROM $table WHERE userId=:userId AND roleId=:roleId AND resourceCn=:resourceCn";
		$stmt = $this->connexion->prepare($sql);
		$bind = array(
			':userId' => $userId,
			':roleId' => $roleId,
			':resourceCn' => $resourceCn
		);
		$this->connexion->beginTransaction();
		$stmt->execute($bind);
		$this->connexion->commit();
		return $this;
	}

	/**
	 * @param integer $roleId
	 * @param Filter $filter
	 * @return \PDOStatement
	 */
	public function getUsersOfRole($roleId, Filter $filter = null)
	{
		$ltable = $this->_table;
		$rtable = 'acl_user';
		if ( $filter ) {
			$strFilter = $filter->__toString();
			$select = $filter->selectToString();
		}
		else {
			$strFilter = '1=1';
			$select = 'obj.*';
		}
		$strFilter = 'lnk.roleId=:roleId AND ' . $strFilter;
		$sql = "SELECT $select FROM $ltable as lnk JOIN $rtable as obj ON lnk.userId=obj.id WHERE $strFilter";

		$stmt = $this->connexion->prepare($sql);
		$bind = array(
			':roleId' => $roleId
		);
		$stmt->execute($bind);
		return $stmt;
	}

	/**
	 * @param integer $roleId
	 * @param Filter $filter
	 * @return \PDOStatement
	 */
	public function getRolesOfUser($userId, Filter $filter = null)
	{
		$ltable = $this->_table;
		$rtable = 'acl_role';
		if ( $filter ) {
			$strFilter = $filter->__toString();
			$select = $filter->selectToString();
		}
		else {
			$strFilter = '1=1';
			$select = 'obj.*';
		}
		$strFilter = 'lnk.userId = :userId AND ' . $strFilter;
		$sql = "SELECT $select FROM $ltable as lnk JOIN $rtable as obj ON lnk.roleId=obj.id WHERE $strFilter";

		$stmt = $this->connexion->prepare($sql);
		$bind = array(
			':userId' => $userId
		);
		$stmt->execute($bind);
		return $stmt;
	}
} /* End of class */
