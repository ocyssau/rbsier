<?php
//%LICENCE_HEADER%
namespace Acl\Model;

/**
 * @brief A rule apply to a user/role pair.
 */
class UserRole extends \Rbplm\Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Mapped;

	public static $classId = 'acluserrole59';

	/**
	 * @var string
	 */
	protected $uid;

	/**
	 * The User Id
	 * @var integer
	 */
	protected $userId;

	/**
	 * The Role Id
	 * @var integer
	 */
	protected $roleId;

	/**
	 * The Resource Cn
	 * @var string
	 */
	protected $resourceCn;

	/**
	 *
	 */
	public function __construct()
	{
		$this->uid = uniqid();
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.MappedInterface::hydrate()
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['userId'])) ? $this->userId = $properties['userId'] : null;
		(isset($properties['roleId'])) ? $this->roleId = $properties['roleId'] : null;
		(isset($properties['resourceCn'])) ? $this->resourceCn = $properties['resourceCn'] : null;
		(isset($properties['parentId'])) ? $this->parentId = $properties['parentId'] : null;
		(isset($properties['childId'])) ? $this->childId = $properties['childId'] : null;
	}

	/**
	 * Setter for roleId
	 *
	 * @param string $id
	 * @return void
	 */
	public function setRoleId($id)
	{
		$this->roleId = $id;
	}

	/**
	 * Getter RoleId
	 *
	 * @return string
	 */
	public function getRoleId()
	{
		return $this->roleId;
	}

	/**
	 * Setter for userId
	 *
	 * @param string $id
	 * @return void
	 */
	public function setUserId($id)
	{
		$this->userId = $id;
	}

	/**
	 * Getter for userId
	 *
	 * @return string
	 */
	public function getUserId()
	{
		return $this->userId;
	}

	/**
	 * Setter for resourceCn
	 *
	 * @param string $cn
	 * @return void
	 */
	public function setResourceCn($cn)
	{
		$this->resourceCn = $cn;
	}

	/**
	 * Getter for resource CanonicalName
	 * @return string
	 */
	public function getResourceCn()
	{
		return $this->resourceCn;
	}
} /* End of class */
