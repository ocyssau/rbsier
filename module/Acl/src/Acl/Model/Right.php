<?php
namespace Acl\Model;

/**
 *
 *
 */
class Right extends \Rbplm\Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Mapped;

	public static $classId = 'aclrightze45d';

	const READ = 1;

	const CREATE = 2;

	const EDIT = 3;

	const DELETE = 4;

	const ADMIN = 5;

	const READ_NAME = 'read';

	const CREATE_NAME = 'create';

	const EDIT_NAME = 'edit';

	const DELETE_NAME = 'delete';

	const ADMIN_NAME = 'admin';

	/**
	 * @var string
	 */
	public $id;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $description;

	/**
	 *
	 * @param string $name
	 * @param string $description
	 */
	public function __construct($name, $description)
	{
		$this->name = $name;
		$this->description = $description;
	}

	/**
	 * @param array $properties
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id = $properties['id'] : null;
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['description'])) ? $this->description = $properties['description'] : null;
	}

	/**
	 * @param array $properties
	 */
	public static function enum()
	{
		return array(
			'READ' => self::READ,
			'CREATE' => self::CREATE,
			'EDIT' => self::EDIT,
			'DELETE' => self::DELETE,
			'ADMIN' => self::ADMIN
		);
	}

	/**
	 *
	 * @param string $name
	 * @return int
	 */
	public static function getIdFromName($name)
	{
		switch ($name) {
			case 'read':
				return self::READ;
				break;
			case 'create':
				return self::CREATE;
				break;
			case 'edit':
				return self::EDIT;
				break;
			case 'delete':
				return self::DELETE;
				break;
			case 'admin':
				return self::ADMIN;
				break;
		}
	}
}
