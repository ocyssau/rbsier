<?php
namespace Acl\Model;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `acl_resource` (
 `id` int(11) NOT NULL,
 `uid` VARCHAR(64) NOT NULL,
 `cid` VARCHAR(64) DEFAULT 'aclresource60',
 `cn` VARCHAR(255) NOT NULL,
 `ownerId` int(11) DEFAULT NULL,
 `ownerUid` VARCHAR(64) DEFAULT NULL,
 `referToUid` VARCHAR(64) DEFAULT NULL,
 `referToId` int(11) DEFAULT NULL,
 `referToCid` VARCHAR(64) DEFAULT NULL,
 UNIQUE KEY (`id`),
 UNIQUE KEY (`uid`),
 UNIQUE KEY (`cn`),
 KEY (`referToUid`),
 KEY (`referToId`),
 KEY (`referToCid`),
 KEY (`ownerId`)
 ) ENGINE=InnoDB;

 CREATE TABLE IF NOT EXISTS `acl_resource_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=10;
 INSERT INTO acl_resource_seq (id) VALUES (10);
 <<*/

/** SQL_INSERT>>
 INSERT INTO `acl_resource`
 (`id`,`uid`,`cid`,`cn`)
 VALUES
 (1,
 '/app/',
 'resourceappli',
 '/app/'),
 (2,
 '/app/admin/',
 'resourceadmin',
 '/app/admin/'),
 (3,
 '/app/ged/',
 'resourcea5ged',
 '/app/ged/'),
 (4,
 '/app/owner/',
 'resourceowner',
 '/app/owner/'),
 (5,
 '/app/admin/acl/',
 'resourceadaclf',
 '/app/admin/acl/'),
 (6,
 '/app/admin/wf/',
 'resourceadmwf',
 '/app/admin/wf/'),
 (7,
 '/app/gedmanager/',
 'resourgedmana',
 '/app/gedmanager/'),
 (8,
 '/app/ged/pdm/',
 'resourceb3pdm',
 '/app/ged/pdm/'),
 (9,
 '/app/ged/project/',
 'resourproject',
 '/app/ged/project/');
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>

 -- ***************************************************
 DROP FUNCTION IF EXISTS `aclSequence`;
 DELIMITER $$
 CREATE FUNCTION `aclSequence`() RETURNS int(11)
 BEGIN
 DECLARE lastId INT(11);

 UPDATE acl_resource_seq SET id = LAST_INSERT_ID(id+1) LIMIT 1;
 SELECT LAST_INSERT_ID(id) INTO lastId FROM acl_resource_seq;

 set @ret = lastId;
 RETURN @ret;
 END$$
 DELIMITER ;

 -- ***************************************************
 -- Get resourceCn of object uid, if not exists return resourceCn of parent
 DROP FUNCTION IF EXISTS getResourceCnFromReferUid;
 DELIMITER $$
 CREATE FUNCTION `getResourceCnFromReferUid`(_uid VARCHAR(128))
 RETURNS VARCHAR(256)
 BEGIN
 DECLARE resourceCn VARCHAR(256);

 SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = _uid;
 IF(resourceCn IS NULL) THEN
 SELECT cn INTO resourceCn FROM acl_resource AS resource
 JOIN acl_resource_breakdown as breakdown ON resource.referToUid = breakdown.parent_uid
 WHERE breakdown.uid = _uid;
 END IF;

 return resourceCn;
 END$$
 DELIMITER ;

 -- ***************************************************
 -- Get resourceCn of object Id and Cid, if not exists return resourceCn of parent
 DROP FUNCTION IF EXISTS getResourceCnFromReferIdAndCid;
 DELIMITER $$
 CREATE FUNCTION `getResourceCnFromReferIdAndCid`(_id INT(11), _cid VARCHAR(128))
 RETURNS VARCHAR(256)
 BEGIN
 DECLARE resourceCn VARCHAR(256);

 SELECT cn INTO resourceCn FROM acl_resource WHERE referToId = _id AND referToCid = _cid;
 IF(resourceCn IS NULL) THEN
 SELECT cn INTO resourceCn FROM acl_resource AS resource
 JOIN acl_resource_breakdown as breakdown ON resource.referToUid = breakdown.parent_uid
 WHERE breakdown.id = _id AND breakdown.cid=_cid;
 END IF;

 return resourceCn;
 END$$
 DELIMITER ;

 -- ***************************************************
 -- build the cn path
 DROP FUNCTION IF EXISTS build_path_withuid;
 DELIMITER $$
 CREATE FUNCTION `build_path_withuid`(_uid VARCHAR(64))
 RETURNS text
 BEGIN
 DECLARE parentUid VARCHAR(64);
 DECLARE cid VARCHAR(64);
 DECLARE message VARCHAR(256);
 set @ret = '';

 while _uid is not null do
 SELECT parent_uid, cid INTO parentUid, cid 
 FROM acl_resource_breakdown 
 WHERE uid = _uid;

 IF(found_rows() != 1) THEN
 set message = CONCAT('uid not found in table acl_resource_breakdown :', _uid);
 SIGNAL SQLSTATE '45000'
 SET MESSAGE_TEXT = message;
 END IF;

 IF(parentUid IS NULL) THEN
 set @ret := CONCAT('/app/ged/project/', _uid, @ret);
 ELSE
 set @ret := CONCAT('/', _uid, @ret);
 END IF;

 set _uid := parentUid;
 end while;

 RETURN @ret;
 END$$
 DELIMITER ;

 -- ***************************************************
 DROP PROCEDURE IF EXISTS `getResourceFromReferUid`;
 DELIMITER $$
 CREATE PROCEDURE `getResourceFromReferUid`(_uid VARCHAR(64))
 BEGIN
 DECLARE resourceUid VARCHAR(64);

 SELECT uid INTO resourceUid FROM acl_resource WHERE referToUid = _uid;
 IF(resourceUid IS NULL) THEN
 SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE referToUid = (SELECT parent_uid FROM view_resource_breakdown WHERE uid = _uid);
 ELSE
 SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE uid = resourceUid;
 END IF;
 END $$
 DELIMITER ;


 -- ***************************************************
 DROP PROCEDURE IF EXISTS updateResourceBreakdown;
 DELIMITER $$
 CREATE PROCEDURE `updateResourceBreakdown`()
 BEGIN
 DROP TABLE IF EXISTS acl_resource_breakdown;
 CREATE TABLE IF NOT EXISTS acl_resource_breakdown as
 (SELECT 
 `doc`.`id` AS `id`,
 `doc`.`uid` AS `uid`,
 `doc`.`name` AS `name`,
 `doc`.`cid` AS `cid`,
 `doc`.`container_id` AS `parent_id`,
 `doc`.`container_uid` AS `parent_uid`
 FROM
 `workitem_documents` `doc`) UNION (SELECT 
 `doc`.`id` AS `id`,
 `doc`.`uid` AS `uid`,
 `doc`.`name` AS `name`,
 `doc`.`cid` AS `cid`,
 `doc`.`container_id` AS `parent_id`,
 `doc`.`container_uid` AS `parent_uid`
 FROM
 `bookshop_documents` `doc`) UNION (SELECT 
 `doc`.`id` AS `id`,
 `doc`.`uid` AS `uid`,
 `doc`.`name` AS `name`,
 `doc`.`cid` AS `cid`,
 `doc`.`container_id` AS `parent_id`,
 `doc`.`container_uid` AS `parent_uid`
 FROM
 `cadlib_documents` `doc`) UNION (SELECT 
 `cont`.`id` AS `id`,
 `cont`.`uid` AS `uid`,
 `cont`.`name` AS `name`,
 `cont`.`cid` AS `cid`,
 `cont`.`parent_id` AS `parent_id`,
 `cont`.`parent_uid` AS `parent_uid`
 FROM
 `workitems` `cont`) UNION (SELECT 
 `cont`.`id` AS `id`,
 `cont`.`uid` AS `uid`,
 `cont`.`name` AS `name`,
 `cont`.`cid` AS `cid`,
 `cont`.`parent_id` AS `parent_id`,
 `cont`.`parent_uid` AS `parent_uid`
 FROM
 `cadlibs` `cont`) UNION (SELECT 
 `cont`.`id` AS `id`,
 `cont`.`uid` AS `uid`,
 `cont`.`name` AS `name`,
 `cont`.`cid` AS `cid`,
 `cont`.`parent_id` AS `parent_id`,
 `cont`.`parent_uid` AS `parent_uid`
 FROM
 `bookshops` `cont`) UNION (SELECT 
 `cont`.`id` AS `id`,
 `cont`.`uid` AS `uid`,
 `cont`.`name` AS `name`,
 `cont`.`cid` AS `cid`,
 `cont`.`parent_id` AS `parent_id`,
 `cont`.`parent_uid` AS `parent_uid`
 FROM
 `mockups` `cont`) UNION (SELECT 
 `projects`.`id` AS `id`,
 `projects`.`uid` AS `uid`,
 `projects`.`name` AS `name`,
 `projects`.`cid` AS `cid`,
 `projects`.`parent_id` AS `parent_id`,
 `projects`.`parent_uid` AS `parent_uid`
 FROM
 `projects`);

 ALTER TABLE acl_resource_breakdown 
 ADD KEY (`id`),
 ADD KEY (`uid`),
 ADD KEY (`name`),
 ADD KEY (`cid`),
 ADD KEY (`parent_id`),
 ADD KEY (`parent_uid`);

 END$$
 DELIMITER ;

 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * SELECT cn as resourceCn FROM acl_resource AS resource
 * JOIN acl_resource_breakdown as breakdown ON  resource.referToUid = breakdown.parent_uid
 * WHERE breakdown.uid = "dca724b1";
 *
 *
 */
class ResourceDao extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table = 'acl_resource';

	public static $vtable = 'acl_resource';

	public static $sequenceName = 'acl_resource_seq';

	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'cn' => 'cn',
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'ownerId' => 'ownerId',
		'ownerUid' => 'ownerUid',
		'referToUid' => 'referToUid',
		'referToId' => 'referToId',
		'referToCid' => 'referToCid'
	);

	/**
	 *
	 * @param \Acl\Model\Resource $mapped
	 * @param string $uid
	 */
	public function loadFromCn($mapped, $cn)
	{
		$filter = 'obj.cn=:cn';
		$bind = array(
			':cn' => $cn
		);
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 *
	 * @param \Acl\Model\Resource $mapped
	 * @param string $uid
	 */
	public function loadFromReferUid($mapped, $uid)
	{
		$filter = 'obj.referToUid=:uid';
		$bind = array(
			':uid' => $uid
		);
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 *
	 * @param \Acl\Model\Resource $mapped
	 * @param string $uid
	 */
	public function loadFromReferIdAndCid($mapped, $id, $cid)
	{
		$filter = 'obj.referToId=:id AND obj.referToCid=:cid';
		$bind = array(
			':id' => $id,
			':cid' => $cid
		);
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 * Get resourceCn of object uid, if not exists return resourceCn of parent
	 *
	 * @param string $referUid
	 * @return string
	 */
	public function getResourceCnFromReferUid($referUid)
	{
		$stmt = $this->connexion->query('SELECT getResourceCnFromReferUid(\'' . $referUid . '\')');
		$resourceCn = $stmt->fetchColumn(0);
		return $resourceCn;
	}

	/**
	 * Get resourceCn of object uid, if not exists return resourceCn of parent
	 *
	 * @param integer $referId
	 * @param string $referCid
	 * @return string
	 */
	public function getResourceCnFromReferIdAndCid($referId, $referCid)
	{
		$stmt = $this->connexion->query(sprintf("SELECT getResourceCnFromReferIdAndCid('%s', '%s')", $referId, $referCid));
		$resourceCn = $stmt->fetchColumn(0);
		return $resourceCn;
	}
}
