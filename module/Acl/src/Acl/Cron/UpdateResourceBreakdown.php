<?php
//%LICENCE_HEADER%
namespace Acl\Cron;

use Rbs\Batch\CallbackInterface;
use Rbs\Space\Factory as DaoFactory;

/**
 * CallbackInterface interface
 *
 * Must be implemented by callbock for execution of cronTasks
 *
 */
class UpdateResourceBreakdown implements CallbackInterface
{

	/** @var array */
	protected $params;

	/** @var \Rbs\Batch\Job */
	protected $job;

	/**
	 *
	 */
	public function __construct($params, \Rbs\Batch\Job $job)
	{
		$this->params = $params;
		$this->job = $job;
	}

	/**
	 *
	 */
	public function run()
	{
		echo 'Update the Acl Breakdown' . "\n";
		$connexion = DaoFactory::get()->getConnexion();
		$connexion->exec('CALL updateResourceBreakdown()');
		return $this;
	}
}
