<?php
namespace Acl\Form\User;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;
use Rbplm\People;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 * @param \Rbs\Space\Factory $factory
	 * @param string $name
	 */
	public function __construct($factory = null, $name = null)
	{
		parent::__construct($factory, $name);

		$this->template = 'acl/user/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* NAME */
		$this->add(array(
			'name' => 'find_name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for name...',
				'class' => 'form-control',
				'data-where' => 'name',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Name'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_name',
			'required' => false
		));

		/* ACTIF */
		$this->add(array(
			'name' => 'find_actif',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control submitOnClick'
			),
			'options' => array(
				'label' => 'Actifs only'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_actif',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();
		$dao = $this->daoFactory->getDao(People\User::$classId);

		/* NAME */
		if ( $datas['find_name'] ) {
			$filter->andFind($datas['find_name'], $dao->toSys('login'), Op::CONTAINS);
		}

		/* ACTIF */
		if ( $datas['find_actif'] == 1 ) {
			$filter->andFind(1, $dao->toSys('active'), Op::EQUAL);
		}

		return $this;
	}
}
