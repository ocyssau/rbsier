<?php
namespace Acl\Form\User;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class PasswordForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct('passwordEdit');

		$this->template = 'acl/user/passwordform';
		$this->setAttribute('method', 'post')
			->setAttribute('autocomplete', 'off')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		$this->add(array(
			'name' => 'password1',
			'type' => 'Zend\Form\Element\Password',
			'attributes' => array(
				'placeholder' => 'Password',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => 'Password'
			)
		));

		$this->add(array(
			'name' => 'password2',
			'type' => 'Zend\Form\Element\Password',
			'attributes' => array(
				'placeholder' => 'Confirm Password',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => 'Retype Password'
			)
		));

		$this->add(array(
			'name' => 'password',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'password1' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				)
			),
			'password2' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				)
			)
		);
	}
}
