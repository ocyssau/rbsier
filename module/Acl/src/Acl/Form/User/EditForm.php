<?php
namespace Acl\Form\User;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 */
	public function __construct()
	{
		parent::__construct('userEdit');

		$this->template = 'acl/user/editform';
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		$this->add(array(
			'name' => 'firstname',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'placeholder' => 'First Name',
				'type' => 'text',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => 'First Name'
			)
		));

		$this->add(array(
			'name' => 'lastname',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'placeholder' => 'Last Name',
				'type' => 'text',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => 'Last Name'
			)
		));

		$this->add(array(
			'name' => 'login',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'placeholder' => 'Login',
				'type' => 'text',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => 'Connexion Name'
			)
		));

		$this->add(array(
			'name' => 'mail',
			'type' => 'Zend\Form\Element\Email',
			'attributes' => array(
				'placeholder' => 'Mail',
				'autocomplete' => 'off'
			),
			'options' => array(
				'label' => 'Mail'
			)
		));

		/* authFrom */
		$this->add(array(
			'name' => 'authFrom',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(),
			'options' => array(
				'label' => tra('Authenticate From'),
				'value_options' => array(
					'db' => 'Database',
					'ldap' => 'Active Directory'
				),
				'multiple' => false
			)
		));

		/* connexionFrom */
		$this->add(array(
			'name' => 'connFrom',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(),
			'options' => array(
				'label' => tra('Connexion From'),
				'value_options' => array(
					'auto' => 'AutoDetect',
					'local' => 'Local',
					'distant' => 'Distant'
				),
				'multiple' => false
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'firstname' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'lastname' => array(
				'required' => false,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'login' => array(
				'required' => true,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				),
				'validators' => array()
			),
			'mail' => array(
				'required' => true,
				'filters' => array(
					array(
						'name' => 'StripTags'
					),
					array(
						'name' => 'StringTrim'
					)
				)
			)
		);
	}
}
