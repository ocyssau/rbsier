<?php
namespace Acl\Form\User;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Acl\Model\Role;
use Acl\Model\Resource;

/**
 *
 *
 */
class AssignRoleForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 */
	public function __construct($factory)
	{
		parent::__construct('assignroleEdit');

		$this->template = 'acl/user/assignroleform';
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/**/
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/**/
		$this->add(array(
			'name' => 'resourceCn',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/**/
		$this->add(array(
			'name' => 'userId',
			'type' => 'Application\Form\Element\SelectUser',
			'attributes' => array(
				'class' => 'form-control rb-select rb-select-user'
			),
			'options' => array(
				'label' => 'User'
			)
		));

		/**/
		$this->add(array(
			'name' => 'roleId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control rb-select'
			),
			'options' => array(
				'label' => 'Role',
				'value_options' => $this->_getRoles($factory)
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'userId' => array(
				'required' => true
			),
			'roleId' => array(
				'required' => true
			),
			'resourceCn' => array(
				'required' => true
			)
		);
	}

	/**
	 *
	 * @return array
	 */
	protected function _getRoles($factory)
	{
		$list = $factory->getList(Role::$classId);
		$list->load("1=1 LIMIT 1000");

		$selectSet = array(
			null => '...'
		);
		foreach( $list as $item ) {
			$selectSet[$item['id']] = $item['name'];
		}

		return $selectSet;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getResource()
	{
		return array(
			Resource\Application::$appCn => '/Application',
			Resource\Admin::$appCn => '/Application/Admin',
			Resource\Ged::$appCn => '/Application/Ged',
			Resource\Workflow::$appCn => '/Application/Workflow',
			Resource\Project::$appCn => '/Application/Project'
		);
	}
} /* End of class */
