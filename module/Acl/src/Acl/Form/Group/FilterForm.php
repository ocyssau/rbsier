<?php
namespace Acl\Form\Group;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;
use Rbplm\People;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 *
	 */
	public function __construct($factory, $name)
	{
		parent::__construct($factory, $name);

		$this->template = 'acl/role/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* NAME */
		$this->add(array(
			'name' => 'find_name',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for name...',
				'class' => 'form-control',
				'data-where' => 'name',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Name'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_name',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();
		$dao = $this->daoFactory->getDao(People\Group::$classId);

		/* NAME */
		if ( $datas['find_name'] ) {
			$filter->andFind($datas['find_name'], $dao->toSys('login'), Op::CONTAINS);
		}

		return $this;
	}
}
