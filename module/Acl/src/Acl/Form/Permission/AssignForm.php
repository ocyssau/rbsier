<?php
namespace Acl\Form\Permission;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Acl\Model\Role;
use Acl\Model\Resource;

/**
 */
class AssignForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 */
	public function __construct($factory, $cn = null)
	{
		parent::__construct('assignPermissionForm');

		/* */
		$this->template = 'acl/permission/assign';
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* */
		$this->add(array(
			'name' => 'spacename',
			'type' => 'Zend\Form\Element\Hidden'
		));

		/* */
		$this->add(array(
			'name' => 'roleId',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control rb-select'
			),
			'options' => array(
				'label' => 'Role',
				'value_options' => $this->_getRoles($factory)
			)
		));

		/* */
		if ( $cn ) {
			$this->add(array(
				'name' => 'resourceCn',
				'type' => 'Zend\Form\Element\Hidden'
			))->setValue($cn);
		}
		/* */
		else {
			$this->add(array(
				'name' => 'resourceCn',
				'type' => 'Zend\Form\Element\Select',
				'attributes' => array(
					'class' => 'form-control rb-select'
				),
				'options' => array(
					'label' => 'Resource',
					'value_options' => $this->_getResource()
				)
			));
		}

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Ok'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'roleId' => array(
				'required' => true
			),
			'resourceCn' => array(
				'required' => true
			),
			'spacename' => array(
				'required' => false
			)
		);
	}

	/**
	 *
	 * @return array
	 */
	protected function _getRoles($factory)
	{
		$list = $factory->getList(Role::$classId);
		$list->load("1=1 LIMIT 1000");

		$selectSet = array(
			null => '...'
		);
		foreach( $list as $item ) {
			$selectSet[$item['id']] = $item['name'];
		}

		return $selectSet;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getResource()
	{
		return array(
			Resource\Application::$appCn => '/Application',
			Resource\Admin::$appCn => '/Application/Admin',
			Resource\Ged::$appCn => '/Application/Ged',
			Resource\Workflow::$appCn => '/Application/Admin/Workflow',
			Resource\Project::$appCn => '/Application/Ged/Project',
			Resource\GedManager::$appCn => '/Application/GedManager'
		);
	}
} /* End of class */
