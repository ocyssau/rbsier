<?php
namespace Acl\Form\Role;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;
use Rbplm\People;

/**
 *
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 *
	 */
	public function __construct($factory, $name)
	{
		parent::__construct($factory, $name);

		$this->template = 'acl/role/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* NAME */
		$this->add(array(
			'name' => 'find_magic',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for name...',
				'class' => 'form-control',
				'data-where' => 'name',
				'data-op' => Op::CONTAINS
			),
			'options' => array(
				'label' => 'Name'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_magic',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$this->prepare()->isValid();
		$datas = $this->getData();
		$dao = $this->daoFactory->getDao(People\Group::$classId);

		/* MAGIC */
		if ( isset($datas['find_magic']) && $datas['find_magic'] ) {
			$concat = sprintf("CONCAT_WS('-', `%s`, `%s`, '-')", $dao->toSys('name'), $dao->toSys('description'));
			$filter->andFind($datas['find_magic'], $concat, Op::OP_CONTAINS);
		}

		return $this;
	}
}
