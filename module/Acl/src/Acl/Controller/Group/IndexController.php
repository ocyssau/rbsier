<?php
namespace Acl\Controller\Group;

use Application\Controller\AbstractController;
use Rbplm\People;
use Rbs\Space\Factory as DaoFactory;
use Application\Controller\ControllerException;

/**
 */
class IndexController extends AbstractController
{

	public $pageId = 'group_index';

	public $defaultSuccessForward = 'acl/group/index';

	public $defaultFailedForward = 'acl/group/index';

	/**
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('role');
		$this->layout()->tabs = $tabs;

		/* */
		$this->resourceCn = \Acl\Model\Resource\Acl::$appCn;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Acl::$appCn;
	}

	/**
	 * HTTP GET METHOD
	 * 
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction($list = null)
	{
		$request = $this->getRequest();

		if ( !$request->isGet() ) {
			throw new ControllerException(__FUNCTION__ . ' accept only HTTP GET Method');
		}

		$resourceId = $request->getQuery('resourceid', $this->session['resourceId']);
		$areaId = $request->getQuery('areaid', $this->session['areaid']);
		#$cancel = $request->getQuery('cancel', null);
		#$validate = $request->getQuery('validate', null);

		$this->session['resourceId'] = $resourceId;
		$this->session['areaid'] = $areaId;

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$view = $this->view;
		$view->setTemplate('acl/group/index/index');
		$this->layoutSelector()->clear($this);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\Group::$classId);
		$list = $factory->getList(People\Group::$classId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);

		$filterForm = new \Acl\Form\Group\FilterForm($factory, $this->pageId);
		$filterForm->load();

		/* Select main table */
		$select = $dao->getSelectAsApp();
		$filter->select($select);

		$filterForm->bindToFilter($filter)->save();
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter);

		/* Display the template */
		$view->list = $list->toArray();
		$view->filter = $filterForm;
		$view->pageTitle = tra('Group Manager');
		$view->headers = [
			'firstname' => 'firstname',
			'lastname' => 'lastname',
			'login' => 'login',
			'mail' => 'mail',
			'lastlogin' => 'lastlogin',
			'role' => 'memberof'
		];

		$view->resourceId = $resourceId;
		$view->areaId = $areaId;

		return $view;
	}

	/**
	 *
	 * @return string|\Application\View\ViewModel
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$role = People\Group::init('New Group');
		$factory = DaoFactory::get();
		$dao = $factory->getDao($role->cid);

		$form = new \Acl\Form\Group\EditForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($role);

		/* Try to validate the form */
		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$dao->save($role);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Add user';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$roleIds = $request->getQuery('checked', array());
			$roleId = $roleIds[0];
		}
		elseif ( $request->isPost() ) {
			$roleId = $request->getPost('id', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$factory = DaoFactory::get();
		$role = new People\Group();
		$dao = $factory->getDao($role->cid);
		$dao->loadFromId($role, $roleId);

		$form = new \Acl\Form\Group\EditForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($role);

		/* Try to validate the form */
		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$dao->save($role);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Group ' . $role->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 * @return \Zend\Http\Response
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		$groupIds = $request->getQuery('checked', []);
		$groupId = $request->getQuery('groupid', null);
		isset($groupId) ? $groupIds[] = $groupId : null;

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		/* init helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\Group::$classId);

		foreach( $groupIds as $groupId ) {
			try {
				$dao->deleteFromId($groupId);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		return $this->successForward();
	}
}
