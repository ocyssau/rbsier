<?php
namespace Acl\Controller\Permission;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Acl\Model\Right;
use Acl\Model\Rule;

/**
 *
 */
class IndexController extends AbstractController
{

	public $pageId = 'acl_permission';

	public $defaultSuccessForward = 'acl/role/index';

	public $defaultFailedForward = 'acl/permission/assign';

	/**
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('admin');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Acl::$appCn;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Acl::$appCn;
	}

	/**
	 */
	public function assigntoroleAction($resourceCn = '/app/')
	{
		$request = $this->getRequest();

		if ( $request->isGet() ) {
			$ids = $request->getQuery('checked', []);
			$roleId = $request->getQuery('id', current($ids));
		}
		else {
			$roleId = null;
		}

		return $this->assignAction($resourceCn, $roleId);
	}

	/**
	 */
	public function assignAction($resourceCn = '/app/', $roleId = null)
	{
		$view = $this->view;
		/* @var \Zend\Http\Request $request */
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$resourceCn = $request->getQuery('resourceCn', $resourceCn);
			$roleId = $request->getQuery('roleId', $roleId);
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$resourceCn = $request->getPost('resourceCn', $resourceCn);
			$roleId = $request->getPost('roleId', null);
			$rights = $request->getPost('rights', array());

			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward(array(
					'roleId' => $roleId,
					'resourceCn' => $resourceCn
				));
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* */
		$factory = DaoFactory::get();
		$ruleDao = $factory->getDao(Rule::$classId);
		$rule = new Rule($resourceCn, $roleId);

		/* */
		$form = new \Acl\Form\Permission\AssignForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$form->bind($rule);

		/* */
		if ( $validate ) {
			foreach( $rights as $rightId => $ruleCode ) {
				$rule->addRule($rightId, $ruleCode);
			}

			$ruleDao->save($rule);

			return $this->successForward(array(
				'roleId' => $roleId,
				'resourceCn' => $resourceCn
			));
		}

		/* Get the permissions of the role */
		$roleRightsStmt = $ruleDao->getFromRoleAndResource($roleId, $resourceCn);
		$roleRights = $roleRightsStmt->fetchAll(\PDO::FETCH_ASSOC);

		$view->list = $roleRights;
		$view->rights = Right::enum();

		/* Display the template */
		$view->pageTitle = 'Assign Permission To Role ' . $roleId;
		$view->form = $form;
		$view->roleId = $roleId;
		$view->resourceCn = $resourceCn;
		$view->setTemplate($form->template);
		return $view;
	}
} /* End of class */
