<?php
namespace Acl\Controller\User;

use Application\Controller\AbstractController;
use Rbplm\People;
use Acl\Model\UserRole;
use Acl\Model\Resource;
use Rbs\Space\Factory as DaoFactory;
use Exception;
use ErrorException;
use Rbplm\People\User\Wildspace;

/**
 */
class IndexController extends AbstractController
{

	/* @var string */
	public $pageId = 'user_index';

	/* @var string */
	public $defaultSuccessForward = 'acl/user/index';

	/* @var string */
	public $defaultFailedForward = 'acl/user/index';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Application\Controller\AbstractController::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('user');
		$this->layout()->tabs = $tabs;

		/**/
		$this->resourceCn = \Acl\Model\Resource\Acl::$appCn;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Acl::$appCn;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction($list = null)
	{
		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$view = $this->view;
		$view->setTemplate('acl/user/index/index');
		$this->layoutSelector()->clear($this);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\User::$classId);
		$list = $factory->getList(People\User::$classId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->load(['default'=>[
			'orderby'=>'login',
			'order'=>'asc',
			'limit' => 50
			
		]]);
		$paginator->setMaxLimit($list->countAll($filter));

		$filterForm = new \Acl\Form\User\FilterForm($factory, $this->pageId);
		$filterForm->load();

		/* Select main table */
		$filter->select($dao->getSelectAsApp());

		$filterForm->bindToFilter($filter)->save();
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();
		$list->load($filter);

		/* Display the template */
		$view->list = $list;
		$view->filter = $filterForm;
		$view->pageTitle = tra('Users Manager');
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}
		else {
			$validate = false;
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$user = People\User::init();
		$factory = DaoFactory::get();
		$dao = $factory->getDao($user->cid);

		$form = new \Acl\Form\User\EditForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($user);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($user);
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Add user';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$userIds = $request->getQuery('checked', array());
			$userId = $userIds[0];
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$userId = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		$user = new People\User();
		$factory = DaoFactory::get();
		$dao = $factory->getDao($user->cid);
		$dao->loadFromId($user, $userId);

		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$form = new \Acl\Form\User\EditForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($user);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($user);
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit User ' . $user->getLogin();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 * @return \Zend\Http\Response
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$userIds = $request->getQuery('checked', array());
			$userId = $request->getQuery('id', null);
			if ( $userId ) $userIds[] = $userId;
		}

		/* Check right */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		/**/
		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\User::$classId);

		foreach( $userIds as $userId ) {
			try {
				/* protect system users */
				if ( $userId < 50 ) {
					throw new ErrorException('You cant delete built-in users. A built-in user has id < 50.');
				}
				$dao->deleteFromId($userId);
			}
			catch( \Throwable $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		return $this->successForward();
	}

	/**
	 * Process the form to edit a user
	 */
	public function setpasswordAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$userIds = $request->getQuery('checked', array());
			$userId = $userIds[0];
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$userId = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		if ( $userId == 'me' ) {
			$user = People\CurrentUser::get();
			$userId = $user->getId();
		}
		else {
			$factory = DaoFactory::get();
			$dao = $factory->getDao(People\User::$classId);
			$user = new People\User();
			$dao->loadFromId($user, $userId);

			$currentUserId = People\CurrentUser::get()->getId();
			if ( $currentUserId != $userId ) {
				$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);
			}
		}

		$form = new \Acl\Form\User\PasswordForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($user);
		$form->setData(array(
			'password1' => 'azerty',
			'password2' => 'azerty',
			'id' => $userId
		));

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$password1 = $form->get('password1')->getValue();
					$password2 = $form->get('password2')->getValue();

					if ( $password1 == $password2 ) {
						$password = md5($password1);
						$user->setPassword($password);
						$dao = $factory->getDao('aclxxpassword')->save($user);
					}
					else {
						throw new Exception('Password mismatched!');
					}
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit User ' . $user->getLogin();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 * Assign a role to user
	 */
	public function assignroleAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$userIds = $request->getQuery('checked', array());
			$userId = $userIds[0];
			$validate = false;
			$resourceCn = Resource\Application::$appCn;
		}
		if ( $request->isPost() ) {
			$userId = $request->getPost('userId', null);
			$roleId = $request->getPost('roleId', null);
			$resourceCn = $request->getPost('resource', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
			if ( !$resourceCn ) {
				$resourceCn = Resource\Application::$appCn;
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\User::$classId);
		$urDao = $factory->getDao(UserRole::$classId);
		$urList = $factory->getList(UserRole::$classId);

		/* init model */
		$role = new UserRole();

		/* Load User */
		$user = new People\User();
		$dao->loadFromId($user, $userId);

		/**/
		if ( !$user->isActive() ) {
			$this->errorStack()->error(sprintf(tra('You can not set roles on unactif user. Please, activate the user %s before.'), $user->getLogin()));
			return $this->successForward();
		}

		/* Load roles */
		/* @var \Rbs\Dao\Sier\Filter $filter */
		$filter = $factory->getFilter(UserRole::$classId);
		$filter->with(array(
			'table' => 'acl_user',
			'alias' => 'user',
			'lefton' => $urDao->toSys('userId'),
			'righton' => $urDao::$parentForeignKey
		));
		$filter->with(array(
			'table' => 'acl_role',
			'alias' => 'role',
			'lefton' => $urDao->toSys('roleId'),
			'righton' => $urDao::$childForeignKey
		));
		$filter->andfind(':userId', $urDao->toSys('userId'));
		$filter->select(array(
			'user.id as userId',
			'user.login as userName',
			'role.id as roleId',
			'role.name as roleName'
		));
		$urList->load($filter, array(
			':userId' => $userId
		));
		$view->roles = $urList;

		/* Select group props as Application sementic */
		/* Get User Groups */
		/* Get Availables Groups */
		/* Load form */
		$form = new \Acl\Form\User\AssignRoleForm($factory);
		$form->setUrlFromCurrentRoute($this);
		$role->setUserId($userId);
		$role->setResourceCn($resourceCn);
		$form->bind($role);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$urDao->assign($userId, $roleId, $resourceCn);
				}
				catch( \PDOException $e ) {
					if ( strpos($e->getMessage(), 'Integrity constraint violation') ) {
						$this->errorStack()->error('This role is existing');
					}
					else {
						$this->errorStack()->error($e->getMessage());
					}
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}

				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Assign Role To %s'), $user->getLogin());
		$view->resourceCn = $resourceCn;
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 * Unassign a role to user
	 */
	public function unassignroleAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$userId = $request->getQuery('userid', null);
			$roleId = $request->getQuery('roleid', null);
			$resourceCn = $request->getQuery('resource', Resource\Application::$appCn);
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* helpers */
		$factory = DaoFactory::get();
		$urDao = $factory->getDao(UserRole::$classId);
		$urDao->unassign($userId, $roleId, $resourceCn);

		return $this->successForward();
	}

	/**
	 *
	 * @return \Zend\Http\Response
	 */
	public function activateAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$userIds = $request->getQuery('checked', array());
			$userId = $request->getQuery('id', null);
			if ( $userId ) $userIds[] = $userId;
		}

		/* Check right */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		/**/
		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\User::$classId);

		foreach( $userIds as $userId ) {
			try {
				$user = new People\User();
				$dao->loadFromId($user, $userId);
				$user->isActive(true);
				$wildspace = new Wildspace($user);
				$wildspace->init();
				$dao->save($user);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		return $this->successForward();
	}

	/**
	 *
	 * @return \Zend\Http\Response
	 */
	public function unactivateAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$userIds = $request->getQuery('checked', array());
			$userId = $request->getQuery('id', null);
			if ( $userId ) $userIds[] = $userId;
		}

		/* Check right */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		/**/
		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\User::$classId);

		foreach( $userIds as $userId ) {
			try {
				$user = new People\User();
				$dao->loadFromId($user, $userId);
				$user->isActive(false);
				$dao->save($user);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		return $this->successForward();
	}
}
