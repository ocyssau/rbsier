<?php
namespace Acl\Controller\Role;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Acl\Model\Role;
use Acl\Model\Resource;
use Rbplm\Dao\Filter\Op;
use Application\Controller\ControllerException;
use ErrorException;

/**
 */
class IndexController extends AbstractController
{

	/** @var string */
	public $pageId = 'role_index';

	/** @var string */
	public $defaultSuccessForward = 'acl/role/index';

	/** @var string */
	public $defaultFailedForward = 'acl/role/index';

	/**
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('role');
		$this->layout()->tabs = $tabs;

		$this->resourceCn = Resource\Acl::$appCn;
	}

	/**
	 * Run controller
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = Resource\Acl::$appCn;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction($list = null)
	{
		$view = $this->view;
		$request = $this->getRequest();

		if ( !$request->isGet() ) {
			throw new ControllerException(__FUNCTION__ . ' accept only HTTP GET Method');
		}

		$resourceId = $request->getQuery('resourceId', null);
		$resourceCn = $request->getQuery('resourceCn', $this->resourceCn);

		/* Check right */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		/* */
		$view->setTemplate('acl/role/index/index');
		$this->layoutSelector()->clear($this);

		/* Helpers inits */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Role::$classId);
		$list = $factory->getList(Role::$classId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		/* */
		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll($filter));
		$paginator->orderby = 'name';

		/* */
		$filterForm = new \Acl\Form\Role\FilterForm($factory, $this->pageId);
		$filterForm->load();

		/* Select main table */
		$select = $dao->getSelectAsApp();
		$filter->select($select);

		$filterForm->bindToFilter($filter)->save();
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter);

		/* Display the template */
		$view->list = $list->toArray();
		$view->filter = $filterForm;
		$view->pageTitle = tra('Roles Manager');
		$view->headers = array(
			'firstname' => 'firstname',
			'lastname' => 'lastname',
			'login' => 'login',
			'mail' => 'mail',
			'lastlogin' => 'lastlogin',
			'role' => 'memberof'
		);

		$view->resourceCn = $resourceCn;
		$view->resourceId = $resourceId;
		return $view;
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function getusersAction()
	{
		$view = $this->view;
		$request = $this->getRequest();

		if ( !$request->isGet() ) {
			throw new ControllerException(__FUNCTION__ . ' accept only HTTP GET Method');
		}

		$roleIds = $request->getQuery('checked', []);
		$roleId = $roleIds[0];

		/* Check right */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$factory = DaoFactory::get();
		$role = new Role();
		$role->dao = $factory->getDao(Role::$classId);
		$role->dao->loadFromId($role, $roleId);

		$userDao = $factory->getDao(\Rbplm\People\User::$classId);

		/* load users list */
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		foreach( $userDao->metaModel as $asSys => $asApp ) {
			$select[] = 'obj.' . $asSys . ' AS ' . $asApp;
		}
		$select[] = 'lnk.roleId AS roleId';
		$dao = $factory->getDao(\Acl\Model\UserRole::$classId);
		$filter->select($select);
		$filter->andfind(1, 'obj.' . $userDao->toSys('active'), Op::EQUAL);
		$filter->sort('login', 'ASC');
		$list = $dao->getUsersOfRole($roleId, $filter);

		$view->list = $list;
		$view->setTemplate('acl/role/getusers');
		$view->pageTitle = sprintf(tra('Get users of role %s'), $role->getName());
		return $view;
	}

	/**
	 *
	 * @return string|\Application\View\ViewModel
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}
		else {
			$validate = false;
		}

		/* Check right */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		/* */
		$role = Role::init('New Role');
		$factory = DaoFactory::get();
		$dao = $factory->getDao($role::$classId);

		$form = new \Acl\Form\Role\EditForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($role);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$role->setUid(strtolower($role->getName()));
					$dao->save($role);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Add role';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$roleIds = $request->getQuery('checked', []);
			$roleId = $roleIds[0];
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$roleId = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check right */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$factory = DaoFactory::get();
		$role = new Role();
		$dao = $factory->getDao($role::$classId);
		$dao->loadFromId($role, $roleId);

		$form = new \Acl\Form\Role\EditForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($role);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				try {
					$dao->save($role);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Role ' . $role->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 * @return \Zend\Http\Response
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		$roleIds = $request->getQuery('checked', []);
		$roleId = (int)$request->getQuery('roleid', null);
		isset($roleId) ? $roleIds[] = $roleId : null;

		/* Check right */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		/* init helpers */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Role::$classId);

		foreach( $roleIds as $roleId ) {
			try {
				/* protect system users */
				if ( $roleId < 50 ) {
					throw new ErrorException('You cant delete built-in role. A built-in role has id < 50.');
				}
				$dao->deleteFromId($roleId);
			}
			catch( \Throwable $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		return $this->successForward();
	}
}
