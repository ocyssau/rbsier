<?php
//%LICENCE_HEADER%
namespace Rbs\Ged;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 DROP PROCEDURE IF EXISTS getFilesWithoutDocument;
 DELIMITER $$
 CREATE PROCEDURE `getFilesWithoutDocument`()
 BEGIN
 SELECT
 `df`.`id` as `id`,
 `df`.`uid` as `uid`,
 `df`.`name` as `name`,
 "workitem" as `spacename`
 FROM workitem_doc_files as df
 LEFT OUTER JOIN workitem_documents as doc ON df.document_id=doc.id
 WHERE doc.id IS NULL
 UNION
 SELECT
 `df`.`id` as `id`,
 `df`.`uid` as `uid`,
 `df`.`name` as `name`,
 "bookshop" as `spacename`
 FROM bookshop_doc_files as df
 LEFT OUTER JOIN bookshop_documents as doc ON df.document_id=doc.id
 WHERE doc.id IS NULL
 UNION
 SELECT
 `df`.`id` as `id`,
 `df`.`uid` as `uid`,
 `df`.`name` as `name`,
 "cadlib" as `spacename`
 FROM cadlib_doc_files as df
 LEFT OUTER JOIN cadlib_documents as doc ON df.document_id=doc.id
 WHERE doc.id IS NULL
 UNION
 SELECT
 `df`.`id` as `id`,
 `df`.`uid` as `uid`,
 `df`.`name` as `name`,
 "mockup" as `spacename`
 FROM mockup_doc_files as df
 LEFT OUTER JOIN mockup_documents as doc ON df.document_id=doc.id
 WHERE doc.id IS NULL;
 END$$
 DELIMITER ;

 -- ###################################################
 DROP PROCEDURE IF EXISTS getDocumentsWithoutContainer;
 DELIMITER $$
 CREATE PROCEDURE `getDocumentsWithoutContainer`()
 BEGIN
 SELECT
 `doc`.`id` as `id`,
 `doc`.`uid` as `uid`,
 `doc`.`name` as `name`,
 "workitem" as `spacename`
 FROM workitem_documents as doc
 LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id
 WHERE cont.id IS NULL
 UNION
 SELECT
 `doc`.`id` as `id`,
 `doc`.`uid` as `uid`,
 `doc`.`name` as `name`,
 "bookshop" as `spacename`
 FROM bookshop_documents as doc
 LEFT OUTER JOIN bookshops as cont ON doc.container_id=cont.id
 WHERE cont.id IS NULL
 UNION
 SELECT
 `doc`.`id` as `id`,
 `doc`.`uid` as `uid`,
 `doc`.`name` as `name`,
 "cadlib" as `spacename`
 FROM cadlib_documents as doc
 LEFT OUTER JOIN cadlibs as cont ON doc.container_id=cont.id
 WHERE cont.id IS NULL
 UNION
 SELECT
 `doc`.`id` as `id`,
 `doc`.`uid` as `uid`,
 `doc`.`name` as `name`,
 "mockup" as `spacename`
 FROM mockup_documents as doc
 LEFT OUTER JOIN mockups as cont ON doc.container_id=cont.id
 WHERE cont.id IS NULL;
 END$$
 DELIMITER ;

 -- ###################################################
 DROP PROCEDURE IF EXISTS `cancelCheckout`;
 DELIMITER $$
 CREATE PROCEDURE `cancelCheckout`(_userUid VARCHAR(64))
 BEGIN
 UPDATE `workitem_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
 UPDATE `workitem_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
 UPDATE `bookshop_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
 UPDATE `bookshop_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
 UPDATE `cadlib_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
 UPDATE `cadlib_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
 END $$
 DELIMITER ;

 -- ################################################### 
 DROP PROCEDURE IF EXISTS `deleteDocumentsWithoutContainer`;
 DELIMITER $$
 CREATE  PROCEDURE `deleteDocumentsWithoutContainer`()
 BEGIN
 CREATE TABLE tmpdoctodelete AS (
 SELECT doc.id, cont.number FROM workitem_documents as doc
 LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id 
 WHERE cont.number is null
 );
 DELETE FROM workitem_doc_files WHERE document_id IN(SELECT id FROM tmpdoctodelete);
 DELETE FROM workitem_documents WHERE id IN(SELECT id FROM tmpdoctodelete);
 DROP TABLE tmpdoctodelete;

 END $$
 DELIMITER ;

 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class ToolDao extends DaoSier
{

	/**
	 *
	 * @var \PDO
	 */
	protected $connexion;

	/**
	 *
	 * @param \PDO $connexion
	 */
	public function __construct($connexion)
	{
		$this->connexion = $connexion;
	}

	/**
	 * @return \PDOStatement
	 */
	public function getFilesWithoutDocument()
	{
		$sql = "CALL getFilesWithoutDocument()";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute();
		return $stmt;
	}

	/**
	 * @return \PDOStatement
	 */
	public function getDocumentsWithoutContainer()
	{
		$sql = "CALL getDocumentsWithoutContainer()";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute();
		return $stmt;
	}
}
