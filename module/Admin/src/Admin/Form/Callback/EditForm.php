<?php
namespace Admin\Form\Callback;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;
use Ranchbe;

/**
 *
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 * 
	 * @var string
	 */
	public $template;

	/**
	 * 
	 * @var Ranchbe
	 */
	public $ranchbe;

	/**
	 * 
	 * @var InputFilter
	 */
	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view, $factory)
	{
		/* we want to ignore the name passed */
		parent::__construct('callbackEdit');

		$this->template = 'admin/callback/editform';
		$this->ranchbe = Ranchbe::get();

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => 'Set A Convenient Name',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));

		/* Name */
		$this->add(array(
			'name' => 'isActif',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'placeholder' => 'This callback is actif',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Is Actif')
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* On Class */
		$this->add(array(
			'name' => 'referenceCid',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control rb-select',
				'multiple' => false
			),
			'options' => array(
				'label' => tra('On Reference Class'),
				'value_options' => $this->_getReference()
			)
		));

		/* Events */
		$this->add(array(
			'name' => 'events',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control rb-select selectpicker',
				'multiple' => true
			),
			'options' => array(
				'label' => 'Select Events',
				'value_options' => $this->_getEvents(),
				'use_hidden_element' => true
			)
		));

		/* Callback Class */
		$this->add(array(
			'name' => 'callbackClass',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Callback Class')
			)
		));

		/* Callback Method */
		$this->add(array(
			'name' => 'callbackMethod',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Callback Method')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => false
			),
			'name' => array(
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z0-9-_]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, numbers, space, and -,_'
							)
						)
					)
				)
			),
			'events' => array(
				'required' => false
			),
			'referenceCid' => array(
				'required' => false
			),
			'callbackClass' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z\\\\]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters, and \\'
							)
						)
					)
				)
			),
			'callbackMethod' => array(
				'required' => false,
				'validators' => array(
					array(
						'name' => 'Regex',
						'options' => array(
							'pattern' => '/^[a-z]*$/i',
							'messages' => array(
								\Zend\Validator\Regex::NOT_MATCH => 'Use only letters'
							)
						)
					)
				)
			)
		);
	}

	/**
	 *
	 * @return array
	 */
	protected function _getEvents()
	{
		return array(
			'create.pre' => 'create.pre',
			'create.post' => 'create.post',
			'save.pre' => 'save.pre',
			'save.post' => 'save.post',
			'delete.pre' => 'delete.pre',
			'delete.post' => 'delete.post',
			'edit.pre' => 'edit.pre',
			'edit.post' => 'edit.post',
			'checkout.pre' => 'checkout.pre',
			'checkout.post' => 'checkout.post',
			'checkin.pre' => 'checkin.pre',
			'checkin.post' => 'checkin.post',
			'newversion.pre' => 'newversion.pre',
			'newversion.post' => 'newversion.post'
		);
	}

	/**
	 *
	 * @return array
	 */
	protected function _getReference()
	{
		return array(
			\Rbplm\Org\Workitem::$classId => 'Org\Container',
			\Rbplm\Ged\Document\Version::$classId => 'Document\Version',
			\Rbplm\Ged\Docfile\Version::$classId => 'Docfile\Version'
		);
	}
}
