<?php
namespace Admin\Form\Maintenance;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class ModeForm extends AbstractForm implements InputFilterProviderInterface
{

	/**
	 *
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('callbackEdit');

		$this->template = 'admin/maintenance/modeform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* Message */
		$this->add(array(
			'name' => 'message',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => 'type your message to display to users, here',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Message')
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'activate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Activate'),
				'id' => 'submitbutton',
				'class' => 'btn btn-warning'
			)
		));

		/* Unactivate button */
		$this->add(array(
			'name' => 'unactivate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Unactivate'),
				'id' => 'unactivatebutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'message' => array(
				'required' => false
			)
		);
	}
}
