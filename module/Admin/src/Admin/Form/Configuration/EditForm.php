<?php
namespace Admin\Form\Configuration;

use Application\Form\AbstractForm;
use Zend\InputFilter\InputFilter;
use Zend\Hydrator\ObjectProperty as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 *
 */
class EditForm extends AbstractForm implements InputFilterProviderInterface
{

	/**/
	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct()
	{
		/* we want to ignore the name passed */
		parent::__construct('configuration-edit');

		$this->template = 'admin/configuration/editform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());
		
		/* Json editor */
		$this->add([
			'name' => 'jsonEditor',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => [
				'placeholder' => '{"name1":"value1", "name2":"value2"}',
				'class' => 'form-control'
			],
			'options' => [
				'label' => tra('Json Editor')
			]
		]);
		
		/* Source */
		$this->add([
			'name' => 'source',
			'type' => \Zend\Form\Element\Select::class,
			'attributes' => [
				'placeholder' => '',
				'class' => 'form-control source-select'
			],
			'options' => [
				'label' => tra('Config Source'),
				'multiple' => false
			]
		]);
		
		/* Submit button */
		$this->add([
			'name' => 'validate',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			]
		]);

		/* Cancel button */
		$this->add([
			'name' => 'cancel',
			'attributes' => [
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			]
		]);
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return [
			'source' => [
				'required' => true
			],
			'jsonEditor' => [
				'required' => false
			]
		];
	}
}
