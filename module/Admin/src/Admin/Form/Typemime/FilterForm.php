<?php
namespace Admin\Form\Typemime;

use Application\Form\AbstractFilterForm;
use Rbplm\Dao\Filter\Op;

/**
 * 
 *
 */
class FilterForm extends AbstractFilterForm
{

	/**
	 */
	public function __construct($factory, $nameSpace)
	{
		parent::__construct($factory, $nameSpace);

		$this->template = 'admin/typemime/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		/* Find */
		$this->add(array(
			'name' => 'find_all',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Search for ...',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Seach'
			)
		));
		$inputFilter->add(array(
			'name' => 'find_all',
			'required' => false
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter($filter)
	{
		$dao = $this->daoFactory->getDao(\Rbplm\Ged\Doctype::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* ALL */
		if ( $datas['find_all'] ) {
			$filter->andFind($datas['find_all'], sprintf('CONCAT(%s,%s,%s)', $dao->toSys('uid'), $dao->toSys('name'), $dao->toSys('extensions')), Op::OP_CONTAINS);
		}

		return $this;
	}
}
