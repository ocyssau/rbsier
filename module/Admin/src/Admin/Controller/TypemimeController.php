<?php
namespace Admin\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Typemime\Typemime;
use Exception;

/**
 */
class TypemimeController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'admin_typemime';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'admin/typemime/index';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'admin/tools';

	/**
	 */
	public function init()
	{
		parent::init();

		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('typemime');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 */
	public function initAjax()
	{
		parent::initAjax();
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Typemime::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Admin\Form\Typemime\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'typemimefilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Typemime::$classId);
		$select = [];
		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->orderby = 'name';
		$paginator->limit = 50;
		$paginator->setData($request->getPost())
			->setData($request->getQuery());
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()
			->bindToView($view)
			->bindToFilter($filter)
			->save();

		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Typemime Manager';
		return $view;
	}

	/**
	 */
	public function importAction()
	{
		$view = $this->view;

		/* Read CSV file */
		$mimeFile = 'config/mimes.csv';
		$dao = DaoFactory::get()->getDao(Typemime::$classId);
		$imported = array();

		/* Check permissions */
		//$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		if ( !$handle = fopen($mimeFile, "r") ) {
			throw new Exception('cant open mime definition file :' . $mimeFile);
		}
		else {
			while( !feof($handle) ) {
				try {
					$data = fgetcsv($handle, 1000, ";");
					$typeMime = new Typemime();
					$typeMime->hydrate(array(
						'uid' => $data[0],
						'name' => $data[1],
						'extensions' => $data[2]
					));
					$dao->save($typeMime);
					$imported[] = $typeMime->getArrayCopy();
				}
				catch( \PDOException $e ) {
					continue;
				}
			}
			fclose($handle);
		}
		$view->list = $imported;
		$view->pageTitle = 'Imported mimetypes';
		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$validate = false;
		}
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('create', $this->resourceCn);

		$typemime = new Typemime();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Typemime::$classId);

		$form = new \Admin\Form\Typemime\EditForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($typemime);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($typemime);
				return $this->successForward();
			}
		}

		$view->pageTitle = tra('Create Typemime');
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$typemimeIds = $request->getQuery('checked', array());
			$typemimeId = $typemimeIds[0];
			$validate = false;
		}
		elseif ( $request->isPost() ) {
			$typemimeId = $request->getPost('id', null);
			$validate = $request->getPost('validate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('edit', $this->resourceCn);

		$typemime = new Typemime();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Typemime::$classId);
		$dao->loadFromId($typemime, $typemimeId);

		$form = new \Admin\Form\Typemime\EditForm();
		$form->setUrlFromCurrentRoute($this);
		$form->bind($typemime);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($typemime);
				return $this->successForward();
			}
		}

		$view->pageTitle = sprintf(tra('Edit Typemime %s'), $typemime->getName());
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		if ( $request->isGet() ) {
			$typemimeIds = $request->getQuery('checked', array());
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Typemime::$classId);

		foreach( $typemimeIds as $typemimeId ) {
			try {
				$dao->deleteFromId($typemimeId);
			}
			catch( \PDOException $e ) {
				if ( strstr($e->getMessage(), 'Integrity constraint violation') ) {
					$this->errorStack()->error(sprintf(tra('Object %s is used by something, you cant delete it'), $typemimeId));
				}
				else {
					$this->errorStack()->error($e->getMessage());
				}
				continue;
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}
}
