<?php
namespace Admin\Controller;

use Application\Controller\AbstractController;

/**
 *
 * To enable configuration file to edited with this tool, you must add it to configuration key :
 * 
 * [config_editor]['editables']['files'][key]=>path
 * 
 * Where key is a unique name for the file and used to display in select form field.
 * 
 */
class ConfigurationController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'admin_configuration';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('config');
		$this->layout()->tabs = $tabs;

		$this->defaultSuccessForward = $this->url()->fromRoute('admin-configuration');
		$this->defaultFailedForward = $this->defaultSuccessForward;

		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
		$this->basecamp()->setForward($this);
	}

	/**
	 *
	 * @return \Application\View\ViewModel
	 */
	public function editAction()
	{
		$view = $this->view;
		/* @var \Zend\Http\Request $request */
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', null);
			$source = $request->getPost('source', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}
		else {
			$source = $request->getQuery('source', null);
			$validate = false;
		}

		$configFile = '';

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		$form = new \Admin\Form\Configuration\EditForm();
		$form->setAttribute('action', $this->url()
			->fromRoute('admin-configuration'));

		$config = $this->getEvent()
			->getApplication()
			->getServiceManager()
			->get('config')['config_editor'];
		$select = [
			null => '...'
		];
		
		foreach( array_keys($config['editables']['files']) as $k ) {
			$select[$k] = $k;
		}
		$form->get('source')->setValueOptions($select);

		/* Try to validate the form */
		if ( $source ) {
			$form->setData(array_merge(
				$request->getQuery()->toArray(), 
				$request->getPost()->toArray()
			));

			if ( $form->isValid() ) {

				$datas = $form->getData();
				$source = $datas['source'];

				$configFile = $config['editables']['files'][$source];

				if ( is_file($configFile) ) {
					$configAsArray = include ($configFile);
				}
				else {
					$configAsArray = [];
				}

				$json = json_encode($configAsArray);
				$form->get('jsonEditor')->setValue($json);
				
			}
			
		}

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($request->getPost());

			if ( $form->isValid() ) {

				$datas = $form->getData();

				$configAsArray = json_decode($datas['jsonEditor'], true);
				$configAsString = var_export($configAsArray, true);

				if ( is_file($configFile) ) {
					copy($configFile, $configFile . '.bak');
					file_put_contents($configFile, "<?php \n return " . $configAsString . "\n ?>");
				}

				return $this->redirect()->toRoute('admin-configuration', [], [
					'query' => [
						'source' => $source
					]
				]);
			}
		}

		$view->pageTitle = sprintf(tra('Edit Configuration %s'), $configFile);
		$view->form = $form;
		$view->setTemplate($form->template);

		return $view;
	}
}
