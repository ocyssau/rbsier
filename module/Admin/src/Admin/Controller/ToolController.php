<?php
namespace Admin\Controller;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Exception;

/**
 *
 * @author olivier
 *
 */
class ToolController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'tool_index';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'admin/tools';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'home';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('tools');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$view = $this->view;
		return $view;
	}

	/**
	 *
	 */
	public function sqlrequestAction()
	{
		//include('inc/sql_user_queries/sql_user_queries.php');
	}

	/**
	 * Get files without documents
	 */
	public function getfileswithoutdocumentAction()
	{
		$view = $this->view;

		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$connexion = DaoFactory::get()->getConnexion();
		$sql = "CALL getFilesWithoutDocument()";
		$stmt = $connexion->prepare($sql);
		$stmt->execute();

		$view->setTemplate('admin/tool/getfiles');
		$view->list = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$view->pageTitle = 'Files Without Document';
		return $view;
	}

	/**
	 * Get documents without container
	 */
	public function getdocumentswithoutcontainerAction()
	{
		$view = $this->view;

		/* Check rights */
		$this->getAcl()->checkRightFromCn('read', $this->resourceCn);

		$connexion = DaoFactory::get()->getConnexion();
		$sql = "CALL getDocumentsWithoutContainer()";
		$stmt = $connexion->prepare($sql);
		$stmt->execute();

		$view->setTemplate('admin/tool/getfiles');
		$view->list = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$view->pageTitle = 'Documents Without Container';
		return $view;
	}

	/**
	 *
	 */
	public function dbbackupAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);
		$format = $this->params()->fromRoute('format', 'sql');

		/**/
		$config = $this->getEvent()->getApplication()->getServiceManager()->get('Configuration');
		$dbParams = $config['rbp']['db']['default']['params'];
		$host = $dbParams['host'];
		$user = $dbParams['username'];
		$pass = $dbParams['password'];
		$dbs = $dbParams['dbname'];
		$date = date("Y-m-d_H-i-s");
		$dumpCommand = $config['rbp']['backup.dump.cmd'];

		/* Utilise les fonctions système : MySQLdump */
		if ( $format == 'xml' ) {
			$backupFile = $dbs . '_backup_' . $date . '.xml';
			$command = $dumpCommand . " --xml -h$host -u$user --password=$pass $dbs";
		}
		else if ( $format == 'sql' ) {
			$backupFile = $dbs . '_backup_' . $date . '.sql';
			$command = $dumpCommand . " --opt -h$host -u$user --password=$pass $dbs";
		}
		else {
			throw new Exception('format parameter must be "xml" or "sql"');
		}

		$returnVar = false;
		$backup = '';
		exec($command, $backup, $returnVar);
		$backupStr = implode("\n\r", $backup);

		if ( $returnVar != 0 ) {
			throw new Exception(sprintf('Unable to dump database %s. Check privileges of the db user.', $dbs), $returnVar);
		}

		header("Content-disposition: attachment; filename=$backupFile");
		header("Content-Type: text/plain");
		header("Content-Transfer-Encoding: $backupFile\n"); // Surtout ne pas enlever le \n
		header("Content-Length: " . strlen($backupStr));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		echo $backupStr;
		die();
	}

	/**
	 * To generate doctypes table
	 */
	public function gendoctypesAction()
	{
		echo 'INSERT INTO `doctypes` (`doctype_id`, `doctype_number`, `doctype_description`, `can_be_composite`, `script_post_store`, `script_pre_store`, `script_post_update`, `script_pre_update`, `recognition_regexp`, `file_extension`, `file_type`, `icon`) VALUES ' . "<br>";

		$file = "./Docs/doctypes.csv";
		$handle = fopen($file, "r");
		$linecount = count(file($file));

		$row = 1;

		while( ($data = fgetcsv($handle, 1000, ";")) !== FALSE ) {
			$doctype_number = $data[0];
			$doctype_description = $data[1];
			$file_extension = $data[2];
			$file_type = $data[3];
			$recognition_regexp = $data[4];
			$icon = $data[5];
			$file_extension_list[] = $file_extension;
			echo "($row, '$doctype_number', '$doctype_description','0','','','','','$recognition_regexp', '$file_extension', '$file_type', '$icon')";
			if ( $linecount !== $row ) echo ",<br>";
			else echo ";<br>";
			$row++;
		}
		fclose($handle);

		$rows = $row - 1;
		print "
		DROP TABLE `doctypes_seq`; <br>
		CREATE TABLE IF NOT EXISTS `doctypes_seq` (
		`sequence` int(11) NOT NULL auto_increment,
		PRIMARY KEY  (`sequence`)
		) ENGINE=MyIsam AUTO_INCREMENT=$row ;
		<br>
		INSERT INTO `doctypes_seq` (`sequence`) VALUES
		($rows);
		";

		echo '<br />';
		foreach( $file_extension_list as $extension ) {
			echo '$valid_file_ext["' . $extension . '"] = "' . $extension . '";';
			echo '<br>';
		}

		die();
	}

	/**
	 * get list of scripts and display it in navigator window
	 */
	public function getcustomAction()
	{
		$list = glob(dirname(__FILE__) . '/custom/*.php');

		$html = '<h1>CUSTOM RANCHBE SCRIPTS</h1>';

		$html .= '<ul>';
		foreach( $list as $script ) {
			$script = basename($script);
			$html .= "<li><a href=./custom.php?module=$script>$script</a></li>";
		}
		$html .= '</ul>';
		$this->view->assign('literalContent', $html);
		$this->view->display('layouts/withoutTabs.tpl');
		die();
	}

	/**
	 * 
	 * @return \Application\View\ViewModel
	 */
	public function renamearchivedfileswithoutversionAction()
	{
		$view = $this->view;
		$validate = false;
		$request = $this->getRequest();
		if ( $request->isPost() ) {
			$validate = $request->getPost('validate', false);
			$cancel = $request->getPost('cancel', false);
			if ( $cancel ) {
				return $this->successForward();
			}
		}

		/* Check rights */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);

		/* \PDO $connexion */
		$connexion = DaoFactory::get()->getConnexion();
		$filter = "path LIKE '/ARCHIVES/%' AND name <> concat(`root_name`, `extension`)";
		$sql = "SELECT id, path, name, concat(`root_name`, `extension`) as renamed FROM workitem_doc_files WHERE $filter";
		$stmt = $connexion->prepare($sql);
		$stmt->execute();

		$sql = "UPDATE workitem_doc_files SET name= :name";
		$updateStmt = $connexion->prepare($sql);

		//DELETE FROM workitem_doc_files WHERE path like '/ARCHIVES/%' AND name <> concat(`root_name`, `extension`);

		$list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		if ( $validate ) {
			foreach( $list as $entry ) {
				$path = $entry['path'];
				$name = $entry['name'];
				$fullpath = $path . '/' . $name;
				$renamed = $entry['renamed'];
				$renamedfullpath = $path . '/' . $renamed;

				try {
					$connexion->beginTransaction();
					if ( !is_file($fullpath) ) {
						throw new \Exception(sprintf('File %s is not existing', $fullpath));
					}
					if ( is_file($renamedfullpath) ) {
						throw new \Exception(sprintf('File %s is existing', $renamedfullpath));
					}
					$updateStmt->execute([
						':name' => $renamed
					]);
					rename($fullpath, $renamedfullpath);
					sprintf('renamed %s to %s', $fullpath, $renamedfullpath) . "<br> \n";
					$connexion->commit();
				}
				catch( \Exception $e ) {
					echo $e->getMessage() . "<br> \n";
					$connexion->rollBack();
				}
			}
			die();
		}

		$view->pageTitle = 'Rename Archived Files Without Version';
		$view->cssClass = '';
		$view->list = $list;
		return $view;
	}
}

