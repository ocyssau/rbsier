<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\View\ViewModel;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;
use Application\Controller\AbstractController;

/**
 *
 * @author olivier
 *
 */
class MaintenanceController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'tool_maintenance';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'admin/tools';

	/**
	 *	public function indexAction()
	 {
	 $view = $this->view;
	 $request = $this->getRequest();

	 * @var string
	 */
	public $defaultFailedForward = 'home';

	/**
	 *
	 */
	public function init()
	{
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
		$this->ifSuccessForward = 'admin/tools';
		$this->ifFailedForward = 'home';
		$this->basecamp()->setForward($this);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractController::dispatch()
	 *
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		$this->authIdentityCheck();

		$isService = $request->getPost('isService', $request->getQuery('isService', null));

		if ( $request->isXmlHttpRequest() || isset($isService) ) {
			$this->initAjax();
			AbstractActionController::dispatch($request, $respons);
		}
		else {
			$this->init();
			AbstractActionController::dispatch($request, $respons);
			/* Layout must be set after dispatch */
			$this->layoutSelector()
				->setLayout($this)
				->save();
		}
	}

	/**
	 * 
	 */
	public function messageAction()
	{
		$view = new ViewModel(\Ranchbe::get());
		$file = 'data/cache/maintenance.mode';
		if ( is_file($file) ) {
			$view->message = file_get_contents($file);
			return $view;
		}
		else {
			return $this->redirect()->toRoute('home');
		}
	}

	/**
	 *
	 */
	public function modeAction()
	{
		$view = new ViewModel(\Ranchbe::get());
		$request = $this->getRequest();

		if ( $request->isPost() ) {
			$message = $request->getPost('message', null);
			$activate = $request->getPost('activate', null);
			$unactivate = $request->getPost('unactivate', null);
			$cancel = $request->getPost('cancel', null);
			if ( $cancel ) {
				return $this->successForward();
			}
		}
		else {
			$activate = false;
			$unactivate = false;
			$cancel = false;
		}

		/* Check permissions */
		$this->getAcl()->checkRightFromCn('admin', $this->resourceCn);
		$acl = new \Acl\Service\Acl($this);
		$acl->setBaseCn($this->resourceCn);

		$file = 'data/cache/maintenance.mode';

		if ( is_file($file) ) {
			$message = file_get_contents('data/cache/maintenance.mode');
			$isActif = true;
		}
		else {
			$message = 'Ranchbe is under maintenance, retry later';
			$isActif = false;
		}

		$obj = new \stdClass();
		$obj->message = $message;

		$form = new \Admin\Form\Maintenance\ModeForm($view);
		$form->setAttribute('action', $this->url()
			->fromRoute('admin-maintenance-mode'));
		$form->bind($obj);

		/* Try to validate the form */
		if ( $activate ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$message = $obj->message;
				file_put_contents($file, $message);
				return $this->successForward();
			}
		}
		elseif ( $unactivate ) {
			if ( $isActif ) {
				unlink($file);
			}
			return $this->successForward();
		}

		$view->pageTitle = tra('Maintenance Mode');
		$view->form = $form;
		$view->setTemplate($form->template);
		$view->isActif = $isActif;
		return $view;
	}
}

