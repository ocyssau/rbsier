<?php
namespace Admin\Controller;

use Application\Controller\AbstractController;
use Rbplm\People;
use Application\View\ViewModel;

/**
 *
 *
 */
class IndexController extends AbstractController
{

	/**
	 *
	 */
	public function indexAction()
	{
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_ADMIN) ) {
			return $this->notauthorized();
		}

		return new ViewModel();
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function databaseAction()
	{
		$appAcl = People\CurrentUser::get()->appacl;

		if ( $appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			$config = $this->getServiceLocator()->get('Configuration');
			$url = $config['externalLinks']['phpmyadmin']['url'];
			header('location: ' . $url);
			// header('Authorization: Basic c2llcmdhdGU6Y3F4MzhqNldh',false);
			die();
		}
		else {
			return $this->notauthorized();
		}
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function rbgateAction()
	{
		$view = $this->view;

		// Record url for page and Active the tab
		$tabs = \Application\View\Menu\MainTabBar::get($view);
		$tabs->getTab('User')->activate('rbgate');
		$this->layout()->tabs = $tabs;

		$config = $this->getServiceLocator()->get('Configuration');
		$rbgateUrl = $config['rbp']['rbgate.server.url'];
		$t = (explode('/', $rbgateUrl));
		$url = $t[0] . '//' . $t[2] . '/application/index/configure';

		$view->rbgateUrl = $url;
		return $view;
	}
}
