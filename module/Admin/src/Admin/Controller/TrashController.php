<?php
namespace Admin\Controller;

use Application\Controller\AbstractController;
use Rbplm\Sys\Directory;
use Rbplm\Sys\Filesystem;
use Ranchbe;
use Rbplm\Sys\Fsdata;
use Exception;

/**
 */
class TrashController extends AbstractController
{

	/**
	 *
	 * @var string
	 */
	public $pageId = 'tool_trash';

	/**
	 *
	 * @var string
	 */
	public $defaultSuccessForward = 'admin/trash/index';

	/**
	 *
	 * @var string
	 */
	public $defaultFailedForward = 'admin/tools';

	/**
	 */
	public function init()
	{
		parent::init();

		$tabs = \Application\View\Menu\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('trash');
		$this->layout()->tabs = $tabs;
		$this->resourceCn = \Acl\Model\Resource\Admin::$appCn;
	}

	/**
	 * Get files in Trash
	 */
	public function indexAction()
	{
		$view = $this->view;
		$dir = new Directory();
		$path = Ranchbe::get()->getConfig('path.reposit.trash');
		$iterator = $dir->getIterator($path);
		$list = array();

		if ( $iterator ) {
			/* @var \SplFileInfo $file */
			foreach( $iterator as $file ) {
				$fileName = $file->getFilename();

				if ( $fileName == '.' || $fileName == '..' ) {
					continue;
				}

				/* Get the original directory */
				$oriPath = Filesystem::decodePath($fileName);
				$oriPath = str_replace('%&_', '/', $oriPath);
				$oriPath = preg_replace('/\([0-9]{1,2}\)./i', '.', $oriPath);
				$list[] = array(
					'originalFile' => $oriPath,
					'name' => $fileName,
					'path' => $file->getPath(),
					'size' => $file->getSize(),
					'mtime' => $file->getMTime()
				);
			}
		}

		$view->pageTitle = 'Trash Files';
		$view->list = $list;
		return $view;
	}

	/**
	 * To empty the trash
	 */
	public function vacummAction()
	{
		/* Check rights */
		$this->getAcl()->checkRightFromCn('delete', $this->resourceCn);

		/**/
		$dir = new Directory();
		$path = Ranchbe::get()->getConfig('path.reposit.trash');
		$iterator = $dir->getIterator($path);

		if ( $iterator ) {
			/* @var \SplFileInfo $file */
			foreach( $iterator as $file ) {
				$fileName = $file->getFilename();

				if ( $fileName == '.' || $fileName == '..' ) {
					continue;
				}
				if ( $file->isFile() == false ) {
					continue;
				}

				try {
					@unlink($file->getRealPath());
				}
				catch( Exception $e ) {
					throw $e;
				}
			}
		}
		return $this->redirectTo('admin/trash/index');
	}

	/**
	 * download a trashed file
	 */
	public function downloadAction()
	{
		$request = $this->getRequest();
		$path = Ranchbe::get()->getConfig('path.reposit.trash');
		if ( $request->isGet() ) {
			$checked = $request->getQuery('checked', []);
			$filename = $request->getQuery('file', null);
		}
		if ( $filename ) {
			$checked[] = $filename;
		}

		if ( count($checked) == 1 ) {
			$filename = $checked[0];
			try {
				$file = $path . '/' . $filename;
				$fsdata = new Fsdata($file);
				$fsdata->download();
			}
			catch( Exception $e ) {
				throw $e;
			}
		}
		else if ( count($checked) > 1 ) {
			try {
				$zipfile = tempnam(sys_get_temp_dir(), 'rb') . '.zip';
				$zip = new \ZipArchive();
				$zip->open($zipfile, \ZipArchive::CREATE);
				foreach( $checked as $filename ) {
					$file = $path . '/' . $filename;
					$zip->addFile($file, $filename);
				}
				$zip->close();

				$fsdata = new Fsdata($zipfile);
				$fsdata->download();
				unlink($zipfile);
			}
			catch( Exception $e ) {
				throw $e;
			}
		}
		else {
			throw new Exception("None file selected");
		}
	}
}
