<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'admin-database' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/database',
					'defaults' => array(
						'controller' => 'Admin\Controller\Index',
						'action' => 'database',
					),
				),
			),
			'admin-tools' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/tools',
					'defaults' => array(
						'controller' => 'Admin\Controller\Tool',
						'action' => 'index',
					),
				),
			),
			'admin-tools-getfileswithoutdocument' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/tools/getfileswithoutdocument',
					'defaults' => array(
						'controller' => 'Admin\Controller\Tool',
						'action' => 'getfileswithoutdocument',
					),
				),
			),
			'admin-tools-getdocumentswithoutcontainer' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/tools/getdocumentswithoutcontainer',
					'defaults' => array(
						'controller' => 'Admin\Controller\Tool',
						'action' => 'getdocumentswithoutcontainer',
					),
				),
			),
			'admin-tools-renamearchivedfileswithoutversion' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/tools/renamearchived',
					'defaults' => array(
						'controller' => 'Admin\Controller\Tool',
						'action' => 'renamearchivedfileswithoutversion',
					),
				),
			),
			'admin-db-backup' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/admin/dbbackup/:format',
					'defaults' => array(
						'controller' => 'Admin\Controller\Tool',
						'action' => 'dbbackup',
					),
				),
			),
			'admin-trash' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/admin/trash[/][:action]',
					'defaults' => array(
						'controller' => 'Admin\Controller\Trash',
						'action' => 'index',
					),
				),
			),
			'admin-callback' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/admin/callback[/][:action]',
					'defaults' => array(
						'controller' => 'Admin\Controller\Callback',
						'action' => 'index',
					),
				),
			),
			'admin-typemime' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/admin/typemime[/][:action]',
					'defaults' => array(
						'controller' => 'Admin\Controller\Typemime',
						'action' => 'index',
					),
				),
			),
			'admin-maintenance-mode' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/maintenance/mode',
					'defaults' => array(
						'controller' => 'Admin\Controller\Maintenance',
						'action' => 'mode',
					),
				),
			),
			'admin-maintenance-message' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/maintenance/message',
					'defaults' => array(
						'controller' => 'Admin\Controller\Maintenance',
						'action' => 'message',
					),
				),
			),
			'admin-configuration' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/configuration/edit',
					'defaults' => array(
						'controller' => 'Admin\Controller\Configuration',
						'action' => 'edit',
					),
				),
			),
			
		),
	),
	'controllers' => [
		'invokables' => [
			'Admin\Controller\Index'=>'Admin\Controller\IndexController',
			'Admin\Controller\Tool'=>'Admin\Controller\ToolController',
			'Admin\Controller\Trash'=>'Admin\Controller\TrashController',
			'Admin\Controller\Callback'=>'Admin\Controller\CallbackController',
			'Admin\Controller\Typemime'=>'Admin\Controller\TypemimeController',
			'Admin\Controller\Maintenance'=>'Admin\Controller\MaintenanceController',
			'Admin\Controller\Configuration'=>'Admin\Controller\ConfigurationController',
		],
	],
	'view_manager' => [
		'template_map' => [
			'layout/admin'   => __DIR__ . '/../view/layout/layout.phtml',
		],
		'template_path_stack' => [
			__DIR__ . '/../view',
		],
	],
	'config_editor' => [
		'editables' => [
			'files' => [
				'Local'=>'config/autoload/local.php',
				'Application'=>'config/application.config.php',
				'Custom'=>'config/autoload/custom.local.php',
			],
		],
	]
);
