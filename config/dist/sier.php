<?php
/**
 */
return array(
	'rbp' => array(
		'sier' => array(
			'filetree' => array(
				'ingenierie' => array(
					'local' => '/DATA/AFFAIRES/',
					'clientaccess' => '\\\\SERVER\AFFAIRES',
					'template' => '/DATA/AFFAIRES/template-folder.zip'
				),
				'stress' => array(
					'local' => '/DATA/STRESS',
					'clientaccess' => '\\\\SERVER\STRESS',
					'template' => '/DATA/STRESS/template-folder.zip'
				),
				'direction' => array(
					'local' => '/DATA/DIRECTION',
					'clientaccess' => '\\\\SERVER\DIRECTION',
					'template' => '/DATA/DIRECTION/template-folder.zip'
				)
			)
		),
	)
);
