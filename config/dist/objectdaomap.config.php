<?php
return array(
	'default'=>array(
		'569e94192201a'=>array('\Rbs\Org\WorkitemDao', 'workitems', '\Rbplm\Org\Workitem'),
		'45c84a5zalias'=>array('\Rbs\Space\Workitem\AliasDao', 'workitem_alias', '\Rbs\Org\Container\Alias'),
		'569e924be82e7'=>array('\Rbs\Space\Workitem\DocumentDao', 'workitem_documents', '\Rbplm\Ged\Document'),
		'569e91f77eb45'=>array('\Rbs\Space\Workitem\DocfileDao', 'workitem_doc_files', '\Rbplm\Ged\Docfile'),
		'569e92709feb6'=>array('\Rbs\Space\Workitem\Document\VersionDao', 'workitem_documents', '\Rbplm\Ged\Document\Version'),
		'569e9270histo'=>array('\Rbs\Space\Workitem\Document\HistoryDao', 'workitem_documents_history', '\Rbs\Ged\Document\History'),
		'600b50cfhisto'=>array('\Rbs\Space\Workitem\Document\History\BatchDao', 'workitem_documents_history', '\Rbs\Ged\Document\History\Batch'),
		'569e92b86d248'=>array('\Rbs\Space\Workitem\Docfile\VersionDao', 'workitem_doc_files', '\Rbplm\Ged\Docfile\Version'),
		'569e92biterat'=>array('\Rbs\Space\Workitem\Docfile\IterationDao', 'workitem_doc_files_versions', '\Rbplm\Ged\Docfile\Iteration'),
		'56a3f337b1a82'=>array('\Rbs\Space\Workitem\Document\LinkDao', 'workitem_doc_rel', '\Rbplm\Ged\Document\Link'),
		'569e9419histo'=>array('\Rbs\Space\Workitem\HistoryDao', 'workitem_history', '\Rbs\Org\Container\History'),
		'56b269f80load'=>array('\Rbs\Extended\Loader', 'workitem_metadata', '\Rbs\Extended\Loader'),
		'56b269f80prop'=>array('\Rbs\Space\Workitem\PropertyDao', 'workitem_metadata','\Rbs\Extended\Property'),
		'56cd63f2d0daf'=>array('\Rbs\Space\Workitem\Container\Link\PropertyDao', 'workitem_metadata_rel','\Rbs\Org\Container\Link\Property'),
		'56cd9c03b856f'=>array('\Rbs\Space\Workitem\Container\Link\CategoryDao', 'workitem_category_rel','\Rbs\Org\Container\Link\Category'),
		'56cd9c5032428'=>array('\Rbs\Space\Workitem\Container\Link\DoctypeDao', 'workitem_doctype_process','\Rbs\Org\Container\Link\Doctype'),
		'56clnkprocess'=>array('\Rbs\Space\Workitem\Container\Link\ProcessDao', 'workitem_process_rel','\Rbs\Org\Container\Link\Process'),
		'56acc299ab315'=>array('\Rbs\Space\Workitem\Wf\Instance\DocumentLinkDao', 'wf_document_link', '\Rbs\Wf\Instance\DocumentLink'),
	),
	'workitem'=>array(
		'569e94192201a'=>array('\Rbs\Org\WorkitemDao', 'workitems', '\Rbplm\Org\Workitem'),
		'45c84a5zalias'=>array('\Rbs\Space\Workitem\AliasDao', 'workitem_alias', '\Rbs\Org\Container\Alias'),
		'569e924be82e7'=>array('\Rbs\Space\Workitem\DocumentDao', 'workitem_documents', '\Rbplm\Ged\Document'),
		'569e91f77eb45'=>array('\Rbs\Space\Workitem\DocfileDao', 'workitem_doc_files', '\Rbplm\Ged\Docfile'),
		'569e92709feb6'=>array('\Rbs\Space\Workitem\Document\VersionDao', 'workitem_documents', '\Rbplm\Ged\Document\Version'),
		'569e9270histo'=>array('\Rbs\Space\Workitem\Document\HistoryDao', 'workitem_documents_history', '\Rbs\Ged\Document\History'),
		'600b50cfhisto'=>array('\Rbs\Space\Workitem\Document\History\BatchDao', 'workitem_batches', '\Rbs\Ged\Document\History\Batch'),
		'569e92b86d248'=>array('\Rbs\Space\Workitem\Docfile\VersionDao', 'workitem_doc_files', '\Rbplm\Ged\Docfile\Version'),
		'569e92biterat'=>array('\Rbs\Space\Workitem\Docfile\IterationDao', 'workitem_doc_files_versions', '\Rbplm\Ged\Docfile\Iteration'),
		'56a3f337b1a82'=>array('\Rbs\Space\Workitem\Document\LinkDao', 'workitem_doc_rel', '\Rbplm\Ged\Document\Link'),
		'569e9419histo'=>array('\Rbs\Space\Workitem\HistoryDao', 'workitem_history', '\Rbs\Org\Container\History'),
		'56b269f80load'=>array('\Rbs\Extended\Loader', 'workitem_metadata', '\Rbs\Extended\Loader'),
		'56b269f80prop'=>array('\Rbs\Space\Workitem\PropertyDao', 'workitem_metadata','\Rbs\Extended\Property'),
		'56cd63f2d0daf'=>array('\Rbs\Space\Workitem\Container\Link\PropertyDao', 'workitem_metadata_rel','\Rbs\Org\Container\Link\Property'),
		'56cd9c03b856f'=>array('\Rbs\Space\Workitem\Container\Link\CategoryDao', 'workitem_category_rel','\Rbs\Org\Container\Link\Category'),
		'56cd9c5032428'=>array('\Rbs\Space\Workitem\Container\Link\DoctypeDao', 'workitem_doctype_process','\Rbs\Org\Container\Link\Doctype'),
		'56clnkprocess'=>array('\Rbs\Space\Workitem\Container\Link\ProcessDao', 'workitem_process_rel','\Rbs\Org\Container\Link\Process'),
		'56acc299ab315'=>array('\Rbs\Space\Workitem\Wf\Instance\DocumentLinkDao', 'wf_document_link', '\Rbs\Wf\Instance\DocumentLink'),
	),
	'mockup'=>array(
		'569e94192201a'=>array('\Rbs\Org\MockupDao', 'mockups', '\Rbplm\Org\Workitem'),
		'45c84a5zalias'=>array('\Rbs\Space\Mockup\AliasDao', 'mockup_alias', '\Rbs\Org\Container\Alias'),
		'569e924be82e7'=>array('\Rbs\Space\Mockup\DocumentDao', 'mockup_documents', '\Rbplm\Ged\Document'),
		'569e91f77eb45'=>array('\Rbs\Space\Mockup\DocfileDao', 'mockup_doc_files', '\Rbplm\Ged\Docfile'),
		'569e92biterat'=>array('\Rbs\Space\Mockup\Docfile\IterationDao', 'mockup_doc_files_versions', '\Rbplm\Ged\Docfile\Iteration'),
		'569e92709feb6'=>array('\Rbs\Space\Mockup\Document\VersionDao', 'mockup_documents', '\Rbplm\Ged\Document\Version'),
		'569e9270histo'=>array('\Rbs\Space\Mockup\Document\HistoryDao', 'mockup_documents_history', '\Rbs\Ged\Document\History'),
		'600b50cfhisto'=>array('\Rbs\Space\Mockup\Document\History\BatchDao', 'mockup_batches', '\Rbs\Ged\Document\History\Batch'),
		'569e92b86d248'=>array('\Rbs\Space\Mockup\Docfile\VersionDao', 'mockup_doc_files', '\Rbplm\Ged\Docfile\Version'),
		'56a3f337b1a82'=>array('\Rbs\Space\Mockup\Document\LinkDao', 'mockup_doc_rel', '\Rbplm\Ged\Document\Link'),
		'569e9419histo'=>array('\Rbs\Space\Mockup\HistoryDao', 'mockup_history', '\Rbs\Org\Container\History'),
		'56b269f80load'=>array('\Rbs\Extended\Loader', 'mockup_metadata', '\Rbs\Extended\Loader'),
		'56b269f80prop'=>array('\Rbs\Space\Mockup\PropertyDao', 'mockup_metadata','\Rbs\Extended\Property'),
		'56cd63f2d0daf'=>array('\Rbs\Space\Mockup\Container\Link\PropertyDao', 'mockup_metadata_rel','\Rbs\Org\Container\Link\Property'),
		'56cd9c03b856f'=>array('\Rbs\Space\Mockup\Container\Link\CategoryDao', 'mockup_category_rel','\Rbs\Org\Container\Link\Category'),
		'56cd9c5032428'=>array('\Rbs\Space\Mockup\Container\Link\DoctypeDao', 'mockup_doctype_process','\Rbs\Org\Container\Link\Doctype'),
		'56clnkprocess'=>array('\Rbs\Space\Mockup\Container\Link\ProcessDao', 'mockup_process_rel','\Rbs\Org\Container\Link\Process'),
		'56acc299ab315'=>array('\Rbs\Space\Mockup\Wf\Instance\DocumentLinkDao', 'wf_document_link', '\Rbs\Wf\Instance\DocumentLink'),
	),
	'cadlib'=>array(
		'569e94192201a'=>array('\Rbs\Org\CadlibDao', 'cadlibs', '\Rbplm\Org\Workitem'),
		'45c84a5zalias'=>array('\Rbs\Space\Cadlib\AliasDao', 'cadlib_alias', '\Rbs\Org\Container\Alias'),
		'569e924be82e7'=>array('\Rbs\Space\Cadlib\DocumentDao', 'cadlib_documents', '\Rbplm\Ged\Document'),
		'569e91f77eb45'=>array('\Rbs\Space\Cadlib\DocfileDao', 'cadlib_doc_files', '\Rbplm\Ged\Docfile'),
		'569e92biterat'=>array('\Rbs\Space\Cadlib\Docfile\IterationDao', 'cadlib_doc_files_versions', '\Rbplm\Ged\Docfile\Iteration'),
		'569e92709feb6'=>array('\Rbs\Space\Cadlib\Document\VersionDao', 'cadlib_documents', '\Rbplm\Ged\Document\Version'),
		'569e9270histo'=>array('\Rbs\Space\Cadlib\Document\HistoryDao', 'cadlib_documents_history', '\Rbs\Ged\Document\History'),
		'600b50cfhisto'=>array('\Rbs\Space\Cadlib\Document\History\BatchDao', 'cadlib_batches', '\Rbs\Ged\Document\History\Batch'),
		'569e92b86d248'=>array('\Rbs\Space\Cadlib\Docfile\VersionDao', 'cadlib_doc_files', '\Rbplm\Ged\Docfile\Version'),
		'56a3f337b1a82'=>array('\Rbs\Space\Cadlib\Document\LinkDao', 'cadlib_doc_rel', '\Rbplm\Ged\Document\Link'),
		'569e9419histo'=>array('\Rbs\Space\Cadlib\HistoryDao', 'cadlib_history', '\Rbs\Org\Container\History'),
		'56b269f80load'=>array('\Rbs\Extended\Loader', 'cadlib_metadata', '\Rbs\Extended\Loader'),
		'56b269f80prop'=>array('\Rbs\Space\Cadlib\PropertyDao', 'cadlib_metadata','\Rbs\Extended\Property'),
		'56cd63f2d0daf'=>array('\Rbs\Space\Cadlib\Container\Link\PropertyDao', 'cadlib_metadata_rel','\Rbs\Org\Container\Link\Property'),
		'56cd9c03b856f'=>array('\Rbs\Space\Cadlib\Container\Link\CategoryDao', 'cadlib_category_rel','\Rbs\Org\Container\Link\Category'),
		'56cd9c5032428'=>array('\Rbs\Space\Cadlib\Container\Link\DoctypeDao', 'cadlib_doctype_process','\Rbs\Org\Container\Link\Doctype'),
		'56clnkprocess'=>array('\Rbs\Space\Cadlib\Container\Link\ProcessDao', 'cadlib_process_rel','\Rbs\Org\Container\Link\Process'),
		'56acc299ab315'=>array('\Rbs\Space\Cadlib\Wf\Instance\DocumentLinkDao', 'wf_document_link', '\Rbs\Wf\Instance\DocumentLink'),
	),
	'bookshop'=>array(
		'569e94192201a'=>array('\Rbs\Org\BookshopDao', 'bookshops', '\Rbplm\Org\Workitem'),
		'45c84a5zalias'=>array('\Rbs\Space\Bookshop\AliasDao', 'bookshop_alias', '\Rbs\Org\Container\Alias'),
		'569e924be82e7'=>array('\Rbs\Space\Bookshop\DocumentDao', 'bookshop_documents', '\Rbplm\Ged\Document'),
		'569e91f77eb45'=>array('\Rbs\Space\Bookshop\DocfileDao', 'bookshop_doc_files', '\Rbplm\Ged\Docfile'),
		'569e92biterat'=>array('\Rbs\Space\Bookshop\Docfile\IterationDao', 'bookshop_doc_files_versions', '\Rbplm\Ged\Docfile\Iteration'),
		'569e92709feb6'=>array('\Rbs\Space\Bookshop\Document\VersionDao', 'bookshop_documents', '\Rbplm\Ged\Document\Version'),
		'569e9270histo'=>array('\Rbs\Space\Bookshop\Document\HistoryDao', 'bookshop_documents_history', '\Rbs\Ged\Document\History'),
		'600b50cfhisto'=>array('\Rbs\Space\Bookshop\Document\History\BatchDao', 'bookshop_batches', '\Rbs\Ged\Document\History\Batch'),
		'569e92b86d248'=>array('\Rbs\Space\Bookshop\Docfile\VersionDao', 'bookshop_doc_files', '\Rbplm\Ged\Docfile\Version'),
		'56a3f337b1a82'=>array('\Rbs\Space\Bookshop\Document\LinkDao', 'bookshop_doc_rel', '\Rbplm\Ged\Document\Link'),
		'569e9419histo'=>array('\Rbs\Space\Bookshop\HistoryDao', 'bookshop_history', '\Rbs\Org\Container\History'),
		'56b269f80load'=>array('\Rbs\Extended\Loader', 'bookshop_metadata', '\Rbs\Extended\Loader'),
		'56b269f80prop'=>array('\Rbs\Space\Bookshop\PropertyDao', 'bookshop_metadata','\Rbs\Extended\Property'),
		'56cd63f2d0daf'=>array('\Rbs\Space\Bookshop\Container\Link\PropertyDao', 'bookshop_metadata_rel','\Rbs\Org\Container\Link\Property'),
		'56cd9c03b856f'=>array('\Rbs\Space\Bookshop\Container\Link\CategoryDao', 'bookshop_category_rel','\Rbs\Org\Container\Link\Category'),
		'56cd9c5032428'=>array('\Rbs\Space\Bookshop\Container\Link\DoctypeDao', 'bookshop_doctype_process','\Rbs\Org\Container\Link\Doctype'),
		'56clnkprocess'=>array('\Rbs\Space\Bookshop\Container\Link\ProcessDao', 'bookshop_process_rel','\Rbs\Org\Container\Link\Process'),
		'56acc299ab315'=>array('\Rbs\Space\Bookshop\Wf\Instance\DocumentLinkDao', 'wf_document_link', '\Rbs\Wf\Instance\DocumentLink'),
	),
	'all'=>array(
		'pdmlink4r3v49'=>array('\Rbs\Ged\Document\PdmLinkDao', 'document_pdm_rel', '\Rbplm\Ged\Document\PdmLink'),
		'70searchdd497'=>array('\Search\Engine\SearchDao', 'search','\Search\Engine\Search'),
		'64bdd156af045'=>array('\Rbs\Space\SpaceDao', 'spaces','\Rbs\Space\Space'),
		'569e921e55b03'=>array('\Rbs\Ged\DoctypeDao', 'doctypes', '\Rbplm\Ged\Doctype'),
		//TYPEMIMES
		'typemime'=>array('\Rbs\Typemime\TypemimeDao', 'typemimes', '\Rbs\Typemime\Typemime'),
		'569e93c6histo'=>array('\Rbs\Org\Project\HistoryDao', 'project_history', '\Rbs\Org\Project\History'),
		'569e94192201a'=>array('\Rbs\Org\WorkitemDao', 'workitems', '\Rbplm\Org\Workitem'),
		'569e93ae9000f'=>array('\Rbs\Org\BookshopDao', 'bookshops', '\Rbplm\Org\Bookshop'),
		'569e93ae6500f'=>array('\Rbs\Org\CadlibDao', 'cadlibs', '\Rbplm\Org\Cadlib'),
		'569e93ae7111f'=>array('\Rbs\Org\MockupDao', 'mockups', '\Rbplm\Org\Mockup'),
		'598a45cf2hist'=>array('\Rbs\History\HistoryDao', '', '\Rbs\History\HistoryAbstract'),
		//---
		'569b832a77794'=>array('\Rbs\Org\Container\FavoriteDao','container_favorite','\Rbs\Org\Container\Favorite'),
		'56c4876b1e741'=>array('\Rbs\Postit\PostitDao','postit','\Rbs\Postit\Postit'),
		'569e92f45d8ad'=>array('\Rbs\Ged\Docfile\RoleDao', 'ged_docfile_role', '\Rbplm\Ged\Docfile\Role'),
		//MESSAGE
		'569b7d17e4ccb'=>array('\Rbs\Sys\Message\MailboxDao','message_mailbox','\Rbplm\Sys\Message'),
		'569b7d17esent'=>array('\Rbs\Sys\Message\SentDao','message_sent','\Rbplm\Sys\Message\Sent'),
		'569b7archived'=>array('\Rbs\Sys\Message\ArchiveDao','message_archive','\Rbplm\Sys\Message\Archived'),
		//ORG
		'569e93dc9439c'=>array('\Rbs\Org\UnitDao', '', '\Rbplm\Org\Root'),
		'569e93ef2cbbc'=>array('\Rbs\Org\UnitDao', '', '\Rbplm\Org\Unit'),
		'569e93c6ee156'=>array('\Rbs\Org\ProjectDao', 'projects', '\Rbplm\Org\Project'),
		'569e918a134ca'=>array('\Rbs\Ged\CategoryDao', 'categories', '\Rbplm\Ged\Category'),
		//PROJECT LINKS
		'569elnkcontain'=>array('\Rbs\Org\Project\Link\ContainerDao', 'project_container_rel','\Rbs\Org\Project\Link\Container'),
		'569lnkproperty'=>array('\Rbs\Org\Project\Link\PropertyDao', 'project_metadata_rel','\Rbs\Org\Project\Link\Property'),
		'569lnkcategory'=>array('\Rbs\Org\Project\Link\CategoryDao', 'project_category_rel','\Rbs\Org\Project\Link\Category'),
		'569elnkdoctype'=>array('\Rbs\Org\Project\Link\DoctypeDao', 'project_doctype_process','\Rbs\Org\Project\Link\Doctype'),
		'569elnkprocess'=>array('\Rbs\Org\Project\Link\ProcessDao', 'project_process_rel','\Rbs\Org\Project\Link\Process'),
		//ACL AND PEOPLE
		'somebody5lwg7'=>array('\Rbs\People\SomebodyDao','partners','\Rbplm\People\Somebody'),
		'569b63eb7cce4'=>array('\Rbs\People\User\PreferenceDao','user_pref','\Rbplm\People\User\Preference'),
		'569e9459a1e71'=>array('\Rbs\People\GroupDao', 'acl_group', '\Rbplm\People\Group'),
		'569e94984ded3'=>array('\Rbs\People\UserDao', 'acl_user', '\Rbplm\People\User'),
		'aclxxpassword'=>array('\Rbs\People\PasswordDao', 'acl_user', '\Rbplm\People\User'),
		'aclrole3zc547'=>array('\Acl\Model\RoleDao','acl_role','\Acl\Model\Role'),
		'acluserrole59'=>array('\Acl\Model\UserRoleDao','acl_user_role','\Acl\Model\UserRole'),
		'aclresource60'=>array('\Acl\Model\ResourceDao','acl_resource','\Acl\Model\Resource'),
		'aclrightze45d'=>array('\Acl\Model\RightDao','acl_right','\Acl\Model\Right'),
		'89a3rhd5ad456'=>array('\Acl\Model\RuleDao','acl_rule','\Acl\Model\Rule'),
		//VAULT
		'569e951e03bac'=>array('\Rbs\Vault\RecordDao', 'vault_record', '\Rbplm\Vault\Record'),
		'569e955d1d1c4'=>array('\Rbs\Vault\RepositDao', 'vault_reposit', '\Rbplm\Vault\Reposit'),
		'569e95a3ec264'=>array('\Rbs\Vault\RepositDao', 'vault_reposit', '\Rbplm\Vault\Reposit\Read'),
		'569e95d92cc5a'=>array('\Rbs\Vault\RepositDao', 'vault_reposit', '\Rbplm\Vault\Cache\Reposit'),
		'distantsite36'=>array('\Rbs\Vault\DistantSiteDao', 'vault_distant_site', '\Rbs\Vault\DistantSite'),
		'reporeplicate'=>array('\Rbs\Vault\ReplicatedDao', 'vault_replicated', '\Rbs\Vault\Replicated'),
		//PDM
		'569e96b70f2a1'=>array('\Rbs\Pdm\ProductDao', 'pdm_product', '\Rbplm\Pdm\Product'),
		'569e972dd4c2c'=>array('\Rbs\Pdm\Product\VersionDao', 'pdm_product_version', '\Rbplm\Pdm\Product\Version'),
		'569e971bb57c0'=>array('\Rbs\Pdm\Product\InstanceDao', 'pdm_product_instance', '\Rbplm\Pdm\Product\Instance'),
		'569e96e35bab7'=>array('\Rbs\Pdm\UsageDao', 'pdm_product_instance', '\Rbplm\Pdm\Usage'),
		'569e9714da9aa'=>array('\Rbs\Pdm\Product\ContextDao', 'pdm_context_application', '\Rbplm\Pdm\Product\Context'),
		'569e967f01ca6'=>array('', 'pdm_effectivity', '\Rbplm\Pdm\Effectivity'),
		'569e970bdc932'=>array('', 'pdm_product_concept', '\Rbplm\Pdm\Product\Concept'),
		'569e9722bf42a'=>array('', 'pdm_product_material', '\Rbplm\Pdm\Product\Material'),
		'569e972851e2e'=>array('', 'pdm_product_physicalproperties', '\Rbplm\Pdm\Product\PhysicalProperties'),
		//WF
		'56acc299ecd6d'=>array('\Rbs\Wf\ProcessDao', 'wf_process', '\Workflow\Model\Wf\Process'),
		'56acc299ecdd8'=>array('\Rbs\Wf\Activity\ActivityDao', 'wf_activity', '\Workflow\Model\Wf\Activity\Activity'),
		'56acc299ece44'=>array('\Rbs\Wf\Activity\StartDao', 'wf_activity', '\Workflow\Model\Wf\Activity\Start'),
		'56acc299eceaf'=>array('\Rbs\Wf\Activity\EndDao', 'wf_activity', '\Workflow\Model\Wf\Activity\End'),
		'56acc299ecf1a'=>array('\Rbs\Wf\Activity\JoinDao', 'wf_activity', '\Workflow\Model\Wf\Activity\Join'),
		'56acc299ecf85'=>array('\Rbs\Wf\Activity\SplitDao', 'wf_activity', '\Workflow\Model\Wf\Activity\Split'),
		'56acc299ed00a'=>array('\Rbs\Wf\Activity\AswitchDao', 'wf_activity', '\Workflow\Model\Wf\Activity\Aswitch'),
		'56acc299ed078'=>array('\Rbs\Wf\Activity\StandaloneDao', 'wf_activity', '\Workflow\Model\Wf\Activity\Standalone'),
		'56acc299ed0e4'=>array('\Rbs\Wf\ActivityDao', 'wf_activity', '\Workflow\Model\Wf\Activity'),
		'56acc299ed150'=>array('\Rbs\Wf\TransitionDao', 'wf_transition', '\Workflow\Model\Wf\Transition'),
		'56acc299ed1bd'=>array('\Rbs\Wf\InstanceDao', 'wf_instance', '\Workflow\Model\Wf\Instance'),
		'56acc299ed22a'=>array('\Rbs\Wf\Instance\ActivityDao', 'wf_instance_activity', '\Workflow\Model\Wf\Instance\Activity'),
		'56acc299ed295'=>array('\Rbs\Wf\Instance\ActivityDao', 'wf_instance_activity', '\Workflow\Model\Wf\Instance\Standalone'),
		'56acc299ed301'=>array('', '', '\Workflow\Model\Link'),
		'56acc299ed36e'=>array('', '', '\Application\Model\Acl\Acl'),
		//COMMENT
		'568be4fc7a0a8'=>array('\Discussion\Dao\CommentDao', 'discussion_comment', '\Discussion\Model\Comment'),
		//NOTIFICATION
		'479v169a42a96'=>array('Rbs\Notification\NotificationDao', 'notifications', 'Rbs\Notification\Notification'),
		//IMPORTS
		'batch5994f736'=>array('Import\Model\BatchjobDao', 'import_batchjob', 'Import\Model\Batchjob'),
		'package94f736'=>array('Import\Model\PackageDao', 'import_package', 'Import\Model\Package'),
		//ARCHIVER
		'archivermedia'=>array('Ged\Archiver\MediaDao', 'archiver_media', 'Ged\Archiver\Media'),
		//CHECKOUT INDEX
		'checkoutindex'=>array('Rbs\Ged\CheckoutIndexDao', 'checkout_index', 'Rbs\Ged\CheckoutIndex'),
		//DOCSEEDER
		'docseedertype'=>array('Docseeder\Dao\TypeDao', 'docseeder_type', 'Docseeder\Model\Type'),
		'docseedermask'=>array('Docseeder\Dao\Template\MaskDao', 'docseeder_mask', 'Docseeder\Model\Template\Mask'),
		//CALLBACK
		'callback9u687'=>array('Rbs\Observers\CallbackFactoryDao', 'callbacks', 'Rbs\Observers\CallbackFactory'),
		'dtcallbackjh9'=>array('Rbs\Observers\CallbackFactoryDao', 'callbacks', 'Rbs\Observers\DoctypeCallbackFactory'),
		//BATCH JOB
		'batchjob35c98'=>array('\Rbs\Batch\JobDao', 'batchjob', '\Rbs\Batch\Job'),
		'crontask35f98'=>array('\Rbs\Batch\CronTaskDao', 'cron_task', '\Rbs\Batch\CronTab'),
		'batch86g598qd'=>array('\Rbs\Batch\BatchDao', 'batch', '\Rbs\Batch\Batch'),
		//SESSION AND CONTEXT
		'usercontext'=>array('\Rbs\People\User\ContextDao', 'user_context', '\Rbs\People\User\Context'),
		//SHARE
		'sharepublicurl'=>array('\Rbs\Ged\Document\Share\PublicUrlDao', 'share_public_url', '\Rbs\Ged\Document\Share\PublicUrl'),
		//CHANGE
		'change47g548rh'=>array('\Change\Dao\ChangeDao', 'changes', '\Change\Model\Change'),
		//PORTAIL
		'pcompabstract'=>array('\Portail\Dao\Component\ComponentDao', 'portail_components', null),
		'pcompproject'=>array('\Portail\Dao\Component\ProjectDao', 'portail_components', '\Portail\Model\Component\Project'),
		'pcomptemplate'=>array('\Portail\Dao\Component\TemplateDao', 'portail_components', '\Portail\Model\Component\Template'),
		'pcompbox'=>array('\Portail\Dao\Component\BoxDao', 'portail_components', '\Portail\Model\Component\Box'),
		'pcompindicator'=>array('\Portail\Dao\Component\IndicatorDao', 'portail_components', '\Portail\Model\Component\Indicator'),
		'pcompjalon'=>array('\Portail\Dao\Component\JalonDao', 'portail_components', '\Portail\Model\Component\Jalon'),
		'pcompstatusbox'=>array('\Portail\Dao\Component\StatusboxDao', 'portail_components', '\Portail\Model\Component\Statusbox'),
		'pcompcheckbox'=>array('\Portail\Dao\Component\CheckboxDao', 'portail_components', '\Portail\Model\Component\Checkbox'),
		'pcompcheckboxitem'=>array('\Portail\Dao\Component\CheckboxDao', 'portail_components', '\Portail\Model\Component\Checkbox\Item'),
		'pcompsimplegrid'=>array('\Portail\Dao\Component\SimplegridDao', 'portail_components', '\Portail\Model\Component\Simplegrid'),
		'pcompsimplegriditem'=>array('\Portail\Dao\Component\SimplegridDao', 'portail_components', '\Portail\Model\Component\Simplegrid\Item'),
		'pcomptinymce'=>array('\Portail\Dao\Component\TinymceDao', 'portail_components', '\Portail\Model\Component\Tinymce'),
		'pcompjssor'=>array('\Portail\Dao\Component\JssorDao', 'portail_components', '\Portail\Model\Component\Jssor'),
		'pcomptab'=>array('\Portail\Dao\Component\TabDao', 'portail_components', '\Portail\Model\Component\Tab'),
		'pcomptabpanel'=>array('\Portail\Dao\Component\TabpanelDao', 'portail_components', '\Portail\Model\Component\Tabpanel'),
		'pcomptabpanelitem'=>array('\Portail\Dao\Component\Tabpanel\ItemDao', 'portail_components', '\Portail\Model\Component\Tabpanel\Item'),
		'pcompfilelist'=>array('\Portail\Dao\Component\FilelistDao', 'portail_components', '\Portail\Model\Component\Filelist'),
		'pcompfilelistitem'=>array('\Portail\Dao\Component\FilelistDao', 'portail_components', '\Portail\Model\Component\Filelist\Item'),
		'pcompdoclist'=>array('\Portail\Dao\Component\DocumentlistDao', 'portail_components', '\Portail\Model\Component\Documentlist'),
		'pcompdoclistitem'=>array('\Portail\Dao\Component\Documentlist\ItemDao', 'portail_documentlist_items', '\Portail\Model\Component\Documentlist\Item'),
		'pcompaccordion'=>array('\Portail\Dao\Component\AccordionDao', 'portail_components', '\Portail\Model\Component\Accordion'),
		'pcompdoclist'=>array('\Portail\Dao\Component\DocumentlistDao', 'portail_components', '\Portail\Model\Component\Documentlist')
	)
);
