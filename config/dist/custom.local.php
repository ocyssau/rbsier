<?php
$year = date_format(new \DateTime(), 'Y');

return array(
	'rbp'=>array(
		'sier'=>array(
			'delivred.path'=>'/DATA6/livraison/a_livrer/ranchbe/',
			'filetree'=>array(
				'ingenierie' => array(
					'local' => '/DATA2/AFFAIRES/',
					'clientaccess' => '\\\\NIEPCE\AFFAIRES',
					'template' => '/DATA2/AFFAIRES/template/template.zip'
				),
				'stress' => array(
					'local' => '/DATA1/CALCUL',
					'clientaccess' => '\\\\NIEPCE\STRESS',
					'template' => '/DATA1/CALCUL/99-template/template.zip'
				),
				'direction' => array(
					'local' => '/DIRECTION/COURRIER_'.$year.'/CLIENTS/',
					'clientaccess' => '\\\\NIEPCE\DIRECTION',
					'template' => '/DIRECTION/COURRIER_'.$year.'/CLIENTS/TEMPLATE/template.zip',
				)
			),
			'ssh'=>[
				'server1'=>[
					'host'=>'server.domain.com',
					'port'=>22,
					'username'=>'user',
					'password'=>'password',
				],
				'server2'=>[
					'host'=>'server2.domain.com',
					'port'=>22,
					'username'=>'user',
					'password'=>'password',
				]
			]
		),
	));
