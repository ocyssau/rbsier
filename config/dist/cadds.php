<?php
//Define extension to determine cadds4 type
$FILE_TYPE_LIST['cadds4'] = 'cadds4'; //Never change this value
$FILE_EXTENSION_LIST['cadds4'] = array('_pd'=>'_pd'); //Valid extension for type cadds4

//Define extension to determine cadds5 type
$FILE_TYPE_LIST['cadds5'] = 'cadds5'; //Never change this value
$FILE_EXTENSION_LIST['cadds5'] = array('_fd'=>'_fd'); //Valid extension for type cadds5

//Define extension to determine camu type
$FILE_TYPE_LIST['camu'] = 'camu'; //Never change this value
$FILE_EXTENSION_LIST['camu'] = array('_db'=>'_db'); //Valid extension for type camu

//Define extension to determine pstree type
$FILE_TYPE_LIST['pstree'] = 'pstree'; //Never change this value
$FILE_EXTENSION_LIST['pstree'] = array('_ps'=>'_ps'); //Valid extension for type pstree

//Define extension to determine adrawc4 type
$FILE_TYPE_LIST['adrawc4'] = 'adrawc4'; //Never change this value
$FILE_EXTENSION_LIST['adrawc4'] = array('_pd'=>'_pd'); //Valid extension for type adrawc4

//Define extension to determine adrawc5 type
$FILE_TYPE_LIST['adrawc5'] = 'adrawc5'; //Never change this value
$FILE_EXTENSION_LIST['adrawc5'] = array('_fd'=>'_fd'); //Valid extension for type adrawc5

//Define extension to determine adrawc4 type
$FILE_TYPE_LIST['zipadrawc4'] = 'zipadrawc4'; //Never change this value
$FILE_EXTENSION_LIST['zipadrawc4'] = array('.adrawc4'=>'.adrawc4'); //Valid extension for type adrawc4

//Define extension to determine adrawc5 type
$FILE_TYPE_LIST['zipadrawc5'] = 'zipadrawc5'; //Never change this value
$FILE_EXTENSION_LIST['zipadrawc5'] = array('.adrawc5'=>'.adrawc5'); //Valid extension for type adrawc5
