<?php
if ( !defined('DEBUG') ) define('DEBUG', false); /* Activate debug mode */
if ( !defined('DESACTIVATE') ) define('DESACTIVATE', false); /* if true display an advertissement when user try to connect */
if ( !defined('DESACTIVATE_MESSAGE') ) define('DESACTIVATE_MESSAGE', 'RanchBe est en cours de maintenance. Réessayer plus tard'); /*message to display when ranchbe is desactivated*/

return array(
	'rbp' => array(
		'db' => array(
			'default' => array(
				'adapter' => 'mysql',
				'params' => array(
					'host' => 'localhost',
					'dbname' => 'ranchbe',
					'username' => 'ranchbe',
					'password' => 'ranchbe',
					'readonlyuser' => 'rbconsult',
					'readonlypass' => 'rbconsult',
					'port' => '3306',
					'autocommit' => 0
				)
			)
		),
		'auth' => array(
			/* db, ldap or passall for no authentications */
			'adapter' => 'ldap',
			'options' => array(
				/* Typical options for Active Directory */
				'server1' => array(
					'host' => 'ad.mondomain.com',
					'useStartTls' => false,
					'accountDomainName' => 'mondomain.com',
					'accountDomainNameShort' => 'MONDOMAIN',
					'accountCanonicalForm' => 3,
					'baseDn' => 'DC=mondomain,DC=com',
					'username' => 'CN=rbportail,OU=Utilisateurs système,DC=mondomain,DC=com',
					'password' => 'rbportail'
					//'accountFilterFormat'=>'(&(|(objectclass=user))(|(memberof=CN=Cloud,OU=Utilisateurs système,DC=sierbla,DC=int)))'
				)
			),
			/* ldap roles to ranchbe roles mapping */
			'rolemap' => [],
			/* ldap user to ranchbe users mapping */
			'usermap' => []
		),
		'filesystem.secure' => false, /* If true limit access to directory defined by path. configuration directives */
		
		/*Filesystem paths*/
		/* Default dir to store files */
		'path.reposit.default' => 'data/reposit/default',
		/* Default dir to store workitem files */
		'path.reposit.workitem' => 'data/reposit/workitem',
		/* Default dir to stock mockup files */
		'path.reposit.mockup' => 'data/reposit/mockup',
		/* Default dir to store cadlib files */
		'path.reposit.cadlib' => 'data/reposit/cadlib',
		/* Default dir to store bookshop files */
		'path.reposit.bookshop' => 'data/reposit/bookshop',
		/* path to archive directories */
		'path.reposit.workitem.archive' => 'data/archive/workitem',
		'path.reposit.mockup.archive' => 'data/archive/mockup',
		'path.reposit.cadlib.archive' => 'data/archive/cadlib',
		'path.reposit.bookshop.archive' => 'data/archive/bookshop',
		/* Default dir to put users files */
		'path.reposit.wildspace' => 'data/wildspace/%login%',
		/* Default dir to put tmp unzip files from a mockup update package before update */
		'path.reposit.importpackage' => 'data/import',
		'path.reposit.importunpack' => 'data/import/unpack',
		/* Default dir to put suppress files. Its better to put this dir on same partition than reposits */
		'path.reposit.trash' => 'data/trash',
		'path.temp' => '/tmp',
		/* log files */
		'path.log' => 'php://stderr', //accept values : syslog, [a php stream name], [a file location]
		/* Log file of apache (log file dispayed when application/syslog is requested) */
		'apache.log.path' => '/var/log/apache2/error.log',
		/*cron*/
		'cron.log.path' => 'data/log/cron.log',
		'job.log.path' => 'data/log/job.log',
		
		/* Scripts and triggers */
		/* Default directory where are store doctypes scripts */
		'path.scripts.doctype' => 'data/scripts/doctypeTriggers',
		/* Default directory where are store datatype scripts */
		'path.scripts.datatype' => 'data/scripts/datatypeTriggers',
		/* Default directory where are stored the sql user queries files */
		'path.scripts.queries' => 'data/scripts/sqlQueries',
		'path.scripts.document.xlsrenderer' => 'data/scripts/XlsRenderer/Document',
		/* Default directory where are store statistic query scripts */
		'path.scripts.stats' => 'data/scripts/statsQueries',
		
		/* Db Backup */
		'path.database.backup' => 'data/backup',
		/* Time in days during backup is keep before deletion */
		'dbbackup.daily.retention.delay' => 1,
		'dbbackup.weekly.retention.delay' => 15,
		
		/* Filesystem security */
		'path.authorized' => array(),

		/*Workflow*/
		'path.workflow.process' => 'data/Process/ProcessDef',
		'namespace.workflow.process' => '\ProcessDef',
		/* Time in days before delete completed process instances */
		'workflow.instance.retention.delay' => 365,
		
		/* Time in days before delete files in trash */
		'trash.retention.delay' => 30,

		/*Valid extension for visualisation file.*/
		'visu.fileExtension' => array(
			0 => '.pdf',
			1 => '.3dxml',
			2 => '.wrl',
			3 => '.igs',
			4 => '.vda',
			5 => '.stp'
		),
		'visu.roles.searchorder' => array(),
		'visu.passphrase' => 'my passphrase for encoded visualizations files',
		'thumbnails.extension' => 'png',
		'thumbnails.path' => 'public/img/thumbnails',
		'thumbnails.url' => 'img/thumbnails',
		'viewer.reposit.path' => 'data/reposit/default/visualizations',

		/* View */
		'icons.doctype.compiled.path' => 'public/img/filetypes32/C', /* Default directory where are store doctypes icon */
		'icons.doctype.compiled.url' => 'img/filetypes32/C',
		'icons.doctype.source.path' => 'public/img/filetypes32', /* Default url where are store doctypes icon */
		'icons.doctype.source.url' => 'img/filetypes32',
		'icons.doctype.type' => 'gif',
		'icons.file.path' => 'public/img/filetypes', /* Default directory where are store files icon */
		'icons.file.url' => 'img/filetypes', /* Default directory where are store files icon */

		/* */
		'view.lang' => 'en', /*Language to use*/
		'view.charset' => 'UTF-8',
		'view.timezone' => 'UMT',
		'view.stylesheet' => 'css/application.css',
		'view.stylesheet.custom' => 'css/custom.css',
		'view.longdate.format' => 'd-m-Y H:i:s', /* Format to display date with hour, min, sec. */
		'view.shortdate.format' => 'd-m-Y', /* Format to display short date. */
		'view.hour.format' => 'H:i:s', /* Format to display hour. */
		'view.url.shortcuticon' => 'img/favicon.png',
		'view.url.logo' => 'img/logo.png',
		'view.url.favicon' => 'img/favicon.png',
		'view.sitename' => 'Ranchbe',
		
		/* Converter Rbgate */
		'rbgate.server.url' => '',
		'rbgate.application' => '',
		'rbgate.working.directory' => 'data/upload',

		/* Document and objects */
		'document.assocfile.checkname' => true, /* If true, the name of file to associate to a document must be begin by the number of the document, else all files name are permit */
		'document.assocfile.iterationtokeep' => 5, /* Number of version to keep for rollback */
		'document.name.mask' => '^[a-zA-Z0-9-_.#\/]+$', /* Regex to must be verified by document number if no doctype set */
		'document.version.init' => 1, /* Default indice to indicate when create a new doc */
		'document.smartstore.maxsize' => 200, /* define the max number of files to store in one batch */
		'document.name.normalize' => true,

		'number.filter' => array(
			'search' => '[^A-Za-z0-9\-_.#]',
			'replace' => ''
		),
		
		/*Docseeder*/
		'document.number.generator' => '\Docseeder\Model\Number\Rule\DefaultRule',

		'project.name.mask' => '^[a-zA-Z0-9-_.#\/]+$', /* Regex to must be verified by project number */
		'project.name.mask.help' => 'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help display to user when he create a project */

		'workitem.name.mask' => '^[a-zA-Z0-9-_.#\/]+$', /* Regex to must be verified by number */
		'workitem.name.mask.help' => 'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */

		'mockup.name.mask' => '^[a-zA-Z0-9-_.#\/]+$', /* Regex to must be verified by number */
		'mockup.name.mask.help' => 'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */

		'bookshop.name.mask' => '^[a-zA-Z0-9-_.#\/]+$', /* Regex to must be verified by number */
		'bookshop.name.mask.help' => 'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */

		'cadlib.name.mask' => '^[a-zA-Z0-9-_.#\/]+$', /* Regex to must be verified by number */
		'cadlib.name.mask.help' => 'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */

		'doctype.name.mask' => '^[a-zA-Z0-9-_.#\/]+$', /* Regex to must be verified by number */
		'doctype.name.mask.help' => 'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */
		'doctype.default.id' => 1,

		'category.name.mask' => '^[a-zA-Z0-9-_.#\/]+$', /* Regex to must be verified by number */
		'category.name.mask.help' => 'Le nom ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */

		/* Mail */
		'admin.mail' => '',
		/* Smtp options @see Zend\Mail\Transport\SmtpOptions */
		'mail.smtp' => array(
			'name' => 'localhost.localdomain',
			'host' => '127.0.0.1'
			/* Only for activate authencation :
		 'connection_class'  => 'login',
		 'connection_config' => array(
		 'username' => 'user',
		 'password' => 'pass',
		 ),
		 */
		),
		'message.mailbox.maxsize' => 50,
		'message.archive.maxsize' => 500,

		/* Set true to send message by mail */
		'message.mail.enable' => false,
		/* Set true to send message in ranchbe message box */
		'message.enable' => true,

		/*Modules*/
		'module.product' => true,/* if true, product tab is displayed*/
		'module.project' => true,/* if true, project tab is displayed*/
		'module.workitem' => true,/* if true, workitem tab is displayed*/
		'module.cadlib' => true,/* if true, cadlib tab is displayed*/
		'module.mockup' => true,/* if true, mockup tab is displayed*/
		'module.bookshop' => true,/* if true, bookshop tab is displayed*/
		'module.partner' => true,/* if true, partner tab is displayed*/
		'module.admin' => true,/* if true, admin tab is displayed*/

		/*Security*/
		'login.password.minlength' => 10,
		'login.password.mask' => '', /* Regex to must be verified by the user password*/
		'login.password.mask.help' => '', /* Help display to user when he modify password*/
		'user.allowprefs' => true,
		/*Anonymous user*/
		'user.anonymous.id' => 2,
		'user.anonymous.uid' => 'anonymous',
		'user.anonymous.mail' => null,
		'user.anonymous.login' => 'anonymous',
		'user.anonymous.isactive' => true,

		/*Admin*/
		//'unzip.cmd'=>'.\lib\gzip\bin\gzip -d -f', /* Path to exec to uncompress .Z files */
		'unzip.cmd' => '/usr/bin/gunzip -f', /* Path to exec to uncompress .Z files */
		'backup.dump.cmd' => 'mysqldump', /* Command to dump database */

		/*Session*/
		'session.used' => true,
		'session.path' => 'data/cache/sessions',
		'session.name' => 'ranchbe',
		
		/*Wicoti*/
		'wicoti' => [
			'isdeployed' => true,
			'package' => [
				'name' => 'Wicoti.zip',
				'spacename' => 'workitem'
			]
		],

		/*Anti Flood*/
		'flood.maxsize' => 100, /* Number of tickets to keep in session */
		'flood.timeinterval' => 10, /* in sec, time interval to  */
		'flood.maxrequest' => 50, /* max request in the timeinterval */
		'flood.wait' => 5, /* If flood detected, wait n sec before execute new request */

		'converter.map' => array(
			'pdf-txt' => array(
				'\Rbs\Converter\PdfToTxt',
				'convert'
			),
			'doc-txt' => array(
				'\Rbs\Converter\DocToTxt',
				'convert'
			),
			'docx-txt' => array(
				'\Rbs\Converter\DocxToTxt',
				'convert'
			),
			'xls-txt' => array(
				'\Rbs\Converter\XlsToTxt',
				'convert'
			),
			'xlsx-txt' => array(
				'\Rbs\Converter\XlsToTxt',
				'convert'
			),
			'ppt-txt' => array(
				'\Rbs\Converter\PptToTxt',
				'convert'
			),
			'pptx-txt' => array(
				'\Rbs\Converter\PptxToTxt',
				'convert'
			),
			'doc-pdf' => array(
				'\Rbs\Converter\DocToPdfOoo',
				'convert'
			),
			'docx-pdf' => array(
				'\Rbs\Converter\DocToPdfOoo',
				'convert'
			),
			'xls-pdf' => array(
				'\Rbs\Converter\XlsToPdf',
				'convert'
			),
			'xls-txt' => array(
				'\Rbs\Converter\XlsToCsv',
				'convert'
			),
			'xls-csv' => array(
				'\Rbs\Converter\XlsToCsv',
				'convert'
			),
			'xlsx-pdf' => array(
				'\Rbs\Converter\XlsToPdf',
				'convert'
			),
			'ppt-pdf' => array(
				'\Rbs\Converter\PptToPdf',
				'convert'
			),
			'pptx-pdf' => array(
				'\Rbs\Converter\PptToPdf',
				'convert'
			),
			'pdf-doc' => array(
				'\Rbs\Converter\PdfToDoc',
				'convert'
			),
			'catproduct-ranchbe' => array(
				'\Rbgate\Converter\CatiaToRanchbe',
				'convert'
			),
			'catpart-ranchbe' => array(
				'\Rbgate\Converter\CatiaToRanchbe',
				'convert'
			),
			'catdrawing-ranchbe' => array(
				'\Rbgate\Converter\CatiaToRanchbe',
				'convert'
			)
		),
		'search.solr.client' => array(
			'hostname' => 'localhost',
			'login' => '',
			'password' => '',
			'port' => '8080'
		)
	)
);
