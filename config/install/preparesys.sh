#!/bin/bash

## HOW TO USE :
#> sudo chmod 777 preparesys.sh
#> sudo preparesys.sh

#sudo apt install git
#git clone https://ocyssau@bitbucket.org/ocyssau/rbsier.git

apt update
apt install curl apache2 php php-ldap php-mysql php-uuid php-mbstring php-xml php-gd php-zip php-xdebug php-curl php-intl python python-pip git catdoc docx2txt abiword nodejs
pip install python-pptx

#install composer
curl -s https://getcomposer.org/installer | php --
#php composer.phar self-update
php composer.phar config --global repo.packagist composer https://packagist.org
#php composer.phar install --prefer-dist

################
# Optional
#################
#sudo apt install samba
#GUI to share smb folders :
#shares-admin

#pour ubuntu 16.04 :
#sudo apt install php-ssh2 libapache2-mod-php
#pour ubuntu 14 : 
#sudo apt-get install libssh2-1-dev libssh2-php libapache2-mod-php

if [ -d /var/www ]
then 
	if [ ! -d /var/www/rbsier ]
	then 
		sudo ln -s `pwd`/public /var/www/rbsier
	fi
fi

if [ ! -e /etc/apache2/sites-available/rbsier.conf ]
then 
	cp config/dist/apache.conf /etc/apache2/sites-available/rbsier.conf
	#sudo nano /etc/apache2/sites-available/rbsier.conf
fi

#enable the site
sudo a2ensite rbsier

#enable modules
phpenmod xdebug
phpenmod ldap
a2enmod xml
a2enmod rewrite
a2enmod expires

#apply changements
service apache2 restart

#installation of bower
apt install nodejs npm
npm install -g bower
if [ ! -f /usr/bin/nodejs ]
then
	ln -s /usr/bin/nodejs /usr/bin/node
fi

echo "Xdebug is activated. For production unactive it with command : phpdismmod xdebug \n"

# Short open tags
# echo "Be sure that directive short_open_tag in your php.ini file is set to On \n"
# nano /etc/php/7.0/apache2/php.ini

#For dev
#ln -s ~/git/rbservice/rbService/vendor/Rbs/ lib/Rbs
#ln -s ~/git/rbplm3/rbplm3/vendor/Rbplm/ lib/Rbplm
#ln -s `pwd`/vendor/ocyssau/rbplm3/rbplm3/module/Rbplm/src/Rbplm lib/Rbplm
