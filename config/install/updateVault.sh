for folder in `ls -1`; do
   if [ $folder = "." ] || [ $folder = ".." ]
   then
	echo "ignore .."
   else
	iterationFolder=$folder"/__iterations"
	if [ ! -d $iterationFolder ]
        then
	     echo "create "$iterationFolder
	     mkdir $iterationFolder
             chown caoadmin:caomeca $iterationFolder
             chmod 755 $iterationFolder
	fi
   fi
done
