#!/bin/bash

## HOW TO USE :
#> sudo chmod 777 install.sh
#> install.sh

if [ ! -f public/bootstrap-tagsinput ]
then
	wget https://codeload.github.com/bootstrap-tagsinput/bootstrap-tagsinput/zip/latest
	mv latest public/latest.zip
	cd public
	unzip latest.zip
	mv bootstrap-tagsinput-latest bootstrap-tagsinput
	rm latest.zip
	bower install jqcloud2
	mv bower_components/jqcloud2/ js/.
	cd ..
fi
