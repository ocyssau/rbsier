<?php
namespace ComposerScript;

use Composer\Script\Event;
use Rbplm\Dao\Connexion;
use Application\Console\Output as Cli;
use Ranchbe;
use Zend\Stdlib\ArrayUtils;
use Zend\Console\ColorInterface;
use Zend\Console\Prompt;
use Zend\Console\Console;
use Zend\Console\Exception\ExceptionInterface as ConsoleException;
use Zend\Text\Figlet\Figlet;
use Application\Tools\FileSystemTool;
if ( !defined('CLRF') ) {
	define('CRLF', "\n");
}

/**
 * $confPath = dirname(__FILE__);
 * $appPath = dirname(dirname($confPath));
 * chdir($appPath);
 *
 */
class Installer
{

	/**
	 * @var Installer
	 */
	protected static $instance;

	/**
	 * @var Ranchbe
	 */
	public $ranchbe;

	/**
	 * @var array
	 */
	public $config;

	/**
	 *
	 * @return \ComposerScript\Installer
	 */
	public static function get()
	{
		if ( !isset(self::$instance) ) {
			$installer = new self();

			require_once 'module/Ranchbe/Ranchbe.php';
			require_once 'config/boot.php';

			$appConfig = include ('config/application.config.php');
			rbinit_autoloader($appConfig);

			$ranchbe = new \Ranchbe();
			$global = include ('config/autoload/global.php');
			$local = include ('config/autoload/local.php');
			$config = ArrayUtils::merge($global, $local);
			$ranchbe->setConfig($config['rbp']);
			$ranchbe->setLogger($ranchbe->getServiceManager()->getLogger());

			$installer->ranchbe = $ranchbe;
			$installer->config = $appConfig;

			self::$instance = $installer;
			self::welcome('Ranchbe Cli', 'Hello, Welcome to Ranchbe Command line');
		}
		return self::$instance;
	}

	/**
	 * 
	 * @param Event $event
	 * @throws Exception
	 */
	public static function postUpdate(Event $event)
	{
		// $installedPackage = $event->getOperation()->getPackage();
		// $composer = $event->getComposer();
		// copy('vendor/zendframework/zend-developer-tools/config/zenddevelopertools.local.php.dist','config/autoload/zdt.local.php');
		$installer = self::get();

		/* Twitter Bootstrap */
		if ( !is_dir('./public/bootstrap') ) {
			mkdir('./public/bootstrap');
		}
		if ( !is_dir('./public/bootstrap/js') ) {
			mkdir('./public/bootstrap/js');
		}
		if ( !is_dir('./public/bootstrap/css') ) {
			mkdir('./public/bootstrap/css');
		}
		if ( !is_dir('./public/bootstrap/fonts') ) {
			mkdir('./public/bootstrap/fonts');
		}

		$directory = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/css');
		foreach( $directory as $file ) {
			if ( $directory->isFile() ) {
				copy($directory->getPathname(), './public/bootstrap/css/' . $directory->getFileName());
			}
		}

		$directory = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/js');
		foreach( $directory as $file ) {
			if ( $directory->isFile() ) {
				copy($directory->getPathname(), './public/bootstrap/js/' . $directory->getFileName());
			}
		}

		$directory = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/fonts');
		foreach( $directory as $file ) {
			if ( $directory->isFile() ) {
				copy($directory->getPathname(), './public/bootstrap/fonts/' . $directory->getFileName());
			}
		}

		$filter = [
			'.git'
		];
		$logger = $installer->ranchbe->getLogger();

		/* copy tinymce in js */
		try {
			if ( !is_dir('./public/js/tinymce') ) {
				mkdir('./public/js/tinymce');
			}
			FileSystemTool::dircopy('vendor/tinymce/tinymce', 'public/js/tinymce', $filter, $logger, true, 0775);
		}
		catch( \Throwable $e ) {
			$package = 'TinyMCE';
			Cli::red(sprintf('error during copy of %s, check if file is present', $package));
			throw $e;
		}

		/* copy jquery in js */
		try {
			if ( !is_dir('./public/js/jquery') ) {
				mkdir('./public/js/jquery');
			}
			if ( !is_dir('./public/js/jqueryui') ) {
				mkdir('./public/js/jqueryui');
			}
			FileSystemTool::dircopy('vendor/components/jquery/', 'public/js/jquery', $filter, $logger, true, 0775);
			FileSystemTool::dircopy('vendor/components/jqueryui', 'public/js/jqueryui', $filter, $logger, true, 0775);
		}
		catch( \Throwable $e ) {
			$package = 'Jquery or JqueryUi';
			Cli::red(sprintf('error during copy of %s, check if file is present', $package));
			throw $e;
		}

		/* jstree */
		try {
			if ( !is_dir('./public/js/jstree') ) {
				mkdir('./public/js/jstree');
			}
			FileSystemTool::dircopy('vendor/vakata/jstree/dist', 'public/js/jstree', $filter, $logger, true, 0775);
		}
		catch( \Throwable $e ) {
			$package = 'Jquery or JqueryUi';
			Cli::red(sprintf('error during copy of %s, check if file is present', $package));
			throw $e;
		}

		/* threeJs */
		try {
			if ( !is_dir('./public/js/threejs') ) {
				mkdir('./public/js/threejs');
				mkdir('./public/js/threejs/examples');
			}
			FileSystemTool::dircopy('vendor/mrdoob/threejs/build', 'public/js/threejs', $filter, $logger, true, 0775);
			FileSystemTool::dircopy('vendor/mrdoob/threejs/examples/js', 'public/js/threejs', $filter, $logger, true, 0775);
		}
		catch( \Throwable $e ) {
			$package = 'ThreeJS';
			Cli::red(sprintf('error during copy of %s, check if file is present', $package));
			throw $e;
		}

		/* Bootstrap-Select */
		try {
			if ( !is_dir('./public/bootstrap-select') ) {
				mkdir('./public/bootstrap-select');
			}
			FileSystemTool::dircopy('vendor/bootstrap-select/bootstrap-select/dist/css', 'public/bootstrap-select', $filter, $logger, true, 0775);
			FileSystemTool::dircopy('vendor/bootstrap-select/bootstrap-select/dist/js', 'public/bootstrap-select', $filter, $logger, true, 0775);
		}
		catch( \Throwable $e ) {
			$package = 'Bootstrap-Select';
			Cli::red(printf('error during copy of %s, check if file is present', $package));
			throw $e;
		}

		/* EditInPlace */
		/*
		 try {
		 copy('./vendor/jquery-editInPlace/lib/jquery.editinplace.js', './public/js/jquery.editinplace.js');
		 }
		 catch( \Exception $e ) {
		 $package = 'EditInPlace';
		 Cli::red(printf('error during copy of %s, check if file is present', $package));
		 throw $e;
		 }
		 */

		/* jqueryMD5 */
		try {
			copy('./vendor/gabrieleromanato/jQuery-MD5/jquery.md5.min.js', './public/js/jquery.md5.min.js');
		}
		catch( \Throwable $e ) {
			$package = 'JqueryMd5';
			Cli::red(sprintf('error during copy of %s, check if file is present', $package));
			throw $e;
		}
	}

	/**
	 * 
	 * @param Event $event
	 */
	public static function postInstall(Event $event)
	{
		if ( !is_file('config/autoload/local.php') ) {
			Cli::blue('try to create config/autoload/local.php');
			copy('config/dist/local.php.dist', 'config/autoload/local.php');
		}

		if ( !is_file('public/.htaccess') ) {
			Cli::blue('try to create public/.htaccess');
			copy('config/dist/htaccess.dist', 'public/.htaccess');
		}

		if ( !is_file('config/application.config.php') ) {
			Cli::blue('try to create config/application.config.php');
			copy('config/dist/application.config.php.dist', 'config/application.config.php');
		}

		self::postUpdate($event);
		self::checkPaths($event);
	}

	/**
	 * Installation
	 */
	public static function checkPaths(Event $event)
	{
		$ranchbe = self::get()->ranchbe;
		$rbconfig = $ranchbe->getConfig();

		/* @var Zend\Console\Adapter\Posix $console */
		$console = Console::getInstance();

		$paths = [
			$rbconfig['path.reposit.wildspace'],
			$rbconfig['path.reposit.workitem'],
			$rbconfig['path.reposit.mockup'],
			$rbconfig['path.reposit.cadlib'],
			$rbconfig['path.reposit.bookshop'],
			$rbconfig['path.reposit.workitem.archive'],
			$rbconfig['path.reposit.mockup.archive'],
			$rbconfig['path.reposit.cadlib.archive'],
			$rbconfig['path.reposit.bookshop.archive'],
			$rbconfig['path.reposit.trash'],
			$rbconfig['path.reposit.importpackage'],
			$rbconfig['path.reposit.importunpack'],
		];

		/* make directories */
		foreach( $paths as $path ) {
			try {
				if ( !is_dir($path) ) {
					$console->writeLine('try to mkdir ' . $path, ColorInterface::BLUE);
					mkdir($path, 0777, true);
				}
				elseif ( !is_writable($path) ) {
					$console->writeLine('directory ' . $path . ' is not writable', ColorInterface::RED);
				}
			}
			catch( \Throwable $e ) {
				throw new \Exception("unable to mkdir $path");
			}
		}
	}

	/**
	 *
	 */
	public static function preparesys(Event $event)
	{
		$return = null;
		$output = null;

		self::headerLine('PrepareSys : Install required apt packages. Work only for Ubuntu plateform.', ColorInterface::LIGHT_MAGENTA);

		Cli::green('Enter sudo user password for installations of missings packages, when asked');

		$os = php_uname();
		Cli::green($os);
		/* only for linux */
		if ( strtolower(substr($os, 0, 5)) != 'linux' ) {
			Cli::red('Only Linux system is recognize by this function');
			return false;
		}
		/* Only for ubuntu */
		if ( !strstr(strtolower($os), 'ubuntu') ) {
			Cli::red('Only Ubuntu system is recognize by this function');
			return false;
		}
		/* Ubuntu version */
		exec('lsb_release -r', $output, $return);
		if ( $return === 0 ) {
			$release = $output[0];
			$release = trim(str_replace('Release:', '', $release));
			Cli::green($release);
		}

		$packages = [
			'curl',
			'apache2',
			'php',
			'php-ldap',
			'php-mysql',
			'php-uuid',
			'php-mbstring',
			'php-xml',
			'php-gd',
			'php-zip',
			'php-xdebug',
			'php-curl',
			'python',
			'python-pip',
			'git',
			'catdoc',
			'docx2txt',
			'abiword',
			'nodejs'
		];

		if ( version_compare($release, '16.04') >= 0 ) {
			$packages[] = 'php-ssh2';
			$packages[] = 'libapache2-mod-php';
		}
		elseif ( version_compare($release, '14.04') >= 0 ) {
			$packages[] = 'libssh2-1-dev';
			$packages[] = 'libssh2-php';
			$packages[] = 'libapache2-mod-php';
		}
		else {
			Cli::red('Require Ubuntu 14.04 minimum');
			return false;
		}

		passthru('sudo apt-get update', $return);

		foreach( $packages as $package ) {
			$cmd = 'sudo apt-get install ' . $package;
			passthru($cmd, $return);
		}

		passthru('pip install python-pptx', $return);

		#enable modules
		passthru('sudo phpenmod xdebug', $return);
		passthru('sudo phpenmod ldap', $return);
		passthru('sudo a2enmod xml', $return);
		passthru('sudo a2enmod rewrite', $return);
		passthru('sudo a2enmod expires', $return);

		#apply changements
		passthru('sudo service apache2 restart', $return);
	}

	/**
	 *
	 */
	public static function initDb(Event $event)
	{
		/* init installer */
		/* @var Ranchbe $ranchbe */
		$ranchbe = self::get()->ranchbe;

		/* @var Zend\Console\Adapter\Posix $console */
		$console = Console::getInstance();
		self::headerLine('INITDB : Tool to Create Database and init db user used by ranchbe to connect to Db Server.', ColorInterface::LIGHT_MAGENTA);

		if ( !is_file('config/autoload/local.php') ) {
			$console->writeLine("'config/autoload/local.php' is not found. Create it: copy config/dist/local.php.dist to config/autoload/local.php", ColorInterface::RED);
			copy('config/dist/local.php.dist', 'config/autoload/local.php');
		}
		$config = include ('config/autoload/local.php');

		/* Root database user */
		$ok = false;
		while( $ok == false ) {
			/* Database host */
			$dbhost = Prompt\Line::prompt('Input host of database server: [localhost]', true);
			(!$dbhost) ? $dbhost = 'localhost' : null;

			/* Database port */
			$dbport = Prompt\Line::prompt('Input port of database server: [3306]', true);
			(!$dbport) ? $dbport = '3306' : null;

			$config['rbp']['db']['default']['params']['host'] = $dbhost;
			$config['rbp']['db']['default']['params']['port'] = $dbport;

			/* get password for db admin user */
			$root = Prompt\Line::prompt('Input database admin user name : [root]', true);
			$rootpwd = Prompt\Password::prompt('Input database admin password :', false);

			(!$root) ? $root = 'ranchbe' : null;
			(!$rootpwd) ? $rootpwd = 'ranchbe' : null;
			
			/* Open root connexion on server */
			try {
				/* configure a connexion to server on an existing database: sys */
				Connexion::addConfig('install', [
					'adapter' => $config['rbp']['db']['default']['adapter'],
					'params' => [
						'dbname' => 'sys',
						'host' => $dbhost,
						'port' => $dbport,
						'autocommit' => 0,
						'username' => $root,
						'password' => $rootpwd
					]
				]);
				$rootConnexion = Connexion::get('install');
				$console->writeLine('Connexion on server is successfull : ', ColorInterface::LIGHT_GREEN);
				$ok = true;
			}
			catch( \Rbplm\Dao\ConnexionException $e ) {
				$console->writeLine('This credential on this database is not valid. Retry : ', ColorInterface::LIGHT_GREEN);
			}
		}
		
		/* Create database */
		$ok = false;
		while( $ok == false ) {
			try {
				/* Database Name */
				$dbname = Prompt\Line::prompt('Input database name : [ranchbe]', true);
				(!$dbname) ? $dbname = 'ranchbe' : null;
				$config['rbp']['db']['default']['params']['dbname'] = $dbname;
				$schema = new Bddschema($ranchbe->getLogger());
				$schema->createDb($rootConnexion, $dbname);
				$ok = true;
			}
			catch( \ComposerScript\BddschemaException $e ) {
				$message = $e->getMessage();
				$console->writeLine($message, ColorInterface::LIGHT_RED);
				if ( strstr('database exists', $message) ) {
					$console->writeLine('The database exist. Retry with other name : ', ColorInterface::LIGHT_GREEN);
				}
			}
		}
		
		/* Init database user */
		$dbuser = Prompt\Line::prompt('Input user name to used for access to ranchbe database: [ranchbe]', true);
		$dbpassword = 'password1';
		$dbpassword2 = 'password2';
		while( $dbpassword != $dbpassword2 || $dbpassword == '' ) {
			$dbpassword = Prompt\Password::prompt('Input password of ranchbe database user:', false);
			$dbpassword2 = Prompt\Password::prompt('Re-Input password of ranchbe database user:', false);
			if ( $dbpassword != $dbpassword2 ) {
				$console->writeLine('Passwords dont matches:', ColorInterface::RED);
			}
			if ( $dbpassword == '' ) {
				$console->writeLine('Passwords can not be null:', ColorInterface::RED);
			}
		}

		(!$dbuser) ? $dbuser = 'ranchbe' : null;

		$config['rbp']['db']['default']['params']['username'] = $dbuser;
		$config['rbp']['db']['default']['params']['password'] = $dbpassword;
		
		/* create user */
		try {
			$schema->createUser($rootConnexion, $config['rbp']['db']['default']['params']);
		}
		catch( \Throwable $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
		}

		/* grant privileges */
		try {
			$schema->grantPrivileges($rootConnexion, $config['rbp']['db']['default']['params'], 'all');
		}
		catch( \Throwable $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
		}
		
		file_put_contents('config/autoload/local.php', "<?php \n return " . var_export($config, true) . "; \n");
		
		self::headerLine('init Db is successfull', ColorInterface::LIGHT_GREEN);
		$console->writeLine('You are now ready to run ::dbtable scripts as \'composer dbtables\':', ColorInterface::LIGHT_YELLOW);
		
		#$console->writeLine('Admin config:', ColorInterface::LIGHT_YELLOW);
		#$console->writeLine(var_export($schema->adminConfig, true), ColorInterface::LIGHT_YELLOW);
		#$console->writeLine('Prod config:', ColorInterface::LIGHT_YELLOW);
		#$console->writeLine(var_export($schema->prodConfig, true), ColorInterface::LIGHT_YELLOW);
	}

	/**
	 *
	 */
	public static function compileSqlScripts(Event $event)
	{
		self::headerLine('compileSchema : Compile Sql sctipts in one script', ColorInterface::LIGHT_MAGENTA);
		
		$ranchbe = self::get()->ranchbe;
		$schema = new Bddschema($ranchbe->getLogger());
		
		$make = include ('config/install/db/makeconf.php');
		$version = $make['version'];
		
		/* @var \Zend\Console\Adapter\Posix $console */
		$console = Console::getInstance();
		
		try {
			$compiled = $make['results']['compiledSchema'];
			$schema::compile($make['schema'], $compiled, $version);
			$console->writeLine('schema successfull compiled in file ' . $compiled, ColorInterface::GREEN);
		}
		catch( \Throwable $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
			return false;
		}
		
		try {
			$compiled = $make['results']['compiledData'];
			$schema::compile($make['datas'], $compiled, $version);
			$console->writeLine('schema successfull compiled in file ' . $compiled, ColorInterface::GREEN);
		}
		catch( \Throwable $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
			return false;
		}
	}
	
	
	/**
	 *
	 */
	public static function dbTables(Event $event)
	{
		self::headerLine('DBTABLES : Tool to create tables in previously created database. Script ::initdb must be called before.', ColorInterface::LIGHT_MAGENTA);

		$ranchbe = self::get()->ranchbe;
		$config = $ranchbe->getConfig('db');
		$schema = new Bddschema($ranchbe->getLogger());

		$make = include ('config/install/db/makeconf.php');
		$version = $make['version'];
		$compiled = $make['results']['compiledSchema'];

		/* @var \Zend\Console\Adapter\Posix $console */
		$console = Console::getInstance();

		try {
			$schema::compile($make['schema'], $compiled, $version);
			$console->writeLine('schema successfull compiled in file ' . $compiled, ColorInterface::GREEN);
		}
		catch( \Throwable $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
			return false;
		}

		try {
			Connexion::setConfig($config);
			$connexion = Connexion::get('default');
			$schema->import($connexion, $compiled);
		}
		catch( \Throwable $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
			return false;
		}

		self::headerLine('Tables creation is successfull', ColorInterface::GREEN);
		$console->writeLine('You are now ready to run ::dbdatas scripts as \'composer dbdatas\':', ColorInterface::LIGHT_YELLOW);
	}

	/**
	 *
	 */
	public static function dbDatas(Event $event)
	{
		$ranchbe = self::get()->ranchbe;
		$config = $ranchbe->getConfig('db');
		$schema = new Bddschema($ranchbe->getLogger());

		self::headerLine('DBDATAS : Tool to imports datas in previously created database. Script ::dbtables must be called before.', ColorInterface::LIGHT_MAGENTA);

		/**/
		$make = include ('config/install/db/makeconf.php');
		$version = $make['version'];
		$compiled = $make['results']['compiledData'];

		/* @var \Zend\Console\Adapter\Posix $console */
		$console = Console::getInstance();

		try {
			$schema::compile($make['datas'], $compiled, $version);
			$console->writeLine('datas successfull compiled in file ' . $compiled, ColorInterface::GREEN);
		}
		catch( \Throwable $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
			return false;
		}

		try {
			Connexion::setConfig($config);
			$connexion = Connexion::get('default');
			$schema->import($connexion, $compiled);
		}
		catch( \Throwable $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
			return false;
		}

		self::headerLine('Datas import is successfull', ColorInterface::GREEN);
		$console->writeLine('The database is now fully initilized', ColorInterface::LIGHT_YELLOW);
	}

	/**
	 *
	 */
	public static function sqlExtract(Event $event)
	{
		self::headerLine('SQLEXTRACT : Tool to build the sql schema from SQLs set in Daos classes files.', ColorInterface::LIGHT_MAGENTA);

		$make = include ('config/install/db/makeconf.php');
		$topath = $make['results']['extracted'];
		$version = $make['version'];
		/* init application */
		self::get();
		/* @var \Zend\Console\Adapter\Posix $console */
		$console = Console::getInstance();

		try {
			if ( !is_writeable($topath) ) {
				if ( is_dir($topath) ) {
					throw new \Exception('Directory is existing and is not writable ' . $topath);
				}
				else {
					try {
						mkdir($topath);
					}
					catch( \Throwable $e ) {
						throw new \Exception('Path is not writable ' . $topath . ' >> ' . $e->getMessage());
					}
				}
			}
		}
		catch( \Exception $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
		}

		$console->writeLine('Extract to ' . $topath, ColorInterface::LIGHT_YELLOW);

		try {
			$extractor = new SqlScriptsExtractor();
		}
		catch( \Throwable $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
		}

		try {
			$extractor->extract('module', $topath, $version);
		}
		catch( \Throwable $e ) {
			$console->writeLine($e->getMessage(), ColorInterface::RED);
		}
	}

	/**
	 *
	 * @param Event $event
	 */
	public static function warmCache(Event $event)
	{
		// make cache toasty
	}

	/**
	 * 
	 */
	private static function welcome($title, $subtitle)
	{
		try {
			/* @var Zend\Console\Adapter\Posix $console */
			$console = Console::getInstance();
			$console->setColor(ColorInterface::GREEN);
			$console->showCursor();

			/* welcome figlet */
			$figlet = new Figlet();
			$figlet->setOutputWidth(200);
			echo $figlet->render($title);
			$console->writeLine($subtitle, ColorInterface::GREEN);
			$console->figlet = $figlet;
			$console->resetColor();
		}
		catch( ConsoleException $e ) {
			var_dump($e);
		}
	}

	/**
	 * @param string $string
	 * @param string $color
	 */
	private static function headerLine($string, $color)
	{
		$console = Console::getInstance();
		$console->writeLine(str_repeat('-', 80), $color);
		$console->writeLine($string, $color);
		$console->writeLine(str_repeat('-', 80), $color);
	}
}
