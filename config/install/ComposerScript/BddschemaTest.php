<?php
namespace ComposerScript;

use Application\Console\Output as Cli;
use Application\Model\DataTestFactory as DataFactory;

/**
 * HowTo use :
 * 
 * 		cd [ranchbe/base/dir]
 * 		php public/index.php test /ComposerScript/Bddschema initdb
 * 		php public/index.php test /ComposerScript/Bddschema schema
 * 		php public/index.php test /ComposerScript/Bddschema data
 *
 */
class BddschemaTest extends \Application\Model\AbstractTest
{

	/**
	 * 
	 */
	public function initdbTest()
	{
		$config = \Ranchbe::get()->getConfig('db');
		$schema = new Bddschema($config, 'root', 'root');

		Cli::yellow('Admin config:');
		Cli::blue(var_export($schema->adminConfig, true));
		Cli::yellow('Prod config:');
		Cli::blue(var_export($schema->prodConfig, true));

		try {
			$schema->createDb();
		}
		catch( \Throwable $e ) {
			Cli::red($e->getMessage());
		}

		try {
			$schema->initUser();
		}
		catch( \Throwable $e ) {
			Cli::red($e->getMessage());
		}
	}

	/**
	 *
	 */
	public function schemaTest()
	{
		$config = \Ranchbe::get()->getConfig('db');
		$schema = new Bddschema($config, 'root', 'root');

		$make = include ('config/install/makeconf.php');
		$version = $make['version'];
		$compiled = $make['results']['compiledSchema'];

		Cli::yellow('Admin config:');
		Cli::blue(var_export($schema->adminConfig, true));
		Cli::yellow('Prod config:');
		Cli::blue(var_export($schema->prodConfig, true));

		try {
			Bddschema::compile($make['schema'], $compiled, $version);
			Cli::green($compliedSchema);
		}
		catch( \Throwable $e ) {
			Cli::red($e->getMessage());
		}

		try {
			$schema->import($compliedSchema);
		}
		catch( \Throwable $e ) {
			Cli::red($e->getMessage());
		}
	}

	/**
	 *
	 */
	public function dataTest()
	{
		$config = \Ranchbe::get()->getConfig('db');
		$schema = new Bddschema($config, 'root', 'root');

		$make = include ('config/install/makeconf.php');
		$version = $make['version'];
		$compiled = $make['results']['compiledData'];

		Cli::yellow('Admin config:');
		Cli::blue(var_export($schema->adminConfig, true));
		Cli::yellow('Prod config:');
		Cli::blue(var_export($schema->prodConfig, true));

		try {
			Bddschema::compile($make['datas'], $compiled, $version);
			Cli::green($schema->datasScript);
		}
		catch( \Throwable $e ) {
			Cli::red($e->getMessage());
		}

		try {
			$schema->import($compiled);
		}
		catch( \Throwable $e ) {
			Cli::red($e->getMessage());
		}
	}
}
