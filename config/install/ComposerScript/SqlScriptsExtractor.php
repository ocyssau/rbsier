<?php
namespace ComposerScript;

use Application\Console\Output as Cli;

/**
 *
 * @author olivier
 *
 */
class SqlScriptsExtractor
{

	/**
	 * 
	 */
	public function __construct()
	{}

	/**
	 * Return a list of dao class definition files.
	 *
	 * @param string $basePath
	 * @return array
	 */
	private static function _parseDirectoryTree($basePath)
	{
		$directoryIt = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($basePath));
		foreach( $directoryIt as $file ) {
			if ( substr($file->getFilename(), -7) == 'Dao.php' ) {
				$files[] = $file->getRealPath();
			}
		}
		return $files;
	}

	/**
	 * 
	 * @param \SplFileObject $file
	 * @param string $openFlag
	 * @return string[]
	 */
	private static function _parseSql(\SplFileObject $file, $openFlag)
	{
		Cli::green(sprintf('parse %s with open flag %s', $file->getPathname(), $openFlag));

		$lines = [];
		$file->rewind();

		$lines[] = '-- ' . str_repeat('*', 70) . "\n";
		$lines[] = '-- From file ' . $file->getPathname() . "\n";
		$lines[] = '-- ' . str_repeat('*', 70) . "\n";

		/* go to line of open flag*/
		while( strstr($file->current(), $openFlag) === false && !$file->eof() ) {
			$file->next();
		}

		/* if read until end of file, open flag is not found*/
		if ( $file->eof() ) {
			Cli::blue(sprintf('Open flag %s is not found', $openFlag));
			return $lines;
		}

		/* ignore opne flag */
		$file->next();

		/* read next lines until close flag */
		while( strstr($file->current(), '<<') === false && !$file->eof() ) {
			$string = $file->current();
			$string = trim($string);
			$string = trim($string, '*');
			$string = $string . "\n";
			if ( $string != '' ) {
				$lines[] = $string;
			}
			$file->next();
		}

		/* if read until end of file, close flag is not found */
		if ( $file->eof() ) {
			Cli::red(sprintf('Close flag is not found in file %s for open flag %s', $file->getPathname(), $openFlag));
		}

		return $lines;
	}

	/**
	 *
	 */
	private static function _compile($fileName, array $content)
	{
		$resultFile = new \SplFileObject($fileName, 'w+');
		foreach( $content as $sql ) {
			$resultFile->fwrite(implode("", $sql));
		}
	}

	/** SQL_SCRIPT>>
	 <<*/

	/** SQL_INSERT>>
	 <<*/

	/** SQL_ALTER>>
	 <<*/

	/** SQL_FKEY>>
	 <<*/

	/** SQL_TRIGGER>>
	 <<*/

	/** SQL_VIEW>>
	 <<*/

	/** SQL_DROP>>
	 <<*/

	/**
	 *
	 * @param string $pathToScan path of folder tree to scan
	 * @param string $resultPath path where put extracted data files
	 * @param string $version version tag of compilation
	 * @return void
	 */
	public function extract($pathToScan, $resultPath, $version)
	{
		$creates = [];
		$alters = [];
		$fkeys = [];
		$triggers = [];
		$views = [];
		$inserts = [];
		$drops = [];

		foreach( self::_parseDirectoryTree($pathToScan) as $path ) {
			$file = new \SplFileObject($path);
			$creates[] = self::_parseSql($file, 'SQL_SCRIPT');
			$alters[] = self::_parseSql($file, 'SQL_ALTER');
			$fkeys[] = self::_parseSql($file, 'SQL_FKEY');
			$triggers[] = self::_parseSql($file, 'SQL_TRIGGER');
			$views[] = self::_parseSql($file, 'SQL_VIEW');
			$inserts[] = self::_parseSql($file, 'SQL_INSERT');
			$drops[] = self::_parseSql($file, 'SQL_DROP');
		}

		$creates = $this->_extractAndReorderLike($creates);

		self::_compile(sprintf('%s/create.%s.sql', $resultPath, $version), $creates);
		self::_compile(sprintf('%s/alter.%s.sql', $resultPath, $version), $alters);
		self::_compile(sprintf('%s/foreignKey.%s.sql', $resultPath, $version), $fkeys);
		self::_compile(sprintf('%s/trigger.%s.sql', $resultPath, $version), $triggers);
		self::_compile(sprintf('%s/view.%s.sql', $resultPath, $version), $views);
		self::_compile(sprintf('%s/insert.%s.sql', $resultPath, $version), $inserts);
		self::_compile(sprintf('%s/drop.%s.sql', $resultPath, $version), $drops);
	}

	/**
	 * Try to re order creation of TABLE before creation of LIKE TABLE.
	 * The LIKE keyword must be on same line than CREATE
	 * 
	 * @param array	$create
	 * @param boolean $v If true, verbose mode
	 * @return array
	 */
	public function _extractAndReorderLike($create, $v = true)
	{
		$createIter = new \ArrayIterator($create);
		$likes = [];
		$creates = [];
		$lines = [];

		/* extract inherits rules: */
		while( $createIter->valid() ) {
			$table = '';
			$parentTable = '';
			$current = $createIter->current();
			foreach( $current as $line ) {
				/* search create table command */
				if ( !$table ) {
					/* current line begin a create table command */
					$ok = preg_match('/(CREATE\s+TABLE){1}(\s+IF\s+NOT\s+EXISTS){0,1}(\s+`?[a-zA-Z_]+`?)/i', $line, $match);
					if ( $ok ) {
						$table = array_pop($match);
						$table = trim(str_replace('`', '', $table));
						if ( $v ) Cli::green("Table $table");
					}
				}
				if ( $table ) {
					$lines[] = $line;
					$match = null;
					$ok = preg_match('/(\s+LIKE\s+)(`?[a-zA-Z_]+`?)/i', $line, $match);
					if ( $ok ) {
						$parentTable = array_pop($match);
						$parentTable = trim(str_replace('`', '', $parentTable));
						if ( $v ) Cli::green("Parent table $parentTable");
					}

					/* if contain ;, and of CREATE TABLE command
					 * write result in index
					 * ATTENTION @todo: ';' symbole not must be found on a comment line 
					 */
					$ok = preg_match('/;/', $line);
					if ( $ok ) {
						if ( $table && $parentTable ) {
							$likes[] = [
								'sql' => $lines,
								'table' => $table,
								'parent' => $parentTable
							];
						}
						else {
							$creates[] = $lines;
						}
						$lines = [];
						$table = '';
						$parentTable = '';
					}
				}
			}
			$createIter->next();
		} /* End of while */

		/* Re-orders table creation in order of inherits */
		$index = [];
		/* build index */
		foreach( $likes as $i => $like ) {
			$index[$like['table']] = $i;
		}
		/* Re-Order */
		foreach( $likes as $i => $like ) {
			$table = $like['table'];
			$parentTable = $like['parent'];
			$sql = $like['sql'];

			/* move creation to out pile */
			if ( isset($index[$parentTable]) ) {
				if ( $index[$parentTable] > $i ) {
					if ( $v ) Cli::yellow("Re-Order table " . $table);
					unset($likes[$i]);
					array_push($likes, $like);
				}
			}
		} /* End foreach */

		/* rebuild output */
		foreach( $likes as $like ) {
			$sql = $like['sql'];
			$creates[] = $sql;
		} /* End foreach */

		return $create;
	}
}
