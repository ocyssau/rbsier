<?php
namespace ComposerScript;

use Rbplm\Dao\Connexion;
use ComposerScript\BddschemaException as Exception;

/**
 * 
 *
 */
class Bddschema
{

	/**
	 *
	 * @var string
	 */
	public $schemaScript;

	/**
	 *
	 * @var string
	 */
	public $datasScript;

	/**
	 * Sql schema as imported
	 * @var string
	 */
	public $schema;

	/**
	 * 
	 * @var \PDO
	 */
	private $connexion;

	/**
	 * List of sql scripts files containing schema sql builder
	 * @var array
	 */
	private $sqlInScripts = [];

	/**
	 * List of sql scripts files containing datas to import in db
	 * @var array
	 */
	private $sqlDatasInScripts = [];

	/**
	 * 
	 * @var \Rbplm\Sys\Logger
	 */
	private $logger;

	/**
	 * 
	 * @param \Rbplm\Sys\Logger $logger
	 */
	public function __construct($logger = null)
	{
		$this->logger = $logger;
	}

	/**
	 * 
	 * @param \PDO $connexion
	 * @return Bddschema
	 */
	public function setConnexion(\PDO $connexion)
	{
		$this->connexion = $connexion;
		return $this;
	}

	/**
	 * Create a new database
	 * 
	 * @param \PDO $connexion PDO connexion to use
	 * @param string $name Name of the new database
	 * @throws Exception
	 * @return Bddschema
	 */
	public function createDb($connexion, $name)
	{
		try {
			$sql = "CREATE DATABASE $name CHARACTER SET utf8 COLLATE utf8_general_ci";
			$connexion->exec($sql);
		}
		catch( \Throwable $e ) {
			throw new Exception('unable to create bdd ' . $name . '. May be it is yet existing. >' . $e->getMessage());
		}

		return $this;
	}

	/**
	 * Create user
	 * 
	 * @param \PDO $connexion
	 * @param array $params Array[dbname=>string, username=>string, password=>string, host=>string]
	 * @throws Exception
	 * @return Bddschema
	 */
	public function createUser(\PDO $connexion, array $params)
	{
		$dbname = $params['dbname'];
		$user = $params['username'];
		$password = $params['password'];
		$host = $params['host'];

		try {
			$sql = sprintf("CREATE USER IF NOT EXISTS '%s'@'%s' IDENTIFIED BY '%s'", $user, $host, $password);
			$connexion->exec($sql);
		}
		catch( \Throwable $e ) {
			throw new Exception('unable to create user ' . $user . ' > ' . $e->getMessage());
		}

		return $this;
	}

	/**
	 * Grant privileges
	 *
	 * @param \PDO $connexion
	 * @param array $params Array[dbname=>string, username=>string, password=>string, host=>string]
	 * @throws Exception
	 * @return Bddschema
	 */
	public function grantPrivileges(\PDO $connexion, array $params, $rules = 'all')
	{
		$dbname = $params['dbname'];
		$user = $params['username'];
		$password = $params['password'];
		$host = $params['host'];

		try {
			$sql = sprintf("GRANT ALL ON %s.* TO '%s'@'localhost'", $dbname, $user);
			$connexion->exec($sql);
		}
		catch( \Throwable $e ) {
			throw new Exception('unable to grant privilege to ' . $user . ' >' . $e->getMessage());
		}

		return $this;
	}

	/**
	 * @param array $scripts Path to result compiled script
	 * @param string $scriptFile Path to result compiled script
	 * @param string $version Version tag for this 
	 * @return string
	 */
	public static function compile(array $scripts, string $scriptFile, string $version)
	{
		/* compile scripts */
		$now = date('Y-m-d');
		$sql = "-- compiled $now \n";
		$sql .= "-- version $version \n";
		$sql .= "-- \n";
		foreach( $scripts as $script ) {
			$sql .= file_get_contents($script, true);
		}

		try {
			file_put_contents($scriptFile, $sql);
		}
		catch( \Exception $e ) {
			throw new Exception('Unable to save sql scripts in : ' . $scriptFile . ' >' . $e->getMessage());
		}
	}

	/**
	 * @param \PDO $connexion
	 * @param string $scriptFile
	 * @return Bddschema
	 */
	public function import(\PDO $connexion, string $scriptFile)
	{
		$sql = '';
		try {
			$sql = file_get_contents($scriptFile, true);
			$connexion->exec($sql);
		}
		catch( \Exception $e ) {
			throw new Exception('Unable to import sql from : ' . $scriptFile . ' >' . $e->getMessage());
		}
		
		return $this;
	}

	/**
	 * Populate doctype table with initales doctypes define in csv file.
	 * @param string	$csvFile	Full path to valid csv file
	 * @param array 	$options	Array of options:
	 * 			array('csvDelimiter'=>',', 'csvEnclosure'=>'"', 'table'=>'ged_doctype')
	 *
	 * @return void
	 * @deprecated
	 */
	private function NOTUSED_________initDoctype________________($csvFile, $options = array())
	{
		$conn = Rbplm_Dao_Connexion::get();
		$table = 'ged_doctype';
		$delimiter = ',';
		$enclosure = '"';

		/* Create a Ou for doctypes*/
		try {
			$Ou = new Rbplm_Org_Unit(array(
				'name' => 'doctypes'
			), Rbplm_Org_Root::singleton());
			//$OuDao = Rbplm_Dao_Factory::getDao('Rbplm_Org_Unit');
			$OuDao = new Rbplm_Org_UnitDaoPg(array(), $conn);
			$OuDao->save($Ou);
		}
		catch( Exception $e ) {
			echo 'Ou is yet created? See errors:' . CRLF;
			echo $e->getMessage() . CRLF;
			$OuDao->loadFromPath($Ou, '/RanchbePlm/doctypes');
		}

		if ( $options['csvDelimiter'] ) {
			$delimiter = $options['csvDelimiter'];
		}
		if ( $options['csvEnclosure'] ) {
			$enclosure = $options['csvEnclosure'];
		}
		if ( $options['table'] ) {
			$table = $options['table'];
		}
		if ( $options['conn'] ) {
			$conn = $options['conn'];
		}

		if ( !file_exists($csvFile) ) {
			throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::FILE_NOT_FOUND, Rbplm_Sys_Error::WARNING, $csvFile);
		}

		$file = new SplFileObject($csvFile, 'r', true);
		$file->setFlags(SplFileObject::SKIP_EMPTY);
		$List = new Rbplm_Dao_Pg_List(array(
			'table' => $table
		), $conn);
		$alist = array();

		/*First line is fields names*/
		$fields = $file->fgetcsv($delimiter, $enclosure, '\\');
		while( !$file->eof() ) {
			$line = $file->fgetcsv($delimiter, $enclosure, '\\');
			$properties = array(); //temp array
			if ( $line ) {
				foreach( $line as $i => $val ) {
					$properties[$fields[$i]] = $val;
				}
				$properties['parent'] = $Ou->getUid();
				$alist[] = $properties;
			}
		}

		try {
			/*Save the list in Db*/
			$List->save($alist, array(
				'pkey' => array(
					'uid'
				),
				'priority' => 'insert'
			));
		}
		catch( Exception $e ) {
			echo 'WARNING: Doctypes are probably yet init, see errors below :' . CRLF;
			echo 'Error: ' . $e->getMessage() . CRLF;
		}
	}
}


