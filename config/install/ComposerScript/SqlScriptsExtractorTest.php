<?php
namespace ComposerScript;

use Application\Console\Output as Cli;
use Application\Model\DataTestFactory as DataFactory;

/** 
 * HowTo use :
 * 
 * 		cd [ranchbe/base/dir]
 * 		php public/index.php test /ComposerScript/SqlScriptsExtractor run
 * 
 */
class SqlScriptsExtractorTest extends \Application\Model\AbstractTest
{

	/**
	 *
	 */
	public function runTest()
	{
		$make = include('config/install/db/makeconf.php');
		
		$topath = $make['results']['extracted'];
		$version = $make['version'];
		
		try{
			if(!is_writeable($topath)){
				if(is_dir($topath)){
					throw new \Exception('Directory is existing and is not writable ' . $topath);
				}
				else{
					try{
						mkdir($topath);
					}
					catch(\Throwable $e){
						throw new \Exception('Path is not writable ' . $topath . ' >> '. $e->getMessage());
					}
				}
			}
		}
		catch(\Exception $e){
			Cli::red($e->getMessage());
		}
		
		Cli::yellow('Extract to ' . $topath);
		
		try {
			$extractor = new SqlScriptsExtractor();
		}
		catch( \Throwable $e ) {
			Cli::red($e->getMessage());
		}
		
		try {
			$extractor->extract('module', $topath, $version);
		}
		catch( \Throwable $e ) {
			Cli::red($e->getMessage());
		}
	}
}
