#!/bin/bash

## HOW TO USE :
#> sudo chmod 777 config/install/initmod.sh
#> sudo config/install/initmod.sh

apacheUser=`ps -ef | grep apache2 | tail -1 | awk '{print $1}'`
apacheGroup=$apacheUser

chgrp -R $apacheGroup *
chgrp -R $apacheGroup data
chmod -R 750 *
chmod -R 770 data
