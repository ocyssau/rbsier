#!/bin/bash

## HOW TO USE :
#> chmod 777 gitconf.sh
#> gitconf.sh

git config credential.helper 'cache --timeout=28800'
git config core.filemode false
