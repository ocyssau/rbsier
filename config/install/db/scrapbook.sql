-- #############################################################################################################"
-- VERSION 0.40 >>
ALTER TABLE `workitem_categories`
	ADD COLUMN `uid` VARCHAR(64) NOT NULL;
UPDATE `workitem_categories` SET uid=SUBSTR(UUID(),1,8);
ALTER TABLE `workitem_categories`
	ADD UNIQUE KEY `UC_category_uid` (`uid`);

ALTER TABLE `cadlib_categories`
	ADD COLUMN `uid` VARCHAR(64) NOT NULL;
UPDATE `cadlib_categories` SET uid=SUBSTR(UUID(),1,8);
ALTER TABLE `cadlib_categories`
	ADD UNIQUE KEY `UC_category_uid` (`uid`);

ALTER TABLE `bookshop_categories`
	ADD COLUMN `uid` VARCHAR(64) NOT NULL;
UPDATE `bookshop_categories` SET uid=SUBSTR(UUID(),1,8);
ALTER TABLE `bookshop_categories`
	ADD UNIQUE KEY `UC_category_uid` (`uid`);

ALTER TABLE `mockup_categories`
	ADD COLUMN `uid` VARCHAR(64) NOT NULL;
UPDATE `mockup_categories` SET uid=SUBSTR(UUID(),1,8);
ALTER TABLE `mockup_categories`
	ADD UNIQUE KEY `UC_category_uid` (`uid`);

-- ##################
ALTER TABLE `workitem_metadata` 
	ADD COLUMN `uid` VARCHAR(64) DEFAULT NULL AFTER `id`,
	ADD UNIQUE KEY `UC_uid` (`uid`);

UPDATE `workitem_metadata` SET 
	`uid`=`field_name`;
	
	
ALTER TABLE `cadlib_metadata` 
	ADD COLUMN `uid` VARCHAR(64) DEFAULT NULL AFTER `id`,
	ADD UNIQUE KEY `UC_uid` (`uid`);

UPDATE `cadlib_metadata` SET 
	`uid`=`field_name`;
	
ALTER TABLE `bookshop_metadata` 
	ADD COLUMN `uid` VARCHAR(64) DEFAULT NULL AFTER `id`,
	ADD UNIQUE KEY `UC_uid` (`uid`);

UPDATE `bookshop_metadata` SET 
	`uid`=`field_name`;
	
ALTER TABLE `mockup_metadata` 
	ADD COLUMN `uid` VARCHAR(64) DEFAULT NULL AFTER `id`,
	ADD UNIQUE KEY `UC_uid` (`uid`);

UPDATE `mockup_metadata` SET 
	`uid`=`field_name`;

INSERT INTO schema_version(version,lastModification) VALUES('0.40',now());

-- << VERSION 0.40 
-- ########################################################################################################"
 


SELECT * FROM projects;
SELECT * FROM workitems;
SELECT * FROM bookshops;
SELECT * FROM cadlibs;
SELECT * FROM mockups;
SELECT * FROM org_doctype_rel;
SELECT * FROM doctypes;


-- SELECT tree nodes WITH PARENT
SELECT child.id, child.name, parent.name FROM
(
SELECT id,uid,cid,dn,name FROM workitems
UNION 
SELECT id,uid,cid,dn,name FROM projects
) as parent 
	JOIN org_doctype_rel as lnk ON lnk.parent_id=parent.id  AND '/08bf11e1/00z000-00/' LIKE CONCAT(parent.dn,'%')
	JOIN doctypes AS child ON lnk.child_id=child.id 
ORDER BY child.name ASC;


-- SELECT tree nodes FROM LINK ONLY
SELECT lnk.parent_id, child.id, child.name, lnk.rdn FROM 
	org_doctype_rel as lnk 
	JOIN doctypes AS child ON lnk.child_id=child.id 
WHERE '/08bf11e1/00z000-00/' LIKE CONCAT(lnk.rdn,'%')
ORDER BY child.name ASC;

-- SELECT tree nodes FROM LINK ONLY
SELECT child.id, child.name, lnk.rdn FROM 
	org_doctype_rel as lnk 
	JOIN doctypes AS child ON lnk.child_id=child.id 
WHERE (SELECT dn FROM workitems WHERE id=520) LIKE CONCAT(lnk.rdn,'%')
ORDER BY child.name ASC;



	
	