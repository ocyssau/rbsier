-- compiled 2020-02-15 
-- version 1.2.2 
-- 
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE IF NOT EXISTS `schema_version` (
  `version` varchar(64) DEFAULT NULL,
  `lastModification` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Application/src/Application/Cron/schemaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbgate/src/Rbgate/Logger/LogDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `cadimporter_log` (
`document_id` int(11) NOT NULL,
`docfile_id` int(11) NOT NULL,
`spacename` VARCHAR(32) NOT NULL,
`updated` DATETIME NULL,
PRIMARY KEY (`docfile_id`, `spacename`),
UNIQUE `UK_cadimporter_log_1` (`document_id`),
KEY `UK_cadimporter_log_2` (`spacename`)
) ENGINE=InnoDb;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Import/src/Import/Model/PackageDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `import_package` (
`package_id` INT(11) NOT NULL,
`uid` VARCHAR(64) NOT NULL,
`batch_id` INT(11) DEFAULT NULL,
`description` text,
`state` VARCHAR(32) DEFAULT NULL,
`imported_logfile` text,
`error_logfile` text,
`feedback_logfile` text,
`file_name` VARCHAR(128) DEFAULT NULL,
`file_path` VARCHAR(512) DEFAULT NULL,
`file_extension` VARCHAR(32) DEFAULT NULL,
`file_mtime` INT(11) DEFAULT NULL,
`file_size` INT(11) DEFAULT NULL,
`file_md5` VARCHAR(128) NOT NULL DEFAULT '',
PRIMARY KEY (`package_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `import_package_in_reposit` (
`file_name` VARCHAR(128) NOT NULL,
`file_path` VARCHAR(512) NOT NULL,
`file_extension` VARCHAR(32) DEFAULT NULL,
`file_mtime` INT(11) DEFAULT NULL,
`file_size` INT(11) DEFAULT NULL,
`file_md5` VARCHAR(128) NOT NULL DEFAULT '',
`package_id` INT(11) NOT NULL,
PRIMARY KEY (`file_name`,`file_path`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/TypeDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `docseeder_type` (
`id` int(11) NOT NULL DEFAULT '0',
`name` VARCHAR(128) NOT NULL DEFAULT '',
`designation` VARCHAR(256) DEFAULT NULL,
`domain` VARCHAR(64) DEFAULT NULL,
`template_id` int(11) DEFAULT NULL COMMENT 'id of the template docfile',
`template_uid` VARCHAR(64) DEFAULT NULL COMMENT 'uid of the template docfile',
`mask_id` int(11) DEFAULT NULL COMMENT 'id of the mask to use',
`mask_uid` VARCHAR(64) DEFAULT NULL COMMENT 'uid of the mask',
PRIMARY KEY (`id`),
UNIQUE KEY (`name`),
INDEX (`template_id`),
INDEX (`template_uid`),
INDEX (`mask_id`),
INDEX (`mask_uid`),
INDEX (`domain`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `docseeder_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO docseeder_seq(id) VALUES(10);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/Template/MaskDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `docseeder_mask` (
`id` int(11) NOT NULL,
`doctype_id` int(11) NOT NULL,
`uid` VARCHAR(32) NOT NULL,
`name` VARCHAR(64) NOT NULL DEFAULT '',
`map` JSON DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`doctype_id`),
UNIQUE KEY (`uid`),
UNIQUE KEY (`name`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Admin/src/Admin/Dao/ToolDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaProjectLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaDao.php
-- **********************************************************************

CREATE TABLE IF NOT EXISTS `archiver_media` (
`id` int(11) NOT NULL,
`uid` VARCHAR(64) NOT NULL,
`cid` VARCHAR(64) NOT NULL DEFAULT 'archivermedia',
`name` VARCHAR(128) DEFAULT NULL,
`number` VARCHAR(128) DEFAULT NULL,
`designation` text,
`type` VARCHAR(64) DEFAULT NULL,
`emplacement` VARCHAR(128) DEFAULT NULL,
`created` DATETIME DEFAULT NULL,
`create_by_id` int(11) DEFAULT NULL,
`owner_id` int(11) DEFAULT NULL,
`notice` text,
PRIMARY KEY (`id`),
UNIQUE KEY `UC_archiver_media_1` (`uid`),
UNIQUE KEY `UC_archiver_media_2` (`number`),
INDEX `INDEX_archiver_media_1` (`name`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `archiver_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO archiver_seq SET id=10;

CREATE TABLE IF NOT EXISTS `archiver_media_object_rel` (
`uid` VARCHAR(64) NOT NULL,
`media_id` int(11) NOT NULL,
`ofObjectId` int(11) DEFAULT NULL,
`ofObjectUid` VARCHAR(64) DEFAULT NULL,
`ofObjectCid` VARCHAR(64) DEFAULT NULL,
`spacename` VARCHAR(64) DEFAULT NULL,
PRIMARY KEY (`uid`),
INDEX `INDEX_archiver_media_2` (`ofObjectId`),
INDEX `INDEX_archiver_media_3` (`ofObjectUid`),
INDEX `INDEX_archiver_media_4` (`ofObjectCid`),
INDEX `INDEX_archiver_media_5` (`spacename`)
) ENGINE=InnoDB;


-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Model/IndexerDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `indexer` (
`document_id` int(11) NOT NULL,
`docfile_id` int(11) NOT NULL,
`spacename` VARCHAR(32) NOT NULL,
`updated` DATETIME NULL,
`content` TEXT,
PRIMARY KEY (`docfile_id`, `spacename`),
UNIQUE (`spacename` ASC, `docfile_id` ASC),
KEY (`spacename`),
KEY (`document_id` ASC, `spacename` ASC),
FULLTEXT (content)
) ENGINE=MyIsam;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Engine/SearchDao.php
-- **********************************************************************
DROP TABLE IF EXISTS `search_update`;
CREATE TABLE IF NOT EXISTS `search_update` (
`updated` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyIsam;
INSERT INTO `search_update` SET `updated`=now();

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/ResourceDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `acl_resource` (
`id` int(11) NOT NULL,
`uid` VARCHAR(64) NOT NULL,
`cid` VARCHAR(64) DEFAULT 'aclresource60',
`cn` VARCHAR(255) NOT NULL,
`ownerId` int(11) DEFAULT NULL,
`ownerUid` VARCHAR(64) DEFAULT NULL,
`referToUid` VARCHAR(64) DEFAULT NULL,
`referToId` int(11) DEFAULT NULL,
`referToCid` VARCHAR(64) DEFAULT NULL,
UNIQUE KEY (`id`),
UNIQUE KEY (`uid`),
UNIQUE KEY (`cn`),
KEY (`referToUid`),
KEY (`referToId`),
KEY (`referToCid`),
KEY (`ownerId`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `acl_resource_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10;
INSERT INTO acl_resource_seq (id) VALUES (10);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RuleDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `acl_rule` (
`roleId` int(11) NOT NULL,
`resource` VARCHAR(128) NOT NULL,
`resourceId` INT(11) DEFAULT NULL,
`rightId` int(11) NOT NULL,
`rule` VARCHAR(16) DEFAULT 'allow',
PRIMARY KEY (`roleId`,`rightId`, `resource`),
KEY role_idx(`roleId`),
KEY right_idx(`rightId`),
KEY resource_idx(`resource`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RightDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `acl_right` (
`id` int(11) NOT NULL,
`name` VARCHAR(32) NOT NULL,
`description` VARCHAR(64) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`name`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/UserRoleDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `acl_user_role` (
`uid` VARCHAR(64) NOT NULL,
`cid` char(13) NOT NULL DEFAULT 'acluserrole59',
`userId` int(11) NOT NULL,
`roleId` int(11) NOT NULL,
`resourceCn` VARCHAR(255) NOT NULL,
PRIMARY KEY `id_i_idx` (`userId`,`roleId`,`resourceCn`),
KEY (`userId`),
KEY (`roleId`),
KEY (`resourceCn`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RoleDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `acl_role` (
`id` INT(11) NOT NULL,
`uid` VARCHAR(64) NOT NULL,
`cid` VARCHAR(64) NOT NULL DEFAULT 'aclrole3zc547',
`name` VARCHAR(32) DEFAULT NULL,
`parentId` INT(11) DEFAULT NULL,
`parentUid` VARCHAR(64) DEFAULT NULL,
`description` TEXT DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`uid`),
UNIQUE KEY (`name`),
KEY (`parentId`),
KEY (`parentUid`)
) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `acl_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100;
INSERT INTO acl_seq (id) VALUES (100);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/ClassDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/PropsetDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS propset (
lparent VARCHAR(32) NOT NULL,
property integer NOT NULL,
cid integer NOT NULL,
PRIMARY KEY (lparent, property, cid)
);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/History/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Typemime/TypemimeDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `typemimes` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`uid` VARCHAR(255) NOT NULL DEFAULT '',
`name` VARCHAR(255) NOT NULL DEFAULT '',
`extensions` VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE (`uid`),
INDEX (`name`),
INDEX (`extensions`)
) ENGINE=MyIsam;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Extended/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Postit/PostitDao.php
-- **********************************************************************
-- 
CREATE TABLE IF NOT EXISTS `postit` (
`id` int(11) NOT NULL,
`uid` VARCHAR(32) NOT NULL,
`cid` VARCHAR(32) NOT NULL,
`name` VARCHAR(128) DEFAULT NULL,
`parent_id` INT(11) NOT NULL,
`parent_uid` VARCHAR(32) DEFAULT NULL,
`parent_cid` VARCHAR(32) NOT NULL,
`owner_id` VARCHAR(32) DEFAULT NULL,
`spacename` VARCHAR(32) NOT NULL,
`body` MEDIUMTEXT DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `U_uid` (`uid`),
KEY `K_parentId` (`parent_id`),
KEY `K_parentUid` (`parent_uid`),
KEY `K_parentCid` (`parent_cid`),
KEY `K_spaceName` (`spacename`),
KEY `K_ownerId` (`owner_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `postit_seq`(
`id` INT(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO postit_seq(id) VALUES(10);

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `wf_document_link` (
`uid` VARCHAR(64) NOT NULL,
`parentId` int(11) NOT NULL,
`childId` int(11) NOT NULL,
`parentUid` VARCHAR(255) DEFAULT NULL,
`childUid` VARCHAR(255) DEFAULT NULL,
`name` VARCHAR(255) DEFAULT NULL,
`lindex` int(11) DEFAULT '0',
`attributes` mediumtext,
`spacename` VARCHAR(64) NOT NULL,
PRIMARY KEY (`parentId`,`childId`),
UNIQUE KEY `uid` (`uid`),
KEY `name` (`name`),
KEY `lindex` (`lindex`),
KEY `parentUid` (`parentUid`),
KEY `childUid` (`childUid`),
KEY `parentId` (`parentId`),
KEY `childId` (`childId`),
CONSTRAINT `FK_wf_document_link_1` FOREIGN KEY (`childId`) REFERENCES `wf_instance` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/ActivityDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS wf_instance_activity(
`id` INT(11) NOT NULL AUTO_INCREMENT,
`uid` VARCHAR(64)  NOT NULL,
`cid` VARCHAR(64) NOT NULL DEFAULT '56acc299ed22a',
`name` VARCHAR(128) DEFAULT NULL,
`title` VARCHAR(256) DEFAULT NULL,
`ownerId` VARCHAR(64) DEFAULT NULL,
`updated` datetime DEFAULT NULL,
`updateById` VARCHAR(64) DEFAULT NULL,
`parentId` INT(11) DEFAULT NULL,
`parentUid` VARCHAR(128) DEFAULT NULL,
`instanceId` INT(11) NOT NULL,
`activityId` INT(11) NOT NULL,
`status` ENUM('running','completed') NULL,
`type` ENUM('start','end','split','switch','join','activity','standalone') DEFAULT NULL,
`started` datetime NULL,
`ended` datetime NULL,
`attributes` MEDIUMTEXT NULL,
`comment` MEDIUMTEXT NULL,
PRIMARY KEY (`id`),
UNIQUE (uid),
INDEX (`uid` ASC),
INDEX (`name` ASC),
INDEX (`parentUid` ASC),
INDEX (`parentId` ASC),
INDEX (`cid` ASC),
INDEX (`ownerId` ASC),
INDEX (`instanceId` ASC),
INDEX (`activityId` ASC),
INDEX (`type` ASC),
INDEX (`started` ASC),
INDEX (`ended` ASC),
CONSTRAINT `FK_wf_instance_activity_1` FOREIGN KEY (`activityId`) REFERENCES `wf_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `FK_wf_instance_activity_2` FOREIGN KEY (`instanceId`) REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/EndDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StartDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StandaloneDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/AswitchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/JoinDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/SplitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/TransitionDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `wf_transition` (
`uid` VARCHAR(128) NOT NULL,
`cid` VARCHAR(32) NOT NULL DEFAULT '56acc299ed150',
`processId` INT(11) NOT NULL DEFAULT '0',
`parentId` INT(11) NOT NULL DEFAULT '0',
`parentUid` VARCHAR(32) NOT NULL,
`childId` INT(11) NOT NULL DEFAULT '0',
`childUid` VARCHAR(32) NOT NULL,
`name` VARCHAR(128) DEFAULT NULL,
`attributes` MEDIUMTEXT,
`lindex` INT(7) DEFAULT '0',
PRIMARY KEY (`parentId`,`childId`),
UNIQUE KEY `UC_uid` (`uid`),
KEY `K_parentUid` (`parentUid`),
KEY `K_childUid` (`childUid`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ProcessDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `wf_process` (
`id` INT(11) NOT NULL,
`uid` VARCHAR(128)  NOT NULL,
`cid` VARCHAR(64) NOT NULL DEFAULT '56acc299ecd6d',
`name` VARCHAR(128) DEFAULT NULL,
`title` VARCHAR(256) DEFAULT NULL,
`ownerId` VARCHAR(255) DEFAULT NULL,
`updated` DATETIME DEFAULT NULL,
`updateById` VARCHAR(128) DEFAULT NULL,
`parentId` INT(11) DEFAULT NULL,
`parentUid` VARCHAR(128) DEFAULT NULL,
`version` VARCHAR(32) DEFAULT NULL,
`normalizedName` VARCHAR(256) DEFAULT NULL,
`isValid` TINYINT(1) DEFAULT 0,
`isActive` TINYINT(1) DEFAULT 0,
PRIMARY KEY (`id`),
UNIQUE (uid),
UNIQUE (normalizedName),
INDEX (`uid` ASC),
INDEX (`cid` ASC),
INDEX (`name` ASC),
INDEX (`parentUid` ASC),
INDEX (`parentId` ASC),
INDEX (`ownerId` ASC),
INDEX (`isValid` ASC),
INDEX (`isActive` ASC)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `wf_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO `wf_seq` (`id`) VALUES (100);

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ActivityDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `wf_activity`(
`id` int(11) NOT NULL,
`uid` VARCHAR(64)  NOT NULL,
`cid` VARCHAR(64) NOT NULL DEFAULT '56acc299ed0e4',
`name` VARCHAR(128) DEFAULT NULL,
`title` TEXT DEFAULT NULL,
`ownerId` VARCHAR(255) DEFAULT NULL,
`updated` DATETIME DEFAULT NULL,
`updateById` VARCHAR(64) DEFAULT NULL,
`parentId` INT(11) DEFAULT NULL,
`parentUid` VARCHAR(255) DEFAULT NULL,
`type` ENUM('start','end','split','switch','join','activity','standalone') DEFAULT NULL,
`normalizedName` VARCHAR(256) NULL,
`isInteractive` TINYINT(1) default 0,
`isAutorouted` TINYINT(1) default 0,
`isAutomatic` TINYINT(1) default 0,
`isComment` TINYINT(1) default 0,
`processId` INT(11) NULL,
`progression` float NULL,
`expirationTime` DATETIME NULL,
`roles` MEDIUMTEXT NULL,
`attributes` MEDIUMTEXT NULL,
PRIMARY KEY (`id`),
UNIQUE (uid),
UNIQUE (`name` ,`processId`),
INDEX (`normalizedName` ASC),
INDEX (`uid` ASC),
INDEX (`name` ASC),
INDEX (`parentUid` ASC),
INDEX (`parentId` ASC),
INDEX (`cid` ASC),
INDEX (`ownerId` ASC),
INDEX (`processId` ASC),
INDEX (roles(100) ASC)
);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/InstanceDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `wf_instance`(
`id` INT(11) NOT NULL,
`uid` VARCHAR(255) NOT NULL,
`cid` VARCHAR(32) NOT NULL DEFAULT '56acc299ed1bd',
`name` VARCHAR(255) NULL,
`title` VARCHAR(255) NULL,
`ownerId` VARCHAR(255) NULL,
`parentId` INT(11) NULL,
`parentUid` VARCHAR(255) NULL,
`updateById` VARCHAR(255) NULL,
`updated` DATETIME NULL,
`processId` INT(11) NULL,
`status` VARCHAR(64) NULL,
`nextActivities` MEDIUMTEXT NULL,
`nextUsers` MEDIUMTEXT NULL,
`attributes` MEDIUMTEXT NULL,
`started` DATETIME NULL,
`ended` DATETIME NULL,
PRIMARY KEY (`id`),
UNIQUE (uid),
INDEX (`name` ASC),
INDEX (`parentUid` ASC),
INDEX (`parentId` ASC),
INDEX (`cid` ASC),
INDEX (`ownerId` ASC),
INDEX (`processId` ASC),
INDEX (nextActivities(100) ASC),
INDEX (nextUsers(100) ASC),
INDEX (`started` ASC),
INDEX (`ended` ASC)
);

CREATE TABLE IF NOT EXISTS `wf_instance_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO `wf_instance_seq` (`id`) VALUES (100);

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CategoryDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) NOT NULL,
`uid` VARCHAR(32) NOT NULL,
`cid` CHAR(13) NOT NULL DEFAULT '569e918a134ca',
`dn` VARCHAR(128) NULL,
`name` VARCHAR(512) DEFAULT NULL,
`nodelabel` VARCHAR(64) DEFAULT NULL,
`designation` text,
`parent_id` int(11) NULL,
`parent_uid` VARCHAR(32) NULL,
`icon` VARCHAR(32) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `U_categories_uid` (`uid`),
UNIQUE KEY `U_categories_dn` (`dn`),
KEY `I_categories_nodelabel` (`nodelabel`),
KEY `I_categories_name` (`name`),
KEY `I_categories_parent_id` (`parent_id`),
KEY `I_categories_parent_uid` (`parent_uid`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `categories_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
INSERT INTO `categories_seq` (`id`) VALUES(100);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/RoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CheckoutIndexDao.php
-- **********************************************************************

CREATE TABLE IF NOT EXISTS `checkout_index` (
`file_name` VARCHAR(128) NOT NULL,
`lock_by_id` int(11) NULL,
`lock_by_uid` VARCHAR(64) NULL,
`file_id` int(11) NULL,
`file_uid` VARCHAR(140) NULL,
`designation` VARCHAR(128) NULL,
`spacename` VARCHAR(128) NULL,
`container_id` int(11) NULL,
`container_number` VARCHAR(128) NULL,
`document_id` int(11) NULL,
PRIMARY KEY (`file_name`),
KEY `IK_checkout_index_1` (`lock_by_id`),
KEY `IK_checkout_index_2` (`lock_by_uid`),
KEY `IK_checkout_index_3` (`file_id`),
KEY `IK_checkout_index_4` (`file_uid`),
KEY `IK_checkout_index_5` (`spacename`),
KEY `IK_checkout_index_6` (`container_id`),
KEY `IK_checkout_index_7` (`container_number`),
KEY `IK_checkout_index_8` (`document_id`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DoctypeDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `doctypes` (
`id` INT(11) NOT NULL,
`uid` VARCHAR(64) NOT NULL,
`number` VARCHAR(128) NOT NULL,
`name` VARCHAR(128) NOT NULL DEFAULT '',
`designation` MEDIUMTEXT DEFAULT NULL,
`can_be_composite` BOOLEAN NOT NULL DEFAULT '1',
`file_extensions` MEDIUMTEXT DEFAULT NULL,
`file_type` VARCHAR(128) DEFAULT NULL,
`priority` INT(5) NOT NULL DEFAULT 0,
`icon` VARCHAR(32) DEFAULT NULL,
`pre_store_class` VARCHAR(128) DEFAULT NULL,
`pre_store_method` VARCHAR(128) DEFAULT NULL,
`post_store_class` VARCHAR(128) DEFAULT NULL,
`post_store_method` VARCHAR(128) DEFAULT NULL,
`pre_update_class` VARCHAR(128) DEFAULT NULL,
`pre_update_method` VARCHAR(128) DEFAULT NULL,
`post_update_class` VARCHAR(128) DEFAULT NULL,
`post_update_method` VARCHAR(128) DEFAULT NULL,
`number_generator_class` VARCHAR(128) DEFAULT NULL,
`recognition_regexp` MEDIUMTEXT DEFAULT NULL,
`visu_file_extension` VARCHAR(32) DEFAULT NULL,
`domain` VARCHAR(64) DEFAULT NULL,
`template_id` INT(11) DEFAULT NULL COMMENT 'id of the template docfile',
`template_uid` VARCHAR(64) DEFAULT NULL COMMENT 'uid of the template docfile',
`mask_id` INT(11) DEFAULT NULL COMMENT 'id of the mask to use',
`mask_uid` VARCHAR(64) DEFAULT NULL COMMENT 'uid of the mask',
`docseeder` BOOLEAN COMMENT 'flag to retrieve the valid type for docseeder',
PRIMARY KEY (`id`),
UNIQUE KEY (`uid`),
UNIQUE KEY (`number`),
INDEX (`template_id`),
INDEX (`template_uid`),
INDEX (`mask_id`),
INDEX (`mask_uid`),
INDEX (`domain`),
INDEX (`docseeder`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `doctypes_seq`(
`id` INT(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO doctypes_seq(id) VALUES(500);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocfileDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocumentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/PdmLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/BasketDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `docbasket` (
`user_id` INT(11) NOT NULL,
`document_id` INT(11) NOT NULL,
`space_id` INT(11) NOT NULL,
PRIMARY KEY (`user_id`, `document_id`, `space_id`)
) ENGINE = MyISAM DEFAULT CHARACTER SET = utf8;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/IndiceDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `document_indice` (
`indice_id` int(11) NOT NULL DEFAULT '0',
`indice_value` VARCHAR(8) NOT NULL DEFAULT '',
PRIMARY KEY (`indice_id`),
UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/LinkDao.php
-- **********************************************************************

CREATE TABLE IF NOT EXISTS `workitem_doc_rel` (
`link_id` INT(11) NOT NULL,
`name` VARCHAR(256) default NULL,
`parent_id` INT(11) default NULL,
`parent_uid` VARCHAR(64) default NULL,
`parent_space_id` INT(11) default NULL,
`child_id` INT(11) default NULL,
`child_uid` VARCHAR(64) default NULL,
`child_space_id` INT(11) default NULL,
`acode` INT(11) default NULL,
`lindex` INT(11) default 0,
hash CHAR(32) default NULL,
data TEXT default NULL,
PRIMARY KEY (`link_id`),
UNIQUE KEY (`parent_id`,`child_id`),
UNIQUE KEY (`parent_id`,`name`),
INDEX (`lindex`),
INDEX (`parent_uid`),
INDEX (`child_uid`),
INDEX (`parent_id`, `parent_space_id`),
INDEX (`child_id`, `child_space_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `document_rel_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `document_rel_seq` (`id`) VALUES (10);

CREATE TABLE IF NOT EXISTS `bookshop_doc_rel` LIKE `workitem_doc_rel`;
CREATE TABLE IF NOT EXISTS `cadlib_doc_rel` LIKE `workitem_doc_rel`;
CREATE TABLE IF NOT EXISTS `mockup_doc_rel` LIKE `workitem_doc_rel`;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/Share/PublicUrlDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `share_public_url` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`uid` VARCHAR(255) NOT NULL,
`document_id` int(11) DEFAULT NULL,
`spacename` VARCHAR(32) DEFAULT NULL,
`document_uid` VARCHAR(255) DEFAULT NULL,
`expiration_date` datetime DEFAULT NULL,
`comment` text,
PRIMARY KEY (`id`),
UNIQUE KEY `UC_share_public_url_uid` (`uid`),
KEY `K_share_public_url_document_id` (`document_id`),
KEY `K_share_public_url_spacename` (`spacename`),
KEY `K_share_public_url_document_uid` (`document_uid`),
KEY `K_share_public_url_expiration_date` (`expiration_date`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/SpaceDao.php
-- **********************************************************************

CREATE TABLE IF NOT EXISTS `spaces` (
`id` int(11) NOT NULL,
`uid` VARCHAR(64) NOT NULL DEFAULT '',
`cid` VARCHAR(64) NOT NULL DEFAULT '64bdd156af045',
`name` VARCHAR(255) NOT NULL DEFAULT '',
`designation` VARCHAR(256) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `UC_spaces_uid` (`uid`),
KEY `K_spaces_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/CategoryDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitem_categories` (
`id` int(11) NOT NULL DEFAULT '0',
`uid` VARCHAR(32) NOT NULL,
`cid` CHAR(13) NOT NULL DEFAULT '569e918a134ca',
`dn` VARCHAR(128) NULL,
`number` VARCHAR(128) DEFAULT NULL,
`designation` text,
`parent_id` int(11) NULL,
`parent_uid` VARCHAR(32) NULL,
`icon` VARCHAR(32) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`uid`),
UNIQUE KEY (`number`),
UNIQUE KEY (`dn`),
KEY (`parent_id`),
KEY (`parent_uid`)
) ENGINE=InnoDB;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/IterationDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitem_doc_files_versions` LIKE `workitem_doc_files`;
CREATE TABLE IF NOT EXISTS `bookshop_doc_files_versions` LIKE `bookshop_doc_files`;
CREATE TABLE IF NOT EXISTS `cadlib_doc_files_versions` LIKE `cadlib_doc_files`;
CREATE TABLE IF NOT EXISTS `mockup_doc_files_versions` LIKE `mockup_doc_files`;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/VersionDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitem_doc_files` (
`id` int(11) NOT NULL,
`uid` VARCHAR(140) NOT NULL UNIQUE,
`cid` VARCHAR(16) NOT NULL DEFAULT '569e92b86d248',
`document_id` INT(11) NOT NULL,
`document_uid` VARCHAR(64) NULL,
`name` VARCHAR(255) NOT NULL,
`file_used_name` VARCHAR(128) NULL DEFAULT NULL,
`path` MEDIUMTEXT NOT NULL,
`iteration` INT(11) NOT NULL DEFAULT '1',
`acode` INT(11) NOT NULL DEFAULT '0',
`root_name` VARCHAR(255) NOT NULL,
`extension` VARCHAR(16) DEFAULT NULL,
`life_stage` VARCHAR(64) NOT NULL DEFAULT 'init',
`type` VARCHAR(128) NOT NULL,
`size` INT(11) DEFAULT NULL,
`mtime` DATETIME DEFAULT NULL,
`md5` VARCHAR(128) DEFAULT NULL,
`locked` DATETIME DEFAULT NULL,
`lock_by_id` INT(11) DEFAULT NULL,
`lock_by_uid` VARCHAR(64) DEFAULT NULL,
`created` DATETIME NOT NULL,
`create_by_id` INT(11) NOT NULL,
`create_by_uid` VARCHAR(64) DEFAULT NULL,
`updated` DATETIME DEFAULT NULL,
`update_by_id` INT(11) DEFAULT NULL,
`update_by_uid` VARCHAR(64) DEFAULT NULL,
`mainrole` INT(2) NOT NULL DEFAULT 1,
`roles` TEXT DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`name`,`path`(512)),
UNIQUE KEY (`name`,`path`(128)),
KEY (`document_id`),
KEY (`name`),
KEY (`path`(128)),
KEY (`iteration`,`document_id`),
KEY (`mainrole`),
KEY (`roles`(32)),
KEY (`create_by_uid`),
KEY (`lock_by_uid`),
KEY (`update_by_uid`),
FULLTEXT INDEX (`name` ASC)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bookshop_doc_files` LIKE `workitem_doc_files`;
CREATE TABLE IF NOT EXISTS `cadlib_doc_files` LIKE `workitem_doc_files`;
CREATE TABLE IF NOT EXISTS `mockup_doc_files` LIKE `workitem_doc_files`;

CREATE TABLE IF NOT EXISTS `workitem_doc_files_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `workitem_doc_files_seq` (`id`) VALUES ('10');

CREATE TABLE IF NOT EXISTS `bookshop_doc_files_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `bookshop_doc_files_seq` (`id`) VALUES ('10');

CREATE TABLE IF NOT EXISTS `cadlib_doc_files_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `cadlib_doc_files_seq` (`id`) VALUES ('10');

CREATE TABLE IF NOT EXISTS `mockup_doc_files_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `mockup_doc_files_seq` (`id`) VALUES ('10');

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/HistoryDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitem_history` (
`histo_order` int(11) NOT NULL DEFAULT '0',
`action_name` VARCHAR(32)  DEFAULT NULL,
`action_by` VARCHAR(32)  DEFAULT NULL,
`action_started` int(11) DEFAULT NULL,
`container_id` int(11) NOT NULL DEFAULT '0',
`number` VARCHAR(128)  DEFAULT NULL,
`life_stage` VARCHAR(16)  DEFAULT NULL,
`description` VARCHAR(256)  DEFAULT NULL,
`indice_id` int(11) DEFAULT NULL,
`project_id` int(11) DEFAULT NULL,
`default_file_path` text,
`created` int(11) DEFAULT NULL,
`open_by` int(11) DEFAULT NULL,
`planned_closure` int(11) DEFAULT NULL,
`closed` int(11) DEFAULT NULL,
`close_by` int(11) DEFAULT NULL,
PRIMARY KEY (`histo_order`)
);

CREATE TABLE IF NOT EXISTS `workitem_history_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
INSERT INTO `workitem_history_seq` (`id`) VALUES(100);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/CategoryDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitem_category_rel` LIKE `project_category_rel`;
CREATE TABLE IF NOT EXISTS `bookshop_category_rel` LIKE `project_category_rel`;
CREATE TABLE IF NOT EXISTS `cadlib_category_rel` LIKE `project_category_rel`;
CREATE TABLE IF NOT EXISTS `mockup_category_rel` LIKE `project_category_rel`;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/ProcessDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitem_process_rel` LIKE `project_process_rel`;
CREATE TABLE IF NOT EXISTS `bookshop_process_rel` LIKE `project_process_rel`;
CREATE TABLE IF NOT EXISTS `cadlib_process_rel` LIKE `project_process_rel`;
CREATE TABLE IF NOT EXISTS `mockup_process_rel` LIKE `project_process_rel`;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/DoctypeDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitem_doctype_rel` LIKE `project_doctype_rel`;
CREATE TABLE IF NOT EXISTS `bookshop_doctype_rel` LIKE `project_doctype_rel`;
CREATE TABLE IF NOT EXISTS `cadlib_doctype_rel` LIKE `project_doctype_rel`;
CREATE TABLE IF NOT EXISTS `mockup_doctype_rel` LIKE `project_doctype_rel`;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/PropertyDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitem_property_rel` LIKE `project_property_rel`;
CREATE TABLE IF NOT EXISTS `bookshop_property_rel` LIKE `project_property_rel`;
CREATE TABLE IF NOT EXISTS `cadlib_property_rel` LIKE `project_property_rel`;
CREATE TABLE IF NOT EXISTS `mockup_property_rel` LIKE `project_property_rel`;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/PropertyDao.php
-- **********************************************************************


CREATE TABLE `workitem_metadata` (
`id` int(11) NOT NULL,
`uid` varchar(64) DEFAULT NULL,
`field_name` varchar(32) NOT NULL DEFAULT '',
`extendedCid` varchar(32) DEFAULT NULL,
`field_description` varchar(128) NOT NULL DEFAULT '',
`field_type` varchar(32) NOT NULL DEFAULT '',
`field_regex` varchar(255) DEFAULT NULL,
`field_required` int(1) NOT NULL DEFAULT '0',
`field_multiple` int(1) NOT NULL DEFAULT '0',
`maybenull` int(1) NOT NULL DEFAULT '0',
`field_size` int(11) DEFAULT NULL,
`return_name` int(1) NOT NULL DEFAULT '0',
`field_list` varchar(255) DEFAULT NULL,
`field_where` varchar(255) DEFAULT NULL,
`is_hide` int(1) NOT NULL DEFAULT '0',
`table_name` varchar(32) DEFAULT NULL,
`field_for_value` varchar(32) DEFAULT NULL,
`field_for_display` varchar(32) DEFAULT NULL,
`date_format` varchar(32) DEFAULT NULL,
`adv_select` tinyint(1) NOT NULL DEFAULT '0',
`appname` varchar(128) DEFAULT NULL,
`getter` varchar(128) DEFAULT NULL,
`setter` varchar(128) DEFAULT NULL,
`label` varchar(255) DEFAULT NULL,
`min` decimal(10,0) DEFAULT NULL,
`max` decimal(10,0) DEFAULT NULL,
`step` decimal(10,0) DEFAULT NULL,
`dbfilter` mediumtext,
`dbquery` mediumtext,
`attributes` mediumtext,
PRIMARY KEY (`id`),
UNIQUE KEY (`field_name`,`extendedCid`),
KEY  (`extendedCid`),
KEY  (`field_name`),
KEY  (`appname`),
KEY  (`label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `bookshop_metadata` LIKE `workitem_metadata`;
CREATE TABLE IF NOT EXISTS `cadlib_metadata` LIKE `workitem_metadata`;
CREATE TABLE IF NOT EXISTS `mockup_metadata` LIKE `workitem_metadata`;

CREATE TABLE IF NOT EXISTS `workitem_metadata_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `workitem_metadata_seq` (`id`) VALUES (10);

CREATE TABLE IF NOT EXISTS `bookshop_metadata_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `bookshop_metadata_seq` (`id`) VALUES (10);

CREATE TABLE IF NOT EXISTS `cadlib_metadata_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `cadlib_metadata_seq` (`id`) VALUES (10);

CREATE TABLE IF NOT EXISTS `mockup_metadata_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO `mockup_metadata_seq` (`id`) VALUES (10);

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/HistoryDao.php
-- **********************************************************************

CREATE TABLE IF NOT EXISTS `workitem_documents_history` (
`histo_order` INT(11) NOT NULL,
`action_name` VARCHAR(32) NOT NULL,
`action_by` VARCHAR(64) NOT NULL,
`action_started` DATETIME NOT NULL,
`action_batch_uid` VARCHAR(128),
`action_comment` MEDIUMTEXT,
`document_id` INT(11) DEFAULT NULL,
`number` VARCHAR(128) DEFAULT NULL,
`life_stage` VARCHAR(32) DEFAULT NULL,
`iteration` INT(11) DEFAULT NULL,
`version` INT(11) DEFAULT NULL,
`designation` VARCHAR(128) DEFAULT NULL,
`data` MEDIUMTEXT,
PRIMARY KEY (`histo_order`),
INDEX (`document_id` ASC),
INDEX (`action_batch_uid` ASC)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bookshop_documents_history` LIKE `workitem_documents_history`;
CREATE TABLE IF NOT EXISTS `cadlib_documents_history` LIKE `workitem_documents_history`;
CREATE TABLE IF NOT EXISTS `mockup_documents_history` LIKE `workitem_documents_history`;

CREATE TABLE IF NOT EXISTS `workitem_documents_history_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
INSERT INTO `workitem_documents_history_seq` (`id`) VALUES(100);

CREATE TABLE IF NOT EXISTS `bookshop_documents_history_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
INSERT INTO `bookshop_documents_history_seq` (`id`) VALUES(100);

CREATE TABLE IF NOT EXISTS `cadlib_documents_history_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
INSERT INTO `cadlib_documents_history_seq` (`id`) VALUES(100);

CREATE TABLE IF NOT EXISTS `mockup_documents_history_seq` (
`id` int(11) NOT NULL
) ENGINE=InnoDB ;
INSERT INTO `mockup_documents_history_seq` (`id`) VALUES (10);

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/VersionDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitem_documents` (
`id` INT(11) NOT NULL DEFAULT '0',
`uid` VARCHAR(140) NOT NULL UNIQUE,
`cid` VARCHAR(64) DEFAULT '569e924be82e7',
`number` VARCHAR(128)  NOT NULL DEFAULT '',
`name` VARCHAR(255)  DEFAULT NULL,
`designation` VARCHAR(512)  DEFAULT NULL,
`label` VARCHAR(255)  DEFAULT NULL,
`life_stage` VARCHAR(32)  NOT NULL DEFAULT 'init',
`acode` INT(11) NOT NULL DEFAULT '0',
`branch_id` INT(1) DEFAULT NULL,
`iteration` INT(11) NOT NULL DEFAULT '1',
`version` INT(5) DEFAULT NULL,
`container_id` INT(11) NOT NULL DEFAULT '0',
`container_uid` VARCHAR(128) NULL,
`spacename` VARCHAR(8)  NOT NULL DEFAULT 'workitem',
`doctype_id` INT(11) NOT NULL DEFAULT '0',
`default_process_id` INT(11) DEFAULT NULL,
`category_id` INT(11) DEFAULT NULL,
`tags` VARCHAR(256) DEFAULT NULL,
`from_document_id` INT(11) DEFAULT NULL,
`as_template` BOOLEAN DEFAULT FALSE,
`lock_by_id` INT(11) DEFAULT NULL,
`lock_by_uid` VARCHAR(64) DEFAULT NULL,
`locked` DATETIME DEFAULT NULL,
`updated` DATETIME DEFAULT NULL,
`update_by_id` INT(11) DEFAULT NULL,
`update_by_uid` VARCHAR(64) DEFAULT NULL,
`created` DATETIME DEFAULT NULL,
`create_by_id` INT(11) DEFAULT NULL,
`create_by_uid` VARCHAR(64) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`number`,`version`),
KEY (`uid`),
KEY (`number`),
KEY (`name`),
KEY (`doctype_id`),
KEY (`version`),
KEY (`container_id`),
KEY (`container_uid`),
KEY (`category_id`),
KEY (`create_by_uid`),
KEY (`lock_by_uid`),
KEY (`update_by_uid`),
KEY (`as_template`),
KEY (`tags`),
KEY (`branch_id`),
KEY (`label`),
KEY (`spacename`),
FULLTEXT (name, number, designation, label, tags)
);

CREATE TABLE IF NOT EXISTS `bookshop_documents` LIKE `workitem_documents`;
CREATE TABLE IF NOT EXISTS `cadlib_documents` LIKE `workitem_documents`;
CREATE TABLE IF NOT EXISTS `mockup_documents` LIKE `workitem_documents`;

CREATE TABLE IF NOT EXISTS `workitem_documents_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `workitem_documents_seq` (id) VALUES(500);

CREATE TABLE IF NOT EXISTS `bookshop_documents_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `bookshop_documents_seq` (id) VALUES(500);

CREATE TABLE IF NOT EXISTS `cadlib_documents_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `cadlib_documents_seq` (id) VALUES(500);

CREATE TABLE IF NOT EXISTS `mockup_documents_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `mockup_documents_seq` (id) VALUES(500);

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/AliasDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitem_alias` (
`alias_id` INT(11) NOT NULL ,
`uid` VARCHAR(128)  NOT NULL,
`container_id` INT(11) NOT NULL,
`number` VARCHAR(128)  NOT NULL,
`name` VARCHAR(128) NOT NULL,
`designation` VARCHAR(128)  default NULL,
`cid` VARCHAR(16)  NOT NULL default '45c84a5zalias',
`created` DATETIME DEFAULT NULL,
`create_by_id` INT(11) DEFAULT NULL,
`create_by_uid` VARCHAR(128) DEFAULT NULL,
PRIMARY KEY  (`alias_id`),
KEY (`uid`),
KEY (`container_id`),
KEY (`number`),
KEY (`cid`)
) ENGINE=InnoDB ;

CREATE TABLE IF NOT EXISTS `bookshop_alias` LIKE `workitem_alias`;
CREATE TABLE IF NOT EXISTS `cadlib_alias` LIKE `workitem_alias`;
CREATE TABLE IF NOT EXISTS `mockup_alias` LIKE `workitem_alias`;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/MessageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/ArchiveDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `message_archive` (
`id` int(14) NOT NULL,
`ownerUid` VARCHAR(128) NOT NULL,
`created` DATETIME DEFAULT NULL,
`from` VARCHAR(200) NOT NULL,
`to` VARCHAR(255),
`cc` VARCHAR(255),
`bcc` VARCHAR(255),
`subject` VARCHAR(255)  DEFAULT NULL,
`body` text,
`hash` VARCHAR(32)  DEFAULT NULL,
`replyto_hash` VARCHAR(32)  DEFAULT NULL,
`isRead` tinyint(1)  DEFAULT 0,
`isReplied` tinyint(1)  DEFAULT 0,
`isFlagged` tinyint(1)  DEFAULT 0,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `ownerUid` ),
INDEX ( `isRead` ),
INDEX ( `isReplied` ),
INDEX ( `isFlagged` ),
INDEX ( `subject` ),
FULLTEXT ( `body`,`subject` )
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/SentDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `message_sent` (
`id` int(14) NOT NULL,
`ownerUid` VARCHAR(128) NOT NULL,
`created` DATETIME DEFAULT NULL,
`from` VARCHAR(200) NOT NULL,
`to` VARCHAR(255),
`cc` VARCHAR(255),
`bcc` VARCHAR(255),
`subject` VARCHAR(255)  DEFAULT NULL,
`body` text,
`hash` VARCHAR(32)  DEFAULT NULL,
`replyto_hash` VARCHAR(32)  DEFAULT NULL,
`isRead` tinyint(1)  DEFAULT 0,
`isReplied` tinyint(1)  DEFAULT 0,
`isFlagged` tinyint(1)  DEFAULT 0,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `ownerUid` ),
INDEX ( `isRead` ),
INDEX ( `isReplied` ),
INDEX ( `isFlagged` ),
INDEX ( `subject` ),
FULLTEXT ( `body`,`subject` )
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/MailboxDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `message_mailbox` (
`id` int(14) NOT NULL,
`ownerUid` VARCHAR(128) NOT NULL,
`created` DATETIME DEFAULT NULL,
`from` VARCHAR(200) NOT NULL,
`to` VARCHAR(255),
`cc` VARCHAR(255),
`bcc` VARCHAR(255),
`subject` VARCHAR(255)  DEFAULT NULL,
`body` text,
`hash` VARCHAR(32)  DEFAULT NULL,
`replyto_hash` VARCHAR(32)  DEFAULT NULL,
`isRead` tinyint(1)  DEFAULT 0,
`isReplied` tinyint(1)  DEFAULT 0,
`isFlagged` tinyint(1)  DEFAULT 0,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `ownerUid` ),
INDEX ( `isRead` ),
INDEX ( `isReplied` ),
INDEX ( `isFlagged` ),
INDEX ( `subject` ),
FULLTEXT ( `body`,`subject` )
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `message_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO message_seq(id) VALUES(10);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/GroupDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `acl_group` (
`id` int(11) NOT NULL,
`uid` VARCHAR(30) NOT NULL,
`name` VARCHAR(128) DEFAULT NULL,
`description` VARCHAR(64) DEFAULT NULL,
`is_active` tinyint(1) DEFAULT '1',
`owner_id` int(11) DEFAULT NULL,
`owner_uid`  VARCHAR(64) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`uid`),
UNIQUE KEY (`name`),
KEY (`owner_id` ASC)
);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/UserDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `acl_user` (
`id` int(11) NOT NULL,
`uid` VARCHAR(128) NOT NULL,
`login` VARCHAR(128) NOT NULL,
`lastname` VARCHAR(128) DEFAULT NULL,
`firstname` VARCHAR(128) DEFAULT NULL,
`password` VARCHAR(128) DEFAULT NULL,
`mail` VARCHAR(128) DEFAULT NULL,
`owner_id` int(11) DEFAULT NULL,
`owner_uid` VARCHAR(64) DEFAULT NULL,
`primary_role_id` int(11) DEFAULT NULL,
`lastlogin` DATETIME DEFAULT NULL,
`is_active` tinyint(1) DEFAULT '1',
`auth_from` VARCHAR(16) DEFAULT 'db',
`conn_from` VARCHAR(16) DEFAULT 'auto',
`extends` JSON,
PRIMARY KEY (`id`),
UNIQUE KEY `login_idx` (`login`),
UNIQUE KEY `uid_idx` (`uid`)
);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/PreferenceDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `user_prefs` (
`id` int(11) AUTO_INCREMENT,
`owner_id` int(11) NOT NULL,
`css_sheet` VARCHAR(32) default NULL,
`lang` VARCHAR(32) default NULL,
`long_date_format` VARCHAR(32) default NULL,
`short_date_format` VARCHAR(32) default NULL,
`hour_format` VARCHAR(32) default NULL,
`time_zone` VARCHAR(32) default NULL,
`rbgateserver_url` VARCHAR(256) default NULL,
PRIMARY KEY (`id`),
UNIQUE (`owner_id`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/SessionDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `user_sessions` (
`user_id` int(11) NOT NULL,
`datas` LONGTEXT,
PRIMARY KEY (`user_id`)
) ENGINE=MyIsam;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/PasswordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/SomebodyDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `partners` (
`id` INT(11) NOT NULL,
`number` VARCHAR(128) NOT NULL DEFAULT '',
`type` enum('customer','supplier','staff') DEFAULT NULL,
`first_name` VARCHAR(64) DEFAULT NULL,
`last_name` VARCHAR(64) DEFAULT NULL,
`adress` VARCHAR(64) DEFAULT NULL,
`city` VARCHAR(64) DEFAULT NULL,
`zip_code` int(11) DEFAULT NULL,
`phone` VARCHAR(64) DEFAULT NULL,
`cell_phone` VARCHAR(64) DEFAULT NULL,
`mail` VARCHAR(64) DEFAULT NULL,
`web_site` VARCHAR(64) DEFAULT NULL,
`activity` VARCHAR(64) DEFAULT NULL,
`company` VARCHAR(64) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE (`number`),
INDEX (`type`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `partners_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
)ENGINE=MyIsam AUTO_INCREMENT=10;
INSERT INTO partners_seq(id) VALUES(10);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Tag/TagDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/ProductDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `pdm_product` (
`id` int(11) NOT NULL,
`uid` VARCHAR(32) NOT NULL,
`name` VARCHAR(128) NOT NULL,
`description` VARCHAR(512) NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `pdm_product_version_uniq1` (`uid`),
KEY `INDEX_pdm_product_version_3` (`description`),
KEY `INDEX_pdm_product_version_4` (`uid`),
KEY `INDEX_pdm_product_version_5` (`name`)
);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ConceptDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ContextDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `pdm_context_application` (
`id` int(11) NOT NULL,
`uid` VARCHAR(32) NOT NULL,
`cid` VARCHAR(32) NOT NULL DEFAULT '569e9714da9aa',
`name` VARCHAR(128) NULL,
PRIMARY KEY  (`id`),
UNIQUE KEY `INDEX_pdm_context_application_1` (`uid`),
KEY `INDEX_pdm_context_application_2` (`cid`),
KEY `INDEX_pdm_context_application_3` (`name`)
);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/InstanceDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `pdm_product_instance` (
`id` int(11) NOT NULL,
`uid` VARCHAR(32) NOT NULL,
`cid` VARCHAR(32) NOT NULL DEFAULT '569e971bb57c0',
`name` VARCHAR(128) NOT NULL,
`number` VARCHAR(128) NOT NULL,
`nomenclature` VARCHAR(128) NULL,
`description` VARCHAR(512) NULL,
`position` VARCHAR(512) NULL,
`quantity` int(11) NULL,
`parent_id` int(11) NULL,
`path` VARCHAR(256) NULL,
`of_product_id` int(11) NULL,
`of_product_uid` VARCHAR(32) NULL,
`of_product_number` VARCHAR(128) NULL,
`context_id` VARCHAR(32) NULL,
`type` VARCHAR(64) NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `U_pdm_product_instance_1` (`uid`),
UNIQUE KEY `U_pdm_product_instance_2` (`number`, `parent_id`),
KEY `INDEX_pdm_product_instance_1` (`cid`),
KEY `INDEX_pdm_product_instance_2` (`number`),
KEY `INDEX_pdm_product_instance_3` (`description`),
KEY `INDEX_pdm_product_instance_4` (`nomenclature`),
KEY `INDEX_pdm_product_instance_5` (`uid`),
KEY `INDEX_pdm_product_instance_6` (`name`),
KEY `INDEX_pdm_product_instance_7` (`type`),
KEY `INDEX_pdm_product_instance_8` (`of_product_uid`),
KEY `INDEX_pdm_product_instance_9` (`of_product_id`),
KEY `INDEX_pdm_product_instance_10` (`path`),
KEY `INDEX_pdm_product_version_12` (`of_product_number` ASC),
CONSTRAINT `FK_pdm_product_instance_1`
FOREIGN KEY (`of_product_id`)
REFERENCES `pdm_product_version` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE,
CONSTRAINT `FK_pdm_product_instance_2`
FOREIGN KEY (`parent_id`)
REFERENCES `pdm_product_version` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS `pdm_instance_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO pdm_instance_seq(id) VALUES(100);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/VersionDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `pdm_product_version` (
`id` int(11) NOT NULL,
`uid` VARCHAR(32) NOT NULL,
`name` VARCHAR(128) NOT NULL,
`number` VARCHAR(128) NOT NULL,
`description` VARCHAR(512) NULL,
`version` int(11) NOT NULL DEFAULT 1,
`of_product_id` int(11) NULL,
`of_product_uid` VARCHAR(32) NULL,
`document_id` int(11) NULL,
`document_uid` VARCHAR(32) NULL,
`lock_by_id` int(11) DEFAULT NULL,
`lock_by_uid` VARCHAR(64) DEFAULT NULL,
`locked` DATETIME DEFAULT NULL,
`updated` DATETIME DEFAULT NULL,
`update_by_id` int(11) DEFAULT NULL,
`update_by_uid` VARCHAR(64) DEFAULT NULL,
`created` DATETIME DEFAULT NULL,
`create_by_id` int(11) DEFAULT NULL,
`create_by_uid` VARCHAR(64) DEFAULT NULL,
`spacename` VARCHAR(32) NULL,
`type` VARCHAR(64) NULL,
`materials` VARCHAR(512) NULL,
`weight` float NULL,
`volume` float NULL,
`wetsurface` float NULL,
`density` float NULL,
`gravitycenter` VARCHAR(512) NULL,
`inertiacenter` VARCHAR(512) NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `U_pdm_product_version_1` (`uid`),
UNIQUE KEY `U_pdm_product_version_2` (`number`),
UNIQUE KEY `U_pdm_product_version_3` (`of_product_uid`,`version`),
KEY `INDEX_pdm_product_version_1` (`of_product_uid`),
KEY `INDEX_pdm_product_version_6` (`of_product_id`),
KEY `INDEX_pdm_product_version_2` (`version`),
KEY `INDEX_pdm_product_version_3` (`description`),
KEY `INDEX_pdm_product_version_4` (`uid`),
KEY `INDEX_pdm_product_version_5` (`name`),
KEY `INDEX_pdm_product_version_7` (`document_id`),
KEY `INDEX_pdm_product_version_8` (`document_uid`),
KEY `INDEX_pdm_product_version_9` (`spacename`),
KEY `INDEX_pdm_product_version_10` (`type`),
KEY `INDEX_pdm_product_version_11` (`create_by_uid`),
KEY `INDEX_pdm_product_version_12` (`lock_by_uid`),
KEY `INDEX_pdm_product_version_13` (`update_by_uid`),
KEY `INDEX_pdm_product_version_14` (`created`),
KEY `INDEX_pdm_product_version_15` (`locked`),
KEY `INDEX_pdm_product_version_16` (`updated`),
FULLTEXT INDEX `FT_name` (`name` ASC, `number` ASC, `description` ASC)
);

CREATE TABLE IF NOT EXISTS `pdm_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO pdm_seq(id) VALUES(100);

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/UsageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Observers/CallbackFactoryDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `callbacks` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`uid` VARCHAR (128) NOT NULL,
`cid` VARCHAR (32) NOT NULL,
`name` VARCHAR (128) NULL,
`description` VARCHAR (512) NULL,
`is_actif` INT(1) DEFAULT 1,
`reference_id` VARCHAR (128) NULL,
`reference_uid` VARCHAR (128) NULL,
`reference_cid` VARCHAR (128) NULL,
`spacename` VARCHAR (64) NULL,
`events` VARCHAR (256) NULL,
`callback_class` VARCHAR (256) NULL,
`callback_method` VARCHAR (256) NULL,
PRIMARY KEY (`id`),
KEY `INDEX_callbacks_1` (`uid`),
KEY `INDEX_callbacks_2` (`cid`),
KEY `INDEX_callbacks_3` (`reference_id`),
KEY `INDEX_callbacks_4` (`reference_uid`),
KEY `INDEX_callbacks_5` (`reference_cid`),
KEY `INDEX_callbacks_8` (`events`),
UNIQUE KEY `UNIQ_callbacks_1` (`uid`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/JobDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `batchjob` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`name` VARCHAR(258) NULL,
`callback_class` TEXT NULL,
`callback_method` VARCHAR(64) NULL,
`callback_params` TEXT NULL,
`owner_id` INT(11) NULL,
`owner_uid` VARCHAR(128) NULL,
`status` VARCHAR(32) NULL,
`planned` DATETIME NULL,
`created` DATETIME NULL,
`started` DOUBLE NULL,
`ended` DOUBLE NULL,
`duration` FLOAT NULL,
`hashkey` VARCHAR(40) NOT NULL,
`pid` VARCHAR(32) NULL,
`sapi` VARCHAR(32) NULL,
`log` LONGBLOB NULL,
PRIMARY KEY (`id`),
INDEX `K_batchjob_1` (`planned`),
INDEX `K_batchjob_2` (`status`),
INDEX `K_batchjob_3` (`name`),
UNIQUE `U_batchjob_4` (`hashkey`)
) ENGINE=MyIsam;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/CronTaskDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `cron_task` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`name` VARCHAR(258) NULL,
`description` VARCHAR(258) NULL,
`callback_class` TEXT NULL,
`callback_method` VARCHAR(64) NULL,
`callback_params` TEXT NULL,
`owner_id` INT(11) NULL,
`owner_uid` VARCHAR(128) NULL,
`status` VARCHAR(32) NULL,
`created` DATETIME NULL,
`exec_frequency` DECIMAL NULL,
`exec_schedule_start` DECIMAL NULL,
`exec_schedule_end` DECIMAL NULL,
`last_exec` DATETIME NULL,
`is_actif` tinyint DEFAULT 1,
PRIMARY KEY (`id`),
INDEX `K_cron_task_1` (`name`),
INDEX `K_cron_task_2` (`owner_id`),
INDEX `K_cron_task_3` (`owner_uid`),
INDEX `K_cron_task_4` (`status`),
INDEX `K_cron_task_5` (`created`),
INDEX `K_cron_task_6` (`exec_schedule_start`),
INDEX `K_cron_task_7` (`exec_schedule_end`),
INDEX `K_cron_task_8` (`exec_frequency`),
INDEX `K_cron_task_9` (`is_actif`)
) ENGINE=MyIsam;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/BatchDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `batch` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`uid` VARCHAR(258) NULL,
`owner_id` INT(11) NULL,
`owner_uid` VARCHAR(128) NULL,
`callback_class` TEXT NULL,
`callback_method` VARCHAR(64) NULL,
`callback_params` TEXT NULL,
`comment` TEXT NULL,
PRIMARY KEY (`id`),
UNIQUE (`uid`),
INDEX (`owner_id`),
INDEX (`owner_uid`)
) ENGINE=MyIsam;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RepositDao.php
-- **********************************************************************

CREATE TABLE IF NOT EXISTS `vault_reposit` (
`id` int(11) NOT NULL,
`uid` VARCHAR(64) NOT NULL,
`number` VARCHAR(255) NOT NULL,
`name` VARCHAR(255) NOT NULL,
`description` VARCHAR(256) DEFAULT NULL,
`default_file_path` VARCHAR(512) NOT NULL,
`spacename` VARCHAR(64) NOT NULL DEFAULT 'default',
`type` int(2) NOT NULL,
`mode` int(2) NOT NULL,
`actif` int(2) NOT NULL DEFAULT 1,
`priority` int(2) NOT NULL DEFAULT 1,
`maxsize` int(11) NOT NULL DEFAULT 50000000,
`maxcount` int(11) NOT NULL DEFAULT 50000,
`created` DATETIME DEFAULT NULL,
`create_by_id` int(11) DEFAULT NULL,
`create_by_uid` VARCHAR(64) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `U_vault_reposit_1` (`uid`),
UNIQUE KEY `U_vault_reposit_2` (`number`),
UNIQUE KEY `U_vault_reposit_3` (`default_file_path`),
KEY `FK_vault_reposit_8` (`spacename`),
KEY `FK_vault_reposit_1` (`name`),
KEY `FK_vault_reposit_2` (`description`),
KEY `FK_vault_reposit_4` (`type`),
KEY `FK_vault_reposit_5` (`mode`),
KEY `FK_vault_reposit_6` (`actif`),
KEY `FK_vault_reposit_7` (`priority`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `vault_reposit_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `vault_reposit_seq` (id) VALUES(500);

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/DistantSiteDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `vault_distant_site` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`uid` VARCHAR(32) NOT NULL,
`cid` CHAR(13) NOT NULL DEFAULT 'distantsite36',
`name` VARCHAR(512) DEFAULT NULL,
`description` text,
`method` VARCHAR(32),
`parameters` text NULL,
`auth_user` VARCHAR(128),
`auth_password` VARCHAR(64),
PRIMARY KEY (`id`),
UNIQUE KEY `UC_vault_distant_site_uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/ReplicatedDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `vault_replicated` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`uid` VARCHAR(32) NOT NULL,
`cid` CHAR(13) NOT NULL DEFAULT 'repositsynchr',
`name` VARCHAR(512) DEFAULT NULL,
`description` text,
`reposit_id` int(11) NULL,
`reposit_uid` VARCHAR(32) NULL,
`distantsite_id` int(11) NULL,
`distantsite_uid` VARCHAR(32) NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `UC_vault_replicated_uid` (`uid`),
KEY `K_vault_replicated_name_1` (`reposit_id`),
KEY `K_vault_replicated_name_2` (`reposit_uid`),
KEY `K_vault_replicated_name_3` (`distantsite_id`),
KEY `K_vault_replicated_name_4` (`distantsite_uid`),
INDEX `K_vault_replicated_1_idx` (`distantsite_id` ASC, `distantsite_uid` ASC),
INDEX `K_vault_replicated_2_idx` (`reposit_id` ASC, `reposit_uid` ASC)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/MissingFileDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `tools_missing_files` (
`id` int(11) NOT NULL,
`uid` VARCHAR(140) NOT NULL UNIQUE,
`document_id` int(11) NOT NULL,
`path` text NOT NULL,
`name` VARCHAR(128) NOT NULL,
`spacename` VARCHAR(64) NULL,
UNIQUE KEY `U_tools_missing_files_1` (`name`,`path`(128)),
KEY `IK_tools_missing_files_2` (`name`),
KEY `IK_tools_missing_files_3` (`path`(128)),
KEY `IK_tools_missing_files_4` (`spacename`)
) ENGINE=MyIsam;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RecordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/BookshopDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/CadlibDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/MockupDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/WorkitemDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `workitems` (
`id` INT(11) NOT NULL DEFAULT '0',
`uid` VARCHAR( 128 ) NOT NULL DEFAULT '',
`cid` VARCHAR(16) NOT NULL DEFAULT '569e94192201a',
`alias_id` INT(11) NULL DEFAULT NULL,
`object_class` VARCHAR(16) NOT NULL DEFAULT 'workitem',
`dn` VARCHAR(128) NULL,
`name` VARCHAR(128) NOT NULL DEFAULT '',
`number` VARCHAR(128) NOT NULL DEFAULT '',
`designation` TEXT DEFAULT NULL,
`file_only` TINYINT(1) NOT NULL DEFAULT '0',
`life_stage` VARCHAR(16) NOT NULL DEFAULT 'init',
`acode` INT(11) DEFAULT NULL,
`parent_id` INT(11) DEFAULT NULL,
`parent_uid` VARCHAR(128) DEFAULT NULL,
`default_process_id` INT(11) DEFAULT NULL,
`created` DATETIME DEFAULT NULL,
`create_by_id` INT(11) DEFAULT NULL,
`create_by_uid` VARCHAR(64) DEFAULT NULL,
`closed` DATETIME DEFAULT NULL,
`close_by_id` INT(11) DEFAULT NULL,
`close_by_uid` VARCHAR(64) DEFAULT NULL,
`planned_closure` DATETIME DEFAULT NULL,
`spacename` VARCHAR(16) NOT NULL DEFAULT 'workitem',
`default_file_path` MEDIUMTEXT NOT NULL,
`reposit_id` INT(11) DEFAULT NULL,
`reposit_uid` VARCHAR(128) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`number`),
UNIQUE KEY (`uid`),
KEY (`create_by_uid`),
KEY (`close_by_uid`),
KEY (`parent_id`),
KEY (`default_process_id`),
KEY (`name`),
KEY (`dn`),
KEY (`reposit_uid`),
KEY (`reposit_id`),
FULLTEXT KEY `FT_1` (`number`,`name`,`designation`),
CONSTRAINT `FK_workitems_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
CONSTRAINT `FK_workitems_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
CONSTRAINT `FK_workitems_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/ProjectDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `projects` (
`id` int(11) NOT NULL,
`uid` VARCHAR(140) NOT NULL,
`cid` VARCHAR(64) NOT NULL DEFAULT '569e93c6ee156',
`dn` VARCHAR(128) NULL,
`number` VARCHAR(128) NOT NULL,
`name` VARCHAR(128) NOT NULL DEFAULT '',
`designation` TEXT DEFAULT NULL,
`parent_id` int(11) NULL,
`parent_uid` VARCHAR(64) NULL,
`life_stage` VARCHAR(32) NOT NULL DEFAULT 'init',
`acode` int(11) DEFAULT NULL,
`default_process_id` int(11) DEFAULT NULL,
`created` DATETIME DEFAULT NULL,
`create_by_id` int(11) DEFAULT NULL,
`create_by_uid` VARCHAR(64) DEFAULT NULL,
`closed` DATETIME DEFAULT NULL,
`close_by_id` int(11) DEFAULT NULL,
`close_by_uid` VARCHAR(64) DEFAULT NULL,
`planned_closure` DATETIME DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY (`uid`),
UNIQUE KEY (`number`),
UNIQUE KEY (`dn`),
KEY (`name`),
KEY (`parent_id`),
KEY (`parent_uid`),
KEY (`life_stage`),
KEY (`acode`),
KEY (`create_by_uid`),
KEY (`close_by_uid`),
FULLTEXT KEY (`number`,`name`,`designation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/FavoriteDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `container_favorite` (
`user_id` int(11) NOT NULL,
`container_id` int(11) NOT NULL,
`container_number` VARCHAR(512) NOT NULL,
`container_description` MEDIUMTEXT NULL,
`space_id` int(11) NOT NULL,
`spacename` VARCHAR(16) NOT NULL,
PRIMARY KEY  (`user_id`, `container_id`, `space_id`)
) ENGINE=MyIsam;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/UnitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/CategoryDao.php
-- **********************************************************************

CREATE TABLE IF NOT EXISTS `project_category_rel` (
`parent_id` int(11) NOT NULL,
`parent_uid` VARCHAR(64) NOT NULL,
`parent_cid` VARCHAR(64) NOT NULL,
`child_id` int(11) DEFAULT NULL,
`child_uid` VARCHAR(64) DEFAULT NULL,
`child_cid` VARCHAR(64) DEFAULT NULL,
`rdn` VARCHAR(128) DEFAULT NULL,
KEY `K_project_category_rel_1` (`parent_id`),
KEY `K_project_category_rel_2` (`parent_uid`),
KEY `K_project_category_rel_3` (`parent_cid`),
KEY `K_project_category_rel_4` (`child_id`),
KEY `K_project_category_rel_5` (`child_uid`),
KEY `K_project_category_rel_6` (`child_cid`),
KEY `K_project_category_rel_7` (`rdn`),
UNIQUE KEY `U_project_category_rel_1` (`parent_id`,`parent_cid`,`child_id`),
CONSTRAINT `FK_project_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
CONSTRAINT `FK_project_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `FK_project_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ProcessDao.php
-- **********************************************************************

CREATE TABLE IF NOT EXISTS `project_process_rel` (
`parent_id` int(11) NOT NULL,
`parent_uid` VARCHAR(64) NOT NULL,
`parent_cid` VARCHAR(64) NOT NULL,
`child_id` int(11) DEFAULT NULL,
`child_uid` VARCHAR(64) DEFAULT NULL,
`child_cid` VARCHAR(64) DEFAULT NULL,
`rdn` VARCHAR(128) DEFAULT NULL,
KEY `K_project_process_rel_1` (`parent_id`),
KEY `K_project_process_rel_2` (`parent_uid`),
KEY `K_project_process_rel_3` (`parent_cid`),
KEY `K_project_process_rel_4` (`child_id`),
KEY `K_project_process_rel_5` (`child_uid`),
KEY `K_project_process_rel_6` (`child_cid`),
KEY `K_project_process_rel_7` (`rdn`),
UNIQUE KEY `U_project_process_rel_1` (`parent_id`,`parent_cid`,`child_id`),
CONSTRAINT `U_project_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
CONSTRAINT `U_project_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `U_project_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ContainerDao.php
-- **********************************************************************

-- CONTAINER
CREATE TABLE IF NOT EXISTS `project_container_rel` (
`parent_id` int(11) NOT NULL,
`parent_uid` VARCHAR(64) NOT NULL,
`parent_cid` VARCHAR(64) NOT NULL,
`child_id` int(11) DEFAULT NULL,
`child_uid` VARCHAR(64) DEFAULT NULL,
`child_cid` VARCHAR(64) DEFAULT NULL,
`spacename` VARCHAR(64) NOT NULL,
`rdn` VARCHAR(128) DEFAULT NULL,
KEY `K_project_container_rel_1` (`parent_id`),
KEY `K_project_container_rel_2` (`parent_uid`),
KEY `K_project_container_rel_3` (`parent_cid`),
KEY `K_project_container_rel_4` (`child_id`),
KEY `K_project_container_rel_5` (`child_uid`),
KEY `K_project_container_rel_6` (`child_cid`),
KEY `K_project_container_rel_7` (`rdn`),
UNIQUE KEY `U_project_container_rel_1` (`parent_uid`,`child_uid`),
CONSTRAINT `FK_project_container_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
CONSTRAINT `FK_project_container_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;


-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/DoctypeDao.php
-- **********************************************************************

CREATE TABLE IF NOT EXISTS `project_doctype_rel` (
`parent_id` int(11) NOT NULL,
`parent_uid` VARCHAR(64) NOT NULL,
`parent_cid` VARCHAR(64) NOT NULL,
`child_id` int(11) DEFAULT NULL,
`child_uid` VARCHAR(64) DEFAULT NULL,
`child_cid` VARCHAR(64) DEFAULT NULL,
`rdn` VARCHAR(128) DEFAULT NULL,
KEY `K_project_doctype_rel_1` (`parent_id`),
KEY `K_project_doctype_rel_2` (`parent_uid`),
KEY `K_project_doctype_rel_3` (`parent_cid`),
KEY `K_project_doctype_rel_4` (`child_id`),
KEY `K_project_doctype_rel_5` (`child_uid`),
KEY `K_project_doctype_rel_6` (`child_cid`),
KEY `K_project_doctype_rel_7` (`rdn`),
UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
CONSTRAINT `U_project_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
CONSTRAINT `U_project_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `U_project_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/PropertyDao.php
-- **********************************************************************

-- ############################################################################"
-- PROPERTY
-- ############################################################################"
-- PROPERTY
CREATE TABLE IF NOT EXISTS `project_property_rel` (
`parent_id` int(11) NOT NULL,
`parent_uid` VARCHAR(64) NOT NULL,
`parent_cid` VARCHAR(64) NOT NULL,
`child_id` int(11) DEFAULT NULL,
`child_uid` VARCHAR(64) DEFAULT NULL,
`child_cid` VARCHAR(64) DEFAULT NULL,
`rdn` VARCHAR(128) DEFAULT NULL,
KEY `K_project_property_rel_1` (`parent_id`),
KEY `K_project_property_rel_2` (`parent_uid`),
KEY `K_project_property_rel_3` (`parent_cid`),
KEY `K_project_property_rel_4` (`child_id`),
KEY `K_project_property_rel_5` (`child_uid`),
KEY `K_project_property_rel_6` (`child_cid`),
KEY `K_project_property_rel_7` (`rdn`),
UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
CONSTRAINT `FK_project_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
CONSTRAINT `FK_project_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;


-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/HistoryDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `project_history` (
`histo_order` int(11) NOT NULL DEFAULT '0',
`action_name` VARCHAR(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
`action_by` VARCHAR(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
`action_started` int(11) NOT NULL DEFAULT '0',
`project_id` int(11) NOT NULL DEFAULT '0',
`project_number` VARCHAR(16) COLLATE latin1_general_ci DEFAULT NULL,
`project_description` VARCHAR(128) COLLATE latin1_general_ci DEFAULT NULL,
`project_state` VARCHAR(16) COLLATE latin1_general_ci DEFAULT NULL,
`project_indice_id` int(11) DEFAULT NULL,
`default_process_id` int(11) DEFAULT NULL,
`open_by` int(11) DEFAULT NULL,
`created` int(11) DEFAULT NULL,
`planned_closure` int(11) DEFAULT NULL,
`close_by` int(11) DEFAULT NULL,
`closed` int(11) DEFAULT NULL,
`area_id` int(11) DEFAULT NULL,
PRIMARY KEY (`histo_order`)
);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/NotificationDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `notifications` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`uid` VARCHAR(32) NOT NULL,
`name` VARCHAR(128) NOT NULL,
`owner_uid` VARCHAR (128) NOT NULL,
`owner_id` VARCHAR (128) NOT NULL,
`reference_uid` VARCHAR (128) NOT NULL,
`reference_cid` VARCHAR (128) NULL,
`reference_id` VARCHAR (128) NULL,
`spacename` VARCHAR (64) NOT NULL,
`events` VARCHAR (256) NULL,
`condition` BLOB,
PRIMARY KEY (`id`),
UNIQUE KEY `U_notifications_1` (`uid`),
UNIQUE KEY `U_notifications_2` (`owner_id`,`reference_uid`,`events`),
KEY `K_notifications_owner_id` (`owner_id`),
KEY `K_notifications_reference_uid` (`reference_uid`),
KEY `K_notifications_events` (`events`),
KEY `K_notifications_name` (`name`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/IndexDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Change/src/Change/Dao/ChangeDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `changes` (
`id` INT(11) NOT NULL,
`uid` VARCHAR(32) NOT NULL,
`cid` VARCHAR(16) NOT NULL DEFAULT 'change47g548rh',
`dn` VARCHAR(256) NULL,
`number` VARCHAR(256) NOT NULL,
`name` VARCHAR(256) NOT NULL,
`label` VARCHAR(16) DEFAULT NULL,
`designation` TEXT DEFAULT NULL,
`reason_of_change` TEXT DEFAULT NULL,
`parent_id` INT(11) NULL,
`parent_uid` VARCHAR(32) NULL,
`life_stage` VARCHAR(64) DEFAULT NULL,
`created` DATETIME DEFAULT NULL,
`create_by_id` INT(11) DEFAULT NULL,
`create_by_uid` VARCHAR(64) DEFAULT NULL,
`closed` DATETIME DEFAULT NULL,
`close_by_id` INT(11) DEFAULT NULL,
`close_by_uid` VARCHAR(64) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `UK_changes_uid` (`uid`),
UNIQUE KEY `UK_changes_number` (`number`),
UNIQUE KEY `UK_changes_dn` (`dn`),
KEY `K_changes_name` (`name`),
FULLTEXT `FT_changes_reason_of_change` (`reason_of_change`),
FULLTEXT `FT_changes_designation` (`designation`),
KEY `K_changes_parent_id` (`parent_id`),
KEY `K_changes_parent_uid` (`parent_uid`),
KEY `K_changes_life_stage` (`life_stage`),
KEY `K_changes_created` (`created`),
KEY `K_changes_create_by_id` (`create_by_id`),
KEY `K_changes_create_by_uid` (`create_by_uid`),
KEY `K_changes_closed` (`closed`),
KEY `K_changes_close_by_id` (`close_by_id`),
KEY `K_changes_close_by_uid` (`close_by_uid`)
) ENGINE=InnoDB;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Sier/src/Sier/Model/Como/ComoDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Discussion/src/Discussion/Dao/CommentDao.php
-- **********************************************************************

CREATE TABLE IF NOT EXISTS `discussion_comment` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`uid` VARCHAR(255) NOT NULL,
`cid` CHAR(16) NOT NULL DEFAULT '568be4fc7a0a8',
`name` VARCHAR(255) NULL DEFAULT NULL,
`discussion_uid` VARCHAR(255) NOT NULL,
`owner_id` VARCHAR(255) NULL DEFAULT NULL,
`parent_id` INT(11) NULL DEFAULT NULL,
`parent_uid` VARCHAR(255) NULL DEFAULT NULL,
`updated` DATETIME NOT NULL,
`body` MEDIUMTEXT NOT NULL,
PRIMARY KEY (`id`),
UNIQUE INDEX (`uid` ASC),
INDEX (`parent_id` ASC),
INDEX (`parent_uid` ASC),
INDEX (`discussion_uid` ASC),
INDEX (`owner_id` ASC),
INDEX (`updated` ASC)
)ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/FilelistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabpanelDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ComponentDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `portail_components`(
`id` INT NOT NULL AUTO_INCREMENT,
`uid` VARCHAR(255) NOT NULL,
`cid` VARCHAR(64) NOT NULL,
`name` VARCHAR(255) NOT NULL,
`title` MEDIUMTEXT,
`url` VARCHAR(255),
`internalref` VARCHAR(255) NULL,
`index` INT NULL,
`visibility` VARCHAR(255) NULL,
`ownerId` VARCHAR(255) NULL,
`parentId` INT NULL,
`parentUid` VARCHAR(255) NULL,
`updateById` VARCHAR(255) NULL,
`updated` DATETIME NULL,
`attributes` MEDIUMTEXT NULL,
PRIMARY KEY (`id`),
UNIQUE (uid),
INDEX (`url` ASC),
INDEX (`index` ASC),
INDEX (`visibility` ASC),
INDEX (`parentId` ASC),
INDEX (`parentUid` ASC),
INDEX (`cid` ASC),
INDEX (`internalref` ASC)
);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/IndicatorDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/SimplegridDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/StatusboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ProjectDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/ItemDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `portail_documentlist_items` (
`link_id` INT(11) NOT NULL AUTO_INCREMENT,
`parent_uid` VARCHAR(64) default NULL,
`child_id` INT(11) default NULL,
`child_uid` VARCHAR(64) default NULL,
`child_space_id` INT(11) default NULL,
`lindex` INT(11) default 0,
data TEXT default NULL,
PRIMARY KEY (`link_id`),
UNIQUE KEY (`parent_uid`,`child_uid`),
INDEX (`lindex`),
INDEX (`parent_uid`),
INDEX (`child_uid`),
INDEX (`child_id`, `child_space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=10;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/SearchEngineDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/JalonDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/DocumentlistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TinymceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/CheckboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/BoxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Application/src/Application/Cron/schemaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbgate/src/Rbgate/Logger/LogDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Import/src/Import/Model/PackageDao.php
-- **********************************************************************
ALTER TABLE `mockup_doc_files` ADD COLUMN `package_id` INT(11) DEFAULT NULL;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/TypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/Template/MaskDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Admin/src/Admin/Dao/ToolDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaProjectLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Model/IndexerDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Engine/SearchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/ResourceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RuleDao.php
-- **********************************************************************
ALTER TABLE `acl_rule`
ADD CONSTRAINT `FK_acl_rule_resource`
FOREIGN KEY (`resource`)
REFERENCES `acl_resource` (`cn`)
ON DELETE CASCADE
ON UPDATE CASCADE,

ADD CONSTRAINT `FK_acl_rule_rightid`
FOREIGN KEY (`rightId`)
REFERENCES `acl_right` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE,

ADD CONSTRAINT `FK_acl_rule_roleid`
FOREIGN KEY (`roleId`)
REFERENCES `acl_role` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RightDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/UserRoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/ClassDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/PropsetDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/History/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Typemime/TypemimeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Extended/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Postit/PostitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/EndDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StartDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StandaloneDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/AswitchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/JoinDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/SplitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/TransitionDao.php
-- **********************************************************************
ALTER TABLE `wf_transition`
ADD CONSTRAINT `FK_wf_transition_1`
FOREIGN KEY (`parentId`)
REFERENCES `wf_activity` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE,
ADD CONSTRAINT `FK_wf_transition_2`
FOREIGN KEY (`childId`)
REFERENCES `wf_activity` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ActivityDao.php
-- **********************************************************************
ALTER TABLE `wf_activity`
ADD CONSTRAINT `FK_wf_activity_1`
FOREIGN KEY (`processId`)
REFERENCES `wf_process` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/InstanceDao.php
-- **********************************************************************
ALTER TABLE `wf_instance`
ADD CONSTRAINT `FK_wf_instance_1`
FOREIGN KEY (`processId`)
REFERENCES `wf_process` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/RoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CheckoutIndexDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocfileDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocumentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/PdmLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/BasketDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/IndiceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/LinkDao.php
-- **********************************************************************

ALTER TABLE `workitem_doc_rel`
ADD CONSTRAINT `FK_workitem_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_doc_rel_11` FOREIGN KEY (`child_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `bookshop_doc_rel`
ADD CONSTRAINT `FK_bookshop_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_doc_rel_11` FOREIGN KEY (`child_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cadlib_doc_rel`
ADD CONSTRAINT `FK_cadlib_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_doc_rel_11` FOREIGN KEY (`child_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `mockup_doc_rel`
ADD CONSTRAINT `FK_mockup_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_doc_rel_11` FOREIGN KEY (`child_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/Share/PublicUrlDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/SpaceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/IterationDao.php
-- **********************************************************************
ALTER TABLE `workitem_doc_files_versions`
ADD COLUMN `of_file_id` INT(11) NULL AFTER `id`,
ADD CONSTRAINT `FK_workitem_doc_files_versions_1` FOREIGN KEY (`of_file_id`) REFERENCES `workitem_doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD KEY (`of_file_id`);

ALTER TABLE `bookshop_doc_files_versions`
ADD COLUMN `of_file_id` INT(11) NULL AFTER `id`,
ADD CONSTRAINT `FK_bookshop_doc_files_versions_1` FOREIGN KEY (`of_file_id`) REFERENCES `bookshop_doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD KEY (`of_file_id`);

ALTER TABLE `cadlib_doc_files_versions`
ADD COLUMN `of_file_id` INT(11) NULL AFTER `id`,
ADD CONSTRAINT `FK_cadlib_doc_files_versions_1` FOREIGN KEY (`of_file_id`) REFERENCES `cadlib_doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD KEY (`of_file_id`);

ALTER TABLE `mockup_doc_files_versions`
ADD COLUMN `of_file_id` INT(11) NULL AFTER `id`,
ADD CONSTRAINT `FK_mockup_doc_files_versions_1` FOREIGN KEY (`of_file_id`) REFERENCES `mockup_doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD KEY (`of_file_id`);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/VersionDao.php
-- **********************************************************************
ALTER TABLE `workitem_doc_files`
ADD CONSTRAINT `FK_workitem_doc_files_1`
FOREIGN KEY (`document_id`)
REFERENCES `workitem_documents` (`id`)
ON UPDATE CASCADE;

ALTER TABLE `bookshop_doc_files`
ADD CONSTRAINT `FK_bookshop_doc_files_1`
FOREIGN KEY (`document_id`)
REFERENCES `bookshop_documents` (`id`)
ON UPDATE CASCADE;

ALTER TABLE `cadlib_doc_files`
ADD CONSTRAINT `FK_cadlib_doc_files_1`
FOREIGN KEY (`document_id`)
REFERENCES `cadlib_documents` (`id`)
ON UPDATE CASCADE;

ALTER TABLE `mockup_doc_files`
ADD CONSTRAINT `FK_mockup_doc_files_1`
FOREIGN KEY (`document_id`)
REFERENCES `mockup_documents` (`id`)
ON UPDATE CASCADE;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/CategoryDao.php
-- **********************************************************************
ALTER TABLE `workitem_category_rel`
ADD CONSTRAINT `FK_workitem_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

ALTER TABLE `bookshop_category_rel`
ADD CONSTRAINT `FK_bookshop_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

ALTER TABLE `cadlib_category_rel`
ADD CONSTRAINT `FK_cadlib_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
ALTER TABLE `mockup_category_rel`
ADD CONSTRAINT `FK_mockup_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/ProcessDao.php
-- **********************************************************************
ALTER TABLE `workitem_process_rel`
ADD CONSTRAINT `FK_workitem_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

ALTER TABLE `bookshop_process_rel`
ADD CONSTRAINT `FK_bookshop_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE;

ALTER TABLE `cadlib_process_rel`
ADD CONSTRAINT `FK_cadlib_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;


ALTER TABLE `mockup_process_rel`
ADD CONSTRAINT `FK_mockup_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/DoctypeDao.php
-- **********************************************************************

ALTER TABLE `workitem_doctype_rel`
ADD CONSTRAINT `FK_workitem_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `bookshop_doctype_rel`
ADD CONSTRAINT `FK_bookshop_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cadlib_doctype_rel`
ADD CONSTRAINT `FK_cadlib_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `mockup_doctype_rel`
ADD CONSTRAINT `FK_mockup_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/PropertyDao.php
-- **********************************************************************
ALTER TABLE `workitem_property_rel`
ADD CONSTRAINT `FK_workitem_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `workitem_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `bookshop_property_rel`
ADD CONSTRAINT `FK_bookshop_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `bookshop_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `cadlib_property_rel`
ADD CONSTRAINT `FK_cadlib_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `cadlib_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `mockup_property_rel`
ADD CONSTRAINT `FK_mockup_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `mockup_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/VersionDao.php
-- **********************************************************************
ALTER TABLE `workitem_documents`
ADD CONSTRAINT `FK_workitem_documents_doctype_id` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`),
ADD CONSTRAINT `FK_workitem_documents_version` FOREIGN KEY (`version`) REFERENCES `document_indice` (`indice_id`),
ADD CONSTRAINT `FK_workitem_documents_container_id` FOREIGN KEY (`container_id`) REFERENCES `workitems` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_documents_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `bookshop_documents`
CHANGE COLUMN `spacename` `spacename` VARCHAR(8) NOT NULL DEFAULT 'bookshop',
ADD CONSTRAINT `FK_bookshop_documents_doctype_id` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_documents_version` FOREIGN KEY (`version`) REFERENCES `document_indice` (`indice_id`),
ADD CONSTRAINT `FK_bookshop_documents_container_id` FOREIGN KEY (`container_id`) REFERENCES `bookshops` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_documents_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `cadlib_documents`
CHANGE COLUMN `spacename` `spacename` VARCHAR(8) NOT NULL DEFAULT 'cadlib',
ADD CONSTRAINT `FK_cadlib_documents_doctype_id` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_documents_version` FOREIGN KEY (`version`) REFERENCES `document_indice` (`indice_id`),
ADD CONSTRAINT `FK_cadlib_documents_container_id` FOREIGN KEY (`container_id`) REFERENCES `cadlibs` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_documents_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `mockup_documents`
CHANGE COLUMN `spacename` `spacename` VARCHAR(8) NOT NULL DEFAULT 'mockup',
ADD CONSTRAINT `FK_mockup_documents_doctype_id` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_documents_version` FOREIGN KEY (`version`) REFERENCES `document_indice` (`indice_id`),
ADD CONSTRAINT `FK_mockup_documents_container_id` FOREIGN KEY (`container_id`) REFERENCES `mockups` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_documents_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/AliasDao.php
-- **********************************************************************

ALTER TABLE `workitem_alias`
ADD CONSTRAINT `FK_workitem_alias_1`
FOREIGN KEY (`container_id`)
REFERENCES `workitems` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE `bookshop_alias`
ADD CONSTRAINT `FK_bookshop_alias_1`
FOREIGN KEY (`container_id`)
REFERENCES `bookshops` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE `cadlib_alias`
ADD CONSTRAINT `FK_cadlib_alias_1`
FOREIGN KEY (`container_id`)
REFERENCES `cadlibs` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE `mockup_alias`
ADD CONSTRAINT `FK_mockup_alias_1`
FOREIGN KEY (`container_id`)
REFERENCES `mockups` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE;


-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/VersionDao.php
-- **********************************************************************

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/MessageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/ArchiveDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/SentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/MailboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/GroupDao.php
-- **********************************************************************

ALTER TABLE `acl_group`
ADD CONSTRAINT `FK_acl_group_acl_user1`
FOREIGN KEY (`owner_id`)
REFERENCES `acl_user` (`id`)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/UserDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/PreferenceDao.php
-- **********************************************************************
ALTER TABLE `user_prefs`
ADD CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`owner_id`) REFERENCES `acl_user` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/SessionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/PasswordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/SomebodyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Tag/TagDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/ProductDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ConceptDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ContextDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/InstanceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/UsageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Observers/CallbackFactoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/JobDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/CronTaskDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RepositDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/DistantSiteDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/ReplicatedDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/MissingFileDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RecordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/BookshopDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `bookshops` LIKE `workitems`;
ALTER TABLE `bookshops`
CHANGE COLUMN `spacename` `spacename` VARCHAR(16) NOT NULL DEFAULT 'bookshop',
ADD CONSTRAINT `FK_bookshops_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
ADD CONSTRAINT `FK_bookshops_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
ADD CONSTRAINT `FK_bookshops_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/CadlibDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `cadlibs` LIKE `workitems`;
ALTER TABLE `cadlibs`
CHANGE COLUMN `spacename` `spacename` VARCHAR(16) NOT NULL DEFAULT 'cadlib',
ADD CONSTRAINT `FK_cadlibs_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
ADD CONSTRAINT `FK_cadlibs_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
ADD CONSTRAINT `FK_cadlibs_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/MockupDao.php
-- **********************************************************************
CREATE TABLE IF NOT EXISTS `mockups` LIKE `workitems`;
ALTER TABLE `mockups`
CHANGE COLUMN `spacename` `spacename` VARCHAR(16) NOT NULL DEFAULT 'mockup',
ADD CONSTRAINT `FK_mockups_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
ADD CONSTRAINT `FK_mockups_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
ADD CONSTRAINT `FK_mockups_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/WorkitemDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/ProjectDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/FavoriteDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/UnitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ContainerDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/NotificationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/IndexDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Change/src/Change/Dao/ChangeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Sier/src/Sier/Model/Como/ComoDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Discussion/src/Discussion/Dao/CommentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/FilelistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabpanelDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ComponentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/IndicatorDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/SimplegridDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/StatusboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ProjectDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/ItemDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/SearchEngineDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/JalonDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/DocumentlistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TinymceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/CheckboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/BoxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Application/src/Application/Cron/schemaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbgate/src/Rbgate/Logger/LogDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Import/src/Import/Model/PackageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/TypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/Template/MaskDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Admin/src/Admin/Dao/ToolDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaProjectLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Model/IndexerDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Engine/SearchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/ResourceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RuleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RightDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/UserRoleDao.php
-- **********************************************************************

ALTER TABLE `acl_user_role`
ADD CONSTRAINT `FK_acl_user_role_acl_user1` FOREIGN KEY (`userId`) REFERENCES `acl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_acl_user_role_acl_role1` FOREIGN KEY (`roleId`) REFERENCES `acl_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_acl_user_role_acl_resource1` FOREIGN KEY (`resourceCn`) REFERENCES `acl_resource` (`cn`) ON DELETE CASCADE ON UPDATE CASCADE;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/ClassDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/PropsetDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/History/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Typemime/TypemimeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Extended/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Postit/PostitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/EndDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StartDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StandaloneDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/AswitchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/JoinDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/SplitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/TransitionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/InstanceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/RoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CheckoutIndexDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocfileDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocumentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/PdmLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/BasketDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/IndiceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/Share/PublicUrlDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/SpaceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/MessageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/ArchiveDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/SentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/MailboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/GroupDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/UserDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/PreferenceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/SessionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/PasswordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/SomebodyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Tag/TagDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/ProductDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ConceptDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ContextDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/InstanceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/UsageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Observers/CallbackFactoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/JobDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/CronTaskDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RepositDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/DistantSiteDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/ReplicatedDao.php
-- **********************************************************************
ALTER TABLE `vault_replicated`
ADD CONSTRAINT `fk_vault_replicated_1`
FOREIGN KEY (`distantsite_id`)
REFERENCES `vault_distant_site` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE,
ADD CONSTRAINT `fk_vault_replicated_2`
FOREIGN KEY (`reposit_id`)
REFERENCES `vault_reposit` (`id`)
ON DELETE CASCADE
ON UPDATE CASCADE;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/MissingFileDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RecordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/BookshopDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/CadlibDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/MockupDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/WorkitemDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/ProjectDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/FavoriteDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/UnitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ContainerDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/NotificationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/IndexDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Change/src/Change/Dao/ChangeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Sier/src/Sier/Model/Como/ComoDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Discussion/src/Discussion/Dao/CommentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/FilelistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabpanelDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ComponentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/IndicatorDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/SimplegridDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/StatusboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ProjectDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/ItemDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/SearchEngineDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/JalonDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/DocumentlistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TinymceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/CheckboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/BoxDao.php
-- **********************************************************************
--
-- generate from getSeqInitStatement.sql
--


DROP TABLE IF EXISTS acl_resource_seq;
DROP TABLE IF EXISTS acl_seq;
DROP TABLE IF EXISTS bookshop_cont_metadata_seq;
DROP TABLE IF EXISTS bookshop_doc_files_seq;
DROP TABLE IF EXISTS bookshop_doc_files_versions_seq;
DROP TABLE IF EXISTS bookshop_doccomments_seq;
DROP TABLE IF EXISTS bookshop_documents_history_seq;
DROP TABLE IF EXISTS bookshop_documents_seq;
DROP TABLE IF EXISTS bookshop_metadata_seq;
DROP TABLE IF EXISTS cadlib_cont_metadata_seq;
DROP TABLE IF EXISTS cadlib_doc_files_seq;
DROP TABLE IF EXISTS cadlib_doc_files_versions_seq;
DROP TABLE IF EXISTS cadlib_doccomments_seq;
DROP TABLE IF EXISTS cadlib_documents_history_seq;
DROP TABLE IF EXISTS cadlib_documents_seq;
DROP TABLE IF EXISTS cadlib_metadata_seq;
DROP TABLE IF EXISTS categories_seq;
DROP TABLE IF EXISTS docseeder_seq;
DROP TABLE IF EXISTS doctypes_seq;
DROP TABLE IF EXISTS document_rel_seq;
DROP TABLE IF EXISTS message_seq;
DROP TABLE IF EXISTS mockup_category_rel_seq;
DROP TABLE IF EXISTS mockup_cont_metadata_seq;
DROP TABLE IF EXISTS mockup_doc_files_seq;
DROP TABLE IF EXISTS mockup_doc_files_versions_seq;
DROP TABLE IF EXISTS mockup_doccomments_seq;
DROP TABLE IF EXISTS mockup_doctype_process_seq;
DROP TABLE IF EXISTS mockup_documents_history_seq;
DROP TABLE IF EXISTS mockup_documents_seq;
DROP TABLE IF EXISTS mockup_files_seq;
DROP TABLE IF EXISTS mockup_import_history_seq;
DROP TABLE IF EXISTS mockup_metadata_seq;
DROP TABLE IF EXISTS org_seq;
DROP TABLE IF EXISTS partners_seq;
DROP TABLE IF EXISTS pdm_instance_seq;
DROP TABLE IF EXISTS pdm_seq;
DROP TABLE IF EXISTS postit_seq;
DROP TABLE IF EXISTS rt_aif_code_a350_seq;
DROP TABLE IF EXISTS vault_reposit_seq;
DROP TABLE IF EXISTS wf_instance_seq;
DROP TABLE IF EXISTS wf_seq;
DROP TABLE IF EXISTS workitem_cont_metadata_seq;
DROP TABLE IF EXISTS workitem_doc_files_seq;
DROP TABLE IF EXISTS workitem_doc_files_versions_seq;
DROP TABLE IF EXISTS workitem_doccomments_seq;
DROP TABLE IF EXISTS workitem_documents_history_seq;
DROP TABLE IF EXISTS workitem_documents_seq;
DROP TABLE IF EXISTS workitem_metadata_seq;


CREATE TABLE acl_resource_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE acl_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE bookshop_cont_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE bookshop_doc_files_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE bookshop_doc_files_versions_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE bookshop_doccomments_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE bookshop_documents_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE bookshop_documents_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE bookshop_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE cadlib_cont_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE cadlib_doc_files_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE cadlib_doc_files_versions_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE cadlib_doccomments_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE cadlib_documents_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE cadlib_documents_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE cadlib_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE categories_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE docseeder_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE doctypes_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE document_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE message_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_category_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_cont_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_doc_files_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_doc_files_versions_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_doccomments_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_doctype_process_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_documents_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_documents_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_files_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_import_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE org_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE partners_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE pdm_instance_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE pdm_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE postit_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE rt_aif_code_a350_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE vault_reposit_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE wf_instance_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE wf_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE workitem_cont_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE workitem_doc_files_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE workitem_doc_files_versions_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE workitem_doccomments_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE workitem_documents_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE workitem_documents_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE workitem_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;

INSERT INTO acl_resource_seq SET id=100;
INSERT INTO acl_seq SET id=100;
INSERT INTO bookshop_cont_metadata_seq SET id=100;
INSERT INTO bookshop_doc_files_seq SET id=100;
INSERT INTO bookshop_doc_files_versions_seq SET id=100;
INSERT INTO bookshop_doccomments_seq SET id=100;
INSERT INTO bookshop_documents_history_seq SET id=100;
INSERT INTO bookshop_documents_seq SET id=100;
INSERT INTO bookshop_metadata_seq SET id=100;
INSERT INTO cadlib_cont_metadata_seq SET id=100;
INSERT INTO cadlib_doc_files_seq SET id=100;
INSERT INTO cadlib_doc_files_versions_seq SET id=100;
INSERT INTO cadlib_doccomments_seq SET id=100;
INSERT INTO cadlib_documents_history_seq SET id=100;
INSERT INTO cadlib_documents_seq SET id=100;
INSERT INTO cadlib_metadata_seq SET id=100;
INSERT INTO categories_seq SET id=100;
INSERT INTO docseeder_seq SET id=100;
INSERT INTO document_rel_seq SET id=100;
INSERT INTO message_seq SET id=100;
INSERT INTO mockup_category_rel_seq SET id=100;
INSERT INTO mockup_cont_metadata_seq SET id=100;
INSERT INTO mockup_doc_files_seq SET id=100;
INSERT INTO mockup_doc_files_versions_seq SET id=100;
INSERT INTO mockup_doccomments_seq SET id=100;
INSERT INTO mockup_doctype_process_seq SET id=100;
INSERT INTO mockup_documents_history_seq SET id=100;
INSERT INTO mockup_documents_seq SET id=100;
INSERT INTO mockup_metadata_seq SET id=100;
INSERT INTO org_seq SET id=100;
INSERT INTO partners_seq SET id=100;
INSERT INTO pdm_instance_seq SET id=100;
INSERT INTO pdm_seq SET id=100;
INSERT INTO postit_seq SET id=100;
INSERT INTO rt_aif_code_a350_seq SET id=100;
INSERT INTO vault_reposit_seq SET id=100;
INSERT INTO wf_instance_seq SET id=100;
INSERT INTO wf_seq SET id=100;
INSERT INTO workitem_cont_metadata_seq SET id=100;
INSERT INTO workitem_doc_files_seq SET id=100;
INSERT INTO workitem_doc_files_versions_seq SET id=100;
INSERT INTO workitem_doccomments_seq SET id=100;
INSERT INTO workitem_documents_history_seq SET id=100;
INSERT INTO workitem_documents_seq SET id=100;
INSERT INTO workitem_metadata_seq SET id=100;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Application/src/Application/Cron/schemaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbgate/src/Rbgate/Logger/LogDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Import/src/Import/Model/PackageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/TypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/Template/MaskDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Admin/src/Admin/Dao/ToolDao.php
-- **********************************************************************
DROP PROCEDURE IF EXISTS getFilesWithoutDocument;
DELIMITER $$
CREATE PROCEDURE `getFilesWithoutDocument`()
BEGIN
SELECT
`df`.`id` as `id`,
`df`.`uid` as `uid`,
`df`.`name` as `name`,
"workitem" as `spacename`
FROM workitem_doc_files as df
LEFT OUTER JOIN workitem_documents as doc ON df.document_id=doc.id
WHERE doc.id IS NULL
UNION
SELECT
`df`.`id` as `id`,
`df`.`uid` as `uid`,
`df`.`name` as `name`,
"bookshop" as `spacename`
FROM bookshop_doc_files as df
LEFT OUTER JOIN bookshop_documents as doc ON df.document_id=doc.id
WHERE doc.id IS NULL
UNION
SELECT
`df`.`id` as `id`,
`df`.`uid` as `uid`,
`df`.`name` as `name`,
"cadlib" as `spacename`
FROM cadlib_doc_files as df
LEFT OUTER JOIN cadlib_documents as doc ON df.document_id=doc.id
WHERE doc.id IS NULL
UNION
SELECT
`df`.`id` as `id`,
`df`.`uid` as `uid`,
`df`.`name` as `name`,
"mockup" as `spacename`
FROM mockup_doc_files as df
LEFT OUTER JOIN mockup_documents as doc ON df.document_id=doc.id
WHERE doc.id IS NULL;
END$$
DELIMITER ;

-- ###################################################
DROP PROCEDURE IF EXISTS getDocumentsWithoutContainer;
DELIMITER $$
CREATE PROCEDURE `getDocumentsWithoutContainer`()
BEGIN
SELECT
`doc`.`id` as `id`,
`doc`.`uid` as `uid`,
`doc`.`name` as `name`,
"workitem" as `spacename`
FROM workitem_documents as doc
LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id
WHERE cont.id IS NULL
UNION
SELECT
`doc`.`id` as `id`,
`doc`.`uid` as `uid`,
`doc`.`name` as `name`,
"bookshop" as `spacename`
FROM bookshop_documents as doc
LEFT OUTER JOIN bookshops as cont ON doc.container_id=cont.id
WHERE cont.id IS NULL
UNION
SELECT
`doc`.`id` as `id`,
`doc`.`uid` as `uid`,
`doc`.`name` as `name`,
"cadlib" as `spacename`
FROM cadlib_documents as doc
LEFT OUTER JOIN cadlibs as cont ON doc.container_id=cont.id
WHERE cont.id IS NULL
UNION
SELECT
`doc`.`id` as `id`,
`doc`.`uid` as `uid`,
`doc`.`name` as `name`,
"mockup" as `spacename`
FROM mockup_documents as doc
LEFT OUTER JOIN mockups as cont ON doc.container_id=cont.id
WHERE cont.id IS NULL;
END$$
DELIMITER ;

-- ###################################################
DROP PROCEDURE IF EXISTS `cancelCheckout`;
DELIMITER $$
CREATE PROCEDURE `cancelCheckout`(_userUid VARCHAR(64))
BEGIN
UPDATE `workitem_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
UPDATE `workitem_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
UPDATE `bookshop_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
UPDATE `bookshop_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
UPDATE `cadlib_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
UPDATE `cadlib_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
END $$
DELIMITER ;

-- ###################################################
DROP PROCEDURE IF EXISTS `deleteDocumentsWithoutContainer`;
DELIMITER $$
CREATE  PROCEDURE `deleteDocumentsWithoutContainer`()
BEGIN
CREATE TABLE tmpdoctodelete AS (
SELECT doc.id, cont.number FROM workitem_documents as doc
LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id
WHERE cont.number is null
);
DELETE FROM workitem_doc_files WHERE document_id IN(SELECT id FROM tmpdoctodelete);
DELETE FROM workitem_documents WHERE id IN(SELECT id FROM tmpdoctodelete);
DROP TABLE tmpdoctodelete;

END $$
DELIMITER ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaProjectLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Model/IndexerDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Engine/SearchDao.php
-- **********************************************************************
SET NAMES 'utf8' COLLATE 'utf8_general_ci';
DROP PROCEDURE IF EXISTS updateSearch;
DELIMITER $$
CREATE PROCEDURE `updateSearch`(_intervalInMn integer)
BEGIN
SELECT `updated` INTO @updated FROM `search_update`;
CASE
WHEN @updated > (now() + INTERVAL - _intervalInMn minute) THEN
SELECT 'is up to date';
BEGIN
END;
ELSE
DROP TABLE IF EXISTS search;
CREATE TABLE IF NOT EXISTS search AS SELECT * FROM objects;
UPDATE `search_update` SET `updated`=now();
END CASE;
END$$
DELIMITER ;
CALL updateSearch(1);

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/ResourceDao.php
-- **********************************************************************

-- 
DROP FUNCTION IF EXISTS `aclSequence`;
DELIMITER $$
CREATE FUNCTION `aclSequence`() RETURNS int(11)
BEGIN
DECLARE lastId INT(11);

UPDATE acl_resource_seq SET id = LAST_INSERT_ID(id+1) LIMIT 1;
SELECT LAST_INSERT_ID(id) INTO lastId FROM acl_resource_seq;

set @ret = lastId;
RETURN @ret;
END$$
DELIMITER ;

-- 
-- Get resourceCn of object uid, if not exists return resourceCn of parent
DROP FUNCTION IF EXISTS getResourceCnFromReferUid;
DELIMITER $$
CREATE FUNCTION `getResourceCnFromReferUid`(_uid VARCHAR(128))
RETURNS VARCHAR(256)
BEGIN
DECLARE resourceCn VARCHAR(256);

SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = _uid;
IF(resourceCn IS NULL) THEN
SELECT cn INTO resourceCn FROM acl_resource AS resource
JOIN acl_resource_breakdown as breakdown ON resource.referToUid = breakdown.parent_uid
WHERE breakdown.uid = _uid;
END IF;

return resourceCn;
END$$
DELIMITER ;

-- 
-- Get resourceCn of object Id and Cid, if not exists return resourceCn of parent
DROP FUNCTION IF EXISTS getResourceCnFromReferIdAndCid;
DELIMITER $$
CREATE FUNCTION `getResourceCnFromReferIdAndCid`(_id INT(11), _cid VARCHAR(128))
RETURNS VARCHAR(256)
BEGIN
DECLARE resourceCn VARCHAR(256);

SELECT cn INTO resourceCn FROM acl_resource WHERE referToId = _id AND referToCid = _cid;
IF(resourceCn IS NULL) THEN
SELECT cn INTO resourceCn FROM acl_resource AS resource
JOIN acl_resource_breakdown as breakdown ON resource.referToUid = breakdown.parent_uid
WHERE breakdown.id = _id AND breakdown.cid=_cid;
END IF;

return resourceCn;
END$$
DELIMITER ;

-- 
-- build the cn path
DROP FUNCTION IF EXISTS build_path_withuid;
DELIMITER $$
CREATE FUNCTION `build_path_withuid`(_uid VARCHAR(64))
RETURNS text
BEGIN
DECLARE parentUid VARCHAR(64);
DECLARE cid VARCHAR(64);
DECLARE message VARCHAR(256);
set @ret = '';

while _uid is not null do
SELECT parent_uid, cid INTO parentUid, cid
FROM acl_resource_breakdown
WHERE uid = _uid;

IF(found_rows() != 1) THEN
set message = CONCAT('uid not found in table acl_resource_breakdown :', _uid);
SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = message;
END IF;

IF(parentUid IS NULL) THEN
set @ret := CONCAT('/app/ged/project/', _uid, @ret);
ELSE
set @ret := CONCAT('/', _uid, @ret);
END IF;

set _uid := parentUid;
end while;

RETURN @ret;
END$$
DELIMITER ;

-- 
DROP PROCEDURE IF EXISTS `getResourceFromReferUid`;
DELIMITER $$
CREATE PROCEDURE `getResourceFromReferUid`(_uid VARCHAR(64))
BEGIN
DECLARE resourceUid VARCHAR(64);

SELECT uid INTO resourceUid FROM acl_resource WHERE referToUid = _uid;
IF(resourceUid IS NULL) THEN
SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE referToUid = (SELECT parent_uid FROM view_resource_breakdown WHERE uid = _uid);
ELSE
SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE uid = resourceUid;
END IF;
END $$
DELIMITER ;


-- 
DROP PROCEDURE IF EXISTS updateResourceBreakdown;
DELIMITER $$
CREATE PROCEDURE `updateResourceBreakdown`()
BEGIN
DROP TABLE IF EXISTS acl_resource_breakdown;
CREATE TABLE IF NOT EXISTS acl_resource_breakdown as
(SELECT
`doc`.`id` AS `id`,
`doc`.`uid` AS `uid`,
`doc`.`name` AS `name`,
`doc`.`cid` AS `cid`,
`doc`.`container_id` AS `parent_id`,
`doc`.`container_uid` AS `parent_uid`
FROM
`workitem_documents` `doc`) UNION (SELECT
`doc`.`id` AS `id`,
`doc`.`uid` AS `uid`,
`doc`.`name` AS `name`,
`doc`.`cid` AS `cid`,
`doc`.`container_id` AS `parent_id`,
`doc`.`container_uid` AS `parent_uid`
FROM
`bookshop_documents` `doc`) UNION (SELECT
`doc`.`id` AS `id`,
`doc`.`uid` AS `uid`,
`doc`.`name` AS `name`,
`doc`.`cid` AS `cid`,
`doc`.`container_id` AS `parent_id`,
`doc`.`container_uid` AS `parent_uid`
FROM
`cadlib_documents` `doc`) UNION (SELECT
`cont`.`id` AS `id`,
`cont`.`uid` AS `uid`,
`cont`.`name` AS `name`,
`cont`.`cid` AS `cid`,
`cont`.`parent_id` AS `parent_id`,
`cont`.`parent_uid` AS `parent_uid`
FROM
`workitems` `cont`) UNION (SELECT
`cont`.`id` AS `id`,
`cont`.`uid` AS `uid`,
`cont`.`name` AS `name`,
`cont`.`cid` AS `cid`,
`cont`.`parent_id` AS `parent_id`,
`cont`.`parent_uid` AS `parent_uid`
FROM
`cadlibs` `cont`) UNION (SELECT
`cont`.`id` AS `id`,
`cont`.`uid` AS `uid`,
`cont`.`name` AS `name`,
`cont`.`cid` AS `cid`,
`cont`.`parent_id` AS `parent_id`,
`cont`.`parent_uid` AS `parent_uid`
FROM
`bookshops` `cont`) UNION (SELECT
`cont`.`id` AS `id`,
`cont`.`uid` AS `uid`,
`cont`.`name` AS `name`,
`cont`.`cid` AS `cid`,
`cont`.`parent_id` AS `parent_id`,
`cont`.`parent_uid` AS `parent_uid`
FROM
`mockups` `cont`) UNION (SELECT
`projects`.`id` AS `id`,
`projects`.`uid` AS `uid`,
`projects`.`name` AS `name`,
`projects`.`cid` AS `cid`,
`projects`.`parent_id` AS `parent_id`,
`projects`.`parent_uid` AS `parent_uid`
FROM
`projects`);

ALTER TABLE acl_resource_breakdown
ADD KEY (`id`),
ADD KEY (`uid`),
ADD KEY (`name`),
ADD KEY (`cid`),
ADD KEY (`parent_id`),
ADD KEY (`parent_uid`);

END$$
DELIMITER ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RuleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RightDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/UserRoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/ClassDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/PropsetDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/History/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Typemime/TypemimeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Extended/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Postit/PostitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/EndDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StartDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StandaloneDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/AswitchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/JoinDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/SplitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/TransitionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/InstanceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CategoryDao.php
-- **********************************************************************

-- CATEGORIES INSERT
DROP TRIGGER IF EXISTS onCategoriesInsert;
delimiter $$
CREATE TRIGGER onCategoriesInsert BEFORE INSERT ON categories FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE parentName VARCHAR(128);

IF(NEW.parent_id IS NULL) THEN
SET NEW.dn = CONCAT('/',NEW.id,'/');
SET NEW.name = CONCAT('/',NEW.nodelabel,'/');
ELSE
SELECT dn, name INTO parentDn, parentName from categories where id=NEW.parent_id;
SET NEW.dn = CONCAT(parentDn, NEW.id,'/');
SET NEW.name = CONCAT(parentName, NEW.nodelabel,'/');
END IF;

END;$$
delimiter ;

-- CATEGORIES UPDATE
DROP TRIGGER IF EXISTS onCategoriesUpdate;
delimiter $$
CREATE TRIGGER onCategoriesUpdate BEFORE UPDATE ON categories FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE parentName VARCHAR(128);

IF(NEW.parent_id IS NULL) THEN
SET NEW.dn = CONCAT('/',NEW.id,'/');
SET NEW.name = CONCAT('/',NEW.nodelabel,'/');
ELSEIF(IFNULL(NEW.parent_id=OLD.parent_id, TRUE) OR IFNULL(NEW.nodelabel=OLD.nodelabel, TRUE) OR NEW.parent_id != OLD.parent_id OR NEW.nodelabel != OLD.nodelabel) THEN
-- get parent dn
SELECT dn, name INTO parentDn, parentName from categories where id=NEW.parent_id;
SET NEW.dn = CONCAT(parentDn, NEW.id,'/');
SET NEW.name = CONCAT(parentName, NEW.nodelabel,'/');
END IF;
END;$$
delimiter ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/RoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CheckoutIndexDao.php
-- **********************************************************************
DROP PROCEDURE IF EXISTS `updateCheckoutIndexFromUserId`;
DELIMITER $$
CREATE PROCEDURE `updateCheckoutIndexFromUserId`(_userId INT(11))
BEGIN
DECLARE userId INT(11);
SET @userId=_userId;
DELETE FROM `checkout_index` WHERE `lock_by_id`=@userId;
INSERT INTO checkout_index(
SELECT * FROM(
SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
FROM workitem_doc_files as file
JOIN workitem_documents as doc ON file.document_id=doc.id
where file.acode=1 and file.lock_by_id=@userId
UNION(
SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
FROM bookshop_doc_files as file
JOIN bookshop_documents as doc ON file.document_id=doc.id
where file.acode=1 and file.lock_by_id=@userId
)
UNION(
SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
FROM cadlib_doc_files as file
JOIN cadlib_documents as doc ON file.document_id=doc.id
where file.acode=1 and file.lock_by_id=@userId
)
UNION(
SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
FROM mockup_doc_files as file
JOIN mockup_documents as doc ON file.document_id=doc.id
where file.acode=1 and file.lock_by_id=@userId
)
) as checkout_index
);
END $$
DELIMITER ;

-- CALL updateCheckoutIndexFromUserId(13);


DROP PROCEDURE IF EXISTS `updateCheckoutIndex`;
DELIMITER $$
CREATE PROCEDURE `updateCheckoutIndex`()
BEGIN
DELETE FROM `checkout_index`;
INSERT INTO checkout_index(
SELECT * FROM(
SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
FROM workitem_doc_files as file
JOIN workitem_documents as doc ON file.document_id=doc.id
WHERE file.acode=1
UNION(
SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
FROM bookshop_doc_files as file
JOIN bookshop_documents as doc ON file.document_id=doc.id
where file.acode=1
)
UNION(
SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
FROM cadlib_doc_files as file
JOIN cadlib_documents as doc ON file.document_id=doc.id
WHERE file.acode=1
)
UNION(
SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
FROM mockup_doc_files as file
JOIN mockup_documents as doc ON file.document_id=doc.id
WHERE file.acode=1
)
) as checkout_index
);
END $$
DELIMITER ;



-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocfileDao.php
-- **********************************************************************

DROP PROCEDURE IF EXISTS `updateDocfileDocumentUidRelation`;
DELIMITER $$
CREATE PROCEDURE `updateDocfileDocumentUidRelation`()
BEGIN

#### WORKITEM
DROP TEMPORARY TABLE IF EXISTS tmptableworkitem;
CREATE TEMPORARY TABLE tmptableworkitem AS (
SELECT df.* FROM workitem_doc_files as df
LEFT OUTER JOIN workitem_documents as doc ON df.document_uid=doc.uid
WHERE doc.id IS NULL
);
DROP TABLE IF EXISTS workitem_doc_files_backup;
CREATE TABLE workitem_doc_files_backup AS(
SELECT * FROM workitem_doc_files
);
UPDATE workitem_doc_files as df
SET document_uid=(SELECT uid FROM workitem_documents WHERE id=df.document_id)
WHERE df.id IN (SELECT id FROM tmptableworkitem);

#### BOOKSHOP
DROP TEMPORARY TABLE IF EXISTS tmptablebookshop;
CREATE TEMPORARY TABLE tmptablebookshop AS (
SELECT df.* FROM bookshop_doc_files as df
LEFT OUTER JOIN bookshop_documents as doc ON df.document_uid=doc.uid
WHERE doc.id IS NULL
);
DROP TABLE IF EXISTS bookshop_doc_files_backup;
CREATE TABLE bookshop_doc_files_backup AS(
SELECT * FROM bookshop_doc_files
);
UPDATE bookshop_doc_files as df
SET document_uid=(SELECT uid FROM bookshop_documents WHERE id=df.document_id)
WHERE df.id IN (SELECT id FROM tmptablebookshop);

END $$
DELIMITER ;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocumentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/PdmLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/BasketDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/IndiceDao.php
-- **********************************************************************
DROP PROCEDURE IF EXISTS `populateIndices`;
DELIMITER $$
CREATE PROCEDURE populateIndices()
BEGIN
DECLARE i INT DEFAULT 1;
DECLARE label VARCHAR(12);

TRUNCATE TABLE `document_indice`;

WHILE i < 100 DO
SET label = CONCAT('SI',LPAD(i, 2, '0'));
INSERT INTO `document_indice` (`indice_id`, `indice_value`) VALUES (i, label);
SET i = i + 1;
END WHILE;
END$$
DELIMITER ;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/Share/PublicUrlDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/SpaceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/CategoryDao.php
-- **********************************************************************

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/CategoryDao.php
-- **********************************************************************

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/VersionDao.php
-- **********************************************************************

-- ON DOCFILE DELETE --
DROP TRIGGER IF EXISTS `onWIDocfileDelete`;
DELIMITER $$
CREATE TRIGGER `onWIDocfileDelete` AFTER DELETE ON `workitem_doc_files` FOR EACH ROW
BEGIN
DELETE FROM `checkout_index` WHERE `file_id`=OLD.`id` AND `spacename`='workitem';
END;$$
DELIMITER ;

-- ON DOCFILE UPDATE --
DROP TRIGGER IF EXISTS onWIDocfileUpdate;
DELIMITER $$
CREATE TRIGGER onWIDocfileUpdate AFTER UPDATE ON workitem_doc_files
FOR EACH ROW
BEGIN
DECLARE containerId INT(11);
DECLARE containerNumber VARCHAR(256);
DECLARE documentId INT(11);
DECLARE designation VARCHAR(256);

IF(NEW.acode <> OLD.acode) THEN
DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
IF NEW.acode = 1 THEN
SELECT
`container_id`,`container_uid`,`id`,`designation`
INTO
containerId, containerNumber, documentId, designation
FROM `workitem_documents`
WHERE `id`=NEW.document_id;

INSERT INTO `checkout_index`
(
`file_name`,
`lock_by_id`,
`lock_by_uid`,
`file_id`,
`file_uid`,
`designation`,
`spacename`,
`container_id`,
`container_number`,
`document_id`
)
VALUES
(
NEW.name,
NEW.lock_by_id,
NEW.lock_by_uid,
NEW.id,
NEW.uid,
`designation`,
'workitem',
`containerId`,
`containerNumber`,
`documentId`
);
END IF;
END IF;
END$$
DELIMITER ;

-- ON DOCFILE INSERT --
DROP TRIGGER IF EXISTS onWIDocfileInsert;
DELIMITER $$
CREATE TRIGGER onWIDocfileInsert AFTER INSERT ON workitem_doc_files
FOR EACH ROW
BEGIN
DECLARE containerId INT(11);
DECLARE containerNumber VARCHAR(256);
DECLARE documentId INT(11);
DECLARE designation VARCHAR(256);

IF NEW.acode = 1 THEN
SELECT
`container_id`,`container_uid`,`id`,`designation`
INTO
containerId, containerNumber, documentId, designation
FROM `workitem_documents`
WHERE `id`=NEW.document_id;

INSERT INTO `checkout_index`
(
`file_name`,
`lock_by_id`,
`lock_by_uid`,
`file_id`,
`file_uid`,
`designation`,
`spacename`,
`container_id`,
`container_number`,
`document_id`
)
VALUES
(
NEW.name,
NEW.lock_by_id,
NEW.lock_by_uid,
NEW.id,
NEW.uid,
`designation`,
'workitem',
`containerId`,
`containerNumber`,
`documentId`
);
END IF;
END$$
DELIMITER ;

-- ************BOOKSHOPS 

DROP TRIGGER IF EXISTS onBookshopDocfileDelete;
delimiter $$
CREATE TRIGGER onBookshopDocfileDelete AFTER DELETE ON bookshop_doc_files FOR EACH ROW
BEGIN
DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='bookshop';
END;$$
delimiter ;


-- ON DOCFILE UPDATE --
DROP TRIGGER IF EXISTS onBookshopDocfileUpdate;
DELIMITER $$
CREATE TRIGGER onBookshopDocfileUpdate AFTER UPDATE ON bookshop_doc_files
FOR EACH ROW
BEGIN
DECLARE containerId INT(11);
DECLARE containerNumber VARCHAR(256);
DECLARE documentId INT(11);
DECLARE designation VARCHAR(256);

IF(NEW.acode <> OLD.acode) THEN
DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
IF NEW.acode = 1 THEN
SELECT
`container_id`,`container_uid`,`id`,`designation`
INTO
containerId, containerNumber, documentId, designation
FROM `bookshop_documents`
WHERE `id`=NEW.document_id;

INSERT INTO `checkout_index`
(
`file_name`,
`lock_by_id`,
`lock_by_uid`,
`file_id`,
`file_uid`,
`designation`,
`spacename`,
`container_id`,
`container_number`,
`document_id`
)
VALUES
(
NEW.name,
NEW.lock_by_id,
NEW.lock_by_uid,
NEW.id,
NEW.uid,
`designation`,
'bookshop',
`containerId`,
`containerNumber`,
`documentId`
);
END IF;
END IF;
END$$
DELIMITER ;


-- ON DOCFILE INSERT --
DROP TRIGGER IF EXISTS onBookshopDocfileInsert;
DELIMITER $$
CREATE TRIGGER onBookshopDocfileInsert AFTER INSERT ON bookshop_doc_files
FOR EACH ROW
BEGIN
DECLARE containerId INT(11);
DECLARE containerNumber VARCHAR(256);
DECLARE documentId INT(11);
DECLARE designation VARCHAR(256);

IF NEW.acode = 1 THEN
SELECT
`container_id`,`container_uid`,`id`,`designation`
INTO
containerId, containerNumber, documentId, designation
FROM `bookshop_documents`
WHERE `id`=NEW.document_id;

INSERT INTO `checkout_index`
(
`file_name`,
`lock_by_id`,
`lock_by_uid`,
`file_id`,
`file_uid`,
`designation`,
`spacename`,
`container_id`,
`container_number`,
`document_id`
)
VALUES
(
NEW.name,
NEW.lock_by_id,
NEW.lock_by_uid,
NEW.id,
NEW.uid,
`designation`,
'bookshop',
`containerId`,
`containerNumber`,
`documentId`
);
END IF;
END$$
DELIMITER ;

-- ************CADLIB 

DROP TRIGGER IF EXISTS onCadlibDocfileDelete;
delimiter $$
CREATE TRIGGER onCadlibDocfileDelete AFTER DELETE ON cadlib_doc_files FOR EACH ROW
BEGIN
DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='cadlib';
END;$$
delimiter ;

-- ON DOCFILE UPDATE --
DROP TRIGGER IF EXISTS onCadlibDocfileUpdate;
DELIMITER $$
CREATE TRIGGER onCadlibDocfileUpdate AFTER UPDATE ON cadlib_doc_files
FOR EACH ROW
BEGIN
DECLARE containerId INT(11);
DECLARE containerNumber VARCHAR(256);
DECLARE documentId INT(11);
DECLARE designation VARCHAR(256);

IF(NEW.acode <> OLD.acode) THEN
DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
IF NEW.acode = 1 THEN
SELECT
`container_id`,`container_uid`,`id`,`designation`
INTO
containerId, containerNumber, documentId, designation
FROM `cadlib_documents`
WHERE `id`=NEW.document_id;

INSERT INTO `checkout_index`
(
`file_name`,
`lock_by_id`,
`lock_by_uid`,
`file_id`,
`file_uid`,
`designation`,
`spacename`,
`container_id`,
`container_number`,
`document_id`
)
VALUES
(
NEW.name,
NEW.lock_by_id,
NEW.lock_by_uid,
NEW.id,
NEW.uid,
`designation`,
'cadlib',
`containerId`,
`containerNumber`,
`documentId`
);
END IF;
END IF;
END$$
DELIMITER ;


-- ON DOCFILE INSERT --
DROP TRIGGER IF EXISTS onCadlibDocfileInsert;
DELIMITER $$
CREATE TRIGGER onCadlibDocfileInsert AFTER INSERT ON cadlib_doc_files
FOR EACH ROW
BEGIN
DECLARE containerId INT(11);
DECLARE containerNumber VARCHAR(256);
DECLARE documentId INT(11);
DECLARE designation VARCHAR(256);

IF NEW.acode = 1 THEN
SELECT
`container_id`,`container_uid`,`id`,`designation`
INTO
containerId, containerNumber, documentId, designation
FROM `cadlib_documents`
WHERE `id`=NEW.document_id;

INSERT INTO `checkout_index`
(
`file_name`,
`lock_by_id`,
`lock_by_uid`,
`file_id`,
`file_uid`,
`designation`,
`spacename`,
`container_id`,
`container_number`,
`document_id`
)
VALUES
(
NEW.name,
NEW.lock_by_id,
NEW.lock_by_uid,
NEW.id,
NEW.uid,
`designation`,
'cadlib',
`containerId`,
`containerNumber`,
`documentId`
);
END IF;
END$$
DELIMITER ;

-- ************ MOCKUP  
DROP TRIGGER IF EXISTS onMockupDocfileDelete;
delimiter $$
CREATE TRIGGER onMockupDocfileDelete AFTER DELETE ON mockup_doc_files FOR EACH ROW
BEGIN
DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='mockup';
END;$$
delimiter ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/CategoryDao.php
-- **********************************************************************

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onWorkitemCategoryRelInsert;
DELIMITER $$
CREATE TRIGGER onWorkitemCategoryRelInsert BEFORE INSERT ON workitem_category_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onBookshopCategoryRelInsert;
DELIMITER $$
CREATE TRIGGER onBookshopCategoryRelInsert BEFORE INSERT ON bookshop_category_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END $$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onCadlibCategoryRelInsert;
DELIMITER $$
CREATE TRIGGER onCadlibCategoryRelInsert BEFORE INSERT ON cadlib_category_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onMockupCategoryRelInsert;
DELIMITER $$
CREATE TRIGGER onMockupCategoryRelInsert BEFORE INSERT ON mockup_category_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/ProcessDao.php
-- **********************************************************************

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onWorkitemProcessRelInsert;
DELIMITER $$
CREATE TRIGGER onWorkitemProcessRelInsert BEFORE INSERT ON workitem_process_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onBookshopProcessRelInsert;
DELIMITER $$
CREATE TRIGGER onBookshopProcessRelInsert BEFORE INSERT ON bookshop_process_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onCadlibProcessRelInsert;
DELIMITER $$
CREATE TRIGGER onCadlibProcessRelInsert BEFORE INSERT ON cadlib_process_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;


-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onMockupProcessRelInsert;
DELIMITER $$
CREATE TRIGGER onMockupProcessRelInsert BEFORE INSERT ON mockup_process_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/DoctypeDao.php
-- **********************************************************************

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onWorkitemDoctypeRelInsert;
DELIMITER $$
CREATE TRIGGER onWorkitemDoctypeRelInsert BEFORE INSERT ON workitem_doctype_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onBookshopDoctypeRelInsert;
DELIMITER $$
CREATE TRIGGER onBookshopDoctypeRelInsert BEFORE INSERT ON bookshop_doctype_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onCadlibDoctypeRelInsert;
DELIMITER $$
CREATE TRIGGER onCadlibDoctypeRelInsert BEFORE INSERT ON cadlib_doctype_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onMockupDoctypeRelInsert;
DELIMITER $$
CREATE TRIGGER onMockupDoctypeRelInsert BEFORE INSERT ON mockup_doctype_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/PropertyDao.php
-- **********************************************************************

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS `onWorkitemPropertyRelInsert`;
DELIMITER $$
CREATE TRIGGER `onWorkitemPropertyRelInsert` BEFORE INSERT ON `workitem_property_rel` FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS `onBookshopPropertyRelInsert`;
DELIMITER $$
CREATE TRIGGER `onBookshopPropertyRelInsert` BEFORE INSERT ON `bookshop_property_rel` FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS `onCadlibPropertyRelInsert`;
DELIMITER $$
CREATE TRIGGER `onCadlibPropertyRelInsert` BEFORE INSERT ON `cadlib_property_rel` FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS `onMockupPropertyRelInsert`;
DELIMITER $$
CREATE TRIGGER `onMockupPropertyRelInsert` BEFORE INSERT ON `mockup_property_rel` FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SET NEW.rdn = parentDn;
END;$$
DELIMITER ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/VersionDao.php
-- **********************************************************************

-- ON DOCUMENT DELETE --
DROP TRIGGER IF EXISTS `onWIDocumentDelete`;
DELIMITER $$
CREATE TRIGGER `onWIDocumentDelete` AFTER DELETE ON `workitem_documents` FOR EACH ROW
BEGIN
DELETE FROM `docbasket` WHERE `document_id`=OLD.`id` AND `space_id`=20;
DELETE FROM `postit` WHERE `parent_uid`=OLD.`uid`;
DELETE FROM `discussion_comment` WHERE `parent_uid`=OLD.`uid`;
DELETE FROM `notifications` WHERE `reference_uid`=OLD.`uid`;
UPDATE `pdm_product_version` SET `document_id`=NULL, `document_uid`=NULL WHERE `document_uid`=OLD.`uid`;
DELETE FROM `wf_instance` WHERE `id` IN (SELECT `id` FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`);
DELETE FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`;
DELETE FROM `acl_resource` WHERE `referToUid`=OLD.`uid`;
END;$$
DELIMITER ;

-- ***************** BOOKSHOP 
DROP TRIGGER IF EXISTS `onBookshopDocumentDelete`;
DELIMITER $$
CREATE TRIGGER `onBookshopDocumentDelete` AFTER DELETE ON `bookshop_documents` FOR EACH ROW
BEGIN
DELETE FROM `docbasket` WHERE `document_id`=OLD.`id` AND `space_id`=20;
DELETE FROM `postit` WHERE `parent_uid`=OLD.`uid`;
DELETE FROM `discussion_comment` WHERE `parent_uid`=OLD.`uid`;
DELETE FROM `notifications` WHERE `reference_uid`=OLD.`uid`;
UPDATE `pdm_product_version` SET `document_id`=NULL, `document_uid`=NULL WHERE `document_uid`=OLD.`uid`;
DELETE FROM `wf_instance` WHERE `id` IN (SELECT `id` FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`);
DELETE FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`;
DELETE FROM `acl_resource` WHERE `referToUid`=OLD.`uid`;
END;$$
DELIMITER ;

-- ***************** CADLIB 
DROP TRIGGER IF EXISTS onCadlibDocumentDelete;
DELIMITER $$
CREATE TRIGGER onCadlibDocumentDelete AFTER DELETE ON cadlib_documents FOR EACH ROW
BEGIN
DELETE FROM `docbasket` WHERE `document_id`=OLD.`id` AND `space_id`=20;
DELETE FROM `postit` WHERE `parent_uid`=OLD.`uid`;
DELETE FROM `discussion_comment` WHERE `parent_uid`=OLD.`uid`;
DELETE FROM `notifications` WHERE `reference_uid`=OLD.`uid`;
UPDATE `pdm_product_version` SET `document_id`=NULL, `document_uid`=NULL WHERE `document_uid`=OLD.`uid`;
DELETE FROM `wf_instance` WHERE `id` IN (SELECT `id` FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`);
DELETE FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`;
DELETE FROM `acl_resource` WHERE `referToUid`=OLD.`uid`;
END;$$
DELIMITER ;

-- ***************** MOCKUP 
DROP TRIGGER IF EXISTS onMockupDocumentDelete;
DELIMITER $$
CREATE TRIGGER onMockupDocumentDelete AFTER DELETE ON mockup_documents FOR EACH ROW
BEGIN
DELETE FROM `docbasket` WHERE `document_id`=OLD.`id` AND `space_id`=20;
DELETE FROM `postit` WHERE `parent_uid`=OLD.`uid`;
DELETE FROM `discussion_comment` WHERE `parent_uid`=OLD.`uid`;
DELETE FROM `notifications` WHERE `reference_uid`=OLD.`uid`;
UPDATE `pdm_product_version` SET `document_id`=NULL, `document_uid`=NULL WHERE `document_uid`=OLD.`uid`;
DELETE FROM `wf_instance` WHERE `id` IN (SELECT `id` FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`);
DELETE FROM `wf_document_link` WHERE `parentUid`=OLD.`uid`;
DELETE FROM `acl_resource` WHERE `referToUid`=OLD.`uid`;
END;$$
DELIMITER ;


-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/MessageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/ArchiveDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/SentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/MailboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/GroupDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/UserDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/PreferenceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/SessionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/PasswordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/SomebodyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Tag/TagDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/ProductDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ConceptDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ContextDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/InstanceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/UsageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Observers/CallbackFactoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/JobDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/CronTaskDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RepositDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/DistantSiteDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/ReplicatedDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/MissingFileDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RecordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/BookshopDao.php
-- **********************************************************************

DROP TRIGGER IF EXISTS onBookshopInsert;
delimiter $$
CREATE TRIGGER onBookshopInsert BEFORE INSERT ON bookshops FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE resourceDn VARCHAR(128);
DECLARE parentResourceDn VARCHAR(128);

SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');

SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');

-- add resource
INSERT INTO `acl_resource`
(`id`,
`uid`,
`cn`,
`referToUid`,
`referToId`,
`referToCid`)
VALUES (
aclSequence(),
substr(uuid(),1,8),
resourceDn,
NEW.uid,
NEW.id,
NEW.cid
);
END;$$
delimiter ;

-- WORKITEM UPDATE
DROP TRIGGER IF EXISTS onBookshopUpdate;
delimiter $$
CREATE TRIGGER onBookshopUpdate BEFORE UPDATE ON bookshops FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE resourceDn VARCHAR(128);
DECLARE parentResourceDn VARCHAR(128);

IF(NEW.parent_id != OLD.parent_id) THEN
-- get parent dn
SELECT dn INTO parentDn from projects where id=NEW.parent_id;
SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');

SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
END IF;
END;$$
delimiter ;


-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/CadlibDao.php
-- **********************************************************************
DROP TRIGGER IF EXISTS onCadlibInsert;
delimiter $$
CREATE TRIGGER onCadlibInsert BEFORE INSERT ON cadlibs FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE resourceDn VARCHAR(128);
DECLARE parentResourceDn VARCHAR(128);

SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');

SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');

-- add resource
INSERT INTO `acl_resource`
(`id`,
`uid`,
`cn`,
`referToUid`,
`referToId`,
`referToCid`)
VALUES (
aclSequence(),
substr(uuid(),1,8),
resourceDn,
NEW.uid,
NEW.id,
NEW.cid
);
END;$$
delimiter ;

-- CADLIB UPDATE
DROP TRIGGER IF EXISTS onCadlibUpdate;
delimiter $$
CREATE TRIGGER onCadlibUpdate BEFORE UPDATE ON cadlibs FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE resourceDn VARCHAR(128);
DECLARE parentResourceDn VARCHAR(128);

IF(NEW.parent_id != OLD.parent_id) THEN
-- get parent dn
SELECT dn INTO parentDn from projects where id=NEW.parent_id;
SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');

SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
END IF;
END;$$
delimiter ;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/MockupDao.php
-- **********************************************************************
DROP TRIGGER IF EXISTS onMockupInsert;
delimiter $$
CREATE TRIGGER onMockupInsert BEFORE INSERT ON mockups FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE resourceDn VARCHAR(128);
DECLARE parentResourceDn VARCHAR(128);

SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');

SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');

-- add resource
INSERT INTO `acl_resource`
(`id`,
`uid`,
`cn`,
`referToUid`,
`referToId`,
`referToCid`)
VALUES (
aclSequence(),
substr(uuid(),1,8),
resourceDn,
NEW.uid,
NEW.id,
NEW.cid
);
END;$$
delimiter ;

-- WORKITEM UPDATE
DROP TRIGGER IF EXISTS onMockupUpdate;
delimiter $$
CREATE TRIGGER onMockupUpdate BEFORE UPDATE ON mockups FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE resourceDn VARCHAR(128);
DECLARE parentResourceDn VARCHAR(128);

IF(NEW.parent_id != OLD.parent_id) THEN
-- get parent dn
SELECT dn INTO parentDn from projects where id=NEW.parent_id;
SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');

SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
END IF;
END;$$
delimiter ;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/WorkitemDao.php
-- **********************************************************************

-- 
DROP TRIGGER IF EXISTS onWorkitemInsert;
DELIMITER $$
CREATE TRIGGER onWorkitemInsert BEFORE INSERT ON workitems FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE resourceDn VARCHAR(128);
DECLARE parentResourceDn VARCHAR(128);

SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');

SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');

-- add resource
INSERT INTO `acl_resource`(
`id`,
`uid`,
`cn`,
`referToUid`,
`referToId`,
`referToCid`)
VALUES (
aclSequence(),
substr(uuid(),1,8),
resourceDn,
NEW.uid,
NEW.id,
NEW.cid
);
END $$
DELIMITER ;

-- 
-- WORKITEM UPDATE
DROP TRIGGER IF EXISTS onWorkitemUpdate;
DELIMITER $$
CREATE TRIGGER onWorkitemUpdate BEFORE UPDATE ON workitems FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE resourceDn VARCHAR(128);
DECLARE parentResourceDn VARCHAR(128);

IF(NEW.parent_id != OLD.parent_id) THEN
-- get parent dn
SELECT dn INTO parentDn from projects where id=NEW.parent_id;
SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');

SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
END IF;
END;$$
DELIMITER ;

-- 
DROP PROCEDURE IF EXISTS `updateParentUid`;
DELIMITER $$
CREATE PROCEDURE `updateParentUid`()
BEGIN
UPDATE workitems SET parent_uid = (SELECT uid FROM projects WHERE id=workitems.parent_id) WHERE workitems.parent_uid IS NULL;
UPDATE cadlibs SET parent_uid = (SELECT uid FROM projects WHERE id=cadlibs.parent_id) WHERE cadlibs.parent_uid IS NULL;
UPDATE bookshops SET parent_uid = (SELECT uid FROM projects WHERE id=bookshops.parent_id) WHERE bookshops.parent_uid IS NULL;
UPDATE mockups SET parent_uid = (SELECT uid FROM projects WHERE id=mockups.parent_id) WHERE mockups.parent_uid IS NULL;
END $$
DELIMITER ;

-- 
DROP PROCEDURE IF EXISTS `updateWorkitemDn`;
DELIMITER $$
CREATE PROCEDURE `updateWorkitemDn`()
BEGIN
UPDATE workitems SET dn = CONCAT((SELECT dn FROM projects WHERE id=workitems.parent_id), uid,'/') WHERE workitems.dn IS NULL;
UPDATE cadlibs SET dn = CONCAT((SELECT dn FROM projects WHERE id=cadlibs.parent_id), uid,'/') WHERE cadlibs.dn IS NULL;
UPDATE bookshops SET dn = CONCAT((SELECT dn FROM projects WHERE id=bookshops.parent_id), uid,'/') WHERE bookshops.dn IS NULL;
UPDATE mockups SET dn = CONCAT((SELECT dn FROM projects WHERE id=mockups.parent_id), uid,'/') WHERE mockups.dn IS NULL;
END $$
DELIMITER ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/ProjectDao.php
-- **********************************************************************

DROP TRIGGER IF EXISTS `onProjectInsert`;
DELIMITER $$
CREATE TRIGGER `onProjectInsert` BEFORE INSERT ON `projects` FOR EACH ROW
BEGIN
DECLARE resourceDn VARCHAR(128);
DECLARE parentDn VARCHAR(128);

IF(NEW.parent_id IS NULL) THEN
SET NEW.dn = CONCAT('/',NEW.uid,'/');
set resourceDn = CONCAT('/app/ged/project', NEW.dn);
ELSE
-- get parent dn
SELECT dn INTO parentDn from projects where id=NEW.parent_id;
SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
set resourceDn = CONCAT('/app/ged/project', NEW.dn);
END IF;

-- add resource
INSERT INTO `acl_resource`
(`id`,
`uid`,
`cn`,
`referToUid`,
`referToId`,
`referToCid`)
VALUES (
aclSequence(),
substr(uuid(),1,8),
resourceDn,
NEW.uid,
NEW.id,
NEW.cid
);
END;$$
DELIMITER ;

DROP TRIGGER IF EXISTS `onProjectUpdate`;
DELIMITER $$
CREATE TRIGGER `onProjectUpdate` BEFORE UPDATE ON `projects` FOR EACH ROW
BEGIN
DECLARE resourceDn VARCHAR(128);
DECLARE parentDn VARCHAR(128);

IF(NEW.parent_id != OLD.parent_id) THEN
-- update parent dn
SELECT dn INTO parentDn from projects where id=NEW.parent_id;
SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
-- update resource
set resourceDn = CONCAT('/app/ged/project', NEW.dn);
UPDATE `acl_resource` SET `cn`=resourceDn WHERE referToId=NEW.id AND `referToCid`='569e93c6ee156';
END IF;
END;$$
DELIMITER ;


DROP TRIGGER IF EXISTS `onProjectDelete`;
DELIMITER $$
CREATE TRIGGER `onProjectDelete` AFTER DELETE ON `projects` FOR EACH ROW
BEGIN
DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
DELIMITER ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/FavoriteDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/UnitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/CategoryDao.php
-- **********************************************************************

DROP TRIGGER IF EXISTS onOrgCategoryRelInsert;
delimiter $$
CREATE TRIGGER onProjectCategoryRelInsert BEFORE INSERT ON project_category_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE parentUid VARCHAR(128);
DECLARE childUid VARCHAR(128);
SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SELECT uid INTO childUid FROM categories WHERE id=NEW.child_id;
SET NEW.rdn = parentDn;
SET NEW.parent_uid = parentUid;
SET NEW.child_uid = childUid;
END;$$
delimiter ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ProcessDao.php
-- **********************************************************************

DROP TRIGGER IF EXISTS onProjectDoctypeRelInsert;
delimiter $$
CREATE TRIGGER onProjectDoctypeRelInsert BEFORE INSERT ON project_process_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE parentUid VARCHAR(128);
DECLARE childUid VARCHAR(128);
SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SELECT uid INTO childUid FROM wf_process WHERE id=NEW.child_id;
SET NEW.rdn = parentDn;
SET NEW.parent_uid = parentUid;
SET NEW.child_uid = childUid;
END;$$
delimiter ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ContainerDao.php
-- **********************************************************************

-- TRIGGER --
DROP TRIGGER IF EXISTS onOrgContainerRelInsert;
delimiter $$
CREATE TRIGGER onOrgContainerRelInsert BEFORE INSERT ON project_container_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE parentUid VARCHAR(128);
DECLARE childUid VARCHAR(128);
SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
CASE NEW.spacename
WHEN 'bookshop' THEN
SELECT uid INTO childUid FROM bookshops WHERE id=NEW.child_id;
WHEN 'cadlib' THEN
SELECT uid INTO childUid FROM cadlibs WHERE id=NEW.child_id;
WHEN 'mockup' THEN
SELECT uid INTO childUid FROM mockups WHERE id=NEW.child_id;
WHEN 'workitem' THEN
SELECT uid INTO childUid FROM workitems WHERE id=NEW.child_id;
END CASE;

SET NEW.rdn = parentDn;
SET NEW.parent_uid = parentUid;
SET NEW.child_uid = childUid;
END;$$
delimiter ;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/DoctypeDao.php
-- **********************************************************************

DROP TRIGGER IF EXISTS onProjectDoctypeRelInsert;
delimiter $$
CREATE TRIGGER onProjectDoctypeRelInsert BEFORE INSERT ON project_doctype_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE parentUid VARCHAR(128);
DECLARE childUid VARCHAR(128);
SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SELECT uid INTO childUid FROM doctypes WHERE id=NEW.child_id;
SET NEW.rdn = parentDn;
SET NEW.parent_uid = parentUid;
SET NEW.child_uid = childUid;
END;$$
delimiter ;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/PropertyDao.php
-- **********************************************************************

-- TRIGGER --
DROP TRIGGER IF EXISTS onOrgPropertyRelInsert;
delimiter $$
CREATE TRIGGER onOrgPropertyRelInsert BEFORE INSERT ON project_property_rel FOR EACH ROW
BEGIN
DECLARE parentDn VARCHAR(128);
DECLARE parentUid VARCHAR(128);
DECLARE childUid VARCHAR(128);
SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
SELECT uid INTO childUid FROM doctypes WHERE id=NEW.child_id;
SET NEW.rdn = parentDn;
SET NEW.parent_uid = parentUid;
SET NEW.child_uid = childUid;
END;$$
delimiter ;


-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/NotificationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/IndexDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Change/src/Change/Dao/ChangeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Sier/src/Sier/Model/Como/ComoDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Discussion/src/Discussion/Dao/CommentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/FilelistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabpanelDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ComponentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/IndicatorDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/SimplegridDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/StatusboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ProjectDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/ItemDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/SearchEngineDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/JalonDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/DocumentlistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TinymceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/CheckboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/BoxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Application/src/Application/Cron/schemaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbgate/src/Rbgate/Logger/LogDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Import/src/Import/Model/PackageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/TypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/Template/MaskDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Admin/src/Admin/Dao/ToolDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaProjectLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Model/IndexerDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Engine/SearchDao.php
-- **********************************************************************
CREATE OR REPLACE VIEW `objects` AS
SELECT
`obj`.`id` AS `id`,
`obj`.`uid` AS `uid`,
`obj`.`number` AS `number`,
`obj`.`name` AS `name`,
`obj`.`designation` AS `designation`,
`obj`.`version` AS `version`,
`obj`.`iteration` AS `iteration`,
`obj`.`doctype_id` AS `doctypeId`,
`obj`.`lock_by_id` AS `lockById`,
`obj`.`locked` AS `locked`,
`obj`.`updated` AS `updated`,
`obj`.`update_by_id` AS `updateById`,
`obj`.`created` AS `created`,
`obj`.`create_by_id` AS `createById`,
'workitem' AS `spacename`,
'569e92709feb6' AS `cid`
FROM `workitem_documents` `obj`
UNION SELECT
`obj`.`id` AS `id`,
`obj`.`uid` AS `uid`,
`obj`.`number` AS `number`,
`obj`.`name` AS `name`,
`obj`.`designation` AS `designation`,
`obj`.`version` AS `version`,
`obj`.`iteration` AS `iteration`,
`obj`.`doctype_id` AS `doctypeId`,
`obj`.`lock_by_id` AS `lockById`,
`obj`.`locked` AS `locked`,
`obj`.`updated` AS `updated`,
`obj`.`update_by_id` AS `updateById`,
`obj`.`created` AS `created`,
`obj`.`create_by_id` AS `createById`,
'bookshop' AS `spacename`,
'569e92709feb6' AS `cid`
FROM `bookshop_documents` `obj`
UNION SELECT
`obj`.`id` AS `id`,
`obj`.`uid` AS `uid`,
`obj`.`number` AS `number`,
`obj`.`name` AS `name`,
`obj`.`designation` AS `designation`,
`obj`.`version` AS `version`,
`obj`.`iteration` AS `iteration`,
`obj`.`doctype_id` AS `doctypeId`,
`obj`.`lock_by_id` AS `lockById`,
`obj`.`locked` AS `locked`,
`obj`.`updated` AS `updated`,
`obj`.`update_by_id` AS `updateById`,
`obj`.`created` AS `created`,
`obj`.`create_by_id` AS `createById`,
'cadlib' AS `spacename`,
'569e92709feb6' AS `cid`
FROM `cadlib_documents` `obj`
UNION SELECT
`obj`.`id` AS `id`,
`obj`.`uid` AS `uid`,
`obj`.`number` AS `number`,
`obj`.`name` AS `name`,
`obj`.`designation` AS `designation`,
`obj`.`version` AS `version`,
`obj`.`iteration` AS `iteration`,
`obj`.`doctype_id` AS `doctypeId`,
`obj`.`lock_by_id` AS `lockById`,
`obj`.`locked` AS `locked`,
`obj`.`updated` AS `updated`,
`obj`.`update_by_id` AS `updateById`,
`obj`.`created` AS `created`,
`obj`.`create_by_id` AS `createById`,
'mockup' AS `spacename`,
'569e92709feb6' AS `cid`
FROM `mockup_documents` `obj`
UNION SELECT
`obj`.`id` AS `id`,
`obj`.`uid` AS `uid`,
`obj`.`uid` AS `number`,
`obj`.`name` AS `name`,
'' AS `designation`,
'' AS `version`,
`obj`.`iteration` AS `iteration`,
-1 AS `doctypeId`,
`obj`.`lock_by_id` AS `lockById`,
`obj`.`locked` AS `locked`,
`obj`.`updated` AS `updated`,
`obj`.`update_by_id` AS `updateById`,
`obj`.`created` AS `created`,
`obj`.`create_by_id` AS `createById`,
'workitem' AS `spacename`,
'569e92b86d248' AS `cid`
FROM `workitem_doc_files` `obj`
UNION
SELECT
`obj`.`id` AS `id`,
`obj`.`uid` AS `uid`,
`obj`.`uid` AS `number`,
`obj`.`name` AS `name`,
'' AS `designation`,
'' AS `version`,
`obj`.`iteration` AS `iteration`,
-1 AS `doctypeId`,
`obj`.`lock_by_id` AS `lockById`,
`obj`.`locked` AS `locked`,
`obj`.`updated` AS `updated`,
`obj`.`update_by_id` AS `updateById`,
`obj`.`created` AS `created`,
`obj`.`create_by_id` AS `createById`,
'bookshop' AS `spacename`,
'569e92b86d248' AS `cid`
FROM `bookshop_doc_files` `obj`
UNION
SELECT
`obj`.`id` AS `id`,
`obj`.`uid` AS `uid`,
`obj`.`uid` AS `number`,
`obj`.`name` AS `name`,
'' AS `designation`,
'' AS `version`,
`obj`.`iteration` AS `iteration`,
-1 AS `doctypeId`,
`obj`.`lock_by_id` AS `lockById`,
`obj`.`locked` AS `locked`,
`obj`.`updated` AS `updated`,
`obj`.`update_by_id` AS `updateById`,
`obj`.`created` AS `created`,
`obj`.`create_by_id` AS `createById`,
'cadlib' AS `spacename`,
'569e92b86d248' AS `cid`
FROM `cadlib_doc_files` `obj`
UNION
SELECT
`obj`.`id` AS `id`,
`obj`.`uid` AS `uid`,
`obj`.`uid` AS `number`,
`obj`.`name` AS `name`,
'' AS `designation`,
'' AS `version`,
`obj`.`iteration` AS `iteration`,
-1 AS `doctypeId`,
`obj`.`lock_by_id` AS `lockById`,
`obj`.`locked` AS `locked`,
`obj`.`updated` AS `updated`,
`obj`.`update_by_id` AS `updateById`,
`obj`.`created` AS `created`,
`obj`.`create_by_id` AS `createById`,
'mockup' AS `spacename`,
'569e92b86d248' AS `cid`
FROM `mockup_doc_files` `obj`
UNION
SELECT
`workitems`.`id` AS `id`,
`workitems`.`uid` AS `uid`,
`workitems`.`number` AS `number`,
`workitems`.`name` AS `name`,
`workitems`.`designation` AS `designation`,
'' AS `version`,
'' AS `iteration`,
-10 AS `doctypeId`,
'' AS `lockById`,
'' AS `locked`,
'' AS `updated`,
'' AS `updateById`,
`workitems`.`created` AS `created`,
`workitems`.`create_by_id` AS `createById`,
'workitem' AS `spacename`,
'569e94192201a' AS `cid`
FROM `workitems`
UNION
SELECT
`bookshops`.`id` AS `id`,
`bookshops`.`uid` AS `uid`,
`bookshops`.`number` AS `number`,
`bookshops`.`name` AS `name`,
`bookshops`.`designation` AS `designation`,
'' AS `version`,
'' AS `iteration`,
-10 AS `doctypeId`,
'' AS `lockById`,
'' AS `locked`,
'' AS `updated`,
'' AS `updateById`,
`bookshops`.`created` AS `created`,
`bookshops`.`create_by_id` AS `createById`,
'bookshop' AS `spacename`,
'569e94192201a' AS `cid`
FROM `bookshops`
UNION
SELECT
`cadlibs`.`id` AS `id`,
`cadlibs`.`uid` AS `uid`,
`cadlibs`.`number` AS `number`,
`cadlibs`.`name` AS `name`,
`cadlibs`.`designation` AS `designation`,
'' AS `version`,
'' AS `iteration`,
-10 AS `doctypeId`,
'' AS `lockById`,
'' AS `locked`,
'' AS `updated`,
'' AS `updateById`,
`cadlibs`.`created` AS `created`,
`cadlibs`.`create_by_id` AS `createById`,
'cadlib' AS `spacename`,
'569e94192201a' AS `cid`
FROM `cadlibs`
UNION
SELECT
`mockups`.`id` AS `id`,
`mockups`.`uid` AS `uid`,
`mockups`.`number` AS `number`,
`mockups`.`name` AS `name`,
`mockups`.`designation` AS `designation`,
'' AS `version`,
'' AS `iteration`,
-10 AS `doctypeId`,
'' AS `lockById`,
'' AS `locked`,
'' AS `updated`,
'' AS `updateById`,
`mockups`.`created` AS `created`,
`mockups`.`create_by_id` AS `createById`,
'mockup' AS `spacename`,
'569e94192201a' AS `cid`
FROM `mockups`
UNION
SELECT
`projects`.`id` AS `id`,
`projects`.`uid` AS `uid`,
`projects`.`number` AS `number`,
`projects`.`name` AS `name`,
`projects`.`designation` AS `designation`,
'' AS `version`,
'' AS `iteration`,
-15 AS `doctypeId`,
'' AS `lockById`,
'' AS `locked`,
'' AS `updated`,
'' AS `updateById`,
`projects`.`created` AS `created`,
`projects`.`create_by_id` AS `createById`,
'default' AS `spacename`,
'569e93c6ee156' AS `cid`
FROM `projects`
UNION
SELECT
`pdm_product_version`.`id` AS `id`,
`pdm_product_version`.`uid` AS `uid`,
`pdm_product_version`.`number` AS `number`,
`pdm_product_version`.`name` AS `name`,
`pdm_product_version`.`description` AS `designation`,
`pdm_product_version`.`version` AS `version`,
'' AS `iteration`,
-20 AS `doctypeId`,
'' AS `lockById`,
'' AS `locked`,
'' AS `updated`,
'' AS `updateById`,
'' AS `created`,
'' AS `createById`,
`pdm_product_version`.`spacename` AS `spacename`,
'569e972dd4c2c' AS `cid`
FROM `pdm_product_version`
UNION
SELECT
`pdm_product_instance`.`id` AS `id`,
`pdm_product_instance`.`uid` AS `uid`,
`pdm_product_instance`.`number` AS `number`,
`pdm_product_instance`.`name` AS `name`,
`pdm_product_instance`.`description` AS `designation`,
'' AS `version`,
'' AS `iteration`,
-21 AS `doctypeId`,
'' AS `lockById`,
'' AS `locked`,
'' AS `updated`,
'' AS `updateById`,
'' AS `created`,
'' AS `createById`,
'default' AS `spacename`,
'569e972dd4c2c' AS `cid`
FROM `pdm_product_instance`;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/ResourceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RuleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RightDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/UserRoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/ClassDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/PropsetDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/History/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Typemime/TypemimeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Extended/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Postit/PostitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/EndDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StartDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StandaloneDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/AswitchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/JoinDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/SplitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/TransitionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/InstanceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/RoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CheckoutIndexDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocfileDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocumentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/PdmLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/BasketDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/IndiceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/Share/PublicUrlDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/SpaceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/CategoryDao.php
-- **********************************************************************

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/DoctypeDao.php
-- **********************************************************************
CREATE VIEW view_workitem_doctype_rel AS
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM workitem_doctype_rel
UNION
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;

DROP VIEW IF EXISTS `view_bookshop_doctype_rel`;
CREATE VIEW view_bookshop_doctype_rel AS
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM bookshop_doctype_rel
UNION
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;

DROP VIEW IF EXISTS `view_cadlib_doctype_rel`;
CREATE VIEW view_cadlib_doctype_rel AS
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM cadlib_doctype_rel
UNION
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;

DROP VIEW IF EXISTS `view_mockup_doctype_rel`;
CREATE VIEW view_mockup_doctype_rel AS
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM mockup_doctype_rel
UNION
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/History/BatchDao.php
-- **********************************************************************

DROP VIEW IF EXISTS `workitem_batches`;
CREATE VIEW `workitem_batches` AS
SELECT DISTINCT
`action_batch_uid` AS `uid`,
`action_name` AS `callback_method`,
`action_by` AS `owner_uid`,
`action_started` AS `created`,
`action_comment` AS `comment`,
'workitem' AS `spacename`
FROM
`workitem_documents_history`
WHERE
`action_batch_uid` IS NOT NULL;

DROP VIEW IF EXISTS `bookshop_batches`;
CREATE VIEW `bookshop_batches` AS
SELECT
`action_batch_uid` AS `uid`,
`action_name` AS `callback_method`,
`action_by` AS `owner_uid`,
`action_started` AS `created`,
`action_comment` AS `comment`,
'bookshop' AS `spacename`
FROM
`bookshop_documents_history`
WHERE
`action_batch_uid` IS NOT NULL;

DROP VIEW IF EXISTS `cadlib_batches`;
CREATE VIEW `cadlib_batches` AS
SELECT
`action_batch_uid` AS `uid`,
`action_name` AS `callback_method`,
`action_by` AS `owner_uid`,
`action_started` AS `created`,
`action_comment` AS `comment`,
'cadlib' AS `spacename`
FROM
`cadlib_documents_history`
WHERE
`action_batch_uid` IS NOT NULL;

DROP VIEW IF EXISTS `mockup_batches`;
CREATE VIEW `mockup_batches` AS
SELECT
`action_batch_uid` AS `uid`,
`action_name` AS `callback_method`,
`action_by` AS `owner_uid`,
`action_started` AS `created`,
`action_comment` AS `comment`,
'mockup' AS `spacename`
FROM
`mockup_documents_history`
WHERE
`action_batch_uid` IS NOT NULL;

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/MessageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/ArchiveDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/SentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/MailboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/GroupDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/UserDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/PreferenceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/SessionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/PasswordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/SomebodyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Tag/TagDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/ProductDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ConceptDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ContextDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/InstanceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/UsageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Observers/CallbackFactoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/JobDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/CronTaskDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RepositDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/DistantSiteDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/ReplicatedDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/MissingFileDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RecordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/BookshopDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/CadlibDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/MockupDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/WorkitemDao.php
-- **********************************************************************

DROP VIEW IF EXISTS `view_containers`;
CREATE VIEW `view_containers` AS
SELECT
`workitems`.`id`,
`workitems`.`number`,
`workitems`.`name`,
`workitems`.`uid`,
`workitems`.`cid`,
`workitems`.`dn`,
`workitems`.`life_stage`,
`workitems`.`designation`,
`workitems`.`file_only`,
`workitems`.`acode`,
`workitems`.`version`,
`workitems`.`parent_id`,
`workitems`.`parent_uid`,
`workitems`.`default_process_id`,
`workitems`.`default_file_path`,
`workitems`.`reposit_id`,
`workitems`.`reposit_uid`,
`workitems`.`created`,
`workitems`.`create_by_id`,
`workitems`.`create_by_uid`,
`workitems`.`planned_closure`,
`workitems`.`closed`,
`workitems`.`close_by_id`,
`workitems`.`close_by_uid`,
`workitems`.`spacename`
FROM `workitems`
UNION
SELECT
`bookshops`.`id`,
`bookshops`.`number`,
`bookshops`.`name`,
`bookshops`.`uid`,
`bookshops`.`cid`,
`bookshops`.`dn`,
`bookshops`.`life_stage`,
`bookshops`.`designation`,
`bookshops`.`file_only`,
`bookshops`.`acode`,
`bookshops`.`version`,
`bookshops`.`parent_id`,
`bookshops`.`parent_uid`,
`bookshops`.`default_process_id`,
`bookshops`.`default_file_path`,
`bookshops`.`reposit_id`,
`bookshops`.`reposit_uid`,
`bookshops`.`created`,
`bookshops`.`create_by_id`,
`bookshops`.`create_by_uid`,
`bookshops`.`planned_closure`,
`bookshops`.`closed`,
`bookshops`.`close_by_id`,
`bookshops`.`close_by_uid`,
`bookshops`.`spacename`
FROM `bookshops`
UNION
SELECT
`cadlibs`.`id`,
`cadlibs`.`number`,
`cadlibs`.`name`,
`cadlibs`.`uid`,
`cadlibs`.`cid`,
`cadlibs`.`dn`,
`cadlibs`.`life_stage`,
`cadlibs`.`designation`,
`cadlibs`.`file_only`,
`cadlibs`.`acode`,
`cadlibs`.`version`,
`cadlibs`.`parent_id`,
`cadlibs`.`parent_uid`,
`cadlibs`.`default_process_id`,
`cadlibs`.`default_file_path`,
`cadlibs`.`reposit_id`,
`cadlibs`.`reposit_uid`,
`cadlibs`.`created`,
`cadlibs`.`create_by_id`,
`cadlibs`.`create_by_uid`,
`cadlibs`.`planned_closure`,
`cadlibs`.`closed`,
`cadlibs`.`close_by_id`,
`cadlibs`.`close_by_uid`,
`cadlibs`.`spacename`
FROM `cadlibs`
UNION
SELECT
`mockups`.`id`,
`mockups`.`number`,
`mockups`.`name`,
`mockups`.`uid`,
`mockups`.`cid`,
`mockups`.`dn`,
`mockups`.`life_stage`,
`mockups`.`designation`,
`mockups`.`file_only`,
`mockups`.`acode`,
`mockups`.`version`,
`mockups`.`parent_id`,
`mockups`.`parent_uid`,
`mockups`.`default_process_id`,
`mockups`.`default_file_path`,
`mockups`.`reposit_id`,
`mockups`.`reposit_uid`,
`mockups`.`created`,
`mockups`.`create_by_id`,
`mockups`.`create_by_uid`,
`mockups`.`planned_closure`,
`mockups`.`closed`,
`mockups`.`close_by_id`,
`mockups`.`close_by_uid`,
`mockups`.`spacename`
FROM `mockups`;
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/ProjectDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/FavoriteDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/UnitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ContainerDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/NotificationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/IndexDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Change/src/Change/Dao/ChangeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Sier/src/Sier/Model/Como/ComoDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Discussion/src/Discussion/Dao/CommentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/FilelistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabpanelDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ComponentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/IndicatorDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/SimplegridDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/StatusboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ProjectDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/ItemDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/SearchEngineDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/JalonDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/DocumentlistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TinymceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/CheckboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/BoxDao.php
-- **********************************************************************
