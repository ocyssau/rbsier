
-- Get resourceCn of object uid, if not exists return resourceCn of parent
DROP FUNCTION IF EXISTS getResourceCnFromReferUid;
DELIMITER $$
CREATE FUNCTION `getResourceCnFromReferUid`(_uid VARCHAR(128))
RETURNS varchar(256)
BEGIN
	DECLARE resourceCn varchar(256);

	SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = _uid;
    IF(resourceCn IS NULL) THEN
		SELECT cn INTO resourceCn FROM acl_resource AS resource
		JOIN acl_resource_breakdown as breakdown ON resource.referToUid = breakdown.parent_uid
		WHERE breakdown.uid = _uid;
	END IF;

	return resourceCn;
END$$
DELIMITER ;

-- Get resourceCn of object Id and Cid, if not exists return resourceCn of parent
DROP FUNCTION IF EXISTS getResourceCnFromReferIdAndCid;
DELIMITER $$
CREATE FUNCTION `getResourceCnFromReferIdAndCid`(_id INT(11), _cid VARCHAR(128))
RETURNS varchar(256)
BEGIN
	DECLARE resourceCn varchar(256);

	SELECT cn INTO resourceCn FROM acl_resource WHERE referToId = _id AND referToCid = _cid;
    IF(resourceCn IS NULL) THEN
		SELECT cn INTO resourceCn FROM acl_resource AS resource
		JOIN acl_resource_breakdown as breakdown ON resource.referToUid = breakdown.parent_uid
		WHERE breakdown.id = _id AND breakdown.cid=_cid;
	END IF;

	return resourceCn;
END$$
DELIMITER ;

-- build the cn path
DROP FUNCTION IF EXISTS build_path_withuid;
DELIMITER $$
CREATE FUNCTION `build_path_withuid`(_uid VARCHAR(64))
RETURNS text
BEGIN
    DECLARE parentUid VARCHAR(64);
    DECLARE cid VARCHAR(64);
    DECLARE message VARCHAR(256);
    set @ret = '';
    
    while _uid is not null do
      SELECT parent_uid, cid INTO parentUid, cid 
      FROM acl_resource_breakdown 
      WHERE uid = _uid;

    IF(found_rows() != 1) THEN
      set message = CONCAT('uid not found in table acl_resource_breakdown :', _uid);
      SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = message;
    END IF;
    
	IF(parentUid IS NULL) THEN
		set @ret := CONCAT('/app/ged/project/', _uid, @ret);
	ELSE
		set @ret := CONCAT('/', _uid, @ret);
	END IF;

	set _uid := parentUid;
    end while;

    RETURN @ret;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS updateResourceBreakdown;
DELIMITER $$
CREATE PROCEDURE `updateResourceBreakdown`()
BEGIN
DROP TABLE IF EXISTS acl_resource_breakdown;
	CREATE TABLE acl_resource_breakdown as
	    (SELECT 
	        `doc`.`id` AS `id`,
	        `doc`.`uid` AS `uid`,
	        `doc`.`name` AS `name`,
	        `doc`.`cid` AS `cid`,
	        `doc`.`container_id` AS `parent_id`,
	        `doc`.`container_uid` AS `parent_uid`
	    FROM
	        `workitem_documents` `doc`) UNION (SELECT 
	        `doc`.`id` AS `id`,
	        `doc`.`uid` AS `uid`,
	        `doc`.`name` AS `name`,
	        `doc`.`cid` AS `cid`,
	        `doc`.`container_id` AS `parent_id`,
	        `doc`.`container_uid` AS `parent_uid`
	    FROM
	        `bookshop_documents` `doc`) UNION (SELECT 
	        `doc`.`id` AS `id`,
	        `doc`.`uid` AS `uid`,
	        `doc`.`name` AS `name`,
	        `doc`.`cid` AS `cid`,
	        `doc`.`container_id` AS `parent_id`,
	        `doc`.`container_uid` AS `parent_uid`
	    FROM
	        `cadlib_documents` `doc`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `workitems` `cont`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `cadlibs` `cont`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `bookshops` `cont`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `mockups` `cont`) UNION (SELECT 
	        `projects`.`id` AS `id`,
	        `projects`.`uid` AS `uid`,
	        `projects`.`name` AS `name`,
	        `projects`.`cid` AS `cid`,
	        `projects`.`parent_id` AS `parent_id`,
	        `projects`.`parent_uid` AS `parent_uid`
	    FROM
	        `projects`);
	        
	ALTER TABLE acl_resource_breakdown 
	ADD KEY (`id`),
	ADD KEY (`uid`),
	ADD KEY (`name`),
	ADD KEY (`cid`),
	ADD KEY (`parent_id`),
	ADD KEY (`parent_uid`);

	END$$
DELIMITER ;

call updateResourceBreakdown();


DROP PROCEDURE IF EXISTS updateDocfileDocumentUidRelation;
DELIMITER $$
CREATE PROCEDURE `updateDocfileDocumentUidRelation`()
BEGIN
	#### WORKITEM
	DROP TEMPORARY TABLE IF EXISTS tmptableworkitem;
	CREATE TEMPORARY TABLE tmptableworkitem AS (
		SELECT df.* FROM workitem_doc_files as df
		LEFT OUTER JOIN workitem_documents as doc ON df.document_uid=doc.uid
		WHERE doc.id IS NULL
	);
	DROP TABLE IF EXISTS workitem_doc_files_backup;
	CREATE TABLE workitem_doc_files_backup AS(
		SELECT * FROM workitem_doc_files
	);
	UPDATE workitem_doc_files as df 
	SET document_uid=(SELECT uid FROM workitem_documents WHERE id=df.document_id)
	WHERE df.id IN (SELECT id FROM tmptableworkitem);
	
	#### BOOKSHOP
	DROP TEMPORARY TABLE IF EXISTS tmptablebookshop;
	CREATE TEMPORARY TABLE tmptablebookshop AS (
		SELECT df.* FROM bookshop_doc_files as df
		LEFT OUTER JOIN bookshop_documents as doc ON df.document_uid=doc.uid
		WHERE doc.id IS NULL
	);
	DROP TABLE IF EXISTS bookshop_doc_files_backup;
	CREATE TABLE bookshop_doc_files_backup AS(
		SELECT * FROM bookshop_doc_files
	);
	UPDATE bookshop_doc_files as df 
	SET document_uid=(SELECT uid FROM bookshop_documents WHERE id=df.document_id)
	WHERE df.id IN (SELECT id FROM tmptablebookshop);
	END$$
DELIMITER ;

call updateDocfileDocumentUidRelation();


INSERT INTO schema_version(version,lastModification) VALUES('1.1.8',now());
-- select * from schema_version ORDER BY version ASC;
