-- update uid with normalize 13char

-- SELECT * FROM workitem_documents AS doc WHERE LENGTH(doc.uid) > 13;
-- SELECT * FROM bookshop_documents AS doc WHERE LENGTH(doc.uid) > 13;

INSERT INTO acl_resource (id, cn, cid, uid, referToId, referToUid, referToCid) SELECT 
	aclSequence() as id,
    SUBSTR(UUID(),1,8) as cn,
	'aclresource60' as cid,
    SUBSTR(UUID(),1,8) as uid,
	doc.id as referToId,
	doc.uid as referToUid, 
	doc.cid as referToCid 
    FROM workitem_documents as doc;
   
-- WORKITEM ----------------------
UPDATE workitem_documents AS doc SET uid=CONCAT(SUBSTR(UUID(),1,8), SUBSTR(UUID(),1,5)) WHERE LENGTH(doc.uid) > 13;
UPDATE workitem_doc_files AS df SET uid=CONCAT(SUBSTR(UUID(),1,8), SUBSTR(UUID(),1,5)) WHERE LENGTH(df.uid) > 13;

-- SELECT * FROM `workitem_documents`;
-- SELECT * FROM `workitem_doc_files`;

UPDATE `workitem_doc_files` AS l SET `document_uid`=(SELECT uid FROM workitem_documents WHERE id=l.document_id LIMIT 1);
UPDATE `wf_document_link` AS l SET `parentUid`=(SELECT uid FROM workitem_documents WHERE id=l.parentId LIMIT 1) WHERE spacename = 'workitem';

-- SELECT * FROM `wf_document_link`;
-- SELECT * FROM `workitem_doc_files`;

UPDATE `share_public_url` AS l SET `document_uid`=(SELECT uid FROM workitem_documents WHERE id=l.document_id LIMIT 1) WHERE spacename = 'workitem';

UPDATE `pdm_product_version` AS l SET `document_uid`=(SELECT uid FROM workitem_documents WHERE id=l.document_id LIMIT 1) WHERE spacename = 'workitem';

UPDATE `notifications` AS l SET `reference_uid`=(SELECT uid FROM workitem_documents WHERE id=l.reference_id LIMIT 1) WHERE spacename = 'workitem';
-- SELECT * FROM `notifications`;

UPDATE `checkout_index` AS l SET `file_uid`=(SELECT uid FROM workitem_doc_files WHERE id=l.file_id LIMIT 1) WHERE spacename = 'workitem';

-- CADLIB ----------------------

UPDATE cadlib_documents AS doc SET uid=CONCAT(SUBSTR(UUID(),1,8), SUBSTR(UUID(),1,5)) WHERE LENGTH(doc.uid) > 13;
UPDATE cadlib_doc_files AS df SET uid=CONCAT(SUBSTR(UUID(),1,8), SUBSTR(UUID(),1,5)) WHERE LENGTH(df.uid) > 13;

-- SELECT * FROM `cadlib_documents`;
-- SELECT * FROM `cadlib_doc_files`;

UPDATE `cadlib_doc_files` AS l SET `document_uid`=(SELECT uid FROM cadlib_documents WHERE id=l.document_id LIMIT 1);
UPDATE `wf_document_link` AS l SET `parentUid`=(SELECT uid FROM cadlib_documents WHERE id=l.parentId LIMIT 1) WHERE spacename = 'cadlib';

-- SELECT * FROM `wf_document_link`;
-- SELECT * FROM `cadlib_doc_files`;

UPDATE `share_public_url` AS l SET `document_uid`=(SELECT uid FROM cadlib_documents WHERE id=l.document_id LIMIT 1) WHERE spacename = 'cadlib';

UPDATE `pdm_product_version` AS l SET `document_uid`=(SELECT uid FROM cadlib_documents WHERE id=l.document_id LIMIT 1) WHERE spacename = 'cadlib';

UPDATE `notifications` AS l SET `reference_uid`=(SELECT uid FROM cadlib_documents WHERE id=l.reference_id LIMIT 1) WHERE spacename = 'cadlib';
-- SELECT * FROM `notifications`;

UPDATE `checkout_index` AS l SET `file_uid`=(SELECT uid FROM cadlib_doc_files WHERE id=l.file_id LIMIT 1) WHERE spacename = 'cadlib';



-- BOOKSHOP ----------------------

UPDATE bookshop_documents AS doc SET uid=CONCAT(SUBSTR(UUID(),1,8), SUBSTR(UUID(),1,5)) WHERE LENGTH(doc.uid) > 13;
UPDATE bookshop_doc_files AS df SET uid=CONCAT(SUBSTR(UUID(),1,8), SUBSTR(UUID(),1,5)) WHERE LENGTH(df.uid) > 13;

-- SELECT * FROM `bookshop_documents`;
-- SELECT * FROM `bookshop_doc_files`;

UPDATE `bookshop_doc_files` AS l SET `document_uid`=(SELECT uid FROM bookshop_documents WHERE id=l.document_id LIMIT 1);
UPDATE `wf_document_link` AS l SET `parentUid`=(SELECT uid FROM bookshop_documents WHERE id=l.parentId LIMIT 1) WHERE spacename = 'bookshop';

-- SELECT * FROM `wf_document_link`;
-- SELECT * FROM `bookshop_doc_files`;

UPDATE `share_public_url` AS l SET `document_uid`=(SELECT uid FROM bookshop_documents WHERE id=l.document_id LIMIT 1) WHERE spacename = 'bookshop';

UPDATE `pdm_product_version` AS l SET `document_uid`=(SELECT uid FROM bookshop_documents WHERE id=l.document_id LIMIT 1) WHERE spacename = 'bookshop';

UPDATE `notifications` AS l SET `reference_uid`=(SELECT uid FROM bookshop_documents WHERE id=l.reference_id LIMIT 1) WHERE spacename = 'bookshop';
-- SELECT * FROM `notifications`;

UPDATE `checkout_index` AS l SET `file_uid`=(SELECT uid FROM bookshop_doc_files WHERE id=l.file_id LIMIT 1) WHERE spacename = 'bookshop';


-- MOCKUP ----------------------

UPDATE mockup_documents AS doc SET uid=CONCAT(SUBSTR(UUID(),1,8), SUBSTR(UUID(),1,5)) WHERE LENGTH(doc.uid) > 13;
UPDATE mockup_doc_files AS df SET uid=CONCAT(SUBSTR(UUID(),1,8), SUBSTR(UUID(),1,5)) WHERE LENGTH(df.uid) > 13;

-- SELECT * FROM `mockup_documents`;
-- SELECT * FROM `mockup_doc_files`;

UPDATE `mockup_doc_files` AS l SET `document_uid`=(SELECT uid FROM mockup_documents WHERE id=l.document_id LIMIT 1);
UPDATE `wf_document_link` AS l SET `parentUid`=(SELECT uid FROM mockup_documents WHERE id=l.parentId LIMIT 1) WHERE spacename = 'mockup';

-- SELECT * FROM `wf_document_link`;
-- SELECT * FROM `mockup_doc_files`;

UPDATE `share_public_url` AS l SET `document_uid`=(SELECT uid FROM mockup_documents WHERE id=l.document_id LIMIT 1) WHERE spacename = 'mockup';

UPDATE `pdm_product_version` AS l SET `document_uid`=(SELECT uid FROM mockup_documents WHERE id=l.document_id LIMIT 1) WHERE spacename = 'mockup';

UPDATE `notifications` AS l SET `reference_uid`=(SELECT uid FROM mockup_documents WHERE id=l.reference_id LIMIT 1) WHERE spacename = 'mockup';
-- SELECT * FROM `notifications`;

UPDATE `checkout_index` AS l SET `file_uid`=(SELECT uid FROM mockup_doc_files WHERE id=l.file_id LIMIT 1) WHERE spacename = 'mockup';

INSERT INTO schema_version(version,lastModification) VALUES('1.1',now());
-- select * from schema_version ORDER BY version ASC;
