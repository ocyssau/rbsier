ALTER TABLE `workitem_alias` 
ADD COLUMN `created` DATETIME NULL DEFAULT NULL AFTER `cid`,
ADD COLUMN `create_by_id` INTEGER(11) NULL DEFAULT NULL AFTER `created`,
ADD COLUMN `create_by_uid` VARCHAR(512) NULL DEFAULT NULL AFTER `create_by_id`;

ALTER TABLE `cadlib_alias` 
ADD COLUMN `created` DATETIME NULL DEFAULT NULL AFTER `cid`,
ADD COLUMN `create_by_id` INTEGER(11) NULL DEFAULT NULL AFTER `created`,
ADD COLUMN `create_by_uid` VARCHAR(512) NULL DEFAULT NULL AFTER `create_by_id`;

ALTER TABLE `bookshop_alias` 
ADD COLUMN `created` DATETIME NULL DEFAULT NULL AFTER `cid`,
ADD COLUMN `create_by_id` INTEGER(11) NULL DEFAULT NULL AFTER `created`,
ADD COLUMN `create_by_uid` VARCHAR(512) NULL DEFAULT NULL AFTER `create_by_id`;

ALTER TABLE `mockup_alias` 
ADD COLUMN `created` DATETIME NULL DEFAULT NULL AFTER `cid`,
ADD COLUMN `create_by_id` INTEGER(11) NULL DEFAULT NULL AFTER `created`,
ADD COLUMN `create_by_uid` VARCHAR(512) NULL DEFAULT NULL AFTER `create_by_id`;

INSERT INTO schema_version(version,lastModification) VALUES('1.1.3',now());
-- select * from schema_version ORDER BY version ASC;
