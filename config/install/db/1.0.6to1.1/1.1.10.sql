DROP TABLE IF EXISTS `workitem_doc_rel_seq`;
DROP TABLE IF EXISTS `bookshop_doc_rel_seq`;
DROP TABLE IF EXISTS `cadlib_doc_rel_seq`;
DROP TABLE IF EXISTS `mockup_doc_rel_seq`;

DROP TABLE IF EXISTS `workitem_doc_rel`;
DROP TABLE IF EXISTS `bookshop_doc_rel`;
DROP TABLE IF EXISTS `cadlib_doc_rel`;
DROP TABLE IF EXISTS `mockup_doc_rel`;

DROP TABLE IF EXISTS `document_rel_seq`;

CREATE TABLE `workitem_doc_rel` (
 `link_id` int(11) NOT NULL,
 `name` varchar(256) default NULL,
 `parent_id` int(11) default NULL,
 `parent_uid` varchar(64) default NULL,
 `parent_space_id` int(11) default NULL,
 `child_id` int(11) default NULL,
 `child_uid` varchar(64) default NULL,
 `child_space_id` int(11) default NULL,
 `acode` int(11) default NULL,
 `lindex` int(11) default 0,
 hash char(32) default NULL,
 data text default NULL,
 PRIMARY KEY (`link_id`),
 UNIQUE KEY `uniq_docid_ldoc_id` (`parent_id`,`child_id`),
 UNIQUE KEY `uniq_docid_lname_id` (`parent_id`,`name`),
 INDEX `workitem_dr_index_01` (`lindex`),
 INDEX `workitem_dr_index_02` (`parent_uid`),
 INDEX `workitem_dr_index_03` (`child_uid`),
 INDEX `workitem_dr_index_04` (`parent_id`, `parent_space_id`),
 INDEX `workitem_dr_index_05` (`child_id`, `child_space_id`)
);

CREATE TABLE `document_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO `document_rel_seq` (`id`) VALUES (100);

CREATE TABLE `bookshop_doc_rel` LIKE `workitem_doc_rel`;
CREATE TABLE `cadlib_doc_rel` LIKE `workitem_doc_rel`;
CREATE TABLE `mockup_doc_rel` LIKE `workitem_doc_rel`;

 ALTER TABLE `workitem_doc_rel`
 ADD CONSTRAINT `FK_workitem_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE;

 ALTER TABLE `bookshop_doc_rel`
 ADD CONSTRAINT `FK_bookshop_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE,
 ADD UNIQUE KEY `bookshop_docid_ldoc_id` (`parent_id`,`child_id`),
 ADD UNIQUE KEY `bookshop_docid_lname_id` (`parent_id`,`name`),
 ADD INDEX `bookshop_dr_index_01` (`lindex`),
 ADD INDEX `bookshop_dr_index_02` (`parent_uid`),
 ADD INDEX `bookshop_dr_index_03` (`child_uid`),
 ADD INDEX `bookshop_dr_index_04` (`parent_id`, `parent_space_id`),
 ADD INDEX `bookshop_dr_index_05` (`child_id`, `child_space_id`);

 ALTER TABLE `cadlib_doc_rel`
 ADD CONSTRAINT `FK_cadlib_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE,
 ADD UNIQUE KEY `cadlib_docid_ldoc_id` (`parent_id`,`child_id`),
 ADD UNIQUE KEY `cadlib_docid_lname_id` (`parent_id`,`name`),
 ADD INDEX `cadlib_dr_index_01` (`lindex`),
 ADD INDEX `cadlib_dr_index_02` (`parent_uid`),
 ADD INDEX `cadlib_dr_index_03` (`child_uid`),
 ADD INDEX `cadlib_dr_index_04` (`parent_id`, `parent_space_id`),
 ADD INDEX `cadlib_dr_index_05` (`child_id`, `child_space_id`);

 ALTER TABLE `mockup_doc_rel`
 ADD CONSTRAINT `FK_mockup_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE,
 ADD UNIQUE KEY `mockup_docid_ldoc_id` (`parent_id`,`child_id`),
 ADD UNIQUE KEY `mockup_docid_lname_id` (`parent_id`,`name`),
 ADD INDEX `mockup_dr_index_01` (`lindex`),
 ADD INDEX `mockup_dr_index_02` (`parent_uid`),
 ADD INDEX `mockup_dr_index_03` (`child_uid`),
 ADD INDEX `mockup_dr_index_04` (`parent_id`, `parent_space_id`),
 ADD INDEX `mockup_dr_index_05` (`child_id`, `child_space_id`);
 
 
INSERT INTO schema_version(version,lastModification) VALUES('1.1.10',now());
-- select * from schema_version ORDER BY version ASC;
