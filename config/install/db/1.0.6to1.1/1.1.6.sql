 CREATE TABLE IF NOT EXISTS `vault_distant_site` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` varchar(32) NOT NULL,
 `cid` CHAR(13) NOT NULL DEFAULT 'distantsite36',
 `name` varchar(512) DEFAULT NULL,
 `description` text,
 `method` varchar(32),
 `parameters` text NULL,
 `auth_user` varchar(128),
 `auth_password` varchar(64),
 PRIMARY KEY (`id`),
 UNIQUE KEY `UC_vault_distant_site_uid` (`uid`)
 ) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

INSERT INTO schema_version(version,lastModification) VALUES('1.1.6',now());
-- select * from schema_version ORDER BY version ASC;
