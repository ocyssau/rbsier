ALTER TABLE `categories` 
ADD COLUMN `name` VARCHAR(512) NULL DEFAULT NULL AFTER `dn`,
CHANGE COLUMN `number` `nodelabel` VARCHAR(64) NULL DEFAULT NULL,
ADD KEY `K_categories_name` (`name`),
ADD KEY `K_categories_nodelabel` (`nodelabel`),
DROP KEY `UC_categories_number`;

UPDATE `categories` SET name=CONCAT('/', nodelabel, '/');
UPDATE `categories` SET dn=CONCAT('/', id, '/');

-- CATEGORIES INSERT
DROP TRIGGER IF EXISTS onCategoriesInsert;
delimiter $$
CREATE TRIGGER onCategoriesInsert BEFORE INSERT ON categories FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentName varchar(128);
	
	IF(NEW.parent_id IS NULL) THEN
		SET NEW.dn = CONCAT('/',NEW.id,'/');
		SET NEW.name = CONCAT('/',NEW.nodelabel,'/');
	ELSE
		SELECT dn, name INTO parentDn, parentName from categories where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn, NEW.id,'/');
		SET NEW.name = CONCAT(parentName, NEW.nodelabel,'/');
	END IF;
	
END;$$
delimiter ;

-- CATEGORIES UPDATE
DROP TRIGGER IF EXISTS onCategoriesUpdate;
delimiter $$
CREATE TRIGGER onCategoriesUpdate BEFORE UPDATE ON categories FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentName varchar(128);
    
	IF(NEW.parent_id IS NULL) THEN
		SET NEW.dn = CONCAT('/',NEW.id,'/');
		SET NEW.name = CONCAT('/',NEW.nodelabel,'/');
	ELSEIF(IFNULL(NEW.parent_id=OLD.parent_id, TRUE) OR IFNULL(NEW.nodelabel=OLD.nodelabel, TRUE) OR NEW.parent_id != OLD.parent_id OR NEW.nodelabel != OLD.nodelabel) THEN
		-- get parent dn
		SELECT dn, name INTO parentDn, parentName from categories where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn, NEW.id,'/');
		SET NEW.name = CONCAT(parentName, NEW.nodelabel,'/');
	END IF;
END;$$
delimiter ;



INSERT INTO schema_version(version,lastModification) VALUES('1.1.2',now());
-- select * from schema_version ORDER BY version ASC;
