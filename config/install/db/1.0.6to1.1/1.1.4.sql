 CREATE TABLE IF NOT EXISTS `vault_replicated` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` varchar(32) NOT NULL,
 `cid` CHAR(13) NOT NULL DEFAULT 'repositsynchr',
 `name` varchar(512) DEFAULT NULL,
 `description` text,
 `reposit_id` int(11) NULL,
 `reposit_uid` varchar(32) NULL,
 `distantsite_id` int(11) NULL,
 `distantsite_uid` varchar(32) NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `UC_vault_replicated_uid` (`uid`),
 KEY `K_vault_replicated_name_1` (`reposit_id`),
 KEY `K_vault_replicated_name_2` (`reposit_uid`),
 KEY `K_vault_replicated_name_3` (`distantsite_id`),
 KEY `K_vault_replicated_name_4` (`distantsite_uid`)
 ) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


ALTER TABLE `vault_replicated` 
ADD INDEX `fk_vault_replicated_1_idx` (`distantsite_id` ASC, `distantsite_uid` ASC),
ADD INDEX `fk_vault_replicated_2_idx` (`reposit_id` ASC, `reposit_uid` ASC);


 CREATE TABLE IF NOT EXISTS `vault_distant_site` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` varchar(32) NOT NULL,
 `cid` CHAR(13) NOT NULL DEFAULT 'distantsite36',
 `name` varchar(512) DEFAULT NULL,
 `description` text,
 `method` varchar(32),
 `parameters` text NULL,
 `auth_user` varchar(128),
 `auth_password` varchar(64),
 PRIMARY KEY (`id`),
 UNIQUE KEY `UC_vault_distant_site_uid` (`uid`)
 ) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


ALTER TABLE `vault_replicated` 
ADD CONSTRAINT `fk_vault_replicated_1`
  FOREIGN KEY (`distantsite_id`)
  REFERENCES `vault_distant_site` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_vault_replicated_2`
  FOREIGN KEY (`reposit_id`)
  REFERENCES `vault_reposit` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


INSERT INTO schema_version(version,lastModification) VALUES('1.1.4',now());
-- select * from schema_version ORDER BY version ASC;
