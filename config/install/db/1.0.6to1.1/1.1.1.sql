UPDATE workitems SET parent_uid=(SELECT uid FROM projects WHERE projects.id=workitems.parent_id);
UPDATE bookshops SET parent_uid=(SELECT uid FROM projects WHERE projects.id=bookshops.parent_id);

-- coherence of referToUid with referToId
UPDATE acl_resource SET referToUid = (SELECT uid FROM view_resource_breakdown WHERE id=acl_resource.referToId AND cid=acl_resource.referToUid) WHERE referToCid='569e94192201a';


DROP VIEW IF EXISTS `view_resource_breakdown`;
CREATE VIEW `view_resource_breakdown` AS
(SELECT 
	`doc`.`id` AS `id`,
	`doc`.`uid` AS `uid`,
	`doc`.`name` AS `name`,
	`doc`.`cid` AS `cid`,
	`doc`.`container_id` AS `parent_id`,
	`doc`.`container_uid` AS `parent_uid`
FROM `workitem_documents` `doc`)
UNION 
(SELECT 
	`doc`.`id` AS `id`,
	`doc`.`uid` AS `uid`,
	`doc`.`name` AS `name`,
	`doc`.`cid` AS `cid`,
	`doc`.`container_id` AS `parent_id`,
	`doc`.`container_uid` AS `parent_uid`
FROM `bookshop_documents` `doc`)
UNION
(SELECT 
	`doc`.`id` AS `id`,
	`doc`.`uid` AS `uid`,
	`doc`.`name` AS `name`,
	`doc`.`cid` AS `cid`,
	`doc`.`container_id` AS `parent_id`,
	`doc`.`container_uid` AS `parent_uid`
FROM `cadlib_documents` `doc`)
UNION 
(SELECT 
	`cont`.`id` AS `id`,
	`cont`.`uid` AS `uid`,
	`cont`.`name` AS `name`,
	`cont`.`cid` AS `cid`,
	`cont`.`parent_id` AS `parent_id`,
	`cont`.`parent_uid` AS `parent_uid`
FROM `workitems` `cont`)
UNION 
(SELECT 
	`cont`.`id` AS `id`,
	`cont`.`uid` AS `uid`,
	`cont`.`name` AS `name`,
	`cont`.`cid` AS `cid`,
	`cont`.`parent_id` AS `parent_id`,
	`cont`.`parent_uid` AS `parent_uid`
FROM `cadlibs` `cont`)
UNION 
(SELECT 
	`cont`.`id` AS `id`,
	`cont`.`uid` AS `uid`,
	`cont`.`name` AS `name`,
	`cont`.`cid` AS `cid`,
	`cont`.`parent_id` AS `parent_id`,
	`cont`.`parent_uid` AS `parent_uid`
FROM `bookshops` `cont`)
UNION 
(SELECT 
	`cont`.`id` AS `id`,
	`cont`.`uid` AS `uid`,
	`cont`.`name` AS `name`,
	`cont`.`cid` AS `cid`,
	`cont`.`parent_id` AS `parent_id`,
	`cont`.`parent_uid` AS `parent_uid`
FROM `mockups` `cont`)
UNION 
(SELECT 
	`projects`.`id` AS `id`,
	`projects`.`uid` AS `uid`,
	`projects`.`name` AS `name`,
	`projects`.`cid` AS `cid`,
	`projects`.`parent_id` AS `parent_id`,
	`projects`.`parent_uid` AS `parent_uid`
FROM `projects`);

-- SELECT * FROM `view_resource_breakdown`;

INSERT INTO schema_version(version,lastModification) VALUES('1.1.1',now());
-- select * from schema_version ORDER BY version ASC;
