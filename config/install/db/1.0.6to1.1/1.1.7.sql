ALTER TABLE `acl_user` 
ADD COLUMN `conn_from` VARCHAR(16) DEFAULT 'auto' AFTER `auth_from`,
ADD COLUMN `extends` JSON AFTER `conn_from`;

INSERT INTO schema_version(version,lastModification) VALUES('1.1.7',now());
-- select * from schema_version ORDER BY version ASC;
