 CREATE TABLE IF NOT EXISTS `spaces` (
 `id` int(11) NOT NULL,
 `uid` varchar(64) NOT NULL DEFAULT '',
 `cid` varchar(64) NOT NULL DEFAULT '64bdd156af045',
 `name` varchar(255) NOT NULL DEFAULT '',
 `designation` varchar(256) DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY (`uid`),
 INDEX (`name`)
 ) ENGINE=InnoDB;

 INSERT INTO `spaces`
(`id`,
`uid`,
`name`,
`designation`)
VALUES
(0,
'default156f045space',
'Default',
'Default'),
(1,
'product56af045space',
'Product',
'Products'),
(10,
'wibdd156af045space',
'Workitem',
'Workitems'),
(15,
'mockupd156f045space',
'Mockup',
'Mockup'),
(20,
'bookshop156f045space',
'Bookshop',
'Bookshop'),
(25,
'cadlib156f045space',
'Cadlib',
'Cadlib');


INSERT INTO schema_version(version,lastModification) VALUES('1.1.5',now());
-- select * from schema_version ORDER BY version ASC;
