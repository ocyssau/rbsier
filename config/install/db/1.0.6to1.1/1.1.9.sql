ALTER TABLE `indexer` 
DROP INDEX `UK_indexer_1`,
ADD UNIQUE INDEX `UK_indexer_1` (`spacename` ASC, `docfile_id` ASC),
ADD INDEX `I_indexer_3` (`document_id` ASC, `spacename` ASC);

INSERT INTO schema_version(version,lastModification) VALUES('1.1.9',now());
-- select * from schema_version ORDER BY version ASC;
