ALTER TABLE `user_prefs`
	ADD COLUMN `rbgateserver_url` varchar(256) DEFAULT NULL AFTER `time_zone`;

	
ALTER TABLE `workitem_documents_history` 
	ADD INDEX `K_workitem_documents_history_documentid` (`document_id` ASC),
	ADD INDEX `K_workitem_documents_history_containerid` (`container_id` ASC);


ALTER TABLE `cadlib_documents_history` 
	ADD INDEX `K_cadlib_documents_history_documentid` (`document_id` ASC),
	ADD INDEX `K_cadlib_documents_history_containerid` (`container_id` ASC);

ALTER TABLE `bookshop_documents_history` 
	ADD INDEX `K_bookshop_documents_history_documentid` (`document_id` ASC),
	ADD INDEX `K_bookshop_documents_history_containerid` (`container_id` ASC);
	
ALTER TABLE `mockup_documents_history` 
	ADD INDEX `K_mockup_documents_history_documentid` (`document_id` ASC),
	ADD INDEX `K_mockup_documents_history_containerid` (`container_id` ASC);
	
INSERT INTO schema_version(version,lastModification) VALUES('1.0.4.1',now());
