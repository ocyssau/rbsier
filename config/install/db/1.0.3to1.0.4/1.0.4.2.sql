ALTER TABLE `workitem_documents`
	ADD COLUMN `tags` varchar(256) DEFAULT NULL AFTER `category_id`,
	ADD INDEX `FK_workitem_documents_11` (`tags`),
	DROP INDEX `number`,
	ADD FULLTEXT `FT_workitem_documents` (name, number, designation, tags);

	
ALTER TABLE `cadlib_documents`
	ADD COLUMN `tags` varchar(256) DEFAULT NULL AFTER `category_id`,
	ADD INDEX `FK_cadlib_documents_11` (`tags`),
	DROP INDEX `number`,
	ADD FULLTEXT `FT_cadlib_documents` (name, number, designation, tags);
	
ALTER TABLE `bookshop_documents`
	ADD COLUMN `tags` varchar(256) DEFAULT NULL AFTER `category_id`,
	ADD INDEX `FK_bookshop_documents_11` (`tags`),
	DROP INDEX `number`,
	ADD FULLTEXT `FT_bookshop_documents` (name, number, designation, tags);
	
ALTER TABLE `mockup_documents`
	ADD COLUMN `tags` varchar(256) DEFAULT NULL AFTER `category_id`,
	ADD INDEX `FK_mockup_documents_11` (`tags`),
	DROP INDEX `number`,
	ADD FULLTEXT `FT_mockup_documents` (name, number, designation, tags);
	
	
INSERT INTO schema_version(version,lastModification) VALUES('1.0.4.2',now());
