-- DROP INSTANCE ACTIVITY WITHOUT PROCESS INSTANCE
DROP TEMPORARY TABLE IF EXISTS tmptable1;
CREATE TEMPORARY TABLE tmptable1 AS (
SELECT wf_instance_activity.id as Aid, wf_instance.id AS Iid FROM wf_instance_activity
LEFT OUTER JOIN wf_instance ON wf_instance_activity.instanceId=wf_instance.id
WHERE wf_instance.id IS NULL
);
-- select Aid from tmptable1;
DELETE FROM wf_instance_activity WHERE id IN (SELECT Aid FROM tmptable1);
DROP TEMPORARY TABLE tmptable1;

ALTER TABLE `wf_instance_activity` 
ADD CONSTRAINT `fk_wf_instance_activity_2`
  FOREIGN KEY (`instanceId`) REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.6',now());
-- select * from schema_version ORDER BY version ASC;
