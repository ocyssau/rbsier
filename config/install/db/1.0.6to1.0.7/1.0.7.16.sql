ALTER TABLE `workitem_doc_files` 
	CHANGE COLUMN `name` `name` VARCHAR(255) NULL DEFAULT NULL ,
	CHANGE COLUMN `path` `path` VARCHAR(512) NULL DEFAULT NULL ,
	CHANGE COLUMN `root_name` `root_name` VARCHAR(255) NULL DEFAULT NULL ;

ALTER TABLE `workitem_documents` 
	CHANGE COLUMN `number` `number` VARCHAR(255) NULL DEFAULT NULL ;


ALTER TABLE `cadlib_doc_files` 
	CHANGE COLUMN `name` `name` VARCHAR(255) NULL DEFAULT NULL ,
	CHANGE COLUMN `path` `path` VARCHAR(512) NULL DEFAULT NULL ,
	CHANGE COLUMN `root_name` `root_name` VARCHAR(255) NULL DEFAULT NULL ;

ALTER TABLE `cadlib_documents` 
	CHANGE COLUMN `number` `number` VARCHAR(255) NULL DEFAULT NULL ;


ALTER TABLE `bookshop_doc_files` 
	CHANGE COLUMN `name` `name` VARCHAR(255) NULL DEFAULT NULL ,
	CHANGE COLUMN `path` `path` VARCHAR(512) NULL DEFAULT NULL ,
	CHANGE COLUMN `root_name` `root_name` VARCHAR(255) NULL DEFAULT NULL ;

ALTER TABLE `bookshop_documents` 
	CHANGE COLUMN `number` `number` VARCHAR(255) NULL DEFAULT NULL ;


ALTER TABLE `mockup_doc_files` 
	CHANGE COLUMN `name` `name` VARCHAR(255) NULL DEFAULT NULL ,
	CHANGE COLUMN `path` `path` VARCHAR(512) NULL DEFAULT NULL ,
	CHANGE COLUMN `root_name` `root_name` VARCHAR(255) NULL DEFAULT NULL ;

ALTER TABLE `mockup_documents` 
	CHANGE COLUMN `number` `number` VARCHAR(255) NULL DEFAULT NULL ;




INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.16',now());
-- select * from schema_version ORDER BY version ASC;
