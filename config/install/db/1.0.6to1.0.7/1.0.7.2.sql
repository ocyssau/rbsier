-- ************************
ALTER TABLE `doctypes`
	ADD COLUMN  `number_generator_class` varchar(128) NULL AFTER `post_update_method`;


INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.2',now());
-- select * from schema_version ORDER BY version ASC;
