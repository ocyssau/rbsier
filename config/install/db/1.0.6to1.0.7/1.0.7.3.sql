 CREATE TABLE `cron_task` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(258) NULL,
 `description` VARCHAR(258) NULL,
 `callback_class` TEXT NULL,
 `callback_method` VARCHAR(64) NULL,
 `owner_id` INT(11) NULL,
 `owner_uid` VARCHAR(128) NULL,
 `status` VARCHAR(32) NULL,
 `created` DATETIME NULL,
 `exec_frequency` DECIMAL NULL,
 `exec_schedule_start` DECIMAL NULL,
 `exec_schedule_end` DECIMAL NULL,
 `last_exec` DATETIME NULL,
 `is_actif` tinyint DEFAULT 1,
 PRIMARY KEY (`id`),
 INDEX `K_cron_task_1` (`name`),
 INDEX `K_cron_task_2` (`owner_id`),
 INDEX `K_cron_task_3` (`owner_uid`),
 INDEX `K_cron_task_4` (`status`),
 INDEX `K_cron_task_5` (`created`),
 INDEX `K_cron_task_6` (`exec_schedule_start`),
 INDEX `K_cron_task_7` (`exec_schedule_end`),
 INDEX `K_cron_task_8` (`exec_frequency`),
 INDEX `K_cron_task_9` (`is_actif`)
 ) ENGINE=MyIsam;

DROP TABLE `batchjob`;
CREATE TABLE `batchjob` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(258) NULL,
	`callback_class` TEXT NULL,
	`callback_method` VARCHAR(64) NULL,
	`owner_id` INT(11) NULL,
	`owner_uid` VARCHAR(128) NULL,
	`status` VARCHAR(32) NULL,
	`planned` DATETIME NULL,
	`created` DATETIME NULL,
	`started` FLOAT NULL,
	`ended` FLOAT NULL,
	`duration` FLOAT NULL,
	`hashkey` VARCHAR(40) NOT NULL,
	`data` TEXT NULL,
	`log` BLOB NULL,
	PRIMARY KEY (`id`),
	INDEX `K_batchjob_1` (`planned`),
	INDEX `K_batchjob_2` (`status`),
	INDEX `K_batchjob_3` (`name`),
	UNIQUE `U_batchjob_4` (`hashkey`)
) ENGINE=MyIsam;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.3',now());
-- select * from schema_version ORDER BY version ASC;
