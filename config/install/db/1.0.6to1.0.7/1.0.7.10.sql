ALTER TABLE `workitems` 
	CHANGE COLUMN designation designation TEXT DEFAULT NULL;
ALTER TABLE `cadlibs` 
	CHANGE COLUMN designation designation TEXT DEFAULT NULL;
ALTER TABLE `bookshops` 
	CHANGE COLUMN designation designation TEXT DEFAULT NULL;
ALTER TABLE `mockups` 
	CHANGE COLUMN designation designation TEXT DEFAULT NULL;
ALTER TABLE `projects` 
	CHANGE COLUMN designation designation TEXT DEFAULT NULL;



INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.10',now());
-- select * from schema_version ORDER BY version ASC;
