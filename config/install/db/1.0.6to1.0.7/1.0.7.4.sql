-- SELECT * FROM doctypes where docseeder=1;

UPDATE doctypes SET
	number_generator_class='\\Sier\\Docseeder\\NumberRule1'
    WHERE docseeder=1;

DROP TABLE indexer_batch_log;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.4',now());

-- select * from schema_version ORDER BY version ASC;
