ALTER TABLE batchjob 
	DROP COLUMN data;

ALTER TABLE cron_task 
	ADD COLUMN `callback_params` TEXT NULL AFTER `callback_method`;
	

ALTER TABLE `workitem_documents_history` 
ADD INDEX `K_workitem_documents_history_number` (`number` ASC);

ALTER TABLE `cadlib_documents_history` 
ADD INDEX `K_cadlib_documents_history_number` (`number` ASC);

ALTER TABLE `bookshop_documents_history` 
ADD INDEX `K_bookshop_documents_history_number` (`number` ASC);

ALTER TABLE `mockup_documents_history` 
ADD INDEX `K_mockup_documents_history_number` (`number` ASC);


INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.9',now());
-- select * from schema_version ORDER BY version ASC;
