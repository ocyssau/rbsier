CREATE TABLE IF NOT EXISTS `categories` (
	`id` int(11) NOT NULL,
	`uid` varchar(32) NOT NULL,
	`cid` CHAR(13) NOT NULL DEFAULT '569e918a134ca',
	`dn` VARCHAR(128) NULL,
	`number` varchar(32) DEFAULT NULL,
	`designation` text,
	`parent_id` int(11) NULL,
	`parent_uid` varchar(32) NULL,
	`icon` varchar(32) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `UC_categories_uid` (`uid`),
	UNIQUE KEY `UC_categories_number` (`number`),
	UNIQUE KEY `UC_categories_dn` (`dn`),
	KEY `K_categories_parent_id` (`parent_id`),
	KEY `K_categories_parent_uid` (`parent_uid`)
) ENGINE=InnoDB;

INSERT INTO `categories` (`id`, `number`, `designation`, `icon`, `uid`) SELECT `id`, `number`, `designation`, `icon`, `uid` FROM `workitem_categories`;
INSERT INTO `categories` (`id`, `number`, `designation`, `icon`, `uid`) SELECT `id`, `number`, `designation`, `icon`, `uid` FROM `bookshop_categories`;
INSERT INTO `categories` (`id`, `number`, `designation`, `icon`, `uid`) SELECT `id`, `number`, `designation`, `icon`, `uid` FROM `cadlib_categories`;

SET @position := (SELECT MAX(`id`) from `categories`);
UPDATE `categories_seq` SET id=@position LIMIT 1;

-- SELECT * FROM `categories`;
-- SELECT * FROM `categories_seq`;

ALTER TABLE `project_category_rel` 
	DROP FOREIGN KEY `FK_workitem_category_rel_3`;
ALTER TABLE `project_category_rel` 
	ADD CONSTRAINT `FK_project_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

ALTER TABLE `workitem_category_rel` 
	DROP FOREIGN KEY `FK_workitem_category_rel_3`;
ALTER TABLE `workitem_category_rel` 
	ADD CONSTRAINT `FK_workitem_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

ALTER TABLE `cadlib_category_rel` 
	DROP FOREIGN KEY `FK_cadlib_category_rel_3`;
ALTER TABLE `cadlib_category_rel` 
	ADD CONSTRAINT `FK_cadlib_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

ALTER TABLE `bookshop_category_rel` 
	DROP FOREIGN KEY `FK_bookshop_category_rel_3`;
ALTER TABLE `bookshop_category_rel` 
	ADD CONSTRAINT `FK_bookshop_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
	
ALTER TABLE `mockup_category_rel` 
	DROP FOREIGN KEY `FK_mockup_category_rel_3`;
ALTER TABLE `mockup_category_rel` 
	ADD CONSTRAINT `FK_mockup_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

ALTER TABLE `cadlib_category_rel` 
DROP INDEX `K_project_category_rel_7`,
DROP INDEX `K_project_category_rel_6`,
DROP INDEX `K_project_category_rel_5`,
DROP INDEX `K_project_category_rel_4`,
DROP INDEX `K_project_category_rel_3`,
DROP INDEX `K_project_category_rel_2`,
DROP INDEX `K_project_category_rel_1`;

ALTER TABLE `bookshop_category_rel` 
DROP INDEX `K_project_category_rel_7`,
DROP INDEX `K_project_category_rel_6`,
DROP INDEX `K_project_category_rel_5`,
DROP INDEX `K_project_category_rel_4`,
DROP INDEX `K_project_category_rel_3`,
DROP INDEX `K_project_category_rel_2`,
DROP INDEX `K_project_category_rel_1`;

ALTER TABLE `mockup_category_rel` 
DROP INDEX `K_project_category_rel_7`,
DROP INDEX `K_project_category_rel_6`,
DROP INDEX `K_project_category_rel_5`,
DROP INDEX `K_project_category_rel_4`,
DROP INDEX `K_project_category_rel_3`,
DROP INDEX `K_project_category_rel_2`,
DROP INDEX `K_project_category_rel_1`;


 DROP TRIGGER IF EXISTS onOrgCategoryRelInsert;
 delimiter $$
 CREATE TRIGGER onProjectCategoryRelInsert BEFORE INSERT ON project_category_rel FOR EACH ROW
 BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM categories WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
 END;$$
 delimiter ;

ALTER TABLE `cadlib_documents` 
	ADD CONSTRAINT `FK_cadlib_documents_3`
	  FOREIGN KEY (`category_id`)
	  REFERENCES `categories` (`id`)
	  ON DELETE SET NULL
	  ON UPDATE SET NULL;

ALTER TABLE `bookshop_documents` 
	ADD CONSTRAINT `FK_bookshop_documents_3`
	  FOREIGN KEY (`category_id`)
	  REFERENCES `categories` (`id`)
	  ON DELETE SET NULL
	  ON UPDATE SET NULL;

ALTER TABLE `workitem_documents` 
	ADD CONSTRAINT `FK_workitem_documents_3`
	  FOREIGN KEY (`category_id`)
	  REFERENCES `categories` (`id`)
	  ON DELETE SET NULL
	  ON UPDATE SET NULL;

ALTER TABLE `mockup_documents` 
	ADD CONSTRAINT `FK_mockup_documents_3`
	  FOREIGN KEY (`category_id`)
	  REFERENCES `categories` (`id`)
	  ON DELETE SET NULL
	  ON UPDATE SET NULL;

DROP TABLE `workitem_categories`;
DROP TABLE `bookshop_categories`;
DROP TABLE `cadlib_categories`;
DROP TABLE `mockup_categories`;

 INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.12',now());
-- select * from schema_version ORDER BY version ASC;
