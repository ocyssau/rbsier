DROP TABLE `pdm_instance_seq`;
CREATE TABLE `pdm_instance_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
  )ENGINE=MyISAM AUTO_INCREMENT=100;
  INSERT INTO pdm_instance_seq(id) VALUES(100);

DROP TABLE pdm_product_instance;
CREATE TABLE `pdm_product_instance` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL DEFAULT '569e971bb57c0',
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `nomenclature` varchar(128) NULL,
  `description` varchar(512) NULL,
  `position` varchar(512) NULL,
  `quantity` int(11) NULL,
  `parent_id` int(11) NULL,
  `path` varchar(256) NULL,
  `of_product_id` int(11) NULL,
  `of_product_uid` varchar(32) NULL,
  `of_product_number` varchar(128) NULL,
  `context_id` varchar(32) NULL,
  `type` varchar(64) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_pdm_product_instance_1` (`uid`),
  UNIQUE KEY `U_pdm_product_instance_2` (`number`, `parent_id`),
  KEY `INDEX_pdm_product_instance_1` (`cid`),
  KEY `INDEX_pdm_product_instance_2` (`number`),
  KEY `INDEX_pdm_product_instance_3` (`description`),
  KEY `INDEX_pdm_product_instance_4` (`nomenclature`),
  KEY `INDEX_pdm_product_instance_5` (`uid`),
  KEY `INDEX_pdm_product_instance_6` (`name`),
  KEY `INDEX_pdm_product_instance_7` (`type`),
  KEY `INDEX_pdm_product_instance_8` (`of_product_uid`),
  KEY `INDEX_pdm_product_instance_9` (`of_product_id`),
  KEY `INDEX_pdm_product_instance_10` (`path`),
  KEY `INDEX_pdm_product_version_12` (`of_product_number` ASC)
);

DROP TABLE `pdm_product_version`;
CREATE TABLE `pdm_product_version` (
	`id` int(11) NOT NULL,
	`uid` varchar(32) NOT NULL,
	`name` varchar(128) NOT NULL,
	`number` varchar(128) NOT NULL,
	`description` varchar(512) NULL,
	`version` int(11) NOT NULL DEFAULT 1,
	`of_product_id` int(11) NULL,
	`of_product_uid` varchar(32) NULL,
	`document_id` int(11) NULL,
	`document_uid` varchar(32) NULL,
	`lock_by_id` int(11) DEFAULT NULL,
	`lock_by_uid` VARCHAR(64) DEFAULT NULL,
	`locked` DATETIME DEFAULT NULL,
	`updated` DATETIME DEFAULT NULL,
	`update_by_id` int(11) DEFAULT NULL,
	`update_by_uid` VARCHAR(64) DEFAULT NULL,
	`created` DATETIME DEFAULT NULL,
	`create_by_id` int(11) DEFAULT NULL,
	`create_by_uid` VARCHAR(64) DEFAULT NULL,
	`spacename` varchar(32) NULL,
	`type` varchar(64) NULL,
	`materials` varchar(512) NULL,
	`weight` float NULL,
	`volume` float NULL,
	`wetsurface` float NULL,
	`density` float NULL,
	`gravitycenter` varchar(512) NULL,
	`inertiacenter` varchar(512) NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `U_pdm_product_version_1` (`uid`),
	UNIQUE KEY `U_pdm_product_version_2` (`number`),
	UNIQUE KEY `U_pdm_product_version_3` (`of_product_uid`,`version`),
	KEY `INDEX_pdm_product_version_1` (`of_product_uid`),
	KEY `INDEX_pdm_product_version_6` (`of_product_id`),
	KEY `INDEX_pdm_product_version_2` (`version`),
	KEY `INDEX_pdm_product_version_3` (`description`),
	KEY `INDEX_pdm_product_version_4` (`uid`),
	KEY `INDEX_pdm_product_version_5` (`name`),
	KEY `INDEX_pdm_product_version_7` (`document_id`),
	KEY `INDEX_pdm_product_version_8` (`document_uid`),
	KEY `INDEX_pdm_product_version_9` (`spacename`),
	KEY `INDEX_pdm_product_version_10` (`type`),
	KEY `INDEX_pdm_product_version_11` (`create_by_uid`),
	KEY `INDEX_pdm_product_version_12` (`lock_by_uid`),
	KEY `INDEX_pdm_product_version_13` (`update_by_uid`),
	KEY `INDEX_pdm_product_version_14` (`created`),
	KEY `INDEX_pdm_product_version_15` (`locked`),
	KEY `INDEX_pdm_product_version_16` (`updated`)
);

ALTER TABLE `pdm_product_version` 
ADD FULLTEXT INDEX `FT_name` (`name` ASC, `number` ASC, `description` ASC);

ALTER TABLE `pdm_product_instance` 
ADD CONSTRAINT `FK_pdm_product_instance_1`
  FOREIGN KEY (`of_product_id`)
  REFERENCES `pdm_product_version` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_pdm_product_instance_2`
  FOREIGN KEY (`parent_id`)
  REFERENCES `pdm_product_version` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

DROP TABLE `pdm_seq`;
CREATE TABLE `pdm_seq`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO pdm_seq(id) VALUES(100);

INSERT INTO `pdm_product_version`
(`id`,
`uid`,
`name`,
`number`,
`description`,
`type`,
`locked`,
`lock_by_id`,
`lock_by_uid`,
`updated`,
`update_by_id`,
`update_by_uid`,
`created`,
`create_by_id`,
`create_by_uid`
)
VALUES
('1', 'ranchbe', 'ranchbe', 'ranchbe', 'Root Product on top of Ranchbe', 'root', now(), 1, 'admin', now(), 1, 'admin', now(), 1, 'admin' );

INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.20',now());
-- select * from schema_version ORDER BY version ASC;
