ALTER TABLE workitem_documents 
	ADD COLUMN branch_id INT(1) DEFAULT NULL AFTER acode,
	ADD  KEY `FK_workitem_documents_12` (`branch_id`);
UPDATE workitem_documents SET branch_id=0 WHERE acode >= 15;
UPDATE workitem_documents SET branch_id=1 WHERE acode < 15;

ALTER TABLE cadlib_documents 
	ADD COLUMN branch_id INT(1) DEFAULT 1 NULL AFTER acode,
	ADD  KEY `FK_cadlib_documents_12` (`branch_id`);
UPDATE cadlib_documents SET branch_id=0 WHERE acode >= 15;
UPDATE cadlib_documents SET branch_id=1 WHERE acode < 15;

ALTER TABLE bookshop_documents 
	ADD COLUMN branch_id INT(1) DEFAULT 1 NULL AFTER acode,
	ADD  KEY `FK_bookshop_documents_12` (`branch_id`);
UPDATE bookshop_documents SET branch_id=0 WHERE acode >= 15;
UPDATE bookshop_documents SET branch_id=1 WHERE acode < 15;

ALTER TABLE mockup_documents 
	ADD COLUMN branch_id INT(1) DEFAULT 1 NULL AFTER acode,
	ADD  KEY `FK_mockup_documents_12` (`branch_id`);
UPDATE mockup_documents SET branch_id=0 WHERE acode >= 15;
UPDATE mockup_documents SET branch_id=1 WHERE acode < 15;

/*
SELECT * FROM(
SELECT count(number) as c FROM workitem_documents WHERE head=0 GROUP BY number
) AS t WHERE t.c > 1;
*/

INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.7',now());
-- select * from schema_version ORDER BY version ASC;
