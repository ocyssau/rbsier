-- SELECT * FROM doctypes where docseeder=1;

ALTER TABLE `batchjob`
	ADD COLUMN `pid` VARCHAR(32) NULL AFTER `hashkey`,
	ADD COLUMN `sapi` VARCHAR(32) NULL AFTER `pid`;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.5',now());
-- select * from schema_version ORDER BY version ASC;
