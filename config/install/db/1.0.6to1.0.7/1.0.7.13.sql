 CREATE TABLE `batch` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(258) NULL,
 `owner_id` INT(11) NULL,
 `owner_uid` VARCHAR(128) NULL,
 `callback_class` TEXT NULL,
 `callback_method` VARCHAR(64) NULL,
 `callback_params` TEXT NULL,
 `comment` TEXT NULL,
 PRIMARY KEY (`id`),
 UNIQUE `U_batch_1` (`uid`),
 INDEX `K_batch_2` (`owner_id`),
 INDEX `K_batch_3` (`owner_uid`)
 ) ENGINE=MyIsam;
 
 
 ALTER TABLE `workitem_documents_history`
 ADD COLUMN `action_batch_uid` VARCHAR(128) AFTER `action_comment`,
 ADD INDEX `K_workitem_documents_history_batchuid` (`action_batch_uid` ASC);
 
ALTER TABLE `cadlib_documents_history`
 ADD COLUMN `action_batch_uid` VARCHAR(128) AFTER `action_comment`,
 ADD INDEX `K_cadlib_documents_history_batchuid` (`action_batch_uid` ASC);

ALTER TABLE `mockup_documents_history`
 ADD COLUMN `action_batch_uid` VARCHAR(128) AFTER `action_comment`,
 ADD INDEX `K_mockup_documents_history_batchuid` (`action_batch_uid` ASC);

ALTER TABLE `bookshop_documents_history`
 ADD COLUMN `action_batch_uid` VARCHAR(128) AFTER `action_comment`,
 ADD INDEX `K_bookshop_documents_history_batchuid` (`action_batch_uid` ASC);


 INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.13',now());
-- select * from schema_version ORDER BY version ASC;

 
 
-- ALTER TABLE `workitem_documents_history`
-- DROP COLUMN `action_batch_id`,
-- DROP INDEX `K_workitem_documents_history_batchid`;
 
-- ALTER TABLE `cadlib_documents_history`
-- DROP COLUMN `action_batch_id`,
-- DROP INDEX `K_cadlib_documents_history_batchid`;

-- ALTER TABLE `mockup_documents_history`
-- DROP COLUMN `action_batch_id`,
-- DROP INDEX `K_mockup_documents_history_batchid`;

-- ALTER TABLE `bookshop_documents_history`
-- DROP COLUMN `action_batch_id`,
-- DROP INDEX `K_bookshop_documents_history_batchid`;

 