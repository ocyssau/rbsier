DROP VIEW IF EXISTS `view_workitem_doctype_rel`;
CREATE VIEW `view_workitem_doctype_rel` AS
	SELECT 
		`parent_id`,
		`parent_uid`,
		`child_id`,
		`child_uid`,
		`rdn`
	FROM
		`workitem_doctype_rel` 
	UNION SELECT 
		`parent_id`,
		`parent_uid`,
		`child_id`,
		`child_uid`,
		`rdn`
	FROM
		`project_doctype_rel`;


DROP VIEW IF EXISTS `view_cadlib_doctype_rel`;
CREATE VIEW `view_cadlib_doctype_rel` AS
	SELECT 
		`parent_id`,
		`parent_uid`,
		`child_id`,
		`child_uid`,
		`rdn`
	FROM
		`cadlib_doctype_rel` 
	UNION SELECT 
		`parent_id`,
		`parent_uid`,
		`child_id`,
		`child_uid`,
		`rdn`
	FROM
		`project_doctype_rel`;

DROP VIEW IF EXISTS `view_bookshop_doctype_rel`;
CREATE VIEW `view_bookshop_doctype_rel` AS
	SELECT 
		`parent_id`,
		`parent_uid`,
		`child_id`,
		`child_uid`,
		`rdn`
	FROM
		`bookshop_doctype_rel` 
	UNION SELECT 
		`parent_id`,
		`parent_uid`,
		`child_id`,
		`child_uid`,
		`rdn`
	FROM
		`project_doctype_rel`;

DROP VIEW IF EXISTS `view_mockup_doctype_rel`;
CREATE VIEW `view_mockup_doctype_rel` AS
	SELECT 
		`parent_id`,
		`parent_uid`,
		`child_id`,
		`child_uid`,
		`rdn`
	FROM
		`mockup_doctype_rel` 
	UNION SELECT 
		`parent_id`,
		`parent_uid`,
		`child_id`,
		`child_uid`,
		`rdn`
	FROM
		`project_doctype_rel`;
		
		
INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.17',now());
-- select * from schema_version ORDER BY version ASC;
		
		