-- DROP TABLE `workitem_process_rel`;
-- DROP TABLE `cadlib_process_rel`;
-- DROP TABLE `mockup_process_rel`;
-- DROP TABLE `bookshop_process_rel`;
 
 
CREATE TABLE `workitem_process_rel` LIKE `project_process_rel`;
 ALTER TABLE `workitem_process_rel` 
 ADD KEY `K_workitem_process_rel_1` (`parent_id`),
 ADD KEY `K_workitem_process_rel_2` (`parent_uid`),
 ADD KEY `K_workitem_process_rel_3` (`parent_cid`),
 ADD KEY `K_workitem_process_rel_4` (`child_id`),
 ADD KEY `K_workitem_process_rel_5` (`child_uid`),
 ADD KEY `K_workitem_process_rel_6` (`child_cid`),
 ADD KEY `K_workitem_process_rel_7` (`rdn`),
 ADD UNIQUE KEY `U_workitem_process_rel_1` (`parent_uid`,`child_uid`),
 ADD CONSTRAINT `FK_workitem_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_workitem_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_workitem_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE;

 DROP TRIGGER IF EXISTS onWorkitemProcessRelInsert;
 delimiter $$
 CREATE TRIGGER onWorkitemProcessRelInsert BEFORE INSERT ON workitem_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn varchar(128);
 SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END;$$
 delimiter ;


CREATE TABLE `mockup_process_rel` LIKE `project_process_rel`;
 ALTER TABLE `mockup_process_rel` 
 ADD KEY `K_mockup_process_rel_1` (`parent_id`),
 ADD KEY `K_mockup_process_rel_2` (`parent_uid`),
 ADD KEY `K_mockup_process_rel_3` (`parent_cid`),
 ADD KEY `K_mockup_process_rel_4` (`child_id`),
 ADD KEY `K_mockup_process_rel_5` (`child_uid`),
 ADD KEY `K_mockup_process_rel_6` (`child_cid`),
 ADD KEY `K_mockup_process_rel_7` (`rdn`),
 ADD UNIQUE KEY `U_mockup_process_rel_1` (`parent_uid`,`child_uid`),
 ADD CONSTRAINT `FK_mockup_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_mockup_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_mockup_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE;

 DROP TRIGGER IF EXISTS onMockupProcessRelInsert;
 delimiter $$
 CREATE TRIGGER onMockupProcessRelInsert BEFORE INSERT ON mockup_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn varchar(128);
 SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END;$$
 delimiter ;

 
CREATE TABLE `cadlib_process_rel` LIKE `project_process_rel`;
 ALTER TABLE `cadlib_process_rel` 
 ADD KEY `K_cadlib_process_rel_1` (`parent_id`),
 ADD KEY `K_cadlib_process_rel_2` (`parent_uid`),
 ADD KEY `K_cadlib_process_rel_3` (`parent_cid`),
 ADD KEY `K_cadlib_process_rel_4` (`child_id`),
 ADD KEY `K_cadlib_process_rel_5` (`child_uid`),
 ADD KEY `K_cadlib_process_rel_6` (`child_cid`),
 ADD KEY `K_cadlib_process_rel_7` (`rdn`),
 ADD UNIQUE KEY `U_cadlib_process_rel_1` (`parent_uid`,`child_uid`),
 ADD CONSTRAINT `FK_cadlib_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_cadlib_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_cadlib_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE;

 DROP TRIGGER IF EXISTS onCadlibProcessRelInsert;
 delimiter $$
 CREATE TRIGGER onCadlibProcessRelInsert BEFORE INSERT ON cadlib_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn varchar(128);
 SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END;$$
 delimiter ;

  CREATE TABLE `bookshop_process_rel` LIKE `project_process_rel`;
 ALTER TABLE `bookshop_process_rel` 
 ADD KEY `K_bookshop_process_rel_1` (`parent_id`),
 ADD KEY `K_bookshop_process_rel_2` (`parent_uid`),
 ADD KEY `K_bookshop_process_rel_3` (`parent_cid`),
 ADD KEY `K_bookshop_process_rel_4` (`child_id`),
 ADD KEY `K_bookshop_process_rel_5` (`child_uid`),
 ADD KEY `K_bookshop_process_rel_6` (`child_cid`),
 ADD KEY `K_bookshop_process_rel_7` (`rdn`),
 ADD UNIQUE KEY `U_bookshop_process_rel_1` (`parent_uid`,`child_uid`),
 ADD CONSTRAINT `FK_bookshop_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_bookshop_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_bookshop_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE;

 DROP TRIGGER IF EXISTS onBookshopProcessRelInsert;
 delimiter $$
 CREATE TRIGGER onBookshopProcessRelInsert BEFORE INSERT ON bookshop_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn varchar(128);
 SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END;$$
 delimiter ;

 
DROP PROCEDURE IF EXISTS `updateWorkitemDn`;
DELIMITER $$
CREATE PROCEDURE updateWorkitemDn()
BEGIN
	UPDATE workitems SET dn = CONCAT((SELECT dn FROM projects WHERE id=workitems.parent_id), uid,'/') WHERE workitems.dn IS NULL;
	UPDATE cadlibs SET dn = CONCAT((SELECT dn FROM projects WHERE id=cadlibs.parent_id), uid,'/') WHERE cadlibs.dn IS NULL;
	UPDATE bookshops SET dn = CONCAT((SELECT dn FROM projects WHERE id=bookshops.parent_id), uid,'/') WHERE bookshops.dn IS NULL;
	UPDATE mockups SET dn = CONCAT((SELECT dn FROM projects WHERE id=mockups.parent_id), uid,'/') WHERE mockups.dn IS NULL;
END$$
DELIMITER ;
 
INSERT INTO workitem_process_rel
SELECT 
parent.id as parent_id, 
parent.uid as parent_uid, 
parent.cid as parent_cid,
child.id as child_id,
child.uid  as child_uid,
child.cid  as child_cid,
null as rdn
FROM workitems as parent
JOIN wf_process as child ON child.id=parent.default_process_id
WHERE default_process_id is not null;
 
INSERT INTO cadlib_process_rel
SELECT 
parent.id as parent_id, 
parent.uid as parent_uid, 
parent.cid as parent_cid,
child.id as child_id,
child.uid  as child_uid,
child.cid  as child_cid,
null as rdn
FROM cadlibs as parent
JOIN wf_process as child ON child.id=parent.default_process_id
WHERE default_process_id is not null;
 
INSERT INTO bookshop_process_rel
SELECT 
parent.id as parent_id, 
parent.uid as parent_uid, 
parent.cid as parent_cid,
child.id as child_id,
child.uid  as child_uid,
child.cid  as child_cid,
null as rdn
FROM bookshops as parent
JOIN wf_process as child ON child.id=parent.default_process_id
WHERE default_process_id is not null;

INSERT INTO mockup_process_rel
SELECT 
parent.id as parent_id, 
parent.uid as parent_uid, 
parent.cid as parent_cid,
child.id as child_id,
child.uid  as child_uid,
child.cid  as child_cid,
null as rdn
FROM mockups as parent
JOIN wf_process as child ON child.id=parent.default_process_id
WHERE default_process_id is not null;

UPDATE workitems SET default_process_id=NULL;
UPDATE cadlibs SET default_process_id=NULL;
UPDATE bookshops SET default_process_id=NULL;
UPDATE mockups SET default_process_id=NULL;

ALTER TABLE `workitem_documents_history` 
CHANGE COLUMN `designation` `designation` VARCHAR(512) NULL DEFAULT NULL ;

ALTER TABLE `bookshop_documents_history` 
CHANGE COLUMN `designation` `designation` VARCHAR(512) NULL DEFAULT NULL ;

ALTER TABLE `cadlib_documents_history` 
CHANGE COLUMN `designation` `designation` VARCHAR(512) NULL DEFAULT NULL ;

ALTER TABLE `mockup_documents_history` 
CHANGE COLUMN `designation` `designation` VARCHAR(512) NULL DEFAULT NULL ;


 INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.15',now());
-- select * from schema_version ORDER BY version ASC;
