 DROP TABLE IF EXISTS `share_public_url`;
 CREATE TABLE `share_public_url` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `document_id` INT(11) NULL,
 `spacename` VARCHAR(32) NULL,
 `document_uid` VARCHAR(255) NULL,
 `expiration_date` DateTime NULL,
 `comment` TEXT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `UC_share_public_url_1` (`uid`),
 KEY `K_share_public_url_2` (`document_id`),
 KEY `K_share_public_url_3` (`spacename`),
 KEY `K_share_public_url_4` (`document_uid`),
 KEY `K_share_public_url_5` (`expiration_date`)
 ) ENGINE=MyISAM AUTO_INCREMENT=100;

 CREATE TABLE IF NOT EXISTS `typemimes` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` varchar(255) NOT NULL DEFAULT '',
 `name` varchar(255) NOT NULL DEFAULT '',
 `extensions` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE (`uid`),
 INDEX (`name`),
 INDEX (`extensions`)
 ) ENGINE=MyIsam;

 INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.14',now());
-- select * from schema_version ORDER BY version ASC;
