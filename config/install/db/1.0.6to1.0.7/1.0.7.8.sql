ALTER TABLE batchjob 
	ADD COLUMN callback_params TEXT DEFAULT NULL AFTER callback_method,
	CHANGE COLUMN started started DOUBLE NULL,
	CHANGE COLUMN ended ended DOUBLE NULL,
	CHANGE COLUMN duration duration FLOAT NULL;
	
INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.8',now());
-- select * from schema_version ORDER BY version ASC;
