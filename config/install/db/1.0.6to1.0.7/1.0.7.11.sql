CREATE TABLE IF NOT EXISTS `tools_missing_files` (
 `id` int(11) NOT NULL,
 `uid` VARCHAR(140) NOT NULL UNIQUE,
 `document_id` int(11) NOT NULL,
 `path` text NOT NULL,
 `name` varchar(128) NOT NULL,
 `spacename` VARCHAR(64) NULL,
 UNIQUE KEY `U_tools_missing_files_1` (`name`,`path`(128)),
 KEY `IK_tools_missing_files_2` (`name`),
 KEY `IK_tools_missing_files_3` (`path`(128)),
 KEY `IK_tools_missing_files_4` (`spacename`)
 ) ENGINE=MyIsam;
	

ALTER TABLE `workitem_documents` 
CHANGE COLUMN `designation` `designation` VARCHAR(512) NULL DEFAULT NULL ;
ALTER TABLE `bookshop_documents` 
CHANGE COLUMN `designation` `designation` VARCHAR(512) NULL DEFAULT NULL ;
ALTER TABLE `cadlib_documents` 
CHANGE COLUMN `designation` `designation` VARCHAR(512) NULL DEFAULT NULL ;
ALTER TABLE `mockup_documents` 
CHANGE COLUMN `designation` `designation` VARCHAR(512) NULL DEFAULT NULL ;

UPDATE acl_resource SET cn=REPLACE(cn, '//', '/');
 
 INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.11',now());
-- select * from schema_version ORDER BY version ASC;
