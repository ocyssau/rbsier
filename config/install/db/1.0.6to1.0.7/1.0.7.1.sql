-- ************************
ALTER TABLE `notifications`
	ADD COLUMN `uid` VARCHAR(32) NULL AFTER `id`,
	ADD COLUMN `name` VARCHAR(128) NULL AFTER `uid`,
	ADD UNIQUE (`uid`),
	ADD KEY (`name`);


INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.1',now());
-- select * from schema_version ORDER BY version ASC;
