DROP TABLE `pdm_instance_seq`;
CREATE TABLE `pdm_instance_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
  )ENGINE=MyISAM AUTO_INCREMENT=10;
  INSERT INTO pdm_instance_seq(id) VALUES(10);

DROP TABLE pdm_product_instance;
CREATE TABLE `pdm_product_instance` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL DEFAULT '569e971bb57c0',
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `nomenclature` varchar(128) NULL,
  `description` varchar(512) NULL,
  `position` varchar(512) NULL,
  `quantity` int(11) NULL,
  `parent_id` int(11) NULL,
  `path` varchar(256) NULL,
  `of_product_id` int(11) NULL,
  `of_product_uid` varchar(32) NULL,
  `of_product_number` varchar(128) NULL,
  `context_id` varchar(32) NULL,
  `type` varchar(64) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdm_product_instance_uniq1` (`uid`),
  UNIQUE KEY `pdm_product_instance_uniq2` (`number`, `parent_id`),
  KEY `INDEX_pdm_product_instance_1` (`cid`),
  KEY `INDEX_pdm_product_instance_2` (`number`),
  KEY `INDEX_pdm_product_instance_3` (`description`),
  KEY `INDEX_pdm_product_instance_4` (`nomenclature`),
  KEY `INDEX_pdm_product_instance_5` (`uid`),
  KEY `INDEX_pdm_product_instance_6` (`name`),
  KEY `INDEX_pdm_product_instance_7` (`type`),
  KEY `INDEX_pdm_product_instance_8` (`of_product_uid`),
  KEY `INDEX_pdm_product_instance_9` (`of_product_id`),
  KEY `INDEX_pdm_product_instance_10` (`path`),
  KEY `INDEX_pdm_product_version_12` (`of_product_number` ASC)
  );

  
  
ALTER TABLE `pdm_product_instance` 
ADD CONSTRAINT `FK_pdm_product_instance_1`
  FOREIGN KEY (`of_product_id`)
  REFERENCES `pdm_product_version` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_pdm_product_instance_2`
  FOREIGN KEY (`parent_id`)
  REFERENCES `pdm_product_instance` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
ALTER TABLE `pdm_product_version` 
DROP INDEX `INDEX_pdm_product_instance_10` ,
ADD INDEX `INDEX_pdm_product_version_10` (`type` ASC);
  
ALTER TABLE `pdm_product_version` 
DROP INDEX `pdm_product_version_uniq2` ,
ADD UNIQUE INDEX `U_pdm_product_version_1` (`uid` ASC),
DROP INDEX `INDEX_pdm_product_version_11` ,
ADD UNIQUE INDEX `U_pdm_product_version_2` (`number` ASC),
DROP INDEX `pdm_product_version_uniq1` ,
ADD UNIQUE INDEX `U_pdm_product_version_3` (`of_product_uid` ASC, `version` ASC);

ALTER TABLE `pdm_product_version` 
DROP INDEX `name`,
ADD FULLTEXT INDEX `FT_name` (`name` ASC, `number` ASC, `description` ASC);

ALTER TABLE `pdm_product_version` 
ADD COLUMN `lock_by_id` int(11) DEFAULT NULL AFTER `document_uid`,
ADD COLUMN `lock_by_uid` VARCHAR(64) DEFAULT NULL AFTER `lock_by_id`,
ADD COLUMN `locked` DATETIME DEFAULT NULL AFTER `lock_by_uid`,
ADD COLUMN `updated` DATETIME DEFAULT NULL AFTER `locked`,
ADD COLUMN `update_by_id` int(11) DEFAULT NULL AFTER `updated`,
ADD COLUMN `update_by_uid` VARCHAR(64) DEFAULT NULL AFTER `update_by_id`,
ADD COLUMN `created` DATETIME DEFAULT NULL AFTER `update_by_uid`,
ADD COLUMN `create_by_id` int(11) DEFAULT NULL AFTER `created`,
ADD COLUMN `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,

ADD KEY `INDEX_pdm_product_version_11` (`create_by_uid`),
ADD KEY `INDEX_pdm_product_version_12` (`lock_by_uid`),
ADD KEY `INDEX_pdm_product_version_13` (`update_by_uid`),
ADD KEY `INDEX_pdm_product_version_14` (`created`),
ADD KEY `INDEX_pdm_product_version_15` (`locked`),
ADD KEY `INDEX_pdm_product_version_16` (`updated`);

INSERT INTO `pdm_product_instance`
(`id`,
`uid`,
`cid`,
`name`,
`number`,
`nomenclature`,
`description`,
`position`,
`quantity`,
`parent_id`,
`path`,
`of_product_id`,
`of_product_uid`,
`context_id`,
`type`,
`is_root`)
VALUES
('1', 'ranchbe', '569e971bb57c0', 'ranchbe', 'ranchbe', NULL, 'Root Node', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'root', '1');

DROP VIEW reconciliate_documents;
CREATE view reconciliate_documents AS
SELECT id, uid, name, number, version, spacename FROM workitem_documents
UNION
SELECT id, uid, name, number, version, spacename FROM cadlib_documents
UNION
SELECT id, uid, name, number, version, spacename FROM mockup_documents;

ALTER TABLE `workitem_doc_files` 
ADD INDEX `FK_workitem_docfiles_10` (`updated` ASC),
ADD INDEX `FK_workitem_docfiles_11` (`created` ASC);

ALTER TABLE `cadlib_doc_files` 
ADD INDEX `FK_cadlib_docfiles_10` (`updated` ASC),
ADD INDEX `FK_cadlib_docfiles_11` (`created` ASC);

ALTER TABLE `bookshop_doc_files` 
ADD INDEX `FK_bookshop_docfiles_10` (`updated` ASC),
ADD INDEX `FK_bookshop_docfiles_11` (`created` ASC);

ALTER TABLE `mockup_doc_files` 
ADD INDEX `FK_mockup_docfiles_10` (`updated` ASC),
ADD INDEX `FK_mockup_docfiles_11` (`created` ASC);


INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.19',now());
-- select * from schema_version ORDER BY version ASC;
