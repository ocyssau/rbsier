-- compiled 2020-02-16 
-- version 1.2.2 
-- 
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Application/src/Application/Cron/schemaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbgate/src/Rbgate/Logger/LogDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Import/src/Import/Model/PackageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/TypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Docseeder/src/Docseeder/Dao/Template/MaskDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Admin/src/Admin/Dao/ToolDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaProjectLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Ged/src/Ged/Archiver/MediaDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Model/IndexerDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Search/src/Search/Engine/SearchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/ResourceDao.php
-- **********************************************************************
INSERT INTO `acl_resource`
(`id`,`uid`,`cid`,`cn`)
VALUES
(1,
'/app/',
'resourceappli',
'/app/'),
(2,
'/app/admin/',
'resourceadmin',
'/app/admin/'),
(3,
'/app/ged/',
'resourcea5ged',
'/app/ged/'),
(4,
'/app/owner/',
'resourceowner',
'/app/owner/'),
(5,
'/app/admin/acl/',
'resourceadaclf',
'/app/admin/acl/'),
(6,
'/app/admin/wf/',
'resourceadmwf',
'/app/admin/wf/'),
(7,
'/app/gedmanager/',
'resourgedmana',
'/app/gedmanager/'),
(8,
'/app/ged/pdm/',
'resourceb3pdm',
'/app/ged/pdm/'),
(9,
'/app/ged/project/',
'resourproject',
'/app/ged/project/');
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RuleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RightDao.php
-- **********************************************************************
INSERT INTO `acl_right`
(`id`,`name`,`description`)
VALUES
('1', 'READ', 'Read'),
('2', 'CREATE', 'Create'),
('3', 'EDIT', 'Edit'),
('4', 'DELETE', 'Delte'),
('5', 'ADMIN', 'Admin');
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/UserRoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Acl/src/Acl/Model/RoleDao.php
-- **********************************************************************
-- Builtin protected users id < 50
-- Builtin protected roles id < 50
-- Builtin others users id < 100 > 50
-- Builtin others roles id < 100 > 50
-- None conflicts of id withs users ids is permits

INSERT INTO `acl_role`
(`id`,`name`,`uid`,`description`)
VALUES
(2,"administrateur","administrateur","Administrateur"),
(20,"reader","reader","Read only access"),
(50,"concepteurs","concepteurs","Concepteurs CAO"),
(51,"groupLeader","groupleader","chef de groupe de conception"),
(52,"Leader","leader","Chargé d'affaire"),
(53,"checkers","checkers","verificateurs des projets CAO");
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/ClassDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Dao/Sier/PropsetDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/History/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Typemime/TypemimeDao.php
-- **********************************************************************
INSERT INTO `typemimes`
(`id`,`uid`,`name`,`extensions`)
VALUES
(1,"application/argouml","Argo UML",".zargo"),
(2,"x-world/x-3dmf","3Fichiers DMF",".3dmf .3dm .qd3d .qd3"),
(3,"x-world/x-vrml","Model 3D Vrml",".wrl"),
(4,"application/x-3dxmlplugin","3D XML",".3dxml"),
(5,"application/acad","Fichiers AutoCAD (d\'après NCSA)",".dwg"),
(6,"application/CATIA.Analysis","Fichier analyse catia",".CATAnalysis"),
(7,"application/CATIA.Catalog","Fichier catalog catia",".catalog"),
(8,"application/CATIA.Drawing","Fichier drawing catia",".CATDrawing"),
(9,"application/CATIA.Material","Fichier material catia",".CATMaterial"),
(10,"application/CATIA.Model","Fichier model catia",".model"),
(11,"application/CATIA.Part","Fichier part catia",".CATPart"),
(12,"application/CATIA.Process","Fichier process catia",".CATProcess"),
(13,"application/CATIA.Product","Fichier product catia",".CATProduct"),
(14,"application/CATIA.Script","Fichier script catia",".CATScript"),
(15,"application/CATIA.Setting","Fichier setting catia",".CATSetting"),
(16,"application/CATIA.Cgr","Cgr catia",".cgr"),
(17,"application/SOLIDWORK.Assembly","Assemblage solidwork",".sldasm"),
(18,"application/SOLIDWORK.Part","Part solidwork",".sldprt"),
(19,"application/SOLIDWORK.Drawing","Drawing solidwork",".slddrw"),
(20,"application/dsptype","Fichiers TSP",".tsp"),
(21,"application/dxf","Fichiers AutoCAD",".dxf"),
(22,"application/futuresplash","Fichiers Flash Futuresplash",".spl"),
(23,"application/gzip","Fichiers GNU Zip",".gz"),
(24,"application/listenup","Fichiers Listenup",".ptlk"),
(25,"application/mac-binhex40","Fichiers binaires Macintosh ",".hqx"),
(26,"application/mbedlet","Fichiers Mbedlet",".mbd"),
(27,"application/mif","Fichiers FrameMaker Interchange Format",".mif"),
(28,"application/msexcel","Fichiers Microsoft Excel",".xls .xla .xlsx .xlsm .xlam .xlt .xltx .xltm"),
(29,"application/mshelp","Fichiers d\'aide Microsoft Windows",".hlp .chm"),
(30,"application/mspowerpoint","Fichiers Microsoft Powerpoint",".ppt .ppz .pps .pot .potx .potm .pptx .pptm .ppsm .ppsx .ppam .ppa"),
(31,"application/msword","Fichiers Microsoft Word",".doc .dot .dotx .dotm .docx .docm"),
(32,"application/octet-stream","Fichiers executables",".bin .exe .com .dll .class"),
(33,"application/oda","Fichiers Oda",".oda"),
(34,"application/ooimpress","Fichiers oo impress",".odp"),
(35,"application/pdf","Fichiers Adobe PDF",".pdf"),
(36,"application/postscript","Fichiers Adobe Postscript",".ai .eps .ps"),
(37,"application/rtc","Fichiers RTC",".rtc"),
(38,"application/rtf","Fichiers Microsoft RTF",".rtf"),
(39,"application/studiom","Fichiers Studiom",".smp"),
(40,"application/toolbook","Fichiers Toolbook",".tbk"),
(41,"application/vnd.wap.wmlc","Fichiers WMLC (WAP)",".wmlc"),
(42,"application/vnd.wap.wmlscriptc","Fichiers script C WML (WAP)",".wmlsc"),
(43,"application/vocaltec-media-desc","Fichiers Vocaltec Mediadesc",".vmd"),
(44,"application/vocaltec-media-file","Fichiers Vocaltec Media",".vmf"),
(45,"application/x-bcpio","Fichiers BCPIO",".bcpio"),
(46,"application/x-compress","Fichiers -",".z"),
(47,"application/x-cpio","Fichiers CPIO",".cpio"),
(48,"application/x-csh","Fichiers C-Shellscript",".csh"),
(49,"application/x-director","Fichiers -",".dcr .dir .dxr"),
(50,"application/x-dvi","Fichiers DVI",".dvi"),
(51,"application/x-envoy","Fichiers Envoy",".evy"),
(52,"application/x-gtar","Fichiers archives GNU tar",".gtar"),
(53,"application/x-hdf","Fichiers HDF",".hdf"),
(54,"application/x-httpd-php","Fichiers PHP",".php .phtml"),
(55,"application/x-javascript","Fichiers JavaScript cote serveur",".js"),
(56,"application/x-latex","Fichiers source Latex",".latex"),
(57,"application/x-mif","Fichiers FrameMaker Interchange Format",".mif"),
(58,"application/x-netcdf","Fichiers Unidata CDF",".nc .cdf"),
(59,"application/x-nschat","Fichiers NS Chat",".nsc"),
(60,"application/x-sh","Fichiers Bourne Shellscript",".sh"),
(61,"application/x-shar","Fichiers atchives Shell",".shar"),
(62,"application/x-shockwave-flash","Fichiers Flash Shockwave",".swf .cab"),
(63,"application/x-sprite","Fichiers Sprite",".spr .sprite"),
(64,"application/x-stuffit","Fichiers Stuffit",".sit"),
(65,"application/x-supercard","Fichiers Supercard",".sca"),
(66,"application/x-sv4cpio","Fichiers CPIO",".sv4cpio"),
(67,"application/x-sv4crc","Fichiers CPIO avec CRC",".sv4crc"),
(68,"application/x-tar","Fichiers archives tar",".tar"),
(69,"application/x-tcl","Fichiers script TCL",".tcl"),
(70,"application/x-tex","Fichiers TEX",".tex"),
(71,"application/x-texinfo","Fichiers TEXinfo",".texinfo .texi"),
(72,"application/x-troff","Fichiers TROFF (Unix)",".t .tr .roff"),
(73,"application/x-troff-man","Fichiers TROFF avec macros MAN (Unix)",".man .troff"),
(74,"application/x-troff-me","Fichiers TROFF avec macros ME (Unix)",".me .troff"),
(75,"application/x-troff-ms","Fichiers TROFF avec macros MS (Unix)",".me .troff"),
(76,"application/x-ustar","Fichiers archives tar (Posix)",".ustar"),
(77,"application/x-wais-source","Fichiers source WAIS",".src"),
(78,"application/zip","Fichiers archives ZIP",".zip .Z"),
(79,"application/tar","Fichiers archives TAR",".tar"),
(80,"audio/basic","Fichiers son",".au .snd"),
(81,"audio/echospeech","Fichiers Echospeed",".es"),
(82,"audio/tsplayer","Fichiers TS-Player",".tsi"),
(83,"audio/voxware","Fichiers Vox",".vox"),
(84,"audio/x-aiff","Fichiers son AIFF",".aif .aiff .aifc"),
(85,"audio/x-dspeeh","Fichiers parole",".dus .cht"),
(86,"audio/x-midi","Fichiers MIDI",".mid .midi"),
(87,"audio/x-mpeg","Fichiers MPEG",".mp2"),
(88,"audio/x-pn-realaudio","Fichiers RealAudio",".ram .ra"),
(89,"audio/x-pn-realaudio-plugin","Fichiers plugin RealAudio",".rpm"),
(90,"audio/x-qt-stream","Fichiers -",".stream"),
(91,"audio/x-wav","Fichiers Wav",".wav"),
(92,"application/i-deas","Fichiers SDRC I-deas",".unv"),
(93,"application/cao__iges","Format d\'echange CAO IGES",".igs"),
(94,"application/proeng","Fichiers ProEngineer",".prt"),
(95,"application/pstree","product ps","_ps"),
(96,"application/set","Fichiers CAO SET",".set"),
(97,"application/sla","Fichiers stereolithographie",".stl"),
(98,"application/step","Fichiers de donnees STEP",".step .stp"),
(99,"application/vda","Fichiers de surface",".vda"),
(100,"drawing/x-dwf","Fichiers Drawing",".dwf"),
(101,"image/cis-cod","Fichiers CIS-Cod",".cod"),
(102,"image/cmu-raster","Fichiers CMU-Raster",".ras"),
(103,"image/fif","Fichiers FIF",".fif"),
(104,"image/gif","Fichiers GIF",".gif"),
(105,"image/ief","Fichiers IEF",".ief"),
(106,"image/jpeg","Fichiers JPEG",".jpeg .jpg .jpe"),
(107,"image/tiff","Fichiers TIFF",".tiff .tif"),
(108,"image/vasa","Fichiers Vasa",".mcf"),
(109,"image/vnd.wap.wbmp","Fichiers Bitmap (WAP)",".wbmp"),
(110,"image/x-freehand","Fichiers Freehand",".fh4 .fh5 .fhc"),
(111,"image/x-portable-anymap","Fichiers PBM Anymap",".pnm"),
(112,"image/x-portable-bitmap","Fichiers Bitmap PBM",".pbm"),
(113,"image/x-portable-graymap","Fichiers PBM Graymap",".pgm"),
(114,"image/x-portable-pixmap","Fichiers PBM Pixmap",".ppm"),
(115,"image/x-rgb","Fichiers RGB",".rgb"),
(116,"image/x-windowdump","X-Windows Dump",".xwd"),
(117,"image/x-xbitmap","Fichiers XBM",".xbm"),
(118,"image/x-xpixmap","Fichiers XPM",".xpm"),
(119,"image/svg+xml","Scalable vector graphic",".svg"),
(120,"text/comma-separated-values","Fichiers de donnees separees par des virgules",".csv"),
(121,"text/css","Fichiers de feuilles de style CSS",".css"),
(122,"text/html","Fichiers -",".htm .html .shtml"),
(123,"text/javascript","Fichiers JavaScript",".js"),
(124,"text/plain","Fichiers pur texte",".txt"),
(125,"text/richtext","Fichiers texte enrichi (Richtext)",".rtx"),
(126,"text/dat","Fichiers dat",".dat"),
(127,"text/rtf","Fichiers Microsoft RTF",".rtf"),
(128,"text/tab-separated-values","Fichiers de donnees separees par des tabulations",".tsv"),
(129,"text/vnd.wap.wml","Fichiers WML (WAP)",".wml"),
(130,"text/vnd.wap.wmlscript","Fichiers script WML (WAP)",".wmls"),
(131,"text/x-setext","Fichiers SeText",".etx"),
(132,"text/x-sgml","Fichiers SGML",".sgm .sgml"),
(133,"text/x-speech","Fichiers Speech",".talk .spc"),
(134,"text/xml","Fichiers xml",".xml"),
(135,"video/mpeg","Fichiers MPEG",".mpeg .mpg .mpe"),
(136,"video/quicktime","Fichiers Quicktime",".qt .mov"),
(137,"video/vnd.vivo","Fichiers Vivo","viv .vivo"),
(138,"video/x-msvideo","Fichiers Microsoft AVI",".avi"),
(139,"video/x-sgi-movie","Fichiers Movie",".movie"),
(140,"workbook/formulaone","Fichiers FormulaOne",".vts .vtts"),
(141,"application/vnd.oasis.opendocument.text","Open office text",".odt"),
(142,"application/vnd.oasis.opendocument.text-template","Open office text template",".ott"),
(143,"application/vnd.oasis.opendocument.text-web","Open office text web",".oth"),
(144,"application/vnd.oasis.opendocument.text-master","Open office text master",".odm"),
(145,"application/vnd.oasis.opendocument.graphics","Open office",".odg"),
(146,"application/vnd.oasis.opendocument.graphics-template","Open office",".otg"),
(147,"application/vnd.oasis.opendocument.presentation","Open office",".odp"),
(148,"application/vnd.oasis.opendocument.presentation-template","Open office",".otp"),
(149,"application/vnd.oasis.opendocument.spreadsheet","Open office",".ods"),
(150,"application/vnd.oasis.opendocument.spreadsheet-template","Open office",".ots"),
(151,"application/vnd.oasis.opendocument.chart","Open office",".odc"),
(152,"application/vnd.oasis.opendocument.formula","Open office",".odf"),
(153,"application/vnd.oasis.opendocument.database","Open office",".odb"),
(154,"application/vnd.oasis.opendocument.image","Open office",".odi");
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Extended/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Postit/PostitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Instance/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/EndDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StartDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/StandaloneDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/AswitchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/JoinDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/Activity/SplitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/TransitionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/ActivityDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Wf/InstanceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CategoryDao.php
-- **********************************************************************
-- UPDATE `categories` SET name=CONCAT('/', nodelabel, '/');
-- UPDATE `categories` SET dn=CONCAT('/', id, '/');
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Docfile/RoleDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/CheckoutIndexDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DoctypeDao.php
-- **********************************************************************
INSERT INTO `doctypes` (`id`, `uid`, `number`, `name`, `designation`, `can_be_composite`, `file_extensions`, `file_type`, `icon`, `recognition_regexp`, `visu_file_extension`, `pre_store_class`, `pre_store_method`, `post_store_class`, `post_store_method`, `pre_update_class`, `pre_update_method`, `post_update_class`, `post_update_method`, `number_generator_class`, `domain`, `template_id`, `template_uid`, `mask_id`, `mask_uid`, `docseeder`) VALUES
(9,"nofile","nofile","nofile","document sans fichiers associés",1,NULL,"nofile","_default.gif",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(10,"cao__cadds4x","cao__cadds4x","cao__cadds4x","Part cadds4x",0,"_pd","cadds4","cadds.gif",NULL,".pdf",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(11,"cao__cadds5","cao__cadds5","cao__cadds5","Parts cadds5",0,"_fd","cadds5","cadds.gif",NULL,".pdf",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(12,"cao__adrawc4","cao__adrawc4","cao__adrawc4","adraw cadds4x",0,"_pd","adrawc4","cadds.gif",NULL,".pdf",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(13,"cao__adrawc5","cao__adrawc5","cao__adrawc5","adraw cadds5",0,"_fd","adrawc5","cadds.gif",NULL,".pdf",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(14,"cao__camu","cao__camu","cao__camu","product cadds",0,"_db","camu","cadds.gif",NULL,".pdf",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(15,"cao__catpart","cao__catpart","cao__catpart","Fichier part catia",0,"[\'.CATPart\']","file","CATPart.gif","",".wrl","","","\\Sier\\Callback\\Catpart","postStore","","","\\Sier\\Callback\\Catpart","postUpdate",NULL,NULL,NULL,NULL,NULL,NULL,0),
(16,"cao__catdrawing","cao__catdrawing","cao__catdrawing","Fichier drawing catia",0,"[\'.CATDrawing\']","file","CATDrawing.gif","","0","","","\\Sier\\Callback\\Catdrawing","postStore","","","\\Sier\\Callback\\Catdrawing","postUpdate",NULL,NULL,NULL,NULL,NULL,NULL,0),
(17,"cao__catproduct","cao__catproduct","cao__catproduct","Fichier product catia",0,"[\'.CATProduct\']","file","CATProduct.gif","",".3dxml","","","\\Sier\\Callback\\Catproduct","postStore","","","\\Sier\\Callback\\Catproduct","postUpdate",NULL,NULL,NULL,NULL,NULL,NULL,0),
(18,"cao__catmodel","cao__catmodel","cao__catmodel","Fichier model catia",0,".model","file","model.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(19,"cao__catalog","cao__catalog","cao__catalog","Fichier catalog catia",0,"[\'.catalog\']","file","CATCatalog.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(20,"cao__catanalysis","cao__catanalysis","cao__catanalysis","Fichier analyse catia",0,".CATAnalysis","file","CATAnalysis.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(21,"cao__catcatalog","cao__catcatalog","cao__catcatalog","Fichier analyse catia",0,".CATCatalog","file","CATCatalog.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(22,"cao__catmaterial","cao__catmaterial","cao__catmaterial","Fichier material catia",0,".CATMaterial","file","CATMaterial.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(23,"cao__catprocess","cao__catprocess","cao__catprocess","Fichier process catia",0,".CATProcess","file","CATProcess.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(24,"cao__catsetting","cao__catsetting","cao__catsetting","Fichier setting catia",0,".CATSetting","file","CATSetting.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(25,"cao__catscript","cao__catscript","cao__catscript","Fichier script catia",0,".CATScript","file","CATScript.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(30,"cao__pstree","cao__pstree","cao__pstree","product ps",0,"[\'_ps\']","file","cadds.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(31,"cao__drafting","cao__drafting","cao__drafting","Fichiers MATRA Prelude drafting",0,"[\'.z\']","file","catia.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(32,"cao__dxf","cao__dxf","cao__dxf","Fichiers AutoCAD",0,".dxf","file","dwf.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(33,"cao__i-deas","cao__i-deas","cao__i-deas","Fichiers SDRC I-deas",0,"[\'.unv\']","file","_default.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(34,"cao__iges","cao__iges","cao__iges","Format d\'echange CAO IGES",0,"[\'.igs\']","file","_default.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(35,"cao__pro_eng","cao__pro_eng","cao__pro_eng","Fichiers ProEngineer",0,"[\'.prt\']","file","_default.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(36,"cao__set","cao__set","cao__set","Fichiers CAO set",0,"[\'.set\']","file","catia.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(37,"cao__sla","cao__sla","cao__sla","Fichiers stéréolithographie",0,"[\'.stl\']","file","catia.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(38,"cao__autocad","cao__autocad","cao__autocad","Fichiers natifs autocad",0,".dwg","file","autocad.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(39,"cao__step","cao__step","cao__step","Fichiers de données STEP",0,"[\'.step\']","file","catia.gif","",".3dxml","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(40,"cao__vda","cao__vda","cao__vda","Fichiers de surface",0,"[\'.vda\']","file","catia.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(41,"cao__3dxml","cao__3dxml","cao__3dxml","format visualisation dassault système",1,".3dxml","file","xml.gif",NULL,".3dxml",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(42,"cao__cgr","cao__cgr","cao__cgr","Visualisation CATIA",1,".cgr","file","catia.gif",NULL,".wrl",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(43,"cao__sw_assembly","cao__sw_assembly","cao__sw_assembly","SolidWork Assembly",1,"[\'.sldasm\']","file","solidwork.gif","",".3dxml","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(44,"cao__sw_part","cao__sw_part","cao__sw_part","SolidWork Part",1,"[\'.sldprt\']","file","solidwork.gif","",".3dxml","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(45,"cao__sw_drawing","cao__sw_drawing","cao__sw_drawing","SolidWork Drawing",1,"[\'.slddrw\']","file","solidwork.gif","",".pdf","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(60,"calcul__dat","calcul__dat","calcul__dat","Fichier DAT",1,".dat","file","txt.gif",NULL,".pdf",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),

(100,"text__pdf","text__pdf","text__pdf","Fichiers Adobe Acrobat",0,".pdf","file","pdf.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(151,"text__postscript","text__postscript","text__postscript","Fichiers PostScript",0,".ps","file","ps.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(152,"text__epostscrip","text__epostscrip","text__epostscrip","Fichiers PostScript",0,".eps","file","eps.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(153,"text__rtf","text__rtf","text__rtf","Format de texte enrichi",0,".rtf","file","rtf.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(154,"text__htm","text__htm","text__htm","Fichiers HTML",0,".htm","file","html.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(155,"text__html","text__html","text__html","Fichiers HTML",0,".html","file","htm.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(156,"text__plain","text__plain","text__plain","Fichiers texte sans mise en forme",0,".txt","file","txt.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(157,"text__tab-separa","text__tab-separa","text__tab-separa","Fichiers texte avec séparation des valeurs",0,".tsv","file","csv.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(158,"text__virg-separ","text__virg-separ","text__virg-separ","Fichiers texte avec séparation des valeurs",0,".csv","file","csv.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(159,"text__msword","text__msword","text__msword","Fichiers Microsoft Office MSword",0,".doc .docm .docx","file","doc.gif",NULL,".3dxml",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(160,"text__msxls","text__msxls","text__msxls","Fichiers Microsoft Office MSexcel",0,".xls .xlsm .xlsx","file","xls.gif",NULL,".pdf",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(161,"text__msppt","text__msppt","text__msppt","Fichiers Microsoft Office MSppt",0,".ppt .pptm .pptx","file","ppt.gif",NULL,".pdf",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(162,"text__oowriter","text__oowriter","text__oowriter","Fichiers Open Office oo writer",0,".odt","file","odt.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(163,"text__oocalc","text__oocalc","text__oocalc","Fichiers Open Office oo calc",0,".ods","file","ods.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(164,"text__ooimpress","text__ooimpress","text__ooimpress","Fichiers Open Office oo impress",0,".odp","file","odp.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(165,"text__xml","text__xml","text__xml","Fichier XML",1,".xml","file","xml.gif",NULL,".3dxml",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(166,"draw__oooDraw","draw__oooDraw","draw__oooDraw","Dessin open office",1,".odg","file","odg.gif",NULL,".png",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),

(175,"model__msword","model__msword","model__msword","Fichiers model Microsoft Office MSword",0,".dot .dotm .dotx","file","doc.gif",NULL,".3dxml",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(176,"model__msxls","model__msxls","model__msxls","Fichiers model Microsoft Office MSword MSexcel",0,".xlt .xltm .xltx","file","xls.gif",NULL,".3dxml",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(177,"model__msppt","model__msppt","model__msppt","Fichiers model Microsoft Office MSword MSppt",0,".pot .potm .potx","file","ppt.gif",NULL,".3dxml",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(178,"model__oowriter","model__oowriter","model__oowriter","Fichiers model Open Office oo writer",0,".ott","file","ott.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(179,"model__oocalc","model__oocalc","model__oocalc","Fichiers model Open Office oo calc",0,".ots","file","ots.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(180,"model__ooimpress","model__ooimpress","model__ooimpress","Fichiers model Open Office oo impress",0,"[\'.otp\']","file","odf.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),

(201,"archive__gtar","archive__gtar","archive__gtar","Tar GNU",0,".gtar","file","gtar.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(202,"archive__tar","archive__tar","archive__tar","Fichiers tar",0,".tar","file","tar.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(203,"archive__zip","archive__zip","archive__zip","Fichiers compressés ZIP",0,".zip","file","zip.gif",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(204,"archive__gzip","archive__gzip","archive__gzip","Fichiers archive GNU zip",0,".gzip","file","zip.gif",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(205,"archive__gz","archive__gz","archive__gz","Fichiers archive GNU zip",0,"[\'.gz\']","file","zip.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(206,"archive__Z","archive__Z","archive__Z","Fichiers archive unix",0,"[\'.Z\']","file","zip.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(208,"msoffice__query","msoffice__query","msoffice__query","Requete source de données msoffice",1,".dqy","file","xls.gif",NULL,".gif",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(209,"ZargoUML","ZargoUML","ZargoUML","Argo UML compressé",1,".zargo","file","_default.gif",NULL,".pdf",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),

(220,"audio__basic","audio__basic","audio__basic","Fichiers audio basiques",0,"[\'.snd\']","file","mp3.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(221,"audio__x-wav","audio__x-wav","audio__x-wav","Fichiers audio Wave",0,"[\'.wav\']","file","mp3.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(222,"audio__x-aiff","audio__x-aiff","audio__x-aiff","Fichiers audio AIFF",0,"[\'.aif\']","file","mp3.gif","","0","","","","","","","","",NULL,NULL,NULL,NULL,NULL,NULL,0),
(223,"image__gif","image__gif","image__gif","Images gif",0,".gif","file","gif.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(224,"image__jpeg","image__jpeg","image__jpeg","Images Jpeg",0,".jpg","file","jpg.gif","",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(225,"image__tiff","image__tiff","image__tiff","Images Tiff",0,".tif","file","gif.gif",NULL,".3dxml",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(226,"image__png","image__png","image__png","Images png",0,".png","file","png.gif",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(227,"image__svg","image__svg","image__svg","scalable vector graphic",1,".svg","file","_default.gif",NULL,".3dxml",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(228,"video__avi","video__avi","video__avi","video AVI",1,".avi","file","avi.gif",NULL,".png",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(250,"app__rcf","app__rcf","app__rcf","Fichie de check AIF",1,".rcf","file","txt.gif",NULL,".pdf",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocfileDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/DocumentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/PdmLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/BasketDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/IndiceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Ged/Document/Share/PublicUrlDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/SpaceDao.php
-- **********************************************************************
-- DELETE FROM `spaces`;
INSERT INTO `spaces`
(`id`,
`uid`,
`name`,
`designation`)
VALUES
(0,
'default156f045space',
'default',
'default'),
(1,
'product56af045space',
'product',
'products'),
(10,
'wibdd156af045space',
'workitem',
'workitems'),
(15,
'mockupd156f045space',
'mockup',
'mockup'),
(20,
'bookshop156f045space',
'bookshop',
'bookshop'),
(25,
'cadlib156f045space',
'cadlib',
'cadlib');
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Bookshop/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Cadlib/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Wf/Instance/DocumentLinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Workitem/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/IterationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Docfile/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Container/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/History/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/LinkDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/Document/VersionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Space/Mockup/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/MessageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/ArchiveDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/SentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Sys/Message/MailboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/GroupDao.php
-- **********************************************************************
-- Builtin protected users id < 50
-- Builtin protected roles id < 50
-- Builtin others users id < 100 > 50
-- Builtin others roles id < 100 > 50
-- None conflicts of id withs users ids is permits
INSERT INTO `acl_group` (`id`,`name`,`uid`,`description`,`is_active`) VALUES (80,"concepteurs","concepteurs","Concepteurs CAO",1);
INSERT INTO `acl_group` (`id`,`name`,`uid`,`description`,`is_active`) VALUES (81,"groupLeader","groupleader","chef de groupe de conception",1);
INSERT INTO `acl_group` (`id`,`name`,`uid`,`description`,`is_active`) VALUES (82,"Leader","leader","Chargé d'affaire",1);
INSERT INTO `acl_group` (`id`,`name`,`uid`,`description`,`is_active`) VALUES (83,"checkers","checkers","verificateurs des projets CAO",1);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/UserDao.php
-- **********************************************************************
-- Builtin protected users id < 50
-- Builtin protected roles id < 50
-- Builtin others users id < 100 > 50
-- Builtin others roles id < 100 > 50
-- None conflicts of id withs users ids is permits

INSERT INTO `acl_user`
(`id`,`uid`,`login`,`lastname`,`firstname`,`password`,`mail`,`owner_id`,`owner_uid`,`primary_role_id`,`lastlogin`,`is_active`)
VALUES
(1,'admin','admin','admin','admin',md5('admin00'),'admin@ranchbe.fr',2,'admin',1,NULL,1),
(5,'nobody','nobody','nobody','nobody',md5('nobody00'),'nobody@ranchbe.fr',20,'admin',1,NULL,1),
(70,'user','user','user','user',md5('user00'),'user@ranchbe.fr',50,'admin',1,NULL,1);
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/PreferenceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/User/SessionDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/PasswordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/People/SomebodyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Tag/TagDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/ProductDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ConceptDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/ContextDao.php
-- **********************************************************************
INSERT INTO pdm_context_application(`id`, `uid`, `name`)
VALUES
(1,'mecanical', 'mecanical'),
(2,'design', 'design'),
(3,'marketing', 'marketing');
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/InstanceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/Product/VersionDao.php
-- **********************************************************************

INSERT INTO `pdm_product_version`
(`id`,
`uid`,
`name`,
`number`,
`description`,
`type`,
`locked`,
`lock_by_id`,
`lock_by_uid`,
`updated`,
`update_by_id`,
`update_by_uid`,
`created`,
`create_by_id`,
`create_by_uid`
)
VALUES
('1', 'ranchbe', 'ranchbe', 'ranchbe', 'Root Product on top of Ranchbe', 'root', now(), 1, 'admin', now(), 1, 'admin', now(), 1, 'admin' );

-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Pdm/UsageDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Observers/CallbackFactoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/JobDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/CronTaskDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Batch/BatchDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RepositDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/DistantSiteDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/ReplicatedDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/MissingFileDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Vault/RecordDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/BookshopDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/CadlibDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/MockupDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/WorkitemDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/ProjectDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/FavoriteDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Container/AliasDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/UnitDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/CategoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ProcessDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/ContainerDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/DoctypeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/Link/PropertyDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Org/Project/HistoryDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/NotificationDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Rbs/src/Rbs/Notification/IndexDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Change/src/Change/Dao/ChangeDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Sier/src/Sier/Model/Como/ComoDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Discussion/src/Discussion/Dao/CommentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/FilelistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabpanelDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ComponentDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/IndicatorDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/SimplegridDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/StatusboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/ProjectDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/ItemDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/Documentlist/SearchEngineDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/JalonDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/DocumentlistDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TinymceDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/CheckboxDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/TabDao.php
-- **********************************************************************
-- **********************************************************************
-- From file /home/olivier/git/rbsier/module/Portail/src/Portail/Dao/Component/BoxDao.php
-- **********************************************************************
INSERT INTO `cron_task` (`id`, `name`, `description`, `callback_class`, `callback_method`, 
`callback_params`, `owner_id`, `owner_uid`, `status`, `created`, 
`exec_frequency`, `exec_schedule_start`, `exec_schedule_end`, `last_exec`, `is_actif`)
VALUES 
(2,'DbBackupDaily','Database dump to SQL','\\Application\\Cron\\DbBackup','daily',NULL,1,'admin','1','2017-10-20 11:22:49',21600,7,19,'2017-11-03 15:01:24',0),
(3,'FullTextIndexer','Full Text Indexer','\\Search\\Cron\\FtIndexer','run','{\"stringLimit\":8000,\"reindex\":false,\"limit\":10000,\"workitems\":[\"\'00z000-00\'\"],\"bookshops\":[\"\'NOTHING\'\"],\"filetypes\":[\".pdf\",\".doc\",\".docx\",\".ppt\",\".pptx\"]}',1,'admin','1','2017-10-20 17:17:43',43200,0,23,'2019-04-23 15:50:34',0),
(4,'CleanupProcess','Cleanup of old process','\\Docflow\\Cron\\CleanProcess','run',NULL,1,'admin','1','2017-10-27 11:40:40',604800,0,23,'2019-06-24 15:40:01',1),(5,'DbBackupWeekly','Databse dump each weeks','\\Application\\Cron\\DbBackup','weekly',NULL,1,'admin','1','2017-11-03 13:59:50',604800,7,8,'2017-11-03 15:02:36',0),
(6,'JobCleaner','Cleanup obsolete Jobs','\\Batch\\Cron\\JobCleaner','run','{\"maxJobAge\":\"15 day\",\"maxExecutionTime\":\"6 hour\"}',1,'admin','1','2017-11-03 15:12:38',86400,20,21,'2019-06-24 20:07:01',1),
(10,'RbgateReconciliate','Reconciliate Product to Document and populate document properties from Product properties','\\Rbgate\\Cron\\Reconciliate','run','{\"sychroniseProperties\":\"productToDocument\",\"verbose\":1}',1,'admin','1','2018-04-24 11:30:39',600,0,23,'2018-05-15 11:29:06',0),
(8,'RbgateCronBatch','Convert and Import CAD file to Ranchbe PDM','\\Rbgate\\Cron\\Batch','run','{\"workingDirectory\":\"data\\/rbgate\",\"serverUrl\":\"http:\\/\\/pccatia57.sierbla.int:8888\\/rbcs\\/converter\\/service\",\"filter\":\"extension LIKE \\\".cat%\\\"\",\"reconciliateCronName\":\"RbgateReconciliate\",\"spacename\":\"workitem\",\"verbose\":1,\"repositForVisuId\":\"863\\n\",\"onlyUpdatedFromLastRun\":true}',1,'admin','1','2018-04-23 15:03:30',3600,0,23,'2018-05-15 11:29:06',0),
(11,'RepositsReplication','Cron for Replication of reposits','\\Vault\\Cron\\Replicate','run','[]',1,'admin','1','2019-01-11 15:14:55',3600,0,23,NULL,0),
(12,'UpdateResourceBreakdown','Update the table acl_resource_breakdown with newly created datas. Call the SQL proc `updateResourceBreakdown()`','\\Acl\\Cron\\UpdateResourceBreakdown','run','[]',1,'admin','1','2019-03-05 10:40:07',1800,0,23,'2019-06-25 13:26:04',1),
(13,'UpdateMissingFiles','Update misssings files index','Vault\\Cron\\UpdateMissingFiles','run','[]',1,'admin','1','2019-05-03 16:30:28',3600,22,23,'2019-05-03 16:33:44',0),
(14,'ReconciliateDoclink','Reconciliate doclinks','\\Pdm\\Cron\\ReconciliateDoclink','run','{\"spacenames\":[\"workitem\",\"cadlib\"]}',1,'admin','1','2019-05-27 16:21:08',300,0,23,'2019-06-25 13:42:01',1);

ALTER TABLE `cron_task` AUTO_INCREMENT = 100;
INSERT INTO `callbacks`(
	`id`,
	`uid`,
	`cid`,
	`name`,
	`description`,
	`is_actif`,
	`reference_id`,
	`reference_uid`,
	`reference_cid`,
	`spacename`,
	`events`,
	`callback_class`,
	`callback_method`
)
VALUES(
	'1', 
	'5e130f5f76bd8', 
	'callback9u687', 
	'PdmPostCheckin', 
	'When a pdm part is created or checkin, extract pdm datas from the pdmset file, and populate pdm tree', 
	'0', 
	NULL, 
	NULL, 
	'569e92709feb6', 
	NULL, 
	'[\"create.post\",\"checkin.post\"]', 
	'\\Pdm\\Callback\\ImportPdmdataSet', 
	'post'
);

ALTER TABLE `callbacks` AUTO_INCREMENT = 100;
