DROP TABLE IF EXISTS `adodb_logsql`;
DROP TABLE IF EXISTS `liveuser_applications`;
DROP TABLE IF EXISTS `liveuser_area_admin_areas`;
DROP TABLE IF EXISTS `liveuser_areas`;
DROP TABLE IF EXISTS `liveuser_areas_seq`;
DROP TABLE IF EXISTS `liveuser_group_subgroups`;
DROP TABLE IF EXISTS `liveuser_grouprights`;
DROP TABLE IF EXISTS `liveuser_groups`;
DROP TABLE IF EXISTS `liveuser_groups_seq`;
DROP TABLE IF EXISTS `liveuser_groupusers`;
DROP TABLE IF EXISTS `liveuser_perm_users`;
DROP TABLE IF EXISTS `liveuser_perm_users_seq`;
DROP TABLE IF EXISTS `liveuser_rights`;
DROP TABLE IF EXISTS `liveuser_rights_seq`;
DROP TABLE IF EXISTS `liveuser_translations`;
DROP TABLE IF EXISTS `liveuser_userrights`;
DROP TABLE IF EXISTS `liveuser_users`;
DROP TABLE IF EXISTS `liveuser_users_seq`;
DROP TABLE IF EXISTS `liveuser_right_implied`;
DROP TABLE IF EXISTS `tiki_user_tasks`;
DROP TABLE IF EXISTS `tiki_user_tasks_history`;
DROP TABLE IF EXISTS `messu_archive`;
DROP TABLE IF EXISTS `messu_messages`;
DROP TABLE IF EXISTS `messu_sent`;
DROP TABLE IF EXISTS `bookshop_cont_metadata`;
DROP TABLE IF EXISTS `cadlib_cont_metadata`;
DROP TABLE IF EXISTS `mockup_cont_metadata`;
DROP TABLE IF EXISTS `workitem_cont_metadata`;
DROP TABLE IF EXISTS
	`galaxia_activity_roles`, 
	`galaxia_instance_comments`, 
	`galaxia_instance_properties`, 
	`galaxia_roles`, 
	`galaxia_user_roles`, 
	`galaxia_workitems`;

DROP TABLE workitem_history;
DROP TABLE workitem_history_seq;
DROP TABLE bookshop_history;
DROP TABLE bookshop_history_seq;
DROP TABLE cadlib_history;
DROP TABLE cadlib_history_seq;
DROP TABLE mockup_history;
DROP TABLE mockup_history_seq;

-- ################# VIEWS #########################
DROP VIEW IF EXISTS `rt_aif_code_view1`;
DROP VIEW IF EXISTS `view_containerfavorite_users`;
DROP VIEW IF EXISTS `view_countapprove_byuser`;
DROP VIEW IF EXISTS `view_countreject_byuser`;
DROP VIEW IF EXISTS `view_docfile`;
DROP VIEW IF EXISTS `view_document_hierarchy`;
DROP VIEW IF EXISTS `view_filedoc`;
DROP VIEW IF EXISTS `view_filedoc_rel`;
DROP VIEW IF EXISTS `view_instances_activities`;
DROP VIEW IF EXISTS `view_rejected_doc`;
DROP VIEW IF EXISTS `view_rejected_docc`;
DROP VIEW IF EXISTS `view_rt`;
DROP VIEW IF EXISTS `view_rt_b`;


-- ************************
DROP TABLE IF EXISTS `project_mockup_rel`;
DROP TABLE IF EXISTS `project_cadlib_rel`;
DROP TABLE IF EXISTS `project_bookshop_rel`;
DROP TABLE IF EXISTS `project_history`;
DROP TABLE IF EXISTS `project_history_seq`;

-- ************************
DROP TABLE products;
DROP TABLE products_workitem_documents;
DROP TABLE product_history;

DROP TABLE workitem_alias_seq;
DROP TABLE cadlib_alias_seq;
DROP TABLE bookshop_alias_seq;
DROP TABLE mockup_alias_seq;


