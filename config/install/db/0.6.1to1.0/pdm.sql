-- ************************
CREATE TABLE `pdm_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO pdm_seq(id) VALUES(10);

-- ************************
CREATE TABLE `pdm_product_version` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `description` varchar(512) NULL,
  `version` int(11) NOT NULL,
  `of_product_id` int(11) NULL,
  `of_product_uid` varchar(32) NULL,
  `document_id` int(11) NULL,
  `document_uid` varchar(32) NULL,
  `spacename` varchar(32) NULL,
  `type` varchar(64) NULL,
  `materials` varchar(512) NULL,
  `weight` float NULL,
  `volume` float NULL,
  `wetsurface` float NULL,
  `density` float NULL,
  `gravitycenter` varchar(512) NULL,
  `inertiacenter` varchar(512) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdm_product_version_uniq1` (`of_product_uid`,`version`),
  UNIQUE KEY `pdm_product_version_uniq2` (`uid`),
  KEY `INDEX_pdm_product_version_1` (`of_product_uid`),
  KEY `INDEX_pdm_product_version_6` (`of_product_id`),
  KEY `INDEX_pdm_product_version_2` (`version`),
  KEY `INDEX_pdm_product_version_3` (`description`),
  KEY `INDEX_pdm_product_version_4` (`uid`),
  KEY `INDEX_pdm_product_version_5` (`name`),
  KEY `INDEX_pdm_product_version_7` (`document_id`),
  KEY `INDEX_pdm_product_version_8` (`document_uid`),
  KEY `INDEX_pdm_product_version_9` (`spacename`),
  KEY `INDEX_pdm_product_instance_10` (`type`)
 );
 
-- ************************
CREATE TABLE `pdm_product_instance` (
	 `id` int(11) NOT NULL,
	 `uid` varchar(32) NOT NULL,
	 `cid` varchar(32) NOT NULL DEFAULT '569e971bb57c0',
	 `name` varchar(128) NOT NULL,
	 `number` varchar(128) NOT NULL,
	 `nomenclature` varchar(128) NULL,
	 `description` varchar(512) NULL,
	 `position` varchar(512) NULL,
	 `quantity` int(11) NULL,
	 `parent_id` int(11) NULL,
	 `path` varchar(256) NULL,
	 `of_product_id` int(11) NULL,
	 `of_product_uid` varchar(32) NULL,
	 `context_id` varchar(32) NULL,
	 `type` varchar(64) NULL,
	 PRIMARY KEY (`id`),
	 UNIQUE KEY `pdm_product_instance_uniq1` (`uid`),
	 KEY `INDEX_pdm_product_instance_1` (`cid`),
	 KEY `INDEX_pdm_product_instance_2` (`number`),
	 KEY `INDEX_pdm_product_instance_3` (`description`),
	 KEY `INDEX_pdm_product_instance_4` (`nomenclature`),
	 KEY `INDEX_pdm_product_instance_5` (`uid`),
	 KEY `INDEX_pdm_product_instance_6` (`name`),
	 KEY `INDEX_pdm_product_instance_7` (`type`),
	 KEY `INDEX_pdm_product_version_8` (`of_product_uid`),
	 KEY `INDEX_pdm_product_version_9` (`of_product_id`),
	 KEY `INDEX_pdm_product_version_10` (`path`)
 );
