-- ************************
DROP TABLE IF EXISTS `user_sessions`;
CREATE TABLE `user_sessions` (
	`user_id` int(11) NOT NULL,
	`datas` LONGTEXT,
	PRIMARY KEY  (`user_id`)
) ENGINE=MyIsam;

-- USER PREFERENCES DROP AND RECREATE
DROP TABLE IF EXISTS `user_prefs`;
CREATE TABLE `user_prefs` (
	`user_id` int(11) NOT NULL,
	`css_sheet` varchar(32)  NOT NULL default 'default',
	`lang` varchar(32)  NOT NULL default 'default',
	`long_date_format` varchar(32)  NOT NULL default 'default',
	`short_date_format` varchar(32)  NOT NULL default 'default',
	`hour_format` varchar(32)  NOT NULL default 'default',
	`time_zone` varchar(32)  NOT NULL default 'default',
	`wildspace_path` varchar(128)  NOT NULL default 'default',
	`max_record` varchar(128)  NOT NULL default 'default',
	PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB;

-- ************************
DROP TABLE IF EXISTS `postit`;
CREATE TABLE `postit` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(32) DEFAULT NULL,
  `parent_cid` varchar(32) NOT NULL,
  `owner_id` varchar(32) DEFAULT NULL,
  `spacename` varchar(32) NOT NULL,
  `body` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_uid` (`uid`),
  KEY `K_parentId` (`parent_id`),
  KEY `K_parentUid` (`parent_uid`),
  KEY `K_parentCid` (`parent_cid`),
  KEY `K_spaceName` (`spacename`),
  KEY `K_ownerId` (`owner_id`)
) ENGINE=InnoDB;

CREATE TABLE `postit_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO postit_seq(id) VALUES(10);

-- ************************
DROP TABLE IF EXISTS `discussion_comment`;
CREATE TABLE discussion_comment(
	 `id` int NOT NULL AUTO_INCREMENT,
	 `uid` VARCHAR(255) NOT NULL,
	 `cid` CHAR(16) NOT NULL DEFAULT '568be4fc7a0a8',
	 `name` VARCHAR(255) NULL DEFAULT NULL,
	 `discussion_uid` VARCHAR(255) NOT NULL,
	 `owner_id` VARCHAR(255) NULL,
	 `parent_id` int NULL,
	 `parent_uid` varchar(255) NULL,
	 `updated` datetime NOT NULL,
	 `body` TEXT NOT NULL,
	 PRIMARY KEY (`id`),
	 UNIQUE (uid),
	 KEY `DISCUSSION_COMMENT_PARENTID` (`parent_id`),
	 KEY `DISCUSSION_COMMENT_PARENTUID` (`parent_uid`),
	 KEY `DISCUSSION_COMMENT_DISCUSSIONUID` (`discussion_uid`),
	 KEY `DISCUSSION_COMMENT_OWNERID` (`owner_id`),
	 KEY `DISCUSSION_COMMENT_UPDATED` (`updated`)
 );
 
-- ************************
DROP TABLE IF EXISTS `notifications`;
 CREATE TABLE `notifications` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `owner_uid` VARCHAR (128) NOT NULL,
 `owner_id` VARCHAR (128) NOT NULL,
 `reference_uid` VARCHAR (128) NOT NULL,
 `reference_cid` VARCHAR (128) NULL,
 `reference_id` VARCHAR (128) NULL,
 `spacename` VARCHAR (64) NOT NULL,
 `events` VARCHAR (256) NOT NULL,
 `condition` BLOB,
 PRIMARY KEY (`id`),
 KEY `INDEX_notifications_1` (`owner_id`),
 KEY `INDEX_notifications_2` (`reference_uid`),
 KEY `INDEX_notifications_3` (`events`),
 UNIQUE KEY `UNIQ_notifications_1` (`owner_id`,`reference_uid`,`events`)
) ENGINE=InnoDB;

-- ################################################################
ALTER TABLE `doctypes` 
	CHANGE COLUMN `doctype_id` `id` INT(11) NOT NULL,
	CHANGE COLUMN `doctype_number` `number` VARCHAR(128),
	ADD COLUMN `uid` VARCHAR(128) NULL AFTER `id`,
	ADD COLUMN `name` VARCHAR(64) AFTER `number`,
	CHANGE COLUMN `doctype_description` `designation` VARCHAR(128),
	CHANGE `file_extension` `file_extensions` VARCHAR(256) DEFAULT NULL,
	ADD UNIQUE `INDEX_doctypes_1` (`uid`),
	ADD KEY `INDEX_doctypes_2` (`number`),
	ADD KEY `INDEX_doctypes_3` (`designation`);
UPDATE `doctypes` SET `uid`=`number`;

ALTER TABLE `doctypes` 
 DROP COLUMN `script_post_store`,
 DROP COLUMN `script_pre_store`,
 DROP COLUMN `script_post_update`,
 DROP COLUMN `script_pre_update`,
 ADD COLUMN `pre_store_class` varchar(64) DEFAULT NULL,
 ADD COLUMN `pre_store_method` varchar(64) DEFAULT NULL,
 ADD COLUMN `post_store_class` varchar(64) DEFAULT NULL,
 ADD COLUMN `post_store_method` varchar(64) DEFAULT NULL,
 ADD COLUMN `pre_update_class` varchar(64) DEFAULT NULL,
 ADD COLUMN `pre_update_method` varchar(64) DEFAULT NULL,
 ADD COLUMN `post_update_class` varchar(64) DEFAULT NULL,
 ADD COLUMN `post_update_method` varchar(64) DEFAULT NULL;


-- ################################################################
ALTER TABLE `docbasket` 
	CHANGE COLUMN `doc_id` `document_id` INT(11) NOT NULL;
	
-- ************************
CREATE TABLE IF NOT EXISTS `schema_version` (
	`version` varchar(64) NULL,
	`lastModification` DATETIME NULL
) ENGINE=MyIsam;
INSERT INTO schema_version(version,lastModification) VALUES('1.0',now());

-- ################################################################
ALTER TABLE `document_indice`
	DROP INDEX `UC_indice_value`,
	DROP PRIMARY KEY,
	CHANGE COLUMN `document_indice_id` `indice_id` int(11) NOT NULL DEFAULT 0,
	ADD PRIMARY KEY (`indice_id`),
	ADD UNIQUE KEY `UC_indice_value` (`indice_value`);

-- ################################################################
ALTER TABLE `container_favorite`
	CHANGE COLUMN `space_name` `spacename` varchar(16) NOT NULL;

-- ################################################################
DROP TABLE IF EXISTS `callbacks`;
CREATE TABLE `callbacks` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR (128) NOT NULL,
 `cid` VARCHAR (32) NOT NULL,
 `name` VARCHAR (128) NULL,
 `description` VARCHAR (512) NULL,
 `reference_id` VARCHAR (128) NULL,
 `reference_uid` VARCHAR (128) NULL,
 `reference_cid` VARCHAR (128) NULL,
 `spacename` VARCHAR (64) NULL,
 `events` VARCHAR (256) NULL,
 `callback_class` VARCHAR (256) NULL,
 `callback_method` VARCHAR (256) NULL,
 PRIMARY KEY (`id`),
 KEY `INDEX_callbacks_1` (`uid`),
 KEY `INDEX_callbacks_2` (`cid`),
 KEY `INDEX_callbacks_3` (`reference_id`),
 KEY `INDEX_callbacks_4` (`reference_uid`),
 KEY `INDEX_callbacks_5` (`reference_cid`),
 KEY `INDEX_callbacks_8` (`events`),
 UNIQUE KEY `UNIQ_callbacks_1` (`uid`)
) ENGINE=InnoDB;
