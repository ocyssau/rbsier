
-- ************************ ADD USER UID REFS ******************************
-- ************** WI *****************************
ALTER TABLE `workitem_documents` 
	ADD COLUMN  `lock_by_uid` VARCHAR(64) DEFAULT NULL AFTER `lock_by_id`,
	ADD COLUMN  `update_by_uid` VARCHAR(64) DEFAULT NULL AFTER `update_by_id`,    	
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
	ADD KEY `FK_workitem_documents_7` (`create_by_uid`),
	ADD KEY `FK_workitem_documents_8` (`lock_by_uid`),
	ADD KEY `FK_workitem_documents_9` (`update_by_uid`);
    
UPDATE `workitem_documents` AS c SET 
	`lock_by_uid`=(select `login` from `acl_user` where id=c.lock_by_id),
    `update_by_uid`=(select `login` from `acl_user` where id=c.update_by_id),
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id);

-- ******** DOCFILES 
ALTER TABLE `workitem_doc_files` 
	ADD COLUMN  `lock_by_uid` VARCHAR(64) DEFAULT NULL AFTER `lock_by_id`,
	ADD COLUMN  `update_by_uid` VARCHAR(64) DEFAULT NULL AFTER `update_by_id`,    	
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
	ADD KEY `FK_workitem_docfiles_7` (`create_by_uid`),
	ADD KEY `FK_workitem_docfiles_8` (`lock_by_uid`),
	ADD KEY `FK_workitem_docfiles_9` (`update_by_uid`);

UPDATE `workitem_doc_files` AS c SET 
	`lock_by_uid`=(select `login` from `acl_user` where id=c.lock_by_id),
    `update_by_uid`=(select `login` from `acl_user` where id=c.update_by_id),
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id);

-- ******** CONTAINER 
ALTER TABLE `workitems` 
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
    ADD COLUMN  `close_by_uid` VARCHAR(64) DEFAULT NULL AFTER `close_by_id`,
	ADD KEY `K_workitems_3` (`create_by_uid`),
	ADD KEY `K_workitems_4` (`close_by_uid`);
UPDATE `workitems` AS c SET 
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id),
    `close_by_uid`=(select `login` from `acl_user` where id=c.close_by_id);

    
-- ************** CADLIB *****************************
ALTER TABLE `cadlib_documents` 
	ADD COLUMN  `lock_by_uid` VARCHAR(64) DEFAULT NULL AFTER `lock_by_id`,
	ADD COLUMN  `update_by_uid` VARCHAR(64) DEFAULT NULL AFTER `update_by_id`,    	
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
	ADD KEY `FK_cadlib_documents_7` (`create_by_uid`),
	ADD KEY `FK_cadlib_documents_8` (`lock_by_uid`),
	ADD KEY `FK_cadlib_documents_9` (`update_by_uid`);
    
UPDATE `cadlib_documents` AS c SET 
	`lock_by_uid`=(select `login` from `acl_user` where id=c.lock_by_id),
    `update_by_uid`=(select `login` from `acl_user` where id=c.update_by_id),
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id);
-- ******** DOCFILES 
ALTER TABLE `cadlib_doc_files` 
	ADD COLUMN  `lock_by_uid` VARCHAR(64) DEFAULT NULL AFTER `lock_by_id`,
	ADD COLUMN  `update_by_uid` VARCHAR(64) DEFAULT NULL AFTER `update_by_id`,    	
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
	ADD KEY `FK_cadlib_docfiles_7` (`create_by_uid`),
	ADD KEY `FK_cadlib_docfiles_8` (`lock_by_uid`),
	ADD KEY `FK_cadlib_docfiles_9` (`update_by_uid`);
    
UPDATE `cadlib_doc_files` AS c SET 
	`lock_by_uid`=(select `login` from `acl_user` where id=c.lock_by_id),
    `update_by_uid`=(select `login` from `acl_user` where id=c.update_by_id),
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id);
    
-- ******** CONTAINER 
ALTER TABLE `cadlibs` 
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
    ADD COLUMN  `close_by_uid` VARCHAR(64) DEFAULT NULL AFTER `close_by_id`,
	ADD KEY `K_cadlibs_3` (`create_by_uid`),
	ADD KEY `K_cadlibs_4` (`close_by_uid`);
UPDATE `cadlibs` AS c SET 
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id),
    `close_by_uid`=(select `login` from `acl_user` where id=c.close_by_id);
    
-- ************** BOOKSHOP *****************************
ALTER TABLE `bookshop_documents` 
	ADD COLUMN  `lock_by_uid` VARCHAR(64) DEFAULT NULL AFTER `lock_by_id`,
	ADD COLUMN  `update_by_uid` VARCHAR(64) DEFAULT NULL AFTER `update_by_id`,    	
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
	ADD KEY `FK_bookshop_documents_7` (`create_by_uid`),
	ADD KEY `FK_bookshop_documents_8` (`lock_by_uid`),
	ADD KEY `FK_bookshop_documents_9` (`update_by_uid`);
    
UPDATE `bookshop_documents` AS c SET 
	`lock_by_uid`=(select `login` from `acl_user` where id=c.lock_by_id),
    `update_by_uid`=(select `login` from `acl_user` where id=c.update_by_id),
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id);

-- ******** DOCFILES 
ALTER TABLE `bookshop_doc_files` 
	ADD COLUMN  `lock_by_uid` VARCHAR(64) DEFAULT NULL AFTER `lock_by_id`,
	ADD COLUMN  `update_by_uid` VARCHAR(64) DEFAULT NULL AFTER `update_by_id`,    	
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
	ADD KEY `FK_bookshop_docfiles_7` (`create_by_uid`),
	ADD KEY `FK_bookshop_docfiles_8` (`lock_by_uid`),
	ADD KEY `FK_bookshop_docfiles_9` (`update_by_uid`);
    
UPDATE `bookshop_doc_files` AS c SET 
	`lock_by_uid`=(select `login` from `acl_user` where id=c.lock_by_id),
    `update_by_uid`=(select `login` from `acl_user` where id=c.update_by_id),
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id);

-- ******** CONTAINER 
ALTER TABLE `bookshops` 
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
    ADD COLUMN  `close_by_uid` VARCHAR(64) DEFAULT NULL AFTER `close_by_id`,
	ADD KEY `K_bookshops_3` (`create_by_uid`),
	ADD KEY `K_bookshops_4` (`close_by_uid`);
UPDATE `bookshops` AS c SET 
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id),
    `close_by_uid`=(select `login` from `acl_user` where id=c.close_by_id);

    
    
-- ************** MOCKUP *****************************
ALTER TABLE `mockup_documents` 
	ADD COLUMN  `lock_by_uid` VARCHAR(64) DEFAULT NULL AFTER `lock_by_id`,
	ADD COLUMN  `update_by_uid` VARCHAR(64) DEFAULT NULL AFTER `update_by_id`,    	
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
	ADD KEY `FK_mockup_documents_7` (`create_by_uid`),
	ADD KEY `FK_mockup_documents_8` (`lock_by_uid`),
	ADD KEY `FK_mockup_documents_9` (`update_by_uid`);
    
UPDATE `mockup_documents` AS c SET 
	`lock_by_uid`=(select `login` from `acl_user` where id=c.lock_by_id),
    `update_by_uid`=(select `login` from `acl_user` where id=c.update_by_id),
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id);
-- ******** DOCFILES 
ALTER TABLE `mockup_doc_files` 
	ADD COLUMN  `lock_by_uid` VARCHAR(64) DEFAULT NULL AFTER `lock_by_id`,
	ADD COLUMN  `update_by_uid` VARCHAR(64) DEFAULT NULL AFTER `update_by_id`,    	
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
	ADD KEY `FK_mockup_docfiles_7` (`create_by_uid`),
	ADD KEY `FK_mockup_docfiles_8` (`lock_by_uid`),
	ADD KEY `FK_mockup_docfiles_9` (`update_by_uid`);
    
UPDATE `mockup_doc_files` AS c SET 
	`lock_by_uid`=(select `login` from `acl_user` where id=c.lock_by_id),
    `update_by_uid`=(select `login` from `acl_user` where id=c.update_by_id),
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id);

-- ******** CONTAINER 
ALTER TABLE `mockups` 
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
    ADD COLUMN  `close_by_uid` VARCHAR(64) DEFAULT NULL AFTER `close_by_id`,
	ADD KEY `K_mockups_3` (`create_by_uid`),
	ADD KEY `K_mockups_4` (`close_by_uid`);
UPDATE `mockups` AS c SET 
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id),
    `close_by_uid`=(select `login` from `acl_user` where id=c.close_by_id);

    
    
-- ******** PROJECT 
ALTER TABLE `projects` 
    ADD COLUMN  `create_by_uid` VARCHAR(64) DEFAULT NULL AFTER `create_by_id`,
    ADD COLUMN  `close_by_uid` VARCHAR(64) DEFAULT NULL AFTER `close_by_id`,
	ADD KEY `K_projects_3` (`create_by_uid`),
	ADD KEY `K_projects_4` (`close_by_uid`);
UPDATE `projects` AS c SET 
    `create_by_uid`=(select `login` from `acl_user` where id=c.create_by_id),
    `close_by_uid`=(select `login` from `acl_user` where id=c.close_by_id);

-- ******** USERS 
CREATE TABLE `temptable` (SELECT * FROM acl_user);
UPDATE `acl_user` AS c SET 
    `owner_uid`=(select `login` from `temptable` where id=c.owner_id);
DROP TABLE `temptable`;

    
    