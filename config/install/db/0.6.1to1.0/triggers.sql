-- ON CONTAINER DELETE --
DROP TRIGGER IF EXISTS onWorkitemDelete; 
delimiter $$
CREATE TRIGGER onWorkitemDelete AFTER DELETE ON workitems FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='workitem';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='workitem';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='workitem';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
delimiter ;

DROP TRIGGER IF EXISTS onCadlibDelete; 
delimiter $$
CREATE TRIGGER onCadlibDelete AFTER DELETE ON cadlibs FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='cadlib';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='cadlib';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='cadlib';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
delimiter ;

DROP TRIGGER IF EXISTS onBookshopDelete; 
delimiter $$
CREATE TRIGGER onBookshopDelete AFTER DELETE ON bookshops FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='bookshop';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='bookshop';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='bookshop';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
delimiter ;

DROP TRIGGER IF EXISTS onMockupDelete; 
delimiter $$
CREATE TRIGGER onMockupDelete AFTER DELETE ON mockups FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='mockup';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='mockup';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='mockup';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
delimiter ;

DROP TRIGGER IF EXISTS onProjectDelete; 
delimiter $$
CREATE TRIGGER onProjectDelete AFTER DELETE ON projects FOR EACH ROW 
BEGIN
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
delimiter ;

-- ON DOCUMENT DELETE --
DROP TRIGGER IF EXISTS onWIDocumentDelete; 
delimiter $$
CREATE TRIGGER onWIDocumentDelete AFTER DELETE ON workitem_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=10;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
delimiter ;

DROP TRIGGER IF EXISTS onBookshopDocumentDelete; 
delimiter $$
CREATE TRIGGER onBookshopDocumentDelete AFTER DELETE ON bookshop_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=20;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
delimiter ;

DROP TRIGGER IF EXISTS onCadlibDocumentDelete; 
delimiter $$
CREATE TRIGGER onCadlibDocumentDelete AFTER DELETE ON cadlib_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=25;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
delimiter ;

DROP TRIGGER IF EXISTS onMockupDocumentDelete; 
delimiter $$
CREATE TRIGGER onMockupDocumentDelete AFTER DELETE ON mockup_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=15;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END;$$
delimiter ;

-- ON DOCFILE DELETE --
DROP TRIGGER IF EXISTS onWIDocfileDelete; 
delimiter $$
CREATE TRIGGER onWIDocfileDelete AFTER DELETE ON workitem_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='workitem';
END;$$
delimiter ;

DROP TRIGGER IF EXISTS onBookshopDocfileDelete; 
delimiter $$
CREATE TRIGGER onBookshopDocfileDelete AFTER DELETE ON bookshop_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='bookshop';
END;$$
delimiter ;

DROP TRIGGER IF EXISTS onCadlibDocfileDelete; 
delimiter $$
CREATE TRIGGER onCadlibDocfileDelete AFTER DELETE ON cadlib_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='cadlib';
END;$$
delimiter ;

DROP TRIGGER IF EXISTS onMockupDocfileDelete; 
delimiter $$
CREATE TRIGGER onMockupDocfileDelete AFTER DELETE ON mockup_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='mockup';
END;$$
delimiter ;

-- ON CONTAINER INSERT --
DROP TRIGGER IF EXISTS onWorkitemInsert;
delimiter $$
CREATE TRIGGER onWorkitemInsert BEFORE INSERT ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM projects WHERE id=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
END;$$
delimiter ;

-- ON CONTAINER UPDATE --
DROP TRIGGER IF EXISTS onWorkitemUpdate;
delimiter $$
CREATE TRIGGER onWorkitemUpdate BEFORE UPDATE ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM projects WHERE id=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
END;$$
delimiter ;

-- ON PROJECT INSERT --
DROP TRIGGER IF EXISTS onProjectInsert;
delimiter $$
CREATE TRIGGER onProjectInsert BEFORE INSERT ON projects FOR EACH ROW 
BEGIN
	SET NEW.dn=CONCAT('/',NEW.uid,'/');
END;$$
delimiter ;

-- ON PROJECT UPDATE --
DROP TRIGGER IF EXISTS onProjectUpdate;
delimiter $$
CREATE TRIGGER onProjectUpdate BEFORE UPDATE ON projects FOR EACH ROW 
BEGIN
	SET NEW.dn=CONCAT('/',NEW.uid,'/');
END;$$
delimiter ;

-- UPDATE DN, LET TRIGGERS OPERATE
UPDATE projects as wi SET dn='';
UPDATE workitems as wi SET dn='';

-- SELECT * FROM workitems ORDER BY dn ASC;
