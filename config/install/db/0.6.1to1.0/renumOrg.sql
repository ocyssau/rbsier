DROP TRIGGER IF EXISTS onWorkitemUpdate; 
delimiter $$
CREATE TRIGGER onWorkitemUpdate AFTER UPDATE ON workitems FOR EACH ROW 
BEGIN
	IF NEW.id != OLD.id THEN
		UPDATE workitem_documents SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE workitem_doctype_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE workitem_property_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE workitem_category_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE workitem_documents_history SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE workitem_alias SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE checkout_index SET container_id=NEW.id WHERE container_id=OLD.id AND spacename='workitem';
		UPDATE container_favorite SET container_id=NEW.id WHERE container_id=OLD.id AND spacename='workitem';
	END IF;
END;$$

DROP TRIGGER IF EXISTS onCadlibUpdate; 
delimiter $$
CREATE TRIGGER onCadlibUpdate AFTER UPDATE ON cadlibs FOR EACH ROW 
BEGIN
	IF NEW.id != OLD.id THEN
		UPDATE cadlib_documents SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE cadlib_doctype_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE cadlib_property_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE cadlib_category_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE cadlib_documents_history SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE cadlib_alias SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE checkout_index SET container_id=NEW.id WHERE container_id=OLD.id AND spacename='cadlib';
		UPDATE container_favorite SET container_id=NEW.id WHERE container_id=OLD.id AND spacename='cadlib';
	END IF;
END;$$

DROP TRIGGER IF EXISTS onBookshopUpdate; 
delimiter $$
CREATE TRIGGER onBookshopUpdate AFTER UPDATE ON bookshops FOR EACH ROW 
BEGIN
	IF NEW.id != OLD.id THEN
		UPDATE bookshop_documents SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE bookshop_doctype_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE bookshop_property_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE bookshop_category_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE bookshop_documents_history SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE bookshop_alias SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE checkout_index SET container_id=NEW.id WHERE container_id=OLD.id AND spacename='bookshop';
		UPDATE container_favorite SET container_id=NEW.id WHERE container_id=OLD.id AND spacename='bookshop';
	END IF;
END;$$

DROP TRIGGER IF EXISTS onMockupUpdate; 
delimiter $$
CREATE TRIGGER onMockupUpdate AFTER UPDATE ON mockups FOR EACH ROW 
BEGIN
	IF NEW.id != OLD.id THEN
		UPDATE mockup_documents SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE mockup_doctype_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE mockup_property_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE mockup_category_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE mockup_documents_history SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE mockup_alias SET container_id=NEW.id WHERE container_id=OLD.id;
		UPDATE checkout_index SET container_id=NEW.id WHERE container_id=OLD.id AND spacename='mockup';
		UPDATE container_favorite SET container_id=NEW.id WHERE container_id=OLD.id AND spacename='mockup';
	END IF;
END;$$

DROP TRIGGER IF EXISTS onProjectUpdate; 
delimiter $$
CREATE TRIGGER onProjectUpdate AFTER UPDATE ON projects FOR EACH ROW 
BEGIN
	IF NEW.id != OLD.id THEN
		UPDATE project_container_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE project_doctype_rel SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE workitems SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE cadlibs SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE mockups SET parent_id=NEW.id WHERE parent_id=OLD.id;
		UPDATE bookshops SET parent_id=NEW.id WHERE parent_id=OLD.id;
	END IF;
END;$$


-- ATTENTION -- ATTENTION -- ATTENTION -- ATTENTION
-- Triggers must be created before --
-- ################################################

-- Set org_seq and renum projects and containers
CREATE TABLE IF NOT EXISTS `org_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `org_seq` (id) VALUES(500);

SET @position := (SELECT id from org_seq);
UPDATE projects SET `id`=(@position := @position + 1);
UPDATE org_seq SET id=@position LIMIT 1;

SET @position := (SELECT id from org_seq);
UPDATE workitems SET `id`=(@position := @position + 1);
UPDATE org_seq SET id=@position LIMIT 1;

SET @position := (SELECT id from org_seq);
UPDATE bookshops SET `id`=(@position := @position + 1);
UPDATE org_seq SET id=@position LIMIT 1;

SET @position := (SELECT id from org_seq);
UPDATE cadlibs SET `id`=(@position := @position + 1);
UPDATE org_seq SET id=@position LIMIT 1;

SET @position := (SELECT id from org_seq);
UPDATE mockups SET `id`=(@position := @position + 1);
UPDATE org_seq SET id=@position LIMIT 1;

DROP TABLE workitems_seq;
DROP TABLE cadlibs_seq;
DROP TABLE mockups_seq;
DROP TABLE bookshops_seq;
DROP TABLE projects_seq;


DROP TRIGGER IF EXISTS onWorkitemUpdate; 
DROP TRIGGER IF EXISTS onCadlibUpdate; 
DROP TRIGGER IF EXISTS onBookshopUpdate; 
DROP TRIGGER IF EXISTS onBookshopUpdate; 
DROP TRIGGER IF EXISTS onMockupUpdate; 
DROP TRIGGER IF EXISTS onProjectUpdate; 



SET @position := (SELECT id from org_seq);
UPDATE bookshop_alias SET `alias_id`=(@position := @position + 1);
UPDATE org_seq SET id=@position LIMIT 1;

SET @position := (SELECT id from org_seq);
UPDATE cadlib_alias SET `alias_id`=(@position := @position + 1);
UPDATE org_seq SET id=@position LIMIT 1;

SET @position := (SELECT id from org_seq);
UPDATE mockup_alias SET `alias_id`=(@position := @position + 1);
UPDATE org_seq SET id=@position LIMIT 1;

SET @position := (SELECT id from org_seq);
UPDATE workitem_alias SET `alias_id`=(@position := @position + 1);
UPDATE org_seq SET id=@position LIMIT 1;




