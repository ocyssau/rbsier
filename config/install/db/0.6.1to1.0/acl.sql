-- ********************************************
CREATE TABLE `acl_role` (
	`id` int(11) NOT NULL,
	`uid` varchar(64) NOT NULL,
	`cid` varchar(64) NOT NULL DEFAULT 'aclrole3zc547',
	`name` varchar(32) DEFAULT NULL,
	`parentId` int(11) DEFAULT NULL,
	`parentUid` varchar(64) DEFAULT NULL,
	`description` varchar(64) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `uid_UNIQUE` (`uid`),
	UNIQUE KEY `name_UNIQUE` (`name`),
	KEY (`parentId`),
	KEY (`parentUid`)
);

CREATE TABLE `acl_seq` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
) ENGINE=MyIsam AUTO_INCREMENT=10;
INSERT INTO acl_seq (id) VALUES (10);

INSERT INTO `acl_role` (`id`,`name`,`uid`,`description`) VALUES (1, "concepteurs", "concepteurs", "Concepteurs CAO");
INSERT INTO `acl_role` (`id`,`name`,`uid`,`description`) VALUES (2, "groupLeader", "groupleader", "chef de groupe de conception");
INSERT INTO `acl_role` (`id`,`name`,`uid`,`description`) VALUES (3, "Leader", "leader", "Chargé d'affaire");
INSERT INTO `acl_role` (`id`,`name`,`uid`,`description`) VALUES (4, "checkers", "checkers", "verificateurs des projets CAO");

CREATE TABLE IF NOT EXISTS `acl_resource` (
	`id` int(11) NOT NULL,
	`uid` varchar(64) NOT NULL,
	`cid` varchar(64) DEFAULT 'aclresource60',
	`cn` varchar(255) NOT NULL,
	`referToUid` varchar(64) DEFAULT NULL,
	`referToId` int(11) DEFAULT NULL,
	`referToCid` varchar(64) DEFAULT NULL,
	UNIQUE KEY (`id`),
	UNIQUE KEY (`uid`),
	UNIQUE KEY (`cn`),
	KEY (`referToUid`),
	KEY (`referToId`),
	KEY (`referToCid`)
) ENGINE=InnoDB;

CREATE TABLE `acl_right` (
	`id` int(11) NOT NULL,
	`name` varchar(32) NOT NULL,
	`description` varchar(64) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `name_i_idx` (`name`)
) ENGINE=InnoDB;

CREATE TABLE `acl_user_role` (
	`uid` varchar(64) NOT NULL,
	`cid` char(13) NOT NULL DEFAULT 'acluserrole59',
	`userId` int(11) NOT NULL,
	`roleId` int(11) NOT NULL,
	`resourceCn` varchar(255) NOT NULL,
	PRIMARY KEY `id_i_idx` (`userId`,`roleId`,`resourceCn`),
	KEY (`userId`),
	KEY (`roleId`),
	KEY (`resourceCn`)
) ENGINE=InnoDB;

CREATE TABLE `acl_user` (
	`id` int(11) NOT NULL,
	`uid` varchar(128) NOT NULL,
	`login` varchar(128) NOT NULL,
	`lastname` VARCHAR(128) DEFAULT NULL,
	`firstname` VARCHAR(128) DEFAULT NULL,
	`password` varchar(128) DEFAULT NULL,
	`mail` varchar(128) DEFAULT NULL,
	`owner_id` int(11) DEFAULT NULL,
	`owner_uid`  VARCHAR(64) DEFAULT NULL,
	`primary_role_id` int(11) DEFAULT NULL,
	`lastlogin` DATETIME DEFAULT NULL,
	`is_active` tinyint(1) DEFAULT '1',
	`auth_from` varchar(16) DEFAULT 'db',
	PRIMARY KEY (`id`),
	KEY `login_idx` (`login`),
	KEY `owner_uid_idx` (`owner_uid`);
	UNIQUE KEY `uid_idx` (`uid`)
);

-- COPY USERS TO NEW TABLE
INSERT INTO `acl_user`
	(`id`,`uid`,`login`,`lastname`,`firstname`,`password`,`mail`,`owner_id`,`primary_role_id`,`lastlogin`,`is_active`)
SELECT 
    `liveuser_users`.`auth_user_id`,
    `liveuser_users`.`handle`,
    `liveuser_users`.`handle`,
    `liveuser_users`.`handle`,
    `liveuser_users`.`handle`,
    `liveuser_users`.`passwd`,
    `liveuser_users`.`email`,
    1,
    null,
    null,
    `liveuser_users`.`is_active`
FROM `liveuser_users`;

DROP TABLE IF EXISTS `acl_group`;
CREATE TABLE `acl_group` (
	`id` int(11) NOT NULL,
	`uid` varchar(30) NOT NULL,
	`name` varchar(32) DEFAULT NULL,
	`description` varchar(64) DEFAULT NULL,
	`is_active` tinyint(1) DEFAULT '1',
	`owner_id` int(11) DEFAULT NULL,
	`owner_uid`  VARCHAR(64) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `uid_UNIQUE` (`uid`),
	UNIQUE KEY `name_UNIQUE` (`name`)
);

DROP TABLE IF EXISTS `acl_rule`;
CREATE TABLE `acl_rule` (
	`roleId` int(11) NOT NULL,
	`resource` varchar(128) NOT NULL,
	`rightId` int(11) NOT NULL,
	`rule` varchar(16) DEFAULT 'allow',
	PRIMARY KEY (`roleId`,`rightId`, `resource`),
	KEY role_idx(`roleId`),
	KEY right_idx(`rightId`),
	KEY resource_idx(`resource`)
) ENGINE=InnoDB;


-- ###################################################"
DROP VIEW IF EXISTS view_resource_breakdown;
CREATE VIEW view_resource_breakdown AS
(SELECT doc.id, doc.uid, doc.name,doc.cid, cont.id as parent_id, cont.uid as parent_uid FROM workitem_documents as doc JOIN workitems as cont ON doc.container_id=cont.id)
UNION (SELECT cont.id, cont.uid,cont.name,cont.cid, proj.id as parent_id, proj.uid as parent_uid FROM workitems as cont JOIN projects as proj ON cont.parent_id = proj.id)
UNION (SELECT id,uid,name,cid,parent_id, parent_uid FROM projects);

-- ###################################################"
DROP FUNCTION IF EXISTS aclSequence;
DELIMITER $$
CREATE FUNCTION aclSequence()
  RETURNS INT
BEGIN
	DECLARE lastId INT(11);

	UPDATE acl_seq SET id = LAST_INSERT_ID(id+1) LIMIT 1;
	SELECT LAST_INSERT_ID(id) INTO lastId FROM acl_seq;

    set @ret = lastId;
    RETURN @ret;
END;
$$
DELIMITER ;

-- ###################################################"
DROP PROCEDURE IF EXISTS getResourceFromReferUid;
DELIMITER $$
CREATE PROCEDURE `getResourceFromReferUid`(_uid VARCHAR(64)) 
BEGIN
	DECLARE resourceUid varchar(64);

	SELECT uid INTO resourceUid FROM acl_resource WHERE referToUid = _uid;
    IF(resourceUid IS NULL) THEN
		SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE referToUid = (SELECT parent_uid FROM view_resource_breakdown WHERE uid = _uid);
	ELSE
		SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE uid = resourceUid;
	END IF;
END$$
DELIMITER ;

-- ###################################################"
DROP FUNCTION IF EXISTS getResourceCnFromReferUid;
DELIMITER $$
CREATE FUNCTION `getResourceCnFromReferUid`(_uid VARCHAR(64)) 
RETURNS varchar(256)
BEGIN
	DECLARE resourceCn varchar(128);

	SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = _uid;
    IF(resourceCn IS NULL) THEN
		SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = (SELECT parent_uid FROM view_resource_breakdown WHERE uid = _uid);
	END IF;

	return resourceCn;
END$$
DELIMITER ;

-- ###################################################"
DROP FUNCTION IF EXISTS getResourceCnFromReferIdAndCid;
DELIMITER $$
CREATE FUNCTION `getResourceCnFromReferIdAndCid`(_id INT(11), _cid VARCHAR(128)) 
RETURNS varchar(256)
BEGIN
	DECLARE resourceCn varchar(128);

	SELECT cn INTO resourceCn FROM acl_resource WHERE referToId = _id AND referToCid = _cid;
    IF(resourceCn IS NULL) THEN
		SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = (SELECT parent_uid FROM view_resource_breakdown WHERE id = _id AND cid=_cid);
	END IF;

	return resourceCn;
END$$
DELIMITER ;


-- ###################################################"
DROP TABLE IF EXISTS tmp_resource_breakdown;
CREATE TABLE tmp_resource_breakdown
SELECT doc.id, doc.uid, doc.name,doc.cid, doc.container_id as parent_id, cont.uid as parent_uid 
FROM workitem_documents as doc 
JOIN workitems as cont ON doc.container_id=cont.id
UNION (
	SELECT cont.id, cont.uid, cont.name, cont.cid, cont.parent_id as parent_id, proj.uid as parent_uid 
	FROM workitems as cont 
	JOIN projects as proj ON cont.parent_id = proj.id
)
UNION (
	SELECT id,uid,name,cid,parent_id, parent_uid 
	FROM projects
);
CREATE TABLE tmp_resource_breakdown_b SELECT * FROM  tmp_resource_breakdown;
ALTER TABLE tmp_resource_breakdown_b ADD KEY (uid);

-- ####################### POPULATE RESOURCES ############################
DROP FUNCTION IF EXISTS build_path_withuid;
DELIMITER $$
CREATE FUNCTION `build_path_withuid`(_uid VARCHAR(64)) 
RETURNS text
BEGIN
    DECLARE parentUid VARCHAR(64);
    DECLARE cid VARCHAR(64);
    set @ret = '';
    
    while _uid is not null do
      SELECT parent_uid, cid INTO parentUid, cid 
      FROM tmp_resource_breakdown_b 
      WHERE uid = _uid;

    IF(found_rows() != 1) THEN
      SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = 'id not found in table view_resource_breakdown';
    END IF;
    
	IF(parentUid IS NULL) THEN
		set @ret := CONCAT('/app/ged/project/', _uid, @ret);
	ELSE
		set @ret := CONCAT('/', _uid, @ret);
	END IF;

	set _uid := parentUid;
    end while;

    RETURN @ret;
END$$
DELIMITER ;

INSERT INTO `acl_resource`
(`id`,
`uid`,
`cn`,
`referToUid`,
`referToId`,
`referToCid`)
SELECT
aclSequence() `id`,
substr(uuid(),1,8) `uid`,
build_path_withuid(`uid`) `cn`,
`uid` as referToUid,
`id` as referToId,
`cid` as referToCid
FROM `tmp_resource_breakdown`;

DROP TABLE IF EXISTS tmp_resource_breakdown;
DROP TABLE IF EXISTS tmp_resource_breakdown_b;
DROP FUNCTION IF EXISTS build_path_withuid;


INSERT INTO `acl_resource`
(`id`,
`uid`,
`cid`,
`cn`)
VALUES
(1,
'/app/',
'resourceappli',
'/app/'),
(2,
'/app/admin/',
'resourceadmin',
'/app/admin/'),
(3,
'/app/ged/',
'resourcea5ged',
'/app/ged/'),
(4,
'/app/owner/',
'resourceowner',
'/app/owner/'),
(5,
'/app/admin/acl/',
'resourceadaclf',
'/app/admin/acl/'),
(6,
'/app/admin/wf/',
'resourceadmwf',
'/app/admin/wf/'),
(7,
'/app/gedmanager/',
'resourgedmana',
'/app/gedmanager/'),
(8,
'/app/ged/pdm/',
'resourceb3pdm',
'/app/ged/pdm/'),
(9,
'/app/ged/project/',
'resourproject',
'/app/ged/project/');




INSERT INTO `acl_right`
(`id`,
`name`,
`description`)
VALUES
(1,
'READ',
'Read'),
(2,
'CREATE',
'Create'),
(3,
'EDIT',
'Edit'),
(4,
'DELETE',
'Delte'),
(5,
'ADMIN',
'Admin');


