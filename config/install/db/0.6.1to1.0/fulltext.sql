-- ################################################################
CREATE TABLE IF NOT EXISTS `indexer` (
	`document_id` int(11) NOT NULL,
	`docfile_id` int(11) NOT NULL,
	`spacename` VARCHAR(32) NOT NULL,
	`updated` DATETIME NULL,
	`content` TEXT,
	PRIMARY KEY (`docfile_id`, `spacename`),
	UNIQUE `UK_indexer_1` (`document_id`),
	KEY `UK_indexer_2` (`spacename`),
	FULLTEXT (content)
) ENGINE=InnoDb;


-- ################################################################
ALTER TABLE `workitem_documents` ADD FULLTEXT (number,name,designation);
ALTER TABLE `cadlib_documents` ADD FULLTEXT (number,name,designation);
ALTER TABLE `mockup_documents` ADD FULLTEXT (number,name,designation);
ALTER TABLE `bookshop_documents` ADD FULLTEXT (number,name,designation);

ALTER TABLE `workitem_doc_files` ADD FULLTEXT (name);
ALTER TABLE `cadlib_doc_files` ADD FULLTEXT (name);
ALTER TABLE `mockup_doc_files` ADD FULLTEXT (name);
ALTER TABLE `bookshop_doc_files` ADD FULLTEXT (name);

ALTER TABLE `pdm_product_version` ADD FULLTEXT (name, number, description);
ALTER TABLE `pdm_product_instance` ADD FULLTEXT (name, number, description);

ALTER TABLE `workitems` ADD FULLTEXT (number,name,designation);
ALTER TABLE `cadlibs` ADD FULLTEXT (number,name,designation);
ALTER TABLE `mockups` ADD FULLTEXT (number,name,designation);
ALTER TABLE `bookshops` ADD FULLTEXT (number,name,designation);
ALTER TABLE `projects` ADD FULLTEXT (number,name,designation);


CREATE OR REPLACE VIEW `objects` AS
SELECT
	`obj`.`id` AS `id`,
	`obj`.`uid` AS `uid`,
	`obj`.`number` AS `number`,
	`obj`.`name` AS `name`,
	`obj`.`designation` AS `designation`,
	`obj`.`version` AS `version`,
	`obj`.`iteration` AS `iteration`,
	`obj`.`doctype_id` AS `doctypeId`,
	`obj`.`lock_by_id` AS `lockById`,
	`obj`.`locked` AS `locked`,
	`obj`.`updated` AS `updated`,
	`obj`.`update_by_id` AS `updateById`,
	`obj`.`created` AS `created`,
	`obj`.`create_by_id` AS `createById`,
	'workitem' AS `spacename`,
	'569e92709feb6' AS `cid`
	FROM `workitem_documents` `obj`
UNION SELECT
	`obj`.`id` AS `id`,
	`obj`.`uid` AS `uid`,
	`obj`.`number` AS `number`,
	`obj`.`name` AS `name`,
	`obj`.`designation` AS `designation`,
	`obj`.`version` AS `version`,
	`obj`.`iteration` AS `iteration`,
	`obj`.`doctype_id` AS `doctypeId`,
	`obj`.`lock_by_id` AS `lockById`,
	`obj`.`locked` AS `locked`,
	`obj`.`updated` AS `updated`,
	`obj`.`update_by_id` AS `updateById`,
	`obj`.`created` AS `created`,
	`obj`.`create_by_id` AS `createById`,
	'bookshop' AS `spacename`,
	'569e92709feb6' AS `cid`
	FROM `bookshop_documents` `obj`
UNION SELECT
	`obj`.`id` AS `id`,
	`obj`.`uid` AS `uid`,
	`obj`.`number` AS `number`,
	`obj`.`name` AS `name`,
	`obj`.`designation` AS `designation`,
	`obj`.`version` AS `version`,
	`obj`.`iteration` AS `iteration`,
	`obj`.`doctype_id` AS `doctypeId`,
	`obj`.`lock_by_id` AS `lockById`,
	`obj`.`locked` AS `locked`,
	`obj`.`updated` AS `updated`,
	`obj`.`update_by_id` AS `updateById`,
	`obj`.`created` AS `created`,
	`obj`.`create_by_id` AS `createById`,
	'cadlib' AS `spacename`,
	'569e92709feb6' AS `cid`
	FROM `cadlib_documents` `obj`
UNION SELECT
	`obj`.`id` AS `id`,
	`obj`.`uid` AS `uid`,
	`obj`.`number` AS `number`,
	`obj`.`name` AS `name`,
	`obj`.`designation` AS `designation`,
	`obj`.`version` AS `version`,
	`obj`.`iteration` AS `iteration`,
	`obj`.`doctype_id` AS `doctypeId`,
	`obj`.`lock_by_id` AS `lockById`,
	`obj`.`locked` AS `locked`,
	`obj`.`updated` AS `updated`,
	`obj`.`update_by_id` AS `updateById`,
	`obj`.`created` AS `created`,
	`obj`.`create_by_id` AS `createById`,
	'mockup' AS `spacename`,
	'569e92709feb6' AS `cid`
	FROM `mockup_documents` `obj`
UNION SELECT
	`obj`.`id` AS `id`,
	`obj`.`uid` AS `uid`,
	`obj`.`uid` AS `number`,
	`obj`.`name` AS `name`,
	'' AS `designation`,
	'' AS `version`,
	`obj`.`iteration` AS `iteration`,
	-1 AS `doctypeId`,
	`obj`.`lock_by_id` AS `lockById`,
	`obj`.`locked` AS `locked`,
	`obj`.`updated` AS `updated`,
	`obj`.`update_by_id` AS `updateById`,
	`obj`.`created` AS `created`,
	`obj`.`create_by_id` AS `createById`,
	'workitem' AS `spacename`,
	'569e92b86d248' AS `cid`
	FROM `workitem_doc_files` `obj`
UNION
	SELECT
	`obj`.`id` AS `id`,
	`obj`.`uid` AS `uid`,
	`obj`.`uid` AS `number`,
	`obj`.`name` AS `name`,
	'' AS `designation`,
	'' AS `version`,
	`obj`.`iteration` AS `iteration`,
	-1 AS `doctypeId`,
	`obj`.`lock_by_id` AS `lockById`,
	`obj`.`locked` AS `locked`,
	`obj`.`updated` AS `updated`,
	`obj`.`update_by_id` AS `updateById`,
	`obj`.`created` AS `created`,
	`obj`.`create_by_id` AS `createById`,
	'bookshop' AS `spacename`,
	'569e92b86d248' AS `cid`
	FROM `bookshop_doc_files` `obj`
UNION
	SELECT
	`obj`.`id` AS `id`,
	`obj`.`uid` AS `uid`,
	`obj`.`uid` AS `number`,
	`obj`.`name` AS `name`,
	'' AS `designation`,
	'' AS `version`,
	`obj`.`iteration` AS `iteration`,
	-1 AS `doctypeId`,
	`obj`.`lock_by_id` AS `lockById`,
	`obj`.`locked` AS `locked`,
	`obj`.`updated` AS `updated`,
	`obj`.`update_by_id` AS `updateById`,
	`obj`.`created` AS `created`,
	`obj`.`create_by_id` AS `createById`,
	'cadlib' AS `spacename`,
	'569e92b86d248' AS `cid`
	FROM `cadlib_doc_files` `obj`
UNION
	SELECT
	`obj`.`id` AS `id`,
	`obj`.`uid` AS `uid`,
	`obj`.`uid` AS `number`,
	`obj`.`name` AS `name`,
	'' AS `designation`,
	'' AS `version`,
	`obj`.`iteration` AS `iteration`,
	-1 AS `doctypeId`,
	`obj`.`lock_by_id` AS `lockById`,
	`obj`.`locked` AS `locked`,
	`obj`.`updated` AS `updated`,
	`obj`.`update_by_id` AS `updateById`,
	`obj`.`created` AS `created`,
	`obj`.`create_by_id` AS `createById`,
	'mockup' AS `spacename`,
	'569e92b86d248' AS `cid`
	FROM `mockup_doc_files` `obj`
UNION
	SELECT
	`workitems`.`id` AS `id`,
	`workitems`.`uid` AS `uid`,
	`workitems`.`number` AS `number`,
	`workitems`.`name` AS `name`,
	`workitems`.`designation` AS `designation`,
	'' AS `version`,
	'' AS `iteration`,
	-10 AS `doctypeId`,
	'' AS `lockById`,
	'' AS `locked`,
	'' AS `updated`,
	'' AS `updateById`,
	`workitems`.`created` AS `created`,
	`workitems`.`create_by_id` AS `createById`,
	'workitem' AS `spacename`,
	'569e94192201a' AS `cid`
	FROM `workitems`
UNION
	SELECT
	`bookshops`.`id` AS `id`,
	`bookshops`.`uid` AS `uid`,
	`bookshops`.`number` AS `number`,
	`bookshops`.`name` AS `name`,
	`bookshops`.`designation` AS `designation`,
	'' AS `version`,
	'' AS `iteration`,
	-10 AS `doctypeId`,
	'' AS `lockById`,
	'' AS `locked`,
	'' AS `updated`,
	'' AS `updateById`,
	`bookshops`.`created` AS `created`,
	`bookshops`.`create_by_id` AS `createById`,
	'bookshop' AS `spacename`,
	'569e94192201a' AS `cid`
	FROM `bookshops`
UNION
	SELECT
	`cadlibs`.`id` AS `id`,
	`cadlibs`.`uid` AS `uid`,
	`cadlibs`.`number` AS `number`,
	`cadlibs`.`name` AS `name`,
	`cadlibs`.`designation` AS `designation`,
	'' AS `version`,
	'' AS `iteration`,
	-10 AS `doctypeId`,
	'' AS `lockById`,
	'' AS `locked`,
	'' AS `updated`,
	'' AS `updateById`,
	`cadlibs`.`created` AS `created`,
	`cadlibs`.`create_by_id` AS `createById`,
	'cadlib' AS `spacename`,
	'569e94192201a' AS `cid`
	FROM `cadlibs`
UNION
	SELECT
	`mockups`.`id` AS `id`,
	`mockups`.`uid` AS `uid`,
	`mockups`.`number` AS `number`,
	`mockups`.`name` AS `name`,
	`mockups`.`designation` AS `designation`,
	'' AS `version`,
	'' AS `iteration`,
	-10 AS `doctypeId`,
	'' AS `lockById`,
	'' AS `locked`,
	'' AS `updated`,
	'' AS `updateById`,
	`mockups`.`created` AS `created`,
	`mockups`.`create_by_id` AS `createById`,
	'mockup' AS `spacename`,
	'569e94192201a' AS `cid`
	FROM `mockups`
UNION
	SELECT
	`projects`.`id` AS `id`,
	`projects`.`uid` AS `uid`,
	`projects`.`number` AS `number`,
	`projects`.`name` AS `name`,
	`projects`.`designation` AS `designation`,
	'' AS `version`,
	'' AS `iteration`,
	-15 AS `doctypeId`,
	'' AS `lockById`,
	'' AS `locked`,
	'' AS `updated`,
	'' AS `updateById`,
	`projects`.`created` AS `created`,
	`projects`.`create_by_id` AS `createById`,
	'default' AS `spacename`,
	'569e93c6ee156' AS `cid`
	FROM `projects`
UNION
	SELECT
	`pdm_product_version`.`id` AS `id`,
	`pdm_product_version`.`uid` AS `uid`,
	`pdm_product_version`.`number` AS `number`,
	`pdm_product_version`.`name` AS `name`,
	`pdm_product_version`.`description` AS `designation`,
	`pdm_product_version`.`version` AS `version`,
	'' AS `iteration`,
	-20 AS `doctypeId`,
	'' AS `lockById`,
	'' AS `locked`,
	'' AS `updated`,
	'' AS `updateById`,
	'' AS `created`,
	'' AS `createById`,
	`pdm_product_version`.`spacename` AS `spacename`,
	'569e972dd4c2c' AS `cid`
	FROM `pdm_product_version`
UNION
	SELECT
	`pdm_product_instance`.`id` AS `id`,
	`pdm_product_instance`.`uid` AS `uid`,
	`pdm_product_instance`.`number` AS `number`,
	`pdm_product_instance`.`name` AS `name`,
	`pdm_product_instance`.`description` AS `designation`,
	'' AS `version`,
	'' AS `iteration`,
	-21 AS `doctypeId`,
	'' AS `lockById`,
	'' AS `locked`,
	'' AS `updated`,
	'' AS `updateById`,
	'' AS `created`,
	'' AS `createById`,
	'default' AS `spacename`,
	'569e972dd4c2c' AS `cid`
	FROM `pdm_product_instance`;

DROP TABLE IF EXISTS `search_update`;
CREATE TABLE IF NOT EXISTS `search_update` (
	`updated` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyIsam;
INSERT INTO `search_update` SET `updated`=now();

SET NAMES 'utf8' COLLATE 'utf8_general_ci';
DROP PROCEDURE IF EXISTS updateSearch;
DELIMITER $$
CREATE PROCEDURE `updateSearch`(_intervalInMn integer)
BEGIN
	SELECT `updated` INTO @updated FROM `search_update`;
    CASE 
		WHEN @updated > (now() + INTERVAL - _intervalInMn minute) THEN 
			SELECT 'is up to date';
			BEGIN
			END;
		ELSE
			DROP TABLE IF EXISTS search;
   			CREATE TABLE search AS SELECT * FROM objects;
			UPDATE `search_update` SET `updated`=now();
	END CASE;
END$$
DELIMITER ;

CALL updateSearch(1);
