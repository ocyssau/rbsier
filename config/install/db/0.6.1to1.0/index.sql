ALTER TABLE `ranchbe`.`workitems` 
DROP INDEX `UC_workitem_number` ,
ADD UNIQUE INDEX `UC_workitems_number` (`number` ASC),
DROP INDEX `UC_uid` ,
ADD UNIQUE INDEX `UC_workitems_uid` (`uid` ASC),
DROP INDEX `INDEX_name` ,
ADD INDEX `K_workitems_name` (`name` ASC),
DROP INDEX `INDEX_dn` ,
ADD INDEX `K_workitems_dn` (`dn` ASC),
DROP INDEX `number` ,
ADD FULLTEXT INDEX `FT_workitems_1` (`number` ASC, `name` ASC, `designation` ASC);



ALTER TABLE `ranchbe`.`bookshops` 
DROP INDEX `UC_bookshop_number` ,
ADD UNIQUE INDEX `UC_bookshops_number` (`number` ASC),
DROP INDEX `UC_uid` ,
ADD UNIQUE INDEX `UC_bookshops_uid` (`uid` ASC),
DROP INDEX `INDEX_name` ,
ADD INDEX `K_bookshops_name` (`name` ASC),
DROP INDEX `INDEX_dn` ,
ADD INDEX `K_bookshops_dn` (`dn` ASC),
DROP INDEX `number` ,
ADD FULLTEXT INDEX `FT_bookshops_number` (`number` ASC, `name` ASC, `designation` ASC);



ALTER TABLE `ranchbe`.`cadlibs` 
DROP INDEX `UC_cadlib_number` ,
ADD UNIQUE INDEX `UC_cadlibs_number` (`number` ASC),
DROP INDEX `UC_uid` ,
ADD UNIQUE INDEX `UC_cadlibs_uid` (`uid` ASC),
DROP INDEX `INDEX_name` ,
ADD INDEX `K_cadlibs_name` (`name` ASC),
DROP INDEX `INDEX_dn` ,
ADD INDEX `K_cadlibs_dn` (`dn` ASC),
DROP INDEX `number` ,
ADD FULLTEXT INDEX `FT_cadlibs_number` (`number` ASC, `name` ASC, `designation` ASC);



ALTER TABLE `ranchbe`.`mockups` 
DROP INDEX `UC_mockup_number` ,
ADD UNIQUE INDEX `UC_mockups_number` (`number` ASC),
DROP INDEX `UC_uid` ,
ADD UNIQUE INDEX `UC_mockups_uid` (`uid` ASC),
DROP INDEX `INDEX_name` ,
ADD INDEX `K_mockups_name` (`name` ASC),
DROP INDEX `INDEX_dn` ,
ADD INDEX `K_mockups_dn` (`dn` ASC),
DROP INDEX `number` ,
ADD FULLTEXT INDEX `FT_mockups_number` (`number` ASC, `name` ASC, `designation` ASC);
