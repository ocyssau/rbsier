#!/bin/bash
echo "Input Db root password"
read -s password

database="ranchbe"

echo "do you want to create a copy of db: yes or no"
read toCopy

if [ ! "$toCopy" = "no" ]
then
	echo "name of source db"
	read src

	echo "name of target db"
	read database

	echo "Dump source schema"
	mysqldump -u root -p$password $src > /tmp/dump1.sql

	echo "Create new db"
	sql="CREATE DATABASE \`$database\` CHARACTER SET latin1 COLLATE latin1_general_ci;"
	echo $sql
	mysql -u root -p$password -e "$sql"

	echo "import schema to new db"
	mysql -u root -p$password $database < /tmp/dump1.sql
	echo "Database is copied, you must re-execute this scripts"
	exit
else
	echo "Name of database to update"
	read database
fi

#drops all foreign keys :
#@see dropForeignKeys.sql
echo "Drop foreign keys before... Continue, yes or no?"
read rep
if [ ! "$rep" = "yes" ]
	mysql -u root -p$password $database < config/install/db/0.6.1to1.0/dropForeignKeys.sql
then
    exit
fi

# update schema
echo "update schema - config/install/db/0.6.1to1.0/migration1.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/migration1.sql

echo "bookshops - config/install/db/0.6.1to1.0/bookshop.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/bookshop.sql

echo "cadlibs - config/install/db/0.6.1to1.0/cadlib.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/cadlib.sql

echo "mockup - config/install/db/0.6.1to1.0/mockup.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/mockup.sql

echo "workitem - config/install/db/0.6.1to1.0/workitem.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/workitem.sql

echo "project - config/install/db/0.6.1to1.0/project.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/project.sql

echo "pdm - config/install/db/0.6.1to1.0/pdm.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/pdm.sql

echo "wf - fig/install/db/0.6.1to1.0/galaxiaToWf.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/galaxiaToWf.sql

echo "mailbox - config/install/db/0.6.1to1.0/mailbox.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/mailbox.sql

echo "fulltext - config/install/db/0.6.1to1.0/fulltext.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/fulltext.sql

echo "tools - config/install/db/0.6.1to1.0/toolProcedures.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/toolProcedures.sql

echo "tools - config/install/db/0.6.1to1.0/checkoutIndex.sql"
mysql -u root -p$password $database <  config/install/db/0.6.1to1.0/checkoutIndex.sql

echo "view - config/install/db/0.6.1to1.0/view.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/view.sql

echo "Convert to UTF8 - config/install/db/0.6.1to1.0/convertUtf8.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/convertUtf8.sql

echo "Create triggers - config/install/db/0.6.1to1.0/triggers.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/triggers.sql

echo "Renum Org objects - config/install/db/0.6.1to1.0/renumOrg.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/renumOrg.sql

echo "Org Relations - config/install/db/0.6.1to1.0/orgRelation.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/orgRelation.sql

echo "Renum categories - config/install/db/0.6.1to1.0/renumCategories.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/renumCategories.sql

echo "acl - config/install/db/0.6.1to1.0/acl.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/acl.sql

echo "Add user uid refs - config/install/db/0.6.1to1.0/userUidRefs.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/userUidRefs.sql

echo "Install docseeder tables - config/install/db/0.6.1to1.0/docseeder.sql"
mysql -u root -p$password $database < config/install/db/0.6.1to1.0/docseeder.sql

echo "Now, if all is ok, you may drop unused, see config/install/db/0.6.1to1.0/dropUnused.sql"
#mysql -u root -p$password $database < config/install/db/0.6.1to1.0/dropUnused.sql

#in case of error, continue with :
#tail --lines=+[number of line] config/install/db/0.6.1to1.0/[file] > /tmp/reprise.sql
#mysql -u root -p$password $database < /tmp/reprise.sql
#mysqladmin drop -u root -p$password $database
#mysql -u root -p$password $database < config/install/db/0-6.sql


#mysqldbcompare --server1=root@localhost rbsier10:copyOfrbsier10 --skip-diff --skip-row-count --difftype=sql
