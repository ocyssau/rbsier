ALTER TABLE `acl_group` 
ADD CONSTRAINT `FK_acl_group_acl_user1`
  FOREIGN KEY (`owner_id`)
  REFERENCES `acl_user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `acl_rule` 
ADD CONSTRAINT `FK_acl_rule_acl_right1`
  FOREIGN KEY (`rightId`)
  REFERENCES `acl_right` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_acl_rule_acl_role1`
  FOREIGN KEY (`roleId`)
  REFERENCES `acl_role` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_acl_rule_acl_resource1`
  FOREIGN KEY (`resource`)
  REFERENCES `acl_resource` (`cn`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `acl_user_role` 
ADD CONSTRAINT `FK_acl_user_role_acl_user1`
  FOREIGN KEY (`userId`)
  REFERENCES `acl_user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_acl_user_role_acl_role1`
  FOREIGN KEY (`roleId`)
  REFERENCES `acl_role` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_acl_user_role_acl_resource1`
  FOREIGN KEY (`resourceCn`)
  REFERENCES `acl_resource` (`cn`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `pdm_product_instance` 
ADD CONSTRAINT `FK_pdm_product_instance_1`
  FOREIGN KEY (`of_product_id`)
  REFERENCES `pdm_product_version` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `wf_activity` 
ADD CONSTRAINT `FK_wf_activity_1`
  FOREIGN KEY (`processId`)
  REFERENCES `wf_process` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `wf_document_link` 
ADD CONSTRAINT `FK_wf_document_link_1`
  FOREIGN KEY (`childId`)
  REFERENCES `wf_instance` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `wf_instance` 
ADD CONSTRAINT `FK_wf_instance_1`
  FOREIGN KEY (`processId`)
  REFERENCES `wf_process` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `wf_instance_activity` 
ADD CONSTRAINT `FK_wf_instance_activity_1`
  FOREIGN KEY (`activityId`)
  REFERENCES `wf_activity` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `wf_transition` 
ADD CONSTRAINT `FK_wf_transition_1`
  FOREIGN KEY (`parentId`)
  REFERENCES `wf_activity` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_wf_transition_2`
  FOREIGN KEY (`childId`)
  REFERENCES `wf_activity` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `workitem_alias` 
ADD CONSTRAINT `FK_workitem_alias_1`
  FOREIGN KEY (`container_id`)
  REFERENCES `workitems` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `workitem_doc_files` 
ADD CONSTRAINT `FK_workitem_doc_files_1`
  FOREIGN KEY (`document_id`)
  REFERENCES `workitem_documents` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `workitem_doc_rel` 
ADD CONSTRAINT `FK_workitem_doc_rel_1`
  FOREIGN KEY (`parent_id`)
  REFERENCES `workitem_documents` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_doc_rel_2`
  FOREIGN KEY (`child_id`)
  REFERENCES `workitem_documents` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `workitem_documents` 
ADD CONSTRAINT `FK_workitem_documents_1`
  FOREIGN KEY (`container_id`)
  REFERENCES `workitems` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitem_documents_2`
  FOREIGN KEY (`doctype_id`)
  REFERENCES `doctypes` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `workitems` 
ADD CONSTRAINT `FK_workitems_1`
  FOREIGN KEY (`parent_id`)
  REFERENCES `projects` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_workitems_2`
  FOREIGN KEY (`default_process_id`)
  REFERENCES `wf_process` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

-- BOOKSHOP
ALTER TABLE `bookshop_alias` 
ADD CONSTRAINT `FK_bookshop_alias_1`
  FOREIGN KEY (`container_id`)
  REFERENCES `bookshops` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `bookshop_doc_files` 
ADD CONSTRAINT `FK_bookshop_doc_files_1`
  FOREIGN KEY (`document_id`)
  REFERENCES `bookshop_documents` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `bookshop_doc_rel` 
ADD CONSTRAINT `FK_bookshop_doc_rel_1`
  FOREIGN KEY (`parent_id`)
  REFERENCES `bookshop_documents` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_doc_rel_2`
  FOREIGN KEY (`child_id`)
  REFERENCES `bookshop_documents` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `bookshop_documents` 
ADD CONSTRAINT `FK_bookshop_documents_1`
  FOREIGN KEY (`container_id`)
  REFERENCES `bookshops` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshop_documents_2`
  FOREIGN KEY (`doctype_id`)
  REFERENCES `doctypes` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `bookshops` 
ADD CONSTRAINT `FK_bookshops_1`
  FOREIGN KEY (`parent_id`)
  REFERENCES `projects` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_bookshops_2`
  FOREIGN KEY (`default_process_id`)
  REFERENCES `wf_process` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;

  
-- CADLIB
ALTER TABLE `cadlib_alias` 
ADD CONSTRAINT `FK_cadlib_alias_1`
  FOREIGN KEY (`container_id`)
  REFERENCES `cadlibs` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `cadlib_doc_files` 
ADD CONSTRAINT `FK_cadlib_doc_files_1`
  FOREIGN KEY (`document_id`)
  REFERENCES `cadlib_documents` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `cadlib_doc_rel` 
ADD CONSTRAINT `FK_cadlib_doc_rel_1`
  FOREIGN KEY (`parent_id`)
  REFERENCES `cadlib_documents` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_doc_rel_2`
  FOREIGN KEY (`child_id`)
  REFERENCES `cadlib_documents` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `cadlib_documents` 
ADD CONSTRAINT `FK_cadlib_documents_1`
  FOREIGN KEY (`container_id`)
  REFERENCES `cadlibs` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlib_documents_2`
  FOREIGN KEY (`doctype_id`)
  REFERENCES `doctypes` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `cadlibs` 
ADD CONSTRAINT `FK_cadlibs_1`
  FOREIGN KEY (`parent_id`)
  REFERENCES `projects` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_cadlibs_2`
  FOREIGN KEY (`default_process_id`)
  REFERENCES `wf_process` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;
  
  
  
-- MOCKUP
ALTER TABLE `mockup_alias` 
ADD CONSTRAINT `FK_mockup_alias_1`
  FOREIGN KEY (`container_id`)
  REFERENCES `mockups` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `mockup_doc_files` 
ADD CONSTRAINT `FK_mockup_doc_files_1`
  FOREIGN KEY (`document_id`)
  REFERENCES `mockup_documents` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `mockup_doc_rel` 
ADD CONSTRAINT `FK_mockup_doc_rel_1`
  FOREIGN KEY (`parent_id`)
  REFERENCES `mockup_documents` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_doc_rel_2`
  FOREIGN KEY (`child_id`)
  REFERENCES `mockup_documents` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `mockup_documents` 
ADD CONSTRAINT `FK_mockup_documents_1`
  FOREIGN KEY (`container_id`)
  REFERENCES `mockups` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockup_documents_2`
  FOREIGN KEY (`doctype_id`)
  REFERENCES `doctypes` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `mockups` 
ADD CONSTRAINT `FK_mockups_1`
  FOREIGN KEY (`parent_id`)
  REFERENCES `projects` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_mockups_2`
  FOREIGN KEY (`default_process_id`)
  REFERENCES `wf_process` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;
  
  
  
  
  
  
  
  