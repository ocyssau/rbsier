
-- ################################################################
-- PROJECTS
-- ################################################################
ALTER TABLE `projects` 
	CHANGE COLUMN `project_id` `id` INT(11) NOT NULL,
	CHANGE COLUMN `project_number` `number` VARCHAR(128),
	ADD COLUMN `name` VARCHAR(128) NOT NULL AFTER `number`,
	ADD COLUMN `uid` VARCHAR( 140 ) NULL AFTER `id`,
	ADD COLUMN `cid` VARCHAR(64) DEFAULT '569e93c6ee156' AFTER `uid`,
	ADD COLUMN `dn` VARCHAR(128) NULL AFTER `cid`,
	CHANGE COLUMN `project_description` `designation` VARCHAR(128),
	ADD COLUMN `parent_id` INT(11) DEFAULT NULL AFTER `designation`,
	ADD COLUMN `parent_uid` VARCHAR(64) DEFAULT NULL AFTER `parent_id`,
	CHANGE COLUMN `project_state` `life_stage` VARCHAR(32),
	CHANGE COLUMN `project_indice_id` `version` int(11),
	CHANGE COLUMN `open_by` `create_by_id` int(11),
	CHANGE COLUMN `close_by` `close_by_id` int(11),
	ADD COLUMN `created` DATETIME NULL DEFAULT NULL AFTER `open_date`,
	ADD COLUMN `closed` DATETIME NULL DEFAULT NULL AFTER `close_date`,
	ADD COLUMN `planned_closure` DATETIME NULL DEFAULT NULL AFTER `forseen_close_date`,
	ADD COLUMN `acode` int(11) DEFAULT NULL AFTER `life_stage`;
	
ALTER TABLE `projects`
	DROP COLUMN `link_id`,
	DROP COLUMN area_id;
	
UPDATE `projects` SET `name`=number;
UPDATE `projects` SET `uid`=LEFT(UUID(),8);
UPDATE `projects` SET `dn`=CONCAT('/',`uid`,'/');

ALTER TABLE `projects` 
	ADD INDEX `UC_project_name` (`name`),
	ADD INDEX `UC_project_dn` (`dn`),
	ADD UNIQUE KEY `IK_project_uid` (`uid`),
	CHANGE COLUMN `uid` `uid` VARCHAR( 140 ) NOT NULL;

UPDATE projects SET 
	`created`=from_unixtime(`open_date`, '%Y-%m-%d %h:%i:%s'),
	`closed`=from_unixtime(`close_date`, '%Y-%m-%d %h:%i:%s'),
	`planned_closure`=from_unixtime(`forseen_close_date`, '%Y-%m-%d %h:%i:%s');
ALTER TABLE projects 
	DROP COLUMN `open_date`,
	DROP COLUMN `close_date`,
	DROP COLUMN `forseen_close_date`;

-- ************************
ALTER TABLE `project_history` 
	ADD `action_comment` text DEFAULT NULL AFTER `action_date`,
	ADD `data` text DEFAULT NULL,
	ADD `action_started` DATETIME NULL DEFAULT NULL AFTER `action_date`,
	ADD `created` DATETIME NULL DEFAULT NULL AFTER `open_date`,
	ADD `closed` DATETIME NULL DEFAULT NULL AFTER `close_date`,
	ADD `planned_closure` DATETIME NULL DEFAULT NULL AFTER `forseen_close_date`;
UPDATE `project_history` SET 
	`action_started`=from_unixtime(`action_date`, '%Y-%m-%d %h:%i:%s'),
	`created`=from_unixtime(`open_date`, '%Y-%m-%d %h:%i:%s'),
	`closed`=from_unixtime(`close_date`, '%Y-%m-%d %h:%i:%s'),
	`planned_closure`=from_unixtime(`forseen_close_date`, '%Y-%m-%d %h:%i:%s');
ALTER TABLE `project_history`
	DROP COLUMN `action_date`,
	DROP COLUMN `open_date`,
	DROP COLUMN `close_date`,
	DROP COLUMN `forseen_close_date`;

	
-- UPDATE uid on children
UPDATE `workitems` AS c SET `parent_uid`=(select uid from projects where id=c.parent_id);
	
	