-- DROP INSTANCE ACTIVITY WITHOUT PROCESS INSTANCE
DROP TEMPORARY TABLE IF EXISTS tmptable1;
CREATE TEMPORARY TABLE tmptable1 AS (
SELECT wf_instance_activity.id as Aid, wf_instance.id AS Iid FROM wf_instance_activity
LEFT OUTER JOIN wf_instance ON wf_instance_activity.instanceId=wf_instance.id
WHERE wf_instance.id IS NULL
);
DELETE FROM wf_instance_activity WHERE id IN (SELECT * FROM tmptable1);

-- > DELETE FROM wf_instance_activity Where id IN (1,8636,8637,8638,8639,8640,19792,19793,19794,19795,19796);

-- DROP INSTANCE ACTIVITY WITHOUT ACTIVITY
DROP TEMPORARY TABLE IF EXISTS tmptable2;
CREATE TEMPORARY TABLE tmptable2 AS (
SELECT wf_instance_activity.id as Aid FROM wf_instance_activity
LEFT OUTER JOIN wf_activity ON wf_instance_activity.activityId=wf_activity.id
WHERE wf_activity.id IS NULL
);
DELETE FROM wf_instance_activity WHERE id IN (SELECT * FROM tmptable2);


