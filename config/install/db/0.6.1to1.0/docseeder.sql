CREATE TABLE IF NOT EXISTS `docseeder_type` (
 `id` int(11) NOT NULL DEFAULT '0',
 `name` varchar(64) NOT NULL DEFAULT '',
 `designation` varchar(256) DEFAULT NULL,
 `domain` varchar(64) DEFAULT NULL,
 `template_id` int(11) DEFAULT NULL COMMENT 'id of the template docfile',
 `template_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the template docfile',
 `mask_id` int(11) DEFAULT NULL COMMENT 'id of the mask to use',
 `mask_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the mask',
 PRIMARY KEY (`id`),
 UNIQUE KEY (`name`),
 INDEX (`template_id`),
 INDEX (`template_uid`),
 INDEX (`mask_id`),
 INDEX (`mask_uid`),
 INDEX (`domain`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `docseeder_mask` (
 `id` int(11) NOT NULL,
 `uid` varchar(32) NOT NULL,
 `name` varchar(64) NOT NULL DEFAULT '',
 `map` JSON DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY (`uid`),
 UNIQUE KEY (`name`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `sequence_rulesier1`(
  `sequence` int(11) NOT NULL,
  `parameter1` VARCHAR(64) NOT NULL,
  `parameter2` VARCHAR(64) NOT NULL,
  `parameter3` VARCHAR(64) NOT NULL,
  UNIQUE KEY(`parameter1`,`parameter2`,`parameter3`)
) ENGINE=MyISAM;

ALTER TABLE `doctypes` 
	ADD `name` varchar(255) NOT NULL DEFAULT '' after `number`,
	ADD `domain` varchar(64) DEFAULT NULL,
	ADD `template_id` int(11) DEFAULT NULL COMMENT 'id of the template docfile',
	ADD `template_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the template docfile',
	ADD `mask_id` int(11) DEFAULT NULL COMMENT 'id of the mask to use',
	ADD `mask_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the mask',
	ADD `docseeder` boolean COMMENT 'flag to retrieve the valid type for docseeder',
	ADD UNIQUE KEY (`number`),
	ADD INDEX (`template_id`),
	ADD INDEX (`template_uid`),
	ADD INDEX (`mask_id`),
	ADD INDEX (`mask_uid`),
	ADD INDEX (`domain`),
	ADD INDEX (`docseeder`);

UPDATE doctypes set `name` = `number`;

ALTER TABLE workitem_documents
	ADD COLUMN `as_template` BOOLEAN DEFAULT false AFTER `from_document_id`,
	ADD KEY `FK_workitem_documents_10` (`as_template`);

ALTER TABLE cadlib_documents
	ADD COLUMN `as_template` BOOLEAN DEFAULT false AFTER `from_document_id`,
	ADD KEY `FK_cadlib_documents_10` (`as_template`);

ALTER TABLE bookshop_documents
	ADD COLUMN `as_template` BOOLEAN DEFAULT false AFTER `from_document_id`,
	ADD KEY `FK_bookshop_documents_10` (`as_template`);

ALTER TABLE mockup_documents
	ADD COLUMN `as_template` BOOLEAN DEFAULT false AFTER `from_document_id`,
	ADD KEY `FK_mockup_documents_10` (`as_template`);
