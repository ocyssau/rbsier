-- DROP REL WITHOUT CHILD
DROP TEMPORARY TABLE IF EXISTS tmptable1;
CREATE TEMPORARY TABLE tmptable1 AS (
SELECT link.link_id FROM workitem_doc_rel as link
LEFT OUTER JOIN workitem_documents as child ON link.child_id=child.id
WHERE child.id is null
);
DELETE FROM workitem_doc_rel WHERE link_id IN (SELECT * FROM tmptable1);


-- DROP REL WITHOUT PARENT
DROP TEMPORARY TABLE IF EXISTS tmptable2;
CREATE TEMPORARY TABLE tmptable2 AS (
SELECT link.link_id FROM workitem_doc_rel as link
LEFT OUTER JOIN workitem_documents as parent ON link.parent_id=parent.id
WHERE parent.id is null
);
DELETE FROM workitem_doc_rel WHERE link_id IN (SELECT * FROM tmptable2);

DROP TABLE tmptable1;
DROP TABLE tmptable2;


-- CHECK DEFAULT PROCESS
SELECT parent.id, parent.default_process_id as parentId, child.id AS childId FROM workitems as parent
LEFT OUTER JOIN wf_process as child ON child.id=parent.default_process_id
WHERE child.id is null AND parent.default_process_id is not null;

UPDATE workitems SET default_process_id = NULL WHERE id=532;


-- replace 0 by null
UPDATE bookshops SET parent_id = NULL WHERE parent_id=0;
UPDATE cadlibs SET parent_id = NULL WHERE parent_id=0;
UPDATE mockups SET parent_id = NULL WHERE parent_id=0;

UPDATE bookshops SET default_process_id = NULL WHERE default_process_id=0;
UPDATE cadlibs SET default_process_id = NULL WHERE default_process_id=0;
UPDATE mockups SET default_process_id = NULL WHERE default_process_id=0;
