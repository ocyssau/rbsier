-- ************************
-- PROJECT LINKS
-- ************************

-- DOCTYPE
CREATE TABLE `project_doctype_rel` (
	`parent_id` int(11) NOT NULL,
	`parent_uid` varchar(64) NOT NULL,
	`parent_cid` varchar(64) NOT NULL,
	`child_id` int(11) DEFAULT NULL,
	`child_uid` varchar(64) DEFAULT NULL,
	`child_cid` varchar(64) DEFAULT NULL,
	`rdn` varchar(128) DEFAULT NULL,
	KEY `K_project_doctype_rel_1` (`parent_id`),
	KEY `K_project_doctype_rel_2` (`parent_uid`),
	KEY `K_project_doctype_rel_3` (`parent_cid`),
	KEY `K_project_doctype_rel_4` (`child_id`),
	KEY `K_project_doctype_rel_5` (`child_uid`),
	KEY `K_project_doctype_rel_6` (`child_cid`),
	KEY `K_project_doctype_rel_7` (`rdn`),
	UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
	CONSTRAINT `U_project_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
	CONSTRAINT `U_project_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `U_project_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- TRIGGER --
DROP TRIGGER IF EXISTS onProjectDoctypeRelInsert;
delimiter $$
CREATE TRIGGER onProjectDoctypeRelInsert BEFORE INSERT ON project_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM doctypes WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END;$$
delimiter ;

-- CONTAINER
RENAME TABLE `project_container_rel` TO `project_container_rel_rb06`;
CREATE TABLE `project_container_rel` (
	`parent_id` int(11) NOT NULL,
	`parent_uid` varchar(64) NOT NULL,
	`parent_cid` varchar(64) NOT NULL,
	`child_id` int(11) DEFAULT NULL,
	`child_uid` varchar(64) DEFAULT NULL,
	`child_cid` varchar(64) DEFAULT NULL,
	`spacename` varchar(64) NOT NULL,
	`rdn` varchar(128) DEFAULT NULL,
	KEY `K_project_container_rel_1` (`parent_id`),
	KEY `K_project_container_rel_2` (`parent_uid`),
	KEY `K_project_container_rel_3` (`parent_cid`),
	KEY `K_project_container_rel_4` (`child_id`),
	KEY `K_project_container_rel_5` (`child_uid`),
	KEY `K_project_container_rel_6` (`child_cid`),
	KEY `K_project_container_rel_7` (`rdn`),
	UNIQUE KEY `U_project_container_rel_1` (`parent_uid`,`child_uid`),
	CONSTRAINT `FK_project_container_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
	CONSTRAINT `FK_project_container_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- TRIGGER --
DROP TRIGGER IF EXISTS onOrgContainerRelInsert;
delimiter $$
CREATE TRIGGER onOrgContainerRelInsert BEFORE INSERT ON project_container_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	CASE NEW.spacename
		WHEN 'bookshop' THEN
			SELECT uid INTO childUid FROM bookshops WHERE id=NEW.child_id;
		WHEN 'cadlib' THEN
			SELECT uid INTO childUid FROM cadlibs WHERE id=NEW.child_id;
		WHEN 'mockup' THEN
			SELECT uid INTO childUid FROM mockups WHERE id=NEW.child_id;
		WHEN 'workitem' THEN
			SELECT uid INTO childUid FROM workitems WHERE id=NEW.child_id;
	END CASE;
			
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END;$$
delimiter ;

-- PROCESS
CREATE TABLE `project_process_rel` (
	`parent_id` int(11) NOT NULL,
	`parent_uid` varchar(64) NOT NULL,
	`parent_cid` varchar(64) NOT NULL,
	`child_id` int(11) DEFAULT NULL,
	`child_uid` varchar(64) DEFAULT NULL,
	`child_cid` varchar(64) DEFAULT NULL,
	`rdn` varchar(128) DEFAULT NULL,
	KEY `K_project_process_rel_1` (`parent_id`),
	KEY `K_project_process_rel_2` (`parent_uid`),
	KEY `K_project_process_rel_3` (`parent_cid`),
	KEY `K_project_process_rel_4` (`child_id`),
	KEY `K_project_process_rel_5` (`child_uid`),
	KEY `K_project_process_rel_6` (`child_cid`),
	KEY `K_project_process_rel_7` (`rdn`),
	UNIQUE KEY `U_project_process_rel_1` (`parent_id`,`parent_cid`,`child_id`),
	CONSTRAINT `U_project_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
	CONSTRAINT `U_project_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `U_project_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- TRIGGER
DROP TRIGGER IF EXISTS onProjectDoctypeRelInsert;
delimiter $$
CREATE TRIGGER onProjectDoctypeRelInsert BEFORE INSERT ON project_process_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM wf_process WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END;$$
delimiter ;


-- CATEGORY
CREATE TABLE `project_category_rel` (
	`parent_id` int(11) NOT NULL,
	`parent_uid` varchar(64) NOT NULL,
	`parent_cid` varchar(64) NOT NULL,
	`child_id` int(11) DEFAULT NULL,
	`child_uid` varchar(64) DEFAULT NULL,
	`child_cid` varchar(64) DEFAULT NULL,
	`rdn` varchar(128) DEFAULT NULL,
	KEY `K_project_category_rel_1` (`parent_id`),
	KEY `K_project_category_rel_2` (`parent_uid`),
	KEY `K_project_category_rel_3` (`parent_cid`),
	KEY `K_project_category_rel_4` (`child_id`),
	KEY `K_project_category_rel_5` (`child_uid`),
	KEY `K_project_category_rel_6` (`child_cid`),
	KEY `K_project_category_rel_7` (`rdn`),
	UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
	CONSTRAINT `FK_project_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
	CONSTRAINT `FK_project_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

DROP TRIGGER IF EXISTS onOrgCategoryRelInsert;
delimiter $$
CREATE TRIGGER onOrgCategoryRelInsert BEFORE INSERT ON project_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM doctypes WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END;$$
delimiter ;


-- ************************
-- UPDATE TRIGGERS ON ORG OBJECTS
-- ON ORG UPDATE, UPDATE DN WHEN PARENT IS EDITED
-- ************************

-- PROJECT INSERT
DROP TRIGGER IF EXISTS onProjectInsert;
delimiter $$
CREATE TRIGGER onProjectInsert BEFORE INSERT ON projects FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	if(NEW.parent_id IS NOT NULL) THEN
		SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn, NEW.uid, '/');
		SET NEW.parent_uid = parentUid;
	ELSE
		SET NEW.dn=CONCAT('/',NEW.uid,'/');
	END IF;
END;$$
delimiter ;

-- PROJECT UPDATE
DROP TRIGGER IF EXISTS onProjectUpdate;
delimiter $$
CREATE TRIGGER onProjectUpdate BEFORE UPDATE ON projects FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	IF(OLD.parent_id <> NEW.parent_id) THEN
		IF(NEW.parent_id IS NULL) THEN
			SET NEW.dn = CONCAT('/',NEW.uid, '/');
		ELSE
			SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
			SET NEW.dn = CONCAT(parentDn, NEW.uid, '/');
			SET NEW.parent_uid = parentUid;
		END IF;
	END IF;
END;$$
delimiter ;

-- WORKITEM INSERT
DROP TRIGGER IF EXISTS onWorkitemInsert;
delimiter $$
CREATE TRIGGER onWorkitemInsert BEFORE INSERT ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	SET NEW.parent_uid = parentUid;
END;$$
delimiter ;

-- WORKITEM UPDATE
DROP TRIGGER IF EXISTS onWorkitemUpdate;
delimiter $$
CREATE TRIGGER onWorkitemUpdate BEFORE UPDATE ON workitems FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	IF(OLD.parent_id <> NEW.parent_id) THEN
		SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
		SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
	END IF;
END;$$
delimiter ;

-- CADLIB INSERT
DROP TRIGGER IF EXISTS onCadlibInsert;
delimiter $$
CREATE TRIGGER onCadlibInsert BEFORE UPDATE ON cadlibs FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	SET NEW.parent_uid = parentUid;
END;$$
delimiter ;

-- CADLIB UPDATE
DROP TRIGGER IF EXISTS onCadlibUpdate;
delimiter $$
CREATE TRIGGER onCadlibUpdate BEFORE UPDATE ON cadlibs FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	IF(OLD.parent_id <> NEW.parent_id) THEN
		SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
		SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
	END IF;
END;$$
delimiter ;

-- BOOKSHOP INSERT
DROP TRIGGER IF EXISTS onBookshopInsert;
delimiter $$
CREATE TRIGGER onBookshopInsert BEFORE UPDATE ON bookshops FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	SET NEW.parent_uid = parentUid;
END;$$
delimiter ;

-- BOOKSHOP UPDATE
DROP TRIGGER IF EXISTS onBookshopUpdate;
delimiter $$
CREATE TRIGGER onBookshopUpdate BEFORE UPDATE ON bookshops FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	IF(OLD.parent_id <> NEW.parent_id) THEN
		SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
		SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
	END IF;
END;$$
delimiter ;

-- MOCKUP INSERT
DROP TRIGGER IF EXISTS onMockupInsert;
delimiter $$
CREATE TRIGGER onMockupInsert BEFORE UPDATE ON mockups FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	IF(OLD.parent_id <> NEW.parent_id) THEN
		SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
		SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
	END IF;
END;$$
delimiter ;

-- MOCKUP UPDATE
DROP TRIGGER IF EXISTS onMockupUpdate;
delimiter $$
CREATE TRIGGER onMockupUpdate BEFORE UPDATE ON mockups FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	IF(OLD.parent_id <> NEW.parent_id) THEN
		SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
		SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
	END IF;
END;$$
delimiter ;

-- ************************
-- WORKITEM LINKS
-- ************************

-- CATEGORY
RENAME TABLE `workitem_category_rel` TO `workitem_category_rel_rb06`;
CREATE TABLE `workitem_category_rel` LIKE `project_category_rel`;
ALTER TABLE `workitem_category_rel` 
	ADD KEY `K_workitem_category_rel_1` (`parent_id`),
	ADD KEY `K_workitem_category_rel_2` (`parent_uid`),
	ADD KEY `K_workitem_category_rel_3` (`parent_cid`),
	ADD KEY `K_workitem_category_rel_4` (`child_id`),
	ADD KEY `K_workitem_category_rel_5` (`child_uid`),
	ADD KEY `K_workitem_category_rel_6` (`child_cid`),
	ADD KEY `K_workitem_category_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_workitem_category_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_workitem_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `workitem_categories` (`id`) ON DELETE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onWorkitemCategoryRelInsert;
delimiter $$
CREATE TRIGGER onWorkitemCategoryRelInsert BEFORE INSERT ON workitem_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;
	
-- ************************
-- DOCTYPES	
CREATE TABLE `workitem_doctype_rel` LIKE `project_doctype_rel`;
ALTER TABLE `workitem_doctype_rel`
	ADD KEY `K_workitem_doctype_rel_1` (`parent_id`),
	ADD KEY `K_workitem_doctype_rel_2` (`parent_uid`),
	ADD KEY `K_workitem_doctype_rel_3` (`parent_cid`),
	ADD KEY `K_workitem_doctype_rel_4` (`child_id`),
	ADD KEY `K_workitem_doctype_rel_5` (`child_uid`),
	ADD KEY `K_workitem_doctype_rel_6` (`child_cid`),
	ADD KEY `K_workitem_doctype_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_workitem_doctype_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_workitem_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onWorkitemDoctypeRelInsert;
delimiter $$
CREATE TRIGGER onWorkitemDoctypeRelInsert BEFORE INSERT ON workitem_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;
	
-- ************************
-- PROPERTY
DROP TABLE IF EXISTS `workitem_property_rel`;
CREATE TABLE `workitem_property_rel` LIKE `project_property_rel`;
ALTER TABLE `workitem_property_rel`
	ADD KEY `K_workitem_property_rel_1` (`parent_id`),
	ADD KEY `K_workitem_property_rel_2` (`parent_uid`),
	ADD KEY `K_workitem_property_rel_3` (`parent_cid`),
	ADD KEY `K_workitem_property_rel_4` (`child_id`),
	ADD KEY `K_workitem_property_rel_5` (`child_uid`),
	ADD KEY `K_workitem_property_rel_6` (`child_cid`),
	ADD KEY `K_workitem_property_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_workitem_property_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_workitem_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_workitem_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `workitem_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onWorkitemPropertyRelInsert;
delimiter $$
CREATE TRIGGER onWorkitemPropertyRelInsert BEFORE INSERT ON workitem_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;
	
-- ************************
-- MOCKUP LINKS
-- ************************

-- CATEGORY
DROP TABLE IF EXISTS `mockup_category_rel`;
CREATE TABLE `mockup_category_rel` LIKE `project_category_rel`;
ALTER TABLE `mockup_category_rel` 
	ADD KEY `K_mockup_category_rel_1` (`parent_id`),
	ADD KEY `K_mockup_category_rel_2` (`parent_uid`),
	ADD KEY `K_mockup_category_rel_3` (`parent_cid`),
	ADD KEY `K_mockup_category_rel_4` (`child_id`),
	ADD KEY `K_mockup_category_rel_5` (`child_uid`),
	ADD KEY `K_mockup_category_rel_6` (`child_cid`),
	ADD KEY `K_mockup_category_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_mockup_category_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_mockup_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `mockup_categories` (`id`) ON DELETE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onMockupCategoryRelInsert;
delimiter $$
CREATE TRIGGER onMockupCategoryRelInsert BEFORE INSERT ON mockup_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;
	
-- ************************
-- DOCTYPES	
DROP TABLE IF EXISTS `mockup_doctype_rel`;
CREATE TABLE `mockup_doctype_rel` LIKE `project_doctype_rel`;
ALTER TABLE `mockup_doctype_rel`
	ADD KEY `K_mockup_doctype_rel_1` (`parent_id`),
	ADD KEY `K_mockup_doctype_rel_2` (`parent_uid`),
	ADD KEY `K_mockup_doctype_rel_3` (`parent_cid`),
	ADD KEY `K_mockup_doctype_rel_4` (`child_id`),
	ADD KEY `K_mockup_doctype_rel_5` (`child_uid`),
	ADD KEY `K_mockup_doctype_rel_6` (`child_cid`),
	ADD KEY `K_mockup_doctype_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_mockup_doctype_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_mockup_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onMockupDoctypeRelInsert;
delimiter $$
CREATE TRIGGER onMockupDoctypeRelInsert BEFORE INSERT ON mockup_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;
	
-- ************************
-- PROPERTY
DROP TABLE IF EXISTS `mockup_property_rel`;
CREATE TABLE `mockup_property_rel` LIKE `project_property_rel`;
ALTER TABLE `mockup_property_rel`
	ADD KEY `K_mockup_property_rel_1` (`parent_id`),
	ADD KEY `K_mockup_property_rel_2` (`parent_uid`),
	ADD KEY `K_mockup_property_rel_3` (`parent_cid`),
	ADD KEY `K_mockup_property_rel_4` (`child_id`),
	ADD KEY `K_mockup_property_rel_5` (`child_uid`),
	ADD KEY `K_mockup_property_rel_6` (`child_cid`),
	ADD KEY `K_mockup_property_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_mockup_property_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_mockup_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_mockup_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `mockup_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onMockupPropertyRelInsert;
delimiter $$
CREATE TRIGGER onMockupPropertyRelInsert BEFORE INSERT ON mockup_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;


-- ************************
-- CADLIB LINKS
-- ************************

-- CATEGORY
RENAME TABLE `cadlib_category_rel` TO `cadlib_category_rel_rb06`;
CREATE TABLE `cadlib_category_rel` LIKE `project_category_rel`;
ALTER TABLE `cadlib_category_rel` 
	ADD KEY `K_cadlib_category_rel_1` (`parent_id`),
	ADD KEY `K_cadlib_category_rel_2` (`parent_uid`),
	ADD KEY `K_cadlib_category_rel_3` (`parent_cid`),
	ADD KEY `K_cadlib_category_rel_4` (`child_id`),
	ADD KEY `K_cadlib_category_rel_5` (`child_uid`),
	ADD KEY `K_cadlib_category_rel_6` (`child_cid`),
	ADD KEY `K_cadlib_category_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_cadlib_category_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_cadlib_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `cadlib_categories` (`id`) ON DELETE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onCadlibCategoryRelInsert;
delimiter $$
CREATE TRIGGER onCadlibCategoryRelInsert BEFORE INSERT ON cadlib_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;
	
-- ************************
-- DOCTYPES	
DROP TABLE IF EXISTS `cadlib_doctype_rel`;
CREATE TABLE `cadlib_doctype_rel` LIKE `project_doctype_rel`;
ALTER TABLE `cadlib_doctype_rel`
	ADD KEY `K_cadlib_doctype_rel_1` (`parent_id`),
	ADD KEY `K_cadlib_doctype_rel_2` (`parent_uid`),
	ADD KEY `K_cadlib_doctype_rel_3` (`parent_cid`),
	ADD KEY `K_cadlib_doctype_rel_4` (`child_id`),
	ADD KEY `K_cadlib_doctype_rel_5` (`child_uid`),
	ADD KEY `K_cadlib_doctype_rel_6` (`child_cid`),
	ADD KEY `K_cadlib_doctype_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_cadlib_doctype_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_cadlib_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onCadlibDoctypeRelInsert;
delimiter $$
CREATE TRIGGER onCadlibDoctypeRelInsert BEFORE INSERT ON cadlib_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;

-- ************************
-- PROPERTY
DROP TABLE IF EXISTS `cadlib_property_rel`;
CREATE TABLE `cadlib_property_rel` LIKE `project_property_rel`;
ALTER TABLE `cadlib_property_rel`
	ADD KEY `K_cadlib_property_rel_1` (`parent_id`),
	ADD KEY `K_cadlib_property_rel_2` (`parent_uid`),
	ADD KEY `K_cadlib_property_rel_3` (`parent_cid`),
	ADD KEY `K_cadlib_property_rel_4` (`child_id`),
	ADD KEY `K_cadlib_property_rel_5` (`child_uid`),
	ADD KEY `K_cadlib_property_rel_6` (`child_cid`),
	ADD KEY `K_cadlib_property_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_cadlib_property_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_cadlib_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_cadlib_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `cadlib_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onCadlibPropertyRelInsert;
delimiter $$
CREATE TRIGGER onCadlibPropertyRelInsert BEFORE INSERT ON cadlib_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;

-- ************************
-- BOOKSHOP LINKS
-- ************************

-- CATEGORY
RENAME TABLE `bookshop_category_rel` TO `bookshop_category_rel_rb06`;
CREATE TABLE `bookshop_category_rel` LIKE `project_category_rel`;
ALTER TABLE `bookshop_category_rel` 
	ADD KEY `K_bookshop_category_rel_1` (`parent_id`),
	ADD KEY `K_bookshop_category_rel_2` (`parent_uid`),
	ADD KEY `K_bookshop_category_rel_3` (`parent_cid`),
	ADD KEY `K_bookshop_category_rel_4` (`child_id`),
	ADD KEY `K_bookshop_category_rel_5` (`child_uid`),
	ADD KEY `K_bookshop_category_rel_6` (`child_cid`),
	ADD KEY `K_bookshop_category_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_bookshop_category_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_bookshop_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `bookshop_categories` (`id`) ON DELETE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onBookshopCategoryRelInsert;
delimiter $$
CREATE TRIGGER onBookshopCategoryRelInsert BEFORE INSERT ON bookshop_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;
	
-- ************************
-- DOCTYPES	
DROP TABLE IF EXISTS `bookshop_doctype_rel`;
CREATE TABLE `bookshop_doctype_rel` LIKE `project_doctype_rel`;
ALTER TABLE `bookshop_doctype_rel`
	ADD KEY `K_bookshop_doctype_rel_1` (`parent_id`),
	ADD KEY `K_bookshop_doctype_rel_2` (`parent_uid`),
	ADD KEY `K_bookshop_doctype_rel_3` (`parent_cid`),
	ADD KEY `K_bookshop_doctype_rel_4` (`child_id`),
	ADD KEY `K_bookshop_doctype_rel_5` (`child_uid`),
	ADD KEY `K_bookshop_doctype_rel_6` (`child_cid`),
	ADD KEY `K_bookshop_doctype_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_bookshop_doctype_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_bookshop_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onBookshopDoctypeRelInsert;
delimiter $$
CREATE TRIGGER onBookshopDoctypeRelInsert BEFORE INSERT ON bookshop_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;

-- ************************
-- PROPERTY
DROP TABLE IF EXISTS `bookshop_property_rel`;
CREATE TABLE `bookshop_property_rel` LIKE `project_property_rel`;
ALTER TABLE `bookshop_property_rel`
	ADD KEY `K_bookshop_property_rel_1` (`parent_id`),
	ADD KEY `K_bookshop_property_rel_2` (`parent_uid`),
	ADD KEY `K_bookshop_property_rel_3` (`parent_cid`),
	ADD KEY `K_bookshop_property_rel_4` (`child_id`),
	ADD KEY `K_bookshop_property_rel_5` (`child_uid`),
	ADD KEY `K_bookshop_property_rel_6` (`child_cid`),
	ADD KEY `K_bookshop_property_rel_7` (`rdn`),
	ADD UNIQUE KEY `U_bookshop_property_rel_1` (`parent_uid`,`child_uid`),
	ADD CONSTRAINT `FK_bookshop_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_bookshop_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `bookshop_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- TRIGGER, Update rdn from parentDn when insert
-- Note: when update, rdn is updated by constraints
DROP TRIGGER IF EXISTS onBookshopPropertyRelInsert;
delimiter $$
CREATE TRIGGER onBookshopPropertyRelInsert BEFORE INSERT ON bookshop_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END;$$
delimiter ;

-- ###################################################"
-- ORGANIZATION BREAKDOWN
-- ###################################################"
DROP VIEW IF EXISTS view_org_breakdown;
CREATE VIEW view_org_breakdown AS
SELECT org.id, org.uid, org.cid, org.dn, org.name, org.parent_id, org.parent_uid, 'default' as spacename FROM projects as org 
UNION 
SELECT org.id, org.uid, org.cid, org.dn, org.name, org.parent_id, org.parent_uid, 'workitem' as spacename FROM workitems as org
UNION 
SELECT org.id, org.uid, org.cid, org.dn, org.name, org.parent_id, org.parent_uid, 'cadlib' as spacename FROM cadlibs as org
UNION 
SELECT org.id, org.uid, org.cid, org.dn, org.name, org.parent_id, org.parent_uid, 'bookshop' as spacename FROM bookshops as org
UNION 
SELECT org.id, org.uid, org.cid, org.dn, org.name, org.parent_id, org.parent_uid, 'mockup' as spacename FROM mockups as org;

CREATE VIEW view_workitem_doctype_rel AS 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM workitem_doctype_rel 
UNION 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;






-- ###################################################"
-- MIGRATE DATAS
-- ###################################################"

-- CATEGORIES
INSERT INTO workitem_category_rel (parent_id, parent_uid, parent_cid, child_id)
SELECT workitem_id, workitems.uid, workitems.cid, category_id FROM workitem_category_rel_rb06 JOIN workitems ON workitems.id=workitem_category_rel_rb06.workitem_id;

INSERT INTO cadlib_category_rel (parent_id, parent_uid, parent_cid, child_id)
SELECT cadlib_id, cadlibs.uid, cadlibs.cid, category_id FROM cadlib_category_rel_rb06 JOIN cadlibs ON cadlibs.id=cadlib_category_rel_rb06.cadlib_id;

INSERT INTO bookshop_category_rel (parent_id, parent_uid, parent_cid, child_id)
SELECT bookshop_id, bookshops.uid, bookshops.cid, category_id FROM bookshop_category_rel_rb06 JOIN bookshops ON bookshops.id=bookshop_category_rel_rb06.bookshop_id;

-- DOCTYPES
INSERT INTO workitem_doctype_rel (parent_id, parent_uid, parent_cid, child_id)
SELECT workitem_id, cont.uid, cont.cid, doctype_id FROM workitem_doctype_process as link JOIN workitems as cont ON cont.id=link.workitem_id;

INSERT INTO cadlib_doctype_rel (parent_id, parent_uid, parent_cid, child_id)
SELECT cadlib_id, cont.uid, cont.cid, doctype_id FROM cadlib_doctype_process as link JOIN cadlibs as cont ON cont.id=link.cadlib_id;

INSERT INTO bookshop_doctype_rel (parent_id, parent_uid, parent_cid, child_id)
SELECT bookshop_id, cont.uid, cont.cid, doctype_id FROM bookshop_doctype_process as link JOIN bookshops as cont ON cont.id=link.bookshop_id;

-- PROPERTY
INSERT INTO workitem_property_rel (parent_id, parent_uid, parent_cid, child_id, child_uid)
SELECT workitem_id, cont.uid, cont.cid, child.id, child.uid FROM workitem_metadata_rel as link 
JOIN workitems as cont ON cont.id=link.workitem_id
JOIN workitem_metadata as child ON child.field_name=link.field_name;

INSERT INTO cadlib_property_rel (parent_id, parent_uid, parent_cid, child_id, child_uid)
SELECT cadlib_id, cont.uid, cont.cid, child.id, child.uid FROM cadlib_metadata_rel as link 
JOIN cadlibs as cont ON cont.id=link.cadlib_id
JOIN cadlib_metadata as child ON child.field_name=link.field_name;

INSERT INTO bookshop_property_rel (parent_id, parent_uid, parent_cid, child_id, child_uid)
SELECT bookshop_id, cont.uid, cont.cid, child.id, child.uid FROM bookshop_metadata_rel as link 
JOIN bookshops as cont ON cont.id=link.bookshop_id
JOIN bookshop_metadata as child ON child.field_name=link.field_name;


DROP TABLE workitem_category_rel_seq;
DROP TABLE cadlib_category_rel_rb06;
DROP TABLE bookshop_category_rel_rb06;
DROP TABLE workitem_doctype_process;
DROP TABLE cadlib_doctype_process;
DROP TABLE bookshop_doctype_process;
DROP TABLE workitem_metadata_rel;
DROP TABLE cadlib_metadata_rel;
DROP TABLE bookshop_metadata_rel;

DROP TABLE workitem_category_rel_seq;
DROP TABLE cadlib_category_rel_seq;
DROP TABLE bookshop_category_rel_seq;
DROP TABLE workitem_doctype_process_seq;
DROP TABLE cadlib_doctype_process_seq;
DROP TABLE bookshop_doctype_process_seq;
DROP TABLE workitem_metadata_rel_seq;
DROP TABLE cadlib_metadata_rel_seq;
DROP TABLE bookshop_metadata_rel_seq;

DROP TABLE project_category_rel_seq;
DROP TABLE project_doctype_process;
DROP TABLE project_doctype_process_seq;
DROP TABLE project_partner_rel;
DROP TABLE project_bookshop_rel;
DROP TABLE project_cadlib_rel;
DROP TABLE project_mockup_rel;

