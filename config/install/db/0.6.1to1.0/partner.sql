
-- ################################################################
-- PARTNER
-- ################################################################
ALTER TABLE `partners` 
	CHANGE COLUMN `partner_id` `id` INT(11) NOT NULL,
	CHANGE COLUMN `partner_number` `number` VARCHAR(128) NOT NULL DEFAULT '',
	CHANGE COLUMN `partner_type` `type` enum('customer','supplier','staff') DEFAULT NULL,
	ADD INDEX `IK_partner_type` (`type`);
