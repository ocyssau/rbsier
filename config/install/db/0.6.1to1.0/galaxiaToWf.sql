-- ************************************
-- --- GALAXIA -> WF ------
-- ************************************

-- ************************************
CREATE TABLE `wf_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO `wf_seq` (`id`) VALUES (100);

CREATE TABLE `wf_instance_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO `wf_instance_seq` (`id`) VALUES (100);

-- ************************************

UPDATE `galaxia_processes` SET 
`isValid`=(
    case when isValid='y'
            then 1 
            else 0 
       end),
`isActive`=(
    case when isActive='y'
            then 1 
            else 0 
       end);

ALTER TABLE `galaxia_processes` 
	CHANGE COLUMN `pId` `id` INT(11) NOT NULL,
	CHANGE COLUMN `description` `title` VARCHAR(256),
	CHANGE COLUMN `normalized_name` `normalizedName` varchar(256),
	CHANGE COLUMN `isValid` `isValid` boolean DEFAULT 0,
	CHANGE COLUMN `isActive` `isActive` boolean DEFAULT 0,
	ADD COLUMN `uid` VARCHAR(255) NOT NULL AFTER id,
	ADD COLUMN `cid` VARCHAR(64) NOT NULL DEFAULT '56acc299ecd6d' AFTER uid,
	ADD COLUMN `updated` datetime NULL AFTER `lastModif`,
	ADD COLUMN `updateById` varchar(255) NULL AFTER updated,
	ADD COLUMN `ownerId` varchar(255) NULL AFTER updateById,
	ADD COLUMN `parentId` int NULL,
	ADD COLUMN `parentUid` varchar(255) NULL,
	ADD UNIQUE `wf_process_u_normalizedName` (normalizedName),
	ADD INDEX `wf_process_index_uid` (`uid` ASC),
	ADD INDEX `wf_process_index_cid` (`cid` ASC),
	ADD INDEX `wf_process_index_name` (`name` ASC),
	ADD INDEX `wf_process_index_parentuid` (`parentUid` ASC),
	ADD INDEX `wf_process_index_parentid` (`parentId` ASC),
	ADD INDEX `wf_process_index_ownerId` (`ownerId` ASC),
	ADD INDEX `wf_process_index_isValid` (`isValid` ASC),
	ADD INDEX `wf_process_index_isActive` (`isActive` ASC);

UPDATE `galaxia_processes` SET 
	`updated`=from_unixtime(`lastModif`, '%Y-%m-%d %h:%i:%s'),
	uid=SUBSTR(UUID(),1,8);

ALTER TABLE `galaxia_processes` 
	DROP COLUMN `lastModif`,
	ADD UNIQUE `wf_process_u_uid` (uid);

ALTER TABLE `galaxia_processes` RENAME TO `wf_process`;

-- ************************************
ALTER TABLE `galaxia_instances`
	CHANGE COLUMN `instanceId` `id` INT(11) NOT NULL,
	CHANGE COLUMN `pId` `processId` int NULL,
	CHANGE COLUMN `name` `name` VARCHAR(255) DEFAULT 'No Name',
	CHANGE COLUMN `owner` `ownerId` varchar(255) NULL,
	CHANGE COLUMN `status` `status` VARCHAR(64) NULL,
	CHANGE COLUMN `nextActivity` `nextActivities` TEXT NULL,
	CHANGE COLUMN `nextUser` `nextUsers` TEXT NULL,
	CHANGE COLUMN `properties` `attributes` TEXT NULL,
	CHANGE COLUMN `started` `startedold` int(11) NULL,
	CHANGE COLUMN `ended` `endedold` int(11) NULL,
	ADD COLUMN `started` datetime NULL AFTER `startedold`,
	ADD COLUMN `ended` datetime NULL AFTER `endedold`,
	ADD COLUMN `uid` VARCHAR(255) NOT NULL AFTER id,
	ADD COLUMN `cid` VARCHAR(64) NOT NULL DEFAULT '56acc299ed1bd' AFTER uid,
	ADD COLUMN `updated` datetime NULL AFTER cid,
	ADD COLUMN `updateById` varchar(255) NULL AFTER updated,
	ADD COLUMN `title` varchar(255) NULL AFTER name,
	ADD COLUMN `parentId` int NULL,
	ADD COLUMN `parentUid` varchar(255) NULL,
	ADD INDEX `WFINST_status` (`status` ASC),
	ADD INDEX `WFINST_processId` (`processId` DESC),
	ADD INDEX `WFINST_uid` (`uid` ASC),
	ADD INDEX `WFINST_cid` (`cid` ASC),
	ADD INDEX `WFINST_parentId` (`parentId` DESC),
	ADD INDEX `WFINST_parentUid` (`parentId` ASC);

UPDATE `galaxia_instances` SET 
	`started`=from_unixtime(`startedold`, '%Y-%m-%d %h:%i:%s'),
	`ended`=from_unixtime(`endedold`, '%Y-%m-%d %h:%i:%s'),
	uid=SUBSTR(UUID(),1,8);

ALTER TABLE `galaxia_instances` 
	DROP COLUMN `startedold`,
	DROP COLUMN `endedold`,
	ADD UNIQUE `wf_instance_u_uid` (uid);
	
UPDATE galaxia_instances SET 
	status='running' WHERE status='active';

ALTER TABLE `galaxia_instances` RENAME TO `wf_instance`;

-- ************************************
ALTER TABLE `galaxia_transitions` 
	CHANGE `actFromId` `parentId` INT( 14 ) NOT NULL,
	CHANGE `actToId` `childId` INT( 14 ) NOT NULL,
	CHANGE `pId` `processId` INT( 14 ) NOT NULL,
	ADD `name` VARCHAR( 30 ) NULL,
	ADD `uid` VARCHAR( 32 ) NOT NULL FIRST,
	ADD `parentUid` VARCHAR( 32 ) NOT NULL AFTER `parentId`,
	ADD `childUid` VARCHAR( 32 ) NOT NULL AFTER `childId`,
	ADD `lindex` INT(7) DEFAULT 0,
	ADD `attributes` TEXT NULL,
	ADD COLUMN `cid` varchar(32) NOT NULL DEFAULT '56acc299ed150',
	ADD INDEX ( `processId` ),
	ADD INDEX ( `parentId` ),
	ADD INDEX ( `parentUid` ),
	ADD INDEX ( `childId` ),
	ADD INDEX ( `childUid` ),
	ADD INDEX ( `lindex` );

UPDATE `galaxia_transitions` SET 
	uid=SUBSTR(UUID(),1,8);

ALTER TABLE `galaxia_transitions` 
	ADD UNIQUE `wf_transition_u_uid` (uid);

ALTER TABLE `galaxia_transitions` RENAME TO `wf_transition`;

-- ************************************


UPDATE `galaxia_activities` SET 
	`isInteractive`=(
    case when isInteractive='y'
            then 1 
            else 0 
       end),
	`isAutoRouted`=(
    case when isAutoRouted='y'
            then 1 
            else 0 
       end),
	`isAutomatic`=(
    case when isAutomatic='y'
            then 1 
            else 0 
       end),
	`isComment`=(
    case when isComment='y'
            then 1 
            else 0 
       end);

ALTER TABLE `galaxia_activities`
	CHANGE COLUMN `activityId` `id` INT(11) NOT NULL,
	CHANGE COLUMN `normalized_name` `normalizedName` VARCHAR(255),
	CHANGE COLUMN `description` `title` VARCHAR(255),
	CHANGE COLUMN `expirationTime` `expirationTimeOld` INT(11) NULL,
	CHANGE COLUMN `pId` `processId` integer NULL,
	CHANGE COLUMN `isInteractive` `isInteractive` boolean default 0,
	CHANGE COLUMN `isAutoRouted` `isAutorouted` boolean default 0,
	CHANGE COLUMN `isAutomatic` `isAutomatic` boolean default 0,
	CHANGE COLUMN `isComment` `isComment` boolean default 0,
	ADD COLUMN `expirationTime` DATETIME NULL AFTER `expirationTimeOld`,
	ADD `uid` VARCHAR( 32 ) NULL AFTER `id`,
	ADD `cid` VARCHAR( 32 ) NOT NULL DEFAULT '56acc299ed0e4' AFTER `uid`,
	ADD `ownerId` varchar(255) NULL,
	ADD `parentId` int NULL,
	ADD `parentUid` varchar(255) NULL,
	ADD `updateById` varchar(255) NULL,
	ADD `updated` datetime NULL,
	ADD `progression` float NULL,
	ADD `roles` text NULL,
	ADD `attributes` TEXT NULL,
	ADD INDEX (roles(255)),
	ADD INDEX ( `ownerId` ),
	ADD INDEX ( `parentId` ),
	ADD INDEX ( `parentUid` );

UPDATE `galaxia_activities` SET 
	`expirationTime`=from_unixtime(`expirationTimeOld`, '%Y-%m-%d %h:%i:%s'),
	uid=SUBSTR(UUID(),1,8);

ALTER TABLE `galaxia_activities` 
	DROP COLUMN `expirationTimeOld`,
	ADD UNIQUE `wf_activity_u_uid` (uid);

ALTER TABLE `galaxia_activities` RENAME TO `wf_activity`;

-- ************************************
ALTER TABLE `galaxia_instance_activities` 
	DROP PRIMARY KEY,
	ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
	CHANGE `activityId` `activityId` INT( 14 ) NOT NULL,
	CHANGE `instanceId` `instanceId` INT( 14 ) NOT NULL,
	CHANGE `started` `intstarted` INT( 11 ) NULL DEFAULT NULL,
	CHANGE `ended` `intended` INT( 11 ) NULL DEFAULT NULL,
	ADD `started` DATETIME,
	ADD `ended` DATETIME,
	ADD COLUMN `uid` varchar(64)  NOT NULL AFTER `id`,
	ADD COLUMN `cid` varchar(64) NOT NULL DEFAULT '56acc299ed22a' AFTER `uid`,
	ADD COLUMN `name` varchar(128) DEFAULT NULL AFTER `cid`,
	ADD COLUMN `title` varchar(256) DEFAULT NULL AFTER `name`,
	CHANGE COLUMN `user` `ownerId` varchar(64) DEFAULT NULL,
	ADD COLUMN `updated` datetime DEFAULT NULL AFTER `status`,
	ADD COLUMN `updateById` varchar(64) DEFAULT NULL AFTER `updated`,
	ADD COLUMN `parentId` int(11) DEFAULT NULL AFTER `updateById`,
	ADD COLUMN `parentUid` varchar(128) DEFAULT NULL AFTER `parentId`,
	ADD COLUMN `type` ENUM('start','end','split','switch','join','activity','standalone') DEFAULT NULL AFTER `status`,
	ADD COLUMN `attributes` TEXT NULL,
	ADD COLUMN `comment` TEXT NULL;

UPDATE galaxia_instance_activities SET 
	intstarted=NULL WHERE intstarted=0;
UPDATE galaxia_instance_activities SET 
	intended=NULL WHERE intended=0;
UPDATE galaxia_instance_activities SET 
	status='running' WHERE status='active';

UPDATE galaxia_instance_activities SET 
	`started`=from_unixtime(`intstarted`, '%Y-%m-%d %h:%i:%s'),
	`ended`=from_unixtime(`intended`, '%Y-%m-%d %h:%i:%s')
	WHERE intended IS NOT NULL;
ALTER TABLE galaxia_instance_activities 
	DROP COLUMN `intstarted`,
	DROP COLUMN `intended`;

UPDATE `galaxia_instance_activities` SET 
	uid=SUBSTR(UUID(),1,8);

ALTER TABLE `galaxia_instance_activities` RENAME TO `wf_instance_activity`;

ALTER TABLE `wf_instance_activity` 
	ADD UNIQUE (uid),
	ADD INDEX `WFINSTACTIVITY_uid` (`uid` ASC),
	ADD INDEX `WFINSTACTIVITY_name` (`name` ASC),
	ADD INDEX `WFINSTACTIVITY_parentuid` (`parentUid` ASC),
	ADD INDEX `WFINSTACTIVITY_parentid` (`parentId` ASC),
	ADD INDEX `WFINSTACTIVITY_cid` (`cid` ASC),
	ADD INDEX `WFINSTACTIVITY_ownerId` (`ownerId` ASC),
	ADD INDEX `WFINSTACTIVITY_instanceId` (`instanceId` ASC),
	ADD INDEX `WFINSTACTIVITY_activityId` (`activityId` ASC),
	ADD INDEX `WFINSTACTIVITY_status` (`status` ASC),
	ADD INDEX `WFINSTACTIVITY_type` (`type` ASC),
	ADD INDEX `WFINSTACTIVITY_started` (`started` ASC),
	ADD INDEX `WFINSTACTIVITY_ended` (`ended` ASC);


-- -- DOCUMENT INSTANCE PROCESS LINK IN LINK TABLE ------
DROP TABLE IF EXISTS wf_document_link;
CREATE TABLE wf_document_link(
	`uid` VARCHAR(64) NOT NULL,
	`parentId` INTEGER NOT NULL,
	`childId` INTEGER NOT NULL,
	`parentUid` VARCHAR(255) NULL,
	`childUid` VARCHAR(255) NULL,
	`name` VARCHAR(255) NULL,
	`lindex` INTEGER DEFAULT 0,
	`attributes` TEXT NULL,
	`spacename` VARCHAR(64) NOT NULL,
	PRIMARY KEY (`parentId`, `childId`),
	UNIQUE (`uid`),
	INDEX `DOCPROCESS_UID` (`uid` ASC),
	INDEX `DOCPROCESS_NAME` (`name` ASC),
	INDEX `DOCPROCESS_LINDEX` (`lindex` ASC),
	INDEX `DOCPROCESS_PARENTUID` (`parentUid` ASC),
	INDEX `DOCPROCESS_CHILDUID` (`childUid` ASC),
	INDEX `DOCPROCESS_PARENTID` (`parentId` ASC),
	INDEX `DOCPROCESS_CHILDID` (`childId` ASC)
);

INSERT INTO `wf_document_link` (`uid`, `parentId`,`parentUid`, `childId`,`childUid`, `spacename`)
	SELECT substr(uuid(),1,8), doc.id, doc.uid, inst.id, inst.uid, 'workitem'
	FROM workitem_documents AS doc
	JOIN wf_instance AS inst ON inst.id = doc.instance_id;

-- ************************************
ALTER TABLE `workitem_documents` DROP COLUMN `instance_id`;
ALTER TABLE `bookshop_documents` DROP COLUMN `instance_id`;
ALTER TABLE `mockup_documents` DROP COLUMN `instance_id`;
ALTER TABLE `cadlib_documents` DROP COLUMN `instance_id`;

-- ************************************
-- cleanup orphelan instanceActivity before
-- ************************************
ALTER TABLE `wf_instance_activity` 
	ADD CONSTRAINT `WF_INSTANCEPROCESS` FOREIGN KEY ( `instanceId` )
	REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
	
-- ************************************
UPDATE wf_instance_seq SET id=(SELECT MAX(id) FROM wf_instance_activity) LIMIT 100;
