DROP TABLE IF EXISTS checkout_index;
CREATE TABLE `checkout_index` (
	`file_name` varchar(128) NOT NULL DEFAULT '',
	`file_id` int(11) NOT NULL,
	`file_uid` VARCHAR(140) NOT NULL,
	`designation` varchar(128) DEFAULT NULL,
	`spacename` varchar(128) NOT NULL DEFAULT '',
	`container_id` int(11) NOT NULL DEFAULT '0',
	`container_number` varchar(128) NOT NULL DEFAULT '',
	`check_out_by` int(11) NOT NULL DEFAULT '0',
	`document_id` int(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`file_name`),
	KEY `IK_checkout_index_1` (`document_id`),
	KEY `IK_checkout_index_2` (`file_id`),
	KEY `IK_checkout_index_3` (`file_uid`),
	KEY `IK_checkout_index_4` (`spacename`),
	KEY `IK_checkout_index_5` (`container_id`),
	KEY `IK_checkout_index_6` (`container_number`),
	KEY `IK_checkout_index_7` (`file_uid`),
	KEY `IK_checkout_index_8` (`file_id`)
) ENGINE=InnoDB;
