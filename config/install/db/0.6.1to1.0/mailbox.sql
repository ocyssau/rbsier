-- ************************
DROP TABLE IF EXISTS `message_seq`;
CREATE TABLE `message_seq`(
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO message_seq(id) VALUES(10);

-- ************************
DROP TABLE IF EXISTS `message_archive`;
CREATE TABLE IF NOT EXISTS `message_archive` (
`id` int(14) NOT NULL,
`ownerUid` varchar(128) NOT NULL,
`created` DATETIME DEFAULT NULL,
`from` varchar(200) NOT NULL,
`to` varchar(255),
`cc` varchar(255),
`bcc` varchar(255),
`subject` varchar(255)  DEFAULT NULL,
`body` text,
`hash` varchar(32)  DEFAULT NULL,
`replyto_hash` varchar(32)  DEFAULT NULL,
`isRead` tinyint(1)  DEFAULT 0,
`isReplied` tinyint(1)  DEFAULT 0,
`isFlagged` tinyint(1)  DEFAULT 0,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `ownerUid` ),
INDEX ( `isRead` ),
INDEX ( `isReplied` ),
INDEX ( `isFlagged` ),
INDEX ( `subject` ),
FULLTEXT ( `body`,`subject` )
) ENGINE=InnoDB;

-- ************************
DROP TABLE IF EXISTS `message_mailbox`;
CREATE TABLE IF NOT EXISTS `message_mailbox` (
`id` int(14) NOT NULL,
`ownerUid` varchar(128) NOT NULL,
`created` DATETIME DEFAULT NULL,
`from` varchar(200) NOT NULL,
`to` varchar(255),
`cc` varchar(255),
`bcc` varchar(255),
`subject` varchar(255)  DEFAULT NULL,
`body` text,
`hash` varchar(32)  DEFAULT NULL,
`replyto_hash` varchar(32)  DEFAULT NULL,
`isRead` tinyint(1)  DEFAULT 0,
`isReplied` tinyint(1)  DEFAULT 0,
`isFlagged` tinyint(1)  DEFAULT 0,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `ownerUid` ),
INDEX ( `isRead` ),
INDEX ( `isReplied` ),
INDEX ( `isFlagged` ),
INDEX ( `subject` ),
FULLTEXT ( `body`,`subject` )
) ENGINE=InnoDB;

-- ************************
DROP TABLE IF EXISTS `message_sent`;
CREATE TABLE IF NOT EXISTS `message_sent` (
	`id` int(14) NOT NULL,
	`ownerUid` varchar(128) NOT NULL,
	`created` DATETIME DEFAULT NULL,
	`from` varchar(200) NOT NULL,
	`to` varchar(255),
	`cc` varchar(255),
	`bcc` varchar(255),
	`subject` varchar(255)  DEFAULT NULL,
	`body` text,
	`hash` varchar(32)  DEFAULT NULL,
	`replyto_hash` varchar(32)  DEFAULT NULL,
	`isRead` tinyint(1)  DEFAULT 0,
	`isReplied` tinyint(1)  DEFAULT 0,
	`isFlagged` tinyint(1)  DEFAULT 0,
	`priority` int(2) DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX ( `ownerUid` ),
	INDEX ( `isRead` ),
	INDEX ( `isReplied` ),
	INDEX ( `isFlagged` ),
	INDEX ( `subject` ),
	FULLTEXT ( `body`,`subject` )
) ENGINE=InnoDB;
