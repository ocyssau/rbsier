DROP TRIGGER IF EXISTS onWorkitemCategoryUpdate; 
delimiter $$
CREATE TRIGGER onWorkitemCategoryUpdate AFTER UPDATE ON workitem_categories FOR EACH ROW 
BEGIN
	IF NEW.id != OLD.id THEN
		UPDATE workitem_documents SET category_id=NEW.id WHERE category_id=OLD.id;
		UPDATE workitem_category_rel SET child_id=NEW.id WHERE child_id=OLD.id;
	END IF;
END;$$


DROP TRIGGER IF EXISTS onCadlibCategoryUpdate; 
delimiter $$
CREATE TRIGGER onCadlibCategoryUpdate AFTER UPDATE ON cadlib_categories FOR EACH ROW 
BEGIN
	IF NEW.id != OLD.id THEN
		UPDATE cadlib_documents SET category_id=NEW.id WHERE category_id=OLD.id;
		UPDATE cadlib_category_rel SET child_id=NEW.id WHERE child_id=OLD.id;
	END IF;
END;$$

DROP TRIGGER IF EXISTS onBookshopCategoryUpdate; 
delimiter $$
CREATE TRIGGER onBookshopCategoryUpdate AFTER UPDATE ON bookshop_categories FOR EACH ROW 
BEGIN
	IF NEW.id != OLD.id THEN
		UPDATE bookshop_documents SET category_id=NEW.id WHERE category_id=OLD.id;
		UPDATE bookshop_category_rel SET child_id=NEW.id WHERE child_id=OLD.id;
	END IF;
END;$$

DROP TRIGGER IF EXISTS onMockupCategoryUpdate; 
delimiter $$
CREATE TRIGGER onMockupCategoryUpdate AFTER UPDATE ON mockup_categories FOR EACH ROW 
BEGIN
	IF NEW.id != OLD.id THEN
		UPDATE mockup_documents SET category_id=NEW.id WHERE category_id=OLD.id;
		UPDATE mockup_category_rel SET child_id=NEW.id WHERE child_id=OLD.id;
	END IF;
END;$$


-- ATTENTION -- ATTENTION -- ATTENTION -- ATTENTION
-- Triggers must be created before --
-- ################################################

-- Set org_seq and renum projects and containers
CREATE TABLE IF NOT EXISTS `categories_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100;
INSERT INTO `categories_seq` (id) VALUES(100);


SET @position := (SELECT id from categories_seq);
ALTER TABLE workitem_category_rel DROP FOREIGN KEY FK_workitem_category_rel_3;
UPDATE workitem_categories SET `id`=(@position := @position + 1);
UPDATE categories_seq SET id=@position LIMIT 1;
ALTER TABLE `workitem_category_rel` 
	ADD CONSTRAINT `FK_workitem_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `workitem_categories` (`id`) ON DELETE CASCADE;

-- SELECT * FROM workitem_categories;
-- SELECT * FROM categories_seq;

SET @position := (SELECT id from categories_seq);
ALTER TABLE cadlib_category_rel DROP FOREIGN KEY FK_cadlib_category_rel_3;
UPDATE cadlib_categories SET `id`=(@position := @position + 1);
UPDATE categories_seq SET id=@position LIMIT 1;
ALTER TABLE `cadlib_category_rel` 
	ADD CONSTRAINT `FK_cadlib_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `cadlib_categories` (`id`) ON DELETE CASCADE;

-- SELECT * FROM cadlib_categories;

SET @position := (SELECT id from categories_seq);
ALTER TABLE bookshop_category_rel DROP FOREIGN KEY FK_bookshop_category_rel_3;
UPDATE bookshop_categories SET `id`=(@position := @position + 1);
UPDATE categories_seq SET id=@position LIMIT 1;
ALTER TABLE `bookshop_category_rel` 
	ADD CONSTRAINT `FK_bookshop_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `bookshop_categories` (`id`) ON DELETE CASCADE;

SET @position := (SELECT id from categories_seq);
ALTER TABLE mockup_category_rel DROP FOREIGN KEY FK_mockup_category_rel_3;
UPDATE mockup_categories SET `id`=(@position := @position + 1);
UPDATE categories_seq SET id=@position LIMIT 1;
ALTER TABLE `mockup_category_rel` 
	ADD CONSTRAINT `FK_mockup_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `mockup_categories` (`id`) ON DELETE CASCADE;


DROP TABLE workitem_categories_seq;
DROP TABLE cadlib_categories_seq;
DROP TABLE mockup_categories_seq;
DROP TABLE bookshop_categories_seq;

DROP TRIGGER IF EXISTS onWorkitemCategoryUpdate; 
DROP TRIGGER IF EXISTS onCadlibCategoryUpdate; 
DROP TRIGGER IF EXISTS onBookshopCategoryUpdate; 
DROP TRIGGER IF EXISTS onMockupCategoryUpdate; 

