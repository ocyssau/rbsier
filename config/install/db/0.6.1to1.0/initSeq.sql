DROP TABLE acl_seq;
DROP TABLE bookshop_alias_seq;
DROP TABLE bookshop_categories_seq;
DROP TABLE bookshop_category_rel_seq;
DROP TABLE bookshop_cont_metadata_seq;
DROP TABLE bookshop_doc_files_seq;
DROP TABLE bookshop_doc_files_versions_seq;
DROP TABLE bookshop_doc_rel_seq;
DROP TABLE bookshop_doccomments_seq;
DROP TABLE bookshop_doctype_process_seq;
DROP TABLE bookshop_documents_history_seq;
DROP TABLE bookshop_documents_seq;
DROP TABLE bookshop_history_seq;
DROP TABLE bookshop_metadata_rel_seq;
DROP TABLE bookshop_metadata_seq;
DROP TABLE bookshops_seq;
DROP TABLE cadlib_alias_seq;
DROP TABLE cadlib_categories_seq;
DROP TABLE cadlib_category_rel_seq;
DROP TABLE cadlib_cont_metadata_seq;
DROP TABLE cadlib_doc_files_seq;
DROP TABLE cadlib_doc_files_versions_seq;
DROP TABLE cadlib_doc_rel_seq;
DROP TABLE cadlib_doccomments_seq;
DROP TABLE cadlib_doctype_process_seq;
DROP TABLE cadlib_documents_history_seq;
DROP TABLE cadlib_documents_seq;
DROP TABLE cadlib_history_seq;
DROP TABLE cadlib_metadata_rel_seq;
DROP TABLE cadlib_metadata_seq;
DROP TABLE cadlibs_seq;
DROP TABLE message_seq;
DROP TABLE mockup_alias_seq;
DROP TABLE mockup_categories_seq;
DROP TABLE mockup_category_rel_seq;
DROP TABLE mockup_cont_metadata_seq;
DROP TABLE mockup_doc_files_seq;
DROP TABLE mockup_doc_files_versions_seq;
DROP TABLE mockup_doc_rel_seq;
DROP TABLE mockup_doccomments_seq;
DROP TABLE mockup_doctype_process_seq;
DROP TABLE mockup_documents_history_seq;
DROP TABLE mockup_documents_seq;
DROP TABLE mockup_history_seq;
DROP TABLE mockup_metadata_rel_seq;
DROP TABLE mockup_metadata_seq;
DROP TABLE mockups_seq;
DROP TABLE partners_seq;
DROP TABLE pdm_seq;
DROP TABLE postit_seq;
DROP TABLE project_doctype_process_seq;
DROP TABLE project_history_seq;
DROP TABLE projects_seq;
DROP TABLE rt_aif_code_a350_seq;
DROP TABLE wf_seq;
DROP TABLE workitem_alias_seq;
DROP TABLE workitem_categories_seq;
DROP TABLE workitem_category_rel_seq;
DROP TABLE workitem_cont_metadata_seq;
DROP TABLE workitem_doc_files_seq;
DROP TABLE workitem_doc_files_versions_seq;
DROP TABLE workitem_doc_rel_seq;
DROP TABLE workitem_doccomments_seq;
DROP TABLE workitem_doctype_process_seq;
DROP TABLE workitem_documents_history_seq;
DROP TABLE workitem_documents_seq;
DROP TABLE workitem_history_seq;
DROP TABLE workitem_metadata_rel_seq;
DROP TABLE workitem_metadata_seq;
DROP TABLE workitems_seq;

CREATE TABLE acl_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=12;
CREATE TABLE bookshop_alias_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE bookshop_categories_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=9;
CREATE TABLE bookshop_category_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=4;
CREATE TABLE bookshop_cont_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE bookshop_doc_files_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=7810;
CREATE TABLE bookshop_doc_files_versions_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=584;
CREATE TABLE bookshop_doc_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=2;
CREATE TABLE bookshop_doccomments_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=11;
CREATE TABLE bookshop_doctype_process_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=119;
CREATE TABLE bookshop_documents_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=14077;
CREATE TABLE bookshop_documents_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=11763;
CREATE TABLE bookshop_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=23;
CREATE TABLE bookshop_metadata_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=47;
CREATE TABLE bookshop_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=5;
CREATE TABLE bookshops_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=16;
CREATE TABLE cadlib_alias_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE cadlib_categories_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE cadlib_category_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE cadlib_cont_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE cadlib_doc_files_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=83;
CREATE TABLE cadlib_doc_files_versions_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=1;
CREATE TABLE cadlib_doc_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE cadlib_doccomments_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE cadlib_doctype_process_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=9;
CREATE TABLE cadlib_documents_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=99;
CREATE TABLE cadlib_documents_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=83;
CREATE TABLE cadlib_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=4;
CREATE TABLE cadlib_metadata_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=9;
CREATE TABLE cadlib_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=9;
CREATE TABLE cadlibs_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=4;
CREATE TABLE message_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=11;
CREATE TABLE mockup_alias_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_categories_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_category_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_cont_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=2;
CREATE TABLE mockup_doc_files_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_doc_files_versions_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_doc_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_doccomments_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_doctype_process_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_documents_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_documents_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=50;
CREATE TABLE mockup_metadata_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockup_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=199;
CREATE TABLE mockups_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=27;
CREATE TABLE partners_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=1;
CREATE TABLE pdm_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=11;
CREATE TABLE postit_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=11;
CREATE TABLE project_doctype_process_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=141;
CREATE TABLE project_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=37;
CREATE TABLE projects_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=24;
CREATE TABLE rt_aif_code_a350_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=1;
CREATE TABLE wf_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=101;
CREATE TABLE workitem_alias_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=59;
CREATE TABLE workitem_categories_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=23;
CREATE TABLE workitem_category_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=119;
CREATE TABLE workitem_cont_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=2;
CREATE TABLE workitem_doc_files_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=217580;
CREATE TABLE workitem_doc_files_versions_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=224528;
CREATE TABLE workitem_doc_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=14533;
CREATE TABLE workitem_doccomments_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=2259;
CREATE TABLE workitem_doctype_process_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=167;
CREATE TABLE workitem_documents_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=1006164;
CREATE TABLE workitem_documents_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=125148;
CREATE TABLE workitem_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=479;
CREATE TABLE workitem_metadata_rel_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=98;
CREATE TABLE workitem_metadata_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=17;
CREATE TABLE workitems_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=274;


INSERT INTO acl_seq SET id=12;
INSERT INTO bookshop_alias_seq SET id=199;
INSERT INTO bookshop_categories_seq SET id=9;
INSERT INTO bookshop_category_rel_seq SET id=4;
INSERT INTO bookshop_cont_metadata_seq SET id=199;
INSERT INTO bookshop_doc_files_seq SET id=7810;
INSERT INTO bookshop_doc_files_versions_seq SET id=584;
INSERT INTO bookshop_doc_rel_seq SET id=2;
INSERT INTO bookshop_doccomments_seq SET id=11;
INSERT INTO bookshop_doctype_process_seq SET id=119;
INSERT INTO bookshop_documents_history_seq SET id=14077;
INSERT INTO bookshop_documents_seq SET id=11763;
INSERT INTO bookshop_history_seq SET id=23;
INSERT INTO bookshop_metadata_rel_seq SET id=47;
INSERT INTO bookshop_metadata_seq SET id=5;
INSERT INTO bookshops_seq SET id=16;
INSERT INTO cadlib_alias_seq SET id=199;
INSERT INTO cadlib_categories_seq SET id=199;
INSERT INTO cadlib_category_rel_seq SET id=199;
INSERT INTO cadlib_cont_metadata_seq SET id=199;
INSERT INTO cadlib_doc_files_seq SET id=83;
INSERT INTO cadlib_doc_files_versions_seq SET id=1;
INSERT INTO cadlib_doc_rel_seq SET id=199;
INSERT INTO cadlib_doccomments_seq SET id=199;
INSERT INTO cadlib_doctype_process_seq SET id=9;
INSERT INTO cadlib_documents_history_seq SET id=99;
INSERT INTO cadlib_documents_seq SET id=83;
INSERT INTO cadlib_history_seq SET id=4;
INSERT INTO cadlib_metadata_rel_seq SET id=9;
INSERT INTO cadlib_metadata_seq SET id=9;
INSERT INTO cadlibs_seq SET id=4;
INSERT INTO message_seq SET id=11;
INSERT INTO mockup_alias_seq SET id=199;
INSERT INTO mockup_categories_seq SET id=199;
INSERT INTO mockup_category_rel_seq SET id=199;
INSERT INTO mockup_cont_metadata_seq SET id=2;
INSERT INTO mockup_doc_files_seq SET id=199;
INSERT INTO mockup_doc_files_versions_seq SET id=199;
INSERT INTO mockup_doc_rel_seq SET id=199;
INSERT INTO mockup_doccomments_seq SET id=199;
INSERT INTO mockup_doctype_process_seq SET id=199;
INSERT INTO mockup_documents_history_seq SET id=199;
INSERT INTO mockup_documents_seq SET id=199;
INSERT INTO mockup_history_seq SET id=50;
INSERT INTO mockup_metadata_rel_seq SET id=199;
INSERT INTO mockup_metadata_seq SET id=199;
INSERT INTO mockups_seq SET id=27;
INSERT INTO partners_seq SET id=1;
INSERT INTO pdm_seq SET id=11;
INSERT INTO postit_seq SET id=11;
INSERT INTO project_doctype_process_seq SET id=141;
INSERT INTO project_history_seq SET id=37;
INSERT INTO projects_seq SET id=24;
INSERT INTO rt_aif_code_a350_seq SET id=1;
INSERT INTO wf_seq SET id=101;
INSERT INTO workitem_alias_seq SET id=59;
INSERT INTO workitem_categories_seq SET id=23;
INSERT INTO workitem_category_rel_seq SET id=119;
INSERT INTO workitem_cont_metadata_seq SET id=2;
INSERT INTO workitem_doc_files_seq SET id=217580;
INSERT INTO workitem_doc_files_versions_seq SET id=224528;
INSERT INTO workitem_doc_rel_seq SET id=14533;
INSERT INTO workitem_doccomments_seq SET id=2259;
INSERT INTO workitem_doctype_process_seq SET id=167;
INSERT INTO workitem_documents_history_seq SET id=1006164;
INSERT INTO workitem_documents_seq SET id=125148;
INSERT INTO workitem_history_seq SET id=479;
INSERT INTO workitem_metadata_rel_seq SET id=98;
INSERT INTO workitem_metadata_seq SET id=17;
INSERT INTO workitems_seq SET id=274;


-- ################## WITH NULL AUTO INCREMENT ################
DROP TABLE doctypes_seq;
DROP TABLE mockup_files_seq;
DROP TABLE mockup_import_history_seq;

CREATE TABLE doctypes_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_files_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;
CREATE TABLE mockup_import_history_seq (id int(11) NOT NULL AUTO_INCREMENT,PRIMARY KEY (`id`)) ENGINE=MyIsam AUTO_INCREMENT=100;

INSERT INTO doctypes_seq SET id=100;
INSERT INTO mockup_files_seq SET id=100;
INSERT INTO mockup_import_history_seq SET id=100;
