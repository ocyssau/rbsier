-- #################################################################
-- WORKITEM
-- #################################################################
-- ************************
ALTER TABLE `workitems` 
	ADD COLUMN `name` VARCHAR( 128 ) NOT NULL AFTER `number`,
	ADD COLUMN `uid` VARCHAR( 128 ) NULL AFTER `name`,
	ADD COLUMN `cid` VARCHAR(64) DEFAULT '569e94192201a' AFTER `uid`,
	ADD COLUMN `dn` VARCHAR(128) NULL AFTER `cid`,
	CHANGE COLUMN `workitem_id` `id` INT(11) NOT NULL,
	CHANGE COLUMN `workitem_number` `number` VARCHAR(64) NOT NULL DEFAULT '',
	CHANGE COLUMN `workitem_description` `designation` VARCHAR(128) NULL DEFAULT NULL ,
	CHANGE COLUMN `workitem_state` `life_stage` VARCHAR(16) DEFAULT 'init',
	CHANGE COLUMN `workitem_indice_id` `version` INT(11) NULL DEFAULT NULL,
	CHANGE COLUMN `access_code` `acode` int(11),
	CHANGE COLUMN `open_by` `create_by_id` int(11),
	CHANGE COLUMN `close_by` `close_by_id` int(11),
	CHANGE COLUMN `container_type` `spacename` VARCHAR(16) NOT NULL DEFAULT 'workitem',
	CHANGE COLUMN `project_id` `parent_id` INT(11) DEFAULT NULL,
	ADD COLUMN `parent_uid` VARCHAR(64) DEFAULT NULL AFTER `parent_id`,
	ADD COLUMN `created` DATETIME NULL DEFAULT NULL AFTER `open_date`,
	ADD COLUMN `closed` DATETIME NULL DEFAULT NULL AFTER `close_date`,
	ADD COLUMN `planned_closure` DATETIME NULL DEFAULT NULL AFTER `forseen_close_date`;
	
UPDATE `workitems` SET `name`=`number`;
UPDATE `workitems` SET `uid`=`number`;
UPDATE `workitems` SET `dn`=CONCAT('/',`uid`,'/');

ALTER TABLE `workitems` 
	ADD INDEX `INDEX_name` (`name`),
	ADD INDEX `INDEX_dn` (`dn`),
	ADD UNIQUE KEY `UC_uid` (`uid`);
UPDATE `workitems` SET 
	`created`=from_unixtime(`open_date`, '%Y-%m-%d %h:%i:%s'),
	`closed`=from_unixtime(`close_date`, '%Y-%m-%d %h:%i:%s'),
	`planned_closure`=from_unixtime(`forseen_close_date`, '%Y-%m-%d %h:%i:%s');
ALTER TABLE `workitems` 
	DROP COLUMN `open_date`,
	DROP COLUMN `close_date`,
	DROP COLUMN `forseen_close_date`;
	
-- ************************
ALTER TABLE `workitem_alias` 
	CHANGE COLUMN `workitem_id` `container_id` INT(11) NOT NULL,
	CHANGE COLUMN `workitem_number` `number` VARCHAR(64) NOT NULL DEFAULT '',
	CHANGE COLUMN `workitem_description` `designation` VARCHAR(128) NULL DEFAULT NULL,
	ADD `name` VARCHAR( 128 ) NOT NULL DEFAULT 'workitem' AFTER `number`,
	ADD `uid` VARCHAR( 128 ) NOT NULL DEFAULT 'workitem' AFTER `name`,
	ADD INDEX `INDEX_uid` ( `uid` ),
	ADD INDEX `INDEX_name` ( `name` );

-- ************************
ALTER TABLE `workitem_history` 
	CHANGE COLUMN `workitem_id` `container_id` INT(11) NOT NULL,
	CHANGE COLUMN `workitem_number` `number` VARCHAR(64) NOT NULL DEFAULT '',
	CHANGE COLUMN `workitem_description` `designation` VARCHAR(128) NULL DEFAULT NULL ,
	CHANGE COLUMN `workitem_state` `life_stage` VARCHAR(16) DEFAULT 'init',
	CHANGE COLUMN `workitem_indice_id` `indice_id` INT(11) NULL DEFAULT NULL,
	ADD `action_started` DATETIME NULL DEFAULT NULL AFTER `action_date`,
	ADD `created` DATETIME NULL DEFAULT NULL AFTER `open_date`,
	ADD `closed` DATETIME NULL DEFAULT NULL AFTER `close_date`,
	ADD `planned_closure` DATETIME NULL DEFAULT NULL AFTER `forseen_close_date`;
	
-- workitem_history --
UPDATE `workitem_history` SET 
	`action_started`=from_unixtime(`action_date`, '%Y-%m-%d %h:%i:%s'),
	`created`=from_unixtime(`open_date`, '%Y-%m-%d %h:%i:%s'),
	`closed`=from_unixtime(`close_date`, '%Y-%m-%d %h:%i:%s'),
	`planned_closure`=from_unixtime(`forseen_close_date`, '%Y-%m-%d %h:%i:%s');
ALTER TABLE `workitem_history`
	DROP COLUMN `action_date`,
	DROP COLUMN `open_date`,
	DROP COLUMN `close_date`,
	DROP COLUMN `forseen_close_date`;
	
-- ************************
ALTER TABLE `workitem_metadata` 
	ADD `id` int(11) FIRST,
	ADD `uid` VARCHAR(64) after `id`,
	DROP PRIMARY KEY,
	ADD `appname` VARCHAR(128) AFTER `adv_select`,
	ADD `getter` VARCHAR(128),
	ADD `setter` VARCHAR(128),
	ADD `label` VARCHAR(255),
	ADD `min` decimal,
	ADD `max` decimal,
	ADD `step` decimal,
	ADD `dbfilter` text,
	ADD `dbquery` text,
	ADD `attributes` text,
	ADD `extendedCid` VARCHAR(32) AFTER field_name,
	ADD UNIQUE KEY `UC_uid` (`uid`);
SET @position := 1;
UPDATE workitem_metadata SET `id`=(@position := @position + 1);
UPDATE workitem_metadata_seq SET id=(SELECT MAX(workitem_metadata.id) FROM workitem_metadata) LIMIT 100;
ALTER TABLE `workitem_metadata` 
	ADD PRIMARY KEY (`id`),
	ADD UNIQUE INDEX `fname-cid` (`field_name` ASC, `extendedCid` ASC);

UPDATE `workitem_metadata` SET 
	`appname`=`field_name`,
	`uid`=`field_name`,
	`label`=`field_description`,
	`extendedCid`='569e92709feb6';

SET @position := (SELECT id from workitem_metadata_seq);
INSERT INTO `workitem_metadata`
(`id`,
`field_name`,
`extendedCid`,
`field_description`,
`field_type`,
`field_regex`,
`field_required`,
`field_multiple`,
`field_size`,
`return_name`,
`field_list`,
`field_where`,
`is_hide`,
`table_name`,
`field_for_value`,
`field_for_display`,
`date_format`,
`adv_select`,
`appname`,
`label`)
(SELECT
	(@position := @position + 1),
	`field_name`,
	'569e94192201a',
	`field_description`,
	`field_type`,
	`field_regex`,
	`field_required`,
	`field_multiple`,
	`field_size`,
	`return_name`,
	`field_list`,
	`field_where`,
	`is_hide`,
	`table_name`,
	`field_for_value`,
	`field_for_display`,
	`date_format`,
	`adv_select`,
	`field_name`,
	`field_description`
	FROM `workitem_cont_metadata`);
UPDATE workitem_metadata_seq SET id=(SELECT MAX(workitem_metadata.id) FROM workitem_metadata) LIMIT 100;

-- ************************
ALTER TABLE `workitem_documents` 
	CHANGE COLUMN `workitem_id` `container_id` INT(11) NOT NULL,
	CHANGE COLUMN `document_id` `id` INT(11) NOT NULL,
	ADD COLUMN uid VARCHAR(140) DEFAULT NULL AFTER id,
	ADD COLUMN cid VARCHAR(64) DEFAULT '569e92709feb6' AFTER uid,
	CHANGE COLUMN `document_number` `number` VARCHAR(128),
	CHANGE COLUMN `document_name` `name` VARCHAR(255) DEFAULT NULL,
	CHANGE COLUMN `document_state` `life_stage` VARCHAR(32),
	CHANGE COLUMN `document_access_code` `acode` int(11),
	CHANGE COLUMN `document_version` `iteration` int(11),
	CHANGE COLUMN `document_indice_id` `version` int(5),
	CHANGE COLUMN `issued_from_document` `from_document_id` int(11),
	CHANGE COLUMN `designation` `designation` VARCHAR(128) AFTER name,
	CHANGE COLUMN `check_out_by` `lock_by_id` INT(11),
	CHANGE COLUMN `open_by` `create_by_id` int(11),
	CHANGE COLUMN `update_by` `update_by_id` int(11),
	ADD COLUMN `created` DATETIME NULL DEFAULT NULL AFTER `open_date`,
	ADD COLUMN `updated` DATETIME NULL DEFAULT NULL AFTER `update_date`,
	ADD COLUMN `locked` DATETIME NULL DEFAULT NULL AFTER `check_out_date`,
	ADD COLUMN `container_uid` VARCHAR(128) NULL DEFAULT NULL AFTER `container_id`,
	DROP COLUMN doc_space,
	ADD `spacename` VARCHAR( 30 ) NOT NULL DEFAULT 'workitem' AFTER `version`,
	ADD INDEX `INDEX_workitem_documents_spacename` ( `spacename` ),
	ADD INDEX `INDEX_workitem_documents_name` ( `name` ),
	ADD INDEX `INDEX_workitem_documents_cuid` ( `container_uid` );
UPDATE `workitem_documents` AS c SET `container_uid`=(select uid from workitems where id=c.container_id);
-- DOCUMENT DATES --
UPDATE `workitem_documents` SET 
	`created`=from_unixtime(`open_date`, '%Y-%m-%d %h:%i:%s'),
	`updated`=from_unixtime(`update_date`, '%Y-%m-%d %h:%i:%s'),
	`locked`=from_unixtime(`check_out_date`, '%Y-%m-%d %h:%i:%s');
ALTER TABLE `workitem_documents`
	DROP COLUMN `open_date`,
	DROP COLUMN `update_date`,
	DROP COLUMN `check_out_date`;
	
-- ADD UID TO DOCUMENTS TABLE
-- ADD document_name TO DOCUMENTS TABLE
UPDATE workitem_documents SET uid=SUBSTR(UUID(),1,8);
ALTER TABLE workitem_documents 	ADD UNIQUE `INDEX_workitem_documents_uid` ( `uid` );
UPDATE workitem_documents SET name=number;

-- ************************
ALTER TABLE `workitem_documents_history` 
	ADD `data` text DEFAULT NULL,
	ADD `action_comment` text DEFAULT NULL AFTER `action_date`,
	DROP `check_out_by`,
	DROP `check_out_date`,
	DROP `issued_from_document`,
	DROP `update_date`,
	DROP `update_by`,
	DROP `open_date`,
	DROP `open_by`,
	DROP `doctype_id`,
	DROP `instance_id`,
	CHANGE COLUMN `workitem_id` `container_id` int(11),
	CHANGE COLUMN `document_number` `number` VARCHAR(128),
	CHANGE COLUMN `document_state` `life_stage` VARCHAR(32),
	CHANGE COLUMN `document_access_code` `acode` int(11),
	CHANGE COLUMN `document_version` `iteration` int(11),
	CHANGE COLUMN `document_indice_id` `version` int(5),
	ADD `action_started` DATETIME NULL DEFAULT NULL AFTER `action_date`;

-- documents_history --
UPDATE `workitem_documents_history` SET 
	`action_started`=from_unixtime(`action_date`, '%Y-%m-%d %h:%i:%s');
ALTER TABLE `workitem_documents_history`
	DROP COLUMN `action_date`;
	
-- ************************
ALTER TABLE `workitem_doc_rel`
	CHANGE COLUMN `dr_link_id` `link_id` int(11),
	CHANGE COLUMN `dr_document_id` `parent_id` INT(11),
	CHANGE COLUMN `dr_l_document_id` `child_id` INT(11),
	CHANGE COLUMN `dr_access_code` `acode` int(11),
	ADD COLUMN `hash` char(32) default NULL AFTER `acode`,
	ADD COLUMN `data` varchar(512) default NULL AFTER `hash`;
	
-- ************************
ALTER TABLE `workitem_doc_files_versions`
	CHANGE COLUMN `file_id` `id` int(11),
	CHANGE COLUMN `file_name` `name` VARCHAR(128) NOT NULL,
	CHANGE COLUMN `file_path` `path` VARCHAR(256) NOT NULL,
	CHANGE COLUMN `file_version` `iteration` int(11),
	CHANGE COLUMN `file_root_name` `root_name` VARCHAR(128),
	CHANGE COLUMN `file_extension` `extension` VARCHAR(16),
	CHANGE COLUMN `file_state` `life_stage` VARCHAR(16),
	CHANGE COLUMN `file_type` `type` VARCHAR(16),
	CHANGE COLUMN `file_size` `size` int(11),
	CHANGE COLUMN `file_md5` `md5` VARCHAR(128),
	CHANGE COLUMN `file_open_by` `create_by_id` INT(11),
	CHANGE COLUMN `file_update_by` `update_by_id` INT(11),
	CHANGE COLUMN `father_id` `document_id` INT(11) NOT NULL,
	ADD COLUMN `acode` int(11) AFTER iteration,
	ADD `created` DATETIME NULL DEFAULT NULL AFTER `file_open_date`,
	ADD `updated` DATETIME NULL DEFAULT NULL AFTER `file_update_date`,
	ADD `locked` DATETIME NULL DEFAULT NULL,
	ADD `mtime` DATETIME NULL DEFAULT NULL AFTER `file_mtime`,
	ADD `of_file_id` int(11) DEFAULT NULL AFTER `document_id`,
	ADD uid VARCHAR(140) DEFAULT NULL AFTER `id`,
	ADD `mainrole` int(2) NOT NULL DEFAULT 1 AFTER `path`,
	ADD `roles` VARCHAR(512) NULL AFTER `mainrole`,
	ADD `lock_by_id` int(11) DEFAULT NULL AFTER `md5`,
	ADD KEY `IK_workitem_doc_files_version_5` (`mainrole`),
	ADD KEY `IK_workitem_doc_files_version_6` (`roles`(32)),
	ADD KEY `IK_workitem_doc_files_version_7` (`of_file_id`);
	
-- Dates --
UPDATE `workitem_doc_files_versions` SET 
	`created`=from_unixtime(`file_open_date`, '%Y-%m-%d %h:%i:%s'),
	`updated`=from_unixtime(`file_update_date`, '%Y-%m-%d %h:%i:%s'),
	`mtime`=from_unixtime(`file_mtime`, '%Y-%m-%d %h:%i:%s');
ALTER TABLE `workitem_doc_files_versions`
	DROP COLUMN `file_open_date`,
	DROP COLUMN `file_update_date`,
	DROP COLUMN `file_mtime`;
	
-- ************************
-- Import history
ALTER TABLE `workitem_import_history`
	ADD `imported` DATETIME NULL DEFAULT NULL AFTER `import_date`,
	ADD `package_mtime` DATETIME NULL DEFAULT NULL AFTER `package_file_mtime`;
UPDATE `workitem_import_history` SET 
	`imported`=from_unixtime(`import_date`, '%Y-%m-%d %h:%i:%s'),
	`package_mtime`=from_unixtime(`package_file_mtime`, '%Y-%m-%d %h:%i:%s');
ALTER TABLE `workitem_import_history`
	DROP COLUMN `import_date`,
	DROP COLUMN `package_file_mtime`;
	
-- ************************
ALTER TABLE `workitem_doc_files`
    CHANGE COLUMN `file_id` `id` int(11),
	CHANGE COLUMN `file_name` `name` VARCHAR(128),
	CHANGE COLUMN `file_path` `path` VARCHAR(256),
	CHANGE COLUMN `file_version` `iteration` int(11),
	CHANGE COLUMN `file_access_code` `acode` int(11),
	CHANGE COLUMN `file_root_name` `root_name` VARCHAR(128),
	CHANGE COLUMN `file_extension` `extension` VARCHAR(16),
	CHANGE COLUMN `file_state` `life_stage` VARCHAR(16),
	CHANGE COLUMN `file_type` `type` VARCHAR(16),
	CHANGE COLUMN `file_size` `size` int(11),
	CHANGE COLUMN `file_md5` `md5` VARCHAR(128),
	CHANGE COLUMN `file_checkout_by` `lock_by_id` INT(11),
	CHANGE COLUMN `file_open_by` `create_by_id` INT(11),
	CHANGE COLUMN `file_update_by` `update_by_id` INT(11),
	CHANGE COLUMN `file_used_name` `file_used_name` VARCHAR(128) NULL,
	ADD `created` DATETIME NULL DEFAULT NULL AFTER `file_open_date`,
	ADD `updated` DATETIME NULL DEFAULT NULL AFTER `file_update_date`,
	ADD `locked` DATETIME NULL DEFAULT NULL AFTER `file_checkout_date`,
	ADD `mtime` DATETIME NULL DEFAULT NULL AFTER `file_mtime`,
	ADD `uid` VARCHAR(140) DEFAULT NULL AFTER id,
	ADD `cid` VARCHAR(16) NOT NULL DEFAULT '569e92b86d248' AFTER uid,
	ADD `mainrole` int(2) NOT NULL DEFAULT 1,
	ADD `roles` VARCHAR(512) NULL,
	ADD COLUMN document_uid VARCHAR(64) DEFAULT NULL AFTER document_id,
    ADD KEY `IK_workitem_doc_files_1` (`document_id`),
	ADD KEY `IK_workitem_doc_files_2` (`name`),
	ADD KEY `IK_workitem_doc_files_3` (`path`(128)),
	ADD KEY `IK_workitem_doc_files_4` (`iteration`,`document_id`),
	ADD KEY `IK_workitem_doc_files_5` (`mainrole`),
	ADD KEY `IK_workitem_doc_files_6` (`roles`(32)),
	ADD KEY `IK_workitem_doc_files_8` (`document_uid`),
	DROP INDEX `file_md5`,
	DROP INDEX `file_name`;

-- dates --
UPDATE `workitem_doc_files` SET 
	`created`=from_unixtime(`file_open_date`, '%Y-%m-%d %h:%i:%s'),
	`updated`=from_unixtime(`file_update_date`, '%Y-%m-%d %h:%i:%s'),
	`locked`=from_unixtime(`file_checkout_date`, '%Y-%m-%d %h:%i:%s'),
	`mtime`=from_unixtime(`file_mtime`, '%Y-%m-%d %h:%i:%s');
ALTER TABLE `workitem_doc_files`
	DROP COLUMN `file_open_date`,
	DROP COLUMN `file_update_date`,
	DROP COLUMN `file_checkout_date`,
	DROP COLUMN `file_mtime`;

UPDATE workitem_doc_files SET uid=SUBSTR(UUID(),1,8);
UPDATE `workitem_doc_files` AS df SET `document_uid`=(SELECT uid FROM workitem_documents WHERE id=df.document_id LIMIT 1);
ALTER TABLE workitem_doc_files 	ADD UNIQUE `INDEX_workitem_doc_files_uid` ( `uid` );

-- ************************
ALTER TABLE `workitem_doccomments`
	CHANGE COLUMN `comment_id` `id` int(11);

-- ************************
ALTER TABLE `workitem_categories`
	CHANGE COLUMN `category_id` `id` int(11),
	CHANGE COLUMN `category_number` `number` varchar(128),
	CHANGE COLUMN `category_description` `designation` varchar(128),
	CHANGE COLUMN `category_icon` `icon` varchar(64),
	ADD COLUMN `uid` VARCHAR(64) NOT NULL;

UPDATE `workitem_categories` SET uid=SUBSTR(UUID(),1,8);


