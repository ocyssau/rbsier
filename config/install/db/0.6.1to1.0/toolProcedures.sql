SET NAMES 'latin1' COLLATE 'utf8_general_ci';

DROP PROCEDURE IF EXISTS getFilesWithoutDocument;
DELIMITER $$
CREATE PROCEDURE `getFilesWithoutDocument`()
BEGIN
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"workitem" as `spacename`
	FROM workitem_doc_files as df
	LEFT OUTER JOIN workitem_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"bookshop" as `spacename`
	FROM bookshop_doc_files as df
	LEFT OUTER JOIN bookshop_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"cadlib" as `spacename`
	FROM cadlib_doc_files as df
	LEFT OUTER JOIN cadlib_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"mockup" as `spacename`
	FROM mockup_doc_files as df
	LEFT OUTER JOIN mockup_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL;
END$$
DELIMITER ;

-- ###################################################
DROP PROCEDURE IF EXISTS getDocumentsWithoutContainer;
DELIMITER $$
CREATE PROCEDURE `getDocumentsWithoutContainer`()
BEGIN
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"workitem" as `spacename`
	FROM workitem_documents as doc
	LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"bookshop" as `spacename`
	FROM bookshop_documents as doc
	LEFT OUTER JOIN bookshops as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"cadlib" as `spacename`
	FROM cadlib_documents as doc
	LEFT OUTER JOIN cadlibs as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"mockup" as `spacename`
	FROM mockup_documents as doc
	LEFT OUTER JOIN mockups as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL;
-- ###################################################
END$$
DELIMITER ;


-- ###################################################
DROP PROCEDURE IF EXISTS updateCheckoutIndex;
DELIMITER $$
CREATE PROCEDURE `updateCheckoutIndex`()
BEGIN
	DELETE FROM checkout_index;
	INSERT INTO checkout_index(
	SELECT 
	df.name as file_name,
	df.id as file_id,
	df.uid as file_uid, 
	doc.designation as designation,
	"workitem" as spacename,
	doc.container_id as container_id, 
	cont.number as container_number,
	doc.lock_by_id as checkout_out_by,
	df.document_id as document_id
	FROM workitem_doc_files as df
	JOIN workitem_documents as doc ON df.document_id=doc.id
	JOIN workitems as cont ON doc.container_id=cont.id
	WHERE doc.lock_by_id IS NOT NULL);
-- ###################################################
END$$
DELIMITER ;

-- ###################################################
DROP PROCEDURE IF EXISTS deleteDocumentsWithoutContainer;
DELIMITER $$
CREATE PROCEDURE `deleteDocumentsWithoutContainer` ()
BEGIN
	CREATE TABLE tmpdoctodelete AS (
		SELECT doc.id, cont.number FROM workitem_documents as doc
		LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id 
		WHERE cont.number is null
	);
	DELETE FROM workitem_doc_files WHERE document_id IN(SELECT id FROM tmpdoctodelete);
	DELETE FROM workitem_documents WHERE id IN(SELECT id FROM tmpdoctodelete);
	DROP TABLE tmpdoctodelete;
-- ###################################################
END$$
DELIMITER ;
