-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- ------------------------------------------------------
-- ------------------------------------------------------
-- ------------------------------------------------------
-- Generate with :
-- mysqldump --routines --no-create-info --no-data --no-create-db --skip-opt ranchbe -u ranchbe -p > routine.sql
-- ------------------------------------------------------
-- ------------------------------------------------------
-- ------------------------------------------------------
-- ------------------------------------------------------
-- Host: localhost    Database: ranchbe
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onBookshopCategoryRelInsert BEFORE INSERT ON bookshop_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onBookshopDocfileUpdate AFTER UPDATE ON bookshop_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `bookshop_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'bookshop',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onBookshopDocfileDelete AFTER DELETE ON bookshop_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='bookshop';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onBookshopDoctypeRelInsert BEFORE INSERT ON bookshop_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onBookshopDocumentDelete AFTER DELETE ON bookshop_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=20;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT childId FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onBookshopProcessRelInsert BEFORE INSERT ON bookshop_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn varchar(128);
 SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onBookshopPropertyRelInsert BEFORE INSERT ON bookshop_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onBookshopInsert BEFORE INSERT ON bookshops FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onBookshopUpdate BEFORE UPDATE ON bookshops FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(64);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn,uid INTO parentDn,parentUid from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onBookshopDelete AFTER DELETE ON bookshops FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='bookshop';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='bookshop';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='bookshop';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCadlibCategoryRelInsert BEFORE INSERT ON cadlib_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCadlibDocfileUpdate AFTER UPDATE ON cadlib_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `cadlib_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'cadlib',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCadlibDocfileDelete AFTER DELETE ON cadlib_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='cadlib';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCadlibDoctypeRelInsert BEFORE INSERT ON cadlib_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCadlibDocumentDelete AFTER DELETE ON cadlib_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=25;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT childId FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCadlibProcessRelInsert BEFORE INSERT ON cadlib_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn varchar(128);
 SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCadlibPropertyRelInsert BEFORE INSERT ON cadlib_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCadlibInsert BEFORE INSERT ON cadlibs FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCadlibUpdate BEFORE UPDATE ON cadlibs FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(64);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn,uid INTO parentDn,parentUid from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCadlibDelete AFTER DELETE ON cadlibs FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='cadlib';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='cadlib';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='cadlib';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCategoriesInsert BEFORE INSERT ON categories FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentName varchar(128);
	
	IF(NEW.parent_id IS NULL) THEN
		SET NEW.dn = CONCAT('/',NEW.id,'/');
		SET NEW.name = CONCAT('/',NEW.nodelabel,'/');
	ELSE
		SELECT dn, name INTO parentDn, parentName from categories where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn, NEW.id,'/');
		SET NEW.name = CONCAT(parentName, NEW.nodelabel,'/');
	END IF;
	
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onCategoriesUpdate BEFORE UPDATE ON categories FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentName varchar(128);
    
	IF(NEW.parent_id IS NULL) THEN
		SET NEW.dn = CONCAT('/',NEW.id,'/');
		SET NEW.name = CONCAT('/',NEW.nodelabel,'/');
	ELSEIF(IFNULL(NEW.parent_id=OLD.parent_id, TRUE) OR IFNULL(NEW.nodelabel=OLD.nodelabel, TRUE) OR NEW.parent_id != OLD.parent_id OR NEW.nodelabel != OLD.nodelabel) THEN
		-- get parent dn
		SELECT dn, name INTO parentDn, parentName from categories where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn, NEW.id,'/');
		SET NEW.name = CONCAT(parentName, NEW.nodelabel,'/');
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onMockupCategoryRelInsert BEFORE INSERT ON mockup_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onMockupDocfileDelete AFTER DELETE ON mockup_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='mockup';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onMockupDoctypeRelInsert BEFORE INSERT ON mockup_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onMockupDocumentDelete AFTER DELETE ON mockup_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=15;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT childId FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onMockupProcessRelInsert BEFORE INSERT ON mockup_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn varchar(128);
 SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onMockupPropertyRelInsert BEFORE INSERT ON mockup_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onMockupInsert BEFORE INSERT ON mockups FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onMockupUpdate BEFORE UPDATE ON mockups FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onMockupDelete AFTER DELETE ON mockups FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='mockup';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='mockup';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='mockup';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onProjectCategoryRelInsert BEFORE INSERT ON project_category_rel FOR EACH ROW
 BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM categories WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onOrgContainerRelInsert BEFORE INSERT ON project_container_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	CASE NEW.spacename
		WHEN 'bookshop' THEN
			SELECT uid INTO childUid FROM bookshops WHERE id=NEW.child_id;
		WHEN 'cadlib' THEN
			SELECT uid INTO childUid FROM cadlibs WHERE id=NEW.child_id;
		WHEN 'mockup' THEN
			SELECT uid INTO childUid FROM mockups WHERE id=NEW.child_id;
		WHEN 'workitem' THEN
			SELECT uid INTO childUid FROM workitems WHERE id=NEW.child_id;
	END CASE;
			
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onProjectDoctypeRelInsert BEFORE INSERT ON project_process_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM wf_process WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onProjectInsert BEFORE INSERT ON projects FOR EACH ROW 
BEGIN
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentDn VARCHAR(128);

	IF(NEW.parent_id IS NULL) THEN
		SET NEW.dn = CONCAT('/',NEW.uid,'/');
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
	ELSE
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
	END IF;
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onProjectUpdate BEFORE UPDATE ON projects FOR EACH ROW 
BEGIN
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- update parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		-- update resource
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE referToId=NEW.id AND `referToCid`='569e93c6ee156';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onProjectDelete AFTER DELETE ON projects FOR EACH ROW 
BEGIN
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWorkitemCategoryRelInsert BEFORE INSERT ON workitem_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWIDocfileInsert AFTER INSERT ON workitem_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF NEW.acode = 1 THEN
		SELECT 
			`container_id`,`container_uid`,`id`,`designation` 
            INTO 
            containerId, containerNumber, documentId, designation
			FROM `workitem_documents`
			WHERE `id`=NEW.document_id;
		
		INSERT INTO `checkout_index`
		(
		`file_name`,
		`lock_by_id`,
		`lock_by_uid`,
		`file_id`,
		`file_uid`,
		`designation`,
		`spacename`,
		`container_id`,
		`container_number`,
		`document_id`
		)
		VALUES
        (
			NEW.name,
			NEW.lock_by_id,
			NEW.lock_by_uid,
			NEW.id,
			NEW.uid,
			`designation`,
			'workitem',
			`containerId`,
			`containerNumber`,
			`documentId`
		);        
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWIDocfileUpdate AFTER UPDATE ON workitem_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `workitem_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'workitem',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWIDocfileDelete AFTER DELETE ON workitem_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='workitem';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWorkitemDoctypeRelInsert BEFORE INSERT ON workitem_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWIDocumentDelete AFTER DELETE ON workitem_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=10;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT childId FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWorkitemProcessRelInsert BEFORE INSERT ON workitem_process_rel FOR EACH ROW
 BEGIN
 DECLARE parentDn varchar(128);
 SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
 SET NEW.rdn = parentDn;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWorkitemPropertyRelInsert BEFORE INSERT ON workitem_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWorkitemInsert BEFORE INSERT ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWorkitemUpdate BEFORE UPDATE ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER onWorkitemDelete AFTER DELETE ON workitems FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='workitem';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='workitem';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='workitem';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'ranchbe'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `aclSequence`() RETURNS int(11)
BEGIN
	DECLARE lastId INT(11);

	UPDATE acl_resource_seq SET id = LAST_INSERT_ID(id+1) LIMIT 1;
	SELECT LAST_INSERT_ID(id) INTO lastId FROM acl_resource_seq;

    set @ret = lastId;
    RETURN @ret;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `build_path_withuid`(_uid VARCHAR(64)) RETURNS text CHARSET utf8
BEGIN
    DECLARE parentUid VARCHAR(64);
    DECLARE cid VARCHAR(64);
    DECLARE message VARCHAR(256);
    set @ret = '';
    
    while _uid is not null do
      SELECT parent_uid, cid INTO parentUid, cid 
      FROM acl_resource_breakdown 
      WHERE uid = _uid;

    IF(found_rows() != 1) THEN
      set message = CONCAT('uid not found in table acl_resource_breakdown :', _uid);
      SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = message;
    END IF;
    
	IF(parentUid IS NULL) THEN
		set @ret := CONCAT('/app/ged/project/', _uid, @ret);
	ELSE
		set @ret := CONCAT('/', _uid, @ret);
	END IF;

	set _uid := parentUid;
    end while;

    RETURN @ret;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `getResourceCnFromReferIdAndCid`(_id INT(11), _cid VARCHAR(128)) RETURNS varchar(256) CHARSET utf8
BEGIN
	DECLARE resourceCn varchar(256);

	SELECT cn INTO resourceCn FROM acl_resource WHERE referToId = _id AND referToCid = _cid;
    IF(resourceCn IS NULL) THEN
		SELECT cn INTO resourceCn FROM acl_resource AS resource
		JOIN acl_resource_breakdown as breakdown ON resource.referToUid = breakdown.parent_uid
		WHERE breakdown.id = _id AND breakdown.cid=_cid;
	END IF;

	return resourceCn;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `getResourceCnFromReferUid`(_uid VARCHAR(128)) RETURNS varchar(256) CHARSET utf8
BEGIN
	DECLARE resourceCn varchar(256);

	SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = _uid;
    IF(resourceCn IS NULL) THEN
		SELECT cn INTO resourceCn FROM acl_resource AS resource
		JOIN acl_resource_breakdown as breakdown ON resource.referToUid = breakdown.parent_uid
		WHERE breakdown.uid = _uid;
	END IF;

	return resourceCn;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `cancelCheckout`(_userUid VARCHAR(64))
BEGIN
	UPDATE `workitem_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
	UPDATE `workitem_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
	UPDATE `bookshop_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
	UPDATE `bookshop_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
	UPDATE `cadlib_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
	UPDATE `cadlib_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `ranchbe` CHARACTER SET latin1 COLLATE latin1_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `deleteDocumentsWithoutContainer`()
BEGIN
	CREATE TABLE tmpdoctodelete AS (
		SELECT doc.id, cont.number FROM workitem_documents as doc
		LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id 
		WHERE cont.number is null
	);
	DELETE FROM workitem_doc_files WHERE document_id IN(SELECT id FROM tmpdoctodelete);
	DELETE FROM workitem_documents WHERE id IN(SELECT id FROM tmpdoctodelete);
	DROP TABLE tmpdoctodelete;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `ranchbe` CHARACTER SET utf8 COLLATE utf8_general_ci ;
ALTER DATABASE `ranchbe` CHARACTER SET latin1 COLLATE latin1_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `getDocumentsWithoutContainer`()
BEGIN
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"workitem" as `spacename`
	FROM workitem_documents as doc
	LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"bookshop" as `spacename`
	FROM bookshop_documents as doc
	LEFT OUTER JOIN bookshops as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"cadlib" as `spacename`
	FROM cadlib_documents as doc
	LEFT OUTER JOIN cadlibs as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"mockup" as `spacename`
	FROM mockup_documents as doc
	LEFT OUTER JOIN mockups as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `ranchbe` CHARACTER SET utf8 COLLATE utf8_general_ci ;
ALTER DATABASE `ranchbe` CHARACTER SET latin1 COLLATE latin1_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `getFilesWithoutDocument`()
BEGIN
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"workitem" as `spacename`
	FROM workitem_doc_files as df
	LEFT OUTER JOIN workitem_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"bookshop" as `spacename`
	FROM bookshop_doc_files as df
	LEFT OUTER JOIN bookshop_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"cadlib" as `spacename`
	FROM cadlib_doc_files as df
	LEFT OUTER JOIN cadlib_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"mockup" as `spacename`
	FROM mockup_doc_files as df
	LEFT OUTER JOIN mockup_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `ranchbe` CHARACTER SET utf8 COLLATE utf8_general_ci ;
ALTER DATABASE `ranchbe` CHARACTER SET latin1 COLLATE latin1_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `getResourceFromReferUid`(_uid VARCHAR(64))
BEGIN
	DECLARE resourceUid varchar(64);

	SELECT uid INTO resourceUid FROM acl_resource WHERE referToUid = _uid;
    IF(resourceUid IS NULL) THEN
		SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE referToUid = (SELECT parent_uid FROM view_resource_breakdown WHERE uid = _uid);
	ELSE
		SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE uid = resourceUid;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `ranchbe` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateCheckoutIndex`()
BEGIN
	DELETE FROM `checkout_index`;
	INSERT INTO checkout_index(
		SELECT * FROM(
			SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
			FROM workitem_doc_files as file
			JOIN workitem_documents as doc ON file.document_id=doc.id
			where file.acode=1
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM bookshop_doc_files as file
				JOIN bookshop_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM cadlib_doc_files as file
				JOIN cadlib_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM mockup_doc_files as file
				JOIN mockup_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
		) as checkout_index
	);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateCheckoutIndexFromUserId`(_userId INT(11))
BEGIN
	DECLARE userId INT(11);
	SET @userId=_userId;
	DELETE FROM `checkout_index` WHERE `lock_by_id`=@userId;
	INSERT INTO checkout_index(
		SELECT * FROM(
			SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
			FROM workitem_doc_files as file
			JOIN workitem_documents as doc ON file.document_id=doc.id
			where file.acode=1 and file.lock_by_id=@userId
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM bookshop_doc_files as file
				JOIN bookshop_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM cadlib_doc_files as file
				JOIN cadlib_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM mockup_doc_files as file
				JOIN mockup_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
		) as checkout_index
	);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateDocfileDocumentUidRelation`()
BEGIN


#### WORKITEM
DROP TEMPORARY TABLE IF EXISTS tmptableworkitem;
CREATE TEMPORARY TABLE tmptableworkitem AS (
	SELECT df.* FROM workitem_doc_files as df
	LEFT OUTER JOIN workitem_documents as doc ON df.document_uid=doc.uid
	WHERE doc.id IS NULL
);
DROP TABLE IF EXISTS workitem_doc_files_backup;
CREATE TABLE workitem_doc_files_backup AS(
	SELECT * FROM workitem_doc_files
);
UPDATE workitem_doc_files as df 
SET document_uid=(SELECT uid FROM workitem_documents WHERE id=df.document_id)
WHERE df.id IN (SELECT id FROM tmptableworkitem);

#### BOOKSHOP
DROP TEMPORARY TABLE IF EXISTS tmptablebookshop;
CREATE TEMPORARY TABLE tmptablebookshop AS (
	SELECT df.* FROM bookshop_doc_files as df
	LEFT OUTER JOIN bookshop_documents as doc ON df.document_uid=doc.uid
	WHERE doc.id IS NULL
);
DROP TABLE IF EXISTS bookshop_doc_files_backup;
CREATE TABLE bookshop_doc_files_backup AS(
	SELECT * FROM bookshop_doc_files
);
UPDATE bookshop_doc_files as df 
SET document_uid=(SELECT uid FROM bookshop_documents WHERE id=df.document_id)
WHERE df.id IN (SELECT id FROM tmptablebookshop);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateParentUid`()
BEGIN
	UPDATE workitems SET parent_uid = (SELECT uid FROM projects WHERE id=workitems.parent_id) WHERE workitems.parent_uid IS NULL;
	UPDATE cadlibs SET parent_uid = (SELECT uid FROM projects WHERE id=cadlibs.parent_id) WHERE cadlibs.parent_uid IS NULL;
	UPDATE bookshops SET parent_uid = (SELECT uid FROM projects WHERE id=bookshops.parent_id) WHERE bookshops.parent_uid IS NULL;
	UPDATE mockups SET parent_uid = (SELECT uid FROM projects WHERE id=mockups.parent_id) WHERE mockups.parent_uid IS NULL;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateResourceBreakdown`()
BEGIN
DROP TABLE IF EXISTS acl_resource_breakdown;
	CREATE TABLE acl_resource_breakdown as
	    (SELECT 
	        `doc`.`id` AS `id`,
	        `doc`.`uid` AS `uid`,
	        `doc`.`name` AS `name`,
	        `doc`.`cid` AS `cid`,
	        `doc`.`container_id` AS `parent_id`,
	        `doc`.`container_uid` AS `parent_uid`
	    FROM
	        `workitem_documents` `doc`) UNION (SELECT 
	        `doc`.`id` AS `id`,
	        `doc`.`uid` AS `uid`,
	        `doc`.`name` AS `name`,
	        `doc`.`cid` AS `cid`,
	        `doc`.`container_id` AS `parent_id`,
	        `doc`.`container_uid` AS `parent_uid`
	    FROM
	        `bookshop_documents` `doc`) UNION (SELECT 
	        `doc`.`id` AS `id`,
	        `doc`.`uid` AS `uid`,
	        `doc`.`name` AS `name`,
	        `doc`.`cid` AS `cid`,
	        `doc`.`container_id` AS `parent_id`,
	        `doc`.`container_uid` AS `parent_uid`
	    FROM
	        `cadlib_documents` `doc`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `workitems` `cont`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `cadlibs` `cont`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `bookshops` `cont`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `mockups` `cont`) UNION (SELECT 
	        `projects`.`id` AS `id`,
	        `projects`.`uid` AS `uid`,
	        `projects`.`name` AS `name`,
	        `projects`.`cid` AS `cid`,
	        `projects`.`parent_id` AS `parent_id`,
	        `projects`.`parent_uid` AS `parent_uid`
	    FROM
	        `projects`);
	        
	ALTER TABLE acl_resource_breakdown 
	ADD KEY (`id`),
	ADD KEY (`uid`),
	ADD KEY (`name`),
	ADD KEY (`cid`),
	ADD KEY (`parent_id`),
	ADD KEY (`parent_uid`);

	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateSearch`(_intervalInMn integer)
BEGIN
	SELECT `updated` INTO @updated FROM `search_update`;
    CASE 
		WHEN @updated > (now() + INTERVAL - _intervalInMn minute) THEN 
			SELECT 'is up to date';
			BEGIN
			END;
		ELSE
			DROP TABLE IF EXISTS search;
   			CREATE TABLE search AS SELECT * FROM objects;
			UPDATE `search_update` SET `updated`=now();
	END CASE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateWorkitemDn`()
BEGIN
	UPDATE workitems SET dn = CONCAT((SELECT dn FROM projects WHERE id=workitems.parent_id), uid,'/') WHERE workitems.dn IS NULL;
	UPDATE cadlibs SET dn = CONCAT((SELECT dn FROM projects WHERE id=cadlibs.parent_id), uid,'/') WHERE cadlibs.dn IS NULL;
	UPDATE bookshops SET dn = CONCAT((SELECT dn FROM projects WHERE id=bookshops.parent_id), uid,'/') WHERE bookshops.dn IS NULL;
	UPDATE mockups SET dn = CONCAT((SELECT dn FROM projects WHERE id=mockups.parent_id), uid,'/') WHERE mockups.dn IS NULL;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-01 18:11:25
