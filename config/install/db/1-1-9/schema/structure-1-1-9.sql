-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: 192.9.200.21    Database: ranchbe
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acl_group`
--

-- DROP TABLE IF EXISTS `acl_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_group` (
  `id` int(11) NOT NULL,
  `uid` varchar(30) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` varchar(64) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `FK_acl_group_acl_user1` (`owner_id`),
  CONSTRAINT `FK_acl_group_acl_user1` FOREIGN KEY (`owner_id`) REFERENCES `acl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_resource`
--

-- DROP TABLE IF EXISTS `acl_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_resource` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `cid` varchar(64) DEFAULT 'aclresource60',
  `cn` varchar(255) NOT NULL,
  `ownerId` int(11) DEFAULT NULL,
  `ownerUid` varchar(64) DEFAULT NULL,
  `referToUid` varchar(64) DEFAULT NULL,
  `referToId` int(11) DEFAULT NULL,
  `referToCid` varchar(64) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `cn` (`cn`),
  KEY `referToUid` (`referToUid`),
  KEY `referToId` (`referToId`),
  KEY `referToCid` (`referToCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_resource_breakdown`
--

-- DROP TABLE IF EXISTS `acl_resource_breakdown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_resource_breakdown` (
  `id` int(11) NOT NULL DEFAULT '0',
  `uid` varchar(140) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `cid` varchar(64) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(128) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `uid` (`uid`),
  KEY `name` (`name`),
  KEY `cid` (`cid`),
  KEY `parent_id` (`parent_id`),
  KEY `parent_uid` (`parent_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_right`
--

-- DROP TABLE IF EXISTS `acl_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_right` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_i_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_role`
--

-- DROP TABLE IF EXISTS `acl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_role` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT 'aclrole3zc547',
  `name` varchar(32) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(64) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `parentId` (`parentId`),
  KEY `parentUid` (`parentUid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_rule`
--

-- DROP TABLE IF EXISTS `acl_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_rule` (
  `roleId` int(11) NOT NULL,
  `resource` varchar(128) NOT NULL,
  `resourceId` int(11) DEFAULT NULL,
  `rightId` int(11) NOT NULL,
  `rule` varchar(16) DEFAULT 'allow',
  PRIMARY KEY (`roleId`,`rightId`,`resource`),
  KEY `role_idx` (`roleId`),
  KEY `right_idx` (`rightId`),
  KEY `resource_idx` (`resource`),
  CONSTRAINT `FK_acl_rule_acl_resource1` FOREIGN KEY (`resource`) REFERENCES `acl_resource` (`cn`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_acl_rule_acl_right1` FOREIGN KEY (`rightId`) REFERENCES `acl_right` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_acl_rule_acl_role1` FOREIGN KEY (`roleId`) REFERENCES `acl_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_user`
--

-- DROP TABLE IF EXISTS `acl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_user` (
  `id` int(11) NOT NULL,
  `uid` varchar(128) NOT NULL,
  `login` varchar(128) NOT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `firstname` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `mail` varchar(128) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(64) DEFAULT NULL,
  `primary_role_id` int(11) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `auth_from` varchar(16) DEFAULT 'db',
  `conn_from` varchar(16) DEFAULT 'auto',
  `extends` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_idx` (`uid`),
  UNIQUE KEY `login_idx` (`login`),
  KEY `owner_uid` (`owner_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_user_role`
--

-- DROP TABLE IF EXISTS `acl_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_user_role` (
  `uid` varchar(64) NOT NULL,
  `cid` char(13) NOT NULL DEFAULT 'acluserrole59',
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  `resourceCn` varchar(255) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`,`resourceCn`),
  KEY `userId` (`userId`),
  KEY `roleId` (`roleId`),
  KEY `resourceCn` (`resourceCn`),
  CONSTRAINT `FK_acl_user_role_acl_resource1` FOREIGN KEY (`resourceCn`) REFERENCES `acl_resource` (`cn`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_acl_user_role_acl_role1` FOREIGN KEY (`roleId`) REFERENCES `acl_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_acl_user_role_acl_user1` FOREIGN KEY (`userId`) REFERENCES `acl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batch`
--

-- DROP TABLE IF EXISTS `batch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(258) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(128) DEFAULT NULL,
  `callback_class` text,
  `callback_method` varchar(64) DEFAULT NULL,
  `callback_params` text,
  `comment` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_batch_1` (`uid`),
  KEY `K_batch_2` (`owner_id`),
  KEY `K_batch_3` (`owner_uid`)
) ENGINE=MyISAM AUTO_INCREMENT=7686 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batchjob`
--

-- DROP TABLE IF EXISTS `batchjob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batchjob` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(258) DEFAULT NULL,
  `callback_class` text,
  `callback_method` varchar(64) DEFAULT NULL,
  `callback_params` text,
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(128) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `planned` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `started` double DEFAULT NULL,
  `ended` double DEFAULT NULL,
  `duration` float DEFAULT NULL,
  `hashkey` varchar(40) NOT NULL,
  `pid` varchar(32) DEFAULT NULL,
  `sapi` varchar(32) DEFAULT NULL,
  `log` longblob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_batchjob_4` (`hashkey`),
  KEY `K_batchjob_1` (`planned`),
  KEY `K_batchjob_2` (`status`),
  KEY `K_batchjob_3` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=13770 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_alias`
--

-- DROP TABLE IF EXISTS `bookshop_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT 'bookshop',
  `uid` varchar(128) NOT NULL DEFAULT 'bookshop',
  `designation` varchar(128) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '45c84a5zalias',
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `U_bookshop_alias_1` (`uid`),
  KEY `K_bookshop_alias_1` (`container_id`),
  KEY `K_bookshop_alias_2` (`number`),
  KEY `K_bookshop_alias_3` (`cid`),
  KEY `K_bookshop_alias_4` (`name`),
  CONSTRAINT `FK_bookshop_alias_1` FOREIGN KEY (`container_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_category_rel`
--

-- DROP TABLE IF EXISTS `bookshop_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_bookshop_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_bookshop_category_rel_1` (`parent_id`),
  KEY `K_bookshop_category_rel_2` (`parent_uid`),
  KEY `K_bookshop_category_rel_3` (`parent_cid`),
  KEY `K_bookshop_category_rel_4` (`child_id`),
  KEY `K_bookshop_category_rel_5` (`child_uid`),
  KEY `K_bookshop_category_rel_6` (`child_cid`),
  KEY `K_bookshop_category_rel_7` (`rdn`),
  CONSTRAINT `FK_bookshop_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_cont_metadata_seq`
--

-- DROP TABLE IF EXISTS `bookshop_cont_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files`
--

-- DROP TABLE IF EXISTS `bookshop_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(255) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  UNIQUE KEY `INDEX_bookshop_doc_files_uid` (`uid`),
  KEY `FK_bookshop_doc_files_1` (`document_id`),
  KEY `IK_bookshop_doc_files_1` (`document_id`),
  KEY `IK_bookshop_doc_files_2` (`name`),
  KEY `IK_bookshop_doc_files_3` (`path`(128)),
  KEY `IK_bookshop_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_bookshop_doc_files_5` (`mainrole`),
  KEY `IK_bookshop_doc_files_6` (`roles`(32)),
  KEY `IK_bookshop_doc_files_8` (`document_uid`),
  KEY `FK_bookshop_docfiles_7` (`create_by_uid`),
  KEY `FK_bookshop_docfiles_8` (`lock_by_uid`),
  KEY `FK_bookshop_docfiles_9` (`update_by_uid`),
  KEY `FK_bookshop_docfiles_10` (`updated`),
  KEY `FK_bookshop_docfiles_11` (`created`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_bookshop_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `bookshop_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files_backup`
--

-- DROP TABLE IF EXISTS `bookshop_doc_files_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files_backup` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(255) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files_seq`
--

-- DROP TABLE IF EXISTS `bookshop_doc_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8211 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files_versions`
--

-- DROP TABLE IF EXISTS `bookshop_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  KEY `IK_bookshop_doc_files_version_1` (`document_id`),
  KEY `IK_bookshop_doc_files_version_2` (`name`),
  KEY `IK_bookshop_doc_files_version_3` (`path`),
  KEY `IK_bookshop_doc_files_version_4` (`iteration`,`document_id`),
  KEY `of_file_id` (`of_file_id`),
  KEY `IK_bookshop_doc_files_version_5` (`mainrole`),
  KEY `IK_bookshop_doc_files_version_6` (`roles`(32)),
  KEY `IK_bookshop_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files_versions_seq`
--

-- DROP TABLE IF EXISTS `bookshop_doc_files_versions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1040 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_rel`
--

-- DROP TABLE IF EXISTS `bookshop_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_rel` (
  `link_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `hash` char(32) DEFAULT NULL,
  `data` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `FK_bookshop_doc_rel_10` (`parent_id`),
  KEY `FK_bookshop_doc_rel_2` (`child_id`),
  CONSTRAINT `FK_bookshop_doc_rel_1` FOREIGN KEY (`parent_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_doc_rel_2` FOREIGN KEY (`child_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doccomments`
--

-- DROP TABLE IF EXISTS `bookshop_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doctype_rel`
--

-- DROP TABLE IF EXISTS `bookshop_doctype_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_bookshop_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_bookshop_doctype_rel_1` (`parent_id`),
  KEY `K_bookshop_doctype_rel_2` (`parent_uid`),
  KEY `K_bookshop_doctype_rel_3` (`parent_cid`),
  KEY `K_bookshop_doctype_rel_4` (`child_id`),
  KEY `K_bookshop_doctype_rel_5` (`child_uid`),
  KEY `K_bookshop_doctype_rel_6` (`child_cid`),
  KEY `K_bookshop_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_bookshop_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_documents`
--

-- DROP TABLE IF EXISTS `bookshop_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  `life_stage` varchar(128) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `branch_id` int(1) DEFAULT '1',
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'bookshop',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tags` varchar(256) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `doc_source` varchar(255) DEFAULT NULL,
  `commentaires` varchar(255) DEFAULT NULL,
  `applicabilite` varchar(255) DEFAULT NULL,
  `maj_int_ind` varchar(255) DEFAULT NULL,
  `min_int_ind` varchar(255) DEFAULT NULL,
  `mot_cles` varchar(255) DEFAULT NULL,
  `fournisseur` varchar(32) DEFAULT NULL,
  `docsier_fournisseur` varchar(32) DEFAULT NULL,
  `docsier_famille` varchar(12) DEFAULT NULL,
  `docsier_date` int(11) DEFAULT NULL,
  `docsier_keyword` mediumtext,
  `docsier_format` varchar(255) DEFAULT NULL,
  `ata` varchar(255) DEFAULT NULL,
  `sub_ata` varchar(255) DEFAULT NULL,
  `a_type` varchar(255) DEFAULT NULL,
  `orig_filename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_bookshop_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_bookshop_documents_uid` (`uid`),
  KEY `FK_bookshop_documents_1` (`cad_model_supplier_id`),
  KEY `FK_bookshop_documents_2` (`supplier_id`),
  KEY `FK_bookshop_documents_3` (`container_id`),
  KEY `FK_bookshop_documents_4` (`category_id`),
  KEY `FK_bookshop_documents_5` (`doctype_id`),
  KEY `FK_bookshop_documents_6` (`version`),
  KEY `INDEX_bookshop_documents_spacename` (`spacename`),
  KEY `INDEX_bookshop_documents_name` (`name`),
  KEY `INDEX_bookshop_documents_cuid` (`container_uid`),
  KEY `FK_bookshop_documents_7` (`create_by_uid`),
  KEY `FK_bookshop_documents_8` (`lock_by_uid`),
  KEY `FK_bookshop_documents_9` (`update_by_uid`),
  KEY `FK_bookshop_documents_10` (`as_template`),
  KEY `FK_bookshop_documents_11` (`tags`),
  KEY `FK_bookshop_documents_12` (`branch_id`),
  FULLTEXT KEY `FT_bookshop_documents` (`name`,`number`,`designation`,`tags`),
  CONSTRAINT `FK_bookshop_documents_1` FOREIGN KEY (`container_id`) REFERENCES `bookshops` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_documents_3` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_documents_history`
--

-- DROP TABLE IF EXISTS `bookshop_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(256) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `action_batch_uid` varchar(128) DEFAULT NULL,
  `action_by` varchar(32) NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`),
  KEY `K_bookshop_documents_history_documentid` (`document_id`),
  KEY `K_bookshop_documents_history_containerid` (`container_id`),
  KEY `K_bookshop_documents_history_number` (`number`),
  KEY `K_bookshop_documents_history_batchuid` (`action_batch_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_familles`
--

-- DROP TABLE IF EXISTS `bookshop_familles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_familles` (
  `id` varchar(12) NOT NULL DEFAULT '0',
  `famille_name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_files`
--

-- DROP TABLE IF EXISTS `bookshop_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` mediumtext NOT NULL,
  `file_root_name` varchar(128) NOT NULL DEFAULT '',
  `file_extension` varchar(16) DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) NOT NULL DEFAULT 'init',
  `file_type` varchar(128) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_bookshop_files_1` (`bookshop_id`),
  KEY `FK_bookshop_files_2` (`import_order`),
  KEY `index_file_name` (`file_name`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_import_history`
--

-- DROP TABLE IF EXISTS `bookshop_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_key_word`
--

-- DROP TABLE IF EXISTS `bookshop_key_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_key_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_word` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `key_word` (`key_word`)
) ENGINE=InnoDB AUTO_INCREMENT=1516 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_metadata`
--

-- DROP TABLE IF EXISTS `bookshop_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `maybenull` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_process_rel`
--

-- DROP TABLE IF EXISTS `bookshop_process_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_process_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_process_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_bookshop_process_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_process_rel_1` (`parent_id`),
  KEY `K_project_process_rel_2` (`parent_uid`),
  KEY `K_project_process_rel_3` (`parent_cid`),
  KEY `K_project_process_rel_4` (`child_id`),
  KEY `K_project_process_rel_5` (`child_uid`),
  KEY `K_project_process_rel_6` (`child_cid`),
  KEY `K_project_process_rel_7` (`rdn`),
  KEY `K_bookshop_process_rel_1` (`parent_id`),
  KEY `K_bookshop_process_rel_2` (`parent_uid`),
  KEY `K_bookshop_process_rel_3` (`parent_cid`),
  KEY `K_bookshop_process_rel_4` (`child_id`),
  KEY `K_bookshop_process_rel_5` (`child_uid`),
  KEY `K_bookshop_process_rel_6` (`child_cid`),
  KEY `K_bookshop_process_rel_7` (`rdn`),
  CONSTRAINT `FK_bookshop_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_property_rel`
--

-- DROP TABLE IF EXISTS `bookshop_property_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_bookshop_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_bookshop_property_rel_1` (`parent_id`),
  KEY `K_bookshop_property_rel_2` (`parent_uid`),
  KEY `K_bookshop_property_rel_3` (`parent_cid`),
  KEY `K_bookshop_property_rel_4` (`child_id`),
  KEY `K_bookshop_property_rel_5` (`child_uid`),
  KEY `K_bookshop_property_rel_6` (`child_cid`),
  KEY `K_bookshop_property_rel_7` (`rdn`),
  CONSTRAINT `FK_bookshop_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `bookshop_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshops`
--

-- DROP TABLE IF EXISTS `bookshops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshops` LIKE `workitems`;
ALTER TABLE `bookshops` 
  CHANGE COLUMN `spacename` `spacename` VARCHAR(16) NOT NULL DEFAULT 'bookshop',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_bookshops_number` (`number`),
  UNIQUE KEY `UC_bookshops_uid` (`uid`),
  KEY `K_bookshops_create_by_uid` (`create_by_uid`),
  KEY `K_bookshops_close_by_uid` (`close_by_uid`),
  KEY `K_bookshops_version` (`version`),
  KEY `K_bookshops_parent_id` (`parent_id`),
  KEY `K_bookshops_default_process_id` (`default_process_id`),
  KEY `K_bookshops_name` (`name`),
  KEY `K_bookshops_dn` (`dn`),
  KEY `K_bookshops_reposit_uid` (`reposit_uid`),
  KEY `K_bookshops_reposit_id` (`reposit_id`),
  FULLTEXT KEY `FT_bookshops_1` (`number`,`name`,`designation`),
  CONSTRAINT `FK_bookshops_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT `FK_bookshops_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
  CONSTRAINT `FK_bookshops_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_alias`
--

-- DROP TABLE IF EXISTS `cadlib_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT 'cadlib',
  `uid` varchar(128) NOT NULL DEFAULT 'cadlib',
  `designation` varchar(128) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '45c84a5zalias',
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `U_cadlib_alias_1` (`uid`),
  KEY `K_cadlib_alias_1` (`container_id`),
  KEY `K_cadlib_alias_2` (`number`),
  KEY `K_cadlib_alias_3` (`cid`),
  KEY `K_cadlib_alias_4` (`name`),
  CONSTRAINT `FK_cadlib_alias_1` FOREIGN KEY (`container_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_category_rel`
--

-- DROP TABLE IF EXISTS `cadlib_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_cadlib_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_cadlib_category_rel_1` (`parent_id`),
  KEY `K_cadlib_category_rel_2` (`parent_uid`),
  KEY `K_cadlib_category_rel_3` (`parent_cid`),
  KEY `K_cadlib_category_rel_4` (`child_id`),
  KEY `K_cadlib_category_rel_5` (`child_uid`),
  KEY `K_cadlib_category_rel_6` (`child_cid`),
  KEY `K_cadlib_category_rel_7` (`rdn`),
  CONSTRAINT `FK_cadlib_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_files`
--

-- DROP TABLE IF EXISTS `cadlib_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(255) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  UNIQUE KEY `INDEX_cadlib_doc_files_uid` (`uid`),
  KEY `FK_cadlib_doc_files_1` (`document_id`),
  KEY `IK_cadlib_doc_files_1` (`document_id`),
  KEY `IK_cadlib_doc_files_2` (`name`),
  KEY `IK_cadlib_doc_files_3` (`path`(128)),
  KEY `IK_cadlib_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_cadlib_doc_files_5` (`mainrole`),
  KEY `IK_cadlib_doc_files_6` (`roles`(32)),
  KEY `IK_cadlib_doc_files_8` (`document_uid`),
  KEY `FK_cadlib_docfiles_7` (`create_by_uid`),
  KEY `FK_cadlib_docfiles_8` (`lock_by_uid`),
  KEY `FK_cadlib_docfiles_9` (`update_by_uid`),
  KEY `FK_cadlib_docfiles_10` (`updated`),
  KEY `FK_cadlib_docfiles_11` (`created`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_cadlib_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `cadlib_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_files_versions`
--

-- DROP TABLE IF EXISTS `cadlib_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`(128)),
  KEY `IK_cadlib_doc_files_version_1` (`document_id`),
  KEY `IK_cadlib_doc_files_version_2` (`name`),
  KEY `IK_cadlib_doc_files_version_3` (`path`(128)),
  KEY `IK_cadlib_doc_files_version_4` (`iteration`,`document_id`),
  KEY `of_file_id` (`of_file_id`),
  KEY `IK_cadlib_doc_files_version_5` (`mainrole`),
  KEY `IK_cadlib_doc_files_version_6` (`roles`(32)),
  KEY `IK_cadlib_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_rel`
--

-- DROP TABLE IF EXISTS `cadlib_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_rel` (
  `link_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `hash` char(32) DEFAULT NULL,
  `data` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `FK_cadlib_doc_rel_10` (`parent_id`),
  KEY `FK_cadlib_doc_rel_2` (`child_id`),
  CONSTRAINT `FK_cadlib_doc_rel_1` FOREIGN KEY (`parent_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_doc_rel_2` FOREIGN KEY (`child_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doccomments`
--

-- DROP TABLE IF EXISTS `cadlib_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doctype_rel`
--

-- DROP TABLE IF EXISTS `cadlib_doctype_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_cadlib_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_cadlib_doctype_rel_1` (`parent_id`),
  KEY `K_cadlib_doctype_rel_2` (`parent_uid`),
  KEY `K_cadlib_doctype_rel_3` (`parent_cid`),
  KEY `K_cadlib_doctype_rel_4` (`child_id`),
  KEY `K_cadlib_doctype_rel_5` (`child_uid`),
  KEY `K_cadlib_doctype_rel_6` (`child_cid`),
  KEY `K_cadlib_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_cadlib_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_documents`
--

-- DROP TABLE IF EXISTS `cadlib_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(255) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `branch_id` int(1) DEFAULT '1',
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'cadlib',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tags` varchar(256) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `fabricant_a` varchar(255) DEFAULT NULL,
  `fabricant_b` varchar(255) DEFAULT NULL,
  `fabricant_c` varchar(255) DEFAULT NULL,
  `ref_a` varchar(255) DEFAULT NULL,
  `ref_b` varchar(255) DEFAULT NULL,
  `ref_c` varchar(255) DEFAULT NULL,
  `chapitre` varchar(255) DEFAULT NULL,
  `subchapitre` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_cadlib_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_cadlib_documents_uid` (`uid`),
  KEY `FK_cadlib_documents_1` (`cad_model_supplier_id`),
  KEY `FK_cadlib_documents_2` (`supplier_id`),
  KEY `FK_cadlib_documents_3` (`container_id`),
  KEY `FK_cadlib_documents_4` (`category_id`),
  KEY `FK_cadlib_documents_5` (`doctype_id`),
  KEY `FK_cadlib_documents_6` (`version`),
  KEY `INDEX_cadlib_documents_spacename` (`spacename`),
  KEY `INDEX_cadlib_documents_name` (`name`),
  KEY `INDEX_cadlib_documents_cuid` (`container_uid`),
  KEY `FK_cadlib_documents_7` (`create_by_uid`),
  KEY `FK_cadlib_documents_8` (`lock_by_uid`),
  KEY `FK_cadlib_documents_9` (`update_by_uid`),
  KEY `FK_cadlib_documents_10` (`as_template`),
  KEY `FK_cadlib_documents_11` (`tags`),
  KEY `FK_cadlib_documents_12` (`branch_id`),
  FULLTEXT KEY `FT_cadlib_documents` (`name`,`number`,`designation`,`tags`),
  CONSTRAINT `FK_cadlib_documents_1` FOREIGN KEY (`container_id`) REFERENCES `cadlibs` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_documents_3` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_documents_history`
--

-- DROP TABLE IF EXISTS `cadlib_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(256) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `action_batch_uid` varchar(128) DEFAULT NULL,
  `action_by` varchar(32) NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`),
  KEY `K_cadlib_documents_history_documentid` (`document_id`),
  KEY `K_cadlib_documents_history_containerid` (`container_id`),
  KEY `K_cadlib_documents_history_number` (`number`),
  KEY `K_cadlib_documents_history_batchuid` (`action_batch_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_files`
--

-- DROP TABLE IF EXISTS `cadlib_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` mediumtext NOT NULL,
  `file_root_name` varchar(128) NOT NULL DEFAULT '',
  `file_extension` varchar(16) DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) NOT NULL DEFAULT 'init',
  `file_type` varchar(128) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_cadlib_files_1` (`cadlib_id`),
  KEY `FK_cadlib_files_2` (`import_order`),
  KEY `index_file_name` (`file_name`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_import_history`
--

-- DROP TABLE IF EXISTS `cadlib_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_metadata`
--

-- DROP TABLE IF EXISTS `cadlib_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `maybenull` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_process_rel`
--

-- DROP TABLE IF EXISTS `cadlib_process_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_process_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_process_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_cadlib_process_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_process_rel_1` (`parent_id`),
  KEY `K_project_process_rel_2` (`parent_uid`),
  KEY `K_project_process_rel_3` (`parent_cid`),
  KEY `K_project_process_rel_4` (`child_id`),
  KEY `K_project_process_rel_5` (`child_uid`),
  KEY `K_project_process_rel_6` (`child_cid`),
  KEY `K_project_process_rel_7` (`rdn`),
  KEY `K_cadlib_process_rel_1` (`parent_id`),
  KEY `K_cadlib_process_rel_2` (`parent_uid`),
  KEY `K_cadlib_process_rel_3` (`parent_cid`),
  KEY `K_cadlib_process_rel_4` (`child_id`),
  KEY `K_cadlib_process_rel_5` (`child_uid`),
  KEY `K_cadlib_process_rel_6` (`child_cid`),
  KEY `K_cadlib_process_rel_7` (`rdn`),
  CONSTRAINT `FK_cadlib_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_property_rel`
--

-- DROP TABLE IF EXISTS `cadlib_property_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_cadlib_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_cadlib_property_rel_1` (`parent_id`),
  KEY `K_cadlib_property_rel_2` (`parent_uid`),
  KEY `K_cadlib_property_rel_3` (`parent_cid`),
  KEY `K_cadlib_property_rel_4` (`child_id`),
  KEY `K_cadlib_property_rel_5` (`child_uid`),
  KEY `K_cadlib_property_rel_6` (`child_cid`),
  KEY `K_cadlib_property_rel_7` (`rdn`),
  CONSTRAINT `FK_cadlib_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `cadlib_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlibs`
--

-- DROP TABLE IF EXISTS `cadlibs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlibs` LIKE `workitems`;
ALTER TABLE `cadlibs` 
  CHANGE COLUMN `spacename` `spacename` VARCHAR(16) NOT NULL DEFAULT 'cadlib',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_cadlibs_number` (`number`),
  UNIQUE KEY `UC_cadlibs_uid` (`uid`),
  KEY `K_cadlibs_create_by_uid` (`create_by_uid`),
  KEY `K_cadlibs_close_by_uid` (`close_by_uid`),
  KEY `K_cadlibs_version` (`version`),
  KEY `K_cadlibs_parent_id` (`parent_id`),
  KEY `K_cadlibs_default_process_id` (`default_process_id`),
  KEY `K_cadlibs_name` (`name`),
  KEY `K_cadlibs_dn` (`dn`),
  KEY `K_cadlibs_reposit_uid` (`reposit_uid`),
  KEY `K_cadlibs_reposit_id` (`reposit_id`),
  FULLTEXT KEY `FT_cadlibs_1` (`number`,`name`,`designation`),
  CONSTRAINT `FK_cadlibs_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT `FK_cadlibs_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
  CONSTRAINT `FK_cadlibs_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `callbacks`
--

-- DROP TABLE IF EXISTS `callbacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(128) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `reference_id` varchar(128) DEFAULT NULL,
  `reference_uid` varchar(128) DEFAULT NULL,
  `reference_cid` varchar(128) DEFAULT NULL,
  `spacename` varchar(64) DEFAULT NULL,
  `events` varchar(256) DEFAULT NULL,
  `callback_class` varchar(256) DEFAULT NULL,
  `callback_method` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_callbacks_1` (`uid`),
  KEY `INDEX_callbacks_1` (`uid`),
  KEY `INDEX_callbacks_2` (`cid`),
  KEY `INDEX_callbacks_3` (`reference_id`),
  KEY `INDEX_callbacks_4` (`reference_uid`),
  KEY `INDEX_callbacks_5` (`reference_cid`),
  KEY `INDEX_callbacks_8` (`events`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories`
--

-- DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` char(13) NOT NULL DEFAULT '569e918a134ca',
  `dn` varchar(128) DEFAULT NULL,
  `name` varchar(512) DEFAULT NULL,
  `nodelabel` varchar(64) DEFAULT NULL,
  `designation` text,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(32) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_categories_uid` (`uid`),
  UNIQUE KEY `UC_categories_dn` (`dn`),
  KEY `K_categories_parent_id` (`parent_id`),
  KEY `K_categories_parent_uid` (`parent_uid`),
  KEY `K_categories_name` (`name`),
  KEY `K_categories_nodelabel` (`nodelabel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `checkout_index`
--

-- DROP TABLE IF EXISTS `checkout_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkout_index` (
  `file_name` varchar(128) NOT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `file_uid` varchar(140) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `spacename` varchar(128) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `container_number` varchar(128) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_name`),
  KEY `IK_checkout_index_1` (`lock_by_id`),
  KEY `IK_checkout_index_2` (`lock_by_uid`),
  KEY `IK_checkout_index_3` (`file_id`),
  KEY `IK_checkout_index_4` (`file_uid`),
  KEY `IK_checkout_index_5` (`spacename`),
  KEY `IK_checkout_index_6` (`container_id`),
  KEY `IK_checkout_index_7` (`container_number`),
  KEY `IK_checkout_index_8` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_favorite`
--

-- DROP TABLE IF EXISTS `container_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_favorite` (
  `user_id` int(11) NOT NULL,
  `container_id` int(11) NOT NULL,
  `container_number` varchar(512) NOT NULL,
  `space_id` int(11) NOT NULL,
  `spacename` varchar(16) NOT NULL,
  PRIMARY KEY (`user_id`,`container_id`,`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_indice`
--

-- DROP TABLE IF EXISTS `container_indice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_indice` (
  `indice_id` int(11) NOT NULL DEFAULT '0',
  `indice_value` varchar(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`indice_id`),
  UNIQUE KEY `UC_indice_id` (`indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cron_task`
--

-- DROP TABLE IF EXISTS `cron_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cron_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(258) DEFAULT NULL,
  `description` varchar(258) DEFAULT NULL,
  `callback_class` text,
  `callback_method` varchar(64) DEFAULT NULL,
  `callback_params` text,
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(128) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `exec_frequency` decimal(10,0) DEFAULT NULL,
  `exec_schedule_start` decimal(10,0) DEFAULT NULL,
  `exec_schedule_end` decimal(10,0) DEFAULT NULL,
  `last_exec` datetime DEFAULT NULL,
  `is_actif` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `K_cron_task_1` (`name`),
  KEY `K_cron_task_2` (`owner_id`),
  KEY `K_cron_task_3` (`owner_uid`),
  KEY `K_cron_task_4` (`status`),
  KEY `K_cron_task_5` (`created`),
  KEY `K_cron_task_6` (`exec_schedule_start`),
  KEY `K_cron_task_7` (`exec_schedule_end`),
  KEY `K_cron_task_8` (`exec_frequency`),
  KEY `K_cron_task_9` (`is_actif`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discussion_comment`
--

-- DROP TABLE IF EXISTS `discussion_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` char(16) NOT NULL DEFAULT '568be4fc7a0a8',
  `name` varchar(255) DEFAULT NULL,
  `discussion_uid` varchar(255) NOT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(255) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `body` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `DISCUSSION_COMMENT_PARENTID` (`parent_id`),
  KEY `DISCUSSION_COMMENT_PARENTUID` (`parent_uid`),
  KEY `DISCUSSION_COMMENT_DISCUSSIONUID` (`discussion_uid`),
  KEY `DISCUSSION_COMMENT_OWNERID` (`owner_id`),
  KEY `DISCUSSION_COMMENT_UPDATED` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `docbasket`
--

-- DROP TABLE IF EXISTS `docbasket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docbasket` (
  `user_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `space_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`document_id`,`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `docseeder_mask`
--

-- DROP TABLE IF EXISTS `docseeder_mask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docseeder_mask` (
  `id` int(11) NOT NULL,
  `doctype_id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `map` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `doctype_id` (`doctype_id`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `name` (`name`),
  CONSTRAINT `FK_docseeder_mask_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `docseeder_type`
--

-- DROP TABLE IF EXISTS `docseeder_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docseeder_type` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  `designation` varchar(256) DEFAULT NULL,
  `domain` varchar(64) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL COMMENT 'id of the template docfile',
  `template_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the template docfile',
  `mask_id` int(11) DEFAULT NULL COMMENT 'id of the mask to use',
  `mask_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the mask',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `template_id` (`template_id`),
  KEY `template_uid` (`template_uid`),
  KEY `mask_id` (`mask_id`),
  KEY `mask_uid` (`mask_uid`),
  KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `doctypes`
--

-- DROP TABLE IF EXISTS `doctypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctypes` (
  `id` int(11) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `number` varchar(128) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `can_be_composite` tinyint(4) NOT NULL DEFAULT '1',
  `file_extensions` varchar(256) DEFAULT NULL,
  `file_type` varchar(128) DEFAULT NULL,
  `priority` int(5) NOT NULL DEFAULT '0',
  `icon` varchar(32) DEFAULT NULL,
  `recognition_regexp` mediumtext,
  `visu_file_extension` varchar(32) DEFAULT NULL,
  `pre_store_class` varchar(64) DEFAULT NULL,
  `pre_store_method` varchar(64) DEFAULT NULL,
  `post_store_class` varchar(64) DEFAULT NULL,
  `post_store_method` varchar(64) DEFAULT NULL,
  `pre_update_class` varchar(64) DEFAULT NULL,
  `pre_update_method` varchar(64) DEFAULT NULL,
  `post_update_class` varchar(64) DEFAULT NULL,
  `post_update_method` varchar(64) DEFAULT NULL,
  `number_generator_class` varchar(128) DEFAULT NULL,
  `domain` varchar(64) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL COMMENT 'id of the template docfile',
  `template_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the template docfile',
  `mask_id` int(11) DEFAULT NULL COMMENT 'id of the mask to use',
  `mask_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the mask',
  `docseeder` tinyint(1) DEFAULT NULL COMMENT 'flag to retrieve the valid type for docseeder',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_doctype_number` (`number`),
  UNIQUE KEY `INDEX_doctypes_1` (`uid`),
  UNIQUE KEY `number` (`number`),
  KEY `INDEX_doctypes_2` (`number`),
  KEY `INDEX_doctypes_3` (`designation`),
  KEY `template_id` (`template_id`),
  KEY `template_uid` (`template_uid`),
  KEY `mask_id` (`mask_id`),
  KEY `mask_uid` (`mask_uid`),
  KEY `domain` (`domain`),
  KEY `docseeder` (`docseeder`),
  KEY `INDEX_doctypes_4` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document_indice`
--

-- DROP TABLE IF EXISTS `document_indice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_indice` (
  `indice_id` int(11) NOT NULL DEFAULT '0',
  `indice_value` varchar(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`indice_id`),
  UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `indexer`
--

-- DROP TABLE IF EXISTS `indexer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indexer` (
  `document_id` int(11) NOT NULL,
  `docfile_id` int(11) NOT NULL,
  `spacename` varchar(32) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `content` mediumtext,
  PRIMARY KEY (`docfile_id`,`spacename`),
  UNIQUE KEY `UK_indexer_1` (`spacename`,`docfile_id`),
  KEY `UK_indexer_2` (`spacename`),
  KEY `I_indexer_3` (`document_id`,`spacename`),
  FULLTEXT KEY `content` (`content`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_archive`
--

-- DROP TABLE IF EXISTS `message_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_archive` (
  `id` int(14) NOT NULL,
  `ownerUid` varchar(128) NOT NULL,
  `created` datetime DEFAULT NULL,
  `from` varchar(200) NOT NULL,
  `to` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `hash` varchar(32) DEFAULT NULL,
  `replyto_hash` varchar(32) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `isReplied` tinyint(1) DEFAULT '0',
  `isFlagged` tinyint(1) DEFAULT '0',
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerUid` (`ownerUid`),
  KEY `isRead` (`isRead`),
  KEY `isReplied` (`isReplied`),
  KEY `isFlagged` (`isFlagged`),
  KEY `subject` (`subject`),
  FULLTEXT KEY `body` (`body`,`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_mailbox`
--

-- DROP TABLE IF EXISTS `message_mailbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_mailbox` (
  `id` int(14) NOT NULL,
  `ownerUid` varchar(128) NOT NULL,
  `created` datetime DEFAULT NULL,
  `from` varchar(200) NOT NULL,
  `to` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `hash` varchar(32) DEFAULT NULL,
  `replyto_hash` varchar(32) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `isReplied` tinyint(1) DEFAULT '0',
  `isFlagged` tinyint(1) DEFAULT '0',
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerUid` (`ownerUid`),
  KEY `isRead` (`isRead`),
  KEY `isReplied` (`isReplied`),
  KEY `isFlagged` (`isFlagged`),
  KEY `subject` (`subject`),
  FULLTEXT KEY `body` (`body`,`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_sent`
--

-- DROP TABLE IF EXISTS `message_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_sent` (
  `id` int(14) NOT NULL,
  `ownerUid` varchar(128) NOT NULL,
  `created` datetime DEFAULT NULL,
  `from` varchar(200) NOT NULL,
  `to` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `hash` varchar(32) DEFAULT NULL,
  `replyto_hash` varchar(32) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `isReplied` tinyint(1) DEFAULT '0',
  `isFlagged` tinyint(1) DEFAULT '0',
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerUid` (`ownerUid`),
  KEY `isRead` (`isRead`),
  KEY `isReplied` (`isReplied`),
  KEY `isFlagged` (`isFlagged`),
  KEY `subject` (`subject`),
  FULLTEXT KEY `body` (`body`,`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_alias`
--

-- DROP TABLE IF EXISTS `mockup_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT 'mockup',
  `uid` varchar(128) NOT NULL DEFAULT 'mockup',
  `designation` varchar(128) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '45c84a5zalias',
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `U_mockup_alias_1` (`uid`),
  KEY `K_mockup_alias_1` (`container_id`),
  KEY `K_mockup_alias_2` (`number`),
  KEY `K_mockup_alias_3` (`cid`),
  KEY `K_mockup_alias_4` (`name`),
  CONSTRAINT `FK_mockup_alias_1` FOREIGN KEY (`container_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_category_rel`
--

-- DROP TABLE IF EXISTS `mockup_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_mockup_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_mockup_category_rel_1` (`parent_id`),
  KEY `K_mockup_category_rel_2` (`parent_uid`),
  KEY `K_mockup_category_rel_3` (`parent_cid`),
  KEY `K_mockup_category_rel_4` (`child_id`),
  KEY `K_mockup_category_rel_5` (`child_uid`),
  KEY `K_mockup_category_rel_6` (`child_cid`),
  KEY `K_mockup_category_rel_7` (`rdn`),
  CONSTRAINT `FK_mockup_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_files`
--

-- DROP TABLE IF EXISTS `mockup_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(255) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  UNIQUE KEY `INDEX_mockup_doc_files_uid` (`uid`),
  KEY `FK_mockup_doc_files_1` (`document_id`),
  KEY `IK_mockup_doc_files_1` (`document_id`),
  KEY `IK_mockup_doc_files_2` (`name`),
  KEY `IK_mockup_doc_files_3` (`path`(128)),
  KEY `IK_mockup_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_mockup_doc_files_5` (`mainrole`),
  KEY `IK_mockup_doc_files_6` (`roles`(32)),
  KEY `IK_mockup_doc_files_8` (`document_uid`),
  KEY `FK_mockup_docfiles_7` (`create_by_uid`),
  KEY `FK_mockup_docfiles_8` (`lock_by_uid`),
  KEY `FK_mockup_docfiles_9` (`update_by_uid`),
  KEY `FK_mockup_docfiles_10` (`updated`),
  KEY `FK_mockup_docfiles_11` (`created`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_mockup_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `mockup_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_files_versions`
--

-- DROP TABLE IF EXISTS `mockup_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`(128)),
  KEY `IK_mockup_doc_files_version_1` (`document_id`),
  KEY `IK_mockup_doc_files_version_2` (`name`),
  KEY `IK_mockup_doc_files_version_3` (`path`(128)),
  KEY `IK_mockup_doc_files_version_4` (`iteration`,`document_id`),
  KEY `of_file_id` (`of_file_id`),
  KEY `IK_mockup_doc_files_version_5` (`mainrole`),
  KEY `IK_mockup_doc_files_version_6` (`roles`(32)),
  KEY `IK_mockup_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_rel`
--

-- DROP TABLE IF EXISTS `mockup_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_rel` (
  `link_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `hash` char(32) DEFAULT NULL,
  `data` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `FK_mockup_doc_rel_10` (`parent_id`),
  KEY `FK_mockup_doc_rel_2` (`child_id`),
  CONSTRAINT `FK_mockup_doc_rel_1` FOREIGN KEY (`parent_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_doc_rel_2` FOREIGN KEY (`child_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doccomments`
--

-- DROP TABLE IF EXISTS `mockup_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doctype_process`
--

-- DROP TABLE IF EXISTS `mockup_doctype_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `mockup_doctype_process_uniq1` (`mockup_id`,`doctype_id`),
  KEY `FK_mockup_doctype_process_1` (`category_id`),
  KEY `FK_mockup_doctype_process_3` (`doctype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doctype_rel`
--

-- DROP TABLE IF EXISTS `mockup_doctype_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_mockup_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_mockup_doctype_rel_1` (`parent_id`),
  KEY `K_mockup_doctype_rel_2` (`parent_uid`),
  KEY `K_mockup_doctype_rel_3` (`parent_cid`),
  KEY `K_mockup_doctype_rel_4` (`child_id`),
  KEY `K_mockup_doctype_rel_5` (`child_uid`),
  KEY `K_mockup_doctype_rel_6` (`child_cid`),
  KEY `K_mockup_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_mockup_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_documents`
--

-- DROP TABLE IF EXISTS `mockup_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(255) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `branch_id` int(1) DEFAULT '1',
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'mockup',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tags` varchar(256) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_mockup_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_mockup_documents_uid` (`uid`),
  KEY `FK_mockup_documents_1` (`cad_model_supplier_id`),
  KEY `FK_mockup_documents_2` (`supplier_id`),
  KEY `FK_mockup_documents_3` (`container_id`),
  KEY `FK_mockup_documents_4` (`category_id`),
  KEY `FK_mockup_documents_5` (`doctype_id`),
  KEY `FK_mockup_documents_6` (`version`),
  KEY `INDEX_mockup_documents_spacename` (`spacename`),
  KEY `INDEX_mockup_documents_name` (`name`),
  KEY `INDEX_mockup_documents_cuid` (`container_uid`),
  KEY `FK_mockup_documents_7` (`create_by_uid`),
  KEY `FK_mockup_documents_8` (`lock_by_uid`),
  KEY `FK_mockup_documents_9` (`update_by_uid`),
  KEY `FK_mockup_documents_10` (`as_template`),
  KEY `FK_mockup_documents_11` (`tags`),
  KEY `FK_mockup_documents_12` (`branch_id`),
  FULLTEXT KEY `FT_mockup_documents` (`name`,`number`,`designation`,`tags`),
  CONSTRAINT `FK_mockup_documents_1` FOREIGN KEY (`container_id`) REFERENCES `mockups` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_documents_3` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_documents_history`
--

-- DROP TABLE IF EXISTS `mockup_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(256) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `action_batch_uid` varchar(128) DEFAULT NULL,
  `action_by` varchar(32) NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`),
  KEY `K_mockup_documents_history_documentid` (`document_id`),
  KEY `K_mockup_documents_history_containerid` (`container_id`),
  KEY `K_mockup_documents_history_number` (`number`),
  KEY `K_mockup_documents_history_batchuid` (`action_batch_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_files`
--

-- DROP TABLE IF EXISTS `mockup_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` mediumtext NOT NULL,
  `file_root_name` varchar(128) NOT NULL DEFAULT '',
  `file_extension` varchar(16) DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) NOT NULL DEFAULT 'init',
  `file_type` varchar(128) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_mockup_files_1` (`mockup_id`),
  KEY `FK_mockup_files_2` (`import_order`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`),
  KEY `index_file_name` (`file_name`),
  KEY `file_md5` (`file_md5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_import_history`
--

-- DROP TABLE IF EXISTS `mockup_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` mediumtext,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_metadata`
--

-- DROP TABLE IF EXISTS `mockup_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `maybenull` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_process_rel`
--

-- DROP TABLE IF EXISTS `mockup_process_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_process_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_process_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_mockup_process_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_process_rel_1` (`parent_id`),
  KEY `K_project_process_rel_2` (`parent_uid`),
  KEY `K_project_process_rel_3` (`parent_cid`),
  KEY `K_project_process_rel_4` (`child_id`),
  KEY `K_project_process_rel_5` (`child_uid`),
  KEY `K_project_process_rel_6` (`child_cid`),
  KEY `K_project_process_rel_7` (`rdn`),
  KEY `K_mockup_process_rel_1` (`parent_id`),
  KEY `K_mockup_process_rel_2` (`parent_uid`),
  KEY `K_mockup_process_rel_3` (`parent_cid`),
  KEY `K_mockup_process_rel_4` (`child_id`),
  KEY `K_mockup_process_rel_5` (`child_uid`),
  KEY `K_mockup_process_rel_6` (`child_cid`),
  KEY `K_mockup_process_rel_7` (`rdn`),
  CONSTRAINT `FK_mockup_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_property_rel`
--

-- DROP TABLE IF EXISTS `mockup_property_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_mockup_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_mockup_property_rel_1` (`parent_id`),
  KEY `K_mockup_property_rel_2` (`parent_uid`),
  KEY `K_mockup_property_rel_3` (`parent_cid`),
  KEY `K_mockup_property_rel_4` (`child_id`),
  KEY `K_mockup_property_rel_5` (`child_uid`),
  KEY `K_mockup_property_rel_6` (`child_cid`),
  KEY `K_mockup_property_rel_7` (`rdn`),
  CONSTRAINT `FK_mockup_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `mockup_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockups`
--

-- DROP TABLE IF EXISTS `mockups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockups` LIKE `workitems`;
ALTER TABLE `mockups` 
  CHANGE COLUMN `spacename` `spacename` VARCHAR(16) NOT NULL DEFAULT 'mockup',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_mockups_number` (`number`),
  UNIQUE KEY `UC_mockups_uid` (`uid`),
  KEY `K_mockups_create_by_uid` (`create_by_uid`),
  KEY `K_mockups_close_by_uid` (`close_by_uid`),
  KEY `K_mockups_version` (`version`),
  KEY `K_mockups_parent_id` (`parent_id`),
  KEY `K_mockups_default_process_id` (`default_process_id`),
  KEY `K_mockups_name` (`name`),
  KEY `K_mockups_dn` (`dn`),
  KEY `K_mockups_reposit_uid` (`reposit_uid`),
  KEY `K_mockups_reposit_id` (`reposit_id`),
  FULLTEXT KEY `FT_mockups_1` (`number`,`name`,`designation`),
  CONSTRAINT `FK_mockups_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT `FK_mockups_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
  CONSTRAINT `FK_mockups_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notifications`
--

-- DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(32) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `owner_uid` varchar(128) NOT NULL,
  `owner_id` varchar(128) NOT NULL,
  `reference_uid` varchar(128) NOT NULL,
  `reference_cid` varchar(128) DEFAULT NULL,
  `reference_id` varchar(128) DEFAULT NULL,
  `spacename` varchar(64) NOT NULL,
  `events` varchar(256) NOT NULL,
  `condition` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_notifications_1` (`owner_id`,`reference_uid`,`events`),
  UNIQUE KEY `uid` (`uid`),
  KEY `INDEX_notifications_1` (`owner_id`),
  KEY `INDEX_notifications_2` (`reference_uid`),
  KEY `INDEX_notifications_3` (`events`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partners`
--

-- DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL DEFAULT '',
  `type` enum('customer','supplier','staff') DEFAULT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `adress` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `cell_phone` varchar(64) DEFAULT NULL,
  `mail` varchar(64) DEFAULT NULL,
  `web_site` varchar(64) DEFAULT NULL,
  `activity` varchar(64) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_partner_number` (`number`),
  KEY `IK_partner_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pdm_product_instance`
--

-- DROP TABLE IF EXISTS `pdm_product_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdm_product_instance` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL DEFAULT '569e971bb57c0',
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `nomenclature` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `position` varchar(512) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `of_product_id` int(11) DEFAULT NULL,
  `of_product_uid` varchar(32) DEFAULT NULL,
  `of_product_number` varchar(128) DEFAULT NULL,
  `context_id` varchar(32) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_pdm_product_instance_1` (`uid`),
  UNIQUE KEY `U_pdm_product_instance_2` (`number`,`parent_id`),
  KEY `INDEX_pdm_product_instance_1` (`cid`),
  KEY `INDEX_pdm_product_instance_2` (`number`),
  KEY `INDEX_pdm_product_instance_3` (`description`),
  KEY `INDEX_pdm_product_instance_4` (`nomenclature`),
  KEY `INDEX_pdm_product_instance_5` (`uid`),
  KEY `INDEX_pdm_product_instance_6` (`name`),
  KEY `INDEX_pdm_product_instance_7` (`type`),
  KEY `INDEX_pdm_product_instance_8` (`of_product_uid`),
  KEY `INDEX_pdm_product_instance_9` (`of_product_id`),
  KEY `INDEX_pdm_product_instance_10` (`path`),
  KEY `INDEX_pdm_product_version_12` (`of_product_number`),
  KEY `FK_pdm_product_instance_2` (`parent_id`),
  CONSTRAINT `FK_pdm_product_instance_1` FOREIGN KEY (`of_product_id`) REFERENCES `pdm_product_version` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_pdm_product_instance_2` FOREIGN KEY (`parent_id`) REFERENCES `pdm_product_version` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pdm_product_version`
--

-- DROP TABLE IF EXISTS `pdm_product_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdm_product_version` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `of_product_id` int(11) DEFAULT NULL,
  `of_product_uid` varchar(32) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `document_uid` varchar(32) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `spacename` varchar(32) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `materials` varchar(512) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `volume` float DEFAULT NULL,
  `wetsurface` float DEFAULT NULL,
  `density` float DEFAULT NULL,
  `gravitycenter` varchar(512) DEFAULT NULL,
  `inertiacenter` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_pdm_product_version_1` (`uid`),
  UNIQUE KEY `U_pdm_product_version_2` (`number`),
  UNIQUE KEY `U_pdm_product_version_3` (`of_product_uid`,`version`),
  KEY `INDEX_pdm_product_version_1` (`of_product_uid`),
  KEY `INDEX_pdm_product_version_6` (`of_product_id`),
  KEY `INDEX_pdm_product_version_2` (`version`),
  KEY `INDEX_pdm_product_version_3` (`description`),
  KEY `INDEX_pdm_product_version_4` (`uid`),
  KEY `INDEX_pdm_product_version_5` (`name`),
  KEY `INDEX_pdm_product_version_7` (`document_id`),
  KEY `INDEX_pdm_product_version_8` (`document_uid`),
  KEY `INDEX_pdm_product_version_9` (`spacename`),
  KEY `INDEX_pdm_product_version_10` (`type`),
  KEY `INDEX_pdm_product_version_11` (`create_by_uid`),
  KEY `INDEX_pdm_product_version_12` (`lock_by_uid`),
  KEY `INDEX_pdm_product_version_13` (`update_by_uid`),
  KEY `INDEX_pdm_product_version_14` (`created`),
  KEY `INDEX_pdm_product_version_15` (`locked`),
  KEY `INDEX_pdm_product_version_16` (`updated`),
  FULLTEXT KEY `FT_name` (`name`,`number`,`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `postit`
--

-- DROP TABLE IF EXISTS `postit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postit` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(32) DEFAULT NULL,
  `parent_cid` varchar(32) NOT NULL,
  `owner_id` varchar(32) DEFAULT NULL,
  `spacename` varchar(32) NOT NULL,
  `body` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_uid` (`uid`),
  KEY `K_parentId` (`parent_id`),
  KEY `K_parentUid` (`parent_uid`),
  KEY `K_parentCid` (`parent_cid`),
  KEY `K_spaceName` (`spacename`),
  KEY `K_ownerId` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_category_rel`
--

-- DROP TABLE IF EXISTS `project_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_category_rel_1` (`parent_id`),
  KEY `K_project_category_rel_2` (`parent_uid`),
  KEY `K_project_category_rel_3` (`parent_cid`),
  KEY `K_project_category_rel_4` (`child_id`),
  KEY `K_project_category_rel_5` (`child_uid`),
  KEY `K_project_category_rel_6` (`child_cid`),
  KEY `K_project_category_rel_7` (`rdn`),
  CONSTRAINT `FK_project_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_project_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_container_rel`
--

-- DROP TABLE IF EXISTS `project_container_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_container_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `spacename` varchar(64) NOT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_container_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_container_rel_1` (`parent_id`),
  KEY `K_project_container_rel_2` (`parent_uid`),
  KEY `K_project_container_rel_3` (`parent_cid`),
  KEY `K_project_container_rel_4` (`child_id`),
  KEY `K_project_container_rel_5` (`child_uid`),
  KEY `K_project_container_rel_6` (`child_cid`),
  KEY `K_project_container_rel_7` (`rdn`),
  CONSTRAINT `FK_project_container_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_project_container_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_doctype_rel`
--

-- DROP TABLE IF EXISTS `project_doctype_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  CONSTRAINT `U_project_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `U_project_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `U_project_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_process_rel`
--

-- DROP TABLE IF EXISTS `project_process_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_process_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_process_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  KEY `K_project_process_rel_1` (`parent_id`),
  KEY `K_project_process_rel_2` (`parent_uid`),
  KEY `K_project_process_rel_3` (`parent_cid`),
  KEY `K_project_process_rel_4` (`child_id`),
  KEY `K_project_process_rel_5` (`child_uid`),
  KEY `K_project_process_rel_6` (`child_cid`),
  KEY `K_project_process_rel_7` (`rdn`),
  CONSTRAINT `U_project_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `U_project_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `U_project_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_property_rel`
--

-- DROP TABLE IF EXISTS `project_property_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  CONSTRAINT `FK_project_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_project_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects`
--

-- DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `projects` (
	 `id` int(11) NOT NULL DEFAULT '0',
	 `uid` varchar(32) NOT NULL DEFAULT '',
	 `cid` VARCHAR(16) DEFAULT NOT NULL DEFAULT '569e93c6ee156',
	 `dn` VARCHAR(128) NULL,
	 `number` varchar(64) NOT NULL DEFAULT '',
	 `name` varchar(16) NOT NULL DEFAULT '',
	 `designation` TEXT DEFAULT NULL,
	 `parent_id` int(11) NULL,
	 `parent_uid` varchar(32) NULL,
	 `life_stage` varchar(16) NOT NULL DEFAULT 'init',
	 `acode` int(11) DEFAULT NULL,
	 `default_process_id` int(11) DEFAULT NULL,
	 `created` DATETIME DEFAULT NULL,
	 `create_by_id` int(11) DEFAULT NULL,
	 `create_by_uid` VARCHAR(64) DEFAULT NULL,
	 `closed` DATETIME DEFAULT NULL,
	 `close_by_id` int(11) DEFAULT NULL,
	 `close_by_uid` VARCHAR(64) DEFAULT NULL,
	 `planned_closure` DATETIME DEFAULT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `UC_project_uid` (`uid`),
	  UNIQUE KEY `UC_project_number` (`number`),
	  UNIQUE KEY `UC_project_dn` (`dn`),
	  KEY `K_project_name` (`name`),
	  KEY `K_project_parent_id` (`parent_id`),
	  KEY `K_project_parent_uid` (`parent_uid`),
	  KEY `K_projects_life_stage` (`life_stage`),
	  KEY `K_project_acode` (`acode`),
	  KEY `K_projects_create_by_uid` (`create_by_uid`),
	  KEY `K_projects_close_by_uid` (`close_by_uid`),
	  FULLTEXT KEY `number` (`number`,`name`,`designation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_version`
--

-- DROP TABLE IF EXISTS `schema_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version` (
  `version` varchar(64) DEFAULT NULL,
  `lastModification` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search`
--

-- DROP TABLE IF EXISTS `search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search` (
  `id` int(11) NOT NULL DEFAULT '0',
  `uid` varchar(140) DEFAULT NULL,
  `number` varchar(140) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` mediumtext,
  `version` varchar(11) DEFAULT NULL,
  `iteration` varchar(11) DEFAULT NULL,
  `doctypeId` bigint(20) DEFAULT NULL,
  `lockById` varchar(11) DEFAULT NULL,
  `locked` varchar(19) DEFAULT NULL,
  `updated` varchar(19) DEFAULT NULL,
  `updateById` varchar(11) DEFAULT NULL,
  `created` varchar(19) DEFAULT NULL,
  `createById` varchar(11) DEFAULT NULL,
  `spacename` varchar(32) DEFAULT NULL,
  `cid` varchar(13) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_update`
--

-- DROP TABLE IF EXISTS `search_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_update` (
  `updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sequence_rulesier1`
--

-- DROP TABLE IF EXISTS `sequence_rulesier1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence_rulesier1` (
  `sequence` int(11) NOT NULL,
  `parameter1` varchar(64) NOT NULL,
  `parameter2` varchar(64) NOT NULL,
  `parameter3` varchar(64) NOT NULL,
  UNIQUE KEY `parameter1` (`parameter1`,`parameter2`,`parameter3`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `share_public_url`
--

-- DROP TABLE IF EXISTS `share_public_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
 CREATE TABLE `share_public_url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `document_id` int(11) DEFAULT NULL,
  `spacename` varchar(32) DEFAULT NULL,
  `document_uid` varchar(255) DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_share_public_url_uid` (`uid`),
  KEY `K_share_public_url_document_id` (`document_id`),
  KEY `K_share_public_url_spacename` (`spacename`),
  KEY `K_share_public_url_document_uid` (`document_uid`),
  KEY `K_share_public_url_expiration_date` (`expiration_date`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spaces`
--

-- DROP TABLE IF EXISTS `spaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spaces` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) NOT NULL DEFAULT '',
  `cid` varchar(64) NOT NULL DEFAULT '64bdd156af045',
  `name` varchar(255) NOT NULL DEFAULT '',
  `designation` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tools_missing_files`
--

-- DROP TABLE IF EXISTS `tools_missing_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tools_missing_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) NOT NULL,
  `document_id` int(11) NOT NULL,
  `path` text NOT NULL,
  `name` varchar(128) NOT NULL,
  `spacename` varchar(64) DEFAULT NULL,
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `U_tools_missing_files_1` (`name`,`path`(128)),
  KEY `IK_tools_missing_files_2` (`name`),
  KEY `IK_tools_missing_files_3` (`path`(128)),
  KEY `IK_tools_missing_files_4` (`spacename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `typemimes`
--

-- DROP TABLE IF EXISTS `typemimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typemimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `extensions` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `name` (`name`),
  KEY `extensions` (`extensions`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_prefs`
--

-- DROP TABLE IF EXISTS `user_prefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_prefs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `css_sheet` varchar(32) DEFAULT NULL,
  `lang` varchar(32) DEFAULT NULL,
  `long_date_format` varchar(32) DEFAULT NULL,
  `short_date_format` varchar(32) DEFAULT NULL,
  `hour_format` varchar(32) DEFAULT NULL,
  `time_zone` varchar(32) DEFAULT NULL,
  `rbgateserver_url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_id` (`owner_id`),
  CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`owner_id`) REFERENCES `acl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_sessions`
--

-- DROP TABLE IF EXISTS `user_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_sessions` (
  `user_id` int(11) NOT NULL,
  `datas` longtext,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vault_distant_site`
--

-- DROP TABLE IF EXISTS `vault_distant_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vault_distant_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(32) NOT NULL,
  `cid` char(13) NOT NULL DEFAULT 'distantsite36',
  `name` varchar(512) DEFAULT NULL,
  `description` text,
  `method` varchar(32) DEFAULT NULL,
  `parameters` text,
  `auth_user` varchar(128) DEFAULT NULL,
  `auth_password` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_vault_distant_site_uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vault_replicated`
--

-- DROP TABLE IF EXISTS `vault_replicated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vault_replicated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(32) NOT NULL,
  `cid` char(13) NOT NULL DEFAULT 'repositsynchr',
  `name` varchar(512) DEFAULT NULL,
  `description` text,
  `reposit_id` int(11) DEFAULT NULL,
  `reposit_uid` varchar(32) DEFAULT NULL,
  `distantsite_id` int(11) DEFAULT NULL,
  `distantsite_uid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_vault_replicated_uid` (`uid`),
  KEY `K_vault_replicated_name_1` (`reposit_id`),
  KEY `K_vault_replicated_name_2` (`reposit_uid`),
  KEY `K_vault_replicated_name_3` (`distantsite_id`),
  KEY `K_vault_replicated_name_4` (`distantsite_uid`),
  KEY `fk_vault_replicated_1_idx` (`distantsite_id`,`distantsite_uid`),
  KEY `fk_vault_replicated_2_idx` (`reposit_id`,`reposit_uid`),
  CONSTRAINT `fk_vault_replicated_1` FOREIGN KEY (`distantsite_id`) REFERENCES `vault_distant_site` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_vault_replicated_2` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vault_reposit`
--

-- DROP TABLE IF EXISTS `vault_reposit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vault_reposit` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `number` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `default_file_path` varchar(512) NOT NULL,
  `type` int(2) NOT NULL,
  `mode` int(2) NOT NULL,
  `actif` int(2) NOT NULL DEFAULT '1',
  `priority` int(2) NOT NULL DEFAULT '1',
  `maxsize` int(11) NOT NULL DEFAULT '50000000',
  `maxcount` int(11) NOT NULL DEFAULT '50000',
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_vault_reposit_1` (`uid`),
  UNIQUE KEY `U_vault_reposit_2` (`number`),
  UNIQUE KEY `U_vault_reposit_3` (`default_file_path`),
  KEY `FK_vault_reposit_1` (`name`),
  KEY `FK_vault_reposit_2` (`description`),
  KEY `FK_vault_reposit_4` (`type`),
  KEY `FK_vault_reposit_5` (`mode`),
  KEY `FK_vault_reposit_6` (`actif`),
  KEY `FK_vault_reposit_7` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vault_reposit_seq`
--

-- DROP TABLE IF EXISTS `vault_reposit_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vault_reposit_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=976 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_activity`
--

-- DROP TABLE IF EXISTS `wf_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_activity` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) DEFAULT NULL,
  `cid` varchar(32) NOT NULL DEFAULT '56acc299ed0e4',
  `name` varchar(80) DEFAULT NULL,
  `normalizedName` varchar(255) DEFAULT NULL,
  `processId` int(11) DEFAULT NULL,
  `type` enum('start','end','split','switch','join','activity','standalone') DEFAULT NULL,
  `isAutorouted` tinyint(1) DEFAULT '0',
  `flowNum` int(10) DEFAULT NULL,
  `isInteractive` tinyint(1) DEFAULT '0',
  `lastModif` int(14) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `expirationTime` datetime DEFAULT NULL,
  `isAutomatic` tinyint(1) DEFAULT '0',
  `isComment` tinyint(1) DEFAULT '0',
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `progression` float DEFAULT NULL,
  `roles` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wf_activity_u_uid` (`uid`),
  KEY `name` (`name`),
  KEY `roles` (`roles`(255)),
  KEY `ownerId` (`ownerId`),
  KEY `parentId` (`parentId`),
  KEY `parentUid` (`parentUid`),
  KEY `FK_wf_activity_1` (`processId`),
  CONSTRAINT `FK_wf_activity_1` FOREIGN KEY (`processId`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_document_link`
--

-- DROP TABLE IF EXISTS `wf_document_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_document_link` (
  `uid` varchar(64) NOT NULL,
  `parentId` int(11) NOT NULL,
  `childId` int(11) NOT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `childUid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lindex` int(11) DEFAULT '0',
  `attributes` mediumtext,
  `spacename` varchar(64) NOT NULL,
  PRIMARY KEY (`parentId`,`childId`),
  UNIQUE KEY `uid` (`uid`),
  KEY `name` (`name`),
  KEY `lindex` (`lindex`),
  KEY `parentUid` (`parentUid`),
  KEY `childUid` (`childUid`),
  KEY `parentId` (`parentId`),
  KEY `childId` (`childId`),
  CONSTRAINT `FK_wf_document_link_1` FOREIGN KEY (`childId`) REFERENCES `wf_instance` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_instance`
--

-- DROP TABLE IF EXISTS `wf_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_instance` (
  `id` int(11) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT '56acc299ed1bd',
  `updated` datetime DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `processId` int(11) DEFAULT NULL,
  `started` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT 'No Name',
  `title` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `nextActivities` mediumtext,
  `nextUsers` mediumtext,
  `ended` datetime DEFAULT NULL,
  `status` varchar(64) DEFAULT NULL,
  `attributes` mediumtext,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wf_instance_u_uid` (`uid`),
  KEY `WFINST_status` (`status`),
  KEY `WFINST_processId` (`processId`),
  KEY `WFINST_uid` (`uid`),
  KEY `WFINST_cid` (`cid`),
  KEY `WFINST_parentId` (`parentId`),
  KEY `WFINST_parentUid` (`parentId`),
  CONSTRAINT `FK_wf_instance_1` FOREIGN KEY (`processId`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_instance_activity`
--

-- DROP TABLE IF EXISTS `wf_instance_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_instance_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT '56acc299ed22a',
  `name` varchar(128) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `instanceId` int(14) NOT NULL,
  `activityId` int(14) NOT NULL,
  `ownerId` varchar(64) DEFAULT NULL,
  `status` enum('running','completed') DEFAULT NULL,
  `type` enum('start','end','split','switch','join','activity','standalone') DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updateById` varchar(64) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(128) DEFAULT NULL,
  `started` datetime DEFAULT NULL,
  `ended` datetime DEFAULT NULL,
  `attributes` mediumtext,
  `comment` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `WFINSTACTIVITY_uid` (`uid`),
  KEY `WFINSTACTIVITY_name` (`name`),
  KEY `WFINSTACTIVITY_parentuid` (`parentUid`),
  KEY `WFINSTACTIVITY_parentid` (`parentId`),
  KEY `WFINSTACTIVITY_cid` (`cid`),
  KEY `WFINSTACTIVITY_ownerId` (`ownerId`),
  KEY `WFINSTACTIVITY_activityId` (`activityId`),
  KEY `WFINSTACTIVITY_status` (`status`),
  KEY `WFINSTACTIVITY_type` (`type`),
  KEY `WFINSTACTIVITY_started` (`started`),
  KEY `WFINSTACTIVITY_ended` (`ended`),
  KEY `fk_wf_instance_activity_2` (`instanceId`),
  CONSTRAINT `FK_wf_instance_activity_1` FOREIGN KEY (`activityId`) REFERENCES `wf_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_wf_instance_activity_2` FOREIGN KEY (`instanceId`) REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=537363 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_process`
--

-- DROP TABLE IF EXISTS `wf_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_process` (
  `id` int(11) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT '56acc299ecd6d',
  `name` varchar(80) DEFAULT NULL,
  `isValid` tinyint(1) DEFAULT '0',
  `isActive` tinyint(1) DEFAULT '0',
  `version` varchar(12) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `normalizedName` varchar(256) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wf_process_u_uid` (`uid`),
  UNIQUE KEY `wf_process_u_normalizedName` (`normalizedName`),
  KEY `wf_process_index_uid` (`uid`),
  KEY `wf_process_index_cid` (`cid`),
  KEY `wf_process_index_name` (`name`),
  KEY `wf_process_index_parentuid` (`parentUid`),
  KEY `wf_process_index_parentid` (`parentId`),
  KEY `wf_process_index_ownerId` (`ownerId`),
  KEY `wf_process_index_isValid` (`isValid`),
  KEY `wf_process_index_isActive` (`isActive`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_transition`
--

-- DROP TABLE IF EXISTS `wf_transition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_transition` (
  `uid` varchar(32) NOT NULL,
  `processId` int(14) NOT NULL,
  `parentId` int(14) NOT NULL,
  `parentUid` varchar(32) NOT NULL,
  `childId` int(14) NOT NULL,
  `childUid` varchar(32) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `lindex` int(7) DEFAULT '0',
  `attributes` mediumtext,
  `cid` varchar(32) NOT NULL DEFAULT '56acc299ed150',
  PRIMARY KEY (`parentId`,`childId`),
  UNIQUE KEY `wf_transition_u_uid` (`uid`),
  KEY `processId` (`processId`),
  KEY `parentId` (`parentId`),
  KEY `parentUid` (`parentUid`),
  KEY `childId` (`childId`),
  KEY `childUid` (`childUid`),
  KEY `lindex` (`lindex`),
  CONSTRAINT `FK_wf_transition_1` FOREIGN KEY (`parentId`) REFERENCES `wf_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wf_transition_2` FOREIGN KEY (`childId`) REFERENCES `wf_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_alias`
--

-- DROP TABLE IF EXISTS `workitem_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT 'workitem',
  `uid` varchar(128) NOT NULL DEFAULT 'workitem',
  `designation` varchar(128) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '45c84a5zalias',
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `U_workitem_alias_1` (`uid`),
  KEY `K_workitem_alias_1` (`container_id`),
  KEY `K_workitem_alias_2` (`number`),
  KEY `K_workitem_alias_3` (`cid`),
  KEY `K_workitem_alias_4` (`name`),
  CONSTRAINT `FK_workitem_alias_1` FOREIGN KEY (`container_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_category_rel`
--

-- DROP TABLE IF EXISTS `workitem_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_category_rel_1` (`parent_id`),
  KEY `K_project_category_rel_2` (`parent_uid`),
  KEY `K_project_category_rel_3` (`parent_cid`),
  KEY `K_project_category_rel_4` (`child_id`),
  KEY `K_project_category_rel_5` (`child_uid`),
  KEY `K_project_category_rel_6` (`child_cid`),
  KEY `K_project_category_rel_7` (`rdn`),
  CONSTRAINT `FK_workitem_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_files`
--

-- DROP TABLE IF EXISTS `workitem_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(260) DEFAULT NULL,
  `extension` varchar(32) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_file_name` (`name`,`path`),
  UNIQUE KEY `INDEX_workitem_doc_files_uid` (`uid`),
  KEY `IK_workitem_doc_files_1` (`document_id`),
  KEY `IK_workitem_doc_files_2` (`name`),
  KEY `IK_workitem_doc_files_3` (`path`(128)),
  KEY `IK_workitem_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_workitem_doc_files_5` (`mainrole`),
  KEY `IK_workitem_doc_files_6` (`roles`(32)),
  KEY `IK_workitem_doc_files_8` (`document_uid`),
  KEY `FK_workitem_docfiles_7` (`create_by_uid`),
  KEY `FK_workitem_docfiles_8` (`lock_by_uid`),
  KEY `FK_workitem_docfiles_9` (`update_by_uid`),
  KEY `FK_workitem_docfiles_10` (`updated`),
  KEY `FK_workitem_docfiles_11` (`created`),
  CONSTRAINT `FK_workitem_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `workitem_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_files_backup`
--

-- DROP TABLE IF EXISTS `workitem_doc_files_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files_backup` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(255) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_files_versions`
--

-- DROP TABLE IF EXISTS `workitem_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`(128)),
  KEY `IK_workitem_doc_files_version_1` (`document_id`),
  KEY `IK_workitem_doc_files_version_2` (`name`),
  KEY `IK_workitem_doc_files_version_3` (`path`(128)),
  KEY `IK_workitem_doc_files_version_4` (`iteration`,`document_id`),
  KEY `IK_workitem_doc_files_version_5` (`mainrole`),
  KEY `IK_workitem_doc_files_version_6` (`roles`(32)),
  KEY `IK_workitem_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_rel`
--

-- DROP TABLE IF EXISTS `workitem_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_rel` (
  `link_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `hash` char(32) DEFAULT NULL,
  `data` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `uniq_docid_ldoc_id` (`parent_id`,`child_id`),
  KEY `FK_workitem_doc_rel_2` (`child_id`),
  CONSTRAINT `FK_workitem_doc_rel_1` FOREIGN KEY (`parent_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_doc_rel_2` FOREIGN KEY (`child_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doccomments`
--

-- DROP TABLE IF EXISTS `workitem_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doctype_rel`
--

-- DROP TABLE IF EXISTS `workitem_doctype_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_workitem_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_workitem_doctype_rel_1` (`parent_id`),
  KEY `K_workitem_doctype_rel_2` (`parent_uid`),
  KEY `K_workitem_doctype_rel_3` (`parent_cid`),
  KEY `K_workitem_doctype_rel_4` (`child_id`),
  KEY `K_workitem_doctype_rel_5` (`child_uid`),
  KEY `K_workitem_doctype_rel_6` (`child_cid`),
  KEY `K_workitem_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_workitem_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_documents`
--

-- DROP TABLE IF EXISTS `workitem_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `branch_id` int(1) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'workitem',
  `doctype_id` int(11) NOT NULL DEFAULT '0',
  `default_process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tags` varchar(256) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `ci` varchar(255) DEFAULT NULL,
  `type_gc` varchar(255) DEFAULT NULL,
  `work_unit` varchar(255) DEFAULT NULL,
  `father_ds` varchar(255) DEFAULT NULL,
  `need_calcul` varchar(255) DEFAULT NULL,
  `ata` varchar(255) DEFAULT NULL,
  `receptionneur` varchar(32) DEFAULT NULL,
  `indice_client` varchar(255) DEFAULT NULL,
  `ext56b26_recept_date` datetime DEFAULT NULL,
  `observation` varchar(255) DEFAULT NULL,
  `ext56b26_date_de_remise` datetime DEFAULT NULL,
  `sub_ata` varchar(255) DEFAULT NULL,
  `work_package` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `fab_process` varchar(255) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `materiau_mak` varchar(255) DEFAULT NULL,
  `masse_mak` double DEFAULT NULL,
  `rt_name` varchar(255) DEFAULT NULL,
  `rt_type` varchar(255) DEFAULT NULL,
  `rt_reason` varchar(255) DEFAULT NULL,
  `ext56b26_rt_emitted` datetime DEFAULT NULL,
  `rt_apply_to` int(11) DEFAULT NULL,
  `modification` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_workitem_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_workitem_documents_uid` (`uid`),
  KEY `FK_workitem_documents_2` (`doctype_id`),
  KEY `FK_workitem_documents_3` (`version`),
  KEY `FK_workitem_documents_4` (`container_id`),
  KEY `FK_workitem_documents_5` (`category_id`),
  KEY `document_name` (`name`),
  KEY `INDEX_workitem_documents_spacename` (`spacename`),
  KEY `INDEX_workitem_documents_name` (`name`),
  KEY `INDEX_workitem_documents_cuid` (`container_uid`),
  KEY `FK_workitem_documents_7` (`create_by_uid`),
  KEY `FK_workitem_documents_8` (`lock_by_uid`),
  KEY `FK_workitem_documents_9` (`update_by_uid`),
  KEY `FK_workitem_documents_10` (`as_template`),
  KEY `FK_workitem_documents_11` (`tags`),
  KEY `FK_workitem_documents_12` (`branch_id`),
  FULLTEXT KEY `FT_workitem_documents` (`name`,`number`,`designation`,`tags`),
  CONSTRAINT `FK_workitem_documents_1` FOREIGN KEY (`container_id`) REFERENCES `workitems` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_documents_3` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_documents_history`
--

-- DROP TABLE IF EXISTS `workitem_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(256) NOT NULL DEFAULT '',
  `action_by` varchar(64) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `action_batch_uid` varchar(128) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`),
  KEY `K_workitem_documents_history_documentid` (`document_id`),
  KEY `K_workitem_documents_history_containerid` (`container_id`),
  KEY `K_workitem_documents_history_number` (`number`),
  KEY `K_workitem_documents_history_batchuid` (`action_batch_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_import_history`
--

-- DROP TABLE IF EXISTS `workitem_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_metadata`
--

-- DROP TABLE IF EXISTS `workitem_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `maybenull` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_process_rel`
--

-- DROP TABLE IF EXISTS `workitem_process_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_process_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_process_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_workitem_process_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_process_rel_1` (`parent_id`),
  KEY `K_project_process_rel_2` (`parent_uid`),
  KEY `K_project_process_rel_3` (`parent_cid`),
  KEY `K_project_process_rel_4` (`child_id`),
  KEY `K_project_process_rel_5` (`child_uid`),
  KEY `K_project_process_rel_6` (`child_cid`),
  KEY `K_project_process_rel_7` (`rdn`),
  KEY `K_workitem_process_rel_1` (`parent_id`),
  KEY `K_workitem_process_rel_2` (`parent_uid`),
  KEY `K_workitem_process_rel_3` (`parent_cid`),
  KEY `K_workitem_process_rel_4` (`child_id`),
  KEY `K_workitem_process_rel_5` (`child_uid`),
  KEY `K_workitem_process_rel_6` (`child_cid`),
  KEY `K_workitem_process_rel_7` (`rdn`),
  CONSTRAINT `FK_workitem_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_property_rel`
--

-- DROP TABLE IF EXISTS `workitem_property_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_workitem_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_workitem_property_rel_1` (`parent_id`),
  KEY `K_workitem_property_rel_2` (`parent_uid`),
  KEY `K_workitem_property_rel_3` (`parent_cid`),
  KEY `K_workitem_property_rel_4` (`child_id`),
  KEY `K_workitem_property_rel_5` (`child_uid`),
  KEY `K_workitem_property_rel_6` (`child_cid`),
  KEY `K_workitem_property_rel_7` (`rdn`),
  CONSTRAINT `FK_workitem_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `workitem_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitems`
--

-- DROP TABLE IF EXISTS `workitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
 CREATE TABLE `workitems` (
 `id` int(11) NOT NULL DEFAULT '0',
 `uid` VARCHAR( 128 ) DEFAULT NOT NULL DEFAULT '',
 `cid` VARCHAR(16) DEFAULT NOT NULL DEFAULT '569e94192201a',
 `dn` VARCHAR(128) NULL,
 `name` VARCHAR( 128 ) DEFAULT NOT NULL DEFAULT '',
 `number` varchar(16) NOT NULL DEFAULT '',
 `designation` TEXT DEFAULT NULL,
 `file_only` tinyint(1) NOT NULL DEFAULT '0',
 `life_stage` varchar(16) NOT NULL DEFAULT 'init',
 `acode` int(11) DEFAULT NULL,
 `parent_id` int(11) DEFAULT NULL,
 `parent_uid` VARCHAR(128) DEFAULT NULL,
 `default_process_id` int(11) DEFAULT NULL,
 `created` DATETIME DEFAULT NULL,
 `create_by_id` int(11) DEFAULT NULL,
 `create_by_uid` VARCHAR(64) DEFAULT NULL,
 `closed` int(11) DEFAULT NULL,
 `close_by_id` int(11) DEFAULT NULL,
 `close_by_uid` VARCHAR(64) DEFAULT NULL,
 `planned_closure` int(11) DEFAULT NULL,
 `spacename` varchar(32) NOT NULL DEFAULT 'workitem',
 `default_file_path` text NOT NULL,
 `reposit_id` int(11) DEFAULT NULL,
 `reposit_uid` VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_workitems_number` (`number`),
  UNIQUE KEY `UC_workitems_uid` (`uid`),
  KEY `K_workitems_create_by_uid` (`create_by_uid`),
  KEY `K_workitems_close_by_uid` (`close_by_uid`),
  KEY `K_workitems_version` (`version`),
  KEY `K_workitems_parent_id` (`parent_id`),
  KEY `K_workitems_default_process_id` (`default_process_id`),
  KEY `K_workitems_name` (`name`),
  KEY `K_workitems_dn` (`dn`),
  KEY `K_workitems_reposit_uid` (`reposit_uid`),
  KEY `K_workitems_reposit_id` (`reposit_id`),
  FULLTEXT KEY `FT_workitems_1` (`number`,`name`,`designation`),
  CONSTRAINT `FK_workitems_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT `FK_workitems_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
  CONSTRAINT `FK_workitems_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-24 13:30:46





--
-- Dumping data for table `schema_version`
--

LOCK TABLES `schema_version` WRITE;
/*!40000 ALTER TABLE `schema_version` DISABLE KEYS */;
INSERT INTO `schema_version` (`version`, `lastModification`) VALUES ('1.0','2017-06-23 10:32:32'),('1.0.1.2','2017-07-03 14:01:57'),('1.0.1','2017-06-27 13:47:48'),('1.0.2.1','2017-07-05 12:33:02'),('1.0.3.1','2017-07-07 10:43:15'),('1.0.4.1','2017-07-10 19:30:25'),('1.0.4.2','2017-07-17 15:48:24'),('1.0.5.1','2017-07-21 11:54:40'),('1.0.5.2','2017-07-21 16:31:38'),('1.0.5.3','2017-08-21 13:59:45'),('1.0.6.1','2017-08-22 20:08:42'),('1.0.5.4','2017-08-22 20:09:42'),('1.0.6.2','2017-08-23 13:55:23'),('1.0.6.3','2017-09-05 16:51:21'),('1.0.6.4','2017-10-20 14:56:24'),('1.0.7.1','2017-10-20 15:02:33'),('1.0.7.2','2017-10-20 15:03:00'),('1.0.7.3','2017-10-20 15:03:34'),('1.0.7.4','2017-10-20 15:04:07'),('1.0.7.4','2017-10-20 15:05:45'),('1.0.7.6','2017-10-27 11:39:46'),('1.0.7.7','2017-10-30 11:26:55'),('1.0.7.5','2017-10-30 17:05:14'),('1.0.7.8','2017-11-03 14:17:57'),('1.0.7.9','2017-11-07 13:27:09'),('1.0.7.9','2017-11-07 17:16:03'),('1.0.7.10','2017-11-09 18:53:46'),('1.0.7.11','2017-11-20 15:45:36'),('1.0.7.12','2017-12-08 19:57:33'),('1.0.7.13','2017-12-15 13:32:19'),('1.0.7.14','2018-01-05 11:29:54'),('1.0.7.15','2018-01-11 12:08:09'),('1.0.7.15','2018-01-11 17:20:26'),('1.0.7.16','2018-01-18 14:38:47'),('1.0.7.17','2018-01-29 14:16:19'),('1.0.7.18','2018-04-16 12:46:44'),('1.0.7.19','2018-04-23 19:51:32'),('1.0.7.20','2018-05-14 13:27:16'),('1.1.2','2018-07-24 10:20:48'),('1.0.1.3','2018-08-29 14:16:40'),('1.1.3','2018-10-04 12:14:41'),('1.0.1.4','2018-10-18 14:31:08'),('1.0.1.5','2018-10-23 13:37:17'),('1.1.4','2019-02-22 11:42:59'),('1.1.5','2019-02-22 11:43:34'),('1.1.7','2019-02-22 11:46:37'),('1.1.8','2019-03-05 10:29:23'),('1.1.9','2019-05-03 13:34:04');
/*!40000 ALTER TABLE `schema_version` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;


-- Dump completed on 2019-06-24 13:35:18


