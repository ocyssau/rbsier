-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: 192.9.200.21    Database: ranchbe
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping events for database 'ranchbe'
--

--
-- Dumping routines for database 'ranchbe'
--
/*!50003 DROP FUNCTION IF EXISTS `aclSequence` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `aclSequence`() RETURNS int(11)
BEGIN
	DECLARE lastId INT(11);

	UPDATE acl_resource_seq SET id = LAST_INSERT_ID(id+1) LIMIT 1;
	SELECT LAST_INSERT_ID(id) INTO lastId FROM acl_resource_seq;

    set @ret = lastId;
    RETURN @ret;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `build_path_withuid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `build_path_withuid`(_uid VARCHAR(64)) RETURNS text CHARSET utf8
BEGIN
    DECLARE parentUid VARCHAR(64);
    DECLARE cid VARCHAR(64);
    DECLARE message VARCHAR(256);
    set @ret = '';
    
    while _uid is not null do
      SELECT parent_uid, cid INTO parentUid, cid 
      FROM acl_resource_breakdown 
      WHERE uid = _uid;

    IF(found_rows() != 1) THEN
      set message = CONCAT('uid not found in table acl_resource_breakdown :', _uid);
      SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = message;
    END IF;
    
	IF(parentUid IS NULL) THEN
		set @ret := CONCAT('/app/ged/project/', _uid, @ret);
	ELSE
		set @ret := CONCAT('/', _uid, @ret);
	END IF;

	set _uid := parentUid;
    end while;

    RETURN @ret;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getResourceCnFromReferIdAndCid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `getResourceCnFromReferIdAndCid`(_id INT(11), _cid VARCHAR(128)) RETURNS varchar(256) CHARSET utf8
BEGIN
	DECLARE resourceCn varchar(256);

	SELECT cn INTO resourceCn FROM acl_resource WHERE referToId = _id AND referToCid = _cid;
    IF(resourceCn IS NULL) THEN
		SELECT cn INTO resourceCn FROM acl_resource AS resource
		JOIN acl_resource_breakdown as breakdown ON resource.referToUid = breakdown.parent_uid
		WHERE breakdown.id = _id AND breakdown.cid=_cid;
	END IF;

	return resourceCn;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getResourceCnFromReferUid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `getResourceCnFromReferUid`(_uid VARCHAR(128)) RETURNS varchar(256) CHARSET utf8
BEGIN
	DECLARE resourceCn varchar(256);

	SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = _uid;
    IF(resourceCn IS NULL) THEN
		SELECT cn INTO resourceCn FROM acl_resource AS resource
		JOIN acl_resource_breakdown as breakdown ON resource.referToUid = breakdown.parent_uid
		WHERE breakdown.uid = _uid;
	END IF;

	return resourceCn;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cancelCheckout` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `cancelCheckout`(_userUid VARCHAR(64))
BEGIN
	UPDATE `workitem_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
	UPDATE `workitem_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
	UPDATE `bookshop_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
	UPDATE `bookshop_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
	UPDATE `cadlib_documents` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
	UPDATE `cadlib_doc_files` SET `acode`='0', `lock_by_id`=NULL, `lock_by_uid`=NULL, `locked`=NULL WHERE `lock_by_uid`=_userUid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteDocumentsWithoutContainer` */;
ALTER DATABASE `ranchbe` CHARACTER SET latin1 COLLATE latin1_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `deleteDocumentsWithoutContainer`()
BEGIN
	CREATE TABLE tmpdoctodelete AS (
		SELECT doc.id, cont.number FROM workitem_documents as doc
		LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id 
		WHERE cont.number is null
	);
	DELETE FROM workitem_doc_files WHERE document_id IN(SELECT id FROM tmpdoctodelete);
	DELETE FROM workitem_documents WHERE id IN(SELECT id FROM tmpdoctodelete);
	DROP TABLE tmpdoctodelete;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `ranchbe` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `getDocumentsWithoutContainer` */;
ALTER DATABASE `ranchbe` CHARACTER SET latin1 COLLATE latin1_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `getDocumentsWithoutContainer`()
BEGIN
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"workitem" as `spacename`
	FROM workitem_documents as doc
	LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"bookshop" as `spacename`
	FROM bookshop_documents as doc
	LEFT OUTER JOIN bookshops as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"cadlib" as `spacename`
	FROM cadlib_documents as doc
	LEFT OUTER JOIN cadlibs as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"mockup" as `spacename`
	FROM mockup_documents as doc
	LEFT OUTER JOIN mockups as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `ranchbe` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `getFilesWithoutDocument` */;
ALTER DATABASE `ranchbe` CHARACTER SET latin1 COLLATE latin1_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `getFilesWithoutDocument`()
BEGIN
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"workitem" as `spacename`
	FROM workitem_doc_files as df
	LEFT OUTER JOIN workitem_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"bookshop" as `spacename`
	FROM bookshop_doc_files as df
	LEFT OUTER JOIN bookshop_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"cadlib" as `spacename`
	FROM cadlib_doc_files as df
	LEFT OUTER JOIN cadlib_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"mockup" as `spacename`
	FROM mockup_doc_files as df
	LEFT OUTER JOIN mockup_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `ranchbe` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `getResourceFromReferUid` */;
ALTER DATABASE `ranchbe` CHARACTER SET latin1 COLLATE latin1_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `getResourceFromReferUid`(_uid VARCHAR(64))
BEGIN
	DECLARE resourceUid varchar(64);

	SELECT uid INTO resourceUid FROM acl_resource WHERE referToUid = _uid;
    IF(resourceUid IS NULL) THEN
		SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE referToUid = (SELECT parent_uid FROM view_resource_breakdown WHERE uid = _uid);
	ELSE
		SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE uid = resourceUid;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `ranchbe` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `updateCheckoutIndex` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateCheckoutIndex`()
BEGIN
	DELETE FROM `checkout_index`;
	INSERT INTO checkout_index(
		SELECT * FROM(
			SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
			FROM workitem_doc_files as file
			JOIN workitem_documents as doc ON file.document_id=doc.id
			where file.acode=1
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM bookshop_doc_files as file
				JOIN bookshop_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM cadlib_doc_files as file
				JOIN cadlib_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM mockup_doc_files as file
				JOIN mockup_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
		) as checkout_index
	);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateCheckoutIndexFromUserId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateCheckoutIndexFromUserId`(_userId INT(11))
BEGIN
	DECLARE userId INT(11);
	SET @userId=_userId;
	DELETE FROM `checkout_index` WHERE `lock_by_id`=@userId;
	INSERT INTO checkout_index(
		SELECT * FROM(
			SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
			FROM workitem_doc_files as file
			JOIN workitem_documents as doc ON file.document_id=doc.id
			where file.acode=1 and file.lock_by_id=@userId
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM bookshop_doc_files as file
				JOIN bookshop_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM cadlib_doc_files as file
				JOIN cadlib_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM mockup_doc_files as file
				JOIN mockup_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
		) as checkout_index
	);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateDocfileDocumentUidRelation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateDocfileDocumentUidRelation`()
BEGIN


#### WORKITEM
DROP TEMPORARY TABLE IF EXISTS tmptableworkitem;
CREATE TEMPORARY TABLE tmptableworkitem AS (
	SELECT df.* FROM workitem_doc_files as df
	LEFT OUTER JOIN workitem_documents as doc ON df.document_uid=doc.uid
	WHERE doc.id IS NULL
);
DROP TABLE IF EXISTS workitem_doc_files_backup;
CREATE TABLE workitem_doc_files_backup AS(
	SELECT * FROM workitem_doc_files
);
UPDATE workitem_doc_files as df 
SET document_uid=(SELECT uid FROM workitem_documents WHERE id=df.document_id)
WHERE df.id IN (SELECT id FROM tmptableworkitem);

#### BOOKSHOP
DROP TEMPORARY TABLE IF EXISTS tmptablebookshop;
CREATE TEMPORARY TABLE tmptablebookshop AS (
	SELECT df.* FROM bookshop_doc_files as df
	LEFT OUTER JOIN bookshop_documents as doc ON df.document_uid=doc.uid
	WHERE doc.id IS NULL
);
DROP TABLE IF EXISTS bookshop_doc_files_backup;
CREATE TABLE bookshop_doc_files_backup AS(
	SELECT * FROM bookshop_doc_files
);
UPDATE bookshop_doc_files as df 
SET document_uid=(SELECT uid FROM bookshop_documents WHERE id=df.document_id)
WHERE df.id IN (SELECT id FROM tmptablebookshop);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateParentUid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateParentUid`()
BEGIN
	UPDATE workitems SET parent_uid = (SELECT uid FROM projects WHERE id=workitems.parent_id) WHERE workitems.parent_uid IS NULL;
	UPDATE cadlibs SET parent_uid = (SELECT uid FROM projects WHERE id=cadlibs.parent_id) WHERE cadlibs.parent_uid IS NULL;
	UPDATE bookshops SET parent_uid = (SELECT uid FROM projects WHERE id=bookshops.parent_id) WHERE bookshops.parent_uid IS NULL;
	UPDATE mockups SET parent_uid = (SELECT uid FROM projects WHERE id=mockups.parent_id) WHERE mockups.parent_uid IS NULL;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateResourceBreakdown` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateResourceBreakdown`()
BEGIN
DROP TABLE IF EXISTS acl_resource_breakdown;
	CREATE TABLE acl_resource_breakdown as
	    (SELECT 
	        `doc`.`id` AS `id`,
	        `doc`.`uid` AS `uid`,
	        `doc`.`name` AS `name`,
	        `doc`.`cid` AS `cid`,
	        `doc`.`container_id` AS `parent_id`,
	        `doc`.`container_uid` AS `parent_uid`
	    FROM
	        `workitem_documents` `doc`) UNION (SELECT 
	        `doc`.`id` AS `id`,
	        `doc`.`uid` AS `uid`,
	        `doc`.`name` AS `name`,
	        `doc`.`cid` AS `cid`,
	        `doc`.`container_id` AS `parent_id`,
	        `doc`.`container_uid` AS `parent_uid`
	    FROM
	        `bookshop_documents` `doc`) UNION (SELECT 
	        `doc`.`id` AS `id`,
	        `doc`.`uid` AS `uid`,
	        `doc`.`name` AS `name`,
	        `doc`.`cid` AS `cid`,
	        `doc`.`container_id` AS `parent_id`,
	        `doc`.`container_uid` AS `parent_uid`
	    FROM
	        `cadlib_documents` `doc`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `workitems` `cont`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `cadlibs` `cont`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `bookshops` `cont`) UNION (SELECT 
	        `cont`.`id` AS `id`,
	        `cont`.`uid` AS `uid`,
	        `cont`.`name` AS `name`,
	        `cont`.`cid` AS `cid`,
	        `cont`.`parent_id` AS `parent_id`,
	        `cont`.`parent_uid` AS `parent_uid`
	    FROM
	        `mockups` `cont`) UNION (SELECT 
	        `projects`.`id` AS `id`,
	        `projects`.`uid` AS `uid`,
	        `projects`.`name` AS `name`,
	        `projects`.`cid` AS `cid`,
	        `projects`.`parent_id` AS `parent_id`,
	        `projects`.`parent_uid` AS `parent_uid`
	    FROM
	        `projects`);
	        
	ALTER TABLE acl_resource_breakdown 
	ADD KEY (`id`),
	ADD KEY (`uid`),
	ADD KEY (`name`),
	ADD KEY (`cid`),
	ADD KEY (`parent_id`),
	ADD KEY (`parent_uid`);

	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateSearch` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateSearch`(_intervalInMn integer)
BEGIN
	SELECT `updated` INTO @updated FROM `search_update`;
    CASE 
		WHEN @updated > (now() + INTERVAL - _intervalInMn minute) THEN 
			SELECT 'is up to date';
			BEGIN
			END;
		ELSE
			DROP TABLE IF EXISTS search;
   			CREATE TABLE search AS SELECT * FROM objects;
			UPDATE `search_update` SET `updated`=now();
	END CASE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateWorkitemDn` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `updateWorkitemDn`()
BEGIN
	UPDATE workitems SET dn = CONCAT((SELECT dn FROM projects WHERE id=workitems.parent_id), uid,'/') WHERE workitems.dn IS NULL;
	UPDATE cadlibs SET dn = CONCAT((SELECT dn FROM projects WHERE id=cadlibs.parent_id), uid,'/') WHERE cadlibs.dn IS NULL;
	UPDATE bookshops SET dn = CONCAT((SELECT dn FROM projects WHERE id=bookshops.parent_id), uid,'/') WHERE bookshops.dn IS NULL;
	UPDATE mockups SET dn = CONCAT((SELECT dn FROM projects WHERE id=mockups.parent_id), uid,'/') WHERE mockups.dn IS NULL;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-24 17:40:07
