-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: 192.9.200.21    Database: ranchbe
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `acl_objects_view`
--

DROP TABLE IF EXISTS `acl_objects_view`;
/*!50001 DROP VIEW IF EXISTS `acl_objects_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `acl_objects_view` AS SELECT 
 1 AS `id`,
 1 AS `uid`,
 1 AS `name`,
 1 AS `fromclass`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `bookshop_batches`
--

DROP TABLE IF EXISTS `bookshop_batches`;
/*!50001 DROP VIEW IF EXISTS `bookshop_batches`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `bookshop_batches` AS SELECT 
 1 AS `uid`,
 1 AS `callback_method`,
 1 AS `owner_uid`,
 1 AS `created`,
 1 AS `comment`,
 1 AS `spacename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `cadlib_batches`
--

DROP TABLE IF EXISTS `cadlib_batches`;
/*!50001 DROP VIEW IF EXISTS `cadlib_batches`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `cadlib_batches` AS SELECT 
 1 AS `uid`,
 1 AS `callback_method`,
 1 AS `owner_uid`,
 1 AS `created`,
 1 AS `comment`,
 1 AS `spacename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `mockup_batches`
--

DROP TABLE IF EXISTS `mockup_batches`;
/*!50001 DROP VIEW IF EXISTS `mockup_batches`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `mockup_batches` AS SELECT 
 1 AS `uid`,
 1 AS `callback_method`,
 1 AS `owner_uid`,
 1 AS `created`,
 1 AS `comment`,
 1 AS `spacename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `objects`
--

DROP TABLE IF EXISTS `objects`;
/*!50001 DROP VIEW IF EXISTS `objects`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `objects` AS SELECT 
 1 AS `id`,
 1 AS `uid`,
 1 AS `number`,
 1 AS `name`,
 1 AS `designation`,
 1 AS `version`,
 1 AS `iteration`,
 1 AS `doctypeId`,
 1 AS `lockById`,
 1 AS `locked`,
 1 AS `updated`,
 1 AS `updateById`,
 1 AS `created`,
 1 AS `createById`,
 1 AS `spacename`,
 1 AS `cid`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `reconciliate_documents`
--

DROP TABLE IF EXISTS `reconciliate_documents`;
/*!50001 DROP VIEW IF EXISTS `reconciliate_documents`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `reconciliate_documents` AS SELECT 
 1 AS `id`,
 1 AS `uid`,
 1 AS `name`,
 1 AS `number`,
 1 AS `version`,
 1 AS `spacename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_bookshop_doctype_rel`
--

DROP TABLE IF EXISTS `view_bookshop_doctype_rel`;
/*!50001 DROP VIEW IF EXISTS `view_bookshop_doctype_rel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_bookshop_doctype_rel` AS SELECT 
 1 AS `parent_id`,
 1 AS `parent_uid`,
 1 AS `child_id`,
 1 AS `child_uid`,
 1 AS `rdn`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_cadlib_doctype_rel`
--

DROP TABLE IF EXISTS `view_cadlib_doctype_rel`;
/*!50001 DROP VIEW IF EXISTS `view_cadlib_doctype_rel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_cadlib_doctype_rel` AS SELECT 
 1 AS `parent_id`,
 1 AS `parent_uid`,
 1 AS `child_id`,
 1 AS `child_uid`,
 1 AS `rdn`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_docseeder_log`
--

DROP TABLE IF EXISTS `view_docseeder_log`;
/*!50001 DROP VIEW IF EXISTS `view_docseeder_log`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_docseeder_log` AS SELECT 
 1 AS `subjet`,
 1 AS `type`,
 1 AS `reference`,
 1 AS `workitem`,
 1 AS `version`,
 1 AS `redactor`,
 1 AS `creationDate`,
 1 AS `num`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_mockup_doctype_rel`
--

DROP TABLE IF EXISTS `view_mockup_doctype_rel`;
/*!50001 DROP VIEW IF EXISTS `view_mockup_doctype_rel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_mockup_doctype_rel` AS SELECT 
 1 AS `parent_id`,
 1 AS `parent_uid`,
 1 AS `child_id`,
 1 AS `child_uid`,
 1 AS `rdn`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_org_breakdown`
--

DROP TABLE IF EXISTS `view_org_breakdown`;
/*!50001 DROP VIEW IF EXISTS `view_org_breakdown`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_org_breakdown` AS SELECT 
 1 AS `id`,
 1 AS `uid`,
 1 AS `cid`,
 1 AS `dn`,
 1 AS `name`,
 1 AS `parent_id`,
 1 AS `parent_uid`,
 1 AS `spacename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_workitem_doctype_rel`
--

DROP TABLE IF EXISTS `view_workitem_doctype_rel`;
/*!50001 DROP VIEW IF EXISTS `view_workitem_doctype_rel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_workitem_doctype_rel` AS SELECT 
 1 AS `parent_id`,
 1 AS `parent_uid`,
 1 AS `child_id`,
 1 AS `child_uid`,
 1 AS `rdn`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `workitem_batches`
--

DROP TABLE IF EXISTS `workitem_batches`;
/*!50001 DROP VIEW IF EXISTS `workitem_batches`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `workitem_batches` AS SELECT 
 1 AS `uid`,
 1 AS `callback_method`,
 1 AS `owner_uid`,
 1 AS `created`,
 1 AS `comment`,
 1 AS `spacename`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `acl_objects_view`
--

/*!50001 DROP VIEW IF EXISTS `acl_objects_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `acl_objects_view` AS select `acl_role`.`id` AS `id`,`acl_role`.`uid` AS `uid`,`acl_role`.`name` AS `name`,'acl_role' AS `fromclass` from `acl_role` union select `acl_user`.`id` AS `id`,`acl_user`.`uid` AS `uid`,`acl_user`.`login` AS `name`,'acl_user' AS `fromclass` from `acl_user` union select `acl_group`.`id` AS `id`,`acl_group`.`uid` AS `uid`,`acl_group`.`name` AS `name`,'acl_group' AS `fromclass` from `acl_group` order by `id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `bookshop_batches`
--

/*!50001 DROP VIEW IF EXISTS `bookshop_batches`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `bookshop_batches` AS select `bookshop_documents_history`.`action_batch_uid` AS `uid`,`bookshop_documents_history`.`action_name` AS `callback_method`,`bookshop_documents_history`.`action_by` AS `owner_uid`,`bookshop_documents_history`.`action_started` AS `created`,`bookshop_documents_history`.`action_comment` AS `comment`,'bookshop' AS `spacename` from `bookshop_documents_history` where (`bookshop_documents_history`.`action_batch_uid` is not null) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cadlib_batches`
--

/*!50001 DROP VIEW IF EXISTS `cadlib_batches`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `cadlib_batches` AS select `cadlib_documents_history`.`action_batch_uid` AS `uid`,`cadlib_documents_history`.`action_name` AS `callback_method`,`cadlib_documents_history`.`action_by` AS `owner_uid`,`cadlib_documents_history`.`action_started` AS `created`,`cadlib_documents_history`.`action_comment` AS `comment`,'cadlib' AS `spacename` from `cadlib_documents_history` where (`cadlib_documents_history`.`action_batch_uid` is not null) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `mockup_batches`
--

/*!50001 DROP VIEW IF EXISTS `mockup_batches`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `mockup_batches` AS select `mockup_documents_history`.`action_batch_uid` AS `uid`,`mockup_documents_history`.`action_name` AS `callback_method`,`mockup_documents_history`.`action_by` AS `owner_uid`,`mockup_documents_history`.`action_started` AS `created`,`mockup_documents_history`.`action_comment` AS `comment`,'mockup' AS `spacename` from `mockup_documents_history` where (`mockup_documents_history`.`action_batch_uid` is not null) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `objects`
--

/*!50001 DROP VIEW IF EXISTS `objects`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `objects` AS select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`number` AS `number`,`obj`.`name` AS `name`,`obj`.`designation` AS `designation`,`obj`.`version` AS `version`,`obj`.`iteration` AS `iteration`,`obj`.`doctype_id` AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'workitem' AS `spacename`,'569e92709feb6' AS `cid` from `workitem_documents` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`number` AS `number`,`obj`.`name` AS `name`,`obj`.`designation` AS `designation`,`obj`.`version` AS `version`,`obj`.`iteration` AS `iteration`,`obj`.`doctype_id` AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'bookshop' AS `spacename`,'569e92709feb6' AS `cid` from `bookshop_documents` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`number` AS `number`,`obj`.`name` AS `name`,`obj`.`designation` AS `designation`,`obj`.`version` AS `version`,`obj`.`iteration` AS `iteration`,`obj`.`doctype_id` AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'cadlib' AS `spacename`,'569e92709feb6' AS `cid` from `cadlib_documents` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`number` AS `number`,`obj`.`name` AS `name`,`obj`.`designation` AS `designation`,`obj`.`version` AS `version`,`obj`.`iteration` AS `iteration`,`obj`.`doctype_id` AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'mockup' AS `spacename`,'569e92709feb6' AS `cid` from `mockup_documents` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`uid` AS `number`,`obj`.`name` AS `name`,'' AS `designation`,'' AS `version`,`obj`.`iteration` AS `iteration`,-(1) AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'workitem' AS `spacename`,'569e92b86d248' AS `cid` from `workitem_doc_files` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`uid` AS `number`,`obj`.`name` AS `name`,'' AS `designation`,'' AS `version`,`obj`.`iteration` AS `iteration`,-(1) AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'bookshop' AS `spacename`,'569e92b86d248' AS `cid` from `bookshop_doc_files` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`uid` AS `number`,`obj`.`name` AS `name`,'' AS `designation`,'' AS `version`,`obj`.`iteration` AS `iteration`,-(1) AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'cadlib' AS `spacename`,'569e92b86d248' AS `cid` from `cadlib_doc_files` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`uid` AS `number`,`obj`.`name` AS `name`,'' AS `designation`,'' AS `version`,`obj`.`iteration` AS `iteration`,-(1) AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'mockup' AS `spacename`,'569e92b86d248' AS `cid` from `mockup_doc_files` `obj` union select `workitems`.`id` AS `id`,`workitems`.`uid` AS `uid`,`workitems`.`number` AS `number`,`workitems`.`name` AS `name`,`workitems`.`designation` AS `designation`,'' AS `version`,'' AS `iteration`,-(10) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,`workitems`.`created` AS `created`,`workitems`.`create_by_id` AS `createById`,'workitem' AS `spacename`,'569e94192201a' AS `cid` from `workitems` union select `bookshops`.`id` AS `id`,`bookshops`.`uid` AS `uid`,`bookshops`.`number` AS `number`,`bookshops`.`name` AS `name`,`bookshops`.`designation` AS `designation`,'' AS `version`,'' AS `iteration`,-(10) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,`bookshops`.`created` AS `created`,`bookshops`.`create_by_id` AS `createById`,'bookshop' AS `spacename`,'569e94192201a' AS `cid` from `bookshops` union select `cadlibs`.`id` AS `id`,`cadlibs`.`uid` AS `uid`,`cadlibs`.`number` AS `number`,`cadlibs`.`name` AS `name`,`cadlibs`.`designation` AS `designation`,'' AS `version`,'' AS `iteration`,-(10) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,`cadlibs`.`created` AS `created`,`cadlibs`.`create_by_id` AS `createById`,'cadlib' AS `spacename`,'569e94192201a' AS `cid` from `cadlibs` union select `mockups`.`id` AS `id`,`mockups`.`uid` AS `uid`,`mockups`.`number` AS `number`,`mockups`.`name` AS `name`,`mockups`.`designation` AS `designation`,'' AS `version`,'' AS `iteration`,-(10) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,`mockups`.`created` AS `created`,`mockups`.`create_by_id` AS `createById`,'mockup' AS `spacename`,'569e94192201a' AS `cid` from `mockups` union select `projects`.`id` AS `id`,`projects`.`uid` AS `uid`,`projects`.`number` AS `number`,`projects`.`name` AS `name`,`projects`.`designation` AS `designation`,'' AS `version`,'' AS `iteration`,-(15) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,`projects`.`created` AS `created`,`projects`.`create_by_id` AS `createById`,'default' AS `spacename`,'569e93c6ee156' AS `cid` from `projects` union select `pdm_product_version`.`id` AS `id`,`pdm_product_version`.`uid` AS `uid`,`pdm_product_version`.`number` AS `number`,`pdm_product_version`.`name` AS `name`,`pdm_product_version`.`description` AS `designation`,`pdm_product_version`.`version` AS `version`,'' AS `iteration`,-(20) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,'' AS `created`,'' AS `createById`,`pdm_product_version`.`spacename` AS `spacename`,'569e972dd4c2c' AS `cid` from `pdm_product_version` union select `pdm_product_instance`.`id` AS `id`,`pdm_product_instance`.`uid` AS `uid`,`pdm_product_instance`.`number` AS `number`,`pdm_product_instance`.`name` AS `name`,`pdm_product_instance`.`description` AS `designation`,'' AS `version`,'' AS `iteration`,-(21) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,'' AS `created`,'' AS `createById`,'default' AS `spacename`,'569e972dd4c2c' AS `cid` from `pdm_product_instance` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `reconciliate_documents`
--

/*!50001 DROP VIEW IF EXISTS `reconciliate_documents`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `reconciliate_documents` AS select `workitem_documents`.`id` AS `id`,`workitem_documents`.`uid` AS `uid`,`workitem_documents`.`name` AS `name`,`workitem_documents`.`number` AS `number`,`workitem_documents`.`version` AS `version`,`workitem_documents`.`spacename` AS `spacename` from `workitem_documents` union select `cadlib_documents`.`id` AS `id`,`cadlib_documents`.`uid` AS `uid`,`cadlib_documents`.`name` AS `name`,`cadlib_documents`.`number` AS `number`,`cadlib_documents`.`version` AS `version`,`cadlib_documents`.`spacename` AS `spacename` from `cadlib_documents` union select `mockup_documents`.`id` AS `id`,`mockup_documents`.`uid` AS `uid`,`mockup_documents`.`name` AS `name`,`mockup_documents`.`number` AS `number`,`mockup_documents`.`version` AS `version`,`mockup_documents`.`spacename` AS `spacename` from `mockup_documents` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_bookshop_doctype_rel`
--

/*!50001 DROP VIEW IF EXISTS `view_bookshop_doctype_rel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `view_bookshop_doctype_rel` AS select `bookshop_doctype_rel`.`parent_id` AS `parent_id`,`bookshop_doctype_rel`.`parent_uid` AS `parent_uid`,`bookshop_doctype_rel`.`child_id` AS `child_id`,`bookshop_doctype_rel`.`child_uid` AS `child_uid`,`bookshop_doctype_rel`.`rdn` AS `rdn` from `bookshop_doctype_rel` union select `project_doctype_rel`.`parent_id` AS `parent_id`,`project_doctype_rel`.`parent_uid` AS `parent_uid`,`project_doctype_rel`.`child_id` AS `child_id`,`project_doctype_rel`.`child_uid` AS `child_uid`,`project_doctype_rel`.`rdn` AS `rdn` from `project_doctype_rel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_cadlib_doctype_rel`
--

/*!50001 DROP VIEW IF EXISTS `view_cadlib_doctype_rel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `view_cadlib_doctype_rel` AS select `cadlib_doctype_rel`.`parent_id` AS `parent_id`,`cadlib_doctype_rel`.`parent_uid` AS `parent_uid`,`cadlib_doctype_rel`.`child_id` AS `child_id`,`cadlib_doctype_rel`.`child_uid` AS `child_uid`,`cadlib_doctype_rel`.`rdn` AS `rdn` from `cadlib_doctype_rel` union select `project_doctype_rel`.`parent_id` AS `parent_id`,`project_doctype_rel`.`parent_uid` AS `parent_uid`,`project_doctype_rel`.`child_id` AS `child_id`,`project_doctype_rel`.`child_uid` AS `child_uid`,`project_doctype_rel`.`rdn` AS `rdn` from `project_doctype_rel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_docseeder_log`
--

/*!50001 DROP VIEW IF EXISTS `view_docseeder_log`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `view_docseeder_log` AS select `doc`.`designation` AS `subjet`,concat(`dt`.`name`,'-',`dt`.`designation`) AS `type`,`doc`.`number` AS `reference`,`wi`.`number` AS `workitem`,`doc`.`version` AS `version`,`doc`.`create_by_uid` AS `redactor`,`doc`.`created` AS `creationDate`,`doc`.`id` AS `num` from ((`workitem_documents` `doc` join `doctypes` `dt` on((`doc`.`doctype_id` = `dt`.`id`))) join `workitems` `wi` on((`doc`.`container_id` = `wi`.`id`))) where ((`dt`.`number` like 'sier%') or (`dt`.`number` like '%dq%')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_mockup_doctype_rel`
--

/*!50001 DROP VIEW IF EXISTS `view_mockup_doctype_rel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `view_mockup_doctype_rel` AS select `mockup_doctype_rel`.`parent_id` AS `parent_id`,`mockup_doctype_rel`.`parent_uid` AS `parent_uid`,`mockup_doctype_rel`.`child_id` AS `child_id`,`mockup_doctype_rel`.`child_uid` AS `child_uid`,`mockup_doctype_rel`.`rdn` AS `rdn` from `mockup_doctype_rel` union select `project_doctype_rel`.`parent_id` AS `parent_id`,`project_doctype_rel`.`parent_uid` AS `parent_uid`,`project_doctype_rel`.`child_id` AS `child_id`,`project_doctype_rel`.`child_uid` AS `child_uid`,`project_doctype_rel`.`rdn` AS `rdn` from `project_doctype_rel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_org_breakdown`
--

/*!50001 DROP VIEW IF EXISTS `view_org_breakdown`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `view_org_breakdown` AS select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'default' AS `spacename` from `projects` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'workitem' AS `spacename` from `workitems` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'cadlib' AS `spacename` from `cadlibs` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'bookshop' AS `spacename` from `bookshops` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'mockup' AS `spacename` from `mockups` `org` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_workitem_doctype_rel`
--

/*!50001 DROP VIEW IF EXISTS `view_workitem_doctype_rel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `view_workitem_doctype_rel` AS select `workitem_doctype_rel`.`parent_id` AS `parent_id`,`workitem_doctype_rel`.`parent_uid` AS `parent_uid`,`workitem_doctype_rel`.`child_id` AS `child_id`,`workitem_doctype_rel`.`child_uid` AS `child_uid`,`workitem_doctype_rel`.`rdn` AS `rdn` from `workitem_doctype_rel` union select `project_doctype_rel`.`parent_id` AS `parent_id`,`project_doctype_rel`.`parent_uid` AS `parent_uid`,`project_doctype_rel`.`child_id` AS `child_id`,`project_doctype_rel`.`child_uid` AS `child_uid`,`project_doctype_rel`.`rdn` AS `rdn` from `project_doctype_rel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `workitem_batches`
--

/*!50001 DROP VIEW IF EXISTS `workitem_batches`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `workitem_batches` AS select distinct `workitem_documents_history`.`action_batch_uid` AS `uid`,`workitem_documents_history`.`action_name` AS `callback_method`,`workitem_documents_history`.`action_by` AS `owner_uid`,`workitem_documents_history`.`action_started` AS `created`,`workitem_documents_history`.`action_comment` AS `comment`,'workitem' AS `spacename` from `workitem_documents_history` where (`workitem_documents_history`.`action_batch_uid` is not null) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-24 17:39:54
