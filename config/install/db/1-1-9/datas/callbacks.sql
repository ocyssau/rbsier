INSERT INTO `callbacks`(
	`id`,
	`uid`,
	`cid`,
	`name`,
	`description`,
	`is_actif`,
	`reference_id`,
	`reference_uid`,
	`reference_cid`,
	`spacename`,
	`events`,
	`callback_class`,
	`callback_method`
)
VALUES(
	'1', 
	'5e130f5f76bd8', 
	'callback9u687', 
	'PdmPostCheckin', 
	'When a pdm part is created or checkin, extract pdm datas from the pdmset file, and populate pdm tree', 
	'0', 
	NULL, 
	NULL, 
	'569e92709feb6', 
	NULL, 
	'[\"create.post\",\"checkin.post\"]', 
	'\\Pdm\\Callback\\ImportPdmdataSet', 
	'post'
);

ALTER TABLE `callbacks` AUTO_INCREMENT = 100;
