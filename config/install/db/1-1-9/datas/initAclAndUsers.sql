INSERT INTO `acl_user`
	(`id`,`uid`,`login`,`lastname`,`firstname`,`password`,`mail`,`owner_id`,`owner_uid`,`primary_role_id`,`lastlogin`,`is_active`)
	VALUES
	(1,'admin','admin','admin','admin',md5('admin00'),'admin@ranchbe.fr',2,'admin',1,NULL,1),
	(5,'nobody','nobody','nobody','nobody',md5('nobody00'),'nobody@ranchbe.fr',20,'admin',1,NULL,1),
	(70,'user','user','user','user',md5('user00'),'user@ranchbe.fr',50,'admin',1,NULL,1);
 
 INSERT INTO `acl_role` (`id`,`name`,`uid`,`description`) VALUES (2,"administrateur","administrateur","Administrateur");
 INSERT INTO `acl_role` (`id`,`name`,`uid`,`description`) VALUES (20,"reader","reader","Read only access");
 INSERT INTO `acl_role` (`id`,`name`,`uid`,`description`) VALUES (50,"concepteurs","concepteurs","Concepteurs CAO");
 INSERT INTO `acl_role` (`id`,`name`,`uid`,`description`) VALUES (51,"groupLeader","groupleader","chef de groupe de conception");
 INSERT INTO `acl_role` (`id`,`name`,`uid`,`description`) VALUES (52,"Leader","leader","Chargé d'affaire");
 INSERT INTO `acl_role` (`id`,`name`,`uid`,`description`) VALUES (53,"checkers","checkers","verificateurs des projets CAO");

INSERT INTO `acl_resource`
(`id`,
`uid`,
`cid`,
`cn`)
VALUES
(1,
'/app/',
'resourceappli',
'/app/'),
(2,
'/app/admin/',
'resourceadmin',
'/app/admin/'),
(3,
'/app/ged/',
'resourcea5ged',
'/app/ged/'),
(4,
'/app/owner/',
'resourceowner',
'/app/owner/'),
(5,
'/app/admin/acl/',
'resourceadaclf',
'/app/admin/acl/'),
(6,
'/app/admin/wf/',
'resourceadmwf',
'/app/admin/wf/'),
(7,
'/app/gedmanager/',
'resourgedmana',
'/app/gedmanager/'),
(8,
'/app/ged/pdm/',
'resourceb3pdm',
'/app/ged/pdm/'),
(9,
'/app/ged/project/',
'resourproject',
'/app/ged/project/');
