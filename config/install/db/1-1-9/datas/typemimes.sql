/*
-- Query: SELECT * FROM rbsier.typemimes
LIMIT 0, 1000

-- Date: 2018-01-13 14:23
*/
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (1,'application/argouml','Argo UML','.zargo');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (2,'x-world/x-3dmf','3Fichiers DMF','.3dmf .3dm .qd3d .qd3');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (3,'x-world/x-vrml','Model 3D Vrml','.wrl');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (4,'application/x-3dxmlplugin','3D XML','.3dxml');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (5,'application/acad','Fichiers AutoCAD (d\'après NCSA)','.dwg');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (6,'application/CATIA.Analysis','Fichier analyse catia','.CATAnalysis');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (7,'application/CATIA.Catalog','Fichier catalog catia','.catalog');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (8,'application/CATIA.Drawing','Fichier drawing catia','.CATDrawing');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (9,'application/CATIA.Material','Fichier material catia','.CATMaterial');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (10,'application/CATIA.Model','Fichier model catia','.model');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (11,'application/CATIA.Part','Fichier part catia','.CATPart');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (12,'application/CATIA.Process','Fichier process catia','.CATProcess');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (13,'application/CATIA.Product','Fichier product catia','.CATProduct');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (14,'application/CATIA.Script','Fichier script catia','.CATScript');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (15,'application/CATIA.Setting','Fichier setting catia','.CATSetting');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (16,'application/CATIA.Cgr','Cgr catia','.cgr');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (17,'application/SOLIDWORK.Assembly','Assemblage solidwork','.sldasm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (18,'application/SOLIDWORK.Part','Part solidwork','.sldprt');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (19,'application/SOLIDWORK.Drawing','Drawing solidwork','.slddrw');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (20,'application/dsptype','Fichiers TSP','.tsp');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (21,'application/dxf','Fichiers AutoCAD','.dxf');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (22,'application/futuresplash','Fichiers Flash Futuresplash','.spl');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (23,'application/gzip','Fichiers GNU Zip','.gz');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (24,'application/listenup','Fichiers Listenup','.ptlk');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (25,'application/mac-binhex40','Fichiers binaires Macintosh ','.hqx');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (26,'application/mbedlet','Fichiers Mbedlet','.mbd');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (27,'application/mif','Fichiers FrameMaker Interchange Format','.mif');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (28,'application/msexcel','Fichiers Microsoft Excel','.xls .xla .xlsx .xlsm .xlam .xlt .xltx .xltm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (29,'application/mshelp','Fichiers d\'aide Microsoft Windows','.hlp .chm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (30,'application/mspowerpoint','Fichiers Microsoft Powerpoint','.ppt .ppz .pps .pot .potx .potm .pptx .pptm .ppsm .ppsx .ppam .ppa');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (31,'application/msword','Fichiers Microsoft Word','.doc .dot .dotx .dotm .docx .docm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (32,'application/octet-stream','Fichiers executables','.bin .exe .com .dll .class');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (33,'application/oda','Fichiers Oda','.oda');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (34,'application/ooimpress','Fichiers oo impress','.odp');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (35,'application/pdf','Fichiers Adobe PDF','.pdf');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (36,'application/postscript','Fichiers Adobe Postscript','.ai .eps .ps');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (37,'application/rtc','Fichiers RTC','.rtc');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (38,'application/rtf','Fichiers Microsoft RTF','.rtf');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (39,'application/studiom','Fichiers Studiom','.smp');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (40,'application/toolbook','Fichiers Toolbook','.tbk');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (41,'application/vnd.wap.wmlc','Fichiers WMLC (WAP)','.wmlc');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (42,'application/vnd.wap.wmlscriptc','Fichiers script C WML (WAP)','.wmlsc');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (43,'application/vocaltec-media-desc','Fichiers Vocaltec Mediadesc','.vmd');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (44,'application/vocaltec-media-file','Fichiers Vocaltec Media','.vmf');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (45,'application/x-bcpio','Fichiers BCPIO','.bcpio');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (46,'application/x-compress','Fichiers -','.z');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (47,'application/x-cpio','Fichiers CPIO','.cpio');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (48,'application/x-csh','Fichiers C-Shellscript','.csh');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (49,'application/x-director','Fichiers -','.dcr .dir .dxr');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (50,'application/x-dvi','Fichiers DVI','.dvi');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (51,'application/x-envoy','Fichiers Envoy','.evy');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (52,'application/x-gtar','Fichiers archives GNU tar','.gtar');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (53,'application/x-hdf','Fichiers HDF','.hdf');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (54,'application/x-httpd-php','Fichiers PHP','.php .phtml');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (55,'application/x-javascript','Fichiers JavaScript cote serveur','.js');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (56,'application/x-latex','Fichiers source Latex','.latex');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (57,'application/x-mif','Fichiers FrameMaker Interchange Format','.mif');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (58,'application/x-netcdf','Fichiers Unidata CDF','.nc .cdf');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (59,'application/x-nschat','Fichiers NS Chat','.nsc');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (60,'application/x-sh','Fichiers Bourne Shellscript','.sh');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (61,'application/x-shar','Fichiers atchives Shell','.shar');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (62,'application/x-shockwave-flash','Fichiers Flash Shockwave','.swf .cab');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (63,'application/x-sprite','Fichiers Sprite','.spr .sprite');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (64,'application/x-stuffit','Fichiers Stuffit','.sit');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (65,'application/x-supercard','Fichiers Supercard','.sca');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (66,'application/x-sv4cpio','Fichiers CPIO','.sv4cpio');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (67,'application/x-sv4crc','Fichiers CPIO avec CRC','.sv4crc');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (68,'application/x-tar','Fichiers archives tar','.tar');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (69,'application/x-tcl','Fichiers script TCL','.tcl');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (70,'application/x-tex','Fichiers TEX','.tex');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (71,'application/x-texinfo','Fichiers TEXinfo','.texinfo .texi');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (72,'application/x-troff','Fichiers TROFF (Unix)','.t .tr .roff');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (73,'application/x-troff-man','Fichiers TROFF avec macros MAN (Unix)','.man .troff');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (74,'application/x-troff-me','Fichiers TROFF avec macros ME (Unix)','.me .troff');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (75,'application/x-troff-ms','Fichiers TROFF avec macros MS (Unix)','.me .troff');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (76,'application/x-ustar','Fichiers archives tar (Posix)','.ustar');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (77,'application/x-wais-source','Fichiers source WAIS','.src');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (78,'application/zip','Fichiers archives ZIP','.zip .Z');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (79,'application/tar','Fichiers archives TAR','.tar');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (80,'audio/basic','Fichiers son','.au .snd');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (81,'audio/echospeech','Fichiers Echospeed','.es');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (82,'audio/tsplayer','Fichiers TS-Player','.tsi');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (83,'audio/voxware','Fichiers Vox','.vox');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (84,'audio/x-aiff','Fichiers son AIFF','.aif .aiff .aifc');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (85,'audio/x-dspeeh','Fichiers parole','.dus .cht');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (86,'audio/x-midi','Fichiers MIDI','.mid .midi');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (87,'audio/x-mpeg','Fichiers MPEG','.mp2');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (88,'audio/x-pn-realaudio','Fichiers RealAudio','.ram .ra');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (89,'audio/x-pn-realaudio-plugin','Fichiers plugin RealAudio','.rpm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (90,'audio/x-qt-stream','Fichiers -','.stream');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (91,'audio/x-wav','Fichiers Wav','.wav');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (92,'application/i-deas','Fichiers SDRC I-deas','.unv');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (93,'application/cao__iges','Format d\'\'echange CAO IGES','.igs');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (94,'application/proeng','Fichiers ProEngineer','.prt');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (95,'application/pstree','product ps','_ps');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (96,'application/set','Fichiers CAO SET','.set');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (97,'application/sla','Fichiers stereolithographie','.stl');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (98,'application/step','Fichiers de donnees STEP','.step .stp');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (99,'application/vda','Fichiers de surface','.vda');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (100,'drawing/x-dwf','Fichiers Drawing','.dwf');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (101,'image/cis-cod','Fichiers CIS-Cod','.cod');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (102,'image/cmu-raster','Fichiers CMU-Raster','.ras');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (103,'image/fif','Fichiers FIF','.fif');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (104,'image/gif','Fichiers GIF','.gif');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (105,'image/ief','Fichiers IEF','.ief');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (106,'image/jpeg','Fichiers JPEG','.jpeg .jpg .jpe');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (107,'image/tiff','Fichiers TIFF','.tiff .tif');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (108,'image/vasa','Fichiers Vasa','.mcf');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (109,'image/vnd.wap.wbmp','Fichiers Bitmap (WAP)','.wbmp');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (110,'image/x-freehand','Fichiers Freehand','.fh4 .fh5 .fhc');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (111,'image/x-portable-anymap','Fichiers PBM Anymap','.pnm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (112,'image/x-portable-bitmap','Fichiers Bitmap PBM','.pbm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (113,'image/x-portable-graymap','Fichiers PBM Graymap','.pgm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (114,'image/x-portable-pixmap','Fichiers PBM Pixmap','.ppm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (115,'image/x-rgb','Fichiers RGB','.rgb');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (116,'image/x-windowdump','X-Windows Dump','.xwd');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (117,'image/x-xbitmap','Fichiers XBM','.xbm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (118,'image/x-xpixmap','Fichiers XPM','.xpm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (119,'image/svg+xml','Scalable vector graphic','.svg');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (120,'text/comma-separated-values','Fichiers de donnees separees par des virgules','.csv');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (121,'text/css','Fichiers de feuilles de style CSS','.css');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (122,'text/html','Fichiers -','.htm .html .shtml');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (123,'text/javascript','Fichiers JavaScript','.js');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (124,'text/plain','Fichiers pur texte','.txt');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (125,'text/richtext','Fichiers texte enrichi (Richtext)','.rtx');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (126,'text/dat','Fichiers dat','.dat');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (127,'text/rtf','Fichiers Microsoft RTF','.rtf');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (128,'text/tab-separated-values','Fichiers de donnees separees par des tabulations','.tsv');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (129,'text/vnd.wap.wml','Fichiers WML (WAP)','.wml');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (130,'text/vnd.wap.wmlscript','Fichiers script WML (WAP)','.wmls');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (131,'text/x-setext','Fichiers SeText','.etx');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (132,'text/x-sgml','Fichiers SGML','.sgm .sgml');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (133,'text/x-speech','Fichiers Speech','.talk .spc');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (134,'text/xml','Fichiers xml','.xml');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (135,'video/mpeg','Fichiers MPEG','.mpeg .mpg .mpe');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (136,'video/quicktime','Fichiers Quicktime','.qt .mov');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (137,'video/vnd.vivo','Fichiers Vivo','viv .vivo');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (138,'video/x-msvideo','Fichiers Microsoft AVI','.avi');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (139,'video/x-sgi-movie','Fichiers Movie','.movie');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (140,'workbook/formulaone','Fichiers FormulaOne','.vts .vtts');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (141,'application/vnd.oasis.opendocument.text','Open office text','.odt');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (142,'application/vnd.oasis.opendocument.text-template','Open office text template','.ott');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (143,'application/vnd.oasis.opendocument.text-web','Open office text web','.oth');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (144,'application/vnd.oasis.opendocument.text-master','Open office text master','.odm');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (145,'application/vnd.oasis.opendocument.graphics','Open office','.odg');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (146,'application/vnd.oasis.opendocument.graphics-template','Open office','.otg');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (147,'application/vnd.oasis.opendocument.presentation','Open office','.odp');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (148,'application/vnd.oasis.opendocument.presentation-template','Open office','.otp');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (149,'application/vnd.oasis.opendocument.spreadsheet','Open office','.ods');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (150,'application/vnd.oasis.opendocument.spreadsheet-template','Open office','.ots');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (151,'application/vnd.oasis.opendocument.chart','Open office','.odc');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (152,'application/vnd.oasis.opendocument.formula','Open office','.odf');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (153,'application/vnd.oasis.opendocument.database','Open office','.odb');
INSERT INTO `typemimes` (`id`,`uid`,`name`,`extensions`) VALUES (154,'application/vnd.oasis.opendocument.image','Open office','.odi');
