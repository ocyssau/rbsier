INSERT INTO `cron_task` (`id`, `name`, `description`, `callback_class`, `callback_method`, 
`callback_params`, `owner_id`, `owner_uid`, `status`, `created`, 
`exec_frequency`, `exec_schedule_start`, `exec_schedule_end`, `last_exec`, `is_actif`)
VALUES 
(2,'DbBackupDaily','Database dump to SQL','\\Application\\Cron\\DbBackup','daily',NULL,1,'admin','1','2017-10-20 11:22:49',21600,7,19,'2017-11-03 15:01:24',0),
(3,'FullTextIndexer','Full Text Indexer','\\Search\\Cron\\FtIndexer','run','{\"stringLimit\":8000,\"reindex\":false,\"limit\":10000,\"workitems\":[\"\'00z000-00\'\"],\"bookshops\":[\"\'NOTHING\'\"],\"filetypes\":[\".pdf\",\".doc\",\".docx\",\".ppt\",\".pptx\"]}',1,'admin','1','2017-10-20 17:17:43',43200,0,23,'2019-04-23 15:50:34',0),
(4,'CleanupProcess','Cleanup of old process','\\Docflow\\Cron\\CleanProcess','run',NULL,1,'admin','1','2017-10-27 11:40:40',604800,0,23,'2019-06-24 15:40:01',1),(5,'DbBackupWeekly','Databse dump each weeks','\\Application\\Cron\\DbBackup','weekly',NULL,1,'admin','1','2017-11-03 13:59:50',604800,7,8,'2017-11-03 15:02:36',0),
(6,'JobCleaner','Cleanup obsolete Jobs','\\Batch\\Cron\\JobCleaner','run','{\"maxJobAge\":\"15 day\",\"maxExecutionTime\":\"6 hour\"}',1,'admin','1','2017-11-03 15:12:38',86400,20,21,'2019-06-24 20:07:01',1),
(10,'RbgateReconciliate','Reconciliate Product to Document and populate document properties from Product properties','\\Rbgate\\Cron\\Reconciliate','run','{\"sychroniseProperties\":\"productToDocument\",\"verbose\":1}',1,'admin','1','2018-04-24 11:30:39',600,0,23,'2018-05-15 11:29:06',0),
(8,'RbgateCronBatch','Convert and Import CAD file to Ranchbe PDM','\\Rbgate\\Cron\\Batch','run','{\"workingDirectory\":\"data\\/rbgate\",\"serverUrl\":\"http:\\/\\/pccatia57.sierbla.int:8888\\/rbcs\\/converter\\/service\",\"filter\":\"extension LIKE \\\".cat%\\\"\",\"reconciliateCronName\":\"RbgateReconciliate\",\"spacename\":\"workitem\",\"verbose\":1,\"repositForVisuId\":\"863\\n\",\"onlyUpdatedFromLastRun\":true}',1,'admin','1','2018-04-23 15:03:30',3600,0,23,'2018-05-15 11:29:06',0),
(11,'RepositsReplication','Cron for Replication of reposits','\\Vault\\Cron\\Replicate','run','[]',1,'admin','1','2019-01-11 15:14:55',3600,0,23,NULL,0),
(12,'UpdateResourceBreakdown','Update the table acl_resource_breakdown with newly created datas. Call the SQL proc `updateResourceBreakdown()`','\\Acl\\Cron\\UpdateResourceBreakdown','run','[]',1,'admin','1','2019-03-05 10:40:07',1800,0,23,'2019-06-25 13:26:04',1),
(13,'UpdateMissingFiles','Update misssings files index','Vault\\Cron\\UpdateMissingFiles','run','[]',1,'admin','1','2019-05-03 16:30:28',3600,22,23,'2019-05-03 16:33:44',0),
(14,'ReconciliateDoclink','Reconciliate doclinks','\\Pdm\\Cron\\ReconciliateDoclink','run','{\"spacenames\":[\"workitem\",\"cadlib\"]}',1,'admin','1','2019-05-27 16:21:08',300,0,23,'2019-06-25 13:42:01',1);

ALTER TABLE `cron_task` AUTO_INCREMENT = 100;
