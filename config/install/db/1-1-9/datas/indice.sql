DROP PROCEDURE IF EXISTS `populateIndices`;
DELIMITER $$
CREATE PROCEDURE populateIndices()
BEGIN
	DECLARE i INT DEFAULT 1;
	DECLARE label VARCHAR(12);
    
   	TRUNCATE TABLE `document_indice`;

	WHILE i < 100 DO
        SET label = CONCAT('SI',LPAD(i, 2, '0'));
        INSERT INTO `document_indice` (`indice_id`, `indice_value`) VALUES (i, label);
   		SET i = i + 1;
	END WHILE;
END$$
DELIMITER ;

call populateIndices();
-- select * from `document_indice`;
