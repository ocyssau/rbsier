-- ###################################################"
DROP FUNCTION IF EXISTS aclSequence;
DELIMITER $$
CREATE FUNCTION aclSequence()
  RETURNS INT
BEGIN
	DECLARE lastId INT(11);

	UPDATE acl_resource_seq SET id = LAST_INSERT_ID(id+1) LIMIT 1;
	SELECT LAST_INSERT_ID(id) INTO lastId FROM acl_resource_seq;

    set @ret = lastId;
    RETURN @ret;
END;
$$
DELIMITER ;


INSERT INTO schema_version(version,lastModification) VALUES('1.0.6.2',now());
-- select * from schema_version;
