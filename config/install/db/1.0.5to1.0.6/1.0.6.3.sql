-- ************************
ALTER TABLE `workitem_documents`
	ADD COLUMN `ext56b26_recept_date` DATETIME NULL AFTER `recept_date`,
	ADD COLUMN `ext56b26_date_de_remise` DATETIME NULL AFTER `date_de_remise`,
	ADD COLUMN `ext56b26_rt_emitted` DATETIME NULL AFTER `rt_emitted`;
	
-- Dates --
UPDATE `workitem_documents` SET 
	`ext56b26_recept_date`=from_unixtime(`recept_date`, '%Y-%m-%d %h:%i:%s'),
	`ext56b26_date_de_remise`=from_unixtime(`date_de_remise`, '%Y-%m-%d %h:%i:%s'),
	`ext56b26_rt_emitted`=from_unixtime(`rt_emitted`, '%Y-%m-%d %h:%i:%s');

ALTER TABLE `workitem_documents`
	DROP COLUMN `recept_date`,
	DROP COLUMN `date_de_remise`,
	DROP COLUMN `rt_emitted`;

UPDATE `workitem_metadata` SET 
	`field_name`='ext56b26_recept_date' WHERE `field_name`='recept_date';
UPDATE `workitem_metadata` SET 
	`field_name`='ext56b26_date_de_remise' WHERE `field_name`='date_de_remise';
UPDATE `workitem_metadata` SET 
	`field_name`='ext56b26_rt_emitted' WHERE `field_name`='rt_emitted';
	
INSERT INTO schema_version(version,lastModification) VALUES('1.0.6.3',now());
-- select * from schema_version;
