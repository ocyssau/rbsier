-- DROP TABLE IF EXISTS `vault_reposit`;
CREATE TABLE IF NOT EXISTS `vault_reposit` (
 `id` int(11) NOT NULL,
 `uid` varchar(64) NOT NULL,
 `number` varchar(64) NOT NULL,
 `name` varchar(255) NOT NULL,
 `description` varchar(256) DEFAULT NULL,
 `default_file_path` varchar(512) NOT NULL,
 `type` int(2) NOT NULL,
 `mode` int(2) NOT NULL,
 `actif` int(2) NOT NULL DEFAULT 1,
 `priority` int(2) NOT NULL DEFAULT 1,
 `maxsize` int(11) NOT NULL DEFAULT 50000000,
 `maxcount` int(11) NOT NULL DEFAULT 50000,
 `created` DATETIME DEFAULT NULL,
 `create_by_id` int(11) DEFAULT NULL,
 `create_by_uid` VARCHAR(64) DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `U_vault_reposit_1` (`uid`),
 UNIQUE KEY `U_vault_reposit_2` (`number`),
 UNIQUE KEY `U_vault_reposit_3` (`default_file_path`),
 KEY `FK_vault_reposit_1` (`name`),
 KEY `FK_vault_reposit_2` (`description`),
 KEY `FK_vault_reposit_4` (`type`),
 KEY `FK_vault_reposit_5` (`mode`),
 KEY `FK_vault_reposit_6` (`actif`),
 KEY `FK_vault_reposit_7` (`priority`)
) ENGINE=InnoDB;

-- DROP TABLE IF EXISTS `vault_reposit_seq`;
CREATE TABLE IF NOT EXISTS `vault_reposit_seq`(
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
INSERT INTO `vault_reposit_seq` (id) VALUES(500);

-- ##############################################
-- WORKITEMS
ALTER TABLE `workitems` 
	ADD COLUMN `reposit_id` int(11) DEFAULT NULL AFTER `default_file_path`,
	ADD COLUMN `reposit_uid` VARCHAR(128) DEFAULT NULL AFTER `reposit_id`,
	ADD KEY `K_workitems_6` (`reposit_uid`);
	
-- CREATE REPOSIT OF EXISTING CONTAINERS
SET @position := (SELECT id from vault_reposit_seq);
INSERT INTO `vault_reposit`
(`id`,
`uid`,
`number`,
`name`,
`description`,
`default_file_path`,
`type`,
`mode`,
`actif`,
`priority`,
`maxsize`,
`maxcount`,
`created`,
`create_by_uid`
)
(SELECT
	(@position := @position + 1),
	SUBSTR(UUID(),1,8),
	cont.number,
	cont.name,
	CONCAT('Reposit for ',cont.number),
	`cont`.`default_file_path`,
	1,
	1,
	1,
	100,
	-1,
	-1,
	NOW(),
	'admin'
	FROM `workitems` AS `cont`);
UPDATE `vault_reposit_seq` SET id=(SELECT MAX(vault_reposit.id) FROM vault_reposit) LIMIT 1;

ALTER TABLE `workitems` 
ADD CONSTRAINT `FK_workitems_3`
  FOREIGN KEY (`reposit_id`)
  REFERENCES `vault_reposit` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

-- LINK CONTAINER TO REPOSIT
UPDATE `workitems` AS `cont`, `vault_reposit` AS `reposit` 
SET 
	cont.reposit_id = reposit.id,
    cont.reposit_uid = reposit.uid
WHERE cont.number = reposit.number;

-- SELECT * FROM vault_reposit;
-- SELECT * FROM workitems;
  

-- ##############################################
-- CADLIB
ALTER TABLE `cadlibs` 
	ADD COLUMN `reposit_id` int(11) DEFAULT NULL AFTER `default_file_path`,
	ADD COLUMN `reposit_uid` VARCHAR(128) DEFAULT NULL AFTER `reposit_id`,
	ADD KEY `K_cadlibs_6` (`reposit_uid`);
	
-- CREATE REPOSIT OF EXISTING CONTAINERS
SET @position := (SELECT id from vault_reposit_seq);
INSERT INTO `vault_reposit`
(`id`,
`uid`,
`number`,
`name`,
`description`,
`default_file_path`,
`type`,
`mode`,
`actif`,
`priority`,
`maxsize`,
`maxcount`,
`created`,
`create_by_uid`
)
(SELECT
	(@position := @position + 1),
	SUBSTR(UUID(),1,8),
	cont.number,
	cont.name,
	CONCAT('Reposit for ',cont.number),
	`cont`.`default_file_path`,
	1,
	1,
	1,
	100,
	-1,
	-1,
	NOW(),
	'admin'
	FROM `cadlibs` AS `cont`);
UPDATE `vault_reposit_seq` SET id=(SELECT MAX(vault_reposit.id) FROM vault_reposit) LIMIT 1;

ALTER TABLE `cadlibs` 
ADD CONSTRAINT `FK_cadlibs_3`
  FOREIGN KEY (`reposit_id`)
  REFERENCES `vault_reposit` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

-- LINK CONTAINER TO REPOSIT
UPDATE `cadlibs` AS `cont`, `vault_reposit` AS `reposit` 
SET 
	cont.reposit_id = reposit.id,
    cont.reposit_uid = reposit.uid
WHERE cont.number = reposit.number;

-- SELECT * FROM vault_reposit;
-- SELECT * FROM cadlibs;


-- ##############################################
-- BOOKSHOP
ALTER TABLE `bookshops` 
	ADD COLUMN `reposit_id` int(11) DEFAULT NULL AFTER `default_file_path`,
	ADD COLUMN `reposit_uid` VARCHAR(128) DEFAULT NULL AFTER `reposit_id`,
	ADD KEY `K_bookshops_6` (`reposit_uid`);
	
-- CREATE REPOSIT OF EXISTING CONTAINERS
SET @position := (SELECT id from vault_reposit_seq);
INSERT INTO `vault_reposit`
(`id`,
`uid`,
`number`,
`name`,
`description`,
`default_file_path`,
`type`,
`mode`,
`actif`,
`priority`,
`maxsize`,
`maxcount`,
`created`,
`create_by_uid`
)
(SELECT
	(@position := @position + 1),
	SUBSTR(UUID(),1,8),
	cont.number,
	cont.name,
	CONCAT('Reposit for ',cont.number),
	`cont`.`default_file_path`,
	1,
	1,
	1,
	100,
	-1,
	-1,
	NOW(),
	'admin'
	FROM `bookshops` AS `cont`);
UPDATE `vault_reposit_seq` SET id=(SELECT MAX(vault_reposit.id) FROM vault_reposit) LIMIT 1;

ALTER TABLE `bookshops` 
ADD CONSTRAINT `FK_bookshops_3`
  FOREIGN KEY (`reposit_id`)
  REFERENCES `vault_reposit` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

-- LINK CONTAINER TO REPOSIT
UPDATE `bookshops` AS `cont`, `vault_reposit` AS `reposit` 
SET 
	cont.reposit_id = reposit.id,
    cont.reposit_uid = reposit.uid
WHERE cont.number = reposit.number;

-- SELECT * FROM vault_reposit;
-- SELECT * FROM bookshops;

-- ##############################################
-- MOCKUP
ALTER TABLE `mockups` 
	ADD COLUMN `reposit_id` int(11) DEFAULT NULL AFTER `default_file_path`,
	ADD COLUMN `reposit_uid` VARCHAR(128) DEFAULT NULL AFTER `reposit_id`,
	ADD KEY `K_mockups_6` (`reposit_uid`);
	
-- CREATE REPOSIT OF EXISTING CONTAINERS
SET @position := (SELECT id from vault_reposit_seq);
INSERT INTO `vault_reposit`
(`id`,
`uid`,
`number`,
`name`,
`description`,
`default_file_path`,
`type`,
`mode`,
`actif`,
`priority`,
`maxsize`,
`maxcount`,
`created`,
`create_by_uid`
)
(SELECT
	(@position := @position + 1),
	SUBSTR(UUID(),1,8),
	cont.number,
	cont.name,
	CONCAT('Reposit for ',cont.number),
	`cont`.`default_file_path`,
	1,
	1,
	1,
	100,
	-1,
	-1,
	NOW(),
	'admin'
	FROM `mockups` AS `cont`);
UPDATE `vault_reposit_seq` SET id=(SELECT MAX(vault_reposit.id) FROM vault_reposit) LIMIT 1;

ALTER TABLE `mockups` 
ADD CONSTRAINT `FK_mockups_3`
  FOREIGN KEY (`reposit_id`)
  REFERENCES `vault_reposit` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

-- LINK CONTAINER TO REPOSIT
UPDATE `mockups` AS `cont`, `vault_reposit` AS `reposit` 
SET 
	cont.reposit_id = reposit.id,
    cont.reposit_uid = reposit.uid
WHERE cont.number = reposit.number;

-- SELECT * FROM vault_reposit;
-- SELECT * FROM mockups;


INSERT INTO schema_version(version,lastModification) VALUES('1.0.6.1',now());
-- select * from schema_version;
