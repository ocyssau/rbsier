/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `schema_version`
--
DROP TABLE IF EXISTS `schema_version`;
CREATE TABLE `schema_version` (
  `version` varchar(64) DEFAULT NULL,
  `lastModification` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
 INSERT INTO schema_version(version,lastModification) VALUES('1.0.7.12',now());

--
-- Table structure for table `acl_group`
--
DROP TABLE IF EXISTS `acl_group`;
CREATE TABLE `acl_group` (
  `id` int(11) NOT NULL,
  `uid` varchar(30) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` varchar(64) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `FK_acl_group_acl_user1` (`owner_id`),
  CONSTRAINT `FK_acl_group_acl_user1` FOREIGN KEY (`owner_id`) REFERENCES `acl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Temporary table structure for view `acl_objects_view`
--

DROP TABLE IF EXISTS `acl_objects_view`;

--
-- Table structure for table `acl_resource`
--
DROP TABLE IF EXISTS `acl_resource`;
CREATE TABLE `acl_resource` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `cid` varchar(64) DEFAULT 'aclresource60',
  `cn` varchar(255) NOT NULL,
  `ownerId` int(11) DEFAULT NULL,
  `ownerUid` varchar(64) DEFAULT NULL,
  `referToUid` varchar(64) DEFAULT NULL,
  `referToId` int(11) DEFAULT NULL,
  `referToCid` varchar(64) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `cn` (`cn`),
  UNIQUE KEY `U_referToUid` (`referToUid`),
  KEY `referToUid` (`referToUid`),
  KEY `referToId` (`referToId`),
  KEY `referToCid` (`referToCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `acl_resource_seq`
--
DROP TABLE IF EXISTS `acl_resource_seq`;
CREATE TABLE `acl_resource_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

--
-- Table structure for table `acl_right`
--
DROP TABLE IF EXISTS `acl_right`;
CREATE TABLE `acl_right` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_i_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `acl_role`
--
DROP TABLE IF EXISTS `acl_role`;
CREATE TABLE `acl_role` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT 'aclrole3zc547',
  `name` varchar(32) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(64) DEFAULT NULL,
  `description` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `parentId` (`parentId`),
  KEY `parentUid` (`parentUid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `acl_rule`
--
DROP TABLE IF EXISTS `acl_rule`;
CREATE TABLE `acl_rule` (
  `roleId` int(11) NOT NULL,
  `resource` varchar(128) NOT NULL,
  `resourceId` int(11) DEFAULT NULL,
  `rightId` int(11) NOT NULL,
  `rule` varchar(16) DEFAULT 'allow',
  PRIMARY KEY (`roleId`,`rightId`,`resource`),
  KEY `role_idx` (`roleId`),
  KEY `right_idx` (`rightId`),
  KEY `resource_idx` (`resource`),
  CONSTRAINT `FK_acl_rule_acl_resource1` FOREIGN KEY (`resource`) REFERENCES `acl_resource` (`cn`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_acl_rule_acl_right1` FOREIGN KEY (`rightId`) REFERENCES `acl_right` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_acl_rule_acl_role1` FOREIGN KEY (`roleId`) REFERENCES `acl_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `acl_seq`
--
DROP TABLE IF EXISTS `acl_seq`;
CREATE TABLE `acl_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `acl_user`
--
DROP TABLE IF EXISTS `acl_user`;
CREATE TABLE `acl_user` (
  `id` int(11) NOT NULL,
  `uid` varchar(128) NOT NULL,
  `login` varchar(128) NOT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `firstname` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `mail` varchar(128) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(64) DEFAULT NULL,
  `primary_role_id` int(11) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `auth_from` varchar(16) DEFAULT 'db',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_idx` (`uid`),
  UNIQUE KEY `login_idx` (`login`),
  KEY `owner_uid` (`owner_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `acl_user_role`
--

DROP TABLE IF EXISTS `acl_user_role`;
CREATE TABLE `acl_user_role` (
  `uid` varchar(64) NOT NULL,
  `cid` char(13) NOT NULL DEFAULT 'acluserrole59',
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  `resourceCn` varchar(255) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`,`resourceCn`),
  KEY `userId` (`userId`),
  KEY `roleId` (`roleId`),
  KEY `resourceCn` (`resourceCn`),
  CONSTRAINT `FK_acl_user_role_acl_resource1` FOREIGN KEY (`resourceCn`) REFERENCES `acl_resource` (`cn`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_acl_user_role_acl_role1` FOREIGN KEY (`roleId`) REFERENCES `acl_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_acl_user_role_acl_user1` FOREIGN KEY (`userId`) REFERENCES `acl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `batchjob`
--
DROP TABLE IF EXISTS `batchjob`;
CREATE TABLE `batchjob` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(258) DEFAULT NULL,
  `callback_class` text,
  `callback_method` varchar(64) DEFAULT NULL,
  `callback_params` text,
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(128) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `planned` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `started` double DEFAULT NULL,
  `ended` double DEFAULT NULL,
  `duration` float DEFAULT NULL,
  `hashkey` varchar(40) NOT NULL,
  `pid` varchar(32) DEFAULT NULL,
  `sapi` varchar(32) DEFAULT NULL,
  `log` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_batchjob_4` (`hashkey`),
  KEY `K_batchjob_1` (`planned`),
  KEY `K_batchjob_2` (`status`),
  KEY `K_batchjob_3` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_alias`
--
DROP TABLE IF EXISTS `bookshop_alias`;
CREATE TABLE `bookshop_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT 'bookshop',
  `uid` varchar(128) NOT NULL DEFAULT 'bookshop',
  `designation` varchar(128) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '45c84a5zalias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `U_bookshop_alias_1` (`uid`),
  KEY `K_bookshop_alias_1` (`container_id`),
  KEY `K_bookshop_alias_2` (`number`),
  KEY `K_bookshop_alias_3` (`cid`),
  KEY `K_bookshop_alias_4` (`name`),
  CONSTRAINT `FK_bookshop_alias_1` FOREIGN KEY (`container_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_category_rel`
--
DROP TABLE IF EXISTS `bookshop_category_rel`;
CREATE TABLE `bookshop_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_bookshop_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_bookshop_category_rel_1` (`parent_id`),
  KEY `K_bookshop_category_rel_2` (`parent_uid`),
  KEY `K_bookshop_category_rel_3` (`parent_cid`),
  KEY `K_bookshop_category_rel_4` (`child_id`),
  KEY `K_bookshop_category_rel_5` (`child_uid`),
  KEY `K_bookshop_category_rel_6` (`child_cid`),
  KEY `K_bookshop_category_rel_7` (`rdn`),
  CONSTRAINT `FK_bookshop_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onBookshopCategoryRelInsert BEFORE INSERT ON bookshop_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;

--
-- Table structure for table `bookshop_cont_metadata_seq`
--
DROP TABLE IF EXISTS `bookshop_cont_metadata_seq`;
CREATE TABLE `bookshop_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

--
-- Table structure for table `bookshop_doc_files`
--
DROP TABLE IF EXISTS `bookshop_doc_files`;
CREATE TABLE `bookshop_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  UNIQUE KEY `INDEX_bookshop_doc_files_uid` (`uid`),
  KEY `FK_bookshop_doc_files_1` (`document_id`),
  KEY `IK_bookshop_doc_files_1` (`document_id`),
  KEY `IK_bookshop_doc_files_2` (`name`),
  KEY `IK_bookshop_doc_files_3` (`path`(128)),
  KEY `IK_bookshop_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_bookshop_doc_files_5` (`mainrole`),
  KEY `IK_bookshop_doc_files_6` (`roles`(32)),
  KEY `IK_bookshop_doc_files_8` (`document_uid`),
  KEY `FK_bookshop_docfiles_7` (`create_by_uid`),
  KEY `FK_bookshop_docfiles_8` (`lock_by_uid`),
  KEY `FK_bookshop_docfiles_9` (`update_by_uid`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_bookshop_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `bookshop_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onBookshopDocfileUpdate AFTER UPDATE ON bookshop_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `bookshop_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'bookshop',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END ;;
DELIMITER ;


DELIMITER ;;
CREATE TRIGGER onBookshopDocfileDelete AFTER DELETE ON bookshop_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='bookshop';
END ;;
DELIMITER ;


--
-- Table structure for table `bookshop_doc_files_seq`
--

DROP TABLE IF EXISTS `bookshop_doc_files_seq`;
CREATE TABLE `bookshop_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_doc_files_versions`
--

DROP TABLE IF EXISTS `bookshop_doc_files_versions`;
CREATE TABLE `bookshop_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  KEY `IK_bookshop_doc_files_version_1` (`document_id`),
  KEY `IK_bookshop_doc_files_version_2` (`name`),
  KEY `IK_bookshop_doc_files_version_3` (`path`),
  KEY `IK_bookshop_doc_files_version_4` (`iteration`,`document_id`),
  KEY `of_file_id` (`of_file_id`),
  KEY `IK_bookshop_doc_files_version_5` (`mainrole`),
  KEY `IK_bookshop_doc_files_version_6` (`roles`(32)),
  KEY `IK_bookshop_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_doc_files_versions_seq`
--
DROP TABLE IF EXISTS `bookshop_doc_files_versions_seq`;
CREATE TABLE `bookshop_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_doc_rel`
--
DROP TABLE IF EXISTS `bookshop_doc_rel`;
CREATE TABLE `bookshop_doc_rel` (
 `link_id` int(11) NOT NULL,
 `name` varchar(256) default NULL,
 `parent_id` int(11) default NULL,
 `parent_uid` varchar(64) default NULL,
 `parent_space_id` int(11) default NULL,
 `child_id` int(11) default NULL,
 `child_uid` varchar(64) default NULL,
 `child_space_id` int(11) default NULL,
 `acode` int(11) default NULL,
 `lindex` int(11) default 0,
 hash char(32) default NULL,
 data text default NULL,
 PRIMARY KEY (`link_id`),
 UNIQUE KEY `uniq_docid_ldoc_id` (`parent_id`,`child_id`),
 UNIQUE KEY `uniq_docid_lname_id` (`parent_id`,`name`),
 INDEX `_dr_index_01` (`lindex`),
 INDEX `_dr_index_02` (`parent_uid`),
 INDEX `_dr_index_03` (`child_uid`),
 INDEX `_dr_index_04` (`parent_id`, `parent_space_id`),
 INDEX `_dr_index_05` (`child_id`, `child_space_id`),
 CONSTRAINT `FK_bookshop_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `FK_bookshop_doc_rel_11` FOREIGN KEY (`child_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_doc_rel_seq`
--
DROP TABLE IF EXISTS `bookshop_doc_rel_seq`;
CREATE TABLE `bookshop_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_doccomments`
--
DROP TABLE IF EXISTS `bookshop_doccomments`;
CREATE TABLE `bookshop_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_doccomments_seq`
--
DROP TABLE IF EXISTS `bookshop_doccomments_seq`;
CREATE TABLE `bookshop_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_doctype_rel`
--
DROP TABLE IF EXISTS `bookshop_doctype_rel`;
CREATE TABLE `bookshop_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_bookshop_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_bookshop_doctype_rel_1` (`parent_id`),
  KEY `K_bookshop_doctype_rel_2` (`parent_uid`),
  KEY `K_bookshop_doctype_rel_3` (`parent_cid`),
  KEY `K_bookshop_doctype_rel_4` (`child_id`),
  KEY `K_bookshop_doctype_rel_5` (`child_uid`),
  KEY `K_bookshop_doctype_rel_6` (`child_cid`),
  KEY `K_bookshop_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_bookshop_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onBookshopDoctypeRelInsert BEFORE INSERT ON bookshop_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;

--
-- Table structure for table `bookshop_documents`
--

DROP TABLE IF EXISTS `bookshop_documents`;
CREATE TABLE `bookshop_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(128) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `branch_id` int(1) DEFAULT '1',
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'bookshop',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tags` varchar(256) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `doc_source` varchar(255) DEFAULT NULL,
  `commentaires` varchar(255) DEFAULT NULL,
  `applicabilite` varchar(255) DEFAULT NULL,
  `maj_int_ind` varchar(255) DEFAULT NULL,
  `min_int_ind` varchar(255) DEFAULT NULL,
  `mot_cles` varchar(255) DEFAULT NULL,
  `fournisseur` varchar(32) DEFAULT NULL,
  `docsier_fournisseur` varchar(32) DEFAULT NULL,
  `docsier_famille` varchar(12) DEFAULT NULL,
  `docsier_date` int(11) DEFAULT NULL,
  `docsier_keyword` mediumtext,
  `docsier_format` varchar(255) DEFAULT NULL,
  `ata` varchar(255) DEFAULT NULL,
  `sub_ata` varchar(255) DEFAULT NULL,
  `a_type` varchar(255) DEFAULT NULL,
  `orig_filename` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_bookshop_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_bookshop_documents_uid` (`uid`),
  KEY `FK_bookshop_documents_1` (`cad_model_supplier_id`),
  KEY `FK_bookshop_documents_2` (`supplier_id`),
  KEY `FK_bookshop_documents_3` (`container_id`),
  KEY `FK_bookshop_documents_4` (`category_id`),
  KEY `FK_bookshop_documents_5` (`doctype_id`),
  KEY `FK_bookshop_documents_6` (`version`),
  KEY `INDEX_bookshop_documents_spacename` (`spacename`),
  KEY `INDEX_bookshop_documents_name` (`name`),
  KEY `INDEX_bookshop_documents_cuid` (`container_uid`),
  KEY `FK_bookshop_documents_7` (`create_by_uid`),
  KEY `FK_bookshop_documents_8` (`lock_by_uid`),
  KEY `FK_bookshop_documents_9` (`update_by_uid`),
  KEY `FK_bookshop_documents_10` (`as_template`),
  KEY `FK_bookshop_documents_11` (`tags`),
  KEY `FK_bookshop_documents_12` (`branch_id`),
  FULLTEXT KEY `FT_bookshop_documents` (`name`,`number`,`designation`,`tags`),
  CONSTRAINT `FK_bookshop_documents_1` FOREIGN KEY (`container_id`) REFERENCES `bookshops` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onBookshopDocumentDelete AFTER DELETE ON bookshop_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=20;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END ;;
DELIMITER ;

--
-- Table structure for table `bookshop_documents_history`
--

DROP TABLE IF EXISTS `bookshop_documents_history`;
CREATE TABLE `bookshop_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `action_by` varchar(32) NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`),
  KEY `K_bookshop_documents_history_number` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_documents_history_seq`
--

DROP TABLE IF EXISTS `bookshop_documents_history_seq`;
CREATE TABLE `bookshop_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_documents_seq`
--
DROP TABLE IF EXISTS `bookshop_documents_seq`;
CREATE TABLE `bookshop_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_familles`
--

DROP TABLE IF EXISTS `bookshop_familles`;
CREATE TABLE `bookshop_familles` (
  `id` varchar(12) NOT NULL DEFAULT '0',
  `famille_name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_files`
--

DROP TABLE IF EXISTS `bookshop_files`;
CREATE TABLE `bookshop_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` mediumtext NOT NULL,
  `file_root_name` varchar(128) NOT NULL DEFAULT '',
  `file_extension` varchar(16) DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) NOT NULL DEFAULT 'init',
  `file_type` varchar(128) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_bookshop_files_1` (`bookshop_id`),
  KEY `FK_bookshop_files_2` (`import_order`),
  KEY `index_file_name` (`file_name`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_import_history`
--

DROP TABLE IF EXISTS `bookshop_import_history`;
CREATE TABLE `bookshop_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_key_word`
--

DROP TABLE IF EXISTS `bookshop_key_word`;
CREATE TABLE `bookshop_key_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_word` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `key_word` (`key_word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_metadata`
--

DROP TABLE IF EXISTS `bookshop_metadata`;
CREATE TABLE `bookshop_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_metadata_seq`
--
DROP TABLE IF EXISTS `bookshop_metadata_seq`;
CREATE TABLE `bookshop_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `bookshop_property_rel`
--

DROP TABLE IF EXISTS `bookshop_property_rel`;
CREATE TABLE `bookshop_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_bookshop_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_bookshop_property_rel_1` (`parent_id`),
  KEY `K_bookshop_property_rel_2` (`parent_uid`),
  KEY `K_bookshop_property_rel_3` (`parent_cid`),
  KEY `K_bookshop_property_rel_4` (`child_id`),
  KEY `K_bookshop_property_rel_5` (`child_uid`),
  KEY `K_bookshop_property_rel_6` (`child_cid`),
  KEY `K_bookshop_property_rel_7` (`rdn`),
  CONSTRAINT `FK_bookshop_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `bookshop_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER ;;
CREATE TRIGGER onBookshopPropertyRelInsert BEFORE INSERT ON bookshop_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;

--
-- Table structure for table `bookshops`
--

DROP TABLE IF EXISTS `bookshops`;
CREATE TABLE `bookshops` (
  `id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e94192201a',
  `dn` varchar(128) DEFAULT NULL,
  `designation` text,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `acode` int(11) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT 'init',
  `version` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` mediumtext NOT NULL,
  `reposit_id` int(11) DEFAULT NULL,
  `reposit_uid` varchar(128) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `planned_closure` datetime DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `close_by_id` int(11) DEFAULT NULL,
  `close_by_uid` varchar(64) DEFAULT NULL,
  `spacename` varchar(16) NOT NULL DEFAULT 'bookshop',
  `proprietaire` varchar(255) DEFAULT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(8) NOT NULL DEFAULT 'bookshop',
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_bookshops_number` (`number`),
  UNIQUE KEY `UC_bookshops_uid` (`uid`),
  KEY `K_bookshops_3` (`create_by_uid`),
  KEY `K_bookshops_4` (`close_by_uid`),
  KEY `K_bookshops_name` (`name`),
  KEY `K_bookshops_dn` (`dn`),
  KEY `FK_bookshops_1` (`parent_id`),
  KEY `FK_bookshops_2` (`default_process_id`),
  KEY `K_bookshops_6` (`reposit_uid`),
  KEY `FK_bookshops_3` (`reposit_id`),
  FULLTEXT KEY `FT_bookshops_number` (`number`,`name`,`designation`),
  CONSTRAINT `FK_bookshops_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshops_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshops_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onBookshopInsert BEFORE INSERT ON bookshops FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END ;;
DELIMITER ;

DELIMITER ;;
CREATE TRIGGER onBookshopUpdate BEFORE UPDATE ON bookshops FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(64);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn,uid INTO parentDn,parentUid from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END ;;
DELIMITER ;


DELIMITER ;;
CREATE TRIGGER onBookshopDelete AFTER DELETE ON bookshops FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='bookshop';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='bookshop';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='bookshop';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END ;;
DELIMITER ;





--
-- Table structure for table `cadlib_alias`
--

DROP TABLE IF EXISTS `cadlib_alias`;
CREATE TABLE `cadlib_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT 'cadlib',
  `uid` varchar(128) NOT NULL DEFAULT 'cadlib',
  `designation` varchar(128) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '45c84a5zalias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `U_cadlib_alias_1` (`uid`),
  KEY `K_cadlib_alias_1` (`container_id`),
  KEY `K_cadlib_alias_2` (`number`),
  KEY `K_cadlib_alias_3` (`cid`),
  KEY `K_cadlib_alias_4` (`name`),
  CONSTRAINT `FK_cadlib_alias_1` FOREIGN KEY (`container_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_category_rel`
--

DROP TABLE IF EXISTS `cadlib_category_rel`;
CREATE TABLE `cadlib_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_cadlib_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_cadlib_category_rel_1` (`parent_id`),
  KEY `K_cadlib_category_rel_2` (`parent_uid`),
  KEY `K_cadlib_category_rel_3` (`parent_cid`),
  KEY `K_cadlib_category_rel_4` (`child_id`),
  KEY `K_cadlib_category_rel_5` (`child_uid`),
  KEY `K_cadlib_category_rel_6` (`child_cid`),
  KEY `K_cadlib_category_rel_7` (`rdn`),
  CONSTRAINT `FK_cadlib_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER ;;
CREATE TRIGGER onCadlibCategoryRelInsert BEFORE INSERT ON cadlib_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;





--
-- Table structure for table `cadlib_cont_metadata_seq`
--
DROP TABLE IF EXISTS `cadlib_cont_metadata_seq`;
CREATE TABLE `cadlib_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_doc_files`
--

DROP TABLE IF EXISTS `cadlib_doc_files`;
CREATE TABLE `cadlib_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  UNIQUE KEY `INDEX_cadlib_doc_files_uid` (`uid`),
  KEY `FK_cadlib_doc_files_1` (`document_id`),
  KEY `IK_cadlib_doc_files_1` (`document_id`),
  KEY `IK_cadlib_doc_files_2` (`name`),
  KEY `IK_cadlib_doc_files_3` (`path`(128)),
  KEY `IK_cadlib_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_cadlib_doc_files_5` (`mainrole`),
  KEY `IK_cadlib_doc_files_6` (`roles`(32)),
  KEY `IK_cadlib_doc_files_8` (`document_uid`),
  KEY `FK_cadlib_docfiles_7` (`create_by_uid`),
  KEY `FK_cadlib_docfiles_8` (`lock_by_uid`),
  KEY `FK_cadlib_docfiles_9` (`update_by_uid`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_cadlib_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `cadlib_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onCadlibDocfileUpdate AFTER UPDATE ON cadlib_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `cadlib_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'cadlib',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE TRIGGER onCadlibDocfileDelete AFTER DELETE ON cadlib_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='cadlib';
END ;;
DELIMITER ;


--
-- Table structure for table `cadlib_doc_files_seq`
--

DROP TABLE IF EXISTS `cadlib_doc_files_seq`;
CREATE TABLE `cadlib_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_doc_files_versions`
--

DROP TABLE IF EXISTS `cadlib_doc_files_versions`;
CREATE TABLE `cadlib_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`(128)),
  KEY `IK_cadlib_doc_files_version_1` (`document_id`),
  KEY `IK_cadlib_doc_files_version_2` (`name`),
  KEY `IK_cadlib_doc_files_version_3` (`path`(128)),
  KEY `IK_cadlib_doc_files_version_4` (`iteration`,`document_id`),
  KEY `of_file_id` (`of_file_id`),
  KEY `IK_cadlib_doc_files_version_5` (`mainrole`),
  KEY `IK_cadlib_doc_files_version_6` (`roles`(32)),
  KEY `IK_cadlib_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `cadlib_doc_files_versions_seq`;
CREATE TABLE `cadlib_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_doc_rel`
--

DROP TABLE IF EXISTS `cadlib_doc_rel`;
CREATE TABLE `cadlib_doc_rel` (
 `link_id` int(11) NOT NULL,
 `name` varchar(256) default NULL,
 `parent_id` int(11) default NULL,
 `parent_uid` varchar(64) default NULL,
 `parent_space_id` int(11) default NULL,
 `child_id` int(11) default NULL,
 `child_uid` varchar(64) default NULL,
 `child_space_id` int(11) default NULL,
 `acode` int(11) default NULL,
 `lindex` int(11) default 0,
 hash char(32) default NULL,
 data text default NULL,
 PRIMARY KEY (`link_id`),
 UNIQUE KEY `uniq_docid_ldoc_id` (`parent_id`,`child_id`),
 UNIQUE KEY `uniq_docid_lname_id` (`parent_id`,`name`),
 INDEX `_dr_index_01` (`lindex`),
 INDEX `_dr_index_02` (`parent_uid`),
 INDEX `_dr_index_03` (`child_uid`),
 INDEX `_dr_index_04` (`parent_id`, `parent_space_id`),
 INDEX `_dr_index_05` (`child_id`, `child_space_id`),
 CONSTRAINT `FK_cadlib_doc_rel_1` FOREIGN KEY (`parent_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `FK_cadlib_doc_rel_2` FOREIGN KEY (`child_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_doc_rel_seq`
--

DROP TABLE IF EXISTS `cadlib_doc_rel_seq`;
CREATE TABLE `cadlib_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_doccomments`
--

DROP TABLE IF EXISTS `cadlib_doccomments`;
CREATE TABLE `cadlib_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_doccomments_seq`
--

DROP TABLE IF EXISTS `cadlib_doccomments_seq`;
CREATE TABLE `cadlib_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_doctype_rel`
--

DROP TABLE IF EXISTS `cadlib_doctype_rel`;
CREATE TABLE `cadlib_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_cadlib_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_cadlib_doctype_rel_1` (`parent_id`),
  KEY `K_cadlib_doctype_rel_2` (`parent_uid`),
  KEY `K_cadlib_doctype_rel_3` (`parent_cid`),
  KEY `K_cadlib_doctype_rel_4` (`child_id`),
  KEY `K_cadlib_doctype_rel_5` (`child_uid`),
  KEY `K_cadlib_doctype_rel_6` (`child_cid`),
  KEY `K_cadlib_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_cadlib_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onCadlibDoctypeRelInsert BEFORE INSERT ON cadlib_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;


--
-- Table structure for table `cadlib_documents`
--

DROP TABLE IF EXISTS `cadlib_documents`;
CREATE TABLE `cadlib_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `branch_id` int(1) DEFAULT '1',
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'cadlib',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tags` varchar(256) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `fabricant_a` varchar(255) DEFAULT NULL,
  `fabricant_b` varchar(255) DEFAULT NULL,
  `fabricant_c` varchar(255) DEFAULT NULL,
  `ref_a` varchar(255) DEFAULT NULL,
  `ref_b` varchar(255) DEFAULT NULL,
  `ref_c` varchar(255) DEFAULT NULL,
  `chapitre` varchar(255) DEFAULT NULL,
  `subchapitre` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_cadlib_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_cadlib_documents_uid` (`uid`),
  KEY `FK_cadlib_documents_1` (`cad_model_supplier_id`),
  KEY `FK_cadlib_documents_2` (`supplier_id`),
  KEY `FK_cadlib_documents_3` (`container_id`),
  KEY `FK_cadlib_documents_4` (`category_id`),
  KEY `FK_cadlib_documents_5` (`doctype_id`),
  KEY `FK_cadlib_documents_6` (`version`),
  KEY `INDEX_cadlib_documents_spacename` (`spacename`),
  KEY `INDEX_cadlib_documents_name` (`name`),
  KEY `INDEX_cadlib_documents_cuid` (`container_uid`),
  KEY `FK_cadlib_documents_7` (`create_by_uid`),
  KEY `FK_cadlib_documents_8` (`lock_by_uid`),
  KEY `FK_cadlib_documents_9` (`update_by_uid`),
  KEY `FK_cadlib_documents_10` (`as_template`),
  KEY `FK_cadlib_documents_11` (`tags`),
  KEY `FK_cadlib_documents_12` (`branch_id`),
  FULLTEXT KEY `FT_cadlib_documents` (`name`,`number`,`designation`,`tags`),
  CONSTRAINT `FK_cadlib_documents_1` FOREIGN KEY (`container_id`) REFERENCES `cadlibs` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onCadlibDocumentDelete AFTER DELETE ON cadlib_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=25;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END ;;
DELIMITER ;



--
-- Table structure for table `cadlib_documents_history`
--

DROP TABLE IF EXISTS `cadlib_documents_history`;
CREATE TABLE `cadlib_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `action_by` varchar(32) NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`),
  KEY `K_cadlib_documents_history_number` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_documents_history_seq`
--

DROP TABLE IF EXISTS `cadlib_documents_history_seq`;
CREATE TABLE `cadlib_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_documents_seq`
--

DROP TABLE IF EXISTS `cadlib_documents_seq`;
CREATE TABLE `cadlib_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_files`
--

DROP TABLE IF EXISTS `cadlib_files`;
CREATE TABLE `cadlib_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` mediumtext NOT NULL,
  `file_root_name` varchar(128) NOT NULL DEFAULT '',
  `file_extension` varchar(16) DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) NOT NULL DEFAULT 'init',
  `file_type` varchar(128) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_cadlib_files_1` (`cadlib_id`),
  KEY `FK_cadlib_files_2` (`import_order`),
  KEY `index_file_name` (`file_name`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_import_history`
--

DROP TABLE IF EXISTS `cadlib_import_history`;
CREATE TABLE `cadlib_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_metadata`
--

DROP TABLE IF EXISTS `cadlib_metadata`;
CREATE TABLE `cadlib_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_metadata_seq`
--

DROP TABLE IF EXISTS `cadlib_metadata_seq`;
CREATE TABLE `cadlib_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `cadlib_property_rel`
--

DROP TABLE IF EXISTS `cadlib_property_rel`;
CREATE TABLE `cadlib_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_cadlib_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_cadlib_property_rel_1` (`parent_id`),
  KEY `K_cadlib_property_rel_2` (`parent_uid`),
  KEY `K_cadlib_property_rel_3` (`parent_cid`),
  KEY `K_cadlib_property_rel_4` (`child_id`),
  KEY `K_cadlib_property_rel_5` (`child_uid`),
  KEY `K_cadlib_property_rel_6` (`child_cid`),
  KEY `K_cadlib_property_rel_7` (`rdn`),
  CONSTRAINT `FK_cadlib_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `cadlib_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onCadlibPropertyRelInsert BEFORE INSERT ON cadlib_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;


--
-- Table structure for table `cadlibs`
--

DROP TABLE IF EXISTS `cadlibs`;
CREATE TABLE `cadlibs` (
  `id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e94192201a',
  `dn` varchar(128) DEFAULT NULL,
  `designation` text,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `acode` int(11) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT 'init',
  `version` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` mediumtext NOT NULL,
  `reposit_id` int(11) DEFAULT NULL,
  `reposit_uid` varchar(128) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `planned_closure` datetime DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `close_by_id` int(11) DEFAULT NULL,
  `close_by_uid` varchar(64) DEFAULT NULL,
  `spacename` varchar(16) NOT NULL DEFAULT 'cadlib',
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(6) NOT NULL DEFAULT 'cadlib',
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_cadlibs_number` (`number`),
  UNIQUE KEY `UC_cadlibs_uid` (`uid`),
  KEY `K_cadlibs_3` (`create_by_uid`),
  KEY `K_cadlibs_4` (`close_by_uid`),
  KEY `K_cadlibs_name` (`name`),
  KEY `K_cadlibs_dn` (`dn`),
  KEY `FK_cadlibs_1` (`parent_id`),
  KEY `FK_cadlibs_2` (`default_process_id`),
  KEY `K_cadlibs_6` (`reposit_uid`),
  KEY `FK_cadlibs_3` (`reposit_id`),
  FULLTEXT KEY `FT_cadlibs_number` (`number`,`name`,`designation`),
  CONSTRAINT `FK_cadlibs_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlibs_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlibs_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onCadlibInsert BEFORE INSERT ON cadlibs FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END ;;
DELIMITER ;

DELIMITER ;;
CREATE TRIGGER onCadlibUpdate BEFORE UPDATE ON cadlibs FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(64);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn,uid INTO parentDn,parentUid from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END ;;
DELIMITER ;





DELIMITER ;;
CREATE TRIGGER onCadlibDelete AFTER DELETE ON cadlibs FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='cadlib';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='cadlib';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='cadlib';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END ;;
DELIMITER ;



--
-- Table structure for table `callbacks`
--

DROP TABLE IF EXISTS `callbacks`;
CREATE TABLE `callbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(128) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `reference_id` varchar(128) DEFAULT NULL,
  `reference_uid` varchar(128) DEFAULT NULL,
  `reference_cid` varchar(128) DEFAULT NULL,
  `spacename` varchar(64) DEFAULT NULL,
  `events` varchar(256) DEFAULT NULL,
  `callback_class` varchar(256) DEFAULT NULL,
  `callback_method` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_callbacks_1` (`uid`),
  KEY `INDEX_callbacks_1` (`uid`),
  KEY `INDEX_callbacks_2` (`cid`),
  KEY `INDEX_callbacks_3` (`reference_id`),
  KEY `INDEX_callbacks_4` (`reference_uid`),
  KEY `INDEX_callbacks_5` (`reference_cid`),
  KEY `INDEX_callbacks_8` (`events`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` char(13) NOT NULL DEFAULT '569e918a134ca',
  `dn` varchar(128) DEFAULT NULL,
  `number` varchar(32) DEFAULT NULL,
  `designation` text,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(32) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_categories_uid` (`uid`),
  UNIQUE KEY `UC_categories_number` (`number`),
  UNIQUE KEY `UC_categories_dn` (`dn`),
  KEY `K_categories_parent_id` (`parent_id`),
  KEY `K_categories_parent_uid` (`parent_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `categories_seq`
--

DROP TABLE IF EXISTS `categories_seq`;
CREATE TABLE `categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `checkout_index`
--

DROP TABLE IF EXISTS `checkout_index`;
CREATE TABLE `checkout_index` (
  `file_name` varchar(128) NOT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `file_uid` varchar(140) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `spacename` varchar(128) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `container_number` varchar(128) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_name`),
  KEY `IK_checkout_index_1` (`lock_by_id`),
  KEY `IK_checkout_index_2` (`lock_by_uid`),
  KEY `IK_checkout_index_3` (`file_id`),
  KEY `IK_checkout_index_4` (`file_uid`),
  KEY `IK_checkout_index_5` (`spacename`),
  KEY `IK_checkout_index_6` (`container_id`),
  KEY `IK_checkout_index_7` (`container_number`),
  KEY `IK_checkout_index_8` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `container_favorite`
--

DROP TABLE IF EXISTS `container_favorite`;
CREATE TABLE `container_favorite` (
  `user_id` int(11) NOT NULL,
  `container_id` int(11) NOT NULL,
  `container_number` varchar(512) NOT NULL,
  `space_id` int(11) NOT NULL,
  `spacename` varchar(16) NOT NULL,
  PRIMARY KEY (`user_id`,`container_id`,`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `container_indice`
--

DROP TABLE IF EXISTS `container_indice`;
CREATE TABLE `container_indice` (
  `indice_id` int(11) NOT NULL DEFAULT '0',
  `indice_value` varchar(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`indice_id`),
  UNIQUE KEY `UC_indice_id` (`indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `cron_task`
--

DROP TABLE IF EXISTS `cron_task`;
CREATE TABLE `cron_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(258) DEFAULT NULL,
  `description` varchar(258) DEFAULT NULL,
  `callback_class` text,
  `callback_method` varchar(64) DEFAULT NULL,
  `callback_params` text,
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(128) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `exec_frequency` decimal(10,0) DEFAULT NULL,
  `exec_schedule_start` decimal(10,0) DEFAULT NULL,
  `exec_schedule_end` decimal(10,0) DEFAULT NULL,
  `last_exec` datetime DEFAULT NULL,
  `is_actif` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `K_cron_task_1` (`name`),
  KEY `K_cron_task_2` (`owner_id`),
  KEY `K_cron_task_3` (`owner_uid`),
  KEY `K_cron_task_4` (`status`),
  KEY `K_cron_task_5` (`created`),
  KEY `K_cron_task_6` (`exec_schedule_start`),
  KEY `K_cron_task_7` (`exec_schedule_end`),
  KEY `K_cron_task_8` (`exec_frequency`),
  KEY `K_cron_task_9` (`is_actif`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `discussion_comment`
--

DROP TABLE IF EXISTS `discussion_comment`;
CREATE TABLE `discussion_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` char(16) NOT NULL DEFAULT '568be4fc7a0a8',
  `name` varchar(255) DEFAULT NULL,
  `discussion_uid` varchar(255) NOT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(255) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `body` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `DISCUSSION_COMMENT_PARENTID` (`parent_id`),
  KEY `DISCUSSION_COMMENT_PARENTUID` (`parent_uid`),
  KEY `DISCUSSION_COMMENT_DISCUSSIONUID` (`discussion_uid`),
  KEY `DISCUSSION_COMMENT_OWNERID` (`owner_id`),
  KEY `DISCUSSION_COMMENT_UPDATED` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `docbasket`
--

DROP TABLE IF EXISTS `docbasket`;
CREATE TABLE `docbasket` (
  `user_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `space_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`document_id`,`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `docseeder_mask`
--

DROP TABLE IF EXISTS `docseeder_mask`;
CREATE TABLE `docseeder_mask` (
  `id` int(11) NOT NULL,
  `doctype_id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `map` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `doctype_id` (`doctype_id`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `name` (`name`),
  CONSTRAINT `FK_docseeder_mask_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `docseeder_seq`
--

DROP TABLE IF EXISTS `docseeder_seq`;
CREATE TABLE `docseeder_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `docseeder_type`
--

DROP TABLE IF EXISTS `docseeder_type`;
CREATE TABLE `docseeder_type` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  `designation` varchar(256) DEFAULT NULL,
  `domain` varchar(64) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL COMMENT 'id of the template docfile',
  `template_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the template docfile',
  `mask_id` int(11) DEFAULT NULL COMMENT 'id of the mask to use',
  `mask_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the mask',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `template_id` (`template_id`),
  KEY `template_uid` (`template_uid`),
  KEY `mask_id` (`mask_id`),
  KEY `mask_uid` (`mask_uid`),
  KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `doctypes`
--

DROP TABLE IF EXISTS `doctypes`;
CREATE TABLE `doctypes` (
  `id` int(11) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `number` varchar(128) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `can_be_composite` tinyint(4) NOT NULL DEFAULT '1',
  `file_extensions` varchar(256) DEFAULT NULL,
  `file_type` varchar(128) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `recognition_regexp` mediumtext,
  `visu_file_extension` varchar(32) DEFAULT NULL,
  `pre_store_class` varchar(64) DEFAULT NULL,
  `pre_store_method` varchar(64) DEFAULT NULL,
  `post_store_class` varchar(64) DEFAULT NULL,
  `post_store_method` varchar(64) DEFAULT NULL,
  `pre_update_class` varchar(64) DEFAULT NULL,
  `pre_update_method` varchar(64) DEFAULT NULL,
  `post_update_class` varchar(64) DEFAULT NULL,
  `post_update_method` varchar(64) DEFAULT NULL,
  `number_generator_class` varchar(128) DEFAULT NULL,
  `domain` varchar(64) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL COMMENT 'id of the template docfile',
  `template_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the template docfile',
  `mask_id` int(11) DEFAULT NULL COMMENT 'id of the mask to use',
  `mask_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the mask',
  `docseeder` tinyint(1) DEFAULT NULL COMMENT 'flag to retrieve the valid type for docseeder',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_doctype_number` (`number`),
  UNIQUE KEY `INDEX_doctypes_1` (`uid`),
  UNIQUE KEY `number` (`number`),
  KEY `INDEX_doctypes_2` (`number`),
  KEY `INDEX_doctypes_3` (`designation`),
  KEY `template_id` (`template_id`),
  KEY `template_uid` (`template_uid`),
  KEY `mask_id` (`mask_id`),
  KEY `mask_uid` (`mask_uid`),
  KEY `domain` (`domain`),
  KEY `docseeder` (`docseeder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `doctypes_seq`
--
DROP TABLE IF EXISTS `doctypes_seq`;
CREATE TABLE `doctypes_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

--
-- Table structure for table `document_indice`
--

DROP TABLE IF EXISTS `document_indice`;
CREATE TABLE `document_indice` (
  `indice_id` int(11) NOT NULL DEFAULT '0',
  `indice_value` varchar(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`indice_id`),
  UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `indexer`
--

DROP TABLE IF EXISTS `indexer`;
CREATE TABLE `indexer` (
  `document_id` int(11) NOT NULL,
  `docfile_id` int(11) NOT NULL,
  `spacename` varchar(32) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `content` mediumtext,
  PRIMARY KEY (`docfile_id`,`spacename`),
  UNIQUE KEY `UK_indexer_1` (`document_id`),
  KEY `UK_indexer_2` (`spacename`),
  FULLTEXT KEY `content` (`content`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `message_archive`
--

DROP TABLE IF EXISTS `message_archive`;
CREATE TABLE `message_archive` (
  `id` int(14) NOT NULL,
  `ownerUid` varchar(128) NOT NULL,
  `created` datetime DEFAULT NULL,
  `from` varchar(200) NOT NULL,
  `to` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `hash` varchar(32) DEFAULT NULL,
  `replyto_hash` varchar(32) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `isReplied` tinyint(1) DEFAULT '0',
  `isFlagged` tinyint(1) DEFAULT '0',
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerUid` (`ownerUid`),
  KEY `isRead` (`isRead`),
  KEY `isReplied` (`isReplied`),
  KEY `isFlagged` (`isFlagged`),
  KEY `subject` (`subject`),
  FULLTEXT KEY `body` (`body`,`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `message_mailbox`
--

DROP TABLE IF EXISTS `message_mailbox`;
CREATE TABLE `message_mailbox` (
  `id` int(14) NOT NULL,
  `ownerUid` varchar(128) NOT NULL,
  `created` datetime DEFAULT NULL,
  `from` varchar(200) NOT NULL,
  `to` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `hash` varchar(32) DEFAULT NULL,
  `replyto_hash` varchar(32) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `isReplied` tinyint(1) DEFAULT '0',
  `isFlagged` tinyint(1) DEFAULT '0',
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerUid` (`ownerUid`),
  KEY `isRead` (`isRead`),
  KEY `isReplied` (`isReplied`),
  KEY `isFlagged` (`isFlagged`),
  KEY `subject` (`subject`),
  FULLTEXT KEY `body` (`body`,`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `message_sent`
--

DROP TABLE IF EXISTS `message_sent`;
CREATE TABLE `message_sent` (
  `id` int(14) NOT NULL,
  `ownerUid` varchar(128) NOT NULL,
  `created` datetime DEFAULT NULL,
  `from` varchar(200) NOT NULL,
  `to` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `hash` varchar(32) DEFAULT NULL,
  `replyto_hash` varchar(32) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `isReplied` tinyint(1) DEFAULT '0',
  `isFlagged` tinyint(1) DEFAULT '0',
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerUid` (`ownerUid`),
  KEY `isRead` (`isRead`),
  KEY `isReplied` (`isReplied`),
  KEY `isFlagged` (`isFlagged`),
  KEY `subject` (`subject`),
  FULLTEXT KEY `body` (`body`,`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `message_seq`
--

DROP TABLE IF EXISTS `message_seq`;
CREATE TABLE `message_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_alias`
--

DROP TABLE IF EXISTS `mockup_alias`;
CREATE TABLE `mockup_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT 'mockup',
  `uid` varchar(128) NOT NULL DEFAULT 'mockup',
  `designation` varchar(128) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '45c84a5zalias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `U_mockup_alias_1` (`uid`),
  KEY `K_mockup_alias_1` (`container_id`),
  KEY `K_mockup_alias_2` (`number`),
  KEY `K_mockup_alias_3` (`cid`),
  KEY `K_mockup_alias_4` (`name`),
  CONSTRAINT `FK_mockup_alias_1` FOREIGN KEY (`container_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_category_rel`
--

DROP TABLE IF EXISTS `mockup_category_rel`;
CREATE TABLE `mockup_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_mockup_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_mockup_category_rel_1` (`parent_id`),
  KEY `K_mockup_category_rel_2` (`parent_uid`),
  KEY `K_mockup_category_rel_3` (`parent_cid`),
  KEY `K_mockup_category_rel_4` (`child_id`),
  KEY `K_mockup_category_rel_5` (`child_uid`),
  KEY `K_mockup_category_rel_6` (`child_cid`),
  KEY `K_mockup_category_rel_7` (`rdn`),
  CONSTRAINT `FK_mockup_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onMockupCategoryRelInsert BEFORE INSERT ON mockup_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;





--
-- Table structure for table `mockup_category_rel_seq`
--

DROP TABLE IF EXISTS `mockup_category_rel_seq`;
CREATE TABLE `mockup_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_cont_metadata_seq`
--

DROP TABLE IF EXISTS `mockup_cont_metadata_seq`;
CREATE TABLE `mockup_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_doc_files`
--

DROP TABLE IF EXISTS `mockup_doc_files`;
CREATE TABLE `mockup_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  UNIQUE KEY `INDEX_mockup_doc_files_uid` (`uid`),
  KEY `FK_mockup_doc_files_1` (`document_id`),
  KEY `IK_mockup_doc_files_1` (`document_id`),
  KEY `IK_mockup_doc_files_2` (`name`),
  KEY `IK_mockup_doc_files_3` (`path`(128)),
  KEY `IK_mockup_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_mockup_doc_files_5` (`mainrole`),
  KEY `IK_mockup_doc_files_6` (`roles`(32)),
  KEY `IK_mockup_doc_files_8` (`document_uid`),
  KEY `FK_mockup_docfiles_7` (`create_by_uid`),
  KEY `FK_mockup_docfiles_8` (`lock_by_uid`),
  KEY `FK_mockup_docfiles_9` (`update_by_uid`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_mockup_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `mockup_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onMockupDocfileDelete AFTER DELETE ON mockup_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='mockup';
END ;;
DELIMITER ;





--
-- Table structure for table `mockup_doc_files_seq`
--

DROP TABLE IF EXISTS `mockup_doc_files_seq`;
CREATE TABLE `mockup_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_doc_files_versions`
--

DROP TABLE IF EXISTS `mockup_doc_files_versions`;
CREATE TABLE `mockup_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`(128)),
  KEY `IK_mockup_doc_files_version_1` (`document_id`),
  KEY `IK_mockup_doc_files_version_2` (`name`),
  KEY `IK_mockup_doc_files_version_3` (`path`(128)),
  KEY `IK_mockup_doc_files_version_4` (`iteration`,`document_id`),
  KEY `of_file_id` (`of_file_id`),
  KEY `IK_mockup_doc_files_version_5` (`mainrole`),
  KEY `IK_mockup_doc_files_version_6` (`roles`(32)),
  KEY `IK_mockup_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `mockup_doc_files_versions_seq`;
CREATE TABLE `mockup_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_doc_rel`
--

DROP TABLE IF EXISTS `mockup_doc_rel`;
CREATE TABLE `mockup_doc_rel` (
 `link_id` int(11) NOT NULL,
 `name` varchar(256) default NULL,
 `parent_id` int(11) default NULL,
 `parent_uid` varchar(64) default NULL,
 `parent_space_id` int(11) default NULL,
 `child_id` int(11) default NULL,
 `child_uid` varchar(64) default NULL,
 `child_space_id` int(11) default NULL,
 `acode` int(11) default NULL,
 `lindex` int(11) default 0,
 hash char(32) default NULL,
 data text default NULL,
 PRIMARY KEY (`link_id`),
 UNIQUE KEY `uniq_docid_ldoc_id` (`parent_id`,`child_id`),
 UNIQUE KEY `uniq_docid_lname_id` (`parent_id`,`name`),
 INDEX `_dr_index_01` (`lindex`),
 INDEX `_dr_index_02` (`parent_uid`),
 INDEX `_dr_index_03` (`child_uid`),
 INDEX `_dr_index_04` (`parent_id`, `parent_space_id`),
 INDEX `_dr_index_05` (`child_id`, `child_space_id`),
 CONSTRAINT `FK_mockup_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `FK_mockup_doc_rel_11` FOREIGN KEY (`child_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_doc_rel_seq`
--
DROP TABLE IF EXISTS `mockup_doc_rel_seq`;
CREATE TABLE `mockup_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_doccomments`
--

DROP TABLE IF EXISTS `mockup_doccomments`;
CREATE TABLE `mockup_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_doccomments_seq`
--

DROP TABLE IF EXISTS `mockup_doccomments_seq`;
CREATE TABLE `mockup_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_doctype_process`
--

DROP TABLE IF EXISTS `mockup_doctype_process`;
CREATE TABLE `mockup_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `mockup_doctype_process_uniq1` (`mockup_id`,`doctype_id`),
  KEY `FK_mockup_doctype_process_1` (`category_id`),
  KEY `FK_mockup_doctype_process_3` (`doctype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_doctype_process_seq`
--

DROP TABLE IF EXISTS `mockup_doctype_process_seq`;
CREATE TABLE `mockup_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_doctype_rel`
--

DROP TABLE IF EXISTS `mockup_doctype_rel`;
CREATE TABLE `mockup_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_mockup_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_mockup_doctype_rel_1` (`parent_id`),
  KEY `K_mockup_doctype_rel_2` (`parent_uid`),
  KEY `K_mockup_doctype_rel_3` (`parent_cid`),
  KEY `K_mockup_doctype_rel_4` (`child_id`),
  KEY `K_mockup_doctype_rel_5` (`child_uid`),
  KEY `K_mockup_doctype_rel_6` (`child_cid`),
  KEY `K_mockup_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_mockup_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onMockupDoctypeRelInsert BEFORE INSERT ON mockup_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;





--
-- Table structure for table `mockup_documents`
--

DROP TABLE IF EXISTS `mockup_documents`;
CREATE TABLE `mockup_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `branch_id` int(1) DEFAULT '1',
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'mockup',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tags` varchar(256) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_mockup_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_mockup_documents_uid` (`uid`),
  KEY `FK_mockup_documents_1` (`cad_model_supplier_id`),
  KEY `FK_mockup_documents_2` (`supplier_id`),
  KEY `FK_mockup_documents_3` (`container_id`),
  KEY `FK_mockup_documents_4` (`category_id`),
  KEY `FK_mockup_documents_5` (`doctype_id`),
  KEY `FK_mockup_documents_6` (`version`),
  KEY `INDEX_mockup_documents_spacename` (`spacename`),
  KEY `INDEX_mockup_documents_name` (`name`),
  KEY `INDEX_mockup_documents_cuid` (`container_uid`),
  KEY `FK_mockup_documents_7` (`create_by_uid`),
  KEY `FK_mockup_documents_8` (`lock_by_uid`),
  KEY `FK_mockup_documents_9` (`update_by_uid`),
  KEY `FK_mockup_documents_10` (`as_template`),
  KEY `FK_mockup_documents_11` (`tags`),
  KEY `FK_mockup_documents_12` (`branch_id`),
  FULLTEXT KEY `FT_mockup_documents` (`name`,`number`,`designation`,`tags`),
  CONSTRAINT `FK_mockup_documents_1` FOREIGN KEY (`container_id`) REFERENCES `mockups` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onMockupDocumentDelete AFTER DELETE ON mockup_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=15;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END ;;
DELIMITER ;



--
-- Table structure for table `mockup_documents_history`
--

DROP TABLE IF EXISTS `mockup_documents_history`;
CREATE TABLE `mockup_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `action_by` varchar(32) NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`),
  KEY `K_mockup_documents_history_number` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_documents_history_seq`
--

DROP TABLE IF EXISTS `mockup_documents_history_seq`;
CREATE TABLE `mockup_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_documents_seq`
--

DROP TABLE IF EXISTS `mockup_documents_seq`;
CREATE TABLE `mockup_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_files`
--

DROP TABLE IF EXISTS `mockup_files`;
CREATE TABLE `mockup_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` mediumtext NOT NULL,
  `file_root_name` varchar(128) NOT NULL DEFAULT '',
  `file_extension` varchar(16) DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) NOT NULL DEFAULT 'init',
  `file_type` varchar(128) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_mockup_files_1` (`mockup_id`),
  KEY `FK_mockup_files_2` (`import_order`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`),
  KEY `index_file_name` (`file_name`),
  KEY `file_md5` (`file_md5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_files_seq`
--
DROP TABLE IF EXISTS `mockup_files_seq`;
CREATE TABLE `mockup_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

--
-- Table structure for table `mockup_import_history`
--

DROP TABLE IF EXISTS `mockup_import_history`;


CREATE TABLE `mockup_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` mediumtext,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_import_history_seq`
--
DROP TABLE IF EXISTS `mockup_import_history_seq`;
CREATE TABLE `mockup_import_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_metadata`
--

DROP TABLE IF EXISTS `mockup_metadata`;


CREATE TABLE `mockup_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_metadata_seq`
--

DROP TABLE IF EXISTS `mockup_metadata_seq`;
CREATE TABLE `mockup_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mockup_property_rel`
--

DROP TABLE IF EXISTS `mockup_property_rel`;


CREATE TABLE `mockup_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_mockup_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_mockup_property_rel_1` (`parent_id`),
  KEY `K_mockup_property_rel_2` (`parent_uid`),
  KEY `K_mockup_property_rel_3` (`parent_cid`),
  KEY `K_mockup_property_rel_4` (`child_id`),
  KEY `K_mockup_property_rel_5` (`child_uid`),
  KEY `K_mockup_property_rel_6` (`child_cid`),
  KEY `K_mockup_property_rel_7` (`rdn`),
  CONSTRAINT `FK_mockup_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `mockup_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onMockupPropertyRelInsert BEFORE INSERT ON mockup_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;





--
-- Table structure for table `mockups`
--

DROP TABLE IF EXISTS `mockups`;


CREATE TABLE `mockups` (
  `id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e94192201a',
  `dn` varchar(128) DEFAULT NULL,
  `designation` text,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `acode` int(11) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT 'init',
  `version` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` mediumtext NOT NULL,
  `reposit_id` int(11) DEFAULT NULL,
  `reposit_uid` varchar(128) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `planned_closure` datetime DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `close_by_id` int(11) DEFAULT NULL,
  `close_by_uid` varchar(64) DEFAULT NULL,
  `spacename` varchar(16) NOT NULL DEFAULT 'mockup',
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(6) NOT NULL DEFAULT 'mockup',
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(64) DEFAULT NULL,
  `package_regex` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_mockups_number` (`number`),
  UNIQUE KEY `UC_mockups_uid` (`uid`),
  KEY `K_mockups_3` (`create_by_uid`),
  KEY `K_mockups_4` (`close_by_uid`),
  KEY `K_mockups_name` (`name`),
  KEY `K_mockups_dn` (`dn`),
  KEY `FK_mockups_1` (`parent_id`),
  KEY `FK_mockups_2` (`default_process_id`),
  KEY `K_mockups_6` (`reposit_uid`),
  KEY `FK_mockups_3` (`reposit_id`),
  FULLTEXT KEY `FT_mockups_number` (`number`,`name`,`designation`),
  CONSTRAINT `FK_mockups_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockups_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_mockups_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onMockupInsert BEFORE INSERT ON mockups FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END ;;
DELIMITER ;



DELIMITER ;;
CREATE TRIGGER onMockupUpdate BEFORE UPDATE ON mockups FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE TRIGGER onMockupDelete AFTER DELETE ON mockups FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='mockup';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='mockup';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='mockup';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END ;;
DELIMITER ;





--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;


CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(32) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `owner_uid` varchar(128) NOT NULL,
  `owner_id` varchar(128) NOT NULL,
  `reference_uid` varchar(128) NOT NULL,
  `reference_cid` varchar(128) DEFAULT NULL,
  `reference_id` varchar(128) DEFAULT NULL,
  `spacename` varchar(64) NOT NULL,
  `events` varchar(256) NOT NULL,
  `condition` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_notifications_1` (`owner_id`,`reference_uid`,`events`),
  UNIQUE KEY `uid` (`uid`),
  KEY `INDEX_notifications_1` (`owner_id`),
  KEY `INDEX_notifications_2` (`reference_uid`),
  KEY `INDEX_notifications_3` (`events`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `org_seq`
--

DROP TABLE IF EXISTS `org_seq`;
CREATE TABLE `org_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL DEFAULT '',
  `type` enum('customer','supplier','staff') DEFAULT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `adress` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `cell_phone` varchar(64) DEFAULT NULL,
  `mail` varchar(64) DEFAULT NULL,
  `web_site` varchar(64) DEFAULT NULL,
  `activity` varchar(64) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_partner_number` (`number`),
  KEY `IK_partner_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `partners_seq`
--

DROP TABLE IF EXISTS `partners_seq`;
CREATE TABLE `partners_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `pdm_product_instance`
--

DROP TABLE IF EXISTS `pdm_product_instance`;
CREATE TABLE `pdm_product_instance` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL DEFAULT '569e971bb57c0',
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `nomenclature` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `position` varchar(512) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `of_product_id` int(11) DEFAULT NULL,
  `of_product_uid` varchar(32) DEFAULT NULL,
  `context_id` varchar(32) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdm_product_instance_uniq1` (`uid`),
  KEY `INDEX_pdm_product_instance_1` (`cid`),
  KEY `INDEX_pdm_product_instance_2` (`number`),
  KEY `INDEX_pdm_product_instance_3` (`description`),
  KEY `INDEX_pdm_product_instance_4` (`nomenclature`),
  KEY `INDEX_pdm_product_instance_5` (`uid`),
  KEY `INDEX_pdm_product_instance_6` (`name`),
  KEY `INDEX_pdm_product_instance_7` (`type`),
  KEY `INDEX_pdm_product_version_8` (`of_product_uid`),
  KEY `INDEX_pdm_product_version_9` (`of_product_id`),
  KEY `INDEX_pdm_product_version_10` (`path`),
  FULLTEXT KEY `name` (`name`,`number`,`description`),
  CONSTRAINT `FK_pdm_product_instance_pdm_product_version1` FOREIGN KEY (`of_product_id`) REFERENCES `pdm_product_version` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `pdm_product_version`
--

DROP TABLE IF EXISTS `pdm_product_version`;
CREATE TABLE `pdm_product_version` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `of_product_id` int(11) DEFAULT NULL,
  `of_product_uid` varchar(32) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `document_uid` varchar(32) DEFAULT NULL,
  `spacename` varchar(32) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `materials` varchar(512) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `volume` float DEFAULT NULL,
  `wetsurface` float DEFAULT NULL,
  `density` float DEFAULT NULL,
  `gravitycenter` varchar(512) DEFAULT NULL,
  `inertiacenter` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdm_product_version_uniq2` (`uid`),
  UNIQUE KEY `pdm_product_version_uniq1` (`of_product_uid`,`version`),
  KEY `INDEX_pdm_product_version_1` (`of_product_uid`),
  KEY `INDEX_pdm_product_version_6` (`of_product_id`),
  KEY `INDEX_pdm_product_version_2` (`version`),
  KEY `INDEX_pdm_product_version_3` (`description`),
  KEY `INDEX_pdm_product_version_4` (`uid`),
  KEY `INDEX_pdm_product_version_5` (`name`),
  KEY `INDEX_pdm_product_version_7` (`document_id`),
  KEY `INDEX_pdm_product_version_8` (`document_uid`),
  KEY `INDEX_pdm_product_version_9` (`spacename`),
  KEY `INDEX_pdm_product_instance_10` (`type`),
  FULLTEXT KEY `name` (`name`,`number`,`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `pdm_seq`
--

DROP TABLE IF EXISTS `pdm_seq`;
CREATE TABLE `pdm_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `postit`
--

DROP TABLE IF EXISTS `postit`;
CREATE TABLE `postit` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(32) DEFAULT NULL,
  `parent_cid` varchar(32) NOT NULL,
  `owner_id` varchar(32) DEFAULT NULL,
  `spacename` varchar(32) NOT NULL,
  `body` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_uid` (`uid`),
  KEY `K_parentId` (`parent_id`),
  KEY `K_parentUid` (`parent_uid`),
  KEY `K_parentCid` (`parent_cid`),
  KEY `K_spaceName` (`spacename`),
  KEY `K_ownerId` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `postit_seq`
--
DROP TABLE IF EXISTS `postit_seq`;
CREATE TABLE `postit_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `project_category_rel`
--
DROP TABLE IF EXISTS `project_category_rel`;
CREATE TABLE `project_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_category_rel_1` (`parent_id`),
  KEY `K_project_category_rel_2` (`parent_uid`),
  KEY `K_project_category_rel_3` (`parent_cid`),
  KEY `K_project_category_rel_4` (`child_id`),
  KEY `K_project_category_rel_5` (`child_uid`),
  KEY `K_project_category_rel_6` (`child_cid`),
  KEY `K_project_category_rel_7` (`rdn`),
  CONSTRAINT `FK_project_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_project_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_project_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onOrgCategoryRelInsert BEFORE INSERT ON project_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM doctypes WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END ;;
DELIMITER ;





--
-- Table structure for table `project_container_rel`
--
DROP TABLE IF EXISTS `project_container_rel`;
CREATE TABLE `project_container_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `spacename` varchar(64) NOT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_container_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_container_rel_1` (`parent_id`),
  KEY `K_project_container_rel_2` (`parent_uid`),
  KEY `K_project_container_rel_3` (`parent_cid`),
  KEY `K_project_container_rel_4` (`child_id`),
  KEY `K_project_container_rel_5` (`child_uid`),
  KEY `K_project_container_rel_6` (`child_cid`),
  KEY `K_project_container_rel_7` (`rdn`),
  CONSTRAINT `FK_project_container_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_project_container_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onOrgContainerRelInsert BEFORE INSERT ON project_container_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	CASE NEW.spacename
		WHEN 'bookshop' THEN
			SELECT uid INTO childUid FROM bookshops WHERE id=NEW.child_id;
		WHEN 'cadlib' THEN
			SELECT uid INTO childUid FROM cadlibs WHERE id=NEW.child_id;
		WHEN 'mockup' THEN
			SELECT uid INTO childUid FROM mockups WHERE id=NEW.child_id;
		WHEN 'workitem' THEN
			SELECT uid INTO childUid FROM workitems WHERE id=NEW.child_id;
	END CASE;
			
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END ;;
DELIMITER ;


--
-- Table structure for table `project_doctype_rel`
--
DROP TABLE IF EXISTS `project_doctype_rel`;
CREATE TABLE `project_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  CONSTRAINT `U_project_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `U_project_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `U_project_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `project_process_rel`
--

DROP TABLE IF EXISTS `project_process_rel`;
CREATE TABLE `project_process_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_process_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  KEY `K_project_process_rel_1` (`parent_id`),
  KEY `K_project_process_rel_2` (`parent_uid`),
  KEY `K_project_process_rel_3` (`parent_cid`),
  KEY `K_project_process_rel_4` (`child_id`),
  KEY `K_project_process_rel_5` (`child_uid`),
  KEY `K_project_process_rel_6` (`child_cid`),
  KEY `K_project_process_rel_7` (`rdn`),
  CONSTRAINT `U_project_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `U_project_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `U_project_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onProjectDoctypeRelInsert BEFORE INSERT ON project_process_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM wf_process WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END ;;
DELIMITER ;





--
-- Table structure for table `project_property_rel`
--

DROP TABLE IF EXISTS `project_property_rel`;
CREATE TABLE `project_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  CONSTRAINT `FK_project_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_project_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) NOT NULL,
  `cid` varchar(64) DEFAULT '569e93c6ee156',
  `dn` varchar(128) DEFAULT NULL,
  `number` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `designation` text,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(64) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `planned_closure` datetime DEFAULT NULL,
  `close_by_id` int(11) DEFAULT NULL,
  `close_by_uid` varchar(64) DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IK_project_uid` (`uid`),
  UNIQUE KEY `UC_project_number` (`number`),
  KEY `FK_projects_1` (`version`),
  KEY `UC_project_name` (`name`),
  KEY `UC_project_dn` (`dn`),
  KEY `K_projects_3` (`create_by_uid`),
  KEY `K_projects_4` (`close_by_uid`),
  FULLTEXT KEY `number` (`number`,`name`,`designation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onProjectInsert BEFORE INSERT ON projects FOR EACH ROW 
BEGIN
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentDn VARCHAR(128);

	IF(NEW.parent_id IS NULL) THEN
		SET NEW.dn = CONCAT('/',NEW.uid,'/');
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
	ELSE
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
	END IF;
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END ;;
DELIMITER ;


DELIMITER ;;
CREATE TRIGGER onProjectUpdate BEFORE UPDATE ON projects FOR EACH ROW 
BEGIN
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- update parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		-- update resource
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE referToId=NEW.id AND `referToCid`='569e93c6ee156';
	END IF;
END ;;
DELIMITER ;


DELIMITER ;;
CREATE TRIGGER onProjectDelete AFTER DELETE ON projects FOR EACH ROW 
BEGIN
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END ;;
DELIMITER ;





--
-- Table structure for table `rt_aif_code_a350`
--

DROP TABLE IF EXISTS `rt_aif_code_a350`;
CREATE TABLE `rt_aif_code_a350` (
  `code` char(15) NOT NULL,
  `itp_cluster` char(4) DEFAULT NULL,
  `check` char(2) DEFAULT NULL,
  `mistake` char(2) DEFAULT NULL,
  `type` enum('B','I','OS') NOT NULL,
  `status` char(1) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Codes erreurs pour le programme A350';


--
-- Table structure for table `rt_aif_code_a350_seq`
--

DROP TABLE IF EXISTS `rt_aif_code_a350_seq`;
CREATE TABLE `rt_aif_code_a350_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `rt_aif_code_a380`
--
DROP TABLE IF EXISTS `rt_aif_code_a380`;
CREATE TABLE `rt_aif_code_a380` (
  `status` varchar(24) NOT NULL,
  `code` char(8) NOT NULL,
  `title` mediumtext NOT NULL,
  `itp` varchar(24) NOT NULL,
  `stec` tinyint(4) NOT NULL,
  `stg` tinyint(4) NOT NULL,
  KEY `rt_aif_code_index01` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `search_update`
--

DROP TABLE IF EXISTS `search_update`;
CREATE TABLE `search_update` (
  `updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `sequence_rulesier1`
--

DROP TABLE IF EXISTS `sequence_rulesier1`;
CREATE TABLE `sequence_rulesier1` (
  `sequence` int(11) NOT NULL,
  `parameter1` varchar(64) NOT NULL,
  `parameter2` varchar(64) NOT NULL,
  `parameter3` varchar(64) NOT NULL,
  UNIQUE KEY `parameter1` (`parameter1`,`parameter2`,`parameter3`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `tools_missing_files`
--

DROP TABLE IF EXISTS `tools_missing_files`;
CREATE TABLE `tools_missing_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) NOT NULL,
  `document_id` int(11) NOT NULL,
  `path` text NOT NULL,
  `name` varchar(128) NOT NULL,
  `spacename` varchar(64) DEFAULT NULL,
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `U_tools_missing_files_1` (`name`,`path`(128)),
  KEY `IK_tools_missing_files_2` (`name`),
  KEY `IK_tools_missing_files_3` (`path`(128)),
  KEY `IK_tools_missing_files_4` (`spacename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `user_prefs`
--

DROP TABLE IF EXISTS `user_prefs`;


CREATE TABLE `user_prefs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `css_sheet` varchar(32) DEFAULT NULL,
  `lang` varchar(32) DEFAULT NULL,
  `long_date_format` varchar(32) DEFAULT NULL,
  `short_date_format` varchar(32) DEFAULT NULL,
  `hour_format` varchar(32) DEFAULT NULL,
  `time_zone` varchar(32) DEFAULT NULL,
  `rbgateserver_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_id` (`owner_id`),
  CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`owner_id`) REFERENCES `acl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `user_sessions`
--

DROP TABLE IF EXISTS `user_sessions`;
CREATE TABLE `user_sessions` (
  `user_id` int(11) NOT NULL,
  `datas` longtext,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `vault_reposit`
--

DROP TABLE IF EXISTS `vault_reposit`;
CREATE TABLE `vault_reposit` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `number` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `default_file_path` varchar(512) NOT NULL,
  `type` int(2) NOT NULL,
  `mode` int(2) NOT NULL,
  `actif` int(2) NOT NULL DEFAULT '1',
  `priority` int(2) NOT NULL DEFAULT '1',
  `maxsize` int(11) NOT NULL DEFAULT '50000000',
  `maxcount` int(11) NOT NULL DEFAULT '50000',
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_vault_reposit_1` (`uid`),
  UNIQUE KEY `U_vault_reposit_2` (`number`),
  UNIQUE KEY `U_vault_reposit_3` (`default_file_path`),
  KEY `FK_vault_reposit_1` (`name`),
  KEY `FK_vault_reposit_2` (`description`),
  KEY `FK_vault_reposit_4` (`type`),
  KEY `FK_vault_reposit_5` (`mode`),
  KEY `FK_vault_reposit_6` (`actif`),
  KEY `FK_vault_reposit_7` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `vault_reposit_seq`
--

DROP TABLE IF EXISTS `vault_reposit_seq`;
CREATE TABLE `vault_reposit_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

--
-- Table structure for table `wf_activity`
--

DROP TABLE IF EXISTS `wf_activity`;
CREATE TABLE `wf_activity` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) DEFAULT NULL,
  `cid` varchar(32) NOT NULL DEFAULT '56acc299ed0e4',
  `name` varchar(80) DEFAULT NULL,
  `normalizedName` varchar(255) DEFAULT NULL,
  `processId` int(11) DEFAULT NULL,
  `type` enum('start','end','split','switch','join','activity','standalone') DEFAULT NULL,
  `isAutorouted` tinyint(1) DEFAULT '0',
  `flowNum` int(10) DEFAULT NULL,
  `isInteractive` tinyint(1) DEFAULT '0',
  `lastModif` int(14) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `expirationTime` datetime DEFAULT NULL,
  `isAutomatic` tinyint(1) DEFAULT '0',
  `isComment` tinyint(1) DEFAULT '0',
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `progression` float DEFAULT NULL,
  `roles` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wf_activity_u_uid` (`uid`),
  KEY `name` (`name`),
  KEY `roles` (`roles`(255)),
  KEY `ownerId` (`ownerId`),
  KEY `parentId` (`parentId`),
  KEY `parentUid` (`parentUid`),
  KEY `FK_wf_activity_1` (`processId`),
  CONSTRAINT `FK_wf_activity_1` FOREIGN KEY (`processId`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wf_activity_wf_process1` FOREIGN KEY (`processId`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `wf_document_link`
--

DROP TABLE IF EXISTS `wf_document_link`;
CREATE TABLE `wf_document_link` (
  `uid` varchar(64) NOT NULL,
  `parentId` int(11) NOT NULL,
  `childId` int(11) NOT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `childUid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lindex` int(11) DEFAULT '0',
  `attributes` mediumtext,
  `spacename` varchar(64) NOT NULL,
  PRIMARY KEY (`parentId`,`childId`),
  UNIQUE KEY `uid` (`uid`),
  KEY `DOCPROCESS_UID` (`uid`),
  KEY `DOCPROCESS_NAME` (`name`),
  KEY `DOCPROCESS_LINDEX` (`lindex`),
  KEY `DOCPROCESS_PARENTUID` (`parentUid`),
  KEY `DOCPROCESS_CHILDUID` (`childUid`),
  KEY `DOCPROCESS_PARENTID` (`parentId`),
  KEY `DOCPROCESS_CHILDID` (`childId`),
  CONSTRAINT `FK_wf_document_link_1` FOREIGN KEY (`childId`) REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wf_document_link_wf_instance1` FOREIGN KEY (`childId`) REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `wf_instance`
--

DROP TABLE IF EXISTS `wf_instance`;
CREATE TABLE `wf_instance` (
  `id` int(11) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT '56acc299ed1bd',
  `updated` datetime DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `processId` int(11) DEFAULT NULL,
  `started` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT 'No Name',
  `title` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `nextActivities` mediumtext,
  `nextUsers` mediumtext,
  `ended` datetime DEFAULT NULL,
  `status` varchar(64) DEFAULT NULL,
  `attributes` mediumtext,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wf_instance_u_uid` (`uid`),
  KEY `WFINST_status` (`status`),
  KEY `WFINST_processId` (`processId`),
  KEY `WFINST_uid` (`uid`),
  KEY `WFINST_cid` (`cid`),
  KEY `WFINST_parentId` (`parentId`),
  KEY `WFINST_parentUid` (`parentId`),
  CONSTRAINT `FK_wf_instance_1` FOREIGN KEY (`processId`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wf_instance_wf_process1` FOREIGN KEY (`processId`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `wf_instance_activity`
--

DROP TABLE IF EXISTS `wf_instance_activity`;
CREATE TABLE `wf_instance_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT '56acc299ed22a',
  `name` varchar(128) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `instanceId` int(14) NOT NULL,
  `activityId` int(14) NOT NULL,
  `ownerId` varchar(64) DEFAULT NULL,
  `status` enum('running','completed') DEFAULT NULL,
  `type` enum('start','end','split','switch','join','activity','standalone') DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updateById` varchar(64) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(128) DEFAULT NULL,
  `started` datetime DEFAULT NULL,
  `ended` datetime DEFAULT NULL,
  `attributes` mediumtext,
  `comment` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `WFINSTACTIVITY_uid` (`uid`),
  KEY `WFINSTACTIVITY_name` (`name`),
  KEY `WFINSTACTIVITY_parentuid` (`parentUid`),
  KEY `WFINSTACTIVITY_parentid` (`parentId`),
  KEY `WFINSTACTIVITY_cid` (`cid`),
  KEY `WFINSTACTIVITY_ownerId` (`ownerId`),
  KEY `WFINSTACTIVITY_activityId` (`activityId`),
  KEY `WFINSTACTIVITY_status` (`status`),
  KEY `WFINSTACTIVITY_type` (`type`),
  KEY `WFINSTACTIVITY_started` (`started`),
  KEY `WFINSTACTIVITY_ended` (`ended`),
  KEY `fk_wf_instance_activity_2` (`instanceId`),
  CONSTRAINT `FK_wf_instance_activity_1` FOREIGN KEY (`activityId`) REFERENCES `wf_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_wf_instance_activity_2` FOREIGN KEY (`instanceId`) REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `wf_instance_seq`
--

DROP TABLE IF EXISTS `wf_instance_seq`;
CREATE TABLE `wf_instance_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `wf_process`
--

DROP TABLE IF EXISTS `wf_process`;
CREATE TABLE `wf_process` (
  `id` int(11) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT '56acc299ecd6d',
  `name` varchar(80) DEFAULT NULL,
  `isValid` tinyint(1) DEFAULT '0',
  `isActive` tinyint(1) DEFAULT '0',
  `version` varchar(12) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `normalizedName` varchar(256) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wf_process_u_uid` (`uid`),
  UNIQUE KEY `wf_process_u_normalizedName` (`normalizedName`),
  KEY `wf_process_index_uid` (`uid`),
  KEY `wf_process_index_cid` (`cid`),
  KEY `wf_process_index_name` (`name`),
  KEY `wf_process_index_parentuid` (`parentUid`),
  KEY `wf_process_index_parentid` (`parentId`),
  KEY `wf_process_index_ownerId` (`ownerId`),
  KEY `wf_process_index_isValid` (`isValid`),
  KEY `wf_process_index_isActive` (`isActive`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `wf_seq`
--

DROP TABLE IF EXISTS `wf_seq`;
CREATE TABLE `wf_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `wf_transition`
--

DROP TABLE IF EXISTS `wf_transition`;
CREATE TABLE `wf_transition` (
  `uid` varchar(32) NOT NULL,
  `processId` int(14) NOT NULL,
  `parentId` int(14) NOT NULL,
  `parentUid` varchar(32) NOT NULL,
  `childId` int(14) NOT NULL,
  `childUid` varchar(32) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `lindex` int(7) DEFAULT '0',
  `attributes` mediumtext,
  `cid` varchar(32) NOT NULL DEFAULT '56acc299ed150',
  PRIMARY KEY (`parentId`,`childId`),
  UNIQUE KEY `wf_transition_u_uid` (`uid`),
  KEY `processId` (`processId`),
  KEY `parentId` (`parentId`),
  KEY `parentUid` (`parentUid`),
  KEY `childId` (`childId`),
  KEY `childUid` (`childUid`),
  KEY `lindex` (`lindex`),
  CONSTRAINT `FK_wf_transition_1` FOREIGN KEY (`parentId`) REFERENCES `wf_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wf_transition_2` FOREIGN KEY (`childId`) REFERENCES `wf_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_alias`
--

DROP TABLE IF EXISTS `workitem_alias`;
CREATE TABLE `workitem_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT 'workitem',
  `uid` varchar(128) NOT NULL DEFAULT 'workitem',
  `designation` varchar(128) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '45c84a5zalias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `U_workitem_alias_1` (`uid`),
  KEY `K_workitem_alias_1` (`container_id`),
  KEY `K_workitem_alias_2` (`number`),
  KEY `K_workitem_alias_3` (`cid`),
  KEY `K_workitem_alias_4` (`name`),
  CONSTRAINT `FK_workitem_alias_1` FOREIGN KEY (`container_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_category_rel`
--

DROP TABLE IF EXISTS `workitem_category_rel`;
CREATE TABLE `workitem_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_category_rel_1` (`parent_id`),
  KEY `K_project_category_rel_2` (`parent_uid`),
  KEY `K_project_category_rel_3` (`parent_cid`),
  KEY `K_project_category_rel_4` (`child_id`),
  KEY `K_project_category_rel_5` (`child_uid`),
  KEY `K_project_category_rel_6` (`child_cid`),
  KEY `K_project_category_rel_7` (`rdn`),
  CONSTRAINT `FK_workitem_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onWorkitemCategoryRelInsert BEFORE INSERT ON workitem_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;


--
-- Table structure for table `workitem_cont_metadata_seq`
--

DROP TABLE IF EXISTS `workitem_cont_metadata_seq`;
CREATE TABLE `workitem_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_doc_files`
--

DROP TABLE IF EXISTS `workitem_doc_files`;
CREATE TABLE `workitem_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_file_name` (`name`,`path`),
  UNIQUE KEY `INDEX_workitem_doc_files_uid` (`uid`),
  KEY `FK_workitem_doc_files_1` (`document_id`),
  KEY `IK_workitem_doc_files_1` (`document_id`),
  KEY `IK_workitem_doc_files_2` (`name`),
  KEY `IK_workitem_doc_files_3` (`path`(128)),
  KEY `IK_workitem_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_workitem_doc_files_5` (`mainrole`),
  KEY `IK_workitem_doc_files_6` (`roles`(32)),
  KEY `IK_workitem_doc_files_8` (`document_uid`),
  KEY `FK_workitem_docfiles_7` (`create_by_uid`),
  KEY `FK_workitem_docfiles_8` (`lock_by_uid`),
  KEY `FK_workitem_docfiles_9` (`update_by_uid`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_workitem_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `workitem_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onWIDocfileUpdate AFTER UPDATE ON workitem_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `workitem_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'workitem',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE TRIGGER onWIDocfileDelete AFTER DELETE ON workitem_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='workitem';
END ;;
DELIMITER ;





--
-- Table structure for table `workitem_doc_files_seq`
--

DROP TABLE IF EXISTS `workitem_doc_files_seq`;
CREATE TABLE `workitem_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_doc_files_versions`
--

DROP TABLE IF EXISTS `workitem_doc_files_versions`;
CREATE TABLE `workitem_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`(128)),
  KEY `IK_workitem_doc_files_version_1` (`document_id`),
  KEY `IK_workitem_doc_files_version_2` (`name`),
  KEY `IK_workitem_doc_files_version_3` (`path`(128)),
  KEY `IK_workitem_doc_files_version_4` (`iteration`,`document_id`),
  KEY `IK_workitem_doc_files_version_5` (`mainrole`),
  KEY `IK_workitem_doc_files_version_6` (`roles`(32)),
  KEY `IK_workitem_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `workitem_doc_files_versions_seq`;
CREATE TABLE `workitem_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_doc_rel`
--
DROP TABLE IF EXISTS `workitem_doc_rel`;
CREATE TABLE `workitem_doc_rel` (
 `link_id` int(11) NOT NULL,
 `name` varchar(256) default NULL,
 `parent_id` int(11) default NULL,
 `parent_uid` varchar(64) default NULL,
 `parent_space_id` int(11) default NULL,
 `child_id` int(11) default NULL,
 `child_uid` varchar(64) default NULL,
 `child_space_id` int(11) default NULL,
 `acode` int(11) default NULL,
 `lindex` int(11) default 0,
 hash char(32) default NULL,
 data text default NULL,
 PRIMARY KEY (`link_id`),
 ADD CONSTRAINT `FK_workitem_doc_rel_10` FOREIGN KEY (`parent_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_workitem_doc_rel_10` FOREIGN KEY (`child_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
 UNIQUE KEY `uniq_docid_ldoc_id` (`parent_id`,`child_id`),
 UNIQUE KEY `uniq_docid_lname_id` (`parent_id`,`name`),
 INDEX `_dr_index_01` (`lindex`),
 INDEX `_dr_index_02` (`parent_uid`),
 INDEX `_dr_index_03` (`child_uid`),
 INDEX `_dr_index_04` (`parent_id`, `parent_space_id`),
 INDEX `_dr_index_05` (`child_id`, `child_space_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `workitem_doc_rel_seq`
--

DROP TABLE IF EXISTS `workitem_doc_rel_seq`;
CREATE TABLE `workitem_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_doccomments`
--
DROP TABLE IF EXISTS `workitem_doccomments`;
CREATE TABLE `workitem_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_doccomments_seq`
--
DROP TABLE IF EXISTS `workitem_doccomments_seq`;
CREATE TABLE `workitem_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_doctype_rel`
--

DROP TABLE IF EXISTS `workitem_doctype_rel`;
CREATE TABLE `workitem_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_workitem_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_workitem_doctype_rel_1` (`parent_id`),
  KEY `K_workitem_doctype_rel_2` (`parent_uid`),
  KEY `K_workitem_doctype_rel_3` (`parent_cid`),
  KEY `K_workitem_doctype_rel_4` (`child_id`),
  KEY `K_workitem_doctype_rel_5` (`child_uid`),
  KEY `K_workitem_doctype_rel_6` (`child_cid`),
  KEY `K_workitem_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_workitem_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
CREATE TRIGGER onWorkitemDoctypeRelInsert BEFORE INSERT ON workitem_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;





--
-- Table structure for table `workitem_documents`
--

DROP TABLE IF EXISTS `workitem_documents`;
CREATE TABLE `workitem_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(128) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `branch_id` int(1) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'workitem',
  `doctype_id` int(11) NOT NULL DEFAULT '0',
  `default_process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tags` varchar(256) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `ci` varchar(255) DEFAULT NULL,
  `type_gc` varchar(255) DEFAULT NULL,
  `work_unit` varchar(255) DEFAULT NULL,
  `father_ds` varchar(255) DEFAULT NULL,
  `need_calcul` varchar(255) DEFAULT NULL,
  `ata` varchar(255) DEFAULT NULL,
  `receptionneur` varchar(32) DEFAULT NULL,
  `indice_client` varchar(255) DEFAULT NULL,
  `ext56b26_recept_date` datetime DEFAULT NULL,
  `observation` varchar(255) DEFAULT NULL,
  `ext56b26_date_de_remise` datetime DEFAULT NULL,
  `sub_ata` varchar(255) DEFAULT NULL,
  `work_package` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `fab_process` varchar(255) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `materiau_mak` varchar(255) DEFAULT NULL,
  `masse_mak` double DEFAULT NULL,
  `rt_name` varchar(255) DEFAULT NULL,
  `rt_type` varchar(255) DEFAULT NULL,
  `rt_reason` varchar(255) DEFAULT NULL,
  `ext56b26_rt_emitted` datetime DEFAULT NULL,
  `rt_apply_to` int(11) DEFAULT NULL,
  `modification` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_workitem_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_workitem_documents_uid` (`uid`),
  KEY `FK_workitem_documents_2` (`doctype_id`),
  KEY `FK_workitem_documents_3` (`version`),
  KEY `FK_workitem_documents_4` (`container_id`),
  KEY `FK_workitem_documents_5` (`category_id`),
  KEY `document_name` (`name`),
  KEY `INDEX_workitem_documents_spacename` (`spacename`),
  KEY `INDEX_workitem_documents_name` (`name`),
  KEY `INDEX_workitem_documents_cuid` (`container_uid`),
  KEY `FK_workitem_documents_7` (`create_by_uid`),
  KEY `FK_workitem_documents_8` (`lock_by_uid`),
  KEY `FK_workitem_documents_9` (`update_by_uid`),
  KEY `FK_workitem_documents_10` (`as_template`),
  KEY `FK_workitem_documents_11` (`tags`),
  KEY `FK_workitem_documents_12` (`updated`),
  KEY `FK_workitem_documents_13` (`branch_id`),
  FULLTEXT KEY `FT_workitem_documents` (`name`,`number`,`designation`,`tags`),
  CONSTRAINT `FK_workitem_documents_1` FOREIGN KEY (`container_id`) REFERENCES `workitems` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER ;;
CREATE TRIGGER onWIDocumentDelete AFTER DELETE ON workitem_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=10;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END ;;
DELIMITER ;

-- Table structure for table `workitem_documents_history`
--
DROP TABLE IF EXISTS `workitem_documents_history`;
CREATE TABLE `workitem_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) NOT NULL DEFAULT '',
  `action_by` varchar(64) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `document_id` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`),
  KEY `K_workitem_documents_history_number` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_documents_history_seq`
--
DROP TABLE IF EXISTS `workitem_documents_history_seq`;
CREATE TABLE `workitem_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_documents_seq`
--
DROP TABLE IF EXISTS `workitem_documents_seq`;
CREATE TABLE `workitem_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_import_history`
--
DROP TABLE IF EXISTS `workitem_import_history`;
CREATE TABLE `workitem_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_metadata`
--
DROP TABLE IF EXISTS `workitem_metadata`;
CREATE TABLE `workitem_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_metadata_seq`
--
DROP TABLE IF EXISTS `workitem_metadata_seq`;
CREATE TABLE `workitem_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


--
-- Table structure for table `workitem_property_rel`
--
DROP TABLE IF EXISTS `workitem_property_rel`;
CREATE TABLE `workitem_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_workitem_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_workitem_property_rel_1` (`parent_id`),
  KEY `K_workitem_property_rel_2` (`parent_uid`),
  KEY `K_workitem_property_rel_3` (`parent_cid`),
  KEY `K_workitem_property_rel_4` (`child_id`),
  KEY `K_workitem_property_rel_5` (`child_uid`),
  KEY `K_workitem_property_rel_6` (`child_cid`),
  KEY `K_workitem_property_rel_7` (`rdn`),
  CONSTRAINT `FK_workitem_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `workitem_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER ;;
CREATE TRIGGER onWorkitemPropertyRelInsert BEFORE INSERT ON workitem_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END ;;
DELIMITER ;




--
-- Table structure for table `workitems`
--
DROP TABLE IF EXISTS `workitems`;
CREATE TABLE `workitems` (
  `id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e94192201a',
  `dn` varchar(128) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT 'init',
  `designation` text,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `acode` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(64) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` mediumtext NOT NULL,
  `reposit_id` int(11) DEFAULT NULL,
  `reposit_uid` varchar(128) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `planned_closure` datetime DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `close_by_id` int(11) DEFAULT NULL,
  `close_by_uid` varchar(64) DEFAULT NULL,
  `spacename` varchar(16) NOT NULL DEFAULT 'workitem',
  `affaires_liees` varchar(255) DEFAULT NULL,
  `interloc_client` varchar(255) DEFAULT NULL,
  `interloc_sier` varchar(255) DEFAULT NULL,
  `client` varchar(255) DEFAULT NULL,
  `numero_client` varchar(255) DEFAULT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(8) NOT NULL DEFAULT 'workitem',
  `chef_group` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_workitems_number` (`number`),
  UNIQUE KEY `UC_workitems_uid` (`uid`),
  KEY `K_workitems_3` (`create_by_uid`),
  KEY `K_workitems_4` (`close_by_uid`),
  KEY `K_workitems_1` (`version`),
  KEY `K_workitems_2` (`parent_id`),
  KEY `FK_workitems_2` (`default_process_id`),
  KEY `K_workitems_name` (`name`),
  KEY `K_workitems_dn` (`dn`),
  KEY `K_workitems_6` (`reposit_uid`),
  KEY `FK_workitems_3` (`reposit_id`),
  FULLTEXT KEY `FT_workitems_1` (`number`,`name`,`designation`),
  CONSTRAINT `FK_workitems_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitems_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_workitems_3` FOREIGN KEY (`reposit_id`) REFERENCES `vault_reposit` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER ;;
CREATE TRIGGER onWorkitemInsert BEFORE INSERT ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END ;;
DELIMITER ;


DELIMITER ;;
CREATE TRIGGER onWorkitemUpdate BEFORE UPDATE ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END ;;
DELIMITER ;




DELIMITER ;;
CREATE TRIGGER onWorkitemDelete AFTER DELETE ON workitems FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='workitem';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='workitem';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='workitem';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END ;;
DELIMITER ;


CREATE VIEW `acl_objects_view` AS select `acl_role`.`id` AS `id`,`acl_role`.`uid` AS `uid`,`acl_role`.`name` AS `name`,'acl_role' AS `fromclass` from `acl_role` union select `acl_user`.`id` AS `id`,`acl_user`.`uid` AS `uid`,`acl_user`.`login` AS `name`,'acl_user' AS `fromclass` from `acl_user` union select `acl_group`.`id` AS `id`,`acl_group`.`uid` AS `uid`,`acl_group`.`name` AS `name`,'acl_group' AS `fromclass` from `acl_group` order by `id`;
CREATE VIEW `acl_objects_view` AS select `acl_role`.`id` AS `id`,`acl_role`.`uid` AS `uid`,`acl_role`.`name` AS `name`,'acl_role' AS `fromclass` from `acl_role` union select `acl_user`.`id` AS `id`,`acl_user`.`uid` AS `uid`,`acl_user`.`login` AS `name`,'acl_user' AS `fromclass` from `acl_user` union select `acl_group`.`id` AS `id`,`acl_group`.`uid` AS `uid`,`acl_group`.`name` AS `name`,'acl_group' AS `fromclass` from `acl_group` order by `id`;
CREATE VIEW `view_org_breakdown` AS select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'default' AS `spacename` from `projects` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'workitem' AS `spacename` from `workitems` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'cadlib' AS `spacename` from `cadlibs` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'bookshop' AS `spacename` from `bookshops` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'mockup' AS `spacename` from `mockups` `org`;
CREATE VIEW `view_resource_breakdown` AS (select `doc`.`id` AS `id`,`doc`.`uid` AS `uid`,`doc`.`name` AS `name`,`doc`.`cid` AS `cid`,`cont`.`id` AS `parent_id`,`cont`.`uid` AS `parent_uid` from (`workitem_documents` `doc` join `workitems` `cont` on((`doc`.`container_id` = `cont`.`id`)))) union (select `cont`.`id` AS `id`,`cont`.`uid` AS `uid`,`cont`.`name` AS `name`,`cont`.`cid` AS `cid`,`proj`.`id` AS `parent_id`,`proj`.`uid` AS `parent_uid` from (`workitems` `cont` join `projects` `proj` on((`cont`.`parent_id` = `proj`.`id`)))) union (select `projects`.`id` AS `id`,`projects`.`uid` AS `uid`,`projects`.`name` AS `name`,`projects`.`cid` AS `cid`,`projects`.`parent_id` AS `parent_id`,`projects`.`parent_uid` AS `parent_uid` from `projects`);

CREATE VIEW `view_workitem_doctype_rel` AS SELECT 
	`parent_id`,
	`parent_uid`,
	`child_id`,
	`child_uid`,
	`rdn` FROM `workitem_doctype_rel` 
UNION SELECT 
	`parent_id`,
	`parent_uid`,
	`child_id`,
	`child_uid`,
	`rdn` FROM `project_doctype_rel`;

	
DROP VIEW IF EXISTS `view_workitem_doctype_rel`;
CREATE VIEW view_workitem_doctype_rel AS 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM workitem_doctype_rel 
UNION 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;
	
DROP VIEW IF EXISTS `view_bookshop_doctype_rel`;
CREATE VIEW view_bookshop_doctype_rel AS 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM bookshop_doctype_rel 
UNION 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;

DROP VIEW IF EXISTS `view_cadlib_doctype_rel`;
CREATE VIEW view_cadlib_doctype_rel AS 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM cadlib_doctype_rel 
UNION 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;

DROP VIEW IF EXISTS `view_mockup_doctype_rel`;
CREATE VIEW view_mockup_doctype_rel AS 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM mockup_doctype_rel 
UNION 
SELECT parent_id, parent_uid, child_id, child_uid, rdn FROM project_doctype_rel;
    
--
-- Dumping events for database 'rbsier'
--

--
-- Dumping routines for database 'rbsier'
--
DELIMITER ;;
CREATE FUNCTION `aclSequence`() RETURNS int(11)
BEGIN
	DECLARE lastId INT(11);

	UPDATE acl_resource_seq SET id = LAST_INSERT_ID(id+1) LIMIT 1;
	SELECT LAST_INSERT_ID(id) INTO lastId FROM acl_resource_seq;

    set @ret = lastId;
    RETURN @ret;
END ;;
DELIMITER ;



DELIMITER ;;
CREATE FUNCTION `getResourceCnFromReferIdAndCid`(_id INT(11), _cid VARCHAR(128)) RETURNS varchar(256) CHARSET utf8
BEGIN
	DECLARE resourceCn varchar(128);

	SELECT cn INTO resourceCn FROM acl_resource WHERE referToId = _id AND referToCid = _cid;
    IF(resourceCn IS NULL) THEN
		SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = (SELECT parent_uid FROM view_resource_breakdown WHERE id = _id AND cid=_cid);
	END IF;

	return resourceCn;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE FUNCTION `getResourceCnFromReferUid`(_uid VARCHAR(64)) RETURNS varchar(256) CHARSET utf8
BEGIN
	DECLARE resourceCn varchar(128);

	SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = _uid;
    IF(resourceCn IS NULL) THEN
		SELECT cn INTO resourceCn FROM acl_resource WHERE referToUid = (SELECT parent_uid FROM view_resource_breakdown WHERE uid = _uid);
	END IF;

	return resourceCn;
END ;;
DELIMITER ;



DELIMITER ;;
CREATE PROCEDURE `deleteDocumentsWithoutContainer`()
BEGIN
	CREATE TABLE tmpdoctodelete AS (
		SELECT doc.id, cont.number FROM workitem_documents as doc
		LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id 
		WHERE cont.number is null
	);
	DELETE FROM workitem_doc_files WHERE document_id IN(SELECT id FROM tmpdoctodelete);
	DELETE FROM workitem_documents WHERE id IN(SELECT id FROM tmpdoctodelete);
	DROP TABLE tmpdoctodelete;
END ;;
DELIMITER ;




DELIMITER ;;
CREATE PROCEDURE `getDocumentsWithoutContainer`()
BEGIN
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"workitem" as `spacename`
	FROM workitem_documents as doc
	LEFT OUTER JOIN workitems as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"bookshop" as `spacename`
	FROM bookshop_documents as doc
	LEFT OUTER JOIN bookshops as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"cadlib" as `spacename`
	FROM cadlib_documents as doc
	LEFT OUTER JOIN cadlibs as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL
	UNION
	SELECT
		`doc`.`id` as `id`,
		`doc`.`uid` as `uid`,
		`doc`.`name` as `name`,
		"mockup" as `spacename`
	FROM mockup_documents as doc
	LEFT OUTER JOIN mockups as cont ON doc.container_id=cont.id
	WHERE cont.id IS NULL;
-- ###################################################
END ;;
DELIMITER ;




DELIMITER ;;
CREATE PROCEDURE `getFilesWithoutDocument`()
BEGIN
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"workitem" as `spacename`
	FROM workitem_doc_files as df
	LEFT OUTER JOIN workitem_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"bookshop" as `spacename`
	FROM bookshop_doc_files as df
	LEFT OUTER JOIN bookshop_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"cadlib" as `spacename`
	FROM cadlib_doc_files as df
	LEFT OUTER JOIN cadlib_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL
	UNION
	SELECT
		`df`.`id` as `id`,
		`df`.`uid` as `uid`,
		`df`.`name` as `name`,
		"mockup" as `spacename`
	FROM mockup_doc_files as df
	LEFT OUTER JOIN mockup_documents as doc ON df.document_id=doc.id
	WHERE doc.id IS NULL;
END ;;
DELIMITER ;




DELIMITER ;;
CREATE PROCEDURE `getResourceFromReferUid`(_uid VARCHAR(64))
BEGIN
	DECLARE resourceUid varchar(64);

	SELECT uid INTO resourceUid FROM acl_resource WHERE referToUid = _uid;
    IF(resourceUid IS NULL) THEN
		SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE referToUid = (SELECT parent_uid FROM view_resource_breakdown WHERE uid = _uid);
	ELSE
		SELECT id, uid, cn, referToId, referToUid, referToCid FROM acl_resource WHERE uid = resourceUid;
	END IF;
END ;;
DELIMITER ;




DELIMITER ;;
CREATE PROCEDURE `updateCheckoutIndex`()
BEGIN
	DELETE FROM `checkout_index`;
	INSERT INTO checkout_index(
		SELECT * FROM(
			SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
			FROM workitem_doc_files as file
			JOIN workitem_documents as doc ON file.document_id=doc.id
			where file.acode=1
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM bookshop_doc_files as file
				JOIN bookshop_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM cadlib_doc_files as file
				JOIN cadlib_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM mockup_doc_files as file
				JOIN mockup_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
		) as checkout_index
	);
END ;;
DELIMITER ;



DELIMITER ;;
CREATE PROCEDURE `updateCheckoutIndexFromUserId`(_userId INT(11))
BEGIN
	DECLARE userId INT(11);
	SET @userId=_userId;
	DELETE FROM `checkout_index` WHERE `lock_by_id`=@userId;
	INSERT INTO checkout_index(
		SELECT * FROM(
			SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
			FROM workitem_doc_files as file
			JOIN workitem_documents as doc ON file.document_id=doc.id
			where file.acode=1 and file.lock_by_id=@userId
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM bookshop_doc_files as file
				JOIN bookshop_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM cadlib_doc_files as file
				JOIN cadlib_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM mockup_doc_files as file
				JOIN mockup_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
		) as checkout_index
	);
END ;;
DELIMITER ;


DELIMITER ;;
CREATE PROCEDURE `updateSearch`(_intervalInMn integer)
BEGIN
	SELECT `updated` INTO @updated FROM `search_update`;
    CASE 
		WHEN @updated > (now() + INTERVAL - _intervalInMn minute) THEN 
			SELECT 'is up to date';
			BEGIN
			END;
		ELSE
			DROP TABLE IF EXISTS search;
   			CREATE TABLE search AS SELECT * FROM objects;
			UPDATE `search_update` SET `updated`=now();
	END CASE;
END ;;
DELIMITER ;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
