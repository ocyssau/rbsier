-- add trailing / to resource cn
UPDATE `acl_resource` SET `cn`=CONCAT(`cn`,'/') WHERE `cn` not like '%/';

-- add owner to resource
ALTER TABLE `acl_resource` 
	ADD COLUMN `ownerId` int(11) NULL AFTER `cn`,
	ADD COLUMN `ownerUid` VARCHAR( 64 ) NULL AFTER `ownerId`;
UPDATE 	`acl_resource` SET `ownerId`=1, `ownerUid`='admin';

-- add resource id to rule to join it
ALTER TABLE `acl_rule` 
	ADD COLUMN `resourceId` int(11) NULL AFTER `resource`;
UPDATE 	`acl_rule` SET `resourceId`=(SELECT id from `acl_resource` where `cn`=`acl_rule`.`resource`);

-- ON PROJECT INSERT --
DROP TRIGGER IF EXISTS onProjectInsert;
delimiter $$
CREATE TRIGGER onProjectInsert BEFORE INSERT ON projects FOR EACH ROW 
BEGIN
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentDn VARCHAR(128);

	IF(NEW.parent_id IS NULL) THEN
		SET NEW.dn = CONCAT('/',NEW.uid,'/');
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
	ELSE
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
	END IF;
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END;$$
delimiter ;


-- ON PROJECT UPDATE --
DROP TRIGGER IF EXISTS onProjectUpdate;
delimiter $$
CREATE TRIGGER onProjectUpdate BEFORE UPDATE ON projects FOR EACH ROW 
BEGIN
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- update parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		-- update resource
		set resourceDn = CONCAT('/app/ged/project', NEW.dn);
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE referToId=NEW.id AND `referToCid`='569e93c6ee156';
	END IF;
END;$$
delimiter ;


-- ON CONTAINER INSERT --
DROP TRIGGER IF EXISTS onWorkitemInsert;
delimiter $$
CREATE TRIGGER onWorkitemInsert BEFORE INSERT ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END;$$
delimiter ;

-- ON CONTAINER UPDATE --
DROP TRIGGER IF EXISTS onWorkitemUpdate;
delimiter $$
CREATE TRIGGER onWorkitemUpdate BEFORE UPDATE ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END;$$
delimiter ;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.1.1',now());

