

ALTER TABLE `workitem_documents_history` 
CHANGE COLUMN `action_name` `action_name` VARCHAR(256) NOT NULL DEFAULT '' ;

ALTER TABLE `cadlib_documents_history` 
CHANGE COLUMN `action_name` `action_name` VARCHAR(256) NOT NULL DEFAULT '' ;

ALTER TABLE `bookshop_documents_history` 
CHANGE COLUMN `action_name` `action_name` VARCHAR(256) NOT NULL DEFAULT '' ;

ALTER TABLE `mockup_documents_history` 
CHANGE COLUMN `action_name` `action_name` VARCHAR(256) NOT NULL DEFAULT '' ;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.1.3',now());
