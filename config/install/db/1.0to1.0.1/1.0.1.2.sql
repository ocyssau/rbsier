-- add owner to resource
ALTER TABLE `workitem_alias` 
	CHANGE COLUMN `object_class` `cid` VARCHAR(16) NOT NULL default '45c84a5zalias',
	CHANGE `number` `number` varchar(128)  NOT NULL,
	DROP INDEX INDEX_name, 
	DROP INDEX INDEX_uid,
	DROP INDEX UC_workitem_alias;

UPDATE `workitem_alias` SET 
	cid='45c84a5zalias',
	uid=SUBSTR(UUID(),1,8);

ALTER TABLE `workitem_alias` 
 	ADD KEY `K_workitem_alias_3` (`cid`),
 	ADD KEY `K_workitem_alias_4` (`name`),
 	ADD UNIQUE `U_workitem_alias_1` (`uid`); 	
 	
-- bookshop
ALTER TABLE `bookshop_alias` 
	CHANGE COLUMN `object_class` `cid` VARCHAR(16) NOT NULL default '45c84a5zalias',
	CHANGE `number` `number` varchar(128)  NOT NULL,
	DROP INDEX INDEX_name, 
	DROP INDEX INDEX_uid,
	DROP INDEX UC_bookshop_alias;

UPDATE `bookshop_alias` SET 
	cid='45c84a5zalias',
	uid=SUBSTR(UUID(),1,8);

ALTER TABLE `bookshop_alias` 
 	ADD KEY `K_bookshop_alias_3` (`cid`),
 	ADD KEY `K_bookshop_alias_4` (`name`),
 	ADD UNIQUE `U_bookshop_alias_1` (`uid`);
 	
-- cadlib
ALTER TABLE `cadlib_alias` 
	CHANGE COLUMN `object_class` `cid` VARCHAR(16) NOT NULL default '45c84a5zalias',
	CHANGE `number` `number` varchar(128)  NOT NULL,
	DROP INDEX INDEX_name, 
	DROP INDEX INDEX_uid,
	DROP INDEX UC_cadlib_alias;

UPDATE `cadlib_alias` SET 
	cid='45c84a5zalias',
	uid=SUBSTR(UUID(),1,8);

ALTER TABLE `cadlib_alias` 
 	ADD KEY `K_cadlib_alias_3` (`cid`),
 	ADD KEY `K_cadlib_alias_4` (`name`),
 	ADD UNIQUE `U_cadlib_alias_1` (`uid`);

-- mockup
ALTER TABLE `mockup_alias` 
	CHANGE COLUMN `object_class` `cid` VARCHAR(16) NOT NULL default '45c84a5zalias',
	CHANGE `number` `number` varchar(128)  NOT NULL,
	DROP INDEX INDEX_name, 
	DROP INDEX INDEX_uid,
	DROP INDEX UC_mockup_alias;

UPDATE `mockup_alias` SET 
	cid='45c84a5zalias',
	uid=SUBSTR(UUID(),1,8);

ALTER TABLE `mockup_alias` 
 	ADD KEY `K_mockup_alias_3` (`cid`),
 	ADD KEY `K_mockup_alias_4` (`name`),
 	ADD UNIQUE `U_mockup_alias_1` (`uid`);

INSERT INTO schema_version(version,lastModification) VALUES('1.0.1.2',now());


