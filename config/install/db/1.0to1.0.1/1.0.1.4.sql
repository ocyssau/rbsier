ALTER TABLE `workitem_metadata` 
ADD COLUMN `maybenull` int(1) NOT NULL DEFAULT '0' AFTER `field_multiple`;

ALTER TABLE `cadlib_metadata` 
ADD COLUMN `maybenull` int(1) NOT NULL DEFAULT '0' AFTER `field_multiple`;

ALTER TABLE `bookshop_metadata` 
ADD COLUMN `maybenull` int(1) NOT NULL DEFAULT '0' AFTER `field_multiple`;

ALTER TABLE `mockup_metadata` 
ADD COLUMN `maybenull` int(1) NOT NULL DEFAULT '0' AFTER `field_multiple`;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.1.4',now());
