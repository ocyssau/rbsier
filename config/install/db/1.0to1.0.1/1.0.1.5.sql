ALTER TABLE `doctypes` 
ADD COLUMN `priority` int(5) NOT NULL DEFAULT '0' AFTER `file_type`,
ADD INDEX `INDEX_doctypes_4` (`priority` ASC);

INSERT INTO schema_version(version,lastModification) VALUES('1.0.1.5',now());
