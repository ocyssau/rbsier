-- USER PREFERENCES DROP AND RECREATE
DROP TABLE IF EXISTS `user_prefs`;
CREATE TABLE `user_prefs` (
`id` int(11) AUTO_INCREMENT,
`owner_id` int(11) NOT NULL,
`css_sheet` varchar(32) default NULL,
`lang` varchar(32) default NULL,
`long_date_format` varchar(32) default NULL,
`short_date_format` varchar(32) default NULL,
`hour_format` varchar(32) default NULL,
`time_zone` varchar(32) default NULL,
PRIMARY KEY (`id`),
UNIQUE (`owner_id`)
) ENGINE=InnoDB;

ALTER TABLE `user_prefs`
ADD CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`owner_id`) REFERENCES `acl_user` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

DROP TABLE `docseeder_mask`;
 CREATE TABLE IF NOT EXISTS `docseeder_mask` (
 `id` int(11) NOT NULL,
 `doctype_id` int(11) NOT NULL,
 `uid` varchar(32) NOT NULL,
 `name` varchar(64) NOT NULL DEFAULT '',
 `map` JSON DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY (`doctype_id`),
 UNIQUE KEY (`uid`),
 UNIQUE KEY (`name`)
 ) ENGINE=InnoDB;

ALTER TABLE `docseeder_mask`
 ADD CONSTRAINT `FK_docseeder_mask_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE TABLE `docseeder_seq`(
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 )ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO docseeder_seq(id) VALUES(10);


INSERT INTO schema_version(version,lastModification) VALUES('1.0.3.1',now());

