ALTER TABLE `notifications` MODIFY COLUMN `events` VARCHAR(256) NULL;
ALTER TABLE `callbacks` ADD COLUMN  `is_actif` INT(1) DEFAULT 1 AFTER `description`;
INSERT INTO schema_version(version,lastModification) VALUES('1.2.3',now());
-- select * from schema_version ORDER BY version ASC;
