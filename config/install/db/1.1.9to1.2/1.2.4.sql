ALTER TABLE `container_favorite` 
ADD COLUMN `container_description` MEDIUMTEXT NULL AFTER `container_number`;

CREATE VIEW `view_containers` AS 
	SELECT 
    `workitems`.`id`,
    `workitems`.`number`,
    `workitems`.`name`,
    `workitems`.`uid`,
    `workitems`.`cid`,
    `workitems`.`dn`,
    `workitems`.`life_stage`,
    `workitems`.`designation`,
    `workitems`.`file_only`,
    `workitems`.`acode`,
    `workitems`.`parent_id`,
    `workitems`.`parent_uid`,
    `workitems`.`default_process_id`,
    `workitems`.`default_file_path`,
    `workitems`.`reposit_id`,
    `workitems`.`reposit_uid`,
    `workitems`.`created`,
    `workitems`.`create_by_id`,
    `workitems`.`create_by_uid`,
    `workitems`.`planned_closure`,
    `workitems`.`closed`,
    `workitems`.`close_by_id`,
    `workitems`.`close_by_uid`,
    `workitems`.`spacename`
FROM `workitems`
UNION 
SELECT
    `bookshops`.`id`,
    `bookshops`.`number`,
    `bookshops`.`name`,
    `bookshops`.`uid`,
    `bookshops`.`cid`,
    `bookshops`.`dn`,
    `bookshops`.`life_stage`,
    `bookshops`.`designation`,
    `bookshops`.`file_only`,
    `bookshops`.`acode`,
    `bookshops`.`parent_id`,
    `bookshops`.`parent_uid`,
    `bookshops`.`default_process_id`,
    `bookshops`.`default_file_path`,
    `bookshops`.`reposit_id`,
    `bookshops`.`reposit_uid`,
    `bookshops`.`created`,
    `bookshops`.`create_by_id`,
    `bookshops`.`create_by_uid`,
    `bookshops`.`planned_closure`,
    `bookshops`.`closed`,
    `bookshops`.`close_by_id`,
    `bookshops`.`close_by_uid`,
    `bookshops`.`spacename`
FROM `bookshops`
UNION
SELECT
    `cadlibs`.`id`,
    `cadlibs`.`number`,
    `cadlibs`.`name`,
    `cadlibs`.`uid`,
    `cadlibs`.`cid`,
    `cadlibs`.`dn`,
    `cadlibs`.`life_stage`,
    `cadlibs`.`designation`,
    `cadlibs`.`file_only`,
    `cadlibs`.`acode`,
    `cadlibs`.`parent_id`,
    `cadlibs`.`parent_uid`,
    `cadlibs`.`default_process_id`,
    `cadlibs`.`default_file_path`,
    `cadlibs`.`reposit_id`,
    `cadlibs`.`reposit_uid`,
    `cadlibs`.`created`,
    `cadlibs`.`create_by_id`,
    `cadlibs`.`create_by_uid`,
    `cadlibs`.`planned_closure`,
    `cadlibs`.`closed`,
    `cadlibs`.`close_by_id`,
    `cadlibs`.`close_by_uid`,
    `cadlibs`.`spacename`
FROM `cadlibs`
UNION
SELECT 
    `mockups`.`id`,
    `mockups`.`number`,
    `mockups`.`name`,
    `mockups`.`uid`,
    `mockups`.`cid`,
    `mockups`.`dn`,
    `mockups`.`life_stage`,
    `mockups`.`designation`,
    `mockups`.`file_only`,
    `mockups`.`acode`,
    `mockups`.`parent_id`,
    `mockups`.`parent_uid`,
    `mockups`.`default_process_id`,
    `mockups`.`default_file_path`,
    `mockups`.`reposit_id`,
    `mockups`.`reposit_uid`,
    `mockups`.`created`,
    `mockups`.`create_by_id`,
    `mockups`.`create_by_uid`,
    `mockups`.`planned_closure`,
    `mockups`.`closed`,
    `mockups`.`close_by_id`,
    `mockups`.`close_by_uid`,
    `mockups`.`spacename`
FROM `mockups`;

UPDATE `container_favorite` SET 
`container_description`= (SELECT `designation` FROM `view_containers` WHERE `id`=`container_favorite`.`container_id` AND `spacename` = `container_favorite`.`spacename`);

INSERT INTO schema_version(version,lastModification) VALUES('1.2.4',now());
-- select * from schema_version ORDER BY version ASC;
