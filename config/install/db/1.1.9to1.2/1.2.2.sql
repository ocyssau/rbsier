-- add label column

 ALTER TABLE `workitem_documents`
 ADD COLUMN `label` varchar(255) NULL AFTER `designation`,
 ADD KEY `I_workitem_documents_label` (`label`),
 DROP INDEX `FT_workitem_documents` ,
 ADD FULLTEXT INDEX `FT_workitem_documents` (`name` ASC, `number` ASC, `designation` ASC, `tags` ASC, `label` ASC);

 ALTER TABLE `bookshop_documents`
 ADD COLUMN `label` varchar(255) NULL AFTER `designation`,
 ADD KEY `I_bookshop_documents_label` (`label`),
 DROP INDEX `FT_bookshop_documents` ,
 ADD FULLTEXT INDEX `FT_bookshop_documents` (`name` ASC, `number` ASC, `designation` ASC, `tags` ASC, `label` ASC);

 ALTER TABLE `cadlib_documents`
 ADD COLUMN `label` varchar(255) NULL AFTER `designation`,
 ADD KEY `I_cadlib_documents_label` (`label`),
 DROP INDEX `FT_cadlib_documents` ,
 ADD FULLTEXT INDEX `FT_cadlib_documents` (`name` ASC, `number` ASC, `designation` ASC, `tags` ASC, `label` ASC);
 
 ALTER TABLE `mockup_documents`
 ADD COLUMN `label` varchar(255) NULL AFTER `designation`,
 ADD KEY `I_mockup_documents_label` (`label`),
 DROP INDEX `FT_mockup_documents` ,
 ADD FULLTEXT INDEX `FT_mockup_documents` (`name` ASC, `number` ASC, `designation` ASC, `tags` ASC, `label` ASC);

INSERT INTO schema_version(version,lastModification) VALUES('1.2.2',now());
-- select * from schema_version ORDER BY version ASC;
