ALTER TABLE `vault_reposit`
 ADD COLUMN `spacename` varchar(64) NOT NULL DEFAULT 'default' AFTER `default_file_path`,
 ADD KEY `FK_vault_reposit_8` (`spacename`);

INSERT INTO schema_version(version,lastModification) VALUES('1.2.1',now());
-- select * from schema_version ORDER BY version ASC;
