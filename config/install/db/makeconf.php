<?php
/** First element identified the schema version
 *  next elements are compiled and imported in db in defined order
 */
return [
	'version' => '1.2.2',
	'schema' => [
		'config/install/db/headers.sql',
		#'config/install/db/1-1-9/schema/structure-1-1-9.sql',
		'config/install/db/daoextracted/create.1.2.2.sql',
		'config/install/db/daoextracted/alter.1.2.2.sql',
		'config/install/db/daoextracted/foreignKey.1.2.2.sql',
		'config/install/db/1-1-9/schema/initSeq.sql',
		'config/install/db/daoextracted/trigger.1.2.2.sql',
		'config/install/db/daoextracted/view.1.2.2.sql',
		#'config/install/db/1-1-9/schema/procAndFunctions.sql',
		#'config/install/db/1-1-9/schema/triggers.sql',
		#'config/install/db/1-1-9/schema/views.sql',
		#'config/install/db/1.0.6to1.1/1.1.10.sql',
		#'config/install/db/1.1.9to1.2/1.2.1.sql',
		#'config/install/db/1.1.9to1.2/1.2.2.sql'
	],
	'datas' => [
		'config/install/db/daoextracted/insert.1.2.2.sql',
		'config/install/db/1-1-9/datas/crontasks.sql',
		'config/install/db/1-1-9/datas/callbacks.sql',
		#'config/install/db/1-1-9/datas/spaces.sql',
		#'config/install/db/1-1-9/datas/initAclAndUsers.sql',
		#'config/install/db/1-1-9/datas/doctypes.sql',
		#'config/install/db/1-1-9/datas/indice.sql',
		#'config/install/db/1-1-9/datas/typemimes.sql',
		#'config/install/db/1-1-9/datas/version.sql'
	],
	'results' => [
		'extracted' => 'config/install/db/daoextracted',
		'compiledData' => 'config/install/db/1.2.2.datas.compiled.sql',
		'compiledSchema' => 'config/install/db/1.2.2.schema.compiled.sql'
	]
];
