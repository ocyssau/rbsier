SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adodb_logsql`
--

DROP TABLE IF EXISTS `adodb_logsql`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adodb_logsql` (
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sql0` varchar(250) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `sql1` text COLLATE latin1_general_ci NOT NULL,
  `params` text COLLATE latin1_general_ci NOT NULL,
  `tracer` text COLLATE latin1_general_ci NOT NULL,
  `timer` decimal(16,6) NOT NULL DEFAULT '0.000000'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_alias`
--

DROP TABLE IF EXISTS `bookshop_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `bookshop_description` text COLLATE latin1_general_ci,
  `object_class` varchar(13) COLLATE latin1_general_ci NOT NULL DEFAULT 'bookshopAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_bookshop_alias` (`alias_id`,`bookshop_id`),
  KEY `K_bookshop_alias_1` (`bookshop_id`),
  KEY `K_bookshop_alias_2` (`bookshop_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_alias_seq`
--

DROP TABLE IF EXISTS `bookshop_alias_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_alias_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_categories`
--

DROP TABLE IF EXISTS `bookshop_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_number` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `category_description` text COLLATE latin1_general_ci,
  `category_icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_categories_seq`
--

DROP TABLE IF EXISTS `bookshop_categories_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_category_rel`
--

DROP TABLE IF EXISTS `bookshop_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_bookshop_categories_rel_1` (`category_id`,`bookshop_id`),
  KEY `K_bookshop_categories_rel_1` (`category_id`),
  KEY `K_bookshop_categories_rel_2` (`bookshop_id`),
  CONSTRAINT `bookshop_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `bookshop_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bookshop_category_rel_ibfk_2` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_category_rel_seq`
--

DROP TABLE IF EXISTS `bookshop_category_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_cont_metadata`
--

DROP TABLE IF EXISTS `bookshop_cont_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_cont_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_cont_metadata_seq`
--

DROP TABLE IF EXISTS `bookshop_cont_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files`
--

DROP TABLE IF EXISTS `bookshop_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_used_name` varchar(128) COLLATE latin1_general_ci NOT NULL COMMENT 'nom d usage',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_checkout_by` int(11) DEFAULT NULL,
  `file_checkout_date` int(11) DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(512)),
  KEY `FK_bookshop_doc_files_1` (`document_id`),
  CONSTRAINT `FK_bookshop_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `bookshop_documents` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files_seq`
--

DROP TABLE IF EXISTS `bookshop_doc_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7810 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files_versions`
--

DROP TABLE IF EXISTS `bookshop_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files_versions` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `father_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_path` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`),
  KEY `IK_bookshop_doc_files_version_1` (`father_id`),
  KEY `IK_bookshop_doc_files_version_2` (`file_name`),
  KEY `IK_bookshop_doc_files_version_3` (`file_path`),
  KEY `IK_bookshop_doc_files_version_4` (`file_version`,`father_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `bookshop_doc_files_versions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=584 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_rel`
--

DROP TABLE IF EXISTS `bookshop_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_rel` (
  `dr_link_id` int(11) NOT NULL DEFAULT '0',
  `dr_document_id` int(11) DEFAULT NULL,
  `dr_l_document_id` int(11) DEFAULT NULL,
  `dr_access_code` int(11) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dr_link_id`),
  KEY `FK_bookshop_doc_rel_10` (`dr_document_id`),
  CONSTRAINT `FK_bookshop_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_rel_seq`
--

DROP TABLE IF EXISTS `bookshop_doc_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doccomments`
--

DROP TABLE IF EXISTS `bookshop_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doccomments` (
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `document_id` (`document_id`),
  CONSTRAINT `bookshop_doccomments_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doccomments_seq`
--

DROP TABLE IF EXISTS `bookshop_doccomments_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doctype_process`
--

DROP TABLE IF EXISTS `bookshop_doctype_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `bookshop_doctype_process_uniq1` (`bookshop_id`,`doctype_id`),
  KEY `FK_bookshop_doctype_process_1` (`category_id`),
  KEY `FK_bookshop_doctype_process_3` (`doctype_id`),
  CONSTRAINT `FK_bookshop_doctype_process_1` FOREIGN KEY (`category_id`) REFERENCES `bookshop_categories` (`category_id`),
  CONSTRAINT `FK_bookshop_doctype_process_2` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`),
  CONSTRAINT `FK_bookshop_doctype_process_3` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doctype_process_seq`
--

DROP TABLE IF EXISTS `bookshop_doctype_process_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_documents`
--

DROP TABLE IF EXISTS `bookshop_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_documents` (
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_state` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `document_access_code` int(11) NOT NULL DEFAULT '0',
  `document_version` int(11) NOT NULL DEFAULT '1',
  `bookshop_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doc_space` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT 'bookshop',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `doc_source` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `commentaires` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `applicabilite` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `maj_int_ind` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `min_int_ind` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `mot_cles` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `fournisseur` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `docsier_fournisseur` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `docsier_famille` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
  `docsier_date` int(11) DEFAULT NULL,
  `docsier_keyword` text COLLATE latin1_general_ci,
  `docsier_format` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ata` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `sub_ata` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `a_type` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `orig_filename` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  UNIQUE KEY `IDX_bookshop_documents_1` (`document_number`,`document_indice_id`),
  KEY `FK_bookshop_documents_1` (`cad_model_supplier_id`),
  KEY `FK_bookshop_documents_2` (`supplier_id`),
  KEY `FK_bookshop_documents_3` (`bookshop_id`),
  KEY `FK_bookshop_documents_4` (`category_id`),
  KEY `FK_bookshop_documents_5` (`doctype_id`),
  KEY `FK_bookshop_documents_6` (`document_indice_id`),
  CONSTRAINT `FK_bookshop_documents_1` FOREIGN KEY (`cad_model_supplier_id`) REFERENCES `partners` (`partner_id`),
  CONSTRAINT `FK_bookshop_documents_2` FOREIGN KEY (`supplier_id`) REFERENCES `partners` (`partner_id`),
  CONSTRAINT `FK_bookshop_documents_3` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`),
  CONSTRAINT `FK_bookshop_documents_4` FOREIGN KEY (`category_id`) REFERENCES `bookshop_categories` (`category_id`),
  CONSTRAINT `FK_bookshop_documents_5` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  CONSTRAINT `FK_bookshop_documents_6` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_documents_history`
--

DROP TABLE IF EXISTS `bookshop_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `action_by` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `document_state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `document_access_code` int(11) DEFAULT NULL,
  `document_version` int(11) DEFAULT NULL,
  `bookshop_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_documents_history_seq`
--

DROP TABLE IF EXISTS `bookshop_documents_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14077 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_documents_seq`
--

DROP TABLE IF EXISTS `bookshop_documents_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11763 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_familles`
--

DROP TABLE IF EXISTS `bookshop_familles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_familles` (
  `id` varchar(12) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `famille_name` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_files`
--

DROP TABLE IF EXISTS `bookshop_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_bookshop_files_1` (`bookshop_id`),
  KEY `FK_bookshop_files_2` (`import_order`),
  KEY `index_file_name` (`file_name`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`),
  CONSTRAINT `FK_bookshop_files_1` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`),
  CONSTRAINT `FK_bookshop_files_2` FOREIGN KEY (`import_order`) REFERENCES `bookshop_import_history` (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_history`
--

DROP TABLE IF EXISTS `bookshop_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `bookshop_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `bookshop_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `bookshop_indice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_history_seq`
--

DROP TABLE IF EXISTS `bookshop_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_import_history`
--

DROP TABLE IF EXISTS `bookshop_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `import_date` int(11) DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` text COLLATE latin1_general_ci,
  `import_logfiles_file` text COLLATE latin1_general_ci,
  `package_file_path` text COLLATE latin1_general_ci,
  `package_file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `package_file_mtime` int(11) DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_key_word`
--

DROP TABLE IF EXISTS `bookshop_key_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_key_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_word` varchar(64) COLLATE latin1_german1_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `key_word` (`key_word`)
) ENGINE=InnoDB AUTO_INCREMENT=1516 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_metadata`
--

DROP TABLE IF EXISTS `bookshop_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_metadata_rel`
--

DROP TABLE IF EXISTS `bookshop_metadata_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_metadata_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `bookshop_id` (`bookshop_id`),
  KEY `field_name` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_metadata_rel_seq`
--

DROP TABLE IF EXISTS `bookshop_metadata_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_metadata_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_metadata_seq`
--

DROP TABLE IF EXISTS `bookshop_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshops`
--

DROP TABLE IF EXISTS `bookshops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshops` (
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_number` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `bookshop_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `access_code` int(11) DEFAULT NULL,
  `bookshop_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `bookshop_indice_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci NOT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `container_type` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'bookshop',
  `proprietaire` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT 'bookshop',
  `project_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bookshop_id`),
  UNIQUE KEY `UC_bookshop_number` (`bookshop_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshops_seq`
--

DROP TABLE IF EXISTS `bookshops_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshops_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_alias`
--

DROP TABLE IF EXISTS `cadlib_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `cadlib_description` text COLLATE latin1_general_ci,
  `object_class` varchar(13) COLLATE latin1_general_ci NOT NULL DEFAULT 'cadlibAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_cadlib_alias` (`alias_id`,`cadlib_id`),
  KEY `K_cadlib_alias_1` (`cadlib_id`),
  KEY `K_cadlib_alias_2` (`cadlib_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_alias_seq`
--

DROP TABLE IF EXISTS `cadlib_alias_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_alias_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_categories`
--

DROP TABLE IF EXISTS `cadlib_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_number` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `category_description` text COLLATE latin1_general_ci,
  `category_icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_categories_seq`
--

DROP TABLE IF EXISTS `cadlib_categories_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_category_rel`
--

DROP TABLE IF EXISTS `cadlib_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_cadlib_categories_rel_1` (`category_id`,`cadlib_id`),
  KEY `K_cadlib_categories_rel_1` (`category_id`),
  KEY `K_cadlib_categories_rel_2` (`cadlib_id`),
  CONSTRAINT `cadlib_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `cadlib_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cadlib_category_rel_ibfk_2` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_category_rel_seq`
--

DROP TABLE IF EXISTS `cadlib_category_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_cont_metadata`
--

DROP TABLE IF EXISTS `cadlib_cont_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_cont_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_cont_metadata_seq`
--

DROP TABLE IF EXISTS `cadlib_cont_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_files`
--

DROP TABLE IF EXISTS `cadlib_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_used_name` varchar(128) COLLATE latin1_general_ci NOT NULL COMMENT 'nom d usage',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_checkout_by` int(11) DEFAULT NULL,
  `file_checkout_date` int(11) DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(512)),
  KEY `FK_cadlib_doc_files_1` (`document_id`),
  CONSTRAINT `FK_cadlib_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `cadlib_documents` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_files_seq`
--

DROP TABLE IF EXISTS `cadlib_doc_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_files_versions`
--

DROP TABLE IF EXISTS `cadlib_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_files_versions` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `father_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_path` tinytext COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(128)),
  KEY `IK_cadlib_doc_files_version_1` (`father_id`),
  KEY `IK_cadlib_doc_files_version_2` (`file_name`),
  KEY `IK_cadlib_doc_files_version_3` (`file_path`(128)),
  KEY `IK_cadlib_doc_files_version_4` (`file_version`,`father_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `cadlib_doc_files_versions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_rel`
--

DROP TABLE IF EXISTS `cadlib_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_rel` (
  `dr_link_id` int(11) NOT NULL DEFAULT '0',
  `dr_document_id` int(11) DEFAULT NULL,
  `dr_l_document_id` int(11) DEFAULT NULL,
  `dr_access_code` int(11) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dr_link_id`),
  KEY `FK_cadlib_doc_rel_10` (`dr_document_id`),
  CONSTRAINT `FK_cadlib_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_rel_seq`
--

DROP TABLE IF EXISTS `cadlib_doc_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doccomments`
--

DROP TABLE IF EXISTS `cadlib_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doccomments` (
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `document_id` (`document_id`),
  CONSTRAINT `cadlib_doccomments_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doccomments_seq`
--

DROP TABLE IF EXISTS `cadlib_doccomments_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doctype_process`
--

DROP TABLE IF EXISTS `cadlib_doctype_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `cadlib_doctype_process_uniq1` (`cadlib_id`,`doctype_id`),
  KEY `FK_cadlib_doctype_process_1` (`category_id`),
  KEY `FK_cadlib_doctype_process_3` (`doctype_id`),
  CONSTRAINT `FK_cadlib_doctype_process_1` FOREIGN KEY (`category_id`) REFERENCES `cadlib_categories` (`category_id`),
  CONSTRAINT `FK_cadlib_doctype_process_2` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`),
  CONSTRAINT `FK_cadlib_doctype_process_3` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doctype_process_seq`
--

DROP TABLE IF EXISTS `cadlib_doctype_process_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_documents`
--

DROP TABLE IF EXISTS `cadlib_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_documents` (
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_state` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `document_access_code` int(11) NOT NULL DEFAULT '0',
  `document_version` int(11) NOT NULL DEFAULT '1',
  `cadlib_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doc_space` varchar(6) COLLATE latin1_general_ci NOT NULL DEFAULT 'cadlib',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `fabricant_a` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `fabricant_b` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `fabricant_c` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ref_a` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ref_b` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ref_c` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `chapitre` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `subchapitre` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  UNIQUE KEY `IDX_cadlib_documents_1` (`document_number`,`document_indice_id`),
  KEY `FK_cadlib_documents_1` (`cad_model_supplier_id`),
  KEY `FK_cadlib_documents_2` (`supplier_id`),
  KEY `FK_cadlib_documents_3` (`cadlib_id`),
  KEY `FK_cadlib_documents_4` (`category_id`),
  KEY `FK_cadlib_documents_5` (`doctype_id`),
  KEY `FK_cadlib_documents_6` (`document_indice_id`),
  CONSTRAINT `FK_cadlib_documents_1` FOREIGN KEY (`cad_model_supplier_id`) REFERENCES `partners` (`partner_id`),
  CONSTRAINT `FK_cadlib_documents_2` FOREIGN KEY (`supplier_id`) REFERENCES `partners` (`partner_id`),
  CONSTRAINT `FK_cadlib_documents_3` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`),
  CONSTRAINT `FK_cadlib_documents_4` FOREIGN KEY (`category_id`) REFERENCES `cadlib_categories` (`category_id`),
  CONSTRAINT `FK_cadlib_documents_5` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  CONSTRAINT `FK_cadlib_documents_6` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_documents_history`
--

DROP TABLE IF EXISTS `cadlib_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `action_by` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `document_state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `document_access_code` int(11) DEFAULT NULL,
  `document_version` int(11) DEFAULT NULL,
  `cadlib_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_documents_history_seq`
--

DROP TABLE IF EXISTS `cadlib_documents_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_documents_seq`
--

DROP TABLE IF EXISTS `cadlib_documents_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_files`
--

DROP TABLE IF EXISTS `cadlib_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_cadlib_files_1` (`cadlib_id`),
  KEY `FK_cadlib_files_2` (`import_order`),
  KEY `index_file_name` (`file_name`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`),
  CONSTRAINT `FK_cadlib_files_1` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`),
  CONSTRAINT `FK_cadlib_files_2` FOREIGN KEY (`import_order`) REFERENCES `cadlib_import_history` (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_history`
--

DROP TABLE IF EXISTS `cadlib_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `cadlib_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `cadlib_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `cadlib_indice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_history_seq`
--

DROP TABLE IF EXISTS `cadlib_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_import_history`
--

DROP TABLE IF EXISTS `cadlib_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `import_date` int(11) DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` text COLLATE latin1_general_ci,
  `import_logfiles_file` text COLLATE latin1_general_ci,
  `package_file_path` text COLLATE latin1_general_ci,
  `package_file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `package_file_mtime` int(11) DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_metadata`
--

DROP TABLE IF EXISTS `cadlib_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_metadata_rel`
--

DROP TABLE IF EXISTS `cadlib_metadata_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_metadata_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `cadlib_metadata_rel_uniq` (`cadlib_id`,`field_name`),
  KEY `cadlib_id` (`cadlib_id`),
  KEY `field_name` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_metadata_rel_seq`
--

DROP TABLE IF EXISTS `cadlib_metadata_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_metadata_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_metadata_seq`
--

DROP TABLE IF EXISTS `cadlib_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlibs`
--

DROP TABLE IF EXISTS `cadlibs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlibs` (
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_number` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `cadlib_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `access_code` int(11) DEFAULT NULL,
  `cadlib_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `cadlib_indice_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci NOT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `container_type` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'cadlib',
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(6) COLLATE latin1_general_ci NOT NULL DEFAULT 'cadlib',
  `project_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cadlib_id`),
  UNIQUE KEY `UC_cadlib_number` (`cadlib_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlibs_seq`
--

DROP TABLE IF EXISTS `cadlibs_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlibs_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `checkout_index`
--

DROP TABLE IF EXISTS `checkout_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkout_index` (
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `container_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `container_id` int(11) NOT NULL DEFAULT '0',
  `container_number` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `check_out_by` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_favorite`
--

DROP TABLE IF EXISTS `container_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_favorite` (
  `user_id` int(11) NOT NULL,
  `container_id` int(11) NOT NULL,
  `container_number` varchar(512) COLLATE latin1_general_ci NOT NULL,
  `space_id` int(11) NOT NULL,
  `space_name` varchar(128) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`user_id`,`container_id`,`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_indice`
--

DROP TABLE IF EXISTS `container_indice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_indice` (
  `indice_id` int(11) NOT NULL DEFAULT '0',
  `indice_value` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`indice_id`),
  UNIQUE KEY `UC_indice_id` (`indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `docbasket`
--

DROP TABLE IF EXISTS `docbasket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docbasket` (
  `user_id` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL,
  `space_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`doc_id`,`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `doctypes`
--

DROP TABLE IF EXISTS `doctypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctypes` (
  `doctype_id` int(11) NOT NULL DEFAULT '0',
  `doctype_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `doctype_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `can_be_composite` tinyint(4) NOT NULL DEFAULT '1',
  `file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `file_type` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `script_post_store` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `script_pre_store` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `script_post_update` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `script_pre_update` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `recognition_regexp` text COLLATE latin1_general_ci,
  `visu_file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`doctype_id`),
  UNIQUE KEY `UC_doctype_number` (`doctype_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `doctypes_seq`
--

DROP TABLE IF EXISTS `doctypes_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctypes_seq` (
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document_indice`
--

DROP TABLE IF EXISTS `document_indice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_indice` (
  `document_indice_id` int(11) NOT NULL DEFAULT '0',
  `indice_value` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`document_indice_id`),
  UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_activities`
--

DROP TABLE IF EXISTS `galaxia_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_activities` (
  `activityId` int(14) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `normalized_name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `pId` int(14) NOT NULL DEFAULT '0',
  `type` enum('start','end','split','switch','join','activity','standalone') COLLATE latin1_general_ci DEFAULT NULL,
  `isAutoRouted` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `flowNum` int(10) DEFAULT NULL,
  `isInteractive` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `lastModif` int(14) DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  `expirationTime` int(6) unsigned NOT NULL DEFAULT '0',
  `isAutomatic` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isComment` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`activityId`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_activity_roles`
--

DROP TABLE IF EXISTS `galaxia_activity_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_activity_roles` (
  `activityId` int(14) NOT NULL DEFAULT '0',
  `roleId` int(14) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activityId`,`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_instance_activities`
--

DROP TABLE IF EXISTS `galaxia_instance_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_instance_activities` (
  `instanceId` int(14) NOT NULL DEFAULT '0',
  `activityId` int(14) NOT NULL DEFAULT '0',
  `started` int(14) NOT NULL DEFAULT '0',
  `ended` int(14) NOT NULL DEFAULT '0',
  `user` varchar(40) COLLATE latin1_general_ci DEFAULT NULL,
  `status` enum('running','completed') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`instanceId`,`activityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_instance_comments`
--

DROP TABLE IF EXISTS `galaxia_instance_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_instance_comments` (
  `cId` int(14) NOT NULL AUTO_INCREMENT,
  `instanceId` int(14) NOT NULL DEFAULT '0',
  `user` varchar(40) COLLATE latin1_general_ci DEFAULT NULL,
  `activityId` int(14) DEFAULT NULL,
  `hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `title` varchar(250) COLLATE latin1_general_ci DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  `activity` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `timestamp` int(14) DEFAULT NULL,
  PRIMARY KEY (`cId`)
) ENGINE=InnoDB AUTO_INCREMENT=581 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_instance_properties`
--

DROP TABLE IF EXISTS `galaxia_instance_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_instance_properties` (
  `iid` int(11) NOT NULL,
  `a_verifier_user` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `previousActivity` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `previousUser` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `checkresult` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `ecodes` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `iid` (`iid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_instances`
--

DROP TABLE IF EXISTS `galaxia_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_instances` (
  `instanceId` int(14) NOT NULL AUTO_INCREMENT,
  `pId` int(14) NOT NULL DEFAULT '0',
  `started` int(14) DEFAULT NULL,
  `name` varchar(200) COLLATE latin1_general_ci DEFAULT 'No Name',
  `owner` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `nextActivity` int(14) DEFAULT NULL,
  `nextUser` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `ended` int(14) DEFAULT NULL,
  `status` enum('active','exception','aborted','completed') COLLATE latin1_general_ci DEFAULT NULL,
  `properties` longblob,
  PRIMARY KEY (`instanceId`)
) ENGINE=InnoDB AUTO_INCREMENT=111861 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_processes`
--

DROP TABLE IF EXISTS `galaxia_processes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_processes` (
  `pId` int(14) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `isValid` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isActive` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `version` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  `lastModif` int(14) DEFAULT NULL,
  `normalized_name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_roles`
--

DROP TABLE IF EXISTS `galaxia_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_roles` (
  `roleId` int(14) NOT NULL AUTO_INCREMENT,
  `pId` int(14) NOT NULL DEFAULT '0',
  `lastModif` int(14) DEFAULT NULL,
  `name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_transitions`
--

DROP TABLE IF EXISTS `galaxia_transitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_transitions` (
  `pId` int(14) NOT NULL DEFAULT '0',
  `actFromId` int(14) NOT NULL DEFAULT '0',
  `actToId` int(14) NOT NULL DEFAULT '0',
  PRIMARY KEY (`actFromId`,`actToId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_user_roles`
--

DROP TABLE IF EXISTS `galaxia_user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_user_roles` (
  `pId` int(14) NOT NULL DEFAULT '0',
  `roleId` int(14) NOT NULL AUTO_INCREMENT,
  `user` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`roleId`,`user`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galaxia_workitems`
--

DROP TABLE IF EXISTS `galaxia_workitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galaxia_workitems` (
  `itemId` int(14) NOT NULL AUTO_INCREMENT,
  `instanceId` int(14) NOT NULL DEFAULT '0',
  `orderId` int(14) NOT NULL DEFAULT '0',
  `activityId` int(14) NOT NULL DEFAULT '0',
  `properties` longblob,
  `started` int(14) DEFAULT NULL,
  `ended` int(14) DEFAULT NULL,
  `user` varchar(40) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`itemId`)
) ENGINE=InnoDB AUTO_INCREMENT=469551 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_applications`
--

DROP TABLE IF EXISTS `liveuser_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_applications` (
  `application_id` int(11) DEFAULT '0',
  `application_define_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `application_id_idx` (`application_id`),
  UNIQUE KEY `define_name_i_idx` (`application_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_area_admin_areas`
--

DROP TABLE IF EXISTS `liveuser_area_admin_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_area_admin_areas` (
  `area_id` int(11) DEFAULT '0',
  `perm_user_id` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`area_id`,`perm_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_areas`
--

DROP TABLE IF EXISTS `liveuser_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_areas` (
  `area_id` int(11) DEFAULT '0',
  `application_id` int(11) DEFAULT '0',
  `area_define_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `area_id_idx` (`area_id`),
  UNIQUE KEY `define_name_i_idx` (`application_id`,`area_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_areas_seq`
--

DROP TABLE IF EXISTS `liveuser_areas_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_areas_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_group_subgroups`
--

DROP TABLE IF EXISTS `liveuser_group_subgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_group_subgroups` (
  `group_id` int(11) DEFAULT '0',
  `subgroup_id` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`group_id`,`subgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_grouprights`
--

DROP TABLE IF EXISTS `liveuser_grouprights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_grouprights` (
  `group_id` int(11) DEFAULT '0',
  `right_id` int(11) DEFAULT '0',
  `right_level` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`group_id`,`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_groups`
--

DROP TABLE IF EXISTS `liveuser_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_groups` (
  `group_id` int(11) DEFAULT '0',
  `group_type` int(11) DEFAULT '0',
  `group_define_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `group_description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `owner_user_id` int(11) DEFAULT '0',
  `owner_group_id` int(11) DEFAULT '0',
  UNIQUE KEY `group_id_idx` (`group_id`),
  UNIQUE KEY `define_name_i_idx` (`group_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_groups_seq`
--

DROP TABLE IF EXISTS `liveuser_groups_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_groups_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_groupusers`
--

DROP TABLE IF EXISTS `liveuser_groupusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_groupusers` (
  `perm_user_id` int(11) DEFAULT '0',
  `group_id` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`perm_user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_perm_users`
--

DROP TABLE IF EXISTS `liveuser_perm_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_perm_users` (
  `perm_user_id` int(11) DEFAULT '0',
  `auth_user_id` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `auth_container_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `perm_type` int(11) DEFAULT '0',
  UNIQUE KEY `perm_user_id_idx` (`perm_user_id`),
  UNIQUE KEY `auth_id_i_idx` (`auth_user_id`,`auth_container_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_perm_users_seq`
--

DROP TABLE IF EXISTS `liveuser_perm_users_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_perm_users_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_right_implied`
--

DROP TABLE IF EXISTS `liveuser_right_implied`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_right_implied` (
  `right_id` int(11) DEFAULT '0',
  `implied_right_id` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`right_id`,`implied_right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_rights`
--

DROP TABLE IF EXISTS `liveuser_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_rights` (
  `right_id` int(11) DEFAULT '0',
  `area_id` int(11) DEFAULT '0',
  `right_define_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `right_description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `has_implied` tinyint(1) DEFAULT '1',
  UNIQUE KEY `right_id_idx` (`right_id`),
  UNIQUE KEY `define_name_i_idx` (`area_id`,`right_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_rights_seq`
--

DROP TABLE IF EXISTS `liveuser_rights_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_rights_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=821 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_translations`
--

DROP TABLE IF EXISTS `liveuser_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_translations` (
  `translation_id` int(11) DEFAULT '0',
  `section_id` int(11) DEFAULT '0',
  `section_type` int(11) DEFAULT '0',
  `language_id` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `description` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `translation_id_idx` (`translation_id`),
  UNIQUE KEY `translation_i_idx` (`section_id`,`section_type`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_userrights`
--

DROP TABLE IF EXISTS `liveuser_userrights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_userrights` (
  `perm_user_id` int(11) DEFAULT '0',
  `right_id` int(11) DEFAULT '0',
  `right_level` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`perm_user_id`,`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_users`
--

DROP TABLE IF EXISTS `liveuser_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_users` (
  `auth_user_id` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `handle` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `passwd` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `email` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `owner_user_id` int(11) DEFAULT '0',
  `owner_group_id` int(11) DEFAULT '0',
  `lastlogin` datetime DEFAULT '1970-01-01 00:00:00',
  `is_active` tinyint(1) DEFAULT '1',
  UNIQUE KEY `auth_user_id_idx` (`auth_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liveuser_users_seq`
--

DROP TABLE IF EXISTS `liveuser_users_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liveuser_users_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `messu_archive`
--

DROP TABLE IF EXISTS `messu_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messu_archive` (
  `msgId` int(14) NOT NULL AUTO_INCREMENT,
  `user` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_from` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_to` text COLLATE latin1_general_ci,
  `user_cc` text COLLATE latin1_general_ci,
  `user_bcc` text COLLATE latin1_general_ci,
  `subject` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `body` text COLLATE latin1_general_ci,
  `hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `replyto_hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date` int(14) DEFAULT NULL,
  `isRead` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isReplied` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isFlagged` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`msgId`)
) ENGINE=InnoDB AUTO_INCREMENT=117783 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `messu_messages`
--

DROP TABLE IF EXISTS `messu_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messu_messages` (
  `msgId` int(14) NOT NULL AUTO_INCREMENT,
  `user` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_from` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_to` text COLLATE latin1_general_ci,
  `user_cc` text COLLATE latin1_general_ci,
  `user_bcc` text COLLATE latin1_general_ci,
  `subject` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `body` text COLLATE latin1_general_ci,
  `hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `replyto_hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date` int(14) DEFAULT NULL,
  `isRead` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isReplied` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isFlagged` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`msgId`)
) ENGINE=MyISAM AUTO_INCREMENT=149812 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `messu_sent`
--

DROP TABLE IF EXISTS `messu_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messu_sent` (
  `msgId` int(14) NOT NULL AUTO_INCREMENT,
  `user` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_from` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_to` text COLLATE latin1_general_ci,
  `user_cc` text COLLATE latin1_general_ci,
  `user_bcc` text COLLATE latin1_general_ci,
  `subject` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `body` text COLLATE latin1_general_ci,
  `hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `replyto_hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date` int(14) DEFAULT NULL,
  `isRead` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isReplied` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isFlagged` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`msgId`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_alias`
--

DROP TABLE IF EXISTS `mockup_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `mockup_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `mockup_description` text COLLATE latin1_general_ci,
  `object_class` varchar(13) COLLATE latin1_general_ci NOT NULL DEFAULT 'mockupAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_mockup_alias` (`alias_id`,`mockup_id`),
  KEY `K_mockup_alias_1` (`mockup_id`),
  KEY `K_mockup_alias_2` (`mockup_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_alias_seq`
--

DROP TABLE IF EXISTS `mockup_alias_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_alias_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_categories`
--

DROP TABLE IF EXISTS `mockup_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_number` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `category_description` text COLLATE latin1_general_ci,
  `category_icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_categories_seq`
--

DROP TABLE IF EXISTS `mockup_categories_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_category_rel`
--

DROP TABLE IF EXISTS `mockup_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_mockup_categories_rel_1` (`category_id`,`mockup_id`),
  KEY `K_mockup_categories_rel_1` (`category_id`),
  KEY `K_mockup_categories_rel_2` (`mockup_id`),
  CONSTRAINT `mockup_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `mockup_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mockup_category_rel_ibfk_2` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_category_rel_seq`
--

DROP TABLE IF EXISTS `mockup_category_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_cont_metadata`
--

DROP TABLE IF EXISTS `mockup_cont_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_cont_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_cont_metadata_seq`
--

DROP TABLE IF EXISTS `mockup_cont_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_files`
--

DROP TABLE IF EXISTS `mockup_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_used_name` varchar(128) COLLATE latin1_general_ci NOT NULL COMMENT 'nom d usage',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_checkout_by` int(11) DEFAULT NULL,
  `file_checkout_date` int(11) DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(512)),
  KEY `FK_mockup_doc_files_1` (`document_id`),
  CONSTRAINT `FK_mockup_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `mockup_documents` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_files_seq`
--

DROP TABLE IF EXISTS `mockup_doc_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_files_versions`
--

DROP TABLE IF EXISTS `mockup_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_files_versions` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `father_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_path` tinytext COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(128)),
  KEY `IK_mockup_doc_files_version_1` (`father_id`),
  KEY `IK_mockup_doc_files_version_2` (`file_name`),
  KEY `IK_mockup_doc_files_version_3` (`file_path`(128)),
  KEY `IK_mockup_doc_files_version_4` (`file_version`,`father_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `mockup_doc_files_versions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_rel`
--

DROP TABLE IF EXISTS `mockup_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_rel` (
  `dr_link_id` int(11) NOT NULL DEFAULT '0',
  `dr_document_id` int(11) DEFAULT NULL,
  `dr_l_document_id` int(11) DEFAULT NULL,
  `dr_access_code` int(11) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dr_link_id`),
  KEY `FK_mockup_doc_rel_10` (`dr_document_id`),
  CONSTRAINT `FK_mockup_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_rel_seq`
--

DROP TABLE IF EXISTS `mockup_doc_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doccomments`
--

DROP TABLE IF EXISTS `mockup_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doccomments` (
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `document_id` (`document_id`),
  CONSTRAINT `mockup_doccomments_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doccomments_seq`
--

DROP TABLE IF EXISTS `mockup_doccomments_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doctype_process`
--

DROP TABLE IF EXISTS `mockup_doctype_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `mockup_doctype_process_uniq1` (`mockup_id`,`doctype_id`),
  KEY `FK_mockup_doctype_process_1` (`category_id`),
  KEY `FK_mockup_doctype_process_3` (`doctype_id`),
  CONSTRAINT `FK_mockup_doctype_process_1` FOREIGN KEY (`category_id`) REFERENCES `mockup_categories` (`category_id`),
  CONSTRAINT `FK_mockup_doctype_process_2` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`),
  CONSTRAINT `FK_mockup_doctype_process_3` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doctype_process_seq`
--

DROP TABLE IF EXISTS `mockup_doctype_process_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_documents`
--

DROP TABLE IF EXISTS `mockup_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_documents` (
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_state` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `document_access_code` int(11) NOT NULL DEFAULT '0',
  `document_version` int(11) NOT NULL DEFAULT '1',
  `mockup_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doc_space` varchar(6) COLLATE latin1_general_ci NOT NULL DEFAULT 'mockup',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  UNIQUE KEY `IDX_mockup_documents_1` (`document_number`,`document_indice_id`),
  KEY `FK_mockup_documents_1` (`cad_model_supplier_id`),
  KEY `FK_mockup_documents_2` (`supplier_id`),
  KEY `FK_mockup_documents_3` (`mockup_id`),
  KEY `FK_mockup_documents_4` (`category_id`),
  KEY `FK_mockup_documents_5` (`doctype_id`),
  KEY `FK_mockup_documents_6` (`document_indice_id`),
  CONSTRAINT `FK_mockup_documents_1` FOREIGN KEY (`cad_model_supplier_id`) REFERENCES `partners` (`partner_id`),
  CONSTRAINT `FK_mockup_documents_2` FOREIGN KEY (`supplier_id`) REFERENCES `partners` (`partner_id`),
  CONSTRAINT `FK_mockup_documents_3` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`),
  CONSTRAINT `FK_mockup_documents_4` FOREIGN KEY (`category_id`) REFERENCES `mockup_categories` (`category_id`),
  CONSTRAINT `FK_mockup_documents_5` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  CONSTRAINT `FK_mockup_documents_6` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_documents_history`
--

DROP TABLE IF EXISTS `mockup_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `action_by` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `document_state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `document_access_code` int(11) DEFAULT NULL,
  `document_version` int(11) DEFAULT NULL,
  `mockup_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_documents_history_seq`
--

DROP TABLE IF EXISTS `mockup_documents_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_documents_seq`
--

DROP TABLE IF EXISTS `mockup_documents_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_files`
--

DROP TABLE IF EXISTS `mockup_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_mockup_files_1` (`mockup_id`),
  KEY `FK_mockup_files_2` (`import_order`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`),
  KEY `index_file_name` (`file_name`),
  KEY `file_md5` (`file_md5`),
  CONSTRAINT `FK_mockup_files_2` FOREIGN KEY (`import_order`) REFERENCES `mockup_import_history` (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_files_seq`
--

DROP TABLE IF EXISTS `mockup_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_files_seq` (
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_history`
--

DROP TABLE IF EXISTS `mockup_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `mockup_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `mockup_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `mockup_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `mockup_indice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_history_seq`
--

DROP TABLE IF EXISTS `mockup_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_import_history`
--

DROP TABLE IF EXISTS `mockup_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` text COLLATE latin1_general_ci,
  `state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `import_date` int(11) DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` text COLLATE latin1_general_ci,
  `import_logfiles_file` text COLLATE latin1_general_ci,
  `package_file_path` text COLLATE latin1_general_ci,
  `package_file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `package_file_mtime` int(11) DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_import_history_seq`
--

DROP TABLE IF EXISTS `mockup_import_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_import_history_seq` (
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_metadata`
--

DROP TABLE IF EXISTS `mockup_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_metadata_rel`
--

DROP TABLE IF EXISTS `mockup_metadata_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_metadata_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `mockup_metadata_rel_uniq` (`mockup_id`,`field_name`),
  KEY `mockup_id` (`mockup_id`),
  KEY `field_name` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_metadata_rel_seq`
--

DROP TABLE IF EXISTS `mockup_metadata_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_metadata_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_metadata_seq`
--

DROP TABLE IF EXISTS `mockup_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockups`
--

DROP TABLE IF EXISTS `mockups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockups` (
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `mockup_number` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `mockup_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `access_code` int(11) DEFAULT NULL,
  `mockup_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `mockup_indice_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci NOT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `container_type` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'mockup',
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(6) COLLATE latin1_general_ci NOT NULL DEFAULT 'mockup',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `package_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`mockup_id`),
  UNIQUE KEY `UC_mockup_number` (`mockup_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockups_seq`
--

DROP TABLE IF EXISTS `mockups_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockups_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `partner_id` int(11) NOT NULL DEFAULT '0',
  `partner_number` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `partner_type` enum('customer','supplier','staff') COLLATE latin1_general_ci DEFAULT NULL,
  `first_name` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `last_name` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `adress` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `city` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `cell_phone` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `mail` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `web_site` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `activity` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `company` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`partner_id`),
  UNIQUE KEY `UC_partner_number` (`partner_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partners_seq`
--

DROP TABLE IF EXISTS `partners_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=965 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_history`
--

DROP TABLE IF EXISTS `product_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_by` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `action_date` int(11) DEFAULT NULL,
  `state_before` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `state_after` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `comments` text COLLATE latin1_general_ci,
  `indice_before` varchar(8) COLLATE latin1_general_ci DEFAULT NULL,
  `indice_after` decimal(10,0) DEFAULT NULL,
  `action_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `product_indice_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `product_number` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `product_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `product_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `product_indice` varchar(8) COLLATE latin1_general_ci DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `UC_product_number` (`product_number`),
  KEY `FK_products_1` (`project_id`),
  CONSTRAINT `FK_products_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products_workitem_documents`
--

DROP TABLE IF EXISTS `products_workitem_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_workitem_documents` (
  `document_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`document_id`,`product_id`),
  KEY `FK_products_workitem_documents_1` (`product_id`),
  CONSTRAINT `FK_products_workitem_documents_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  CONSTRAINT `FK_products_workitem_documents_2` FOREIGN KEY (`document_id`) REFERENCES `workitem_documents` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_bookshop_rel`
--

DROP TABLE IF EXISTS `project_bookshop_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_bookshop_rel` (
  `project_id` int(11) DEFAULT NULL,
  `bookshop_id` int(11) DEFAULT NULL,
  UNIQUE KEY `UC_project_bookshop_rel` (`project_id`,`bookshop_id`),
  KEY `FK_project_bookshop_rel_1` (`bookshop_id`),
  CONSTRAINT `FK_project_bookshop_rel_1` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`),
  CONSTRAINT `FK_project_bookshop_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_cadlib_rel`
--

DROP TABLE IF EXISTS `project_cadlib_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_cadlib_rel` (
  `project_id` int(11) DEFAULT NULL,
  `cadlib_id` int(11) DEFAULT NULL,
  UNIQUE KEY `UC_project_cadlib_rel` (`project_id`,`cadlib_id`),
  KEY `FK_project_cadlib_rel_1` (`cadlib_id`),
  CONSTRAINT `FK_project_cadlib_rel_1` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`),
  CONSTRAINT `FK_project_cadlib_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_container_rel`
--

DROP TABLE IF EXISTS `project_container_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_container_rel` (
  `project_id` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `container_type` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  UNIQUE KEY `IDX_project_container_rel_1` (`project_id`,`container_id`,`container_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_doctype_process`
--

DROP TABLE IF EXISTS `project_doctype_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `process_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `Uniq_doctype_project` (`doctype_id`,`project_id`),
  KEY `FK_project_doctype_process_3` (`project_id`),
  CONSTRAINT `FK_project_doctype_process_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  CONSTRAINT `FK_project_doctype_process_3` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_doctype_process_seq`
--

DROP TABLE IF EXISTS `project_doctype_process_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=141 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_history`
--

DROP TABLE IF EXISTS `project_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `project_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `project_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `project_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `project_indice_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_history_seq`
--

DROP TABLE IF EXISTS `project_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_mockup_rel`
--

DROP TABLE IF EXISTS `project_mockup_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_mockup_rel` (
  `project_id` int(11) DEFAULT NULL,
  `mockup_id` int(11) DEFAULT NULL,
  UNIQUE KEY `UC_project_mockup_rel` (`project_id`,`mockup_id`),
  KEY `FK_project_mockup_rel_1` (`mockup_id`),
  CONSTRAINT `FK_project_mockup_rel_1` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`),
  CONSTRAINT `FK_project_mockup_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_partner_rel`
--

DROP TABLE IF EXISTS `project_partner_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_partner_rel` (
  `project_id` int(11) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `role` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `IDX_project_partner_rel_1` (`partner_id`,`project_id`),
  KEY `FK_project_partner_rel_2` (`project_id`),
  CONSTRAINT `FK_project_partner_rel_1` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`partner_id`),
  CONSTRAINT `FK_project_partner_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL DEFAULT '0',
  `project_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `project_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `project_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `default_process_id` int(11) DEFAULT NULL,
  `project_indice_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `link_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `UC_project_number` (`project_number`),
  KEY `FK_projects_1` (`project_indice_id`),
  KEY `FK_projects_2` (`link_id`),
  CONSTRAINT `FK_projects_1` FOREIGN KEY (`project_indice_id`) REFERENCES `container_indice` (`indice_id`),
  CONSTRAINT `FK_projects_2` FOREIGN KEY (`link_id`) REFERENCES `project_doctype_process` (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects_seq`
--

DROP TABLE IF EXISTS `projects_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rt_aif_code_a350`
--

DROP TABLE IF EXISTS `rt_aif_code_a350`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rt_aif_code_a350` (
  `code` char(15) COLLATE latin1_general_ci NOT NULL,
  `itp_cluster` char(4) COLLATE latin1_general_ci DEFAULT NULL,
  `check` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `mistake` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `type` enum('B','I','OS') COLLATE latin1_general_ci NOT NULL,
  `status` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `description` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Codes erreurs pour le programme A350';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rt_aif_code_a350_seq`
--

DROP TABLE IF EXISTS `rt_aif_code_a350_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rt_aif_code_a350_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rt_aif_code_a380`
--

DROP TABLE IF EXISTS `rt_aif_code_a380`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rt_aif_code_a380` (
  `status` varchar(24) COLLATE latin1_general_ci NOT NULL,
  `code` char(8) COLLATE latin1_general_ci NOT NULL,
  `title` text COLLATE latin1_general_ci NOT NULL,
  `itp` varchar(24) COLLATE latin1_general_ci NOT NULL,
  `stec` tinyint(4) NOT NULL,
  `stg` tinyint(4) NOT NULL,
  KEY `rt_aif_code_index01` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `rt_aif_code_view1`
--

DROP TABLE IF EXISTS `rt_aif_code_view1`;
/*!50001 DROP VIEW IF EXISTS `rt_aif_code_view1`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `rt_aif_code_view1` (
  `code_title` tinyint NOT NULL,
  `code` tinyint NOT NULL,
  `title` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tiki_user_tasks`
--

DROP TABLE IF EXISTS `tiki_user_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiki_user_tasks` (
  `taskId` int(14) NOT NULL AUTO_INCREMENT,
  `last_version` int(4) NOT NULL DEFAULT '0',
  `user` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `creator` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `public_for_group` int(11) DEFAULT NULL,
  `rights_by_creator` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `created` int(14) NOT NULL DEFAULT '0',
  `status` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `priority` int(2) DEFAULT NULL,
  `completed` int(14) DEFAULT NULL,
  `percentage` int(4) DEFAULT NULL,
  PRIMARY KEY (`taskId`),
  UNIQUE KEY `creator` (`creator`,`created`),
  UNIQUE KEY `creator_2` (`creator`,`created`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tiki_user_tasks_history`
--

DROP TABLE IF EXISTS `tiki_user_tasks_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiki_user_tasks_history` (
  `belongs_to` int(14) NOT NULL DEFAULT '0',
  `task_version` int(4) NOT NULL DEFAULT '0',
  `title` varchar(250) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` text COLLATE latin1_general_ci,
  `start` int(14) DEFAULT NULL,
  `end` int(14) DEFAULT NULL,
  `lasteditor` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `lastchanges` int(14) NOT NULL DEFAULT '0',
  `priority` int(2) NOT NULL DEFAULT '3',
  `completed` int(14) DEFAULT NULL,
  `deleted` int(14) DEFAULT NULL,
  `status` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `percentage` int(4) DEFAULT NULL,
  `accepted_creator` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `accepted_user` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`belongs_to`,`task_version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_prefs`
--

DROP TABLE IF EXISTS `user_prefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_prefs` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `css_sheet` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `lang` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `long_date_format` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `short_date_format` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `hour_format` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `time_zone` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `wildspace_path` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `max_record` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`user_id`) REFERENCES `liveuser_perm_users` (`perm_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_sessions`
--

DROP TABLE IF EXISTS `user_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_sessions` (
  `user_id` int(11) NOT NULL,
  `datas` longtext COLLATE latin1_general_ci,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `view_containerfavorite_users`
--

DROP TABLE IF EXISTS `view_containerfavorite_users`;
/*!50001 DROP VIEW IF EXISTS `view_containerfavorite_users`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_containerfavorite_users` (
  `user_id` tinyint NOT NULL,
  `container_id` tinyint NOT NULL,
  `container_number` tinyint NOT NULL,
  `space_id` tinyint NOT NULL,
  `space_name` tinyint NOT NULL,
  `username` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_countapprove_byuser`
--

DROP TABLE IF EXISTS `view_countapprove_byuser`;
/*!50001 DROP VIEW IF EXISTS `view_countapprove_byuser`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_countapprove_byuser` (
  `count` tinyint NOT NULL,
  `owner` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_countreject_byuser`
--

DROP TABLE IF EXISTS `view_countreject_byuser`;
/*!50001 DROP VIEW IF EXISTS `view_countreject_byuser`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_countreject_byuser` (
  `count` tinyint NOT NULL,
  `owner` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_docfile`
--

DROP TABLE IF EXISTS `view_docfile`;
/*!50001 DROP VIEW IF EXISTS `view_docfile`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_docfile` (
  `document_id` tinyint NOT NULL,
  `document_number` tinyint NOT NULL,
  `document_name` tinyint NOT NULL,
  `document_state` tinyint NOT NULL,
  `document_access_code` tinyint NOT NULL,
  `document_version` tinyint NOT NULL,
  `workitem_id` tinyint NOT NULL,
  `document_indice_id` tinyint NOT NULL,
  `doc_space` tinyint NOT NULL,
  `instance_id` tinyint NOT NULL,
  `doctype_id` tinyint NOT NULL,
  `default_process_id` tinyint NOT NULL,
  `category_id` tinyint NOT NULL,
  `check_out_by` tinyint NOT NULL,
  `check_out_date` tinyint NOT NULL,
  `designation` tinyint NOT NULL,
  `issued_from_document` tinyint NOT NULL,
  `update_date` tinyint NOT NULL,
  `update_by` tinyint NOT NULL,
  `open_date` tinyint NOT NULL,
  `open_by` tinyint NOT NULL,
  `ci` tinyint NOT NULL,
  `type_gc` tinyint NOT NULL,
  `work_unit` tinyint NOT NULL,
  `father_ds` tinyint NOT NULL,
  `need_calcul` tinyint NOT NULL,
  `ata` tinyint NOT NULL,
  `receptionneur` tinyint NOT NULL,
  `indice_client` tinyint NOT NULL,
  `recept_date` tinyint NOT NULL,
  `observation` tinyint NOT NULL,
  `date_de_remise` tinyint NOT NULL,
  `sub_ata` tinyint NOT NULL,
  `work_package` tinyint NOT NULL,
  `subject` tinyint NOT NULL,
  `weight` tinyint NOT NULL,
  `cost` tinyint NOT NULL,
  `fab_process` tinyint NOT NULL,
  `material` tinyint NOT NULL,
  `materiau_mak` tinyint NOT NULL,
  `masse_mak` tinyint NOT NULL,
  `rt_name` tinyint NOT NULL,
  `rt_type` tinyint NOT NULL,
  `rt_reason` tinyint NOT NULL,
  `rt_emitted` tinyint NOT NULL,
  `rt_apply_to` tinyint NOT NULL,
  `modification` tinyint NOT NULL,
  `file_version` tinyint NOT NULL,
  `file_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_document_hierarchy`
--

DROP TABLE IF EXISTS `view_document_hierarchy`;
/*!50001 DROP VIEW IF EXISTS `view_document_hierarchy`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_document_hierarchy` (
  `id` tinyint NOT NULL,
  `parent` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_filedoc`
--

DROP TABLE IF EXISTS `view_filedoc`;
/*!50001 DROP VIEW IF EXISTS `view_filedoc`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_filedoc` (
  `file_id` tinyint NOT NULL,
  `document_id` tinyint NOT NULL,
  `file_name` tinyint NOT NULL,
  `file_used_name` tinyint NOT NULL,
  `file_path` tinyint NOT NULL,
  `file_version` tinyint NOT NULL,
  `file_access_code` tinyint NOT NULL,
  `file_root_name` tinyint NOT NULL,
  `file_extension` tinyint NOT NULL,
  `file_state` tinyint NOT NULL,
  `file_type` tinyint NOT NULL,
  `file_size` tinyint NOT NULL,
  `file_mtime` tinyint NOT NULL,
  `file_md5` tinyint NOT NULL,
  `file_checkout_by` tinyint NOT NULL,
  `file_checkout_date` tinyint NOT NULL,
  `file_open_date` tinyint NOT NULL,
  `file_open_by` tinyint NOT NULL,
  `file_update_date` tinyint NOT NULL,
  `file_update_by` tinyint NOT NULL,
  `document_version` tinyint NOT NULL,
  `document_indice_id` tinyint NOT NULL,
  `document_number` tinyint NOT NULL,
  `designation` tinyint NOT NULL,
  `document_state` tinyint NOT NULL,
  `document_access_code` tinyint NOT NULL,
  `workitem_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_filedoc_rel`
--

DROP TABLE IF EXISTS `view_filedoc_rel`;
/*!50001 DROP VIEW IF EXISTS `view_filedoc_rel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_filedoc_rel` (
  `docId` tinyint NOT NULL,
  `filename` tinyint NOT NULL,
  `iteration` tinyint NOT NULL,
  `indice` tinyint NOT NULL,
  `designation` tinyint NOT NULL,
  `state` tinyint NOT NULL,
  `accesscode` tinyint NOT NULL,
  `workitemId` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_instances_activities`
--

DROP TABLE IF EXISTS `view_instances_activities`;
/*!50001 DROP VIEW IF EXISTS `view_instances_activities`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_instances_activities` (
  `inst_name` tinyint NOT NULL,
  `inst_id` tinyint NOT NULL,
  `inst_start` tinyint NOT NULL,
  `inst_end` tinyint NOT NULL,
  `act_user` tinyint NOT NULL,
  `act_name` tinyint NOT NULL,
  `document_id` tinyint NOT NULL,
  `container_id` tinyint NOT NULL,
  `user_id` tinyint NOT NULL,
  `user_email` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_rejected_doc`
--

DROP TABLE IF EXISTS `view_rejected_doc`;
/*!50001 DROP VIEW IF EXISTS `view_rejected_doc`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_rejected_doc` (
  `docid` tinyint NOT NULL,
  `document_number` tinyint NOT NULL,
  `state` tinyint NOT NULL,
  `action_by` tinyint NOT NULL,
  `action_name` tinyint NOT NULL,
  `container_id` tinyint NOT NULL,
  `container_number` tinyint NOT NULL,
  `started` tinyint NOT NULL,
  `owner` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `properties` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_rejected_docc`
--

DROP TABLE IF EXISTS `view_rejected_docc`;
/*!50001 DROP VIEW IF EXISTS `view_rejected_docc`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_rejected_docc` (
  `instanceId` tinyint NOT NULL,
  `docid` tinyint NOT NULL,
  `document_number` tinyint NOT NULL,
  `state` tinyint NOT NULL,
  `action_by` tinyint NOT NULL,
  `action_name` tinyint NOT NULL,
  `doctype` tinyint NOT NULL,
  `container_id` tinyint NOT NULL,
  `container_number` tinyint NOT NULL,
  `started` tinyint NOT NULL,
  `ended` tinyint NOT NULL,
  `owner` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `previousActivity` tinyint NOT NULL,
  `previousUser` tinyint NOT NULL,
  `checkresult` tinyint NOT NULL,
  `ecodes` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_rt`
--

DROP TABLE IF EXISTS `view_rt`;
/*!50001 DROP VIEW IF EXISTS `view_rt`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_rt` (
  `rt_id` tinyint NOT NULL,
  `rt_number` tinyint NOT NULL,
  `rt_status` tinyint NOT NULL,
  `rt_access_code` tinyint NOT NULL,
  `rt_open_date` tinyint NOT NULL,
  `rt_reason` tinyint NOT NULL,
  `rt_type` tinyint NOT NULL,
  `applyto_id` tinyint NOT NULL,
  `applyto_number` tinyint NOT NULL,
  `applyto_indice` tinyint NOT NULL,
  `applyto_version` tinyint NOT NULL,
  `applyto_status` tinyint NOT NULL,
  `from_number` tinyint NOT NULL,
  `from_version` tinyint NOT NULL,
  `container_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_rt_b`
--

DROP TABLE IF EXISTS `view_rt_b`;
/*!50001 DROP VIEW IF EXISTS `view_rt_b`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_rt_b` (
  `rt_id` tinyint NOT NULL,
  `rt_number` tinyint NOT NULL,
  `rt_status` tinyint NOT NULL,
  `rt_access_code` tinyint NOT NULL,
  `rt_open_date` tinyint NOT NULL,
  `rt_reason` tinyint NOT NULL,
  `rt_type` tinyint NOT NULL,
  `applyto_id` tinyint NOT NULL,
  `applyto_number` tinyint NOT NULL,
  `applyto_indice` tinyint NOT NULL,
  `applyto_version` tinyint NOT NULL,
  `applyto_status` tinyint NOT NULL,
  `from_number` tinyint NOT NULL,
  `from_version` tinyint NOT NULL,
  `container_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `workitem_alias`
--

DROP TABLE IF EXISTS `workitem_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `workitem_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `workitem_description` text COLLATE latin1_general_ci,
  `object_class` varchar(13) COLLATE latin1_general_ci NOT NULL DEFAULT 'workitemAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_workitem_alias` (`alias_id`,`workitem_id`),
  KEY `K_workitem_alias_1` (`workitem_id`),
  KEY `K_workitem_alias_2` (`workitem_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_alias_seq`
--

DROP TABLE IF EXISTS `workitem_alias_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_alias_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_categories`
--

DROP TABLE IF EXISTS `workitem_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_number` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `category_description` text COLLATE latin1_general_ci,
  `category_icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `UC_category_number` (`category_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_categories_seq`
--

DROP TABLE IF EXISTS `workitem_categories_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_category_rel`
--

DROP TABLE IF EXISTS `workitem_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_workitem_categories_rel_1` (`category_id`,`workitem_id`),
  KEY `K_workitem_categories_rel_1` (`category_id`),
  KEY `K_workitem_categories_rel_2` (`workitem_id`),
  CONSTRAINT `workitem_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `workitem_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `workitem_category_rel_ibfk_2` FOREIGN KEY (`workitem_id`) REFERENCES `workitems` (`workitem_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_category_rel_seq`
--

DROP TABLE IF EXISTS `workitem_category_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_cont_metadata`
--

DROP TABLE IF EXISTS `workitem_cont_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_cont_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_cont_metadata_seq`
--

DROP TABLE IF EXISTS `workitem_cont_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_files`
--

DROP TABLE IF EXISTS `workitem_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_used_name` varchar(128) COLLATE latin1_general_ci NOT NULL COMMENT 'nom d usage',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_checkout_by` int(11) DEFAULT NULL,
  `file_checkout_date` int(11) DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_file_name` (`file_name`,`file_path`(512)),
  KEY `FK_workitem_doc_files_1` (`document_id`),
  KEY `file_md5` (`file_md5`),
  KEY `file_name` (`file_name`),
  CONSTRAINT `FK_workitem_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `workitem_documents` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_files_seq`
--

DROP TABLE IF EXISTS `workitem_doc_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=217580 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_files_versions`
--

DROP TABLE IF EXISTS `workitem_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files_versions` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `father_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_path` tinytext COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(128)),
  KEY `IK_workitem_doc_files_version_1` (`father_id`),
  KEY `IK_workitem_doc_files_version_2` (`file_name`),
  KEY `IK_workitem_doc_files_version_3` (`file_path`(128)),
  KEY `IK_workitem_doc_files_version_4` (`file_version`,`father_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `workitem_doc_files_versions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=224528 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_rel`
--

DROP TABLE IF EXISTS `workitem_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_rel` (
  `dr_link_id` int(11) NOT NULL DEFAULT '0',
  `dr_document_id` int(11) DEFAULT NULL,
  `dr_l_document_id` int(11) DEFAULT NULL,
  `dr_access_code` int(11) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dr_link_id`),
  UNIQUE KEY `uniq_docid_ldoc_id` (`dr_document_id`,`dr_l_document_id`),
  CONSTRAINT `FK_workitem_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_rel_seq`
--

DROP TABLE IF EXISTS `workitem_doc_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14533 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doccomments`
--

DROP TABLE IF EXISTS `workitem_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doccomments` (
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `document_id` (`document_id`),
  CONSTRAINT `workitem_doccomments_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doccomments_seq`
--

DROP TABLE IF EXISTS `workitem_doccomments_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2259 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doctype_process`
--

DROP TABLE IF EXISTS `workitem_doctype_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `workitem_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `FK_workitem_doctype_process_1` (`doctype_id`),
  KEY `FK_workitem_doctype_process_3` (`category_id`),
  KEY `FK_workitem_doctype_process_4` (`workitem_id`),
  CONSTRAINT `FK_workitem_doctype_process_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  CONSTRAINT `FK_workitem_doctype_process_3` FOREIGN KEY (`category_id`) REFERENCES `workitem_categories` (`category_id`),
  CONSTRAINT `FK_workitem_doctype_process_4` FOREIGN KEY (`workitem_id`) REFERENCES `workitems` (`workitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doctype_process_seq`
--

DROP TABLE IF EXISTS `workitem_doctype_process_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=167 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_documents`
--

DROP TABLE IF EXISTS `workitem_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_documents` (
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_name` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `document_state` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `document_access_code` int(11) NOT NULL DEFAULT '0',
  `document_version` int(11) NOT NULL DEFAULT '1',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `document_indice_id` int(11) DEFAULT NULL,
  `doc_space` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT 'workitem',
  `instance_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) NOT NULL DEFAULT '0',
  `default_process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `ci` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `type_gc` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `work_unit` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `father_ds` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `need_calcul` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ata` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `receptionneur` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `indice_client` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `recept_date` int(11) DEFAULT NULL,
  `observation` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `date_de_remise` int(11) DEFAULT NULL,
  `sub_ata` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `work_package` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `fab_process` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `material` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `materiau_mak` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `masse_mak` double DEFAULT NULL,
  `rt_name` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `rt_type` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `rt_reason` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `rt_emitted` int(11) DEFAULT NULL,
  `rt_apply_to` int(11) DEFAULT NULL,
  `modification` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  UNIQUE KEY `IDX_workitem_documents_1` (`document_number`,`document_indice_id`),
  KEY `FK_workitem_documents_2` (`doctype_id`),
  KEY `FK_workitem_documents_3` (`document_indice_id`),
  KEY `FK_workitem_documents_4` (`workitem_id`),
  KEY `FK_workitem_documents_5` (`category_id`),
  KEY `document_name` (`document_name`),
  CONSTRAINT `FK_workitem_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  CONSTRAINT `FK_workitem_documents_3` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`),
  CONSTRAINT `FK_workitem_documents_4` FOREIGN KEY (`workitem_id`) REFERENCES `workitems` (`workitem_id`),
  CONSTRAINT `FK_workitem_documents_5` FOREIGN KEY (`category_id`) REFERENCES `workitem_categories` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_documents_history`
--

DROP TABLE IF EXISTS `workitem_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) DEFAULT NULL,
  `workitem_id` int(11) DEFAULT NULL,
  `document_number` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `document_state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `document_access_code` int(11) DEFAULT NULL,
  `document_version` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  PRIMARY KEY (`histo_order`),
  KEY `workitem_documents_history_i001` (`instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_documents_history_seq`
--

DROP TABLE IF EXISTS `workitem_documents_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1006164 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_documents_seq`
--

DROP TABLE IF EXISTS `workitem_documents_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=125148 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_history`
--

DROP TABLE IF EXISTS `workitem_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `action_by` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `action_date` int(11) DEFAULT NULL,
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `workitem_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `workitem_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `workitem_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `workitem_indice_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_history_seq`
--

DROP TABLE IF EXISTS `workitem_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=479 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_import_history`
--

DROP TABLE IF EXISTS `workitem_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `import_date` int(11) DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` text COLLATE latin1_general_ci,
  `import_logfiles_file` text COLLATE latin1_general_ci,
  `package_file_path` text COLLATE latin1_general_ci,
  `package_file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `package_file_mtime` int(11) DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_metadata`
--

DROP TABLE IF EXISTS `workitem_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_metadata_rel`
--

DROP TABLE IF EXISTS `workitem_metadata_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_metadata_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `workitem_metadata_rel_uniq` (`workitem_id`,`field_name`),
  KEY `workitem_id` (`workitem_id`),
  KEY `field_name` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_metadata_rel_seq`
--

DROP TABLE IF EXISTS `workitem_metadata_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_metadata_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_metadata_seq`
--

DROP TABLE IF EXISTS `workitem_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitems`
--

DROP TABLE IF EXISTS `workitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitems` (
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `workitem_number` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `workitem_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `workitem_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `access_code` int(11) DEFAULT NULL,
  `workitem_indice_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci NOT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `container_type` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'workitem',
  `affaires_liees` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `interloc_client` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `interloc_sier` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `client` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `numero_client` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT 'workitem',
  `chef_group` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`workitem_id`),
  UNIQUE KEY `UC_workitem_number` (`workitem_number`),
  KEY `FK_workitems_1` (`workitem_indice_id`),
  KEY `FK_workitems_2` (`project_id`),
  CONSTRAINT `FK_workitems_1` FOREIGN KEY (`workitem_indice_id`) REFERENCES `container_indice` (`indice_id`),
  CONSTRAINT `FK_workitems_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitems_seq`
--

DROP TABLE IF EXISTS `workitems_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitems_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=274 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `rt_aif_code_view1`
--

/*!50001 DROP TABLE IF EXISTS `rt_aif_code_view1`*/;
/*!50001 DROP VIEW IF EXISTS `rt_aif_code_view1`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `rt_aif_code_view1` AS select concat(`rt_aif_code_a350`.`code`,'  ',`rt_aif_code_a350`.`description`) AS `code_title`,`rt_aif_code_a350`.`code` AS `code`,`rt_aif_code_a350`.`description` AS `title` from `rt_aif_code_a350` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_containerfavorite_users`
--

/*!50001 DROP TABLE IF EXISTS `view_containerfavorite_users`*/;
/*!50001 DROP VIEW IF EXISTS `view_containerfavorite_users`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_containerfavorite_users` AS select `cf`.`user_id` AS `user_id`,`cf`.`container_id` AS `container_id`,`cf`.`container_number` AS `container_number`,`cf`.`space_id` AS `space_id`,`cf`.`space_name` AS `space_name`,`lu`.`handle` AS `username` from (`container_favorite` `cf` join `liveuser_users` `lu` on((`cf`.`user_id` = `lu`.`auth_user_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_countapprove_byuser`
--

/*!50001 DROP TABLE IF EXISTS `view_countapprove_byuser`*/;
/*!50001 DROP VIEW IF EXISTS `view_countapprove_byuser`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_countapprove_byuser` AS select count(`view_rejected_docc`.`docid`) AS `count`,`view_rejected_docc`.`owner` AS `owner` from `view_rejected_docc` where (`view_rejected_docc`.`checkresult` = 'approve') group by `view_rejected_docc`.`owner` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_countreject_byuser`
--

/*!50001 DROP TABLE IF EXISTS `view_countreject_byuser`*/;
/*!50001 DROP VIEW IF EXISTS `view_countreject_byuser`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_countreject_byuser` AS select count(`view_rejected_docc`.`docid`) AS `count`,`view_rejected_docc`.`owner` AS `owner` from `view_rejected_docc` where (`view_rejected_docc`.`checkresult` like 'rejete%') group by `view_rejected_docc`.`owner` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_docfile`
--

/*!50001 DROP TABLE IF EXISTS `view_docfile`*/;
/*!50001 DROP VIEW IF EXISTS `view_docfile`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_docfile` AS select `doc`.`document_id` AS `document_id`,`doc`.`document_number` AS `document_number`,`doc`.`document_name` AS `document_name`,`doc`.`document_state` AS `document_state`,`doc`.`document_access_code` AS `document_access_code`,`doc`.`document_version` AS `document_version`,`doc`.`workitem_id` AS `workitem_id`,`doc`.`document_indice_id` AS `document_indice_id`,`doc`.`doc_space` AS `doc_space`,`doc`.`instance_id` AS `instance_id`,`doc`.`doctype_id` AS `doctype_id`,`doc`.`default_process_id` AS `default_process_id`,`doc`.`category_id` AS `category_id`,`doc`.`check_out_by` AS `check_out_by`,`doc`.`check_out_date` AS `check_out_date`,`doc`.`designation` AS `designation`,`doc`.`issued_from_document` AS `issued_from_document`,`doc`.`update_date` AS `update_date`,`doc`.`update_by` AS `update_by`,`doc`.`open_date` AS `open_date`,`doc`.`open_by` AS `open_by`,`doc`.`ci` AS `ci`,`doc`.`type_gc` AS `type_gc`,`doc`.`work_unit` AS `work_unit`,`doc`.`father_ds` AS `father_ds`,`doc`.`need_calcul` AS `need_calcul`,`doc`.`ata` AS `ata`,`doc`.`receptionneur` AS `receptionneur`,`doc`.`indice_client` AS `indice_client`,`doc`.`recept_date` AS `recept_date`,`doc`.`observation` AS `observation`,`doc`.`date_de_remise` AS `date_de_remise`,`doc`.`sub_ata` AS `sub_ata`,`doc`.`work_package` AS `work_package`,`doc`.`subject` AS `subject`,`doc`.`weight` AS `weight`,`doc`.`cost` AS `cost`,`doc`.`fab_process` AS `fab_process`,`doc`.`material` AS `material`,`doc`.`materiau_mak` AS `materiau_mak`,`doc`.`masse_mak` AS `masse_mak`,`doc`.`rt_name` AS `rt_name`,`doc`.`rt_type` AS `rt_type`,`doc`.`rt_reason` AS `rt_reason`,`doc`.`rt_emitted` AS `rt_emitted`,`doc`.`rt_apply_to` AS `rt_apply_to`,`doc`.`modification` AS `modification`,`df`.`file_version` AS `file_version`,`df`.`file_name` AS `file_name` from (`workitem_doc_files` `df` join `workitem_documents` `doc` on((`df`.`document_id` = `doc`.`document_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_document_hierarchy`
--

/*!50001 DROP TABLE IF EXISTS `view_document_hierarchy`*/;
/*!50001 DROP VIEW IF EXISTS `view_document_hierarchy`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_document_hierarchy` AS select `docrel`.`dr_l_document_id` AS `id`,`docrel`.`dr_document_id` AS `parent` from `workitem_doc_rel` `docrel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_filedoc`
--

/*!50001 DROP TABLE IF EXISTS `view_filedoc`*/;
/*!50001 DROP VIEW IF EXISTS `view_filedoc`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_filedoc` AS select `df`.`file_id` AS `file_id`,`df`.`document_id` AS `document_id`,`df`.`file_name` AS `file_name`,`df`.`file_used_name` AS `file_used_name`,`df`.`file_path` AS `file_path`,`df`.`file_version` AS `file_version`,`df`.`file_access_code` AS `file_access_code`,`df`.`file_root_name` AS `file_root_name`,`df`.`file_extension` AS `file_extension`,`df`.`file_state` AS `file_state`,`df`.`file_type` AS `file_type`,`df`.`file_size` AS `file_size`,`df`.`file_mtime` AS `file_mtime`,`df`.`file_md5` AS `file_md5`,`df`.`file_checkout_by` AS `file_checkout_by`,`df`.`file_checkout_date` AS `file_checkout_date`,`df`.`file_open_date` AS `file_open_date`,`df`.`file_open_by` AS `file_open_by`,`df`.`file_update_date` AS `file_update_date`,`df`.`file_update_by` AS `file_update_by`,`doc`.`document_version` AS `document_version`,`doc`.`document_indice_id` AS `document_indice_id`,`doc`.`document_number` AS `document_number`,`doc`.`designation` AS `designation`,`doc`.`document_state` AS `document_state`,`doc`.`document_access_code` AS `document_access_code`,`doc`.`workitem_id` AS `workitem_id` from (`workitem_doc_files` `df` join `workitem_documents` `doc` on((`df`.`document_id` = `doc`.`document_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_filedoc_rel`
--

/*!50001 DROP TABLE IF EXISTS `view_filedoc_rel`*/;
/*!50001 DROP VIEW IF EXISTS `view_filedoc_rel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_filedoc_rel` AS select `doc`.`document_id` AS `docId`,`df`.`file_name` AS `filename`,`df`.`file_version` AS `iteration`,`doc`.`document_indice_id` AS `indice`,`doc`.`designation` AS `designation`,`doc`.`document_state` AS `state`,`doc`.`document_access_code` AS `accesscode`,`doc`.`workitem_id` AS `workitemId` from (`workitem_doc_files` `df` join `workitem_documents` `doc` on((`df`.`document_id` = `doc`.`document_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_instances_activities`
--

/*!50001 DROP TABLE IF EXISTS `view_instances_activities`*/;
/*!50001 DROP VIEW IF EXISTS `view_instances_activities`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_instances_activities` AS select `inst`.`name` AS `inst_name`,`inst`.`instanceId` AS `inst_id`,`inst`.`started` AS `inst_start`,`inst`.`ended` AS `inst_end`,`inst_act`.`user` AS `act_user`,`act`.`name` AS `act_name`,`histo`.`document_id` AS `document_id`,`histo`.`workitem_id` AS `container_id`,`users`.`auth_user_id` AS `user_id`,`users`.`email` AS `user_email` from ((((`galaxia_instances` `inst` join `galaxia_instance_activities` `inst_act` on((`inst`.`instanceId` = `inst_act`.`instanceId`))) join `galaxia_activities` `act` on((`act`.`activityId` = `inst_act`.`activityId`))) join `workitem_documents_history` `histo` on((`histo`.`instance_id` = `inst`.`instanceId`))) join `liveuser_users` `users` on((`users`.`handle` = `inst_act`.`user`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_rejected_doc`
--

/*!50001 DROP TABLE IF EXISTS `view_rejected_doc`*/;
/*!50001 DROP VIEW IF EXISTS `view_rejected_doc`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_rejected_doc` AS select `histo`.`document_id` AS `docid`,`histo`.`document_number` AS `document_number`,`histo`.`document_state` AS `state`,`histo`.`action_by` AS `action_by`,`histo`.`action_name` AS `action_name`,`container`.`workitem_id` AS `container_id`,`container`.`workitem_number` AS `container_number`,from_unixtime(`instance`.`started`) AS `started`,`instance`.`owner` AS `owner`,`instance`.`status` AS `status`,substr(`instance`.`properties`,1,1000) AS `properties` from ((`workitem_documents_history` `histo` join `galaxia_instances` `instance` on((`histo`.`instance_id` = `instance`.`instanceId`))) join `workitems` `container` on((`histo`.`workitem_id` = `container`.`workitem_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_rejected_docc`
--

/*!50001 DROP TABLE IF EXISTS `view_rejected_docc`*/;
/*!50001 DROP VIEW IF EXISTS `view_rejected_docc`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_rejected_docc` AS select `instance`.`instanceId` AS `instanceId`,`histo`.`document_id` AS `docid`,`histo`.`document_number` AS `document_number`,`histo`.`document_state` AS `state`,`histo`.`action_by` AS `action_by`,`histo`.`action_name` AS `action_name`,`dt`.`doctype_number` AS `doctype`,`container`.`workitem_id` AS `container_id`,`container`.`workitem_number` AS `container_number`,from_unixtime(`instance`.`started`) AS `started`,from_unixtime(`instance`.`ended`) AS `ended`,`instance`.`owner` AS `owner`,`instance`.`status` AS `status`,`ip`.`previousActivity` AS `previousActivity`,`ip`.`previousUser` AS `previousUser`,`ip`.`checkresult` AS `checkresult`,`ip`.`ecodes` AS `ecodes` from ((((`workitem_documents_history` `histo` join `galaxia_instances` `instance` on((`histo`.`instance_id` = `instance`.`instanceId`))) join `workitems` `container` on((`histo`.`workitem_id` = `container`.`workitem_id`))) join `galaxia_instance_properties` `ip` on((`instance`.`instanceId` = `ip`.`iid`))) join `doctypes` `dt` on((`histo`.`doctype_id` = `dt`.`doctype_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_rt`
--

/*!50001 DROP TABLE IF EXISTS `view_rt`*/;
/*!50001 DROP VIEW IF EXISTS `view_rt`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_rt` AS select `rt`.`document_id` AS `rt_id`,`rt`.`document_number` AS `rt_number`,`rt`.`document_state` AS `rt_status`,`rt`.`document_access_code` AS `rt_access_code`,`rt`.`open_date` AS `rt_open_date`,`rt`.`rt_reason` AS `rt_reason`,`rt`.`rt_type` AS `rt_type`,`applydocs`.`document_id` AS `applyto_id`,`applydocs`.`document_number` AS `applyto_number`,`applydocs`.`document_indice_id` AS `applyto_indice`,`applydocs`.`document_version` AS `applyto_version`,`applydocs`.`document_state` AS `applyto_status`,`linked_docs`.`document_number` AS `from_number`,`linked_docs`.`document_indice_id` AS `from_version`,`linked_docs`.`workitem_id` AS `container_id` from ((`workitem_documents` `rt` left join `workitem_documents` `linked_docs` on((`linked_docs`.`document_id` = `rt`.`rt_apply_to`))) join `workitem_documents` `applydocs` on((`linked_docs`.`document_number` = `applydocs`.`document_number`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_rt_b`
--

/*!50001 DROP TABLE IF EXISTS `view_rt_b`*/;
/*!50001 DROP VIEW IF EXISTS `view_rt_b`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_rt_b` AS select `rt`.`document_id` AS `rt_id`,`rt`.`document_number` AS `rt_number`,`rt`.`document_state` AS `rt_status`,`rt`.`document_access_code` AS `rt_access_code`,`rt`.`open_date` AS `rt_open_date`,`rt`.`rt_reason` AS `rt_reason`,`rt`.`rt_type` AS `rt_type`,`linked_docs`.`document_id` AS `applyto_id`,`linked_docs`.`document_number` AS `applyto_number`,`linked_docs`.`document_indice_id` AS `applyto_indice`,`linked_docs`.`document_version` AS `applyto_version`,`linked_docs`.`document_state` AS `applyto_status`,`linked_docs`.`document_number` AS `from_number`,`linked_docs`.`document_indice_id` AS `from_version`,`linked_docs`.`workitem_id` AS `container_id` from (`workitem_documents` `rt` left join `workitem_documents` `linked_docs` on((`linked_docs`.`document_id` = `rt`.`rt_apply_to`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-22  8:56:24