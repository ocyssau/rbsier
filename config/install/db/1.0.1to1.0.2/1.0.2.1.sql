-- ON BOOKSHOP INSERT --
DROP TRIGGER IF EXISTS onBookshopInsert;
delimiter $$
CREATE TRIGGER onBookshopInsert BEFORE INSERT ON bookshops FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END;$$
delimiter ;

-- ON BOOKSHOP UPDATE --
DROP TRIGGER IF EXISTS onBookshopUpdate;
delimiter $$
CREATE TRIGGER onBookshopUpdate BEFORE UPDATE ON bookshops FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(64);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn,uid INTO parentDn,parentUid from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END;$$
delimiter ;

-- ON CADLIB INSERT --
DROP TRIGGER IF EXISTS onCadlibInsert;
delimiter $$
CREATE TRIGGER onCadlibInsert BEFORE INSERT ON cadlibs FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END;$$
delimiter ;

-- ON CADLIB UPDATE --
DROP TRIGGER IF EXISTS onCadlibUpdate;
delimiter $$
CREATE TRIGGER onCadlibUpdate BEFORE UPDATE ON cadlibs FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(64);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn,uid INTO parentDn,parentUid from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END;$$
delimiter ;



-- ON MOCKUP INSERT --
DROP TRIGGER IF EXISTS onMockupInsert;
delimiter $$
CREATE TRIGGER onMockupInsert BEFORE INSERT ON mockups FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);

	SELECT `dn` INTO parentDn FROM `projects` WHERE `id`=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	
	SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id AND `referToCid`='569e93c6ee156';
	set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
	
	-- add resource
	INSERT INTO `acl_resource`
	(`id`,
	`uid`,
	`cn`,
	`ownerId`,
	`ownerUid`,
	`referToUid`,
	`referToId`,
	`referToCid`)
	VALUES (
	aclSequence(),
	substr(uuid(),1,8),
	resourceDn,
	NEW.create_by_id,
	NEW.create_by_uid,
	NEW.uid,
	NEW.id,
	NEW.cid
	);
END;$$
delimiter ;

-- ON MOCKUP UPDATE --
DROP TRIGGER IF EXISTS onMockupUpdate;
delimiter $$
CREATE TRIGGER onMockupUpdate BEFORE UPDATE ON mockups FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE resourceDn VARCHAR(128);
	DECLARE parentResourceDn VARCHAR(128);
	
	/* */
	IF(NEW.parent_id != OLD.parent_id) THEN
		-- get parent dn
		SELECT dn INTO parentDn from projects where id=NEW.parent_id;
		SET NEW.dn = CONCAT(parentDn,NEW.uid,'/');
		
		SELECT `cn` INTO parentResourceDn FROM `acl_resource` WHERE `referToId`=NEW.parent_id and `referToCid`='569e93c6ee156';
		set resourceDn = CONCAT(parentResourceDn,NEW.uid,'/');
		UPDATE `acl_resource` SET `cn`=resourceDn WHERE `referToId`=NEW.id AND `referToCid`='569e94192201a';
	END IF;
END;$$
delimiter ;

ALTER TABLE `acl_resource` ADD UNIQUE INDEX `U_referToUid` (`referToUid` ASC);

UPDATE `bookshops` AS cont SET `parent_uid`=(SELECT uid FROM projects WHERE id=cont.parent_id);
UPDATE `cadlibs` AS cont SET `parent_uid`=(SELECT uid FROM projects WHERE id=cont.parent_id);
UPDATE `mockups` AS cont SET `parent_uid`=(SELECT uid FROM projects WHERE id=cont.parent_id);

INSERT INTO `acl_resource`
(`id`,
`uid`,
`cn`,
`ownerId`,
`ownerUid`,
`referToUid`,
`referToId`,
`referToCid`)
SELECT
aclSequence() `id`,
substr(uuid(),1,8) `uid`,
CONCAT('/app/ged/project/', dn) as cn,
1 AS ownerId,
'admin' AS ownerUid,
`uid` as referToUid,
`id` as referToId,
`cid` as referToCid
FROM `bookshops`;

INSERT INTO `acl_resource`
(`id`,
`uid`,
`cn`,
`ownerId`,
`ownerUid`,
`referToUid`,
`referToId`,
`referToCid`)
SELECT
aclSequence() `id`,
substr(uuid(),1,8) `uid`,
CONCAT('/app/ged/project/', dn) as cn,
1 AS ownerId,
'admin' AS ownerUid,
`uid` as referToUid,
`id` as referToId,
`cid` as referToCid
FROM `cadlibs`;

INSERT INTO `acl_resource`
(`id`,
`uid`,
`cn`,
`ownerId`,
`ownerUid`,
`referToUid`,
`referToId`,
`referToCid`)
SELECT
aclSequence() `id`,
substr(uuid(),1,8) `uid`,
CONCAT('/app/ged/project/', dn) as cn,
1 AS ownerId,
'admin' AS ownerUid,
`uid` as referToUid,
`id` as referToId,
`cid` as referToCid
FROM `mockups`;

UPDATE `acl_resource` SET ownerId=1, ownerUid='admin' WHERE ownerId is null;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.2.1',now());

