-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: ranchbe
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acl_group`
--

DROP TABLE IF EXISTS `acl_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_group` (
  `id` int(11) NOT NULL,
  `uid` varchar(30) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` varchar(64) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `FK_acl_group_acl_user1` (`owner_id`),
  CONSTRAINT `FK_acl_group_acl_user1` FOREIGN KEY (`owner_id`) REFERENCES `acl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_resource`
--

DROP TABLE IF EXISTS `acl_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_resource` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `cid` varchar(64) DEFAULT 'aclresource60',
  `cn` varchar(255) NOT NULL,
  `referToUid` varchar(64) DEFAULT NULL,
  `referToId` int(11) DEFAULT NULL,
  `referToCid` varchar(64) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `cn` (`cn`),
  KEY `referToUid` (`referToUid`),
  KEY `referToId` (`referToId`),
  KEY `referToCid` (`referToCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_right`
--

DROP TABLE IF EXISTS `acl_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_right` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_i_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_role`
--

DROP TABLE IF EXISTS `acl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_role` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT 'aclrole3zc547',
  `name` varchar(32) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(64) DEFAULT NULL,
  `description` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `parentId` (`parentId`),
  KEY `parentUid` (`parentUid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_rule`
--

DROP TABLE IF EXISTS `acl_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_rule` (
  `roleId` int(11) NOT NULL,
  `resource` varchar(128) NOT NULL,
  `rightId` int(11) NOT NULL,
  `rule` varchar(16) DEFAULT 'allow',
  PRIMARY KEY (`roleId`,`rightId`,`resource`),
  KEY `role_idx` (`roleId`),
  KEY `right_idx` (`rightId`),
  KEY `resource_idx` (`resource`),
  CONSTRAINT `FK_acl_rule_acl_resource1` FOREIGN KEY (`resource`) REFERENCES `acl_resource` (`cn`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_acl_rule_acl_right1` FOREIGN KEY (`rightId`) REFERENCES `acl_right` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_acl_rule_acl_role1` FOREIGN KEY (`roleId`) REFERENCES `acl_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_seq`
--

DROP TABLE IF EXISTS `acl_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=130899 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `acl_user`
--

DROP TABLE IF EXISTS `acl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_user` (
  `id` int(11) NOT NULL,
  `uid` varchar(128) NOT NULL,
  `login` varchar(128) NOT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `firstname` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `mail` varchar(128) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `owner_uid` varchar(64) DEFAULT NULL,
  `primary_role_id` int(11) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `auth_from` varchar(16) DEFAULT 'db',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_idx` (`uid`),
  KEY `login_idx` (`login`),
  KEY `owner_uid` (`owner_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acl_user_role`
--

DROP TABLE IF EXISTS `acl_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_user_role` (
  `uid` varchar(64) NOT NULL,
  `cid` char(13) NOT NULL DEFAULT 'acluserrole59',
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  `resourceCn` varchar(255) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`,`resourceCn`),
  KEY `userId` (`userId`),
  KEY `roleId` (`roleId`),
  KEY `resourceCn` (`resourceCn`),
  CONSTRAINT `FK_acl_user_role_acl_resource1` FOREIGN KEY (`resourceCn`) REFERENCES `acl_resource` (`cn`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_acl_user_role_acl_role1` FOREIGN KEY (`roleId`) REFERENCES `acl_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_acl_user_role_acl_user1` FOREIGN KEY (`userId`) REFERENCES `acl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_alias`
--

DROP TABLE IF EXISTS `bookshop_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT 'bookshop',
  `uid` varchar(128) NOT NULL DEFAULT 'bookshop',
  `designation` varchar(128) DEFAULT NULL,
  `object_class` varchar(13) NOT NULL DEFAULT 'bookshopAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_bookshop_alias` (`alias_id`,`container_id`),
  KEY `K_bookshop_alias_1` (`container_id`),
  KEY `K_bookshop_alias_2` (`number`),
  KEY `INDEX_uid` (`uid`),
  KEY `INDEX_name` (`name`),
  CONSTRAINT `FK_bookshop_alias_1` FOREIGN KEY (`container_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_categories`
--

DROP TABLE IF EXISTS `bookshop_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_categories` (
  `id` int(11) NOT NULL,
  `number` varchar(128) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `icon` varchar(64) DEFAULT NULL,
  `uid` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_category_rel`
--

DROP TABLE IF EXISTS `bookshop_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_bookshop_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_category_rel_1` (`parent_id`),
  KEY `K_project_category_rel_2` (`parent_uid`),
  KEY `K_project_category_rel_3` (`parent_cid`),
  KEY `K_project_category_rel_4` (`child_id`),
  KEY `K_project_category_rel_5` (`child_uid`),
  KEY `K_project_category_rel_6` (`child_cid`),
  KEY `K_project_category_rel_7` (`rdn`),
  KEY `K_bookshop_category_rel_1` (`parent_id`),
  KEY `K_bookshop_category_rel_2` (`parent_uid`),
  KEY `K_bookshop_category_rel_3` (`parent_cid`),
  KEY `K_bookshop_category_rel_4` (`child_id`),
  KEY `K_bookshop_category_rel_5` (`child_uid`),
  KEY `K_bookshop_category_rel_6` (`child_cid`),
  KEY `K_bookshop_category_rel_7` (`rdn`),
  CONSTRAINT `FK_bookshop_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `bookshop_categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onBookshopCategoryRelInsert BEFORE INSERT ON bookshop_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `bookshop_cont_metadata_seq`
--

DROP TABLE IF EXISTS `bookshop_cont_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files`
--

DROP TABLE IF EXISTS `bookshop_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  UNIQUE KEY `INDEX_bookshop_doc_files_uid` (`uid`),
  KEY `FK_bookshop_doc_files_1` (`document_id`),
  KEY `IK_bookshop_doc_files_1` (`document_id`),
  KEY `IK_bookshop_doc_files_2` (`name`),
  KEY `IK_bookshop_doc_files_3` (`path`(128)),
  KEY `IK_bookshop_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_bookshop_doc_files_5` (`mainrole`),
  KEY `IK_bookshop_doc_files_6` (`roles`(32)),
  KEY `IK_bookshop_doc_files_8` (`document_uid`),
  KEY `FK_bookshop_docfiles_7` (`create_by_uid`),
  KEY `FK_bookshop_docfiles_8` (`lock_by_uid`),
  KEY `FK_bookshop_docfiles_9` (`update_by_uid`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_bookshop_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `bookshop_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onBookshopDocfileDelete AFTER DELETE ON bookshop_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='bookshop';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `bookshop_doc_files_seq`
--

DROP TABLE IF EXISTS `bookshop_doc_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7851 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files_versions`
--

DROP TABLE IF EXISTS `bookshop_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  KEY `IK_bookshop_doc_files_version_1` (`document_id`),
  KEY `IK_bookshop_doc_files_version_2` (`name`),
  KEY `IK_bookshop_doc_files_version_3` (`path`),
  KEY `IK_bookshop_doc_files_version_4` (`iteration`,`document_id`),
  KEY `of_file_id` (`of_file_id`),
  KEY `IK_bookshop_doc_files_version_5` (`mainrole`),
  KEY `IK_bookshop_doc_files_version_6` (`roles`(32)),
  KEY `IK_bookshop_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `bookshop_doc_files_versions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=634 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_rel`
--

DROP TABLE IF EXISTS `bookshop_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_rel` (
  `link_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `hash` char(32) DEFAULT NULL,
  `data` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `FK_bookshop_doc_rel_10` (`parent_id`),
  KEY `FK_bookshop_doc_rel_2` (`child_id`),
  CONSTRAINT `FK_bookshop_doc_rel_1` FOREIGN KEY (`parent_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_doc_rel_2` FOREIGN KEY (`child_id`) REFERENCES `bookshop_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doc_rel_seq`
--

DROP TABLE IF EXISTS `bookshop_doc_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doccomments`
--

DROP TABLE IF EXISTS `bookshop_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doccomments_seq`
--

DROP TABLE IF EXISTS `bookshop_doccomments_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_doctype_rel`
--

DROP TABLE IF EXISTS `bookshop_doctype_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_bookshop_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_bookshop_doctype_rel_1` (`parent_id`),
  KEY `K_bookshop_doctype_rel_2` (`parent_uid`),
  KEY `K_bookshop_doctype_rel_3` (`parent_cid`),
  KEY `K_bookshop_doctype_rel_4` (`child_id`),
  KEY `K_bookshop_doctype_rel_5` (`child_uid`),
  KEY `K_bookshop_doctype_rel_6` (`child_cid`),
  KEY `K_bookshop_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_bookshop_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onBookshopDoctypeRelInsert BEFORE INSERT ON bookshop_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `bookshop_documents`
--

DROP TABLE IF EXISTS `bookshop_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(128) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'bookshop',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `doc_source` varchar(255) DEFAULT NULL,
  `commentaires` varchar(255) DEFAULT NULL,
  `applicabilite` varchar(255) DEFAULT NULL,
  `maj_int_ind` varchar(255) DEFAULT NULL,
  `min_int_ind` varchar(255) DEFAULT NULL,
  `mot_cles` varchar(255) DEFAULT NULL,
  `fournisseur` varchar(32) DEFAULT NULL,
  `docsier_fournisseur` varchar(32) DEFAULT NULL,
  `docsier_famille` varchar(12) DEFAULT NULL,
  `docsier_date` int(11) DEFAULT NULL,
  `docsier_keyword` mediumtext,
  `docsier_format` varchar(255) DEFAULT NULL,
  `ata` varchar(255) DEFAULT NULL,
  `sub_ata` varchar(255) DEFAULT NULL,
  `a_type` varchar(255) DEFAULT NULL,
  `orig_filename` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_bookshop_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_bookshop_documents_uid` (`uid`),
  KEY `FK_bookshop_documents_1` (`cad_model_supplier_id`),
  KEY `FK_bookshop_documents_2` (`supplier_id`),
  KEY `FK_bookshop_documents_3` (`container_id`),
  KEY `FK_bookshop_documents_4` (`category_id`),
  KEY `FK_bookshop_documents_5` (`doctype_id`),
  KEY `FK_bookshop_documents_6` (`version`),
  KEY `INDEX_bookshop_documents_spacename` (`spacename`),
  KEY `INDEX_bookshop_documents_name` (`name`),
  KEY `INDEX_bookshop_documents_cuid` (`container_uid`),
  KEY `FK_bookshop_documents_7` (`create_by_uid`),
  KEY `FK_bookshop_documents_8` (`lock_by_uid`),
  KEY `FK_bookshop_documents_9` (`update_by_uid`),
  KEY `FK_bookshop_documents_10` (`as_template`),
  FULLTEXT KEY `number` (`number`,`name`,`designation`),
  CONSTRAINT `FK_bookshop_documents_1` FOREIGN KEY (`container_id`) REFERENCES `bookshops` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onBookshopDocumentDelete AFTER DELETE ON bookshop_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=20;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `bookshop_documents_history`
--

DROP TABLE IF EXISTS `bookshop_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `action_by` varchar(32) NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_documents_history_seq`
--

DROP TABLE IF EXISTS `bookshop_documents_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14265 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_documents_seq`
--

DROP TABLE IF EXISTS `bookshop_documents_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11804 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_familles`
--

DROP TABLE IF EXISTS `bookshop_familles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_familles` (
  `id` varchar(12) NOT NULL DEFAULT '0',
  `famille_name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_files`
--

DROP TABLE IF EXISTS `bookshop_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` mediumtext NOT NULL,
  `file_root_name` varchar(128) NOT NULL DEFAULT '',
  `file_extension` varchar(16) DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) NOT NULL DEFAULT 'init',
  `file_type` varchar(128) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_bookshop_files_1` (`bookshop_id`),
  KEY `FK_bookshop_files_2` (`import_order`),
  KEY `index_file_name` (`file_name`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_import_history`
--

DROP TABLE IF EXISTS `bookshop_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_key_word`
--

DROP TABLE IF EXISTS `bookshop_key_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_key_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_word` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `key_word` (`key_word`)
) ENGINE=InnoDB AUTO_INCREMENT=1516 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_metadata`
--

DROP TABLE IF EXISTS `bookshop_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_metadata_seq`
--

DROP TABLE IF EXISTS `bookshop_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookshop_property_rel`
--

DROP TABLE IF EXISTS `bookshop_property_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshop_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_bookshop_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_bookshop_property_rel_1` (`parent_id`),
  KEY `K_bookshop_property_rel_2` (`parent_uid`),
  KEY `K_bookshop_property_rel_3` (`parent_cid`),
  KEY `K_bookshop_property_rel_4` (`child_id`),
  KEY `K_bookshop_property_rel_5` (`child_uid`),
  KEY `K_bookshop_property_rel_6` (`child_cid`),
  KEY `K_bookshop_property_rel_7` (`rdn`),
  CONSTRAINT `FK_bookshop_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `bookshops` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `bookshops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshop_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `bookshop_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onBookshopPropertyRelInsert BEFORE INSERT ON bookshop_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM bookshops WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `bookshops`
--

DROP TABLE IF EXISTS `bookshops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookshops` (
  `id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e94192201a',
  `dn` varchar(128) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `acode` int(11) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT 'init',
  `version` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` mediumtext NOT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `planned_closure` datetime DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `close_by_id` int(11) DEFAULT NULL,
  `close_by_uid` varchar(64) DEFAULT NULL,
  `spacename` varchar(16) NOT NULL DEFAULT 'bookshop',
  `proprietaire` varchar(255) DEFAULT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(8) NOT NULL DEFAULT 'bookshop',
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_bookshops_number` (`number`),
  UNIQUE KEY `UC_bookshops_uid` (`uid`),
  KEY `K_bookshops_3` (`create_by_uid`),
  KEY `K_bookshops_4` (`close_by_uid`),
  KEY `K_bookshops_name` (`name`),
  KEY `K_bookshops_dn` (`dn`),
  KEY `FK_bookshops_1` (`parent_id`),
  KEY `FK_bookshops_2` (`default_process_id`),
  FULLTEXT KEY `FT_bookshops_number` (`number`,`name`,`designation`),
  CONSTRAINT `FK_bookshops_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_bookshops_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onBookshopInsert BEFORE UPDATE ON bookshops FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	SET NEW.parent_uid = parentUid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onBookshopDelete AFTER DELETE ON bookshops FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='bookshop';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='bookshop';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='bookshop';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cadlib_alias`
--

DROP TABLE IF EXISTS `cadlib_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT 'cadlib',
  `uid` varchar(128) NOT NULL DEFAULT 'cadlib',
  `designation` varchar(128) DEFAULT NULL,
  `object_class` varchar(13) NOT NULL DEFAULT 'cadlibAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_cadlib_alias` (`alias_id`,`container_id`),
  KEY `K_cadlib_alias_1` (`container_id`),
  KEY `K_cadlib_alias_2` (`number`),
  KEY `INDEX_uid` (`uid`),
  KEY `INDEX_name` (`name`),
  CONSTRAINT `FK_cadlib_alias_1` FOREIGN KEY (`container_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_categories`
--

DROP TABLE IF EXISTS `cadlib_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_categories` (
  `id` int(11) NOT NULL,
  `number` varchar(128) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `icon` varchar(64) DEFAULT NULL,
  `uid` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_category_uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_category_rel`
--

DROP TABLE IF EXISTS `cadlib_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_cadlib_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_category_rel_1` (`parent_id`),
  KEY `K_project_category_rel_2` (`parent_uid`),
  KEY `K_project_category_rel_3` (`parent_cid`),
  KEY `K_project_category_rel_4` (`child_id`),
  KEY `K_project_category_rel_5` (`child_uid`),
  KEY `K_project_category_rel_6` (`child_cid`),
  KEY `K_project_category_rel_7` (`rdn`),
  KEY `K_cadlib_category_rel_1` (`parent_id`),
  KEY `K_cadlib_category_rel_2` (`parent_uid`),
  KEY `K_cadlib_category_rel_3` (`parent_cid`),
  KEY `K_cadlib_category_rel_4` (`child_id`),
  KEY `K_cadlib_category_rel_5` (`child_uid`),
  KEY `K_cadlib_category_rel_6` (`child_cid`),
  KEY `K_cadlib_category_rel_7` (`rdn`),
  CONSTRAINT `FK_cadlib_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `cadlib_categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onCadlibCategoryRelInsert BEFORE INSERT ON cadlib_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cadlib_cont_metadata_seq`
--

DROP TABLE IF EXISTS `cadlib_cont_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_files`
--

DROP TABLE IF EXISTS `cadlib_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  UNIQUE KEY `INDEX_cadlib_doc_files_uid` (`uid`),
  KEY `FK_cadlib_doc_files_1` (`document_id`),
  KEY `IK_cadlib_doc_files_1` (`document_id`),
  KEY `IK_cadlib_doc_files_2` (`name`),
  KEY `IK_cadlib_doc_files_3` (`path`(128)),
  KEY `IK_cadlib_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_cadlib_doc_files_5` (`mainrole`),
  KEY `IK_cadlib_doc_files_6` (`roles`(32)),
  KEY `IK_cadlib_doc_files_8` (`document_uid`),
  KEY `FK_cadlib_docfiles_7` (`create_by_uid`),
  KEY `FK_cadlib_docfiles_8` (`lock_by_uid`),
  KEY `FK_cadlib_docfiles_9` (`update_by_uid`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_cadlib_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `cadlib_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onCadlibDocfileDelete AFTER DELETE ON cadlib_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='cadlib';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cadlib_doc_files_seq`
--

DROP TABLE IF EXISTS `cadlib_doc_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_files_versions`
--

DROP TABLE IF EXISTS `cadlib_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`(128)),
  KEY `IK_cadlib_doc_files_version_1` (`document_id`),
  KEY `IK_cadlib_doc_files_version_2` (`name`),
  KEY `IK_cadlib_doc_files_version_3` (`path`(128)),
  KEY `IK_cadlib_doc_files_version_4` (`iteration`,`document_id`),
  KEY `of_file_id` (`of_file_id`),
  KEY `IK_cadlib_doc_files_version_5` (`mainrole`),
  KEY `IK_cadlib_doc_files_version_6` (`roles`(32)),
  KEY `IK_cadlib_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `cadlib_doc_files_versions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_rel`
--

DROP TABLE IF EXISTS `cadlib_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_rel` (
  `link_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `hash` char(32) DEFAULT NULL,
  `data` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `FK_cadlib_doc_rel_10` (`parent_id`),
  KEY `FK_cadlib_doc_rel_2` (`child_id`),
  CONSTRAINT `FK_cadlib_doc_rel_1` FOREIGN KEY (`parent_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_doc_rel_2` FOREIGN KEY (`child_id`) REFERENCES `cadlib_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doc_rel_seq`
--

DROP TABLE IF EXISTS `cadlib_doc_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doccomments`
--

DROP TABLE IF EXISTS `cadlib_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doccomments_seq`
--

DROP TABLE IF EXISTS `cadlib_doccomments_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_doctype_rel`
--

DROP TABLE IF EXISTS `cadlib_doctype_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_cadlib_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_cadlib_doctype_rel_1` (`parent_id`),
  KEY `K_cadlib_doctype_rel_2` (`parent_uid`),
  KEY `K_cadlib_doctype_rel_3` (`parent_cid`),
  KEY `K_cadlib_doctype_rel_4` (`child_id`),
  KEY `K_cadlib_doctype_rel_5` (`child_uid`),
  KEY `K_cadlib_doctype_rel_6` (`child_cid`),
  KEY `K_cadlib_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_cadlib_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onCadlibDoctypeRelInsert BEFORE INSERT ON cadlib_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cadlib_documents`
--

DROP TABLE IF EXISTS `cadlib_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'cadlib',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `fabricant_a` varchar(255) DEFAULT NULL,
  `fabricant_b` varchar(255) DEFAULT NULL,
  `fabricant_c` varchar(255) DEFAULT NULL,
  `ref_a` varchar(255) DEFAULT NULL,
  `ref_b` varchar(255) DEFAULT NULL,
  `ref_c` varchar(255) DEFAULT NULL,
  `chapitre` varchar(255) DEFAULT NULL,
  `subchapitre` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_cadlib_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_cadlib_documents_uid` (`uid`),
  KEY `FK_cadlib_documents_1` (`cad_model_supplier_id`),
  KEY `FK_cadlib_documents_2` (`supplier_id`),
  KEY `FK_cadlib_documents_3` (`container_id`),
  KEY `FK_cadlib_documents_4` (`category_id`),
  KEY `FK_cadlib_documents_5` (`doctype_id`),
  KEY `FK_cadlib_documents_6` (`version`),
  KEY `INDEX_cadlib_documents_spacename` (`spacename`),
  KEY `INDEX_cadlib_documents_name` (`name`),
  KEY `INDEX_cadlib_documents_cuid` (`container_uid`),
  KEY `FK_cadlib_documents_7` (`create_by_uid`),
  KEY `FK_cadlib_documents_8` (`lock_by_uid`),
  KEY `FK_cadlib_documents_9` (`update_by_uid`),
  KEY `FK_cadlib_documents_10` (`as_template`),
  FULLTEXT KEY `number` (`number`,`name`,`designation`),
  CONSTRAINT `FK_cadlib_documents_1` FOREIGN KEY (`container_id`) REFERENCES `cadlibs` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onCadlibDocumentDelete AFTER DELETE ON cadlib_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=25;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cadlib_documents_history`
--

DROP TABLE IF EXISTS `cadlib_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `action_by` varchar(32) NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_documents_history_seq`
--

DROP TABLE IF EXISTS `cadlib_documents_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_documents_seq`
--

DROP TABLE IF EXISTS `cadlib_documents_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_files`
--

DROP TABLE IF EXISTS `cadlib_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` mediumtext NOT NULL,
  `file_root_name` varchar(128) NOT NULL DEFAULT '',
  `file_extension` varchar(16) DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) NOT NULL DEFAULT 'init',
  `file_type` varchar(128) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_cadlib_files_1` (`cadlib_id`),
  KEY `FK_cadlib_files_2` (`import_order`),
  KEY `index_file_name` (`file_name`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_import_history`
--

DROP TABLE IF EXISTS `cadlib_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_metadata`
--

DROP TABLE IF EXISTS `cadlib_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_metadata_seq`
--

DROP TABLE IF EXISTS `cadlib_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cadlib_property_rel`
--

DROP TABLE IF EXISTS `cadlib_property_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlib_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_cadlib_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_cadlib_property_rel_1` (`parent_id`),
  KEY `K_cadlib_property_rel_2` (`parent_uid`),
  KEY `K_cadlib_property_rel_3` (`parent_cid`),
  KEY `K_cadlib_property_rel_4` (`child_id`),
  KEY `K_cadlib_property_rel_5` (`child_uid`),
  KEY `K_cadlib_property_rel_6` (`child_cid`),
  KEY `K_cadlib_property_rel_7` (`rdn`),
  CONSTRAINT `FK_cadlib_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `cadlibs` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `cadlibs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlib_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `cadlib_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onCadlibPropertyRelInsert BEFORE INSERT ON cadlib_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM cadlibs WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cadlibs`
--

DROP TABLE IF EXISTS `cadlibs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadlibs` (
  `id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e94192201a',
  `dn` varchar(128) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `acode` int(11) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT 'init',
  `version` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` mediumtext NOT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `planned_closure` datetime DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `close_by_id` int(11) DEFAULT NULL,
  `close_by_uid` varchar(64) DEFAULT NULL,
  `spacename` varchar(16) NOT NULL DEFAULT 'cadlib',
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(6) NOT NULL DEFAULT 'cadlib',
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_cadlibs_number` (`number`),
  UNIQUE KEY `UC_cadlibs_uid` (`uid`),
  KEY `K_cadlibs_3` (`create_by_uid`),
  KEY `K_cadlibs_4` (`close_by_uid`),
  KEY `K_cadlibs_name` (`name`),
  KEY `K_cadlibs_dn` (`dn`),
  KEY `FK_cadlibs_1` (`parent_id`),
  KEY `FK_cadlibs_2` (`default_process_id`),
  FULLTEXT KEY `FT_cadlibs_number` (`number`,`name`,`designation`),
  CONSTRAINT `FK_cadlibs_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cadlibs_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onCadlibInsert BEFORE UPDATE ON cadlibs FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
	SET NEW.parent_uid = parentUid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onCadlibDelete AFTER DELETE ON cadlibs FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='cadlib';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='cadlib';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='cadlib';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `callbacks`
--

DROP TABLE IF EXISTS `callbacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(128) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `reference_id` varchar(128) DEFAULT NULL,
  `reference_uid` varchar(128) DEFAULT NULL,
  `reference_cid` varchar(128) DEFAULT NULL,
  `spacename` varchar(64) DEFAULT NULL,
  `events` varchar(256) DEFAULT NULL,
  `callback_class` varchar(256) DEFAULT NULL,
  `callback_method` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_callbacks_1` (`uid`),
  KEY `INDEX_callbacks_1` (`uid`),
  KEY `INDEX_callbacks_2` (`cid`),
  KEY `INDEX_callbacks_3` (`reference_id`),
  KEY `INDEX_callbacks_4` (`reference_uid`),
  KEY `INDEX_callbacks_5` (`reference_cid`),
  KEY `INDEX_callbacks_8` (`events`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories_seq`
--

DROP TABLE IF EXISTS `categories_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `checkout_index`
--

DROP TABLE IF EXISTS `checkout_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkout_index` (
  `file_name` varchar(128) NOT NULL DEFAULT '',
  `file_id` int(11) NOT NULL,
  `file_uid` varchar(140) NOT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `spacename` varchar(128) NOT NULL DEFAULT '',
  `container_id` int(11) NOT NULL DEFAULT '0',
  `container_number` varchar(128) NOT NULL DEFAULT '',
  `check_out_by` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_name`),
  KEY `IK_checkout_index_1` (`document_id`),
  KEY `IK_checkout_index_2` (`file_id`),
  KEY `IK_checkout_index_3` (`file_uid`),
  KEY `IK_checkout_index_4` (`spacename`),
  KEY `IK_checkout_index_5` (`container_id`),
  KEY `IK_checkout_index_6` (`container_number`),
  KEY `IK_checkout_index_7` (`file_uid`),
  KEY `IK_checkout_index_8` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_favorite`
--

DROP TABLE IF EXISTS `container_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_favorite` (
  `user_id` int(11) NOT NULL,
  `container_id` int(11) NOT NULL,
  `container_number` varchar(512) NOT NULL,
  `space_id` int(11) NOT NULL,
  `spacename` varchar(16) NOT NULL,
  PRIMARY KEY (`user_id`,`container_id`,`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_indice`
--

DROP TABLE IF EXISTS `container_indice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_indice` (
  `indice_id` int(11) NOT NULL DEFAULT '0',
  `indice_value` varchar(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`indice_id`),
  UNIQUE KEY `UC_indice_id` (`indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discussion_comment`
--

DROP TABLE IF EXISTS `discussion_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `cid` char(16) NOT NULL DEFAULT '568be4fc7a0a8',
  `name` varchar(255) DEFAULT NULL,
  `discussion_uid` varchar(255) NOT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(255) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `body` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `DISCUSSION_COMMENT_PARENTID` (`parent_id`),
  KEY `DISCUSSION_COMMENT_PARENTUID` (`parent_uid`),
  KEY `DISCUSSION_COMMENT_DISCUSSIONUID` (`discussion_uid`),
  KEY `DISCUSSION_COMMENT_OWNERID` (`owner_id`),
  KEY `DISCUSSION_COMMENT_UPDATED` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `docbasket`
--

DROP TABLE IF EXISTS `docbasket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docbasket` (
  `user_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `space_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`document_id`,`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `docseeder_mask`
--

DROP TABLE IF EXISTS `docseeder_mask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docseeder_mask` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `map` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `docseeder_type`
--

DROP TABLE IF EXISTS `docseeder_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docseeder_type` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  `designation` varchar(256) DEFAULT NULL,
  `domain` varchar(64) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL COMMENT 'id of the template docfile',
  `template_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the template docfile',
  `mask_id` int(11) DEFAULT NULL COMMENT 'id of the mask to use',
  `mask_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the mask',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `template_id` (`template_id`),
  KEY `template_uid` (`template_uid`),
  KEY `mask_id` (`mask_id`),
  KEY `mask_uid` (`mask_uid`),
  KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `doctypes`
--

DROP TABLE IF EXISTS `doctypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctypes` (
  `id` int(11) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `number` varchar(128) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `can_be_composite` tinyint(4) NOT NULL DEFAULT '1',
  `file_extensions` varchar(256) DEFAULT NULL,
  `file_type` varchar(128) DEFAULT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `recognition_regexp` mediumtext,
  `visu_file_extension` varchar(32) DEFAULT NULL,
  `pre_store_class` varchar(64) DEFAULT NULL,
  `pre_store_method` varchar(64) DEFAULT NULL,
  `post_store_class` varchar(64) DEFAULT NULL,
  `post_store_method` varchar(64) DEFAULT NULL,
  `pre_update_class` varchar(64) DEFAULT NULL,
  `pre_update_method` varchar(64) DEFAULT NULL,
  `post_update_class` varchar(64) DEFAULT NULL,
  `post_update_method` varchar(64) DEFAULT NULL,
  `domain` varchar(64) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL COMMENT 'id of the template docfile',
  `template_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the template docfile',
  `mask_id` int(11) DEFAULT NULL COMMENT 'id of the mask to use',
  `mask_uid` varchar(64) DEFAULT NULL COMMENT 'uid of the mask',
  `docseeder` tinyint(1) DEFAULT NULL COMMENT 'flag to retrieve the valid type for docseeder',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_doctype_number` (`number`),
  UNIQUE KEY `INDEX_doctypes_1` (`uid`),
  UNIQUE KEY `number` (`number`),
  KEY `INDEX_doctypes_2` (`number`),
  KEY `INDEX_doctypes_3` (`designation`),
  KEY `template_id` (`template_id`),
  KEY `template_uid` (`template_uid`),
  KEY `mask_id` (`mask_id`),
  KEY `mask_uid` (`mask_uid`),
  KEY `domain` (`domain`),
  KEY `docseeder` (`docseeder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `doctypes_seq`
--

DROP TABLE IF EXISTS `doctypes_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctypes_seq` (
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document_indice`
--

DROP TABLE IF EXISTS `document_indice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_indice` (
  `indice_id` int(11) NOT NULL DEFAULT '0',
  `indice_value` varchar(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`indice_id`),
  UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `indexer`
--

DROP TABLE IF EXISTS `indexer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indexer` (
  `document_id` int(11) NOT NULL,
  `docfile_id` int(11) NOT NULL,
  `spacename` varchar(32) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `content` mediumtext,
  PRIMARY KEY (`docfile_id`,`spacename`),
  UNIQUE KEY `UK_indexer_1` (`document_id`),
  KEY `UK_indexer_2` (`spacename`),
  FULLTEXT KEY `content` (`content`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_archive`
--

DROP TABLE IF EXISTS `message_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_archive` (
  `id` int(14) NOT NULL,
  `ownerUid` varchar(128) NOT NULL,
  `created` datetime DEFAULT NULL,
  `from` varchar(200) NOT NULL,
  `to` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `hash` varchar(32) DEFAULT NULL,
  `replyto_hash` varchar(32) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `isReplied` tinyint(1) DEFAULT '0',
  `isFlagged` tinyint(1) DEFAULT '0',
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerUid` (`ownerUid`),
  KEY `isRead` (`isRead`),
  KEY `isReplied` (`isReplied`),
  KEY `isFlagged` (`isFlagged`),
  KEY `subject` (`subject`),
  FULLTEXT KEY `body` (`body`,`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_mailbox`
--

DROP TABLE IF EXISTS `message_mailbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_mailbox` (
  `id` int(14) NOT NULL,
  `ownerUid` varchar(128) NOT NULL,
  `created` datetime DEFAULT NULL,
  `from` varchar(200) NOT NULL,
  `to` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `hash` varchar(32) DEFAULT NULL,
  `replyto_hash` varchar(32) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `isReplied` tinyint(1) DEFAULT '0',
  `isFlagged` tinyint(1) DEFAULT '0',
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerUid` (`ownerUid`),
  KEY `isRead` (`isRead`),
  KEY `isReplied` (`isReplied`),
  KEY `isFlagged` (`isFlagged`),
  KEY `subject` (`subject`),
  FULLTEXT KEY `body` (`body`,`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_sent`
--

DROP TABLE IF EXISTS `message_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_sent` (
  `id` int(14) NOT NULL,
  `ownerUid` varchar(128) NOT NULL,
  `created` datetime DEFAULT NULL,
  `from` varchar(200) NOT NULL,
  `to` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `hash` varchar(32) DEFAULT NULL,
  `replyto_hash` varchar(32) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `isReplied` tinyint(1) DEFAULT '0',
  `isFlagged` tinyint(1) DEFAULT '0',
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerUid` (`ownerUid`),
  KEY `isRead` (`isRead`),
  KEY `isReplied` (`isReplied`),
  KEY `isFlagged` (`isFlagged`),
  KEY `subject` (`subject`),
  FULLTEXT KEY `body` (`body`,`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_seq`
--

DROP TABLE IF EXISTS `message_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_alias`
--

DROP TABLE IF EXISTS `mockup_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT 'mockup',
  `uid` varchar(128) NOT NULL DEFAULT 'mockup',
  `designation` varchar(128) DEFAULT NULL,
  `object_class` varchar(13) NOT NULL DEFAULT 'mockupAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_mockup_alias` (`alias_id`,`container_id`),
  KEY `K_mockup_alias_1` (`container_id`),
  KEY `K_mockup_alias_2` (`number`),
  KEY `INDEX_uid` (`uid`),
  KEY `INDEX_name` (`name`),
  CONSTRAINT `FK_mockup_alias_1` FOREIGN KEY (`container_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_categories`
--

DROP TABLE IF EXISTS `mockup_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_categories` (
  `id` int(11) NOT NULL,
  `number` varchar(128) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `icon` varchar(64) DEFAULT NULL,
  `uid` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_category_uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_category_rel`
--

DROP TABLE IF EXISTS `mockup_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_mockup_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_category_rel_1` (`parent_id`),
  KEY `K_project_category_rel_2` (`parent_uid`),
  KEY `K_project_category_rel_3` (`parent_cid`),
  KEY `K_project_category_rel_4` (`child_id`),
  KEY `K_project_category_rel_5` (`child_uid`),
  KEY `K_project_category_rel_6` (`child_cid`),
  KEY `K_project_category_rel_7` (`rdn`),
  KEY `K_mockup_category_rel_1` (`parent_id`),
  KEY `K_mockup_category_rel_2` (`parent_uid`),
  KEY `K_mockup_category_rel_3` (`parent_cid`),
  KEY `K_mockup_category_rel_4` (`child_id`),
  KEY `K_mockup_category_rel_5` (`child_uid`),
  KEY `K_mockup_category_rel_6` (`child_cid`),
  KEY `K_mockup_category_rel_7` (`rdn`),
  CONSTRAINT `FK_mockup_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `mockup_categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onMockupCategoryRelInsert BEFORE INSERT ON mockup_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mockup_category_rel_seq`
--

DROP TABLE IF EXISTS `mockup_category_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_cont_metadata_seq`
--

DROP TABLE IF EXISTS `mockup_cont_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_files`
--

DROP TABLE IF EXISTS `mockup_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`),
  UNIQUE KEY `INDEX_mockup_doc_files_uid` (`uid`),
  KEY `FK_mockup_doc_files_1` (`document_id`),
  KEY `IK_mockup_doc_files_1` (`document_id`),
  KEY `IK_mockup_doc_files_2` (`name`),
  KEY `IK_mockup_doc_files_3` (`path`(128)),
  KEY `IK_mockup_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_mockup_doc_files_5` (`mainrole`),
  KEY `IK_mockup_doc_files_6` (`roles`(32)),
  KEY `IK_mockup_doc_files_8` (`document_uid`),
  KEY `FK_mockup_docfiles_7` (`create_by_uid`),
  KEY `FK_mockup_docfiles_8` (`lock_by_uid`),
  KEY `FK_mockup_docfiles_9` (`update_by_uid`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_mockup_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `mockup_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onMockupDocfileDelete AFTER DELETE ON mockup_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='mockup';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mockup_doc_files_seq`
--

DROP TABLE IF EXISTS `mockup_doc_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_files_versions`
--

DROP TABLE IF EXISTS `mockup_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`(128)),
  KEY `IK_mockup_doc_files_version_1` (`document_id`),
  KEY `IK_mockup_doc_files_version_2` (`name`),
  KEY `IK_mockup_doc_files_version_3` (`path`(128)),
  KEY `IK_mockup_doc_files_version_4` (`iteration`,`document_id`),
  KEY `of_file_id` (`of_file_id`),
  KEY `IK_mockup_doc_files_version_5` (`mainrole`),
  KEY `IK_mockup_doc_files_version_6` (`roles`(32)),
  KEY `IK_mockup_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `mockup_doc_files_versions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_rel`
--

DROP TABLE IF EXISTS `mockup_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_rel` (
  `link_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `hash` char(32) DEFAULT NULL,
  `data` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `FK_mockup_doc_rel_10` (`parent_id`),
  KEY `FK_mockup_doc_rel_2` (`child_id`),
  CONSTRAINT `FK_mockup_doc_rel_1` FOREIGN KEY (`parent_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_doc_rel_2` FOREIGN KEY (`child_id`) REFERENCES `mockup_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doc_rel_seq`
--

DROP TABLE IF EXISTS `mockup_doc_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doccomments`
--

DROP TABLE IF EXISTS `mockup_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doccomments_seq`
--

DROP TABLE IF EXISTS `mockup_doccomments_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doctype_process`
--

DROP TABLE IF EXISTS `mockup_doctype_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `mockup_doctype_process_uniq1` (`mockup_id`,`doctype_id`),
  KEY `FK_mockup_doctype_process_1` (`category_id`),
  KEY `FK_mockup_doctype_process_3` (`doctype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doctype_process_seq`
--

DROP TABLE IF EXISTS `mockup_doctype_process_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_doctype_rel`
--

DROP TABLE IF EXISTS `mockup_doctype_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_mockup_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_mockup_doctype_rel_1` (`parent_id`),
  KEY `K_mockup_doctype_rel_2` (`parent_uid`),
  KEY `K_mockup_doctype_rel_3` (`parent_cid`),
  KEY `K_mockup_doctype_rel_4` (`child_id`),
  KEY `K_mockup_doctype_rel_5` (`child_uid`),
  KEY `K_mockup_doctype_rel_6` (`child_cid`),
  KEY `K_mockup_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_mockup_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onMockupDoctypeRelInsert BEFORE INSERT ON mockup_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mockup_documents`
--

DROP TABLE IF EXISTS `mockup_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'mockup',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_mockup_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_mockup_documents_uid` (`uid`),
  KEY `FK_mockup_documents_1` (`cad_model_supplier_id`),
  KEY `FK_mockup_documents_2` (`supplier_id`),
  KEY `FK_mockup_documents_3` (`container_id`),
  KEY `FK_mockup_documents_4` (`category_id`),
  KEY `FK_mockup_documents_5` (`doctype_id`),
  KEY `FK_mockup_documents_6` (`version`),
  KEY `INDEX_mockup_documents_spacename` (`spacename`),
  KEY `INDEX_mockup_documents_name` (`name`),
  KEY `INDEX_mockup_documents_cuid` (`container_uid`),
  KEY `FK_mockup_documents_7` (`create_by_uid`),
  KEY `FK_mockup_documents_8` (`lock_by_uid`),
  KEY `FK_mockup_documents_9` (`update_by_uid`),
  KEY `FK_mockup_documents_10` (`as_template`),
  FULLTEXT KEY `number` (`number`,`name`,`designation`),
  CONSTRAINT `FK_mockup_documents_1` FOREIGN KEY (`container_id`) REFERENCES `mockups` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onMockupDocumentDelete AFTER DELETE ON mockup_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=15;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mockup_documents_history`
--

DROP TABLE IF EXISTS `mockup_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `action_by` varchar(32) NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_documents_history_seq`
--

DROP TABLE IF EXISTS `mockup_documents_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_documents_seq`
--

DROP TABLE IF EXISTS `mockup_documents_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_files`
--

DROP TABLE IF EXISTS `mockup_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` mediumtext NOT NULL,
  `file_root_name` varchar(128) NOT NULL DEFAULT '',
  `file_extension` varchar(16) DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) NOT NULL DEFAULT 'init',
  `file_type` varchar(128) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_mockup_files_1` (`mockup_id`),
  KEY `FK_mockup_files_2` (`import_order`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`),
  KEY `index_file_name` (`file_name`),
  KEY `file_md5` (`file_md5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_files_seq`
--

DROP TABLE IF EXISTS `mockup_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_files_seq` (
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_import_history`
--

DROP TABLE IF EXISTS `mockup_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` mediumtext,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_import_history_seq`
--

DROP TABLE IF EXISTS `mockup_import_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_import_history_seq` (
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_metadata`
--

DROP TABLE IF EXISTS `mockup_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_metadata_seq`
--

DROP TABLE IF EXISTS `mockup_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mockup_property_rel`
--

DROP TABLE IF EXISTS `mockup_property_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockup_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_mockup_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_mockup_property_rel_1` (`parent_id`),
  KEY `K_mockup_property_rel_2` (`parent_uid`),
  KEY `K_mockup_property_rel_3` (`parent_cid`),
  KEY `K_mockup_property_rel_4` (`child_id`),
  KEY `K_mockup_property_rel_5` (`child_uid`),
  KEY `K_mockup_property_rel_6` (`child_cid`),
  KEY `K_mockup_property_rel_7` (`rdn`),
  CONSTRAINT `FK_mockup_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `mockups` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `mockups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mockup_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `mockup_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onMockupPropertyRelInsert BEFORE INSERT ON mockup_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM mockups WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mockups`
--

DROP TABLE IF EXISTS `mockups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mockups` (
  `id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e94192201a',
  `dn` varchar(128) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `acode` int(11) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT 'init',
  `version` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` mediumtext NOT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `planned_closure` datetime DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `close_by_id` int(11) DEFAULT NULL,
  `close_by_uid` varchar(64) DEFAULT NULL,
  `spacename` varchar(16) NOT NULL DEFAULT 'mockup',
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(6) NOT NULL DEFAULT 'mockup',
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(64) DEFAULT NULL,
  `package_regex` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_mockups_number` (`number`),
  UNIQUE KEY `UC_mockups_uid` (`uid`),
  KEY `K_mockups_3` (`create_by_uid`),
  KEY `K_mockups_4` (`close_by_uid`),
  KEY `K_mockups_name` (`name`),
  KEY `K_mockups_dn` (`dn`),
  KEY `FK_mockups_1` (`parent_id`),
  KEY `FK_mockups_2` (`default_process_id`),
  FULLTEXT KEY `FT_mockups_number` (`number`,`name`,`designation`),
  CONSTRAINT `FK_mockups_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_mockups_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onMockupInsert BEFORE UPDATE ON mockups FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	IF(OLD.parent_id <> NEW.parent_id) THEN
		SELECT dn, uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id;
		SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
		SET NEW.parent_uid = parentUid;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onMockupDelete AFTER DELETE ON mockups FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='mockup';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='mockup';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='mockup';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_uid` varchar(128) NOT NULL,
  `owner_id` varchar(128) NOT NULL,
  `reference_uid` varchar(128) NOT NULL,
  `reference_cid` varchar(128) DEFAULT NULL,
  `reference_id` varchar(128) DEFAULT NULL,
  `spacename` varchar(64) NOT NULL,
  `events` varchar(256) NOT NULL,
  `condition` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_notifications_1` (`owner_id`,`reference_uid`,`events`),
  KEY `INDEX_notifications_1` (`owner_id`),
  KEY `INDEX_notifications_2` (`reference_uid`),
  KEY `INDEX_notifications_3` (`events`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `objects`
--

DROP TABLE IF EXISTS `objects`;
/*!50001 DROP VIEW IF EXISTS `objects`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `objects` AS SELECT 
 1 AS `id`,
 1 AS `uid`,
 1 AS `number`,
 1 AS `name`,
 1 AS `designation`,
 1 AS `version`,
 1 AS `iteration`,
 1 AS `doctypeId`,
 1 AS `lockById`,
 1 AS `locked`,
 1 AS `updated`,
 1 AS `updateById`,
 1 AS `created`,
 1 AS `createById`,
 1 AS `spacename`,
 1 AS `cid`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `org_seq`
--

DROP TABLE IF EXISTS `org_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=926 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `number` varchar(128) NOT NULL DEFAULT '',
  `type` enum('customer','supplier','staff') DEFAULT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `adress` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `cell_phone` varchar(64) DEFAULT NULL,
  `mail` varchar(64) DEFAULT NULL,
  `web_site` varchar(64) DEFAULT NULL,
  `activity` varchar(64) DEFAULT NULL,
  `company` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_partner_number` (`number`),
  KEY `IK_partner_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partners_seq`
--

DROP TABLE IF EXISTS `partners_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=965 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pdm_product_instance`
--

DROP TABLE IF EXISTS `pdm_product_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdm_product_instance` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL DEFAULT '569e971bb57c0',
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `nomenclature` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `position` varchar(512) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `of_product_id` int(11) DEFAULT NULL,
  `of_product_uid` varchar(32) DEFAULT NULL,
  `context_id` varchar(32) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdm_product_instance_uniq1` (`uid`),
  KEY `INDEX_pdm_product_instance_1` (`cid`),
  KEY `INDEX_pdm_product_instance_2` (`number`),
  KEY `INDEX_pdm_product_instance_3` (`description`),
  KEY `INDEX_pdm_product_instance_4` (`nomenclature`),
  KEY `INDEX_pdm_product_instance_5` (`uid`),
  KEY `INDEX_pdm_product_instance_6` (`name`),
  KEY `INDEX_pdm_product_instance_7` (`type`),
  KEY `INDEX_pdm_product_version_8` (`of_product_uid`),
  KEY `INDEX_pdm_product_version_9` (`of_product_id`),
  KEY `INDEX_pdm_product_version_10` (`path`),
  FULLTEXT KEY `name` (`name`,`number`,`description`),
  CONSTRAINT `FK_pdm_product_instance_pdm_product_version1` FOREIGN KEY (`of_product_id`) REFERENCES `pdm_product_version` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pdm_product_version`
--

DROP TABLE IF EXISTS `pdm_product_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdm_product_version` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `of_product_id` int(11) DEFAULT NULL,
  `of_product_uid` varchar(32) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `document_uid` varchar(32) DEFAULT NULL,
  `spacename` varchar(32) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `materials` varchar(512) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `volume` float DEFAULT NULL,
  `wetsurface` float DEFAULT NULL,
  `density` float DEFAULT NULL,
  `gravitycenter` varchar(512) DEFAULT NULL,
  `inertiacenter` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdm_product_version_uniq2` (`uid`),
  UNIQUE KEY `pdm_product_version_uniq1` (`of_product_uid`,`version`),
  KEY `INDEX_pdm_product_version_1` (`of_product_uid`),
  KEY `INDEX_pdm_product_version_6` (`of_product_id`),
  KEY `INDEX_pdm_product_version_2` (`version`),
  KEY `INDEX_pdm_product_version_3` (`description`),
  KEY `INDEX_pdm_product_version_4` (`uid`),
  KEY `INDEX_pdm_product_version_5` (`name`),
  KEY `INDEX_pdm_product_version_7` (`document_id`),
  KEY `INDEX_pdm_product_version_8` (`document_uid`),
  KEY `INDEX_pdm_product_version_9` (`spacename`),
  KEY `INDEX_pdm_product_instance_10` (`type`),
  FULLTEXT KEY `name` (`name`,`number`,`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pdm_seq`
--

DROP TABLE IF EXISTS `pdm_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdm_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `postit`
--

DROP TABLE IF EXISTS `postit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postit` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(32) DEFAULT NULL,
  `parent_cid` varchar(32) NOT NULL,
  `owner_id` varchar(32) DEFAULT NULL,
  `spacename` varchar(32) NOT NULL,
  `body` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_uid` (`uid`),
  KEY `K_parentId` (`parent_id`),
  KEY `K_parentUid` (`parent_uid`),
  KEY `K_parentCid` (`parent_cid`),
  KEY `K_spaceName` (`spacename`),
  KEY `K_ownerId` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `postit_seq`
--

DROP TABLE IF EXISTS `postit_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postit_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_category_rel`
--

DROP TABLE IF EXISTS `project_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_category_rel_1` (`parent_id`),
  KEY `K_project_category_rel_2` (`parent_uid`),
  KEY `K_project_category_rel_3` (`parent_cid`),
  KEY `K_project_category_rel_4` (`child_id`),
  KEY `K_project_category_rel_5` (`child_uid`),
  KEY `K_project_category_rel_6` (`child_cid`),
  KEY `K_project_category_rel_7` (`rdn`),
  CONSTRAINT `FK_project_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_project_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onOrgCategoryRelInsert BEFORE INSERT ON project_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM doctypes WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_container_rel`
--

DROP TABLE IF EXISTS `project_container_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_container_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `spacename` varchar(64) NOT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_container_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_container_rel_1` (`parent_id`),
  KEY `K_project_container_rel_2` (`parent_uid`),
  KEY `K_project_container_rel_3` (`parent_cid`),
  KEY `K_project_container_rel_4` (`child_id`),
  KEY `K_project_container_rel_5` (`child_uid`),
  KEY `K_project_container_rel_6` (`child_cid`),
  KEY `K_project_container_rel_7` (`rdn`),
  CONSTRAINT `FK_project_container_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_project_container_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onOrgContainerRelInsert BEFORE INSERT ON project_container_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	CASE NEW.spacename
		WHEN 'bookshop' THEN
			SELECT uid INTO childUid FROM bookshops WHERE id=NEW.child_id;
		WHEN 'cadlib' THEN
			SELECT uid INTO childUid FROM cadlibs WHERE id=NEW.child_id;
		WHEN 'mockup' THEN
			SELECT uid INTO childUid FROM mockups WHERE id=NEW.child_id;
		WHEN 'workitem' THEN
			SELECT uid INTO childUid FROM workitems WHERE id=NEW.child_id;
	END CASE;
			
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_doctype_rel`
--

DROP TABLE IF EXISTS `project_doctype_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  CONSTRAINT `U_project_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `U_project_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `U_project_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_process_rel`
--

DROP TABLE IF EXISTS `project_process_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_process_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_process_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  KEY `K_project_process_rel_1` (`parent_id`),
  KEY `K_project_process_rel_2` (`parent_uid`),
  KEY `K_project_process_rel_3` (`parent_cid`),
  KEY `K_project_process_rel_4` (`child_id`),
  KEY `K_project_process_rel_5` (`child_uid`),
  KEY `K_project_process_rel_6` (`child_cid`),
  KEY `K_project_process_rel_7` (`rdn`),
  CONSTRAINT `U_project_process_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `U_project_process_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `U_project_process_rel_3` FOREIGN KEY (`child_id`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onProjectDoctypeRelInsert BEFORE INSERT ON project_process_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	DECLARE parentUid varchar(128);
	DECLARE childUid varchar(128);
	SELECT dn,uid INTO parentDn, parentUid FROM projects WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SELECT uid INTO childUid FROM wf_process WHERE id=NEW.child_id;
	SET NEW.rdn = parentDn;
	SET NEW.parent_uid = parentUid;
	SET NEW.child_uid = childUid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `project_property_rel`
--

DROP TABLE IF EXISTS `project_property_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  CONSTRAINT `FK_project_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `projects` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_project_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) NOT NULL,
  `cid` varchar(64) DEFAULT '569e93c6ee156',
  `dn` varchar(128) DEFAULT NULL,
  `number` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(64) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `planned_closure` datetime DEFAULT NULL,
  `close_by_id` int(11) DEFAULT NULL,
  `close_by_uid` varchar(64) DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IK_project_uid` (`uid`),
  UNIQUE KEY `UC_project_number` (`number`),
  KEY `FK_projects_1` (`version`),
  KEY `UC_project_name` (`name`),
  KEY `UC_project_dn` (`dn`),
  KEY `K_projects_3` (`create_by_uid`),
  KEY `K_projects_4` (`close_by_uid`),
  FULLTEXT KEY `number` (`number`,`name`,`designation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onProjectInsert BEFORE INSERT ON projects FOR EACH ROW 
BEGIN
	SET NEW.dn=CONCAT('/',NEW.uid,'/');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onProjectUpdate BEFORE UPDATE ON projects FOR EACH ROW 
BEGIN
	SET NEW.dn=CONCAT('/',NEW.uid,'/');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onProjectDelete AFTER DELETE ON projects FOR EACH ROW 
BEGIN
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `rt_aif_code_a350`
--

DROP TABLE IF EXISTS `rt_aif_code_a350`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rt_aif_code_a350` (
  `code` char(15) NOT NULL,
  `itp_cluster` char(4) DEFAULT NULL,
  `check` char(2) DEFAULT NULL,
  `mistake` char(2) DEFAULT NULL,
  `type` enum('B','I','OS') NOT NULL,
  `status` char(1) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Codes erreurs pour le programme A350';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rt_aif_code_a350_seq`
--

DROP TABLE IF EXISTS `rt_aif_code_a350_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rt_aif_code_a350_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rt_aif_code_a380`
--

DROP TABLE IF EXISTS `rt_aif_code_a380`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rt_aif_code_a380` (
  `status` varchar(24) NOT NULL,
  `code` char(8) NOT NULL,
  `title` mediumtext NOT NULL,
  `itp` varchar(24) NOT NULL,
  `stec` tinyint(4) NOT NULL,
  `stg` tinyint(4) NOT NULL,
  KEY `rt_aif_code_index01` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_version`
--

DROP TABLE IF EXISTS `schema_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version` (
  `version` varchar(64) DEFAULT NULL,
  `lastModification` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search`
--

DROP TABLE IF EXISTS `search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search` (
  `id` int(11) NOT NULL DEFAULT '0',
  `uid` varchar(140) DEFAULT NULL,
  `number` varchar(140) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(512) DEFAULT NULL,
  `version` varchar(11) DEFAULT NULL,
  `iteration` varchar(11) DEFAULT NULL,
  `doctypeId` bigint(20) DEFAULT NULL,
  `lockById` varchar(11) DEFAULT NULL,
  `locked` varchar(19) DEFAULT NULL,
  `updated` varchar(19) DEFAULT NULL,
  `updateById` varchar(11) DEFAULT NULL,
  `created` varchar(19) DEFAULT NULL,
  `createById` varchar(11) DEFAULT NULL,
  `spacename` varchar(32) DEFAULT NULL,
  `cid` varchar(13) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_update`
--

DROP TABLE IF EXISTS `search_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_update` (
  `updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sequence_rulesier1`
--

DROP TABLE IF EXISTS `sequence_rulesier1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence_rulesier1` (
  `sequence` int(11) NOT NULL,
  `parameter1` varchar(64) NOT NULL,
  `parameter2` varchar(64) NOT NULL,
  `parameter3` varchar(64) NOT NULL,
  UNIQUE KEY `parameter1` (`parameter1`,`parameter2`,`parameter3`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_resource_breakdown`
--

DROP TABLE IF EXISTS `tmp_resource_breakdown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_resource_breakdown` (
  `id` int(11) NOT NULL DEFAULT '0',
  `uid` varchar(140) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `cid` varchar(64) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(140) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_resource_breakdown_b`
--

DROP TABLE IF EXISTS `tmp_resource_breakdown_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_resource_breakdown_b` (
  `id` int(11) NOT NULL DEFAULT '0',
  `uid` varchar(140) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `cid` varchar(64) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(140) DEFAULT NULL,
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmptable`
--

DROP TABLE IF EXISTS `tmptable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmptable` (
  `Aid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmptable2`
--

DROP TABLE IF EXISTS `tmptable2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmptable2` (
  `Aid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_prefs`
--

DROP TABLE IF EXISTS `user_prefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_prefs` (
  `user_id` int(11) NOT NULL,
  `css_sheet` varchar(32) NOT NULL DEFAULT 'default',
  `lang` varchar(32) NOT NULL DEFAULT 'default',
  `long_date_format` varchar(32) NOT NULL DEFAULT 'default',
  `short_date_format` varchar(32) NOT NULL DEFAULT 'default',
  `hour_format` varchar(32) NOT NULL DEFAULT 'default',
  `time_zone` varchar(32) NOT NULL DEFAULT 'default',
  `wildspace_path` varchar(128) NOT NULL DEFAULT 'default',
  `max_record` varchar(128) NOT NULL DEFAULT 'default',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_sessions`
--

DROP TABLE IF EXISTS `user_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_sessions` (
  `user_id` int(11) NOT NULL,
  `datas` longtext,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `view_org_breakdown`
--

DROP TABLE IF EXISTS `view_org_breakdown`;
/*!50001 DROP VIEW IF EXISTS `view_org_breakdown`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_org_breakdown` AS SELECT 
 1 AS `id`,
 1 AS `uid`,
 1 AS `cid`,
 1 AS `dn`,
 1 AS `name`,
 1 AS `parent_id`,
 1 AS `parent_uid`,
 1 AS `spacename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_resource_breakdown`
--

DROP TABLE IF EXISTS `view_resource_breakdown`;
/*!50001 DROP VIEW IF EXISTS `view_resource_breakdown`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_resource_breakdown` AS SELECT 
 1 AS `id`,
 1 AS `uid`,
 1 AS `name`,
 1 AS `cid`,
 1 AS `parent_id`,
 1 AS `parent_uid`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_workitem_doctype_rel`
--

DROP TABLE IF EXISTS `view_workitem_doctype_rel`;
/*!50001 DROP VIEW IF EXISTS `view_workitem_doctype_rel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_workitem_doctype_rel` AS SELECT 
 1 AS `parent_id`,
 1 AS `parent_uid`,
 1 AS `child_id`,
 1 AS `child_uid`,
 1 AS `rdn`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `wf_activity`
--

DROP TABLE IF EXISTS `wf_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_activity` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) DEFAULT NULL,
  `cid` varchar(32) NOT NULL DEFAULT '56acc299ed0e4',
  `name` varchar(80) DEFAULT NULL,
  `normalizedName` varchar(255) DEFAULT NULL,
  `processId` int(11) DEFAULT NULL,
  `type` enum('start','end','split','switch','join','activity','standalone') DEFAULT NULL,
  `isAutorouted` tinyint(1) DEFAULT '0',
  `flowNum` int(10) DEFAULT NULL,
  `isInteractive` tinyint(1) DEFAULT '0',
  `lastModif` int(14) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `expirationTime` datetime DEFAULT NULL,
  `isAutomatic` tinyint(1) DEFAULT '0',
  `isComment` tinyint(1) DEFAULT '0',
  `ownerId` varchar(255) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `progression` float DEFAULT NULL,
  `roles` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wf_activity_u_uid` (`uid`),
  KEY `name` (`name`),
  KEY `roles` (`roles`(255)),
  KEY `ownerId` (`ownerId`),
  KEY `parentId` (`parentId`),
  KEY `parentUid` (`parentUid`),
  KEY `FK_wf_activity_1` (`processId`),
  CONSTRAINT `FK_wf_activity_1` FOREIGN KEY (`processId`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wf_activity_wf_process1` FOREIGN KEY (`processId`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_document_link`
--

DROP TABLE IF EXISTS `wf_document_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_document_link` (
  `uid` varchar(64) NOT NULL,
  `parentId` int(11) NOT NULL,
  `childId` int(11) NOT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  `childUid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lindex` int(11) DEFAULT '0',
  `attributes` mediumtext,
  `spacename` varchar(64) NOT NULL,
  PRIMARY KEY (`parentId`,`childId`),
  UNIQUE KEY `uid` (`uid`),
  KEY `DOCPROCESS_UID` (`uid`),
  KEY `DOCPROCESS_NAME` (`name`),
  KEY `DOCPROCESS_LINDEX` (`lindex`),
  KEY `DOCPROCESS_PARENTUID` (`parentUid`),
  KEY `DOCPROCESS_CHILDUID` (`childUid`),
  KEY `DOCPROCESS_PARENTID` (`parentId`),
  KEY `DOCPROCESS_CHILDID` (`childId`),
  CONSTRAINT `FK_wf_document_link_1` FOREIGN KEY (`childId`) REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wf_document_link_wf_instance1` FOREIGN KEY (`childId`) REFERENCES `wf_instance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_instance`
--

DROP TABLE IF EXISTS `wf_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_instance` (
  `id` int(11) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT '56acc299ed1bd',
  `updated` datetime DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `processId` int(11) DEFAULT NULL,
  `started` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT 'No Name',
  `title` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `nextActivities` mediumtext,
  `nextUsers` mediumtext,
  `ended` datetime DEFAULT NULL,
  `status` varchar(64) DEFAULT NULL,
  `attributes` mediumtext,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wf_instance_u_uid` (`uid`),
  KEY `WFINST_status` (`status`),
  KEY `WFINST_processId` (`processId`),
  KEY `WFINST_uid` (`uid`),
  KEY `WFINST_cid` (`cid`),
  KEY `WFINST_parentId` (`parentId`),
  KEY `WFINST_parentUid` (`parentId`),
  CONSTRAINT `FK_wf_instance_1` FOREIGN KEY (`processId`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wf_instance_wf_process1` FOREIGN KEY (`processId`) REFERENCES `wf_process` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_instance_activity`
--

DROP TABLE IF EXISTS `wf_instance_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_instance_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT '56acc299ed22a',
  `name` varchar(128) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `instanceId` int(14) NOT NULL,
  `activityId` int(14) NOT NULL,
  `ownerId` varchar(64) DEFAULT NULL,
  `status` enum('running','completed') DEFAULT NULL,
  `type` enum('start','end','split','switch','join','activity','standalone') DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updateById` varchar(64) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(128) DEFAULT NULL,
  `started` datetime DEFAULT NULL,
  `ended` datetime DEFAULT NULL,
  `attributes` mediumtext,
  `comment` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `WFINSTACTIVITY_uid` (`uid`),
  KEY `WFINSTACTIVITY_name` (`name`),
  KEY `WFINSTACTIVITY_parentuid` (`parentUid`),
  KEY `WFINSTACTIVITY_parentid` (`parentId`),
  KEY `WFINSTACTIVITY_cid` (`cid`),
  KEY `WFINSTACTIVITY_ownerId` (`ownerId`),
  KEY `WFINSTACTIVITY_activityId` (`activityId`),
  KEY `WFINSTACTIVITY_status` (`status`),
  KEY `WFINSTACTIVITY_type` (`type`),
  KEY `WFINSTACTIVITY_started` (`started`),
  KEY `WFINSTACTIVITY_ended` (`ended`),
  CONSTRAINT `FK_wf_instance_activity_1` FOREIGN KEY (`activityId`) REFERENCES `wf_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=506943 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_instance_seq`
--

DROP TABLE IF EXISTS `wf_instance_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_instance_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=506943 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_process`
--

DROP TABLE IF EXISTS `wf_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_process` (
  `id` int(11) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL DEFAULT '56acc299ecd6d',
  `name` varchar(80) DEFAULT NULL,
  `isValid` tinyint(1) DEFAULT '0',
  `isActive` tinyint(1) DEFAULT '0',
  `version` varchar(12) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updateById` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `normalizedName` varchar(256) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wf_process_u_uid` (`uid`),
  UNIQUE KEY `wf_process_u_normalizedName` (`normalizedName`),
  KEY `wf_process_index_uid` (`uid`),
  KEY `wf_process_index_cid` (`cid`),
  KEY `wf_process_index_name` (`name`),
  KEY `wf_process_index_parentuid` (`parentUid`),
  KEY `wf_process_index_parentid` (`parentId`),
  KEY `wf_process_index_ownerId` (`ownerId`),
  KEY `wf_process_index_isValid` (`isValid`),
  KEY `wf_process_index_isActive` (`isActive`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_seq`
--

DROP TABLE IF EXISTS `wf_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wf_transition`
--

DROP TABLE IF EXISTS `wf_transition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wf_transition` (
  `uid` varchar(32) NOT NULL,
  `processId` int(14) NOT NULL,
  `parentId` int(14) NOT NULL,
  `parentUid` varchar(32) NOT NULL,
  `childId` int(14) NOT NULL,
  `childUid` varchar(32) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `lindex` int(7) DEFAULT '0',
  `attributes` mediumtext,
  `cid` varchar(32) NOT NULL DEFAULT '56acc299ed150',
  PRIMARY KEY (`parentId`,`childId`),
  UNIQUE KEY `wf_transition_u_uid` (`uid`),
  KEY `processId` (`processId`),
  KEY `parentId` (`parentId`),
  KEY `parentUid` (`parentUid`),
  KEY `childId` (`childId`),
  KEY `childUid` (`childUid`),
  KEY `lindex` (`lindex`),
  CONSTRAINT `FK_wf_transition_1` FOREIGN KEY (`parentId`) REFERENCES `wf_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_wf_transition_2` FOREIGN KEY (`childId`) REFERENCES `wf_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_alias`
--

DROP TABLE IF EXISTS `workitem_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `container_id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT 'workitem',
  `uid` varchar(128) NOT NULL DEFAULT 'workitem',
  `designation` varchar(128) DEFAULT NULL,
  `object_class` varchar(13) NOT NULL DEFAULT 'workitemAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_workitem_alias` (`alias_id`,`container_id`),
  KEY `K_workitem_alias_1` (`container_id`),
  KEY `K_workitem_alias_2` (`number`),
  KEY `INDEX_uid` (`uid`),
  KEY `INDEX_name` (`name`),
  CONSTRAINT `FK_workitem_alias_1` FOREIGN KEY (`container_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_categories`
--

DROP TABLE IF EXISTS `workitem_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_categories` (
  `id` int(11) NOT NULL,
  `number` varchar(128) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `icon` varchar(64) DEFAULT NULL,
  `uid` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_category_number` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_category_rel`
--

DROP TABLE IF EXISTS `workitem_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_category_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_category_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_category_rel_1` (`parent_id`),
  KEY `K_project_category_rel_2` (`parent_uid`),
  KEY `K_project_category_rel_3` (`parent_cid`),
  KEY `K_project_category_rel_4` (`child_id`),
  KEY `K_project_category_rel_5` (`child_uid`),
  KEY `K_project_category_rel_6` (`child_cid`),
  KEY `K_project_category_rel_7` (`rdn`),
  CONSTRAINT `FK_workitem_category_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_category_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_category_rel_3` FOREIGN KEY (`child_id`) REFERENCES `workitem_categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onWorkitemCategoryRelInsert BEFORE INSERT ON workitem_category_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `workitem_cont_metadata_seq`
--

DROP TABLE IF EXISTS `workitem_cont_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_files`
--

DROP TABLE IF EXISTS `workitem_doc_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(16) NOT NULL DEFAULT '569e92b86d248',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_uid` varchar(64) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `file_used_name` varchar(128) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_file_name` (`name`,`path`),
  UNIQUE KEY `INDEX_workitem_doc_files_uid` (`uid`),
  KEY `FK_workitem_doc_files_1` (`document_id`),
  KEY `IK_workitem_doc_files_1` (`document_id`),
  KEY `IK_workitem_doc_files_2` (`name`),
  KEY `IK_workitem_doc_files_3` (`path`(128)),
  KEY `IK_workitem_doc_files_4` (`iteration`,`document_id`),
  KEY `IK_workitem_doc_files_5` (`mainrole`),
  KEY `IK_workitem_doc_files_6` (`roles`(32)),
  KEY `IK_workitem_doc_files_8` (`document_uid`),
  KEY `FK_workitem_docfiles_7` (`create_by_uid`),
  KEY `FK_workitem_docfiles_8` (`lock_by_uid`),
  KEY `FK_workitem_docfiles_9` (`update_by_uid`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `FK_workitem_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `workitem_documents` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onWIDocfileDelete AFTER DELETE ON workitem_doc_files FOR EACH ROW 
BEGIN
	DELETE FROM checkout_index WHERE file_id=OLD.id AND spacename='workitem';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `workitem_doc_files_seq`
--

DROP TABLE IF EXISTS `workitem_doc_files_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=230233 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_files_versions`
--

DROP TABLE IF EXISTS `workitem_doc_files_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files_versions` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `of_file_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `path` varchar(256) NOT NULL,
  `mainrole` int(2) NOT NULL DEFAULT '1',
  `roles` varchar(512) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `root_name` varchar(128) DEFAULT NULL,
  `extension` varchar(16) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mtime` datetime DEFAULT NULL,
  `md5` varchar(128) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_filename_filepath` (`name`,`path`(128)),
  KEY `IK_workitem_doc_files_version_1` (`document_id`),
  KEY `IK_workitem_doc_files_version_2` (`name`),
  KEY `IK_workitem_doc_files_version_3` (`path`(128)),
  KEY `IK_workitem_doc_files_version_4` (`iteration`,`document_id`),
  KEY `IK_workitem_doc_files_version_5` (`mainrole`),
  KEY `IK_workitem_doc_files_version_6` (`roles`(32)),
  KEY `IK_workitem_doc_files_version_7` (`of_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_files_versions_seq`
--

DROP TABLE IF EXISTS `workitem_doc_files_versions_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=240956 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_rel`
--

DROP TABLE IF EXISTS `workitem_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_rel` (
  `link_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `hash` char(32) DEFAULT NULL,
  `data` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `uniq_docid_ldoc_id` (`parent_id`,`child_id`),
  KEY `FK_workitem_doc_rel_2` (`child_id`),
  CONSTRAINT `FK_workitem_doc_rel_1` FOREIGN KEY (`parent_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_doc_rel_2` FOREIGN KEY (`child_id`) REFERENCES `workitem_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doc_rel_seq`
--

DROP TABLE IF EXISTS `workitem_doc_rel_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14533 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doccomments`
--

DROP TABLE IF EXISTS `workitem_doccomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doccomments` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doccomments_seq`
--

DROP TABLE IF EXISTS `workitem_doccomments_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2286 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_doctype_rel`
--

DROP TABLE IF EXISTS `workitem_doctype_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_doctype_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_doctype_rel_1` (`parent_id`,`parent_cid`,`child_id`),
  UNIQUE KEY `U_workitem_doctype_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_doctype_rel_1` (`parent_id`),
  KEY `K_project_doctype_rel_2` (`parent_uid`),
  KEY `K_project_doctype_rel_3` (`parent_cid`),
  KEY `K_project_doctype_rel_4` (`child_id`),
  KEY `K_project_doctype_rel_5` (`child_uid`),
  KEY `K_project_doctype_rel_6` (`child_cid`),
  KEY `K_project_doctype_rel_7` (`rdn`),
  KEY `K_workitem_doctype_rel_1` (`parent_id`),
  KEY `K_workitem_doctype_rel_2` (`parent_uid`),
  KEY `K_workitem_doctype_rel_3` (`parent_cid`),
  KEY `K_workitem_doctype_rel_4` (`child_id`),
  KEY `K_workitem_doctype_rel_5` (`child_uid`),
  KEY `K_workitem_doctype_rel_6` (`child_cid`),
  KEY `K_workitem_doctype_rel_7` (`rdn`),
  CONSTRAINT `FK_workitem_doctype_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_doctype_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_doctype_rel_3` FOREIGN KEY (`child_id`) REFERENCES `doctypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onWorkitemDoctypeRelInsert BEFORE INSERT ON workitem_doctype_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `workitem_documents`
--

DROP TABLE IF EXISTS `workitem_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_documents` (
  `id` int(11) NOT NULL,
  `uid` varchar(140) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e92709feb6',
  `number` varchar(128) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `container_id` int(11) NOT NULL,
  `container_uid` varchar(128) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `spacename` varchar(30) NOT NULL DEFAULT 'workitem',
  `doctype_id` int(11) NOT NULL DEFAULT '0',
  `default_process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lock_by_id` int(11) DEFAULT NULL,
  `lock_by_uid` varchar(64) DEFAULT NULL,
  `locked` datetime DEFAULT NULL,
  `from_document_id` int(11) DEFAULT NULL,
  `as_template` tinyint(1) DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  `update_by_id` int(11) DEFAULT NULL,
  `update_by_uid` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `ci` varchar(255) DEFAULT NULL,
  `type_gc` varchar(255) DEFAULT NULL,
  `work_unit` varchar(255) DEFAULT NULL,
  `father_ds` varchar(255) DEFAULT NULL,
  `need_calcul` varchar(255) DEFAULT NULL,
  `ata` varchar(255) DEFAULT NULL,
  `receptionneur` varchar(32) DEFAULT NULL,
  `indice_client` varchar(255) DEFAULT NULL,
  `recept_date` int(11) DEFAULT NULL,
  `observation` varchar(255) DEFAULT NULL,
  `date_de_remise` int(11) DEFAULT NULL,
  `sub_ata` varchar(255) DEFAULT NULL,
  `work_package` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `fab_process` varchar(255) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `materiau_mak` varchar(255) DEFAULT NULL,
  `masse_mak` double DEFAULT NULL,
  `rt_name` varchar(255) DEFAULT NULL,
  `rt_type` varchar(255) DEFAULT NULL,
  `rt_reason` varchar(255) DEFAULT NULL,
  `rt_emitted` int(11) DEFAULT NULL,
  `rt_apply_to` int(11) DEFAULT NULL,
  `modification` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_workitem_documents_1` (`number`,`version`),
  UNIQUE KEY `INDEX_workitem_documents_uid` (`uid`),
  KEY `FK_workitem_documents_2` (`doctype_id`),
  KEY `FK_workitem_documents_3` (`version`),
  KEY `FK_workitem_documents_4` (`container_id`),
  KEY `FK_workitem_documents_5` (`category_id`),
  KEY `document_name` (`name`),
  KEY `INDEX_workitem_documents_spacename` (`spacename`),
  KEY `INDEX_workitem_documents_name` (`name`),
  KEY `INDEX_workitem_documents_cuid` (`container_uid`),
  KEY `FK_workitem_documents_7` (`create_by_uid`),
  KEY `FK_workitem_documents_8` (`lock_by_uid`),
  KEY `FK_workitem_documents_9` (`update_by_uid`),
  KEY `FK_workitem_documents_10` (`as_template`),
  FULLTEXT KEY `number` (`number`,`name`,`designation`),
  CONSTRAINT `FK_workitem_documents_1` FOREIGN KEY (`container_id`) REFERENCES `workitems` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onWIDocumentDelete AFTER DELETE ON workitem_documents FOR EACH ROW 
BEGIN
	DELETE FROM docbasket WHERE document_id=OLD.id AND space_id=10;
	DELETE FROM postit WHERE parent_uid=OLD.uid;
	DELETE FROM discussion_comment WHERE parent_uid=OLD.uid;
	DELETE FROM notifications WHERE reference_uid=OLD.uid;
	UPDATE pdm_product_version SET document_id=NULL, document_uid=NULL WHERE document_uid=OLD.uid;
	DELETE FROM wf_instance WHERE id IN (SELECT id FROM wf_document_link WHERE parentUid=OLD.uid);
	DELETE FROM wf_document_link WHERE parentUid=OLD.uid;
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `workitem_documents_history`
--

DROP TABLE IF EXISTS `workitem_documents_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) NOT NULL DEFAULT '',
  `action_by` varchar(64) NOT NULL DEFAULT '',
  `action_started` datetime DEFAULT NULL,
  `action_comment` mediumtext,
  `document_id` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `number` varchar(128) DEFAULT NULL,
  `life_stage` varchar(32) DEFAULT NULL,
  `acode` int(11) DEFAULT NULL,
  `iteration` int(11) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `comment` mediumtext,
  `data` mediumtext,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_documents_history_seq`
--

DROP TABLE IF EXISTS `workitem_documents_history_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1100397 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_documents_seq`
--

DROP TABLE IF EXISTS `workitem_documents_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=135963 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_import_history`
--

DROP TABLE IF EXISTS `workitem_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `imported` datetime DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` mediumtext,
  `import_logfiles_file` mediumtext,
  `package_file_path` mediumtext,
  `package_file_extension` varchar(32) DEFAULT NULL,
  `package_mtime` datetime DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_metadata`
--

DROP TABLE IF EXISTS `workitem_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_metadata` (
  `id` int(11) NOT NULL,
  `uid` varchar(64) DEFAULT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `extendedCid` varchar(32) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `appname` varchar(128) DEFAULT NULL,
  `getter` varchar(128) DEFAULT NULL,
  `setter` varchar(128) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` mediumtext,
  `dbquery` mediumtext,
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_uid` (`uid`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_metadata_seq`
--

DROP TABLE IF EXISTS `workitem_metadata_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workitem_property_rel`
--

DROP TABLE IF EXISTS `workitem_property_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitem_property_rel` (
  `parent_id` int(11) NOT NULL,
  `parent_uid` varchar(64) NOT NULL,
  `parent_cid` varchar(64) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `child_uid` varchar(64) DEFAULT NULL,
  `child_cid` varchar(64) DEFAULT NULL,
  `rdn` varchar(128) DEFAULT NULL,
  UNIQUE KEY `U_project_property_rel_1` (`parent_uid`,`child_uid`),
  UNIQUE KEY `U_workitem_property_rel_1` (`parent_uid`,`child_uid`),
  KEY `K_project_property_rel_1` (`parent_id`),
  KEY `K_project_property_rel_2` (`parent_uid`),
  KEY `K_project_property_rel_3` (`parent_cid`),
  KEY `K_project_property_rel_4` (`child_id`),
  KEY `K_project_property_rel_5` (`child_uid`),
  KEY `K_project_property_rel_6` (`child_cid`),
  KEY `K_project_property_rel_7` (`rdn`),
  KEY `K_workitem_property_rel_1` (`parent_id`),
  KEY `K_workitem_property_rel_2` (`parent_uid`),
  KEY `K_workitem_property_rel_3` (`parent_cid`),
  KEY `K_workitem_property_rel_4` (`child_id`),
  KEY `K_workitem_property_rel_5` (`child_uid`),
  KEY `K_workitem_property_rel_6` (`child_cid`),
  KEY `K_workitem_property_rel_7` (`rdn`),
  CONSTRAINT `FK_workitem_property_rel_1` FOREIGN KEY (`rdn`) REFERENCES `workitems` (`dn`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_property_rel_2` FOREIGN KEY (`parent_id`) REFERENCES `workitems` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_workitem_property_rel_3` FOREIGN KEY (`child_id`) REFERENCES `workitem_metadata` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onWorkitemPropertyRelInsert BEFORE INSERT ON workitem_property_rel FOR EACH ROW
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM workitems WHERE id=NEW.parent_id AND cid = NEW.parent_cid;
	SET NEW.rdn = parentDn;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `workitems`
--

DROP TABLE IF EXISTS `workitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workitems` (
  `id` int(11) NOT NULL,
  `number` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `cid` varchar(64) DEFAULT '569e94192201a',
  `dn` varchar(128) DEFAULT NULL,
  `life_stage` varchar(16) DEFAULT 'init',
  `designation` varchar(128) DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `acode` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_uid` varchar(64) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` mediumtext NOT NULL,
  `created` datetime DEFAULT NULL,
  `create_by_id` int(11) DEFAULT NULL,
  `create_by_uid` varchar(64) DEFAULT NULL,
  `planned_closure` datetime DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `close_by_id` int(11) DEFAULT NULL,
  `close_by_uid` varchar(64) DEFAULT NULL,
  `spacename` varchar(16) NOT NULL DEFAULT 'workitem',
  `affaires_liees` varchar(255) DEFAULT NULL,
  `interloc_client` varchar(255) DEFAULT NULL,
  `interloc_sier` varchar(255) DEFAULT NULL,
  `client` varchar(255) DEFAULT NULL,
  `numero_client` varchar(255) DEFAULT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(8) NOT NULL DEFAULT 'workitem',
  `chef_group` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_workitems_number` (`number`),
  UNIQUE KEY `UC_workitems_uid` (`uid`),
  KEY `K_workitems_3` (`create_by_uid`),
  KEY `K_workitems_4` (`close_by_uid`),
  KEY `K_workitems_1` (`version`),
  KEY `K_workitems_2` (`parent_id`),
  KEY `FK_workitems_2` (`default_process_id`),
  KEY `K_workitems_name` (`name`),
  KEY `K_workitems_dn` (`dn`),
  FULLTEXT KEY `FT_workitems_1` (`number`,`name`,`designation`),
  CONSTRAINT `FK_workitems_1` FOREIGN KEY (`parent_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_workitems_2` FOREIGN KEY (`default_process_id`) REFERENCES `wf_process` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onWorkitemInsert BEFORE INSERT ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM projects WHERE id=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onWorkitemUpdate BEFORE UPDATE ON workitems FOR EACH ROW 
BEGIN
	DECLARE parentDn varchar(128);
	SELECT dn INTO parentDn FROM projects WHERE id=NEW.parent_id;
	SET NEW.dn=CONCAT(parentDn,NEW.uid,'/');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER onWorkitemDelete AFTER DELETE ON workitems FOR EACH ROW 
BEGIN
	DELETE FROM project_container_rel WHERE child_id=OLD.id AND spacename='workitem';
	DELETE FROM checkout_index WHERE container_id=OLD.id AND spacename='workitem';
	DELETE FROM container_favorite WHERE container_id=OLD.id AND spacename='workitem';
	DELETE FROM acl_resource WHERE referToUid=OLD.uid;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `objects`
--

/*!50001 DROP VIEW IF EXISTS `objects`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `objects` AS select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`number` AS `number`,`obj`.`name` AS `name`,`obj`.`designation` AS `designation`,`obj`.`version` AS `version`,`obj`.`iteration` AS `iteration`,`obj`.`doctype_id` AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'workitem' AS `spacename`,'569e92709feb6' AS `cid` from `workitem_documents` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`number` AS `number`,`obj`.`name` AS `name`,`obj`.`designation` AS `designation`,`obj`.`version` AS `version`,`obj`.`iteration` AS `iteration`,`obj`.`doctype_id` AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'bookshop' AS `spacename`,'569e92709feb6' AS `cid` from `bookshop_documents` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`number` AS `number`,`obj`.`name` AS `name`,`obj`.`designation` AS `designation`,`obj`.`version` AS `version`,`obj`.`iteration` AS `iteration`,`obj`.`doctype_id` AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'cadlib' AS `spacename`,'569e92709feb6' AS `cid` from `cadlib_documents` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`number` AS `number`,`obj`.`name` AS `name`,`obj`.`designation` AS `designation`,`obj`.`version` AS `version`,`obj`.`iteration` AS `iteration`,`obj`.`doctype_id` AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'mockup' AS `spacename`,'569e92709feb6' AS `cid` from `mockup_documents` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`uid` AS `number`,`obj`.`name` AS `name`,'' AS `designation`,'' AS `version`,`obj`.`iteration` AS `iteration`,-(1) AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'workitem' AS `spacename`,'569e92b86d248' AS `cid` from `workitem_doc_files` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`uid` AS `number`,`obj`.`name` AS `name`,'' AS `designation`,'' AS `version`,`obj`.`iteration` AS `iteration`,-(1) AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'bookshop' AS `spacename`,'569e92b86d248' AS `cid` from `bookshop_doc_files` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`uid` AS `number`,`obj`.`name` AS `name`,'' AS `designation`,'' AS `version`,`obj`.`iteration` AS `iteration`,-(1) AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'cadlib' AS `spacename`,'569e92b86d248' AS `cid` from `cadlib_doc_files` `obj` union select `obj`.`id` AS `id`,`obj`.`uid` AS `uid`,`obj`.`uid` AS `number`,`obj`.`name` AS `name`,'' AS `designation`,'' AS `version`,`obj`.`iteration` AS `iteration`,-(1) AS `doctypeId`,`obj`.`lock_by_id` AS `lockById`,`obj`.`locked` AS `locked`,`obj`.`updated` AS `updated`,`obj`.`update_by_id` AS `updateById`,`obj`.`created` AS `created`,`obj`.`create_by_id` AS `createById`,'mockup' AS `spacename`,'569e92b86d248' AS `cid` from `mockup_doc_files` `obj` union select `workitems`.`id` AS `id`,`workitems`.`uid` AS `uid`,`workitems`.`number` AS `number`,`workitems`.`name` AS `name`,`workitems`.`designation` AS `designation`,'' AS `version`,'' AS `iteration`,-(10) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,`workitems`.`created` AS `created`,`workitems`.`create_by_id` AS `createById`,'workitem' AS `spacename`,'569e94192201a' AS `cid` from `workitems` union select `bookshops`.`id` AS `id`,`bookshops`.`uid` AS `uid`,`bookshops`.`number` AS `number`,`bookshops`.`name` AS `name`,`bookshops`.`designation` AS `designation`,'' AS `version`,'' AS `iteration`,-(10) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,`bookshops`.`created` AS `created`,`bookshops`.`create_by_id` AS `createById`,'bookshop' AS `spacename`,'569e94192201a' AS `cid` from `bookshops` union select `cadlibs`.`id` AS `id`,`cadlibs`.`uid` AS `uid`,`cadlibs`.`number` AS `number`,`cadlibs`.`name` AS `name`,`cadlibs`.`designation` AS `designation`,'' AS `version`,'' AS `iteration`,-(10) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,`cadlibs`.`created` AS `created`,`cadlibs`.`create_by_id` AS `createById`,'cadlib' AS `spacename`,'569e94192201a' AS `cid` from `cadlibs` union select `mockups`.`id` AS `id`,`mockups`.`uid` AS `uid`,`mockups`.`number` AS `number`,`mockups`.`name` AS `name`,`mockups`.`designation` AS `designation`,'' AS `version`,'' AS `iteration`,-(10) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,`mockups`.`created` AS `created`,`mockups`.`create_by_id` AS `createById`,'mockup' AS `spacename`,'569e94192201a' AS `cid` from `mockups` union select `projects`.`id` AS `id`,`projects`.`uid` AS `uid`,`projects`.`number` AS `number`,`projects`.`name` AS `name`,`projects`.`designation` AS `designation`,'' AS `version`,'' AS `iteration`,-(15) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,`projects`.`created` AS `created`,`projects`.`create_by_id` AS `createById`,'default' AS `spacename`,'569e93c6ee156' AS `cid` from `projects` union select `pdm_product_version`.`id` AS `id`,`pdm_product_version`.`uid` AS `uid`,`pdm_product_version`.`number` AS `number`,`pdm_product_version`.`name` AS `name`,`pdm_product_version`.`description` AS `designation`,`pdm_product_version`.`version` AS `version`,'' AS `iteration`,-(20) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,'' AS `created`,'' AS `createById`,`pdm_product_version`.`spacename` AS `spacename`,'569e972dd4c2c' AS `cid` from `pdm_product_version` union select `pdm_product_instance`.`id` AS `id`,`pdm_product_instance`.`uid` AS `uid`,`pdm_product_instance`.`number` AS `number`,`pdm_product_instance`.`name` AS `name`,`pdm_product_instance`.`description` AS `designation`,'' AS `version`,'' AS `iteration`,-(21) AS `doctypeId`,'' AS `lockById`,'' AS `locked`,'' AS `updated`,'' AS `updateById`,'' AS `created`,'' AS `createById`,'default' AS `spacename`,'569e972dd4c2c' AS `cid` from `pdm_product_instance` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_org_breakdown`
--

/*!50001 DROP VIEW IF EXISTS `view_org_breakdown`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `view_org_breakdown` AS select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'default' AS `spacename` from `projects` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'workitem' AS `spacename` from `workitems` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'cadlib' AS `spacename` from `cadlibs` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'bookshop' AS `spacename` from `bookshops` `org` union select `org`.`id` AS `id`,`org`.`uid` AS `uid`,`org`.`cid` AS `cid`,`org`.`dn` AS `dn`,`org`.`name` AS `name`,`org`.`parent_id` AS `parent_id`,`org`.`parent_uid` AS `parent_uid`,'mockup' AS `spacename` from `mockups` `org` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_resource_breakdown`
--

/*!50001 DROP VIEW IF EXISTS `view_resource_breakdown`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_resource_breakdown` AS (select `doc`.`id` AS `id`,`doc`.`uid` AS `uid`,`doc`.`name` AS `name`,`doc`.`cid` AS `cid`,`cont`.`id` AS `parent_id`,`cont`.`uid` AS `parent_uid` from (`workitem_documents` `doc` join `workitems` `cont` on((`doc`.`container_id` = `cont`.`id`)))) union (select `cont`.`id` AS `id`,`cont`.`uid` AS `uid`,`cont`.`name` AS `name`,`cont`.`cid` AS `cid`,`proj`.`id` AS `parent_id`,`proj`.`uid` AS `parent_uid` from (`workitems` `cont` join `projects` `proj` on((`cont`.`parent_id` = `proj`.`id`)))) union (select `projects`.`id` AS `id`,`projects`.`uid` AS `uid`,`projects`.`name` AS `name`,`projects`.`cid` AS `cid`,`projects`.`parent_id` AS `parent_id`,`projects`.`parent_uid` AS `parent_uid` from `projects`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_workitem_doctype_rel`
--

/*!50001 DROP VIEW IF EXISTS `view_workitem_doctype_rel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `view_workitem_doctype_rel` AS select `workitem_doctype_rel`.`parent_id` AS `parent_id`,`workitem_doctype_rel`.`parent_uid` AS `parent_uid`,`workitem_doctype_rel`.`child_id` AS `child_id`,`workitem_doctype_rel`.`child_uid` AS `child_uid`,`workitem_doctype_rel`.`rdn` AS `rdn` from `workitem_doctype_rel` union select `project_doctype_rel`.`parent_id` AS `parent_id`,`project_doctype_rel`.`parent_uid` AS `parent_uid`,`project_doctype_rel`.`child_id` AS `child_id`,`project_doctype_rel`.`child_uid` AS `child_uid`,`project_doctype_rel`.`rdn` AS `rdn` from `project_doctype_rel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-23 20:13:26
