DROP TABLE `checkout_index`;
CREATE TABLE `checkout_index` (
	`file_name` varchar(128) NOT NULL,
	`lock_by_id` int(11) NULL,
	`lock_by_uid` varchar(64) NULL,
	`file_id` int(11) NULL,
	`file_uid` VARCHAR(140) NULL,
	`designation` varchar(128) NULL,
	`spacename` varchar(128) NULL,
	`container_id` int(11) NULL,
	`container_number` varchar(128) NULL,
	`document_id` int(11) NULL,
	PRIMARY KEY (`file_name`),
	KEY `IK_checkout_index_1` (`lock_by_id`),
	KEY `IK_checkout_index_2` (`lock_by_uid`),
	KEY `IK_checkout_index_3` (`file_id`),
	KEY `IK_checkout_index_4` (`file_uid`),
	KEY `IK_checkout_index_5` (`spacename`),
	KEY `IK_checkout_index_6` (`container_id`),
	KEY `IK_checkout_index_7` (`container_number`),
	KEY `IK_checkout_index_8` (`document_id`)
) ENGINE=InnoDB;

DROP PROCEDURE IF EXISTS `updateCheckoutIndexFromUserId`;
DELIMITER $$
CREATE PROCEDURE `updateCheckoutIndexFromUserId`(_userId INT(11))
BEGIN
	DECLARE userId INT(11);
	SET @userId=_userId;
	DELETE FROM `checkout_index` WHERE `lock_by_id`=@userId;
	INSERT INTO checkout_index(
		SELECT * FROM(
			SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
			FROM workitem_doc_files as file
			JOIN workitem_documents as doc ON file.document_id=doc.id
			where file.acode=1 and file.lock_by_id=@userId
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM bookshop_doc_files as file
				JOIN bookshop_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM cadlib_doc_files as file
				JOIN cadlib_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM mockup_doc_files as file
				JOIN mockup_documents as doc ON file.document_id=doc.id
				where file.acode=1 and file.lock_by_id=@userId
			)
		) as checkout_index
	);
END$$
DELIMITER ;

-- CALL updateCheckoutIndexFromUserId(13);
-- SELECT * FROM `checkout_index`;

DROP PROCEDURE IF EXISTS `updateCheckoutIndex`;
DELIMITER $$
CREATE PROCEDURE `updateCheckoutIndex`()
BEGIN
	DELETE FROM `checkout_index`;
	INSERT INTO checkout_index(
		SELECT * FROM(
			SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
			FROM workitem_doc_files as file
			JOIN workitem_documents as doc ON file.document_id=doc.id
			where file.acode=1
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM bookshop_doc_files as file
				JOIN bookshop_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM cadlib_doc_files as file
				JOIN cadlib_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
			UNION(
				SELECT file.name as file_name, file.lock_by_id, file.lock_by_uid, file.id as file_id, file.uid as file_uid, doc.designation, doc.spacename, doc.container_id, doc.container_uid as container_number, doc.id as document_id
				FROM mockup_doc_files as file
				JOIN mockup_documents as doc ON file.document_id=doc.id
				where file.acode=1
			)
		) as checkout_index
	);
END$$
DELIMITER ;

-- call updateCheckoutIndex();
-- select * from checkout_index;

-- ON DOCFILE UPDATE --
DROP TRIGGER IF EXISTS onWIDocfileUpdate; 
DELIMITER $$
CREATE TRIGGER onWIDocfileUpdate AFTER UPDATE ON workitem_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `workitem_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'workitem',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END$$
DELIMITER ;

-- ON DOCFILE UPDATE --
DROP TRIGGER IF EXISTS onBookshopDocfileUpdate; 
DELIMITER $$
CREATE TRIGGER onBookshopDocfileUpdate AFTER UPDATE ON bookshop_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `bookshop_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'bookshop',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END$$
DELIMITER ;


-- ON DOCFILE UPDATE --
DROP TRIGGER IF EXISTS onCadlibDocfileUpdate; 
DELIMITER $$
CREATE TRIGGER onCadlibDocfileUpdate AFTER UPDATE ON cadlib_doc_files 
FOR EACH ROW 
BEGIN
	DECLARE containerId INT(11);
	DECLARE containerNumber VARCHAR(256);
	DECLARE documentId INT(11);
	DECLARE designation VARCHAR(256);
    
	IF(NEW.acode <> OLD.acode) THEN
		DELETE FROM `checkout_index` WHERE `file_name`=OLD.name;
		IF NEW.acode = 1 THEN
			SELECT 
				`container_id`,`container_uid`,`id`,`designation` 
                INTO 
                containerId, containerNumber, documentId, designation
				FROM `cadlib_documents`
				WHERE `id`=NEW.document_id;
			
			INSERT INTO `checkout_index`
			(
			`file_name`,
			`lock_by_id`,
			`lock_by_uid`,
			`file_id`,
			`file_uid`,
			`designation`,
			`spacename`,
			`container_id`,
			`container_number`,
			`document_id`
			)
			VALUES
            (
				NEW.name,
				NEW.lock_by_id,
				NEW.lock_by_uid,
				NEW.id,
				NEW.uid,
				`designation`,
				'cadlib',
				`containerId`,
				`containerNumber`,
				`documentId`
			);        
		END IF;
	END IF;
END$$
DELIMITER ;



INSERT INTO schema_version(version,lastModification) VALUES('1.0.5.2',now());
