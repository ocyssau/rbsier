ALTER TABLE `acl_user` 
DROP INDEX `login_idx` ,
ADD UNIQUE INDEX `login_idx` (`login` ASC);

-- select * from schema_version;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.5.4',now());
