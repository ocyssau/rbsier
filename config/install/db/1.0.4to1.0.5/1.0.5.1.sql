CREATE TABLE `acl_resource_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=10;
INSERT INTO acl_resource_seq (id) SELECT id FROM acl_seq;

-- ADD CASCADE ON FK
ALTER TABLE `acl_user_role` 
	DROP FOREIGN KEY `FK_acl_user_role_acl_user1`,
	DROP FOREIGN KEY `FK_acl_user_role_acl_role1`,
	DROP FOREIGN KEY `FK_acl_user_role_acl_resource1`;

ALTER TABLE `acl_user_role` 
ADD CONSTRAINT `FK_acl_user_role_acl_user1`
  FOREIGN KEY (`userId`)
  REFERENCES `acl_user` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_acl_user_role_acl_role1`
  FOREIGN KEY (`roleId`)
  REFERENCES `acl_role` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `FK_acl_user_role_acl_resource1`
  FOREIGN KEY (`resourceCn`)
  REFERENCES `acl_resource` (`cn`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

-- FK
ALTER TABLE `acl_rule` 
	DROP FOREIGN KEY `FK_acl_rule_acl_right1`,
	DROP FOREIGN KEY `FK_acl_rule_acl_role1`,
	DROP FOREIGN KEY `FK_acl_rule_acl_resource1`;

ALTER TABLE `acl_rule`
 ADD CONSTRAINT `FK_acl_rule_acl_right1`
 FOREIGN KEY (`rightId`)
 REFERENCES `acl_right` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_acl_rule_acl_role1`
 FOREIGN KEY (`roleId`)
 REFERENCES `acl_role` (`id`)
 ON DELETE CASCADE
 ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_acl_rule_acl_resource1`
 FOREIGN KEY (`resource`)
 REFERENCES `acl_resource` (`cn`)
 ON DELETE CASCADE
 ON UPDATE CASCADE;

-- renum roles
SET @position=11;
UPDATE acl_role SET `id`=(@position := @position -1) ORDER BY id DESC;
 

-- re-init acl_seq
DROP TABLE acl_seq;
 CREATE TABLE `acl_seq` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=10;
INSERT INTO acl_seq (id) SELECT max(id) FROM(
	SELECT id FROM acl_role
	UNION
	SELECT id FROM acl_user
) AS aclobj;

-- CREATE A VIEW WITH ALL acl_seq dependants objects
DROP VIEW IF EXISTS acl_objects_view;
CREATE VIEW acl_objects_view AS
SELECT `id`, `uid`, `name`, 'acl_role' AS `fromclass` FROM acl_role
UNION
SELECT `id`, `uid`, `login` as `name`, 'acl_user' AS `fromclass` FROM acl_user
UNION
SELECT `id`, `uid`, `name`, 'acl_group' AS `fromclass` FROM acl_group
ORDER BY id ASC;

-- select * from acl_objects_view;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.5.1',now());
