DROP TABLE IF EXISTS `indexer_batch_log`;
CREATE TABLE `indexer_batch_log` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`started` DATETIME NULL,
`ended` DATETIME NULL,
`status` VARCHAR (128) NULL,
PRIMARY KEY (`id`),
KEY `INDEX_batchindexer_1` (`started`)
) ENGINE=MyIsam;

DROP TABLE IF EXISTS `batchjob`;
CREATE TABLE `batchjob` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(258) NULL,
	`runner` VARCHAR(128) NULL,
	`owner_id` INT(11) NULL,
	`owner_uid` VARCHAR(128) NULL,
	`status` VARCHAR(32) NULL,
	`planned` DATETIME NULL,
	`created` DATETIME NULL,
	`started` DATETIME NULL,
	`ended` DATETIME NULL,
	`duration` INT(11) NULL,
	`hashkey` VARCHAR(40) NOT NULL,
	`data` TEXT NULL,
	PRIMARY KEY (`id`),
	INDEX `K_batchjob_1` (`planned`),
	INDEX `K_batchjob_2` (`status`),
	INDEX `K_batchjob_3` (`runner`),
	UNIQUE `U_batchjob_4` (`hashkey`)
) ENGINE=MyIsam;

-- select * from batchjob;

INSERT INTO schema_version(version,lastModification) VALUES('1.0.5.3',now());
