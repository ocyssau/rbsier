<?php
// %LICENCE_HEADER%
rbinit_includepath();

use Rbplm\People;
use Rbs\People\User\Preference as UserPreference;
use Rbs\Auth;
use Rbplm\Rbplm;
use Rbplm\Dao\Connexion;
use Rbplm\Sys\Filesystem;
use Zend\Authentication\AuthenticationService;
use Zend\Diactoros\Exception\DeprecatedMethodException;

/**
 */
function rbinit_filesystem($ranchbe)
{
	if ( $ranchbe->getConfig('filesystem.secure') ) {
		Filesystem::isSecure(true);
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.default')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.workitem')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.mockup')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.cadlib')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.bookshop')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.workitem.archive')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.mockup.archive')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.cadlib.archive')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.bookshop.archive')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.trash')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.importpackage')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.importunpack')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.scripts.doctype')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.scripts.datatype')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.scripts.queries')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.scripts.xlsrender')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.scripts.stats')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.workflow.process')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.temp')));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.default'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.workitem'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.mockup'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.cadlib'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.bookshop'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.workitem.archive'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.mockup.archive'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.cadlib.archive'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.bookshop.archive'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.trash'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.importpackage'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.reposit.importunpack'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.scripts.doctype'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.scripts.datatype'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.scripts.queries'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.scripts.xlsrender'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.scripts.stats'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.workflow.process'));
		Filesystem::addAuthorized($ranchbe->getConfig('path.temp'));

		foreach( $ranchbe->getConfig('path.authorized') as $path ) {
			Filesystem::addAuthorized(realpath($path));
			Filesystem::addAuthorized($path);
		}
		
		\Rbplm\Sys\Trash::init($ranchbe->getConfig('path.reposit.trash'));
	}
	else {
		Filesystem::isSecure(false);
	}
}

/**
 * Init constants for web interface
 *
 * @return void
 */
function rbinit_web()
{
	/*
	 * Disable auto register global of sessions variables.
	 * Note that register_globals must be set to false too.
	 */
	ini_set('session.bug_compat_42', false);
	define('CRLF', "<br />");
}

/**
 *
 * @deprecated
 * @param array $config        	
 */
function rbinit_daoservice($config)
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 *
 * @param array $config        	
 */
function rbinit_rbservice($config)
{
	Rbplm::setConfig($config);
	\Rbplm\Uuid::init('UniqId');
	\Rbplm\Ged\Doctype::$defaultDoctypeId = $config['doctype.default.id'];

	/* Number normalizer */
	\Rbs\Number::$search = $config['number.filter']['search'];
	\Rbs\Number::$replace = $config['number.filter']['replace'];
}

/**
 *
 * @param string $errno        	
 * @param string $errstr        	
 * @param string $errfile        	
 * @param string $errline        	
 * @throws ErrorException
 */
function rbinit_exception_error_handler($errno, $errstr, $errfile, $errline)
{
	if ( error_reporting() === 0 ) {
		return;
	}
	throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

/**
 * Init constants for command line interface
 */
function rbinit_cli()
{
	ini_set('xdebug.cli_color', 1);

	if ( isset($_SERVER['HTTP_USER_AGENT']) ) {
		if ( stristr($_SERVER['HTTP_USER_AGENT'], 'windows') ) {
			if ( !defined('CLIENT_OS') ) {
				define('CLIENT_OS', 'WINDOWS');
			}
			if ( !defined('PHP_EOL') ) {
				define('PHP_EOL', "\r\n");
			}
		}
		else if ( stristr($_SERVER['HTTP_USER_AGENT'], 'macintosh') || stristr($_SERVER['HTTP_USER_AGENT'], 'mac_powerpc') ) {
			if ( !defined('CLIENT_OS') ) {
				define('CLIENT_OS', 'MACINTOSH');
			}
			if ( !defined('PHP_EOL') ) {
				define('PHP_EOL', "\r");
			}
		}
		else {
			if ( !defined('CLIENT_OS') ) {
				define('CLIENT_OS', 'UNIX');
			}
			if ( !defined('PHP_EOL') ) {
				define('PHP_EOL', "\n");
			}
		}
	}
	else {
		if ( !defined('PHP_EOL') ) {
			define('PHP_EOL', "\n");
		}
	}

	if ( !defined('CRLF') ) {
		define('CRLF', PHP_EOL);
	}
}

/**
 * @return \Zend\Session\SessionManager
 * @deprecated
 */
function rbinit_session($options)
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 * Init a autoloader.
 *
 * @param array $config        	
 */
function rbinit_autoloader(array $config)
{
	$loader = include 'vendor/autoload.php';
	foreach( $config['modules'] as $module ) {
		$loader->set($module . '\\', 'module/' . $module . '/src');
	}

	#$loader->set('Application\\', 'module/Application/src');
	#$loader->set('XlsRenderer\\', 'data/scripts/');
	#$loader->set('ComposerScript\\', 'config/install');

	return $loader;
}

/**
 * Init include path
 */
function rbinit_includepath()
{
}

/**
 * @deprecated
 */
function rbinit_logger(array $config)
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 * @deprecated
 */
function rbinit_errorStack()
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 *
 * @param array $config        	
 */
function rbinit_checkInstall($config)
{
	if ( ini_get('register_globals') == true ) {
		print 'Before use RanchBE you must set Register_global php directive to off in php.ini file.
		You can too set this directive in .htaccess file. See php documentation.';
	}

	$wrong = false;

	// ------------------------------------------------------------------------
	// Check that necessary directories exists
	// ------------------------------------------------------------------------
	/*
	 * if (!is_dir ($config['path.reposit.wildspace'])){
	 * print 'Error : '.$config['path.reposit.wildspace'].' do not exits'.CRLF;
	 * $wrong=true;
	 * }
	 */
	if ( !is_dir($config['path.reposit.workitem']) ) {
		print 'Error : ' . $config['path.reposit.workitem'] . '  do not exits' . CRLF;
		$wrong = true;
	}
	if ( !is_dir($config['path.reposit.mockup']) ) {
		print 'Error : ' . $config['path.reposit.mockup'] . '  do not exits' . CRLF;
		$wrong = true;
	}
	if ( !is_dir($config['path.reposit.cadlib']) ) {
		print 'Error : ' . $config['path.reposit.cadlib'] . '  do not exits' . CRLF;
		$wrong = true;
	}
	if ( !is_dir($config['path.reposit.bookshop']) ) {
		print 'Error : ' . $config['path.reposit.bookshop'] . '  do not exits' . CRLF;
		$wrong = true;
	}
	if ( !is_dir($config['path.reposit.trash']) ) {
		print 'Error : ' . $config['path.reposit.trash'] . '  do not exits' . CRLF;
		$wrong = true;
	}
	if ( !is_dir($config['path.reposit.importpackage']) ) {
		print 'Error : ' . $config['path.reposit.importpackage'] . '  do not exits' . CRLF;
		$wrong = true;
	}
	if ( !is_dir($config['path.reposit.importunpack']) ) {
		print 'Error : ' . $config['path.reposit.importunpack'] . '  do not exits' . CRLF;
		$wrong = true;
	}
	if ( !is_dir($config['path.reposit.workitem.archive']) ) {
		print 'Error : ' . $config['path.reposit.workitem.archive'] . '  do not exits' . CRLF;
		$wrong = true;
	}
	if ( !is_dir($config['path.reposit.mockup.archive']) ) {
		print 'Error : ' . $config['path.reposit.mockup.archive'] . '  do not exits' . CRLF;
		$wrong = true;
	}
	if ( !is_dir($config['path.reposit.cadlib.archive']) ) {
		print 'Error : ' . $config['path.reposit.cadlib.archive'] . '  do not exits' . CRLF;
		$wrong = true;
	}
	if ( !is_dir($config['path.reposit.bookshop.archive']) ) {
		print 'Error : ' . $config['path.reposit.bookshop.archive'] . '  do not exits' . CRLF;
		$wrong = true;
	}

	if ( $wrong ) {
		die();
	}
}

/**
 * For Xdebug V3
 * @param boolean $enable        	
 */
function rbinit_enableDebug($enable = true)
{
	if ( $enable ) {
		error_reporting(E_ALL);
		
		ini_set('xdebug.mode', 'develop,debug,trace');
		
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		ini_set('xdebug.show_exception_trace', 0);
		ini_set('xdebug.show_error_trace', 1);
		ini_set('xdebug.show_local_vars', 0);
		/* Display undefined var too */
		ini_set('xdebug.dump_undefined', 0);
		/* Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals() */
		// ini_set('xdebug.dump.POST', '*');
		/* Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals() */
		// ini_set('xdebug.dump.GET', '*');
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.var_display_max_children', 300);
		ini_set('xdebug.var_display_max_data', 3000);
		ini_set('xdebug.var_display_max_depth', 3);

		/* tree nested levels of array elements and object relations are displayed */
		ini_set('xdebug.max_nesting_level', 120);

		/*
		 * 1: computer readable format / 0: shows a human readable indented trace file with: time index,
		 * memory usage, memory delta (if the setting xdebug.show_mem_delta is enabled), level, function name,
		 * function parameters (if the setting xdebug.collect_params is enabled, filename and line number.
		 */
		ini_set('xdebug.trace_format', 0);

		/*
		 * set_exception_handler(function($e){
		 * echo "Uncaught exception: " , $e->getMessage(), "\n";
		 * //debug_print_backtrace();
		 * });
		 */
	}
	else {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);
		ini_set('display_errors', 0);
		ini_set('display_startup_errors', 1);
		ini_set('xdebug.mode', 'off');
	}
	
	/* pour changer tous les messages d'erreurs en ErrorException */
	set_error_handler("rbinit_exception_error_handler", E_ALL);
}

/**
 *
 * @param boolean $enable
 */
function rbinit_enableDebug_xdebugv2($enable = true)
{
	if ( $enable ) {
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		ini_set('xdebug.show_exception_trace', 0);
		ini_set('xdebug.show_error_trace', 1);
		ini_set('xdebug.show_local_vars', 0);
		/* Display undefined var too */
		ini_set('xdebug.dump_undefined', 0);
		/* Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals() */
		// ini_set('xdebug.dump.POST', '*');
		/* Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals() */
		// ini_set('xdebug.dump.GET', '*');
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 300);
		ini_set('xdebug.var_display_max_data', 3000);
		ini_set('xdebug.var_display_max_depth', 3);
		
		/* tree nested levels of array elements and object relations are displayed */
		ini_set('xdebug.max_nesting_level', 120);
		
		/*
		 * 1: computer readable format / 0: shows a human readable indented trace file with: time index,
		 * memory usage, memory delta (if the setting xdebug.show_mem_delta is enabled), level, function name,
		 * function parameters (if the setting xdebug.collect_params is enabled, filename and line number.
		 */
		ini_set('xdebug.trace_format', 0);
		
		/*
		 * set_exception_handler(function($e){
		 * echo "Uncaught exception: " , $e->getMessage(), "\n";
		 * //debug_print_backtrace();
		 * });
		 */
	}
	else {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);
		ini_set('display_errors', 0);
		ini_set('display_startup_errors', 1);
		ini_set('xdebug.show_local_vars', 0);
		ini_set('xdebug.dump_undefined', 0);
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 5);
		ini_set('xdebug.var_display_max_data', 10);
		ini_set('xdebug.var_display_max_depth', 1);
		//ini_set('xdebug.max_nesting_level', 5);
		ini_set('xdebug.show_exception_trace', 0);
		ini_set('xdebug.trace_format', 1);
	}
	
	/* pour changer tous les messages d'erreurs en ErrorException */
	set_error_handler("rbinit_exception_error_handler", E_ALL);
}


/**
 *
 * @deprecated
 */
function rbinit_authservice()
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 * @deprecated
 */
function rbinit_currentUser($authservice)
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 *
 * @deprecated
 */
function rbinit_userprefs(People\User $user)
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 *
 * @deprecated
 * @param People\User $user
 */
function rbinit_wildspace(People\User $user)
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 * @deprecated
 */
function rbinit_anonymousUser($config)
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 *
 * @deprecated
 */
function rbinit_view($config)
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 *
 * @deprecated
 * @param boolean $bool        	
 */
function rbinit_unactive($bool)
{	
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 * @deprecated
 * Init the context
 */
function rbinit_context()
{
	throw new DeprecatedMethodException(__FUNCTION__ . ' is obsolete');
}

/**
 * translate a English string
 *
 * @param $content -
 *        	English string
 */
function tra($txt)
{
	return Ranchbe::tra($txt);
}
