
Array.prototype.last = function () {
    return this[this.length - 1];
};

/**
 * 
 */
function RbViewer(canvas)
{
	this.canvas = canvas;
	var engine = new BABYLON.Engine(canvas, true);
	this.isLoad = false;
	
	this.init = function()
	{
		this.createScene();
		this.showAxis(100);
	}

	/**
	 * INIT TABLE SORTABLE HEADERS 
	 */
	this.createScene = function()
	{
		var canvas = this.canvas;
		BABYLON.Engine.enableOfflineSupport = false;
		var engine = new BABYLON.Engine(canvas, true);
		var scene = new BABYLON.Scene(engine);
		scene.ExclusiveDoubleClickMode = true;
		scene.LongPressDelay = 200;
		scene.DoubleClickDelay = 50;
		scene.fogEnabled = false;
		scene.fogMode = BABYLON.Scene.FOGMODE_EXP;
		scene.shadowsEnabled = true;
		scene.lightsEnabled = true;

		var camera = new BABYLON.ArcRotateCamera("Camera1", 0, 0, 0, new BABYLON.Vector3(500, 500, 500), scene);
	    //var camera = new BABYLON.ArcRotateCamera("Camera1", -Math.PI / 2,  Math.PI / 4, 5, BABYLON.Vector3.Zero(), scene);
	    camera.useFramingBehavior = true;
		camera.attachControl(canvas, false);
	    /* This targets the camera to scene origin */
	    camera.setTarget(BABYLON.Vector3.Zero());
	    camera.wheelDeltaPercentage = 1;
	    camera.zoomOnFactor = 10;
	    
	    //www.html5gamedevs.com 
	    
	    var ground = BABYLON.Mesh.CreateGround("ground1", 1000, 1000, 0, scene);
		ground.isVisible = 0;

	    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);
	    light.intensity = 0.7;
	    
	    var light2 = new BABYLON.HemisphericLight("light2", new BABYLON.Vector3(0, 1, 0), scene);
	    light2.intensity = 0.7;
	    
	    /* Move the light with the camera */
	    scene.registerBeforeRender(function () {
	        light.position = camera.position;
	    });

		var loader = new BABYLON.AssetsManager(scene);
		var i=0;
		var pos = function(t) {
	        i = i+100;
	        t.loadedMeshes.forEach(function (m) {
				//m.dispose();
				m.position = new BABYLON.Vector3(0,0,i)
				/* Reframe on mesh */
				camera.setTarget(m);
				camera.storeState();
			});
	    };

		//meshTask.onSuccess = pos;

		/* a bug on chrome (and playgroud), this will load the batman torso correctly */
		loader.onFinish = function () {
			engine.runRenderLoop(function () {
				scene.render();
			});
	    };
	    
	    this.camera = camera;
	    this.loader = loader;
	    this.scene = scene;
		
		return this;
	};
	
	/**
	 * 
	 */
    this.showAxis = function (size) 
    {
    	var scene = this.scene;
    	
        var makeTextPlane = function (text, color, size) {
            var dynamicTexture = new BABYLON.DynamicTexture("DynamicTexture", 50, scene, true);
            dynamicTexture.hasAlpha = true;
            dynamicTexture.drawText(text, 5, 40, "bold 36px Arial", color, "transparent", true);
            var plane = new BABYLON.Mesh.CreatePlane("TextPlane", size, scene, true);
            plane.material = new BABYLON.StandardMaterial("TextPlaneMaterial", scene);
            plane.material.backFaceCulling = false;
            plane.material.specularColor = new BABYLON.Color3(0, 0, 0);
            plane.material.diffuseTexture = dynamicTexture;
            return plane;
        };

        var axisX = BABYLON.Mesh.CreateLines("axisX", [
            new BABYLON.Vector3.Zero(), new BABYLON.Vector3(size, 0, 0), new BABYLON.Vector3(size * 0.95, 0.05 * size, 0),
            new BABYLON.Vector3(size, 0, 0), new BABYLON.Vector3(size * 0.95, -0.05 * size, 0)
        ], scene);
        
        axisX.color = new BABYLON.Color3(1, 0, 0);
        var xChar = makeTextPlane("X", "red", size / 10);
        xChar.position = new BABYLON.Vector3(0.9 * size, -0.05 * size, 0);
        var axisY = BABYLON.Mesh.CreateLines("axisY", [
            new BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, size, 0), new BABYLON.Vector3(-0.05 * size, size * 0.95, 0),
            new BABYLON.Vector3(0, size, 0), new BABYLON.Vector3(0.05 * size, size * 0.95, 0)
        ], scene);
        axisY.color = new BABYLON.Color3(0, 1, 0);
        var yChar = makeTextPlane("Y", "green", size / 10);
        yChar.position = new BABYLON.Vector3(0, 0.9 * size, -0.05 * size);
        var axisZ = BABYLON.Mesh.CreateLines("axisZ", [
            new BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, 0, size), new BABYLON.Vector3(0, -0.05 * size, size * 0.95),
            new BABYLON.Vector3(0, 0, size), new BABYLON.Vector3(0, 0.05 * size, size * 0.95)
        ], scene);
        axisZ.color = new BABYLON.Color3(0, 0, 1);
        var zChar = makeTextPlane("Z", "blue", size / 10);
        zChar.position = new BABYLON.Vector3(0, 0.05 * size, 0.9 * size);
        
        return this;
    };
    
    /**
     * 
     */
	this.addMesh = function(name, url, filename)
	{
		var meshTask = this.loader.addMeshTask(name, "", url, filename);
        return this;
	}
	
    /**
     * 
     */
	this.load = function()
	{
		this.loader.load();
		this.isLoad = true;
        return this;
	}
	
	/**
	 * 
	 */
	this.addcentersphere = function()
	{
		var scene = this.scene;
	    var matBox4 = new BABYLON.StandardMaterial("mat1", scene);
	    matBox4.diffuseColor = new BABYLON.Color3(0.1, 1, 0.1);
	    var center1 = BABYLON.MeshBuilder.CreateSphere("center1", {diameter:1}, scene);
	    center1.position = new BABYLON.Vector3(0, 0, 0); 
	    center1.material = matBox4;
	    center1.isPickable = true;
	    return this;
	}
	
	this.addcube = function()
	{
		var scene = this.scene;
	    var matBox4 = new BABYLON.StandardMaterial("mat1", scene);
	    matBox4.diffuseColor = new BABYLON.Color3(0.1, 1, 0.1);
	    var cube1 = BABYLON.MeshBuilder.CreateBox("cube1", {size:900}, scene);
	    cube1.position = new BABYLON.Vector3(0, 0, 0); 
	    cube1.material = matBox4;
	    cube1.isPickable = true;
	    return this;
	}
	
	
	/**
	 * 
	 */
	this.gui = function()
	{
    	var scene = this.scene;
    	var camera = this.camera;
    	var ground = scene.getMeshByID("ground1");

	    /* GUI */
	    var advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
	    var panel = new BABYLON.GUI.StackPanel();    
	    panel.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
	    panel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
	    advancedTexture.addControl(panel);
	    
	    function buttonFactory(id, label){
	        var btn = BABYLON.GUI.Button.CreateSimpleButton(id, label);
	        btn.width = "80px";
	        btn.height = "30px";
	        btn.color = "white";
	        btn.background = "green";
	        return btn;
	    }
	    
		var corBtn = buttonFactory("but1", "CoR");
		panel.addControl(corBtn);

		var resetBtn = buttonFactory("but2", "Reset");
		panel.addControl(resetBtn);

		var measureBtn = buttonFactory("but3", "Measure");
		panel.addControl(measureBtn);
		
	    var textblock = new BABYLON.GUI.TextBlock("textblock", "");
	    textblock.width = 1;
	    textblock.height = "40px";
	    textblock.color = "white";
	    panel.addControl(textblock);

	    corBtn.onPointerDownObservable.add(function(event,snd) {
	    	textblock.text="Select Center of view";
	    	scene.onPointerDown = function (evt, pickResult) {
	    		if(pickResult.hit){
	    			camera.setTarget(pickResult.pickedPoint);
	    			scene.onPointerDown = null;
	    	    	textblock.text="";
	    		}
	    	};
	    });

	    resetBtn.onPointerDownObservable.add(function(event,snd) {
	    	//textblock.text = "Reframe";
	    	var mesh = scene.meshes.last();
			camera.setTarget(mesh);
			//camera.restoreState();
	    	//textblock.text = " ";
	    });
	    
	    
	    var zoomOn = function()
	    {
	    	var fov = cameraFov * ( Math.PI / 180 );
	    	var objectSize = 0.6 + ( 0.5 * Math.sin( Date.now() * 0.001 ) );

	    	var cameraPosition = new THREE.Vector3(
	    	    0,
	    	    sphereMesh.position.y + Math.abs( objectSize / Math.sin( fov / 2 ) ),
	    	    0
	    	);
	    }

	    measureBtn.onPointerDownObservable.add(function(event) {
	    	textblock.text = "Select First Point";
		    var material1 = new BABYLON.StandardMaterial("color1", scene);
		    material1.diffuseColor = new BABYLON.Color3(0.1, 1, 0.1);

		    /*cleanup previous marks */
			var mark1 = scene.getMeshByName("measurePt1");
			var mark2 = scene.getMeshByName("measurePt2");

			if(mark1 != null){
				mark1.dispose();
			}
			if(mark2 != null){
				mark2.dispose();
			}

	    	scene.onPointerDown = function (evt, pickResult) {
	        	if(evt.buttons != 1){return false;}
	    		if(pickResult.hit){
	        		var firstPoint = pickResult.pickedPoint;
	        		var sphere1 = BABYLON.MeshBuilder.CreateSphere("measurePt1", {diameter:5}, scene);
	        		sphere1.position = firstPoint;
	        	    sphere1.material = material1;
	            	textblock.text = "Select Second Point";
	            	scene.onPointerDown = function (evt, pickResult) {
	                	if(evt.buttons != 1){return false;}
	            		if(pickResult.hit){
	                		var secondPoint = pickResult.pickedPoint;
	                		var sphere2 = BABYLON.MeshBuilder.CreateSphere("measurePt2", {diameter:5}, scene);
	                		sphere2.position = secondPoint;
	                	    sphere2.material = material1;
	                        		
	                		scene.onPointerDown = null;
	                		var path3d = new BABYLON.Path3D([firstPoint, secondPoint]);
	                		var distances = path3d.getDistances();
							var dist = Math.round(distances[1]*1000)/1000;

							var components = new BABYLON.Vector3();
							components.x = Math.round((secondPoint.x-firstPoint.x)*1000)/1000;
							components.y = Math.round((secondPoint.y-firstPoint.y)*1000)/1000;
							components.z = Math.round((secondPoint.z-firstPoint.z)*1000)/1000;

							textblock.text = dist.toString() +" : "+ components.toString();
	            		}
	            	};
	    		}
	    	};
	    });
	    
        return this;
	}
}

    

