/**
 * 
 */
function RbTypemimeManager(jqElement)
{
	this.service = new RbTypemimeService();
	this.jqElement = jqElement;

	/* @var RbTypemimeButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbTypemimeButtons(jqParentElement);
		return rbButtons;
	};
}
RbTypemimeManager.prototype = new RbManager();

/**
 * 
 */
function RbTypemimeButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* create */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/admin/typemime/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#typemimeEdit"));
				}
			});
			return false;
		});
		
		/* edit */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/admin/typemime/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#typemimeEdit"));
				}
			});
			return false;
		});
		
		/* delete */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this typemime?')){
				var url = document.baseurl+'/admin/typemime/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
	};
}
RbTypemimeButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbTypemimeService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'typemimes':{}};
		$.each(checked, function(index, id){
			serviceData.categories['typemime'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {typemimes:{
			typemime1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbTypemimeService.prototype = new RbService();
