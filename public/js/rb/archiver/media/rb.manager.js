
/**
 * 
 */
function RbMediaManager(jqElement, spacename)
{
	this.spacename = spacename;
	this.service = new RbMediaService(spacename);
	this.jqElement = jqElement;

	/* @var RbMediaButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbMediaButtons(jqParentElement);
		return rbButtons;
	};
}
RbMediaManager.prototype = new RbManager();

/**
 * 
 */
function RbMediaButtons(jqParentElement, spaceName)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/ged/archiver/media/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#mediaEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/ged/archiver/media/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#mediaEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this media?')){
				var url = document.baseurl+'ged/archiver/media/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
	};
}
RbMediaButtons.prototype = new RbButtons();


/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbMediaService()
{
	
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
			if(item.name.substring(0,11) == "spacenames["){
				eval(item.name + "='" + item.value + "'");
			}
		});
		var i=1;
		var serviceData = {'medias':{}};
		$.each(checked, function(index, id){
			serviceData.categories['media'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {medias:{
			media1:{
				id:elemt.data('id'),
				spacename:elemt.data('spacename')
			}
		}};
		return queryData;
	};
}
RbMediaService.prototype = new RbService();
