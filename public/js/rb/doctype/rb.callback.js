/**
 * 
 */
function RbCallbackButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* create */
		parent.find(".create-btn").click(function(e){
			doctypeId = parent.find(".rb-submit-data").data("doctypeid");
			var url = document.baseurl+'/ged/doctype/callback/'+doctypeId+'/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#callbackEdit"));
				}
			});
			return false;
		});
		
		/* edit */
		parent.find(".edit-btn").click(function(e){
			doctypeId = parent.find(".rb-submit-data").data("doctypeid");
			var url = document.baseurl+'/ged/doctype/callback/'+doctypeId+'/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#callbackEdit"));
				}
			});
			return false;
		});
		
		/* delete */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this callback?')){
				doctypeId = parent.find(".rb-submit-data").data("doctypeid");
				var url = document.baseurl+'/ged/doctype/callback/'+doctypeId+'/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
	};
}
RbCallbackButtons.prototype = new RbButtons();

/**
 * 
 */
function RbCallbackManager(jqElement)
{
	this.service = new RbCallbackService();
	this.jqElement = jqElement;

	
	/* INIT TABLE AND CONTEXTUAL MENU */
	this.initTableCm = function()
	{
		var rbButtons = new RbCallbackButtons(jqElement.find("#callback-context-menu"));
		var table = new RbTable( jqElement.find("table.rb-callback") );
		table.markrowsInit();
		table.initContextMenu(rbButtons, new RbCallbackService(), "tbody tr.rb-callback");
		table.jqTable.find("input[name='checked[]']").hide();
		return this;
	};
	
	/* INIT BUTTONS BOX */
	this.initButtonBox = function()
	{
		var jqButtons = jqElement.find("#page-bar");
		var rbButtons = new RbCallbackButtons(jqButtons);
		var rbBtnBox = new RbObjectBox( jqButtons );
		rbBtnBox.initButtons(rbButtons);
		return this;
	};
	
	/* OBJECT BOX */
	this.initObjectBox = function()
	{
		var box = new RbObjectBox(jqElement.find("table.rb-callback .object-box"));
		box.init();
		box.initButtons(new RbCallbackButtons(box.jqElement));
		return this;
	};
}
RbCallbackManager.prototype = new RbManager();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbCallbackService()
{

	/**
	 * Serialize form as required by rb service
	 */
	this.serializeForm = function(form)
	{
		var formdata = form.serializeArray();
		var checked=[];
		var queryData = {categories:{}};
		var i=0;
		$.each(formdata, function(index,item){
			if(item.name=="checked[]"){
				checked[i] = item.value;
				i = i+1;
			}
		});
		i=1;
		$.each(checked, function(index,id){
			queryData['categories']['callback'+i]={
					id:id,
			}
			i = i+1;
		});
		return queryData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {categories:{
			callback1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbCallbackService.prototype = new RbService();
