/**
 * 
 */
function RbDoctypeManager(jqElement)
{
	this.service = new RbDoctypeService();
	this.jqElement = jqElement;

	/* @var RbDoctypeButtons */
	this.rbButtons = null;

	/**
	 * Get context menu
	 * @var jqParentElement jquery element with data definition for actions. Must have class rb-submit-data.
	 * @return RbWildspaceButtons
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbDoctypeButtons(jqParentElement);
		return rbButtons;
	};
	
}
RbDoctypeManager.prototype = new RbManager();

/**
 * 
 */
function RbDoctypeButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* create */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/ged/doctype/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#doctypeEdit"));
				}
			});
			return false;
		});
		
		/* compile icon */
		parent.find(".compileicon-btn").click(function(e){
			var url = document.baseurl+'/ged/doctype/buildicons';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/* edit */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/ged/doctype/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#doctypeEdit"));
				}
			});
			return false;
		});
		
		/* edit callback */
		parent.find(".edit-callback-btn").click(function(e){
			var url = document.baseurl+'/ged/doctype/callback/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#callbackEdit"));
				}
			});
			return false;
		});
		
		/* edit mask */
		parent.find(".edit-mask-btn").click(function(e){
			var maskId = $(this).parents(".rb-submit-data").data('maskid');
			console.log($(this).parents(".rb-submit-data"));
			var url = document.baseurl+'/docseeder/mask/edit/'+maskId;
			actionProcessor.getModal(e, $(this), url, {
				hash: "",
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#maskform"));
				}
			});
			return false;
		});

		/* delete */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this doctype?')){
				var url = document.baseurl+'/ged/doctype/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
	};
}
RbDoctypeButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDoctypeService()
{

	/**
	 * Serialize form as required by rb service
	 */
	this.serializeForm = function(form)
	{
		var formdata = form.serializeArray();
		var checked=[];
		var queryData = {categories:{}};
		var i=0;
		$.each(formdata, function(index,item){
			if(item.name=="checked[]"){
				checked[i] = item.value;
				i = i+1;
			}
		});
	}

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {doctypes:{
			doctype1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbDoctypeService.prototype = new RbService();
