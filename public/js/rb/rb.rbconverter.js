/**
 * 
 */
function RbCheckinForm()
{
	var form = $("#checkinform");
	this.form = form;

	/**
	 * 
	 */
	this.init = function(converter)
	{
		this.converter = converter;
		var me = this;
		var form = this.form;

		/**
		 * 
		 */
		form.find('#submitbutton').click(function(e){
			e.preventDefault();
			e.stopPropagation();

			/* @todo: not used ?*/
			form.append('<input type="hidden" value="validate" name="validate">');
			form.append('<input type="hidden" value="save" name="save">');

			/* convert datas */
			var convertedDatas = converter.convert(me.getToValidate());

			/* datas is array of converted data or false if conversion failed */
			if(convertedDatas != false){
				var pdmDataElemt = $('<input type="hidden" name="pdmdata">');
				pdmDataElemt.val(JSON.stringify(convertedDatas));
				form.append(pdmDataElemt);
				console.log(pdmDataElemt);
			}
			
			me.submit(form, me.successCallback, me.failedCallback);
			
			return false;
		});

		/**
		 * 
		 */
		form.find('#rb-converter-convert-btn').click(function(e){
			e.preventDefault();
			e.stopPropagation();

			var datas = converter.convert(files);
			return false;
		});

		/**
		 * 
		 */
		form.find('#cancelbutton').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			if(me.cancel){
				me.cancel(e, me);
			}
			return false;
		});
	};

	/**
	 * return list of files to convert
	 */
	this.getToValidate = function()
	{
		var files=[];
		form.find('input:checked.tovalidate-cb').each(function(index){
			var filename = $(this).val();
			files[index]=filename;
		});
		console.log(files);
		return files;
	};
	
	/*callback to load after submit */
	this.successCallback = function(e, domElement){};
	/*callback to load after submit */
	this.failedCallback = function(e, domElement){};
	
	/* callback to call on cancel*/
	this.cancel = function(e, domElement){};

	/**
	 * 
	 */
	this.submit = function(form, successCallback, failedCallback)
	{
		var queryData = form.serializeArray();
		var url = form.attr('action');

		$.ajax({
			type : 'POST',
			url : url,
			data : queryData,
			dataType: 'json',
			success : function(data, textStatus, jqXHR){
				var respons = data;
				console.log(data);

				/* display to user */
				messageDisplay(respons.feedbacks);

				if(successCallback){
					successCallback(form);
				}

				return true;
			},
			error : function(data, textStatus, jqXHR){
				var respons = data.responseJSON;
				console.log(respons);

				/* display errors to user */
				errorDisplay(respons.errors);
				
				if(failedCallback){
					failedCallback(form);
				}

				return true;
			}
		});
	}
}

/**
 * 
 */
function RbGateClient(rbgateServerUrl)
{

	/**
	 * 
	 */
	this.options = {
		baseurl: document.baseurl,
		ranchbeServerUrl: document.baseurl,
		rbgateServerUrl: rbgateServerUrl
	};

	/**
	 * 
	 */
	this.setOption = function(name,value)
	{
		eval("this.options."+name+"='"+value+"'");
		return this;
	};

	/**
	 * 
	 */
	this.setOptions = function(options)
	{
		this.options = options;
		return this;
	};
	
	/**
	 * Send result of convert to ranchbe/rbgate/upload
	 */
	this.successCallback = function(data, textStatus, jqXHR){
		console.log(data);
		
		var url = this.options.ranchbeServerUrl+'/rbgate/upload/json';
		var queryData = {
			'name':'',
			'data':'',
		};
		
		$.ajax({
			type : 'POST',
			url : url,
			data : queryData,
			dataType: 'json',
			success : function(data, textStatus, jqXHR){
				var respons = data;
				console.log(data);
				return true;
			},
			error : function(data, textStatus, jqXHR){
				var respons = data.responseJSON;
				console.log(respons);
				/* display errors to user */
				errorDisplay(respons.errors);
				return true;
			}
		});
		
		return data;
	};
	
	/**
	 * Display and log errors
	 */
	this.failedCallback = function(jqXHR, textStatus){
		if(jqXHR.responseJSON){
			if(jqXHR.responseJSON.errors){
				if(jqXHR.responseJSON.errors[0]){
					errorMessage = jqXHR.responseJSON.errors[0];
				}
			}
			ranchbe.confirmDialogBox("Conversion Failed:", errorMessage, function(){
				var data = jqXHR.responseJSON;
				console.log(data);
				return data;
			});
			console.log(jqXHR.responseJSON);
		}
		else{
			console.log(jqXHR);
			return false;
		}
	};
	
	/**
	 * return a array with pdm data resulting of conversion
	 * 
	 * request is a cross domain, and it always send as GET and not in XMLHttpRequest
	 * 
	 */
	this.convert = function(files)
	{
		var rbgateServerUrl = this.options.rbgateServerUrl;
		if(rbgateServerUrl == ''){
			console.log('you must set option "rbgateServerUrl"');
			return;
		}
		
		/* get file from widspace as configure un rbgateServer */
		url = rbgateServerUrl+'/fromwsfiles';
		
		var ajaxData = {
				files: files,
				isService: true
		};
		$.get({
			method : 'get',
			dataType: 'json',
			url : url,
			data : ajaxData,
			timeout: 30000,
		})
		.success(this.successCallback)
		.fail(this.failedCallback);
		return false;
	};
	
	/**
	 * return a array with pdm data resulting of conversion
	 * 
	 * request is a cross domain, and it always send as GET and not in XMLHttpRequest
	 * 
	 */
	this.getConfig = function()
	{
		var url = this.options.ranchbeServerUrl+'/rbgate/config/get';
		var queryData = {};
		
		$.ajax({
			type : 'GET',
			url : url,
			data : queryData,
			dataType: 'json',
			success : function(data, textStatus, jqXHR){
				var respons = data;
				console.log(data);
				return true;
			},
			error : function(data, textStatus, jqXHR){
				var respons = data.responseJSON;
				console.log(respons);
				/* display errors to user */
				errorDisplay(respons.errors);
				return true;
			}
		});
	}
	
}
