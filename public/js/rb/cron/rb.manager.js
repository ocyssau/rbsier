/**
 * 
 */
function RbCronManager(jqElement)
{
	this.service = new RbCronService();
	this.jqElement = jqElement;

	/* @var RbCronButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbCronButtons(jqParentElement);
		return rbButtons;
	};
}
RbCronManager.prototype = new RbManager();

/**
 * 
 */
function RbCronButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* create */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/batch/cron/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				cron: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#cronEdit"));
				}
			});
			return false;
		});
		
		/* edit */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/batch/cron/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				cron: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#cronEdit"));
				}
			});
			return false;
		});
		
		/* run */
		parent.find(".run-btn").click(function(e){
			if(confirm('Do you want really run this cron now?')){
				var url = document.baseurl+'/batch/cron/run';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		/* delete */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this cron?')){
				var url = document.baseurl+'/batch/cron/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		/* jobs */
		parent.find(".job-btn").click(function(e){
			var url = document.baseurl+'/batch/jobs';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* syslog */
		parent.find(".syslog-btn").click(function(e){
			var url = document.baseurl+'/batch/cron/syslog';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		
	};
}
RbCronButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbCronService()
{

	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'crons':{}};
		$.each(checked, function(index, id){
			serviceData.documents['cron'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {crons:{
			cron1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbCronService.prototype = new RbService();
