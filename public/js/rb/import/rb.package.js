/**
 * 
 */
function RbPackageManager(jqElement)
{
	this.service = new RbPackageService();
	this.jqElement = jqElement;

	/* @var RbPackageButtons */
	this.rbButtons = null;

	/* @var dialog */
	this.dialogBox = null;
	
	/* INIT DIALOG BOX */
	this.dialogBox = $("#rename-dialog-form").dialog({
		autoOpen:false,
		height: 300,
		width: 600,
		modal: true,
		close: function() {
		}
	});
	
	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbPackageButtons(jqParentElement);
		return rbButtons;
	};
	
}
RbPackageManager.prototype = new RbManager();

/**
 * 
 */
function RbPackageButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);

	/**/
	this.reload = function(){
		$("#content").load("#", function(){});
		return false;
	};
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		var dialogBox = this.dialogBox;
		
		/**/
		parent.find(".download-btn").click(function(e){
			var url = document.baseurl+'/import/package/download';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/**/
		parent.find(".rename-btn").click(function(e){
			var url = document.baseurl+'/service/import/package/rename';
			var clickedElement = $(this);
			
			var dataDiv = clickedElement.parents(".rb-submit-data").first();
			var file = dataDiv.data('file');
			var nameInput = dialogBox.find("input#name");
			nameInput.val(file);

			dialogBox.dialog({
				buttons: {
					"Ok": function() {
						var newName = nameInput.val();
						dialogBox.dialog( "close" );
						if(newName == null){
							return false;
						}
						actionProcessor.toService(e, $(this), url, {
							successCallback: function(){
								rbButtons.reload();
							},
							queryData: {
								files:{
									file1:{
										name:file,
										newname:newName
									}
								}
							}
						});
					},
					"Cancel": function() {
						dialogBox.dialog("close");
					}
				},
			});
			dialogBox.dialog("open");
			return false;
		});
		
		/**/
		parent.find(".delete-btn").click(function(e){
			if(confirm('Delete file. Are you sure?')){
				var url = document.baseurl+'/service/import/package/delete';
				actionProcessor.toService(e, $(this), url);
				rbButtons.reload();
			}
			return false;
		});
		
		/**/
		parent.find(".copy-btn").click(function(e){
			var url = document.baseurl+'/service/import/package/copy';
			var clickedElement = $(this);
			
			var dataDiv = clickedElement.parents(".rb-submit-data").first();
			var file = dataDiv.data('file');
			var nameInput = dialogBox.find("input#name");
			nameInput.val(file);

			dialogBox.dialog({
				buttons: {
					"Ok": function() {
						var newName = nameInput.val();
						dialogBox.dialog( "close" );
						if(newName == null){
							return false;
						}
						actionProcessor.toService(e, $(this), url, {
							successCallback: function(){
								rbButtons.reload();
							},
							queryData:{
								files:{
									file1:{
										name:file,
										newname:newName
									}
								}
							}
						});
					},
					"Cancel": function() {
						dialogBox.dialog("close");
					}
				}
			});

			dialogBox.dialog("open");
			return false;
		});

		/**/
		parent.find(".btn-uncompress").click(function(){
			var url = document.baseurl+'/import/package/uncompress';
			return false;
		});

		/**/
		parent.find(".btn-zipdownload").click(function(){
			var url = document.baseurl+'/import/package/downloadzip';
			return false;
		});
		
		
	};
}
RbPackageButtons.prototype = new RbButtons();


/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbPackageService()
{
	
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'files':{}};
		$.each(checked, function(index, id){
			serviceData.files['file'+i] = {
				name: id,
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {files:{
			file1:{
				file:elemt.data('file'),
			}
		}};
		return queryData;
	};
}
RbPackageService.prototype = new RbService();
