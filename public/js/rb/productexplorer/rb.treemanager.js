
/**
 * 
 */
function RbPdmButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);

	/**
	 * @param actionProcessor RbAction
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
	};
}
RbProjectButtons.prototype = new RbButtons();

/**
 * 
 */
function RbPdmTreeManager()
{
	
}

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbPdmService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'products':{}};
		$.each(checked, function(index, id){
			serviceData.products['product'+i] = {
				id: id,
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {products:{
			product1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbPdmService.prototype = new RbService();
