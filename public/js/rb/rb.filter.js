/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function rbfilter(formSelector)
{
	var rbfilter = this;
	this.selector = formSelector;
	this.form = $(formSelector);

	/*
	 * 
	 */
	this.displayOption = function(element, speed=400)
	{
		console.log($(element));
		toToggle = $(element).parents("div.form-group").data('toggle');
		if( element.checked ){
			$(toToggle).show(speed);
			//$(element).parent().parent().nextAll('span').children('fieldset').first().show(speed);
		}
		else{
			$(toToggle).hide(speed);
			//$(element).parent().parent().nextAll('span').children('fieldset').first().hide(speed);
		}
	};

	/*
	 * 
	 */
	this.filterSubmit = function(element)
	{
		$(formSelector).submit();
	};
	
	/*
	 * 
	 */
	this.filterReset = function(element)
	{
		rbfilter.form.find(":input").not("input[type='hidden'],button").remove();
		rbfilter.form.append('<input type="hidden" name="resetf" value="resetf">');
		return rbfilter.form.submit();
	};
	
	
	this.buttonCheckbox = function(){
		$('.button-checkbox').each(function () {
			// Settings
			var $widget = $(this),
			$button = $widget.find('button'),
			$checkbox = $widget.find('input:checkbox'),
			color = $button.data('color'),
			settings = {
				on: {
					icon: 'glyphicon glyphicon-check'
				},
				off: {
					icon: 'glyphicon glyphicon-unchecked'
				}
			};

			// Event Handlers
			$button.on('click', function () {
				$checkbox.prop('checked', !$checkbox.is(':checked'));
				$checkbox.triggerHandler('change');
				updateDisplay();
			});
			$checkbox.on('change', function () {
				updateDisplay();
			});

			// Actions
			function updateDisplay() {
				var isChecked = $checkbox.is(':checked');

				// Set the button's state
				$button.data('state', (isChecked) ? "on" : "off");

				// Set the button's icon
				$button.find('.state-icon')
				.removeClass()
				.addClass('state-icon ' + settings[$button.data('state')].icon);

				// Update the button's color
				if (isChecked) {
					$button
					.removeClass('btn-default')
					.addClass('btn-' + color + ' active');
				}
				else {
					$button
					.removeClass('btn-' + color + ' active')
					.addClass('btn-default');
				}
			}

			// Initialization
			function init() {
				updateDisplay();

				// Inject the icon if applicable
				if ($button.find('.state-icon').length == 0) {
					$button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
				}
			}
			init();
		});
	};
	

	/*
	 * 
	 * 
	 */
	this.init = function()
	{
		this.buttonCheckbox();

		$('.resetOnClick').click(function(){
			rbfilter.form.find(":input").not("input[type='hidden'],button").not($(this)).val('');
		});
		
		$('.resetOnChange').change(function(){
			rbfilter.form.find(":input").not("input[type='hidden'],button").not('.selectpicker').not($(this)).val('');
			rbfilter.form.find(":input.selectpicker option:selected").prop("selected", false);
		});
		
		$('.submitOnClick').click(function(){
			return rbfilter.filterSubmit(this);
		});

		$('.submitOnChange').change(function(){
			return rbfilter.filterSubmit(this);
		});
		
		$('.optionSelector').click(function(e){
			return rbfilter.displayOption(this);
		});
		
		$('.btn-resetf').click(function(){
			return rbfilter.filterReset(this);
		});

		$.each($('.optionSelector:checked'), function( index, item ) {
			rbfilter.displayOption(item,0);
		});
	};
};
