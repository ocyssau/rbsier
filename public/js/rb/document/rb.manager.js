
/**
 * 
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDocumentManager(jqElement)
{
	this.jqElement = jqElement;
	this.service = new RbDocumentService();
	
	/* @var RbWildspaceButtons */
	this.rbButtons = null;
	
	/* @var RbDocumentManager */
	var manager = this;

	/**
	 * Get context menu
	 * @var jqParentElement jquery element with data definition for actions. Must have class rb-submit-data.
	 * @return RbWildspaceButtons
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbDocumentButtons( jqParentElement );
		rbButtons.dialogBox = this.dialogBox;
		return rbButtons;
	};
	
	/**
	 * 
	 */
	this.initPostit = function(actionProcessor)
	{
		/* init popover for postit */
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover();
		});

		/* to prevent return to page begin on click postit button : only for show popup */
		$("rb-document .btn-postit").click(function(e){
			e.preventDefault();
		});

		/**
		 * 
		 */
		$(".rb-document .deletepostit-btn").click(function(e){
			e.preventDefault();

			if(!confirm("Are you Sure?")){
				return false;
			}

			var url = document.baseurl+"/service/document/postit/delete";
			var id = $(this).parents(".rb-postit-item").data('id');

			var queryData = {
					postits:{
						postit1:{
							id:id,
						}
					}
			};
			var actionProcessor = new RbAction();
			actionProcessor.serviceRequest(e, $(this), url, {
				queryData: queryData,
				type: "POST",
				successCallback: function(e,jqBtn){
					var postit = jqBtn.parents('.rb-postit-item');
					postit.remove();
				}
			});
			return false;
		});

		$(".rb-document .postit-content").hide();
		$(".rb-document .postit-button").click(function(e){
			e.preventDefault();
			e.stopPropagation();
			var content = $($(this).parent(".postit")[0].children[1]);
			content.show();
			return false;
		});

		return this;
	};

	/**
	 * 
	 */
	this.initTags = function()
	{
		ranchbe.unactiveLoadWaiting();
		ranchbe.include('bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js');
		ranchbe.includeCss('bootstrap-tagsinput/dist/bootstrap-tagsinput.css');
		ranchbe.includeCss('js/jqcloud2/dist/jqcloud.min.css');
		//ranchbe.include('js/jqcloud2/dist/jqcloud.min.js');
		//ranchbe.activateLoadWaiting();

		/* Tags input */
		jqElement.find('.rb-tagsinput-ajax').each(function () {
			var input = $(this);
			input.tagsinput({
				maxTags: 10,
				maxChars: 20,
				trimValue: true,
				allowDuplicates: false
			});

			var callback = function(event){
				var item = event.item;
				var input = $(event.target);
				var tags = input.val();
				var submitDataDiv = input.parents('.rb-submit-data').first();
				if(submitDataDiv.length > 0){
					var id = submitDataDiv.data('id');
					var spacename = submitDataDiv.data('spacename');
				}
				else{
					throw ("rb-submit-data is not founded");
				}

				if(id==null){
					return false;
				}
				if(spacename==null){
					return false;
				}

				var url = ranchbe.options.baseurl+'/service/document/tags/update';
				var queryData = {
						documents:{
							document1:{
								id:id,
								spacename:spacename,
								tags:tags
							}
						}
				};

				$.ajax({
					type : 'POST',
					url : url,
					data : queryData,
					dataType: 'json',
					success : function(data, textStatus, jqXHR){
					},
					error : function(data, textStatus, jqXHR){
					}
				});
			};

			input.on('itemAdded', callback);
			input.on('itemRemoved', callback);
		});

		/* Tags input */
		jqElement.find('.rb-tagsinput').each(function () {
			var input = $(this);
			input.tagsinput({
				maxTags: 10,
				maxChars: 20,
				trimValue: true,
				allowDuplicates: false
			});
		});

		/* Tags input */
		jqElement.find('.rb-tagsinput-display').each(function () {
			var input = $(this);
			input.tagsinput();
			input.attr('disabled', 1);
			var div = input.prev();
			div.find("[data-role='remove']").hide();
		});

		return this;
	};

	/**
	 * 
	 */
	this.initDropzone = function()
	{
		$(".rb-document .dropchildrenzone").droppable({
			drop:function(e, ui){
				e.preventDefault();
				e.stopPropagation();

				var childId = $(ui.draggable).data("id");
				var childSpace = $(ui.draggable).data("spacename");
				var childNumber = $(ui.draggable).data("name");

				/* create ajax request for create relation */
				dataDiv = $(this).parent(".rb-submit-data").andSelf();
				var spacename = dataDiv.data("spacename");
				var documentId = dataDiv.data("id");
				var url= document.baseurl+"/service/document/relation/addchildren";
				var queryData = {
						documents:{
							document1:{
								id:documentId,
								spacename:spacename,
								children:{
									child1:{
										id:childId,
										spacename:childSpace
									}
								}
							}
						}
				}
				console.log(queryData);

				var actionProcessor = new RbAction();
				actionProcessor.serviceRequest(e, dataDiv, url, {
					queryData: queryData,
					type: "POST",
					successCallback: function(e,jqBtn){}
				});

				$(this).removeClass("dragOver");
				$(this).addClass("draggable");
			},
			over:function(e,ui){
				e.preventDefault();
				e.stopPropagation();

				$(this).removeClass("draggable");
				$(this).addClass("dragOver");
				return false;
			},
			out:function(e,ui){
				e.preventDefault();
				e.stopPropagation();

				$(this).removeClass("dragOver");
				$(this).addClass("draggable");
				return false;
			}
		});

		/*DRAG AND DROP*/
		$(".rb-document .draggable").draggable({
			revert:true,
			helper:"clone",
			scroll:false,
			iframeFix: true
		});

		return this;
	};
}
RbDocumentManager.prototype = new RbManager();

/**
 * @param jqParentElement
 */
function RbDocumentButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * @param array datas input datas as extracted from element 
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			if(item.name.substring(0,11) == 'spacenames['){
				name = 'spacename';
			}
			else{
				name = item.name;
			}
			
			switch(name){
				case 'checked[]':
				case 'documentid':
				case 'documentId':
				case 'id':
					if(url.search('%id%') > 0){
						url = url.replace('%id%', item.value);
					}
					else{
						item.name = 'checked[]';
						queryData.push(item);
					}
					break;
				case 'spacename':
					if(url.search('%spacename%') > 0){
						url = url.replace('%spacename%', item.value);
					}
					else{
						queryData.push(item);
					}
					break;
				default:
					queryData.push(item);
					break;
			}
		});
		
		if(url.search('/%id%') > 0){
			url = url.replace('/%id%', '');
		}
		if(url.search('/%spacename%') > 0){
			url = url.replace('/%spacename%', '');
		}
		return url;
	}
	
	/**/
	this.reload = function(){
		$("#content").load("#", function(event){
			ranchbe.initApp($("#content"));
		});
		return false;
	};

	/**
	 * Set events on buttons
	 * 
	 * @param actionProcessor RbAction
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;

		/*BUTTONS DEFINITIONS*/
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this documents?')){
				url = document.baseurl+'/ged/document/manager/%spacename%/delete';
				actionProcessor.uniformSelectionOnData('spacename');
				var queryData = [];
				url = rbButtons.buildRoute(actionProcessor.selectedDataToQueryData(actionProcessor.getSelectedData()), queryData, url);
				
				actionProcessor.getModal(e, $(this), url, {
					queryData: queryData,
					hash: null,
					callback: function(){
						rbForm = new RbForm();
						rbForm.reload = function(){
							rbButtons.reload();
						};
						rbForm.initInModal($("#confirmation"));
					}
				});
			}
			return false;
		});

		/**/
		parent.find(".editmulti-btn").click(function(e){
			url = document.baseurl+'/ged/document/manager/batch/editmulti';
			actionProcessor.uniformSelectionOnData('spacename');
			queryData = actionProcessor.selectedDataToQueryData(actionProcessor.getSelectedData());
			
			actionProcessor.getModal(e, $(this), url, {
				queryData: queryData,
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#documentEditmulti"));
				}
			});
			return false;
		});

		/**/
		parent.find(".edit-btn").click(function(e){
			url = document.baseurl+'/ged/document/manager/%spacename%/edit/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.selectedDataToQueryData(actionProcessor.getSelectedData()), queryData, url);
			
			actionProcessor.getModal(e, $(this), url, {
				queryData: queryData,
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#documentEdit"));
				}
			});
			return false;
		});
		
		/**/
		parent.find(".notification-btn").click(function(e){
			var url = document.baseurl+"/ged/document/notification/edit";
			actionProcessor.getModal(e, $(this), url, {
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#notificationEdit"));
				}
			});
			return false;
		});

		/**/
		parent.find(".marktosuppress-btn").click(function(e){
			var url = document.baseurl+'/service/document/lockmanager/marktosuppress';
			actionProcessor.toService(e, $(this), url, {
				type:"POST",
				successCallback: function(){
					rbButtons.reload();
				}
			});
			return false;
		});

		/**/
		parent.find(".unmarktosuppress-btn").click(function(e){
			var url = document.baseurl+'/service/document/lockmanager/unmarktosuppress';
			actionProcessor.toService(e, $(this), url, {
				type:"POST",
				successCallback: function(){
					rbButtons.reload();
				}
			});
			return false;
		});

		/**/
		parent.find(".checkout-btn").click(function(e){
			e.stopPropagation();
			e.preventDefault();

			var url = document.baseurl+'/ged/document/lockmanager/checkout';
			actionProcessor.getModal(e, $(this), url, {
				hash:null,
				type: 'GET',
				callback: function(respons, textStatus, jqXHR){
					if(textStatus == 'error'){
						rbButtons.reload();
						return false;
					}
					else{
						try{
							respons = JSON.parse(respons);
							if(respons.feedbacks[0].message.indexOf('checkout success') !== -1){
								$('#rb-popup-modal').modal('hide');
								rbButtons.reload();
							}
						}
						catch(e){
						}
					}
				}
			});
			return false;
		});

		/**
		 * Display checkin in modal from ajax request
		 * buttons must be redefined in callback of load().
		 * The scripts of checkin page will not be executed, so all js dependencies must be loaded in parent page.
		 */
		parent.find(".checkin-btn").click(function(e){
			var url = document.baseurl+'/ged/document/lockmanager/checkin?checkinandkeep=0';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				type: "GET",
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#checkinForm"));
				}
			});
			return false;
		});

		/**/
		parent.find(".checkinandkeep-btn").click(function(e){
			var url = document.baseurl+'/ged/document/lockmanager/checkin?checkinandkeep=1';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				type: "GET",
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#checkinForm"));
				}
			});
			return false;
		});

		/**/
		parent.find(".checkinreset-btn").click(function(e){
			var url = document.baseurl+'/service/document/lockmanager/reset';
			actionProcessor.toService(e, $(this), url, {type:"POST"});
			rbButtons.reload();
			return false;
		});
		
		/**/
		parent.find(".admin-checkinreset-btn").click(function(e){
			var url = document.baseurl+'/service/document/lockmanager/adminreset';
			actionProcessor.toService(e, $(this), url, {type:"POST"});
			rbButtons.reload();
			return false;
		});
		
		/**/
		parent.find(".readwithoption-btn").click(function(e){
			var url = document.baseurl+'/ged/document/read/index';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("form#readForm"));
				}
			});
			return false;
		});

		/**/
		parent.find(".putinws-btn").click(function(e){
			var url = document.baseurl+'/service/document/datamanager/putinws';
			actionProcessor.toService(e, $(this), url, {type:"GET"});
			return false;
		});
		
		/**/
		parent.find(".getpdf-btn").click(function(e){
			var url = document.baseurl+'/service/document/viewer/topdf';
			actionProcessor.getRequest(e, $(this), url, {type:"GET"});
			return false;
		});

		/**/
		parent.find(".download-btn").click(function(e){
			var url = document.baseurl+'/service/document/viewer/download';
			actionProcessor.getRequest(e, $(this), url, {type:"GET"});
			return false;
		});
		
		/**/
		parent.find(".downloadvisu-btn").click(function(e){
			var url = document.baseurl+'/service/document/viewer/downloadvisu';
			actionProcessor.getRequest(e, $(this), url, {type:"GET"});
			return false;
		});

		/**/
		parent.find(".move-btn").click(function(e){
			var url = document.baseurl+'/ged/document/datamanager/movedocument';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("form#moveDocument"));
				}
			});
			return false;
		});

		/**/
		parent.find(".copy-btn").click(function(e){
			var url = document.baseurl+'/ged/document/datamanager/copydocument';
			actionProcessor.getModal(e, $(this), url, {
				type:"POST",
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("form#copyDocument"));
				}
			});
			return false;
		});

		/**/
		parent.find(".newversion-btn").click(function(e){
			var url = document.baseurl+'/ged/document/version/newversion';
			actionProcessor.getModal(e, $(this), url, {
				type:"POST",
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("form#newversion"));
				}
			});
			return false;
		});

		/**/
		parent.find(".runworkflow-btn").click(function(e){
			var url = document.baseurl+'/docflow/index/index';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("form#docflowform"));
				}
			});
			return false;
		});

		/**/
		parent.find(".lock-btn").first().click(function(e){
			var url = document.baseurl+'/ged/document/lockmanager/lock';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("form#lockDocument"));
				}
			});
			return false;
		});

		/**/
		parent.find(".unlock-btn").click(function(e){
			//e.stopPropagation();
			var url = document.baseurl+'/service/document/lockmanager/unlock';
			actionProcessor.toService(e, $(this), url, {type:"POST"});
			rbButtons.reload();
			return false;
		});

		/**/
		parent.find(".resetdoctype-btn").click(function(e){
			var url = document.baseurl+'/service/document/doctype/reset';
			actionProcessor.toService(e, $(this), url, {type:"POST"});
			rbButtons.reload();
			return false;
		});

		/**/
		parent.find(".associatefile-btn").click(function(e){
			var url = document.baseurl+'/ged/document/datamanager/%spacename%/assocfile/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.selectedDataToQueryData(actionProcessor.getSelectedData()), queryData, url);
			actionProcessor.getModal(e, $(this), url, {
				queryData: queryData,
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.initInModal($("form#associateFile"));
				}
			});
			return false;
		});

		/**/
		parent.find(".associatevisu-btn").click(function(e){
			var url = document.baseurl+'/ged/document/viewer/assocvisu';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.initInModal($("form#associateFile"));
				}
			});
			return false;
		});

		/**/
		parent.find(".archive-btn").click(function(e){
			if(confirm('Do you want really archive this documents?')){
				var url = document.baseurl+'/ged/document/archiver/document';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});

		/**/
		parent.find(".getchildren-btn").click(function(e){
			var url = document.baseurl+'/ged/document/relation/children/%spacename%/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			return false;
		});

		/**/
		parent.find(".getparent-btn").click(function(e){
			var url = document.baseurl+'/ged/document/relation/parent/%spacename%/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			return false;
		});

		/**/
		parent.find(".getpdmchildren-btn").click(function(e){
			var url = document.baseurl+'/pdm/document/relation/children/%spacename%/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			return false;
		});

		/**/
		parent.find(".getpdmparent-btn").click(function(e){
			var url = document.baseurl+'/pdm/document/relation/parent/%spacename%/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			return false;
		});

		/**/
		parent.find(".getpdmexplorer-btn").click(function(e){
			var url = document.baseurl+'/pdm/document/initproduct/%spacename%/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			return false;
		});
		
		/**/
		parent.find(".initproduct-btn").click(function(e){
			var url = document.baseurl+'/service/pdm/documentversion/initproduct';
			actionProcessor.toService(e, $(this), url, {type:"GET"});
			return false;
		});

		/**/
		parent.find(".gethistory-btn").click(function(e){
			var url = document.baseurl+'/ged/document/history/%spacename%/index/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			return false;
		});
		
		/**/
		parent.find(".getdetail-btn").click(function(e){
			var url = document.baseurl+'/ged/document/detail/%spacename%/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			var dataDiv = $(this).closest(".rb-submit-data").first();
			var options = {
					popup:{
						target:"detail of document " + dataDiv.data("id"),
						ysize:600,
						xsize:1200
					},
					hash:"#documentdetail",
					queryData:queryData
			};
			actionProcessor.getRequest(e, $(this), url, options);
			return false;
		});

		/**/
		parent.find(".basketadd-btn").click(function(e){
			e.preventDefault();
			var url = document.baseurl+'/service/document/basket/add';
			actionProcessor.toService(e, $(this), url, {type:"POST"});
			return false;
		});

		/**/
		parent.find(".basketremove-btn").click(function(e){
			e.preventDefault();
			var url = document.baseurl+'/service/document/basket/remove';
			actionProcessor.toService(e, $(this), url, {type:"POST"});
			return false;
		});

		/*POSTITS*/
		/**/
		parent.find(".addpostit-btn").click(function(e){
			e.preventDefault();
			var dataDiv = $(this).closest(".rb-submit-data").first();

			var newComment = prompt("Input your message", "");
			if (newComment == null) {
				return false;
			}

			var queryData = {
					documents:{
						document1:{
							id:dataDiv.data('id'),
							spacename:dataDiv.data('spacename'),
							postit:newComment
						}
					}
			};

			var url = document.baseurl+'/service/document/postit/add';
			actionProcessor.toService(e,$(this),url,{
				type:"POST",
				successCallback : function(){
					rbButtons.reload();
				},
				queryData: queryData
			});
			return false;
		});

		/* TAGS */
		parent.find(".addtag-btn").click(function(e){
			e.preventDefault();
			var dataDiv = $(this).closest(".rb-submit-data").first();
			var spacename = dataDiv.data("spacename");
			var url = document.baseurl+'/service/document/tags/add';
			var clickedElement = $(this);
			
			if( $('#rb-tags-add-dialog').length ){
				var dialog = $('#rb-tags-add-dialog');
			}
			else{
				/* Init dialog box dom element */
				var dialog = $('<div id="rb-tags-add-dialog" title="Tag"></div>');
				dialog.append('<div><input type="text" name="tag" id="inputTag"></div>');
				$("#content").append(dialog);
				
				/* Init autocompletion */
				var cache = {};
				$('#inputTag').autocomplete({
					minLength: 2,
					source: function(request, respons){
						var term = request.term;
						if(term in cache){
							respons(cache[term]);
							return;
						}
						
						/* load tags */
						var url = ranchbe.options.baseurl+'/service/document/tags/search';
						
						$.ajax({
							type: 'GET',
							url: url,
							data : {
								spacename: spacename,
								term: term
							},
							dataType: 'json',
							success : function(data, textStatus, jqXHR){
								console.log(data);
								cache[term]=data.data;
								respons(data.data);
							},
							error : function(data, textStatus, jqXHR){
							}
						});
					}
				});
			}

			/* JqueryUi dialog box initialisation */
			dialog.dialog({
				height:"auto",
				width:"auto",
				show: {
					effect: "blind",
					duration: 500
				},
				hide: {
					effect: "blind",
					duration: 200
				},
				buttons:{
					"Save": function(event){
						tagString = $("#inputTag").val();
						queryData = actionProcessor.getQueryData(clickedElement);
						queryData = actionProcessor.service.queryDataToServiceData(queryData);
						$.each(queryData.documents, function(index,item){
							item.tag = tagString;
						});
						
						actionProcessor.toService(e, $(this), url, {
							hash: null,
							type: "POST",
							queryData: queryData,
							successCallback: function(respons, textStatus, jqXHR){
								rbButtons.reload();
								dialog.dialog("close");
							}
						});
					},
					"Cancel": function(event){
						$(this).dialog("close");
					},
				}
			});

			return false;
		});

		/*FIX DOCUMENTS*/
		parent.find(".fix-document-btn").click(function (e) {
			e.preventDefault();
			var url = document.baseurl+"/service/document/manager/fix";
			var id = $(this).data("id");
			var spacename = $(this).data("spacename");

			var queryData = {
					documents:{
						document1:{
							id:id,
							spacename:spacename
						}
					}
			};
			actionProcessor.toService(e, $(this), url, {
				queryData:queryData,
				type: 'POST',
				successCallback:function(e,domElemt){
					document.location.replace(document.location.href);
				}
			});
			return false;
		});

		/**
		 * 
		 */
		parent.find(".unfix-document-btn").click(function (e) {
			e.preventDefault();
			var url = document.baseurl+"/service/document/manager/unfix";
			var id = $(this).data("id");
			var spacename = $(this).data("spacename");

			var queryData = {
					documents:{
						document1:{
							id:id,
							spacename:spacename
						}
					}
			};
			actionProcessor.toService(e, $(this), url, {
				queryData:queryData,
				type: 'POST',
				successCallback:function(e,domElemt){
					document.location.replace(document.location.href);
				}
			});
			return false;
		});

		/**/
		parent.find(".getpermalink-btn").click(function(e){
			var url = document.baseurl+'/ged/document/share/getpermanentlink';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#permalinkForm"));
				}
			});
			return false;
		});
		
		/**/
		parent.find(".getpublicurl-btn").click(function(e){
			var url = document.baseurl+'/ged/document/share/publiclink';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#publicurlForm"));
				}
			});
			return false;
		});
		
		/**/
		parent.find(".document-share-btn").click(function(e){
			var url = document.baseurl+'/ged/document/share';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#shareDocumentForm"));
				}
			});
			return false;
		});
	}
};
RbDocumentButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDocumentService()
{
	
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
			if(item.name.substring(0,11) == "spacenames["){
				eval(item.name + "='" + item.value + "'");
			}
		});
		
		var i=1;
		var serviceData = {'documents':{}};
		$.each(checked, function(index, id){
			serviceData.documents['document'+i] = {
				id: id,
				spacename: spacenames[id]
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {documents:{
			document1:{
				id:elemt.data('id'),
				spacename:elemt.data('spacename')
			}
		}};
		return queryData;
	};
};
RbDocumentService.prototype = new RbService();


/**
 * 
 * @returns
 */
function RbTag()
{
	this.getTags = function(containerId, spacename, success, error)
	{
		var url = ranchbe.options.baseurl+'/service/document/tags/index';
		$.ajax({
			type: 'get',
			url: url,
			data : {
				containerId: containerId,
				spacename: spacename
			},
			dataType: 'json',
			success: success,
			error: error
		});
	}
}

//# sourceURL=/ranchbe/dynamic/rb/document/rb.manager.js
