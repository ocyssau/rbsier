
/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDocumentDetail(div)
{
	this.masterDiv = div;

	this.options = {
		baseurl: document.baseurl,
		documentId: div.data('id'),
		spacename: div.data('spacename'),
		uploadUrl:''
	};

	this.setOption = function(name,value)
	{
		eval("this.options."+name+"='"+value+"'");
		return this;
	};

	this.setOptions = function(options)
	{
		this.options = options;
		return this;
	};

	this.ping = function()
	{
		console.log("hello!");
	};
	
	/**
	 * 
	 */
	this.buttonTrigger = function(button)
	{
		var actionName = button.attr('value');
		return eval("this." + actionName + "Action(button)");
	};
	
	/**
	 * reload only th selected tab
	 */
	this.reload = function(tab)
	{
		/* inject div in context */
		var myDiv = this.masterDiv;

		var datas={
			documentid:this.options.documentId,
			spacename:this.options.spacename,
			tab:tab
		}

		$.ajax({
			type : 'GET',
			url : document.baseurl+"/ged/document/detail",
			data : datas,
			dataType: 'html',
			success : function(data, textStatus, jqXHR){
				var s = "#tab-"+tab;
				/* retrieve the right tab div from context div */
				$(myDiv).find(s).empty().append(data);
			},
			error : function(data, textStatus, jqXHR){
			}
		});
	};
	
	/**
	 * 
	 */
	this.refresh = function(tab)
	{
		document.location.hash = "#"+tab;
		document.location.reload();
	};
	
	/**
	 * To prevent the default submit action on the form
	 */
	this.preventForm = function(form)
	{
		console.log( "refine prevent default submit on "+form.id );
		form.unbind("submit");
		form.bind("submit", function(e){
			e.defaultPrevented();
			e.stopPropagation();
			console.log( "submit "+form.id );
			return false;
		});
		return form;
	};
	
	/**
	 * Call by fileReader, in the context of the fileReader
	 */
	this.uploadAndAssociateFile = function(event)
	{
		/*
		var splitresult = event.target.result.split(',');
		var mimestype = splitresult[0].split(';')[0];
		var encode = splitresult[0].split(';')[1];
		var data = splitresult[1];
		var filename = this.file.name;
		var spacename = this.rbContext.options.spacename;
		var documentId = this.rbContext.options.documentId;
		var params={};
		*/
		
		/* put rbContext in context */
		var data = this.result;
		var file = this.file;
		var params={
			documents:{
				document1:{
					id:this.documentId,
					spacename:this.spacename,
					files:{
						file1:{
							name:file.name,
							role:null,
							data:{
								name:file.name,
								size:file.size,
								mimestype:file.type,
								encode:'base64',
								md5:$.md5(data),
								data:data,
							}
						}
					}
				}
			}
		};
		
		console.log(params);
		var ajaxService = new RbAjax();
		ajaxService.serviceRequest(event, this, this.url, {
			type:'POST',
			queryData:params,
			successCallback:function(e,domElemt){
				//domElemt.refresh('files');
			},
			failedCallback:function(e,domElemt){},
		});
	};
}

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDocumentRelationButtons(jqParentElement, spaceName)
{
	
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * @param actionProcessor RbAction
	 */
	this.init = function(actionProcessor)
	{
		parent = this.jqElement;
		
		/*BUTTONS DEFINITIONS*/
		
		/**/
		parent.find(".refresh-btn").click(function(e){
			return false;
		});
		
		/**/
		parent.find(".child-add-btn").click(function(e){
			var url=document.baseurl+"/ged/document/relation/addchild";
			var successMsg="";
			var confirmMsg="";
			this.toService(button, url,successMsg,confirmMsg);
		});
		
		/**/
		parent.find(".child-delete-btn").click(function(e){
			var url=document.baseurl+"/ged/document/relation/deletechild";
			var successMsg="";
			var confirmMsg="";
			this.toService(button, url,successMsg,confirmMsg);
		});
		
		/**/
		parent.find(".getparent-btn").click(function(e){
			var url=document.baseurl+"/ged/document/relation/viewfatherdoc";
			var successMsg="";
			var confirmMsg="";
			this.toService(button, url,successMsg,confirmMsg);
		});

		/**/
		parent.find(".assocfile-btn").click(function(e){
		});
		
		/***************************** Document relations ****************************************/
		parent.find(".rel-delete-btn").click(function(e){
			var url = document.baseurl+"/service/document/relation/removelink";
			var service = new RbDocumentService();
			serviceRequest(e, $(this), url, service.serializeForm($(this).parents("form").first()), function(e,btn){
				document.location.hash = "#tab-children";
				document.location.reload(true);
			});
			return false;
		});

		parent.find(".rel-getdetail-btn").click(function(e){
			var spacename = $(this).data("spacename");
			var documentId = $(this).data("documentid");
			var url = document.baseurl+"/ged/document/detail/"+spacename+"/"+documentId;
			return false;
		});
	}
};


//# sourceURL=/ranchbe/dynamic/rb/document/rb.detail.js

