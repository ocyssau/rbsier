/**
 * 
 */
function RbUserManager(jqElement)
{
	this.service = new RbUserService();
	this.jqElement = jqElement;

	/* @var RbUserButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbUserButtons(jqParentElement);
		return rbButtons;
	};
}
RbUserManager.prototype = new RbManager();

/**
 * 
 */
function RbUserButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/acl/user/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#userEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/acl/user/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: " .container",
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#userEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this user?')){
				var url = document.baseurl+'/acl/user/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		/* init actions buttons */
		parent.find(".init-wildspace-btn").click(function(e){
			var url = document.baseurl+'/workplace/wildspace/init';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".unactivate-btn").click(function(e){
			var url = document.baseurl+'/acl/user/unactivate';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".activate-btn").click(function(e){
			var url = document.baseurl+'/acl/user/activate';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".password-btn").click(function(e){
			var url = document.baseurl+'/acl/user/setpassword';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.initInModal($("#passwordEdit"));
				}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".assignrole-btn").click(function(e){
			var url = document.baseurl+'/acl/user/assignrole';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.initInModal($("#assignroleEdit"));
				}
			});
			return false;
		});
	};
}
RbUserButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbUserService()
{

	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'users':{}};
		$.each(checked, function(index, id){
			serviceData.categories['user'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {users:{
			user1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbUserService.prototype = new RbService();
