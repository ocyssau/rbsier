/**
 * 
 */
function RbRoleManager(jqElement)
{
	this.service = new RbRoleService();
	this.jqElement = jqElement;
	
	/* @var RbRoleButtons */
	this.rbButtons = null;
	
	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbRoleButtons(jqParentElement);
		return rbButtons;
	};
	
}
RbRoleManager.prototype = new RbManager();

/**
 * 
 */
function RbRoleButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		ranchbe.initPaginator($("form#paginator"));
		
		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/acl/role/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#roleEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/acl/role/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: " .container",
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#roleEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this role?')){
				var url = document.baseurl+'/acl/role/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		
		/* init actions buttons */
		parent.find(".getusers-btn").click(function(e){
			var url = document.baseurl+'/acl/role/getusers';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".permission-btn").click(function(e){
			var url = document.baseurl+'/acl/permission/assigntorole';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
	};
}
RbRoleButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbRoleService()
{

	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'roles':{}};
		$.each(checked, function(index, id){
			serviceData.categories['role'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {roles:{
			role1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbRoleService.prototype = new RbService();

