/**
 * 
 */
function RbUserRoleManager(jqElement)
{
	this.service = new RbUserRoleService();
	this.jqElement = jqElement;

	/* @var RbCategoryButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbUserRoleButtons(jqParentElement);
		return rbButtons;
	};
}
RbUserRoleManager.prototype = new RbManager();

/**
 * 
 */
function RbUserRoleButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		ranchbe.initPaginator($("form#paginator"));
		
		/* init actions buttons */
		parent.find(".remove-btn").click(function(e){
			if(confirm('Do you want really remove this role for user?')){
				var url = document.baseurl+'/acl/user/unassignrole';
				var submitDatas = $(this).parents(".rb-submit-data").first();
				userId = submitDatas.data('userid');
				roleId = submitDatas.data('roleid');
				var options = {
						queryData:{
							userid:userId,
							roleid:roleId
						},
						successCallback: function(e,domElemt){
							location.reload();
						},
						failedCallback: function(e,domElemt){
							location.reload();
						},
				};
				actionProcessor.toService(e, $(this), url, options);
			}
			return false;
		});
	};
}
RbUserRoleButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbUserRoleService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'roles':{}};
		$.each(checked, function(index, id){
			serviceData.categories['role'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {roles:{
			role1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbUserRoleService.prototype = new RbService();
