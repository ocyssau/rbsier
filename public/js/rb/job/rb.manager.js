/**
 * 
 */
function RbJobManager(jqElement)
{
	this.service = new RbJobService();
	this.jqElement = jqElement;

	/* @var RbJobButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbJobButtons(jqParentElement);
		return rbButtons;
	};
}
RbJobManager.prototype = new RbManager();

/**
 * 
 */
function RbJobButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* create */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/batch/job/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#jobEdit"));
				}
			});
			return false;
		});
		
		/* edit */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/batch/job/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#jobEditorForm"));
				}
			});
			return false;
		});
		
		/* reset */
		parent.find(".reset-btn").click(function(e){
			if(confirm('Do you want really re-init this jobs?')){
				var url = document.baseurl+'/batch/job/reset';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		/* delete */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this job?')){
				var url = document.baseurl+'/batch/job/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		/* kill */
		parent.find(".kill-btn").click(function(e){
			if(confirm('Do you want really kill this job?')){
				var url = document.baseurl+'/batch/job/kill';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		
		/* getlog-btn */
		parent.find(".getlog-btn").click(function(e){
			var url = document.baseurl+'/batch/job/getlog';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
	};
}
RbJobButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbJobService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'jobs':{}};
		$.each(checked, function(index, id){
			serviceData.jobs['job'+i] = {
				name: id,
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {jobs:{
			job1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbJobService.prototype = new RbService();

