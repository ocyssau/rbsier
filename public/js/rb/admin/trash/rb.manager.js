/**
 * 
 */
function RbTrashManager(jqElement)
{
	this.service = new RbTrashService();
	this.jqElement = jqElement;

	/* @var RbTrashButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbTrashButtons(jqParentElement);
		return rbButtons;
	};
}
RbTrashManager.prototype = new RbManager();

/**
 * 
 */
function RbTrashButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);

	/**/
	this.reload = function(){
		$("#content").load("#", function(){});
		return false;
	};
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/**/
		parent.find(".emptytrash-btn").click(function(e){
			var url = document.baseurl+'/admin/trash/vacumm';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".download-btn").click(function(e){
			var url = document.baseurl+'/admin/trash/download';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/**/
		parent.find(".delete-btn").click(function(e){
			if(confirm('Delete file. Are you sure?')){
				var url = document.baseurl+'/service/trash/file/delete';
				actionProcessor.toService(e, $(this), url);
				rbButtons.reload();
			}
			return false;
		});
		
	};
}
RbTrashButtons.prototype = new RbButtons();


/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbTrashService()
{

	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'files':{}};
		$.each(checked, function(index, name){
			serviceData.files['file'+i] = {
				name: name
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-file AS file name,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {files:{
			file1:{
				name:elemt.data('file'),
			}
		}};
		return queryData;
	};
}
RbTrashService.prototype = new RbService();



//# sourceURL=/ranchbe/dynamic/rb/trash/rb.manager.js
