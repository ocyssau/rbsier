/**
 * 
 */
function RbAdminToolManager(jqElement)
{
	this.service = null;
	this.jqElement = jqElement;
}
RbAdminToolManager.prototype = new RbManager();

/**
 * 
 */
function RbAdminToolButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* init actions buttons */
		parent.find(".maintenance-mode-btn").click(function(e){
			var url = document.baseurl+'/admin/maintenance/mode';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".getfilesrecordwithoutfile-btn").click(function(e){
			var url = document.baseurl+'/admin/tools/getfilesrecordwithoutfile';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".getfileswithoutdocument-btn").click(function(e){
			var url = document.baseurl+'/admin/tools/getfileswithoutdocument';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".getdocumentswithoutcontainer-btn").click(function(e){
			var url = document.baseurl+'/admin/tools/getdocumentswithoutcontainer';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".getmissingfiles-btn").click(function(e){
			var url = document.baseurl+'/admin/tools/getmissingfiles';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".updatemissingfiles-btn").click(function(e){
			var url = document.baseurl+'/admin/tools/updatemissingfiles';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".getfilesintrash-btn").click(function(e){
			var url = document.baseurl+'/admin/trash/index';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/**/
		parent.find(".emptytrash-btn").click(function(e){
			var url = document.baseurl+'/admin/trash/vacumm';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".compileicon-btn").click(function(e){
			var url = document.baseurl+'/ged/doctype/buildicons';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".dbbackupsql-btn").click(function(e){
			var url = document.baseurl+'/admin/dbbackup/sql';
			actionProcessor.getRequest(e, $(this), url, {
				requestData:{type:'sql'}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".dbbackupxml-btn").click(function(e){
			var url = document.baseurl+'/admin/dbbackup/xml';
			actionProcessor.getRequest(e, $(this), url, {
				requestData:{type:'xml'}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".sqlrequests-btn").click(function(e){
			var url = document.baseurl+'/application/sqlrequest/index';
			actionProcessor.getRequest(e, $(this), url, {
			});
			return false;
		});
	};
}
RbAdminToolButtons.prototype = new RbButtons();

