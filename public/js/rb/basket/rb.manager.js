/**
 * @param jqParentElement
 */
function RbBasketButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * Set events on buttons
	 * 
	 * @param actionProcessor RbAction
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;

		/**/
		parent.find(".basketclean-btn").click(function(e){
			e.preventDefault();
			var url = document.baseurl+'/service/document/basket/clean';
			actionProcessor.toService(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".getbasket-btn").click(function(e){
			var url = document.baseurl+'/ged/document/basket';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
	}
};
RbDocumentButtons.prototype = new RbButtons();

