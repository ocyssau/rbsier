
/**
 * 
 */
function RbPortailManager(jqElement)
{
	this.service = new RbPortailService();
	this.jqElement = jqElement;

	/* @var RbPortailButtons */
	this.rbButtons = null;
	
	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbPortailButtons(jqParentElement);
		return rbButtons;
	};
}
RbPortailManager.prototype = new RbManager();

/**
 * 
 */
function RbPortailButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);

	
	/**
	 * @param array datas input datas as extracted from element 
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			name = item.name;
			switch(name){
				case 'checked[]':
				case 'portailid':
				case 'portailId':
				case 'id':
					if(url.search('%id%') > 0){
						url = url.replace('%id%', item.value);
					}
					else{
						item.name = 'checked[]';
						queryData.push(item);
					}
					break;
				case 'url':
					if(url.search('%url%') > 0){
						url = url.replace('%url%', item.value);
					}
					break;
				default:
					queryData.push(item);
					break;
			}
		});
		
		if(url.search('/%id%') > 0){
			url = url.replace('/%id%', '');
		}
		if(url.search('/%url%') > 0){
			url = url.replace('/%url%', '');
		}
		return url;
	}
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;

		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl + '/portail/project/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#portailEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl + '/portail/project/edit/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.selectedDataToQueryData(actionProcessor.getSelectedData()), queryData, url);
			actionProcessor.getModal(e, $(this), url, {
				queryData: queryData,
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#portailEdit"));
				}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really delete this portail ?')){
				var url = document.baseurl+'/portail/project/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		/* init actions buttons */
		parent.find(".build-btn").click(function(e){
			var url = document.baseurl + '/portail/project/build/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.selectedDataToQueryData(actionProcessor.getSelectedData()), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {
				queryData: queryData
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".getpublic-btn").click(function(e){
			var url = document.baseurl + '/portail/%url%';
			datas = actionProcessor.getSelectedData();
			url = url.replace('%url%', datas[0].url);
			
			actionProcessor.getRequest(e, $(this), url, {
				queryData: []
			});
			return false;
		});
	};
}
RbPortailButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbPortailService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'portails':{}};
		$.each(checked, function(index, id){
			serviceData.portails['portail'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {portails:{
			portail1:{
				id:elemt.data('id')
			}
		}};
		return queryData;
	};
	
}
RbPortailService.prototype = new RbService();
