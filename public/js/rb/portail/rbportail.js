function RbPortail(){
	this.componentGetterBaseUrl = "";

	/**
	 * 
	 */
	this.editSelectList = function(selectElemt){
		dialog = $("#editListDialog");
		options = selectElemt.find('option');
		ul = $('<ul></ul>');

		minusButton = $('<button></button>');
		minusButton.attr('title', 'Remove');
		minusButton.attr('data-toggle', 'tooltip');
		minusButton.addClass("btn btn-danger btn-xs remove-btn");
		minusButton.css("margin-left", "10px");
		minusButton.css("margin-right", "10px");
		minusButton.append('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>');
		minusButton.click(removeFromList);

		plusButton = $('<button></button>');
		plusButton.attr('title', 'Add');
		plusButton.attr('data-toggle', 'tooltip');
		plusButton.addClass("btn btn-primary btn-xs add-btn");
		plusButton.append('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>');
		plusButton.click(addToList);

		inputElmt = $('<input type="text" />');

		liPrototype = $('<li></li>');
		liPrototype.append(minusButton.clone(true)); //clone with events

		for (i = 0; i < options.length; ++i) {
			value = $(options[i]).val();
			li = liPrototype.clone(true,true);
			li.append(value);
			ul.append(li);
		}

		modalBody = dialog.find(".modal-body").first();
		modalBody.empty();
		modalBody.append(ul);
		modalBody.append(plusButton);
		modalBody.append(inputElmt);
		dialog.modal('show');

		function removeFromList(){
			value = $(this).parents("li").last().text();
			$(this).parents("li").last().remove(); //remove line in list editor
			selectElemt.find('option[value="'+value+'"]').last().remove(); //remove value from select
		}

		function addToList(){
			value = inputElmt.val().trim();
			if(value==""){
				value="NEW";
			}
			modalBody.find("ul").first().append(liPrototype.clone(true,true).append(value)); //add to editor
			selectElemt.append('<option value="'+value+'">'+value+'</option>'); //add to select
		}
	};

	/**
	 * 
	 */
	this.gotoUrl = function($url)
	{
		window.open($url,"_self");
	};

	/**
	 * 
	 */
	this.openModalbox = function(url,requestOptions,title)
	{
		$.ajax({
			type: 'get',
			url:url,
			dataType: 'html',
			data: requestOptions,
			error: function(jqXHR, textStatus, errorThrown){
				alert(textStatus + ': ' + errorThrown);
			},
			success: function(html, textStatus, jqXHR){
				dialog = $('#modalBox');
				dialog.find('.modal-title').text(title);
				dialog.find('.modal-body').empty();
				dialog.find('.modal-body').append(html);
				dialog.find('.modal-footer>p').text('');
				dialog.modal({show:true});
			}
		});
	};

	/**
	 * 
	 */
	this.setComponentGetterBaseUrl = function(url){
		this.componentGetterBaseUrl = url.replace(/\/$/, "");
	};

	/**
	 * get component from server and add to parent.
	 * the base getter url must set before with setComponentGetterUrl method
	 */
	this.addComponent = function(type,parentselector)
	{
		data = {
				parentid:$(parentselector).parents('div.rbp-component').attr('rbp-id'),
				parentuid:$(parentselector).parents('div.rbp-component').attr('id'),
				save:true
		};

		myGetterUrl = this.componentGetterBaseUrl+"/get"+type+"/";
		$.ajax({
			type: 'get',
			async: false,
			url: myGetterUrl,
			data: data,
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error durind add component web request: ' + textStatus);
			},
			success: function(data, textStatus, jqXHR){
				$(parentselector).find(".rbp-children").first().append(data);
			}
		});
	};

	/**
	 */
	this.delFile = function(componentId, fileId, button)
	{
		data = {
				componentid:componentId,
				fileid:fileId
		};

		myGetterUrl = this.componentGetterBaseUrl+"/delfile";
		$.ajax({
			type: 'get',
			url: myGetterUrl,
			data: data,
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error durind deletion of file: ' + textStatus);
			},
			success: function(data, textStatus, jqXHR){
				//reload current component
				comptype = 'filelist';
				url = $.rbportail.componentGetterBaseUrl+"/get"+comptype+"/"+componentId;
				$.get( url, function( data ) {
					$(button).parents('div.rbp-component').first().parent().html( data );
				});
			}
		});
	};

	/**
	 */
	this.getFile = function(componentId, fileId)
	{
		myGetterUrl = this.componentGetterBaseUrl+"/getfile?componentid="+componentId+"&fileid="+fileId;
		window.location.href=myGetterUrl;
	};

	/**
	 * get component from server and add to parent.
	 * the base getter url must set before with setComponentGetterUrl method
	 */
	this.saveComponent = function(type,properties,component)
	{
		parentComponent = component.parents('.rbp-component').first();
		myGetterUrl = this.componentGetterBaseUrl+"/save"+type+"/";
		title = component.find('.rbp-component-title').first().text();
		isShow = component.find('.panel-collapse').first().hasClass('in');

		properties.index = component.data('rbp-index');
		properties.uid = component.attr('id');
		properties.id = component.data('rbp-id');
		properties.parentid = parentComponent.data('rbp-id');
		properties.parentuid = parentComponent.attr('id');
		properties.visibility = component.data('rbp-visibility-level');
		properties.title = title;
		properties.xoffset = '';
		properties.yoffset = '';
		properties.width = '';
		properties.height = '';
		properties.collapsed = !isShow;

		$.ajax({
			type: "post",
			url: myGetterUrl,
			data: properties,
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error during saving of ' + properties.id + '. Erro message: '+ errorThrown);
			},
			success: function(data, textStatus, jqXHR){
				respons = JSON.parse(data);

				id = respons.id;
				component.attr('rbp-id', id);

				updateBy = respons.updateById;
				updated = respons.updated.date;

				updateLabel = 'Updated by '+updateBy+' at '+updated;
				component.find('.rbp-update-label').first().text(updateLabel);
			}
		});
		return false;
	};

	/**
	 * 
	 */
	this.saveBox = function(component)
	{
		/* get properties: */
		body = component.find('.rbp-component-body').first().text();

		properties = {
				'class' : 'box',
				'name' : 'BOX' + component.attr('id'),
				'body' : body
		};
		return $.rbportail.saveComponent('box', properties, component);
	};

	/**
	 * 
	 */
	this.saveTab = function(component)
	{
		label = component.find('.rbp-component-label').first().text();
		notice = component.find('.rbp-component-notice').first().text();

		properties = {
				'class' : 'tab',
				'name' : 'TAB' + component.attr('id'),
				'body' : '',
				'label' : label,
				'notice' : notice
		};
		return $.rbportail.saveComponent('tab', properties, component);
	};

	/**
	 * 
	 */
	this.saveIndicator = function(component)
	{
		//get properties: 
		label = component.find('.rbp-component-label').first().text();
		notice = component.find('.rbp-component-notice').first().text();
		value = component.find('.rbp-indicator-value').first().attr( "aria-valuenow");
		properties={
				'class':'indicator',
				'name':'INDICATOR'+component.attr('id'),
				'body':'',
				'label':label,
				'notice':notice,
				'value':value,
		};
		return $.rbportail.saveComponent('indicator', properties, component);
	};

	/**
	 * 
	 */
	this.saveJalon = function(component)
	{
		label = component.find('.rbp-component-label').first().text();
		status = component.find('.rbp-component-status').first().text();
		notice = component.find('.rbp-component-notice').first().text();
		targetdate = component.find('.rbp-status-targetdate').first().val();
		selectOptions = component.find('.rbp-status-selector').first().find('option');
		statusOptions = [];

		for (i = 0; i < selectOptions.length; ++i) {
			value = $(selectOptions[i]).val();
			statusOptions[i] = value;
		}

		properties = {
				'class':'jalon',
				'name':'JALON'+component.attr('id'),
				'body':'',
				'label':label,
				'status':status,
				'notice':notice,
				'statuslist':JSON.stringify(statusOptions),
				'targetdate':targetdate
		};
		return $.rbportail.saveComponent('jalon', properties, component);
	};

	/**
	 * 
	 */
	this.saveStatusbox = function(component)
	{
		//get properties: 
		label = component.find('.rbp-component-label').first().text();
		status = component.find('.rbp-component-status').first().text();
		notice = component.find('.rbp-component-notice').first().text();
		selectOptions = component.find('.rbp-status-selector').first().find('option');
		statusOptions = [];

		for (i = 0; i < selectOptions.length; ++i) {
			value = $(selectOptions[i]).val();
			statusOptions[i] = value;
		}

		properties={
				'class':'statusbox',
				'name':'STATUS'+component.attr('id'),
				'body':'',
				'label':label,
				'status':status,
				'notice':notice,
				'statuslist':JSON.stringify(statusOptions)
		};
		return $.rbportail.saveComponent('statusbox', properties, component);
	};

	/**
	 * 
	 */
	this.saveTinymce = function(component)
	{
		tinymce.activeEditor.save();
		body = tinymce.activeEditor.getContent();

		//get properties: 
		properties={
				'class':'tinymce',
				'name':'TINYMCE'+component.attr('id'),
				'body':body
		};
		return $.rbportail.saveComponent('tinymce', properties, component);
	};

	/**
	 * 
	 */
	this.saveCheckbox = function(component)
	{
		//get properties: 
		notice = component.find('.rbp-component-notice').first().text();
		liItems = component.find('.rbp-checkbox-items').first().find('li');
		items = [];

		for (i = 0; i < liItems.length; ++i) {
			items[i] = {};
			items[i].name = $(liItems[i]).find(".rbp-item-name").first().text();
			state = $(liItems[i]).find(".rbp-item-cb").first().prop('checked');
			items[i].checked = state;
			items[i].notice = $(liItems[i]).find(".rbp-item-notice").first().text();
		}

		properties={
				'class':'checkbox',
				'name':'CHECKBOX'+component.attr('id'),
				'body':'',
				'notice':notice,
				'items':JSON.stringify(items)
		};
		return $.rbportail.saveComponent('checkbox', properties, component);
	};

	/**
	 * 
	 */
	this.saveFilelist = function(component)
	{
		/* get properties: */ 
		notice = component.find('.rbp-component-notice').first().text();
		liItems = component.find('tr.rbp-filelist-item');
		items = [];

		for (i = 0; i < liItems.length; ++i) {
			items[i] = {};
			items[i].id = $(liItems[i]).attr('id');
			items[i].name = $(liItems[i]).find("p.rbp-listitem-name").first().text();
			items[i].size = $(liItems[i]).find("p.rbp-listitem-size").first().text();
			items[i].mtime = $(liItems[i]).find("p.rbp-listitem-mtime").first().text();
		}

		properties={
				'class':'filelist',
				'name':'FILELIST'+component.attr('id'),
				'body':'',
				'notice':notice,
				'items':JSON.stringify(items)
		};
		return $.rbportail.saveComponent('filelist', properties, component);
	};

	/**
	 * 
	 */
	this.saveDocumentlist = function(component)
	{
		/**/
		notice = component.find('.rbp-component-notice').first().text();

		/* save filter form */
		filterform = component.find('select.rb-select-filterform').first().val();

		/* save actions form */
		actions = component.find('select.rb-select-action').first().val();

		/* save template form */
		userTemplate = component.find('select.rb-select-template').first().val();

		/* save rb-user-config form */
		userConfigElemt = component.find('textarea.rb-user-config').first();
		editor = userConfigElemt.data('jsonEditor');
		json = editor.get();
		userConfigElemt.val(JSON.stringify(json, null, 2));
		userConfig = userConfigElemt.val();

		/* save columns */
		liItems = component.find('tr.header-selector-item');
		headers = [];
		for (i = 0; i < liItems.length; ++i) {
			headers[i] = {};
			headers[i].column = $(liItems[i]).find('select.rb-select-column').val();
			headers[i].label = $(liItems[i]).find('input.rb-input-label').val();
		}

		properties = {
				'class':'documentlist',
				'name':'DOCLIST'+component.attr('id'),
				'body':'',
				'notice':notice,
				'headers':JSON.stringify(headers),
				'filterform': filterform,
				'template': userTemplate,
				'actions': actions,
				'config': userConfig,
		};

		return $.rbportail.saveComponent('documentlist', properties, component);
	};

	/**
	 * 
	 */
	this.saveTabpanel = function(component)
	{
		//get properties: 
		TabsItems = component.find('.rbp-tabpanel-items').first().find('li');

		//get properties of each tab
		for (i = 0; i < TabsItems.length; ++i) {
			index = i;
			notice = $(TabsItems[i]).find(".rbp-item-notice").first().attr('data-content');
			label = $(TabsItems[i]).find(".rbp-item-label").first().text();

			id = $(TabsItems[i]).find("a[role='tab']").first().attr('href');

			//write properties in tab object
			content = $(id);
			content.find('.rbp-component-label').first().text(label);
			content.find('.rbp-component-notice').first().text(notice);
			content.attr('rbp-index', index);
		}

		properties={
				'class':'tabpanel',
				'name':'TABPANEL'+component.attr('id'),
				'body':'',
		};

		return $.rbportail.saveComponent('tabpanel', properties, component);
	};

	/**
	 * 
	 */
	this.saveSimplegrid = function(component)
	{
		//get properties: 
		notice = component.find('.rbp-component-notice').first().text();
		liItems = component.find('.rbp-checkbox-items').first().find('li');
		items = [];

		for (i = 0; i < liItems.length; ++i) {
			items[i] = {};
			items[i].name = $(liItems[i]).find(".rbp-item-name").first().text();
			state = $(liItems[i]).find(".rbp-item-cb").first().prop('checked');
			items[i].checked = state;
			items[i].notice = $(liItems[i]).find(".rbp-item-notice").first().text();
		}

		properties={
				'class':'checkbox',
				'name':'CHECKBOX'+component.attr('id'),
				'body':'',
				'notice':notice,
				'items':JSON.stringify(items)
		};
		return $.rbportail.saveComponent('simplegrid', properties, component);
	};


	/**
	 * get component from server and add to parent.
	 * the base getter url must set before with setComponentGetterUrl method
	 */
	this.saveProject = function(component)
	{
		children = component.find(".rbp-component");
		$.each(children, function(index, child){
			childComponent = $(child).first();
			childComponent.attr('rbp-index', index);
			childComponent.trigger("save");
		});
		alert('Great success on save!');
	};

	/**
	 * 
	 */
	this.removeComponent = function(componentselector)
	{
		ok = confirm('Are you sure that you want to delete this component and all his children ?');
		if(ok != true) return;

		component = $(componentselector).first();
		uid = component.attr('id');
		id = component.attr('rbp-id');
		cssClass = component.attr('class');
		myGetterUrl = this.componentGetterBaseUrl+"/delete/";
		params = {};
		params.id = id;
		params.uid = uid;
		params.cssclass = cssClass;

		if(id!=""){
			$.ajax({
				type: "GET",
				url:myGetterUrl,
				data: params,
				error: function(jqXHR, textStatus, errorThrown){
					alert('msg_error_notification');
				},
				success: function(data, textStatus, jqXHR){
					component.remove();
				}
			});
		}
		else{
			component.remove();
		}
	};

	/**
	 * 
	 */
	this.removeTab = function(domElement)
	{
		ok = confirm("Do you really want to suppress this tab and his content?");

		if(ok==true){
			id = $(domElement).parent().children('a').attr('aria-controls'); //get id from li.a attrbute

			//remove tab box component from dom and db
			this.removeComponent('#'+id);
			//$(domElement).parents("div[role='tabpanel']").find("#"+id).first().remove(); //go to tabpanel and find tab content element to remove

			$(domElement).parent().remove(); //remove li element
		}
		return false;
	};

	/**
	 * 
	 */
	this.addTab = function(parentselector,label, notice)
	{
		removeButtonProto = '<button type="button" class="btn btn-danger delete-btn btn-xs removetab-btn" data-toggle="tooltip"';
		removeButtonProto += 'onclick="return rbportail.removeTab(this);"';
		removeButtonProto += 'title="Delete">';
		removeButtonProto += '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
		removeButtonProto += '</button>';

		navtab = $(parentselector).find(".nav-tabs").first(); //the tab element
		tabContent = $(parentselector).find(".tab-content").first(); //the content element

		popoverTemplate = '<div data-toggle="popover" data-placement="top" class="rbp-item-notice" data-content="{msg}"><p class="editableone rbp-item-label">{label}</p></div>';
		tabTemplate = '<li role="presentation"><a href="#{href}" aria-controls="{href}" role="tab" data-toggle="tab">'+popoverTemplate+'</a></li>';

		//get and add a box to tab content
		bodyselector = '#'+$(parentselector).find(".panel-body").attr('id');
		$.rbportail.addComponent('tab', bodyselector);

		//get id of the last inserted component
		id = tabContent.find(".rbp-component").last().attr('id');

		//create tab for last component
		if(label == ""){
			label=id;
		}
		li = $( tabTemplate.replace(/(\{href\})/g, id).replace(/(\{label\})/g, label).replace(/(\{msg\})/g, notice) ).append($(removeButtonProto).clone(true,true));
		navtab.append(li);

		$('[data-toggle="popover"]').popover({trigger:'hover'});

		//show the last tab added
		navtab.find('a:last').tab('show');

		//save tabpanel after insert a new tab
		$(parentselector).parents('.rbp-tabpanel').first().trigger('save');
		return false;
	};

	/**
	 * 
	 */
	this.initPublic = function()
	{
	}

	/**
	 * 
	 */
	this.init = function()
	{
		//editable field on one line
		$(".editableone").editInPlace({
			callback: function(unused, enteredText) {
				enteredText=$.trim(enteredText);
				if( enteredText==""){
					return "{empty}";
				}
				else{
					return enteredText;
				}
			},
			bg_over: "#ccc",
		});

		//editable field 3 lines/15cols
		$(".editable").editInPlace({
			callback: function(unused, enteredText) {
				enteredText=$.trim(enteredText);
				if( enteredText==""){
					return "{empty}";
				}
				else{
					return enteredText;
				}
			},
			bg_over: "#cff",
			field_type: "textarea",
			textarea_rows: "3",
			textarea_cols: "15"
		});

		$(".draggable").draggable({
			containment: "parent",
			grid: [ 20, 20 ]
		});

		$(".resizablex").resizable({
			grid: [ 20, 1000000 ],
			minWidth: 500
		});

		$(".resizabley").resizable({
			grid: [ 1000000, 20 ],
			minWidth: 500
		});

		$(".resizable").resizable({
			grid: [ 10, 10 ],
			minWidth: 200,
			minHeight: 60
		});

		$('[data-toggle="popover"]').popover({
			trigger:'hover'
		});

		$( ".rbp-sortable" ).sortable({
			handle: ".panel-heading",
			connectWith:".rbp-children"
		});
	};

	/**
	 * 
	 */
	this.setModeConsult = function()
	{
		$(".panel-concept-btn").remove();
	};

	/**
	 * 
	 */
	this.switchPreviewMode = function(active)
	{
		//alert(document.previewmode);
		if(document.previewmode==0 || typeof(document.previewmode) == "undefined"){
			$(".rbp-conception-mode-btn").not(".rbp-preview-mode-btn").hide();
			$(".btn-addcomponent, .save-btn, .delete-btn, .rbp-indicator-slider, .rbp-status-selector, .edit-btn, .delete-file-btn, .replace-file-btn, .add-btn").hide();
			$("p.editable:contains('{empty}')").hide(); //hide empty text
			$("p.editableone:contains('{empty}')").hide(); //hide empty text
			$(".datepicker").prop('disabled', true); //block date picker
			document.previewmode='1';
		}
		else if(document.previewmode==1){
			$(".rbp-conception-mode-btn").show();
			$(".btn-addcomponent, .save-btn, .delete-btn, .rbp-indicator-slider, .rbp-status-selector, .edit-btn, .delete-file-btn, .replace-file-btn, .add-btn").show();
			$("p.editable:contains('{empty}')").show();
			$("p.editableone:contains('{empty}')").show();
			$(".datepicker").prop('disabled', false);
			document.previewmode='0';
		}
	};

	/**
	 * 
	 */
	this.setVisibility = function(level, button)
	{
		component = $(button).parents(".rbp-component").first();
		component.attr('rbp-visibility-level', level);
		//remove activate state
		$(button).parents('ul').first().find('p.bg-primary').removeClass('bg-primary');
		//and activate selected status
		$(button).children('p').addClass('bg-primary');
		//alert(component.attr('id'));
		//alert(component.attr('rbp-visibility-level'));
		$(button).parents("div.btn-group").find("span.btn-setvisibility-label").text(level);
		return false;
	};

	/**
	 * 
	 */
	this.setRoles = function(level, button)
	{
	};
}

$.rbportail = new RbPortail();

