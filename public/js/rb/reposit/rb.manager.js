/**
 * 
 */
function RbRepositManager(jqElement)
{
	this.service = new RbRepositService();
	this.jqElement = jqElement;

	/* @var RbRepositButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbRepositButtons(jqParentElement);
		return rbButtons;
	};
}
RbRepositManager.prototype = new RbManager();

/**
 * 
 */
function RbRepositButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * @param array datas input datas as extracted from element 
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			switch(item.name){
				case 'checked[]':
				case 'repositid':
				case 'repositId':
				case 'id':
					if(url.search('%id%') > 0){
						url = url.replace('%id%', item.value);
					}
					else{
						item.name = 'checked[]';
						queryData.push(item);
					}
					break;
				case 'spacename':
					if(url.search('%spacename%') > 0){
						url = url.replace('%spacename%', item.value);
					}
					else{
						queryData.push(item);
					}
					break;
				default:
					queryData.push(item);
					break;
			}
		});
		url = url.replace('/%id%', '');
		url = url.replace('/%spacename%', '');
		return url;
	}
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* create */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/vault/reposit/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#repositEdit"));
				}
			});
			return false;
		});
		
		/* edit */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/vault/reposit/edit/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				queryData: queryData,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#repositEdit"));
				}
			});
			return false;
		});
		
		/* delete */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this reposit?')){
				var url = document.baseurl+'/vault/reposit/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		/* check */
		parent.find(".check-btn").click(function(e){
			var url = document.baseurl+'/vault/reposit/check';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* replicate */
		parent.find(".replicate-btn").click(function(e){
			var url = document.baseurl+'/vault/replicated/index/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData: queryData});
			return false;
		});
	};
}
RbRepositButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbRepositService()
{

	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'reposits':{}};
		$.each(checked, function(index, id){
			serviceData.categories['reposit'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {reposits:{
			reposit1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbRepositService.prototype = new RbService();
