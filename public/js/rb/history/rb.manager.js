
/**
 * 
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbHistoryManager(jqElement)
{
	this.service = new RbHistoryService();
	this.jqElement = jqElement;

	/* @var RbHistoryButtons */
	this.rbButtons = null;

	/**/
	this.init = function(refreshCallback){
		RbHistoryManager.prototype.init.call(this,refreshCallback);

		var actionProcessor = new RbObjectBoxAction(new RbHistoryService());

		/* batch content */
		jqElement.find(".index-of-batch-btn").click(function(e){
			var btnElemt = $(this);
			var uid = btnElemt.data('uid');
			var url = document.baseurl+'/ged/document/history/indexofbatch/'+uid;
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		/* get document detail */
		jqElement.find(".get-document-detail-btn").click(function(e){
			var btnElemt = $(this);
			var url = document.baseurl+'/ged/document/detail/'+btnElemt.data('spacename')+'/'+btnElemt.data('id');
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		return this;
	};
	
	/**
	 * Get context menu
	 * @var jqParentElement jquery element with data definition for actions. Must have class rb-submit-data.
	 * @return RbWildspaceButtons
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbHistoryButtons(jqParentElement);
		return rbButtons;
	};
}
RbHistoryManager.prototype = new RbManager();

/**
 * @param jqParentElement
 */
function RbHistoryButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * @param array datas input datas as extracted from element 
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			if(item.name.substring(0,11) == 'spacenames['){
				name = 'spacename';
			}
			else{
				name = item.name;
			}
			
			switch(name){
				case 'checked[]':
				case 'id':
					if(url.search('%id%') > 0){
						url = url.replace('%id%', item.value);
					}
					else{
						item.name = 'checked[]';
						queryData.push(item);
					}
					break;
				case 'spacename':
					if(url.search('%spacename%') > 0){
						url = url.replace('%spacename%', item.value);
					}
					else{
						queryData.push(item);
					}
					break;
				default:
					queryData.push(item);
					break;
			}
		});
		
		
		if(url.search('/%id%') > 0){
			url = url.replace('/%id%', '');
		}
		if(url.search('/%spacename%') > 0){
			url = url.replace('/%spacename%', '');
		}
		return url;
	}
	
	/**/
	this.reload = function(){
		$("#content").load("#", function(){});
		return false;
	};
	
	/**
	 * Set events on buttons
	 * 
	 * @param actionProcessor RbAction
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* delete */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this action?')){
				var url = document.baseurl+'/ged/document/history/%spacename%/delete';
				var queryData = [];
				url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
				actionProcessor.toService(e, $(this), url, {
					queryData:queryData,
					type:"POST",
					successCallback: function(){
						rbButtons.reload();
					}
				});
				return false;
			}
			return false;
		});
	}
};
RbHistoryButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbHistoryService()
{
	
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
			if(item.name.substring(0,11) == "spacenames["){
				eval(item.name + "='" + item.value + "'");
			}
		});
		
		var i=1;
		var serviceData = {'histories':{}};
		$.each(checked, function(index, id){
			serviceData.histories['history'+i] = {
				id: id,
				spacename: spacenames[id]
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {documents:{
			document1:{
				id:elemt.data('id'),
				spacename:elemt.data('spacename')
			}
		}};
		return queryData;
	};
};
RbHistoryService.prototype = new RbService();
