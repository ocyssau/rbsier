/**
 * 
 */
function RbWildspaceManager(jqElement)
{
	this.service = new RbWildspaceService();
	this.jqElement = jqElement;
	
	/* @var RbWildspaceButtons */
	this.rbButtons = null;
	
	/* @var RbWildspaceManager */
	var manager = this;

	/** 
	 * INIT DIALOG BOX
	 */
	var dialogBox = $("#rename-dialog-form").dialog({
		autoOpen:false,
		height: 300,
		width: 600,
		modal: true,
		close: function() {
		}
	});
	this.dialogBox = dialogBox;

	/**
	 * Get context menu
	 * @var jqParentElement jquery element with data definition for actions. Must have class rb-submit-data.
	 * @return RbWildspaceButtons
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbWildspaceButtons( jqParentElement );
		rbButtons.dialogBox = this.dialogBox;
		return rbButtons;
	};
}
RbWildspaceManager.prototype = new RbManager();

/**
 * 
 */
function RbWildspaceButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);

	/**/
	this.reload = function(){
		$("#content").load("#", function(){});
		return false;
	};

	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		this.actionProcessor = actionProcessor;
		var rbButtons = this;
		var dialogBox = this.dialogBox;
		parent = this.jqElement;
		
		/* init actions buttons */
		parent.find(".store-btn").click(function(e){
			var url = document.baseurl+'/ged/document/smartstore/store';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".download-btn").click(function(e){
			var url = document.baseurl+'/workplace/wildspace/download';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
		
		/**/
		parent.find(".downloadzip-btn").click(function(e){
			var url = document.baseurl+'/workplace/wildspace/downloadzip';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".rename-btn").click(function(e){
			var url = document.baseurl+'/service/wildspace/file/rename';
			var clickedElement = $(this);

			var dataDiv = clickedElement.parents(".rb-submit-data").first();
			var file = dataDiv.data('file');
			var nameInput = dialogBox.find("input#name");
			nameInput.val(file);

			dialogBox.dialog({
				buttons: {
					"Ok": function() {
						var newName = nameInput.val();
						dialogBox.dialog( "close" );
						if(newName == null){
							return false;
						}
						actionProcessor.toService(e, $(this), url, {
							successCallback: function(){
								rbButtons.reload();
							},
							queryData: {
								files:{
									file1:{
										name:file,
										newname:newName
									}
								}
							}
						});
					},
					"Cancel": function() {
						dialogBox.dialog("close");
					}
				},
			});
			dialogBox.dialog("open");
			return false;
		});

		/**/
		parent.find(".delete-btn").click(function(e){
			if(confirm('Delete file. Are you sure?')){
				var url = document.baseurl+'/service/wildspace/file/delete';
				actionProcessor.toService(e, $(this), url);
				rbButtons.reload();
			}
			return false;
		});

		/**/
		parent.find(".copy-btn").click(function(e){
			var url = document.baseurl+'/service/wildspace/file/copy';
			var clickedElement = $(this);

			var dataDiv = clickedElement.parents(".rb-submit-data").first();
			var file = dataDiv.data('file');
			var nameInput = dialogBox.find("input#name");
			nameInput.val(file);

			dialogBox.dialog({
				buttons: {
					"Ok": function() {
						var newName = nameInput.val();
						dialogBox.dialog( "close" );
						if(newName == null){
							return false;
						}
						actionProcessor.toService(e, $(this), url, {
							successCallback: function(){
								rbButtons.reload();
							},
							queryData:{
								files:{
									file1:{
										name:file,
										newname:newName
									}
								}
							}
						});
					},
					"Cancel": function() {
						dialogBox.dialog("close");
					}
				}
			});

			dialogBox.dialog("open");
			return false;
		});

		/**/
		parent.find(".btn-uncompress").click(function(){
			var url = document.baseurl+'/workplace/wildspace/uncompress';
			return false;
		});

		/**/
		parent.find(".btn-zipdownload").click(function(){
			var url = document.baseurl+'/workplace/wildspace/downloadzip';
			return false;
		});


	};
}
RbWildspaceButtons.prototype = new RbButtons();


/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbWildspaceService()
{
	
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
			if(item.name.substring(0,11) == "spacenames["){
				try{
					var str = item.name + "='" + item.value + "'";
					eval(str);
				}
				catch(error){
					console.error("Can not evaluate expression :" + str + " with error :" + error);
				}
			}
		});
		
		var i=1;
		var serviceData = {'files':{}};
		$.each(checked, function(index, name){
			serviceData.files['file'+i] = {
					name: name,
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {files:{
			file1:{
				file:elemt.data('file'),
			}
		}};
		return queryData;
	};
}
RbWildspaceService.prototype = new RbService();


/**
 * 
 */
function RbWildspaceFileChecker(currentUserId)
{

	this.currentUserId = currentUserId;

	/**/
	this.check = function(filename, domElemt)
	{
		var vault = this;
		var url = document.baseurl+'/service/object/search/forfilename';
		var ajax = new RbAjax();
		var datas = {
				name:filename,
				spacename:'workitem',
				select: ['id', 'parentId', 'accessCode', 'lockById', 'lockByUid', 'md5'],
				lastversiononly: true
		};
		
		var successCallback = function(e,domElemt){
			var respons = e.respons;
			if(respons.data){
				/* if many file with same name (in various space is possible) */
				if(respons.data.length > 1){
					domElemt.closest('tr').find('td.vault').append('<p><div class="label label-success">There is many files in vault which have same names. Manualy check the status of these files</div></p>');
				}
				else{
					if(respons.data.length == 0){
						console.log('Not Found');
						return;
					}
					dfDatas = respons.data[0];
					docfileId = dfDatas.id;
					parentId = dfDatas.parentId;
					spacename = dfDatas.spacename;
					domElemt.closest('tr').find('td.vault').html(
						'<div class="btn-group"><a href="#" class="btn btn-xs btn-default getdocument-btn">Get document</a></div>'
					);

					domElemt.data('docfileid', docfileId);
					domElemt.data('documentid', parentId);
					domElemt.data('spacename', spacename);

					if(dfDatas.accessCode == 1){
						if(dfDatas.lockById == currentUserId){
							domElemt.closest('tr').find('td.vault').append('<p><div class="label label-info">Yes! you are a good boy, file is checkouted by you</div></p>');
							domElemt.data('checkoutby', 'me');
							domElemt.data('checkoutbyId', dfDatas.lockById);
						}
						else{
							domElemt.data('checkoutby', dfDatas.lockByUid);
							domElemt.data('checkoutbyId', dfDatas.lockById);
							domElemt.closest('tr').find('td.vault').append('<p><div class="alert alert-danger"><strong>BE CAREFUL, really, This file is actually checkouted by '
									+ dfDatas.lockByUid 
									+ '. You are risking to crash his work.</strong></div></p>');
						}
					}
					vault.isModified(domElemt);
				}
			}
			else{
				domElemt.closest('tr').find('td.vault').html(
						'<div class="label label-warning">not yet vaulted, quick, do it!</div>'
				);
			}
		};
		
		ajax.serviceRequest(vault, domElemt, url, {
			dataType: 'json',
			messageDisplay: false,
			queryData:datas,
			successCallback:successCallback
		});
		
		var rbDocument = {};
		return document;
	};

	/**
	 */
	this.isModified = function(domElemt)
	{
		var box = domElemt;
		var url = document.baseurl+'/service/wildspace/file/comparemd5';
		var queryData = {
				name: box.data('name'),
				id: box.data('docfileid'),
				spacename: box.data('spacename')
		};

		var ajax = new RbAjax();
		ajax.serviceRequest({}, box, url, {
			dataType: 'json',
			messageDisplay: false,
			queryData:queryData,
			successCallback:function(e,box){
				var respons = e.respons;
				if(respons.feedbacks){
					msg = respons.feedbacks[0].message;
					if(msg == 'changed'){
						box.data('changed', true);
						var documentTd = box.closest('tr').find('td.vault');
						if(box.data('checkoutby') == 'me'){
							documentTd.find('div.check-result').first().remove();
							documentTd.append('</p><div class="check-result label label-danger">This file has been modified</div></p>');
						}
						else if(box.data('checkoutby') == undefined){
							documentTd.append('</p><div class="check-result alert alert-danger">Serious?? This file has been modified and is not checkouted by you!</div></p>');
						}
						else{
							documentTd.find('div.check-result').first().remove();
							documentTd.append('</p><div class="check-result label label-danger">This file has been modified</div></p>');
						}
					}
					else{
						box.data('changed', false);
						var documentTd = box.closest('tr').find('td.vault');
						documentTd.find('div.check-result').first().remove();
						documentTd.append('<p><div class="check-result label label-success">This file is actually exactly the same than file in vault</div></p>');
					}
				}
			}
		});
		return false;
	};

	/**/
	this.init = function(domElemt)
	{
		$( document ).on( 'click', 'a.getdocument-btn', function(event){
			event.preventDefault();
			var box = $(this).closest('.rb-submit-data').first();
			var url = document.baseurl+'/ged/document/detail/%spacename%/%id%';
			url = url.replace('%id%', box.data('documentid'));
			url = url.replace('%spacename%', box.data('spacename'));

			var options = {
					popup:{
						target:"detail of document " + box.data("documentid"),
						ysize:600,
						xsize:1200,
					},
					hash:"#documentdetail",
					queryData:[]
			};
			var actionProcessor = new RbObjectBoxAction(new RbDocumentService());
			actionProcessor.getRequest(event, $(this), url, options);
			return false;
		});
	}
}

