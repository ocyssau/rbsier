
/**
 * 
 */
function RbCallbackManager(jqElement)
{
	this.service = new RbCallbackService();
	this.jqElement = jqElement;

	/* @var RbCallbackButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbCallbackButtons(jqParentElement);
		return rbButtons;
	};
}
RbCallbackManager.prototype = new RbManager();

/**
 * 
 */
function RbCallbackButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* create */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/admin/callback/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#callbackEdit"));
				}
			});
			return false;
		});
		
		/* edit */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/admin/callback/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#callbackEdit"));
				}
			});
			return false;
		});
		
		/* delete */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this callback?')){
				var url = document.baseurl+'/admin/callback/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
	};
}
RbCallbackButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbCallbackService()
{

	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'callbacks':{}};
		$.each(checked, function(index, id){
			serviceData.documents['callback'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {categories:{
			callback1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbCallbackService.prototype = new RbService();
