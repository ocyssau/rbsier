/**
 * @param jqElement JQuery object of the object boxes
 */
function RbObjectBox(jqElemt)
{
	/* @var jQuery Element */
	this.jqElement = jqElemt;
	
	/* @var RbObjectBoxContextMenu */
	this.contextmenu = null;
	
	/**/
	this.button = null;
	
	/* @var RbObjectBoxContextMenu */
	this.contextmenu = null;
	
	/* @var RbService */
	this.service = null;
	
	/* @var RbObjectBoxAction */
	this.rbAction = null;
	
	/**
	 */
	this.init = function()
	{
		this.jqElement.find(".button-box, .left, .right").hide();
		this.jqElement.hover(
				function(e){
					$(this).find(".button-box").stop(true, true).show(200);
					$(this).find(".left, .right").stop(true, true).show(600);
				},
				function(e){
					$(this).find(".button-box").delay(1000).hide(500);
					$(this).find(".left, .right").delay(1000).hide(500);
				}
		);
	};
	
	/**
	 * 
	 */
	this.newAction = function(rbService)
	{
		service = rbService || this.service;
		rbAction = new RbObjectBoxAction(this, service);
		return rbAction;
	}
	
	/**
	 * rbButtons, service
	 */
	this.initContextMenu = function(rbButtons, service)
	{
		buttons = rbButtons || this.rbButtons;
		service = service || this.service;
		
		rbAction = this.newAction(service);
		
		/* set events on buttons of menu */
		buttons.init(rbAction);
		
		contextMenu = new RbObjectBoxContextMenu(buttons.jqElement);
		contextMenu.init(this.jqElement);
		
		this.contextmenu = contextMenu;
		
		return this;
	};
	
	/**
	 * @param rbButtons RbButtons object
	 */
	this.initButtons = function(rbButtons, service)
	{
		rbAction = this.newAction(service);
		rbButtons.init(rbAction);
		this.button = rbButtons;
		return this;
	};
};
RbObjectBox.prototype = new RbView();

/**
 * @param jqElement Jquery object of the object box
 * @param service Rb*Service object used to convert request in ranchbe service format
 */
function RbObjectBoxAction(rbObjectBox, service)
{
	this.service = service;
	
	/* @var RbObjectBox */
	this.rbObjectBox = rbObjectBox;
	
	/**
	 * @param clickedElement JQuery
	 * @return collection of objects with properties name:'name' and value:'value'
	 */
	this.getQueryData = function(clickedElement){
		var data = this.getSubmitData(clickedElement).data();
		var queryData = [];
		$.each(data, function(name,value){
			queryData.push({
				name:name,
				value:value
			});
		});
		return queryData;
	};
	
	/**
	 * Build query data from data on selected row from elemts.rb-submit-data
	 * @return array
	 */
	this.getSelectedData = function()
	{
		var selectedData = [];
		var jqElement = this.rbObjectBox.jqElement;
		
		if(jqElement.hasClass('rb-submit-data')){
			selectedData.push(jqElement.data());
		}
		else{
			selectedData.push(jqElement.closest(".rb-submit-data").data());
		}
		return selectedData;
	};
	
	/**
	 * clickedElement is the clicked menu button.
	 * When button is clicked, search in parent a element with class "rb-submit-data"
	 * and get data to submit from datas of this element
	 * else
	 * the queryData may be populate with parameters to send.
	 * 
	 * @param e Event
	 * @param clickedElement Jquery
	 * @param url string
	 * @param options array
	 */
	this.toService = function(e, clickedElement, url, options){
		var myOptions = $.extend({}, this.options, options);
		if(myOptions.queryData == null){
			var service = this.service;
			try{
				submitDatas = this.getSubmitData(clickedElement);
				myOptions.queryData = service.extractQueryFromData(submitDatas);
			}
			catch(err){
				console.log(err);
			}
		}
		this.serviceRequest(e, clickedElement, url, myOptions);
		return false;
	};
	
	/**
	 * 
	 */
	this.getModal = function(e, clickedElement, url, options){
		var myOptions = $.extend({}, this.options, options);
		
		/* highlight button */
		this.highlight(clickedElement);
		
		/* extract queryData from table form */
		if(myOptions.queryData == null){
			try{
				submitDatas = this.getSubmitData(clickedElement);
				myOptions.queryData = this.filterSubmitData(submitDatas.data());
			}
			catch(err){
				console.log(err);
			}
		}
		
		/* build url */
		url = this.buildUrl(url, $.param(myOptions.queryData), myOptions.hash);
		console.log(url);
		return this.modalPopup(url, myOptions);
	};
	
	/**
	 * make a http get request on url with some options
	 */
	this.getRequest = function(e, clickedElement, url, options){
		var myOptions = $.extend({}, this.options, options);
		
		/* highlight button */
		this.highlight(clickedElement);

		/* extract queryData from table form */
		if(myOptions.queryData == null){
			try{
				submitDatas = this.getSubmitData(clickedElement);
				myOptions.queryData = this.filterSubmitData(submitDatas.data());
			}
			catch(err){
				console.log(err);
			}
		}

		/* build url */
		url = this.buildUrl(url, $.param(myOptions.queryData), myOptions.hash);

		/* set popup option */
		if(myOptions.popup.xsize){
			Ranchbe.win.popupP(url, myOptions.popup.target , myOptions.popup.ysize , myOptions.popup.xsize);
		}
		else{
			document.location.href = url;
		}
	};
};
RbObjectBoxAction.prototype = new RbAction();

/**
 * 
 */
function RbObjectBoxContextMenu(jqMenuDefinition)
{
	this.jqMenuDefinition = jqMenuDefinition;
	
	/**
	 * Init the context menu object. Use of Jquery-ui .menu() plugin.
	 * Set a class named "rb-submit-data" on top div of menu definition to used by buttonOrAhrefAction and toRbServiceAction.
	 * 
	 * @param onElements Jquery objects wich must toggle the menu when click on
	 * @param menuDefinition the top div of the menu definition
	 * @param callback actionCallback function to call when a menu element is clicked
	 */
	this.init = function(onElements)
	{
		onElements.addClass("with-context-menu");

		var jqMenuDefinition = this.jqMenuDefinition;
		var contextMenuHelper = this;
		
		/*remove item for tables */
		jqMenuDefinition.find("span.cm-header").remove();
		jqMenuDefinition.find("a.cm-clear").parent().remove();
		jqMenuDefinition.find("a.cm-invert").parent().remove();
		
		/* on right click */
		onElements.contextmenu(function(e){
			var submitDataElement = contextMenuHelper.getSubmitData($(this));
			
			/* transmit datas from button to menu definition top div */
			$.each(submitDataElement.data(), function(name, value){
				jqMenuDefinition.data(name, value);
			});
			
			/*add header to menu*/
			contextMenuHelper.setTitle(submitDataElement.data("name"));
			
			/* Filter the buttons to display */
			multiSelect = false;
			contextMenuHelper.filter(multiSelect, submitDataElement);
			
			contextMenuHelper.show(e);
			return false;
		});
		
		return this.initMenuDefinition();
	}
};
RbObjectBoxContextMenu.prototype = new RbContextMenu();


//# sourceURL=/ranchbe/dynamic/rb/model/rb.objectbox.js

