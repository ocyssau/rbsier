/**
 * 
 */
function RbForm()
{
	this.service = null;

	/**
	 * 
	 */
	this.setService = function(service)
	{
		this.service = service;
	}

	/**
	 * 
	 */
	this.reload = function()
	{
		return false;
	};

	/**
	 * 
	 */
	this.initInModal = function(jqForm)
	{
		var validateBtn = jqForm.find("input[name='validate']");
		var cancelBtn = jqForm.find("input[name='cancel']");
		service = this.service;
		var rbForm = this;
		var div = $("#rb-popup-modal").first();
		
		/* beautiful multi-select */
		jqForm.find('.selectpicker').each(function () {
			var selectpicker = $(this);
			$.fn.selectpicker.call(selectpicker, selectpicker.data());
		});

		/* */
		validateBtn.click(function(e){
			var download = jqForm.data('downloadForm');
			if(download == 1){
				/* trigger a new event to discriminate submit event and squize default action */
				jqForm.trigger("validate");
				jqForm.append('<input type="hidden" name="validate" value="validate">');
				jqForm.submit();
				div.modal("hide");
				return false;
			}
			
			var options = {};
			options.dataType = "text";
			
			options.successCallback = function(e, domElemt){
				if(e.respons[0]=='{'){
					div.modal("hide");
					rbForm.reload();
				}
				else{
					/* if its not json, its a html */
					div.html(e.respons);
					div.find('#rb-load-waiting').hide(250);
					rbForm.reload();
				}
				return true;
			};
			
			options.failedCallback = function(e, domElemt){
				if(e.respons[0]=='{'){
					var respons = JSON.parse(e.respons);
					var rbAjax = new RbAjax();
					rbAjax.errorDisplay(respons.errors);
					div.modal("hide");
				}
				else{
					div.empty().append(e.respons);
					div.find("#content").append('<div class="help">Click out this panel to close it</div>');
				}
			};

			var url = jqForm.attr('action');
			var action = new RbFormAction(jqForm, service);
			action.postRequest(e, $(this), url, options);

			return false;
		});

		/**/
		cancelBtn.click(function(e)
		{
			div.modal("hide");
			return false;
		});
	}
};
RbForm.prototype = new RbView();

/**
 * @param service RbService object
 * @param jqForm Jquery Form object
 */
function RbFormAction(jqForm,service)
{
	this.service = service || null;
	this.form = jqForm;

	/**
	 * @param clickedElement JQuery
	 * @return collection of objects with properties name:'name' and value:'value'
	 */
	this.getQueryData = function(clickedElement)
	{
		queryData = this.form.serializeArray();
		return queryData;
	};

	/**
	 * 
	 */
	this.toService = function(e, clickedElement, url, options)
	{
		var myOptions = $.extend({}, this.options, options);
		if(myOptions.queryData == null){
			try{
				myOptions.queryData = this.service.serializeForm(this.tableForm);
			}
			catch(err){
				console.log(err);
			}
		}
		
		myOptions.type = "POST";
		this.serviceRequest(e, clickedElement, url, myOptions);
		return false;
	};

	/**
	 * 
	 */
	this.postRequest = function(e, clickedElement, url, options)
	{
		/* trigger a new event to discriminate submit event and squize default action */
		this.form.trigger("validate");
		
		/* extract queryData from table form */
		if(options.queryData == null){
			options.queryData = this.form.serializeArray();
			options.queryData.push({
				name:"validate", 
				value:"validate"
			});
		}
		options.type = "POST";
		e.rbAction = this;
		this.serviceRequest(e, clickedElement, url, options);
	};
	
	/**
	 * Call by RbAjax to redisplay form with some bad inputs feedbacks
	 * Redisplay in modal popup only
	 */
	this.redisplay = function(htmlContent)
	{
		modal = this.form.parents("#rb-popup-modal");
		if(modal){
			modal.html(htmlContent);
			rbForm = new RbForm();
			rbForm.initInModal($(jqForm.selector));
		}
	}
};
RbFormAction.prototype = new RbAction();

//# sourceURL=/ranchbe/dynamic/rb/model/rb.form.js

