/**
 * domElemt table element
 * 
 */
function RbTable(jqElemt)
{
	/* @var jQuery Element */
	this.jqElement = jqElemt;
	
	/* @var RbTable */
	jqElemt.rbTable = this;
	
	/* @var RbTableContextMenu */
	this.contextMenu = null;
	
	/* @var RbService */
	this.service = null;
	
	/* @var RbTableAction */
	this.rbAction = null;
	
	$("body").mousedown(function (e) {
	    if (e.ctrlKey || e.shiftKey) {
	        e.preventDefault();
	    }
	});	

	/**
	 * 
	 */
	this.select = function(tr){
		tr.find(":checkbox").prop("checked", true);
		tr.addClass("marked");
	}

	/**
	 * 
	 */
	this.unselect = function(tr){
		tr.find(":checkbox").prop("checked", false);
		tr.removeClass("marked");
	}
	
	/**
	 * 
	 */
	this.uniformSelectionOnData = function(onDataName='spacename')
	{
		checkboxes = $(this.getChecked());
		selected = checkboxes.parents('tr').find(".rb-submit-data");
		attrValue = selected.first().data(onDataName);
		table = this;
		filtered = false;
		selected.each(function(index, item){
			item = $(item);
			if(item.data(onDataName) != attrValue){
				table.unselect(item.parents('tr'));
				filtered = true;
			}
		});
		return filtered;
	}

	/**
	 * 
	 * 
	 */
	this.getChecked = function()
	{
		return $(this.jqElement.find("input:checked").not(".switcher"));
	}

	/**
	 * @param rbButtons RbButtons Object
	 */
	this.initContextMenu = function(rbButtons, service, trSelector)
	{
		service = service || this.service;
		trSelector = trSelector || "tbody tr";

		var form = this.jqElement.parents("form").first();
		var actionProcessor = new RbTableAction(this, form, service);
		
		rbButtons.init(actionProcessor);
		tableContextMenu = new RbTableContextMenu(rbButtons.jqElement, this);
		var table = this;
		
		/* Add clear selection trigger */
		rbButtons.jqElement.find('a.cm-clear').first().click(function(event){
			table.clearSelection();
			tableContextMenu.jqMenuDefinition.hide();
			return false; /*! Important */
		});
		
		/* add invert selection trigger */
		rbButtons.jqElement.find('a.cm-invert').first().click(function(event){
			table.invertSelection();
			tableContextMenu.jqMenuDefinition.hide();
			return false; /*! Important */
		});
		
		/* Add context menu on each rows of table */
		tableContextMenu.init(this.jqElement.find(trSelector));

		this.service = service;
		this.contextMenu = tableContextMenu;
		this.actionProcessor = actionProcessor;
		this.form = form;
	};

	/* INIT TABLE SORTABLE HEADERS */
	this.initHeader = function(orderby, order, url)
	{
		var th = this.jqElement.find('[data-field="'+orderby+'"]');
		
		th.data('order', order);
		if(order=='asc'){
			th.prepend('<span class="glyphicon glyphicon-chevron-down"></span>')
		}
		else{
			th.prepend('<span class="glyphicon glyphicon-chevron-up"></span>')
		}

		$('.sortable').click(function(e){
			var order = $(this).data('order');
			var orderby = $(this).data('field');
			if(order == 'asc'){
				order='desc';
			}
			else{
				order='asc';
			}

			var orderQuery = 'order='+order+'&orderby='+orderby;
			if (url.search(/(\?)./i) == -1) { /* GET parameters in query */
				url = url + '?'+ orderQuery;
			}
			else{
				/* remove resetf parameter */
				url = url.replace("resetf", "null")+'&'+orderQuery;
			}
			window.open(url,"_self");
		});
		return this;
	};
	
	
	/**
	 * When click on row of table, check the checkbox
	 * and toggle class "marked".
	 * Set the data-id of the parent .rb-submit-data element with values of selected checkbox.
	 * 
	 * @var checkboxParentTdSelector is string jquery selector to select parent element where found the checkbox use for select
	 */
	this.markrowsInit = function(checkboxParentTdSelector) 
	{
		var lastSelected = null;
		
		checkboxParentTd = this.jqElement.find(checkboxParentTdSelector);
		checkboxParentTd.find(":checkbox").hide();
		
		var table = this;
		
		this.jqElement.find("td.selectable").click(function(event){
			/* not select if click on button or links */
			if(event.target != event.currentTarget){
				if($(event.target).is(':button') || $(event.target).is(':input') || $(event.target).is('a')){
					return;
				}
			}
			
			var tr = $(this).parent("tr");
			var checkbox = tr.find(checkboxParentTdSelector).find(":checkbox");
			var all = table.jqElement.find("tr");
			
			/* init last selected row if not yet */
			if(!lastSelected){
				lastSelected = tr;
			}
			
			/* with shift key */
			if (event.shiftKey) {
				selected = table.jqElement.find("tr.marked");
				start = all.index(tr);
				end = all.index(lastSelected);
				toselect = all.slice(Math.min(start, end), Math.max(start, end) + 1);
				table.markRows(toselect.find(':checkbox'), true);
				return false;
			}
			/* set last selected row */
			lastSelected = tr;
			
			/* add data to parent rb-submit-data*/
			var submitDatas = checkbox.parents(".rb-submit-data").first();
			//var ids = submitDatas.data('id');

			if(tr.hasClass("marked")){
				checkbox.prop("checked", false);
				/* remove from ids 
				if(ids){
					ids.splice(ids.indexOf(checkbox.attr("value")), 1);
				}
				*/
			}
			else{
				checkbox.prop("checked", true);
				/*add to ids
				if(ids){
					ids.push(checkbox.attr("value"))
				}
				*/
			}
			tr.toggleClass("marked");

			var displaybox = $("#displaySelectedRowCount");
			displaybox.html(table.countSelectedRows());

			//submitDatas.data('id', ids);
		});

		this.jqElement.find("input.switcher").click(function(){
			var state = $(this).prop("checked");
			var selector = $(this).data("toswitch");
			var table = new RbTable($(this).parents("table").first());
			table.markRows($(selector), state);
		});
	}

	/**
	 * If checkbox of the checkboxes list are checked, toggle the class marked apply to table row.
	 * Set the data-id of the parent .rb-submit-data element with values of selected checkbox.
	 */
	this.markRows = function(checkboxes, on)
	{
		//var ids = [];
		var table = this;
		checkboxes.each(function(index){
			var checkbox = $(this);
			var tr = checkbox.parents("tr");
			if(on==true){
				table.select(tr);
				//ids.push(checkbox.attr("id"));
			}
			else{
				table.unselect(tr);
			}
		});

		/* count number of checked checkbox */
		var displaybox = $("#displaySelectedRowCount");
		displaybox.html(this.countSelectedRows());

		/* add data to parent rb-submit-data */
		//checkboxes.parents(".rb-submit-data").first().data('id', ids);

		return false;
	}
	
	/**
	 * When click on row of table, check the checkbox
	 * and toggle class "marked".
	 * Set the data-id of the parent .rb-submit-data element with values of selected checkbox.
	 */
	this.clearSelection = function()
	{
		all = this.jqElement.find("td.rb-item-selector");
		this.markRows(all.find(':checkbox'), false);
		return this;
	}
	
	/**
	 * When click on row of table, check the checkbox
	 * and toggle class "marked".
	 * Set the data-id of the parent .rb-submit-data element with values of selected checkbox.
	 */
	this.invertSelection = function()
	{
		all = this.jqElement.find("td.rb-item-selector");
		var table = this;
		all.each(function(index,item){
			checkbox = $(item).find(':checkbox');
			if(checkbox.prop("checked") == true){
				table.markRows(checkbox, false);
			}
			else{
				table.markRows(checkbox, true);
			}
		});
		return this;
	}
	
	/**
	 * Display the count of row selected by rbMarkRowsInit
	 */
	this.countSelectedRows = function()
	{
		/* for every table row ... */
		var count = 0;
		var rows = this.jqElement.find('tr');
		rows.each(function(index){
			var checkbox = $(this).find(":checkbox").not(".switcher");
			if(checkbox.prop("checked") == true){
				count++;
			}
		});
		return count;
	}

	/**
	 * dataField is the data-field value
	 */
	this.hideColumn = function(dataFieldName)
	{
		var col = this.jqElement.find('th[data-field="'+dataFieldName+'"]');
		col.hide();
		var colIndex = col.index() + 1;
		this.jqElement.find('tr td:nth-child(' + colIndex + ')').hide();
	}

	/**
	 * dataField is the data-field value
	 */
	this.showColumn = function(dataFieldName)
	{
		console.log(dataFieldName);
		var col = this.jqElement.find('th[data-field="'+dataFieldName+'"]');
		col.show();
		var colIndex = col.index() + 1;
		this.jqElement.find('tr td:nth-child(' + colIndex + ')').show();
	}

	/**
	 * 
	 */
	this.initColumnSelector = function(dropDownButton, pageId)
	{
		var rbTable = this;
		var cookieKeyName = pageId + '-columnsToHide';

		/* inhib default click action */
		dropDownButton.find(".dropdown-menu").click(function(e){
			e.stopPropagation();
		});

		/* init cookie */
		var cookie = new RbCookie();
		var columnsToHide = cookie.get(cookieKeyName).split(',');

		/* Hide columns as initialy defined by cookie */
		if($.isArray(columnsToHide)){
			$.each(columnsToHide, function(index,item){
				/* hide column */
				rbTable.hideColumn(item);

				/* uncheck checkbox of column selector*/
				cb = dropDownButton.find('input[data-col="'+item+'"]');
				cb.attr('checked', false);
			});
		}

		/* */
		dropDownButton.find("button.validate-btn").click(function(e){
			var cbs = dropDownButton.find('input:not(:checked)');
			var columnsToHide = [];
			$.each(cbs, function(index,item){
				columnsToHide.push($(item).data('col'));
				/* hide columns */
				rbTable.hideColumn($(item).data('col'));
			});

			/* column to show */
			var cbs = dropDownButton.find('input:checked');
			$.each(cbs, function(index,item){
				rbTable.showColumn($(item).data('col'));
			});

			/* Save to cookie */
			console.log(columnsToHide);
			cookie.set(cookieKeyName, columnsToHide, 365);
			dropDownButton.find(".dropdown-menu").toggle();
		});

		/* */
		dropDownButton.find("button").first().click(function(e){
			dropDownButton.find(".dropdown-menu").toggle();
		});
	}
	
	this.clearSelection();
};
RbTable.prototype = new RbView();

/**
 * jqMenuDefinition the jQuery main div element of the menu definition
 * table RbTable
 */
function RbTableContextMenu(jqMenuDefinition, rbTable)
{
	/* @var JQuery */
	this.jqMenuDefinition = jqMenuDefinition;
	
	/* @var RbTable */
	this.rbTable = rbTable;

	/**
	 * add context menu on each onElements
	 * onElements is collection of tr elements
	 */
	this.init = function(onElements)
	{
		var jqMenuDefinition = this.jqMenuDefinition;
		onElements.addClass("with-context-menu");
		var contextMenuHelper = this;
		
		/* on right click */
		onElements.contextmenu(function(e){
			var jqTable = rbTable.jqElement;
			var checkboxes = jqTable.find("input:checked").not(".switcher");
			
			/* if clicked element is not selected, cancel selection of others */
			if($(this).find(":checkbox").prop("checked") == false){
				checkboxes.prop("checked", false);
				checkboxes.parents("tr").removeClass("marked");
				checkboxes = jqTable.find("input:checked").not(".switcher");
				/* ... and select it */
				$(this).find(":checkbox").prop("checked", true);
				$(this).addClass("marked");
			}

			/* check number of selected lines */
			if(checkboxes.size() > 1){
				var multiSelect = true;
				/* set a list of datas of each selected tr */
				var selectionSet = [];
				var tr;
				var submitDataElement;
				$.each(checkboxes, function(i,checkbox){
					tr = $(checkbox).parents("tr").first();
					submitDataElement = tr.find(".rb-submit-data").first();
					selectionSet.push(submitDataElement.data());
				});
				/*add header to menu*/
				contextMenuHelper.setTitle(checkboxes.size()+" selected");
			}
			else{
				var multiSelect = false;
				if(checkboxes.size()==1){
					var checkbox = checkboxes.first();
					var tr = checkbox.parents("tr").first();
					/* one tr selected, right clicked is on the same? */
					if(tr.get(0).offsetTop != this.offsetTop){
						/* unselect it */
						checkbox.prop("checked", false);
						tr.removeClass("marked");
						tr = $(this);
					}
				}
				/*right click on tr but none selection, take only the clicked tr*/
				else if(checkboxes.size()==0){
					var tr = $(this);
				}
				
				if($(tr).hasClass("rb-submit-data")){
					var submitDataElement = $(tr);
				}
				else{
					var submitDataElement = tr.find(".rb-submit-data").first();
				}
				
				/*add header to menu*/
				contextMenuHelper.setTitle(submitDataElement.data("name"));
			}

			/* Update counter */
			//var rbTable = new RbTable(table);
			var displaybox = $("#displaySelectedRowCount");
			displaybox.html(rbTable.countSelectedRows());

			/* Transmit datas from button to menu definition top div */
			$.each(submitDataElement.data(), function(name, value){
				jqMenuDefinition.data(name, value);
			});

			/* Filter the buttons to display */
			contextMenuHelper.filter(multiSelect, submitDataElement);

			/* */
			contextMenuHelper.show(e);
			return false;
		});

		return this.initMenuDefinition();
	};
};
RbTableContextMenu.prototype = new RbContextMenu();

/**
 * @param RbTable rbTable
 * @param jqueryForm form
 * @param RbService service
 * 
 */
function RbTableAction(rbTable, form, service)
{
	
	/* @var RbService */
	this.service = service;
	
	/* JqElement */
	this.tableForm = form;
	
	/* RbTable */
	this.rbTable = rbTable;
	
	/* double link */
	this.rbTable.rbAction = this;
	
	/**
	 * clickedElement is the clicked menu button.
	 * When button is clicked, search in parent a element with class "rb-submit-data"
	 * and get data to submit from datas of this element
	 * Else
	 * the queryData may be populate with parameters to send.
	 * 
	 * @param e Event
	 * @param clickedElement JQuery
	 * @param url string
	 * @param options array
	 */
	this.toService = function(e, clickedElement, url, options)
	{
		var myOptions = $.extend({}, this.options, options);
		if(myOptions.queryData == null){
			try{
				myOptions.queryData = this.service.queryDataToServiceData(this.getQueryData());
			}
			catch(err){
				console.log(err);
			}
		}
		this.serviceRequest(e, clickedElement, url, myOptions);
		return false;
	};
	
	/**
	 * Get data from -data attribute on elemts.rb-submit-data and merge with data from form.
	 * 
	 * @param clickedElement JQuery
	 * @return collection of objects with properties name:'name' and value:'value'
	 */
	this.getQueryData = function(clickedElement)
	{
		var queryData = this.selectedDataToQueryData( this.getSelectedData() );
		
		/* merge with data from form */
		formData =  this.tableForm.serializeArray();
		$.each(formData, function(index, data){
			if(data.name != 'checked[]'){
				queryData.push({
					name: data.name,
					value: data.value
				})
			}
		});
		
		console.log(queryData);
		return queryData;
	};
	
	/**
	 * Build query data from data on selected row from elemts.rb-submit-data
	 */
	this.getSelectedData = function()
	{
		selected = this.rbTable.getChecked();
		var selectedData = [];
		$.each(selected, function(index, checkbox){
			datas = $(checkbox).closest(".rb-submit-data").data();
			selectedData.push(datas);
		});
		
		return selectedData;
	};

	/**
	 * get data and return to service data formater
	 */
	this.getServiceRequestData = function(queryData)
	{
		return service.getServiceDataFromQueryData(queryData);
	};

	
	/**
	 * 
	 */
	this.uniformSelectionOnData = function(onDataName='spacename')
	{
		return this.rbTable.uniformSelectionOnData(onDataName);
	}
	
	
	/**
	 * @param e Event
	 * @param clickedElement JQuery
	 * @param url string
	 * @param options array
	 */
	this.getForm = function(e, clickedElement, url, options)
	{
		var form = clickedElement.parents("form").first();
		queryData = service.serializeForm($(this).parents("form").first());
	};

	/**
	 * Open request in a modal popup
	 */
	this.getModal = function(e, clickedElement, url, options)
	{
		var myOptions = $.extend({}, this.options, options);

		/* highlight button */
		this.highlight(clickedElement);

		/* extract queryData from table form */
		if(myOptions.queryData == null){
			myOptions.queryData = this.getQueryData(clickedElement);
		}

		/* build url */
		url = this.buildUrl(url, $.param(myOptions.queryData), myOptions.hash);

		this.modalPopup(url, myOptions);
	};

	/**
	 * Make a http get request on url with some options
	 */
	this.getRequest = function(e, clickedElement, url, options)
	{
		var myOptions = $.extend({}, this.options, options);

		/* Extract queryData from table form */
		if(myOptions.queryData == null){
			myOptions.queryData = this.getQueryData(clickedElement);
		}

		/* Highlight button */
		this.highlight(clickedElement);

		/* Build url */
		url = this.buildUrl(url, $.param(myOptions.queryData), myOptions.hash);

		/* Set popup option */
		if(myOptions.popup.xsize){
			Ranchbe.win.popupP(url, myOptions.popup.target , myOptions.popup.ysize , myOptions.popup.xsize);
		}
		else{
			document.location.href = url;
		}
	};
};
RbTableAction.prototype = new RbAction();

//# sourceURL=/ranchbe/dynamic/rb/model/rb.table.js

