/**
 * 
 */
function RbDropzone(dropfileDiv, url)
{
	var documentDialog;
	var file;

	if ( ! window.FileReader ) {
		return alert( 'FileReader API is not supported by your browser.' );
	}

	dropfileDiv.on('dragenter', function() {
		$(this).css('border', '3px dashed red');
		return false;
	});
	dropfileDiv.on('dragover', function(e) {
		e.preventDefault();
		e.stopPropagation();
		$(this).css('border', '3px dashed red');
		return false;
	});
	dropfileDiv.on('dragleave', function(e) {
		e.preventDefault();
		e.stopPropagation();
		$(this).css('border', 'none');
		return false;
	});

	dropfileDiv.on('drop', function(e){
		if (e.originalEvent.dataTransfer) {
			if (e.originalEvent.dataTransfer.files.length) {
				// Stop the propagation of the event
				e.preventDefault();
				e.stopPropagation();
				$(this).css('border', 'none');

				var files = e.originalEvent.dataTransfer.files;
			  	for (var i = 0, file; file = files[i]; i++) {
					var reader = new FileReader();
					reader.file = file;
					reader.url = url;
					reader.onload = uploadFile;
					reader.readAsDataURL(file);
				}
			}
		}
		else {
			$(this).css('border', '3px dashed #BBBBBB');
		}
		return false;
	});

	/* on click into dropzone, display file selector */
	dropfileDiv.click(function(e) {
		var dialogBox = $("#rb-fileselector.modal");
		dialogBox.dialog({
			resizable: true,
			modal: true,
			width:400,
			height:210,
			buttons: {
				"Continue": function() {
					var $i = dialogBox.find('input#rb-file'),
					input = $i[0];
					if ( input.files && input.files[0] ) {
						var file = input.files[0];
						var reader = new FileReader();
						reader.file = file;
						reader.url = url;
						reader.onload = uploadFile;
						reader.readAsDataURL(file);
					}
					else {
						alert( "File not selected or browser incompatible." )
					}
					$(this).dialog("close");
				},
				"Cancel": function() {
					$(this).dialog("close");
				}
			}
		});
		return false;
	});

	/* call by fileReader, in the context of the fileReader */
	function uploadFile(event) {
		var splitresult = event.target.result.split(',');
		var mimestype = splitresult[0].split(';')[0];
		var encode = splitresult[0].split(';')[1];
		var data = splitresult[1];
		var filename = this.file.name;

		var params={};
		params.encode = 'json';
		params.files = {
			file1:{
				name:filename,
				data:{
					name:filename,
					md5:$.md5(data),
					size:data.length,
					data:data,
					mimestype:mimestype,
					encode:encode
				}
			}
		};

		$.ajax({
			type : 'post',
			url : this.url,
			data : params,
			dataType: 'json',
			success : function(data, textStatus, jqXHR){
				location.reload();
			},
			error : function(datas, textStatus, jqXHR){
				alert("Error during upload of file, check file name and retry");
				//var rbajax = new RbAjax();
				//rbajax.errorDisplay(datas.errors);
				//rbajax.exceptionDisplay(datas.exception);
			}
		});
	}
}

//# sourceURL=/ranchbe/dynamic/rb/model/rb.dropzone.js

