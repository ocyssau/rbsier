/** 
 * @class Ranchbe Main class for ranchbe application
 */
function Ranchbe()
{
	this.rbajax = null;
	this.user = null;
	this.dynamicScripts = [];

	$.ajaxSetup({
		cache: true
	});	

	/**
	 * @memberOf Ranchbe
	 */
	this.options = {
			baseurl: document.baseurl,
			debug:false,
			mainWindow:"",
			rbconverterUrl:"",
			connFrom:"local", //[distant|local],
			jsReload:1,
			cssReload:1

	};

	/**
	 * @memberOf Ranchbe
	 * 
	 */
	this.setOption = function(name,value)
	{
		eval("this.options."+name+"='"+value+"'");
		return this;
	};

	/**
	 * 
	 */
	this.setOptions = function(options)
	{
		this.options = options;
		return this;
	};

	/**
	 * 
	 * @param parentJqElement
	 */
	this.initApp = function(parentJqElement)
	{
		parentJqElement.find(".rb-popup").click(function(e){
			e.preventDefault();
			var width = $(this).data("width");
			var height = $(this).data("height");
			var url = $(this).prop("href");
			var target = $(this).prop("title");
			Ranchbe.win.popupP(url, target , height , width);
			return false;
		});

		parentJqElement.find(".rb-openintop").click(function(e){
			e.preventDefault();
			var href = $(this).attr("href");
			/* the main window must be set with the name defined here by set name as 'window.name="main rb window";' */
			var mainWindow = window.open(href, "main rb window");
			mainWindow.focus();
			return false;
		});

		/* previous page button */
		parentJqElement.find('.rb-openreferrer').click(function (e) {
			e.preventDefault();
			window.location = document.referrer;
			return false;
		});

		/* FLASH MESSENGER */
		/* AJAX FEEDBACKS */
		var rbFlashMessenger=new RbFlashMessenger(ranchbe);
		rbFlashMessenger.init(parentJqElement.find(".rb-feedbacks"));

		/*
		parentJqElement.find(".rb-feedbacks").alert();
		parentJqElement.find( ".rb-flash-message,.rb-flash-success,.rb-flash-warning" ).delay( 1000 ).fadeOut( 1000 );
		parentJqElement.find( ".btn-redisplay-messages" ).click(function(e){
			$(".rb-flash").show().delay( 10000 ).fadeOut( 1000 );
		});
		parentJqElement.find("#rb-ajax-error").hide();
		parentJqElement.find("#rb-ajax-feedback").hide();
		if(ranchbe.options.debug==false){
			$("#rb-ajax-error").find(".rb-debugdata").remove();
		}
		*/

		/* beautiful multi-select */
		parentJqElement.find('.selectpicker').each(function () {
			var selectpicker = $(this);
			$.fn.selectpicker.call(selectpicker, selectpicker.data());
		});

		$("#search-form").submit(function(event){
			var btn = $("#search-object-selector");
			var input = $("#search-object-input");
			btn.addClass("btn-danger");
			input.attr("title", "You must select type of objects to search");
			input.data("toggle", "tooltip");
			input.data("placement", "bottom");
			input.tooltip();
			input.tooltip('show');
			return false;
		});

		$(".searchindocument-btn").click(function(e){
			var form = $(this).parents("form").first();
			form.attr('action',document.baseurl+'/search/fordocument');
			form.unbind( "submit");
			form.submit();
		});

		$(".searchinfiles-btn").click(function(e){
			var form = $(this).parents("form").first();
			form.attr('action',document.baseurl+'/search/forfile');
			form.unbind( "submit");
			form.submit();
		});

		$(".searchincontainer-btn").click(function(e){
			var form = $(this).parents("form").first();
			form.attr('action',document.baseurl+'/search/forcontainer');
			form.unbind( "submit");
			form.submit();
		});

		$(".searchinproducts-btn").click(function(e){
			var form = $(this).parents("form").first();
			form.attr('action',document.baseurl+'/search/forproduct');
			form.unbind( "submit");
			form.submit();
		});

		if(this.options.connFrom == "distant"){
			Ranchbe.distantAccess();
		}
	};

	/**
	 * 
	 */
	this.loadWaiting = function()
	{
		return $('#rb-load-waiting');
	};

	/**
	 * 
	 */
	this.activateLoadWaiting = function()
	{
		var loadWaiting = this.loadWaiting();
		$(document).on({
			ajaxStart: function(){
				console.log('ajaxStart');
				loadWaiting.show(50);
			},
			ajaxStop: function(){
				loadWaiting.hide();
				console.log('ajaxStop');
			}
		});
	};

	/**
	 * 
	 */
	this.unactiveLoadWaiting = function()
	{
		$(document).off('ajaxStart');
		$(document).off('ajaxStop');
	};

	/**
	 * 
	 */
	this.getModal = function()
	{
		return $("#rb-popup-modal");
	};

	/**
	 * 
	 * @returns
	 */
	this.emptySelectionAlert = function()
	{
		var pmsg = $( '#' + this.dialog + " > p" )[0];
		msg = eval('msg_select_one_item');
		pmsg.textContent = msg;
		$('#' + this.dialog).dialog({
			title: eval('msg_empty_selection'),
			modal: true,
			hide: 'explode',
			show: 'explode',
			buttons: {"Ok": function() {$( this ).dialog( 'close' );}}
		});
		return true;
	};

	/**
	 * 
	 * @returns
	 */
	this.dialog = function( options )
	{
		elemtId = options.id;
		defOpt = {
				height: 200,
				width: 300,
				modal: true,
				title: 'title_create_object',
				html: '',
				hide: 'explode',
				show: 'explode',
				buttons: {}
		};
		jQuery.extend( defOpt, options );

		/* add a close button in all case */
		defOpt.buttons.Close = function(){
			$( this ).dialog( "close" );
			$( this ).dialog( "destroy" );
			$( this ).remove();
		};
		/* destroy existing window */
		$('#' + elemtId).remove();
		$('body').append('<div id=' + elemtId + '></div>');
		$('#'+elemtId).html(defOpt.html);
		return $( '#' + elemtId ).dialog(defOpt);
	};

	/**
	 * 
	 */
	this.confirmDialogBox = function(title, message, continueCallback) 
	{
		var id = 'rb-dialog-confirm';
		if($("#"+id).length == 0){
			$("body").append('<div id="'+id+'"></div>');
		}
		var confirmDialog = $("#"+id);
		confirmDialog.attr('title', title);
		confirmDialog.html(message);

		confirmDialog.dialog({
			resizable: true,
			modal: true,
			buttons: {
				"Ignore And Continue": function() {
					continueCallback();
					$(this).dialog("close");
				},
				"Cancel": function() {
					$(this).dialog("close");
				}
			}
		});
	};

	/**
	 * 
	 */
	this.getFlash = function()
	{
		if(this.flash == null){
			this.flash = new RbFlashMessenger(this);
			this.flash.init($("div.rb-feedbacks"));
		}
		return this.flash;
	};

	/**
	 * 
	 */
	this.getAjax = function()
	{
		if(this.rbajax == null){
			this.rbajax = new RbAjax(this);
		}
		return this.rbajax;
	};

	/**
	 * 
	 */
	this.jsonEditor = function()
	{
		this.jsonEditor = new RbJsonEditor(this);
		return this.jsonEditor;
	};

	/**
	 * 
	 */
	this.initDate = function()
	{
		this.include("js/datetimepicker/build/jquery.datetimepicker.full.min.js");
		this.includeCss("js/datetimepicker/build/jquery.datetimepicker.min.css");

		$( ".datepicker" ).datepicker({
			showWeek: true,
			dateFormat:"dd-mm-yy",
			firstDay: 1
		});
		$( ".datepicker" ).datepicker( "option", $.datepicker.regional[ "fr" ] );

		$( ".datetimepicker" ).datetimepicker({
			showWeek: true,
			formatDate:"dd-mm-yy",
			formatTime:"H:i",
			minDate:0,
			minTime:false,
			maxTime:false,
			//allowTimes: ['12:00','13:00','14:00','15:00','16:00','17:00','18:00','20:00','21:00','00:00'],
			roundTime:'floor',
			firstDay: 1,
			lang: "fr",
			step: 5
		});
	};

	/**
	 */
	this.initPaginator = function(jqPaginatorForm)
	{
		if(jqPaginatorForm.data('isInitialized') == true){
			return;
		}
		jqPaginatorForm.data('isInitialized', true);

		var pageSelector = jqPaginatorForm.find("#paginator-page");
		var limitSelector = jqPaginatorForm.find("#paginator-limit");
		var nextButton = jqPaginatorForm.find("#paginator-next");
		var prevButton = jqPaginatorForm.find("#paginator-prev");

		/* remove resetf parameter */
		var url = location.origin+location.pathname+location.search.replace("resetf=&","").replace("&resetf=resetf","");
		jqPaginatorForm.attr("action", url);

		limitSelector.off('change').change(function(e){
			jqPaginatorForm.submit();
		});
		pageSelector.off('change').change(function(e){
			jqPaginatorForm.submit();
		});
		nextButton.off('click').click(function(e){
			var page = pageSelector.val();
			page++;
			pageSelector.val(page);
			jqPaginatorForm.submit();
		});
		prevButton.off('click').click(function(e){
			var page = pageSelector.val();
			page--;
			pageSelector.append($('<option>',{
				value:page,
				text:page
			}));
			pageSelector.val(page);
			jqPaginatorForm.submit();
		});
	};

	/**
	 * Include a script only if is not yet loaded
	 */
	this.include = function(script)
	{
		script = this.options.baseurl + '/' + script + '?reload='+this.options['jsReload'];
		var s = $('script[src|="' + script + '"]');
		var ranchbe = this;

		if ( s.length == 0 && this.dynamicScripts.indexOf(script) == -1 ) {
			$.ajax(script, {
				async: false,
				dataType: "script",
				cache: true,
				success: function(data, textStatus, xhr){
					try{
						ranchbe.dynamicScripts.push(script);
						console.log('load script :' + script);
					}
					catch(e){
						console.log("Load of script order problem:", e, data);
					}
				}
			});
			return;
		}
		else{
			console.log('script is yet loaded :' + script);
			return;
		}
	};

	/**
	 * Include a css sheet only if is not yet loaded
	 */
	this.includeCss = function(sheet)
	{
		sheet = this.options.baseurl + '/' + sheet + '?reload=' + this.options['cssReload'];
		var s = $('link[href="' + sheet + '"]');

		if ( s.length == 0 ) {
			console.log('load css :' + sheet);
			$('<link href="' + sheet + '" rel="stylesheet">').appendTo("head");
			return;
		}
		else{
			console.log('css is yet loaded :' + sheet);
			return;
		}
	};
};

/**
 * execute by wicoti to remove some functionnalities
 */ 
Ranchbe.wicoti = function(){
	Ranchbe.distantAccess();
	$('.rb-openintop').off('click');
	$('.rb-popup').off('click');
	$('.rb-openintop').click(function(e){return true;});
	$('.rb-popup').click(function(e){return true;});
};

/**
 * execute by wicoti to remove some functionnalities
 */ 
Ranchbe.distantAccess = function(){
	$('a.checkout-btn').parent().remove();
	$('a.checkin-btn').parent().remove();
	$('a.checkinandkeep-btn').parent().remove();
	$('a.putinws-btn').parent().remove();
	$('a.associatefile-btn').parent().remove();
	$('a.associatevisu-btn').parent().remove();
};



Ranchbe.formater = {
		formatFilesize: function(filesize, options, rowObject){
			if (filesize >= 1073741824) {
				filesize = this.formatNumber(filesize / 1073741824, 2, '.', '') + ' Go';
			} else { 
				if (filesize >= 1048576) {
					filesize = this.formatNumber(filesize / 1048576, 2, '.', '') + ' Mo';
				} else { 
					if (filesize >= 1024) {
						filesize = this.formatNumber(filesize / 1024, 0) + ' Ko';
					} else {
						filesize = this.formatNumber(filesize, 0) + ' octets';
					};
				};
			};
			return filesize;
		},
		formatNumber: function ( number, decimals, dec_point, thousands_sep ) {
			// http://kevin.vanzonneveld.net
			// +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
			// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
			// +     bugfix by: Michael White (http://crestidg.com)
			// +     bugfix by: Benjamin Lupton
			// +     bugfix by: Allan Jensen (http://www.winternet.no)
			// +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)    
			// *     example 1: number_format(1234.5678, 2, '.', '');
			// *     returns 1: 1234.57     
			var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
			var d = dec_point == undefined ? "," : dec_point;
			var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
			var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
			return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		},
};

Ranchbe.win = {

		/**
		 * Fonction pour afficher une fenetre pop-up dont la taille est parametrable
		 */
		popup: function (url, usero)
		{
			var options = {width:800, height:600, windowName:url};
			jQuery.extend( options, usero );
			//pageName = jQuery.rb.baseUrl + '/' + jQuery.rb.trim_slash(options.pageName);
			var config = "height=" + options.height + ", width=" + options.width + ", toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=yes, directories=no, status=no";
			window.open (url, options.windowName, config);
		},


		/**
		 *  Fonction pour afficher une fenetre pop-up dont la taille est parametrable 
		 */
		popupP: function(pageUrl, windowName , height , width)
		{
			var options = "height=" + height + ", width=" + width + ", toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=yes, directories=no, status=no";
			if(pageUrl.indexOf("?") > 0){
				pageUrl=pageUrl+'&layout=popup';
			}
			else{
				pageUrl=pageUrl+'?layout=popup';
			}

			/* put index to end */
			var urlIndex = pageUrl.split('#');

			if (typeof urlIndex[1] !== 'undefined'){
				pageUrl = urlIndex[0];
				var urlIndex = urlIndex[1].split('&');
				pageUrl = pageUrl+'&'+urlIndex[1];
				var urlIndex = urlIndex[0];
				pageUrl=pageUrl+'#'+urlIndex;
			}

			popup = window.open(pageUrl, windowName, options);
			popup.focus();

			$(popup).ready(function(){
				popup.name = windowName;
				popup.focus();
				return false;
			});

			return false;
		},

		/**
		 * 
		 */
		openParentTab: function() {
			locationHash = location.hash.substring(1);
			console.log(locationHash);
			// Check if we have an location Hash
			if (locationHash) {
				var divContent = $('#'+locationHash);
				if (divContent.length) {
					var parentTab = divContent.parents(".contentTab");
					if(parentTab.length){
						// Grab the index number of the parent tab so we can activate it
						var tabNumber = parentTab.index()-1;
						$(".tabs").first().tabs({ active: tabNumber });
						// Not need but this puts the focus on the selected hash
						divContent.get(0).scrollIntoView();
					}
				}
			}
		}
}

/**
 * globale function to generate uniqid
 */
Ranchbe.uniqid = function uniqid(a = "", b = false){
	var c = Date.now()/1000;
	var d = c.toString(16).split(".").join("");
	while(d.length < 14){
		d += "0";
	}
	var e = "";
	if(b){
		e = ".";
		var f = Math.round(Math.random()*100000000);
		e += f;
	}
	return a + d + e;
}

/**
 * 
 */
Ranchbe.url = {
		/**
		 * Remove parts after ? of a url
		 * 
		 * @returns {___anonymous671_715}
		 * Clean resetf too
		 * 
		 */
		getUrlWithoutSort: function(){
			//Url without params
			var url = window.location.origin + window.location.pathname;
			var output = {
					order:null,
					orderby:null,
					url:url
			};

			/* put url paramters in object parameters without order and orderby */
			if (window.location.search.length > 1) {
				var aCouples = window.location.search.substr(1).split('&');
				var aItKey, nKeyId;
				var begin = false;
				for (nKeyId = 0; nKeyId < aCouples.length; nKeyId++) {
					aItKey = aCouples[nKeyId].split('=');
					if (aItKey[0] == 'order') {
						output.order=aItKey[1];
					}
					else if (aItKey[0] == 'orderby') {
						output.orderby=aItKey[1];
					}
					else{
						if(begin == false){
							url = url + "?" + aCouples[nKeyId];
							begin = true;
						}
						else{
							url = url + "&" + aCouples[nKeyId];
						}
					}
				}
			}
			output.url = url;
			return output;
		}
}


/**
 * function to suppress space and other specials characters of a string
 * use as: string.normalize()
 */
String.prototype.normalize = function(){
	var accent = [
		/[\300-\306]/g, /[\340-\346]/g, // A, a
		/[\310-\313]/g, /[\350-\353]/g, // E, e
		/[\314-\317]/g, /[\354-\357]/g, // I, i
		/[\322-\330]/g, /[\362-\370]/g, // O, o
		/[\331-\334]/g, /[\371-\374]/g, // U, u
		/[\321]/g, /[\361]/g, // N, n
		/[\307]/g, /[\347]/g, // C, c
		];
	var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];

	var str = this;
	for(var i = 0; i < accent.length; i++){
		str = str.replace(accent[i], noaccent[i]);
	}
	str = str.replace(" ", ""); //supprime les espaces
	str = str.toLowerCase(); //minuscules

	return str;
};

/** 
 * supprime les / de début et fin 
 */
String.prototype.trimSlash = function (astring)
{
	var regExpBeginning = /^\/+/;
	var regExpEnd = /\/+$/;
	astring = astring.replace(regExpBeginning, "").replace(regExpEnd, "");
	return astring;
};


/**
 * 
 */
function RbManager(jqElement)
{
	/* @var jquery object */
	this.jqElement = jqElement;

	/* @var RbTable */
	this.table = null;

	/* @var RbObjectBox */
	this.box = null;

	/* @var RbObjectBox */
	this.buttonbox = null;

	/* @var RbService */
	this.service = null;

	/**/
	this.init = function(refreshCallback){
		ranchbe.initPaginator($("form#paginator"));

		/**/
		if(!refreshCallback){
			refreshCallback = function(e){
				$("#content").load("#", function(event){
					ranchbe.initApp($("#content"));
				});
				return false;
			}
		}
		this.jqElement.find(".refresh-btn").click(refreshCallback);

		return this;
	};

	/* INIT TABLE SORTABLE HEADERS */
	this.initTableHeader = function(orderby, order)
	{
		var url = Ranchbe.url.getUrlWithoutSort(window.location.origin + window.location.pathname).url;
		this.table.initHeader(orderby, order, url);
		return this;
	};

	/** 
	 * INIT ROW SELECTOR
	 * @var string selector css selector to select parent element where found the checkbox use for select
	 */
	this.initTableSelectable = function(selector)
	{
		if(this.table){
			this.table.markrowsInit(selector);
		}
		else{
			console.log('Error: this.table is not set. You must call initTable() before');
		}
		return this;
	};

	/**
	 * INIT TABLE
	 * @var string selector css selector for select table element
	 */
	this.initTable = function(selector)
	{
		if(!this.table){
			this.table = new RbTable( this.jqElement.find(selector) );
		}
		return this;
	};

	/**
	 * INIT TABLE AND CONTEXTUAL MENU
	 * @var string selectorForUl css selector for select parent element where is define the context menu
	 */
	this.initTableCm = function(selectorForUl, trSelector)
	{
		if(!this.table.contextMenu){
			ulElement = $(selectorForUl).first();
			rbButtons = this.buttonsFactory(ulElement);
			this.table.initContextMenu(rbButtons, this.service, trSelector);
		}
		return this;
	};

	/**
	 * INIT TABLE AND CONTEXTUAL MENU
	 * @var string selectorForUl css selector for select parent element where is define the context menu
	 * @var string objectSelector css selector for select element with menu on right click
	 */
	this.initObjectCm = function(selectorForUl, box)
	{
		ulElement = this.jqElement.find(selectorForUl).first();
		rbButtons = this.buttonsFactory(ulElement);
		box.initContextMenu(rbButtons, this.service, trSelector);
		return this;
	};

	/** 
	 * BUTTONS BOX
	 * @var string boxSelector css selector for box elements
	 */
	this.initButtonBox = function(boxSelector)
	{
		if(!this.buttonbox){
			box = new RbObjectBox(this.jqElement.find(boxSelector));
			box.init();
			box.initButtons(this.buttonsFactory(box.jqElement));
			this.buttonbox = box;
		}
		return this;
	};

	/** 
	 * OBJECT BOX init some functionnalities and buttons
	 * 
	 * @var string boxSelector css selector for box elements
	 */
	this.initObjectBox = function(boxSelector)
	{
		if(!this.box){
			box = new RbObjectBox(this.jqElement.find(boxSelector));
			box.init();
			box.initButtons(this.buttonsFactory(box.jqElement));
			this.box = box;
		}
		return this;
	};

	/**
	 * Get context menu
	 * @var jqParentElement jquery element with data definition for actions. Must have class rb-submit-data.
	 * @return RbButtons
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		alert("Not implemented");
	};
}

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbService()
{

	/**
	 * Serialize form as required by rb service
	 */
	this.serializeForm = function(form)
	{
		formdata = form.serializeArray();
		serviceData = this.queryDataToServiceData(formdata);
		return serviceData;
	};


	/**
	 * Convert queryData to array structure as requiered by rb service
	 * 
	 */
	this.extractQueryFromData = function(queryData){
		throw 'ERROR: extractQueryFromData must be implemented';
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.queryDataToServiceData = function(elemt){
		throw 'ERROR: queryDataToServiceData must be implemented';
	};
};

/* Container for helper to init json editor */
function RbJsonEditor(ranchbe)
{
	/**
	 * 
	 */
	this.ranchbe = ranchbe;

	/**
	 * 
	 */
	this.init = function(inputElemt, mode='view'){
		ranchbe.include("js/jsoneditor/dist/jsoneditor.js");
		ranchbe.includeCss("js/jsoneditor/dist/jsoneditor.css");

		if(inputElemt.attr('id') == undefined){
			inputElemt.attr('id', inputElemt.attr('name'));
		}

		inputElemt.hide();
		container = $('<div class="rb-json-editor"></div>');
		container.attr('id', "jsoneditor-" + inputElemt.attr('id'));
		inputElemt.after(container);
		inputElemt.unbind('click');

		/* create the editor */
		var options = {
				"mode": "tree",
				"search": true,
				"modes":['view', 'tree', 'code']
		};

		var editor = new JSONEditor(container.get(0), options);
		editor.setMode(mode);

		var json = inputElemt.val();
		if(json != undefined && (json.substring(0,1) == '{' || json.substring(0,1) == '[')){
			editor.set(JSON.parse(json));
		}

		/* set handler on form save */
		validationCallback = function(event){
			var json = editor.get();
			inputElemt.val(JSON.stringify(json, null, 2));
			inputElemt.show();
			return true;
		};

		editor.validationCallback = validationCallback;
		inputElemt.data('jsonEditor', editor);

		form = inputElemt.parents('form').first();
		form.on("validate", validationCallback);
		form.on("submit", validationCallback);
	}
}

/**
 * 
 */
function RbFlashMessenger(ranchbe)
{
	/**
	 * Include a script only if is not yet loaded
	 */
	this.init = function(baseJqElement)
	{
		this.element = baseJqElement;
		baseJqElement.alert();

		/* FLASH MESSENGER */
		baseJqElement.find( ".rb-flash-message,.rb-flash-success,.rb-flash-warning" ).delay( 1000 ).fadeOut( 1000 );
		baseJqElement.find( ".btn-redisplay-messages" ).click(function(e){
			$(".rb-flash").show().delay( 10000 ).fadeOut( 1000 );
		});

		/* AJAX FEEDBACKS */
		baseJqElement.find("#rb-ajax-error").hide();
		baseJqElement.find("#rb-ajax-feedback").hide();
		if(ranchbe.options.debug == false){
			$("#rb-ajax-error").find(".rb-debugdata").remove();
		}
	};
};

/**
 * 
 */
function RbView()
{
};

/**
 * 
 */
function RbContextMenuDefinition(jqMenuDefinition)
{
	/**
	 * the main div where is define the menu
	 */
	this.jqElement = jqMenuDefinition;

	/**
	 * Return the Jquery element
	 */
	this.getJqueryElement = function()
	{
		return this.jqElement;
	}

	/**
	 *
	 */
	this.init = function(actionProcessor)
	{
		this.actionProcessor = actionProcessor;
		alert("RbContextMenuDefinition.init() must be implemented");
	}
}

/**
 * jqMenuDefinition the jQuery main div element of the menu definition
 */
function RbContextMenu(jqMenuDefinition)
{
	this.jqMenuDefinition = jqMenuDefinition;

	/**
	 *  display menu entry only if assertion is true 
	 */
	this.filter = function(multiSelect, onElement)
	{
		(this.jqMenuDefinition.find('li')).each(function(i, elemt){
			var assert = $(elemt).data('assert');
			if(assert){
				var display = eval(assert);
			}
			else{
				var display = true;
			}

			if(display==true){
				$(elemt).show();
			}
			else{
				$(elemt).hide();
			}
		});
	};

	/**
	 * 
	 */
	this.initMenuDefinition = function()
	{
		/* when leave, hide menu, with delay */
		this.jqMenuDefinition.hover(
				function(e){
					$(this).stop(true, true).show(300);
				},
				function(e){
					$(this).stop().delay(700).hide(300);
				}
		);
		var menu = this.jqMenuDefinition.hide().menu();

		/* when click on line, trigger button !must be call after menu init! */
		/* and close menu => function a probleme a definir */
		this.jqMenuDefinition.find(".ui-menu-item").click(function(e){
			e.stopPropagation();
			$(this).children("a").first().click();
			return false;
		});
		return menu;
	};

	/**
	 * Add a title in menuDefinition header
	 */
	this.setTitle = function(title){
		this.jqMenuDefinition.find("li.cm-header").html(title);
	}

	/**
	 * 
	 */
	this.show = function(e)
	{
		/* arrange display of menu */
		var menu = this.jqMenuDefinition.show().position({
			my: "left center",
			at: "center",
			of: e
		});
	};

	/** 
	 * Search in fromJqElement or in his ancestor a element with class .rb-submit-data.
	 * If found, extract data to submit from data-* attributes
	 */
	this.getSubmitData = function(fromJqElement){
		if(fromJqElement.hasClass("rb-submit-data")){
			var submitDatas = fromJqElement;
		}
		else{
			var submitDatas = fromJqElement.parents(".rb-submit-data").first();
		}
		if(submitDatas == undefined){
			throw "the element or his ancestor must be a .rb-submit-data with data-* setted";
		}
		return submitDatas;
	};
};  /* End of object */

/**
 * 
 */
function RbAction()
{
	this.options = {
			type: "GET",
			dataType:'json',
			hash:"",
			confirmMessage:"",
			msgbox:"",
			queryData:null,
			callback:null,
			successCallback:null,
			failedCallback:null,
			popup:{xsize:null,ysize:null,target:null}
	};

	/*build and send request to service */
	this.toService = function(e, clickedElement, url, options){alert("toService to implements")};

	/* get and show form or other datas in modal window */
	this.getModal = function(e, clickedElement, url, options){alert("getModalForm to implements")};

	/* return query datas formatted for service */
	this.getServiceRequestData = function(e, clickedElement, url, options){alert("getServiceRequestData to implements")};

	/* Build query data from data on selected row from elemts.rb-submit-data */
	this.getSelectedData = function(){alert("getSelectedData to implements")};

	/**
	 * Convert data array to serialized datas as return by serialize form function or getSelectedData
	 */
	this.selectedDataToQueryData = function(selectedData)
	{
		var queryData = [];
		$.each(selectedData, function(i,datas){
			if(datas['id'] != undefined){
				id = datas['id'] || '';
			}
			else if(datas['file'] != undefined){
				id = datas['file'] || '';
			}
			queryData.push({
				name: 'checked[]',
				value: id
			});
			if(datas['spacename'] != undefined){
				queryData.push({
					name: 'spacenames['+id+']',
					value: datas['spacename'] || ''
				});
			}
		});
		return queryData;
	};

	/**
	 * make a http get request on url with some options
	 * Search rb-submit-data in parents of clickedElement
	 */
	this.getRequest = function(e, clickedElement, url, options){
		var myOptions = $.extend({}, this.options, options);

		/* highlight button */
		this.highlight(clickedElement);

		/* extract queryData from table form */
		if(myOptions.queryData == null){
			var submitDatas = clickedElement.parents(".rb-submit-data").first();
			myOptions.queryData = submitDatas.data();
		};

		/* build url */
		url = this.buildUrl(url, $.param(myOptions.queryData), myOptions.hash);

		/* set popup option */
		if(myOptions.popup.xsize){
			popupP(url, myOptions.popup.target , myOptions.popup.ysize , myOptions.popup.xsize);
		}
		else{
			document.location.href = url;
		}
	};

	/**
	 * 
	 */
	this.serviceRequest = function(e, clickedElement, url, options){
		var myOptions = $.extend({}, this.options, options);
		this.highlight(clickedElement);
		var ajax = new RbAjax();
		ajax.serviceRequest(e, clickedElement, url, myOptions);
	};

	/**
	 * 
	 */
	this.modalPopup = function(url,options){
		var div = $("#rb-popup-modal");
		div.empty();
		div.css("z-index",1041);

		if(options.popup.xsize){
			div.css("width",options.popup.xsize);
		}
		if(options.popup.ysize){
			div.css("height",options.popup.ysize);
		}

		/* called after display of modal */
		completeCallback = function(responseText, textStatus, jqXHR){
			if(options.callback != null){
				options.callback(responseText, textStatus, jqXHR);
			}
			if(textStatus=="error"){
				//if(jqXHR.status==500 || jqXHR.status==404 || jqXHR.status==403){}
				/* respons is a json ranchbe service return */
				if(responseText[0]=='{'){
					var respons = JSON.parse(responseText);
					var rbAjax = new RbAjax();
					rbAjax.errorDisplay(respons.errors);
					div.modal("hide");
				}
				else{
					div.empty().append(responseText);
					div.find("#content").append('<div class="help">Click out this panel to close it</div>');
				}
			}
		}

		div.load(url, completeCallback).modal("show");
		return div;
	};

	/**
	 * 
	 */
	this.dialog = function(url,options){
	};

	/**
	 * Higlight button
	 * and hide context menu if is cm button
	 */
	this.highlight = function(clickedElement){
		clickedElement.effect("highlight", {}, 500);
		clickedElement.parents("ul.ul-contextmenu").first().stop().hide(0);
	};

	/**
	 * build a url from url and params.
	 * params is a string of the parameters of the url that must be combined with url.
	 * if url has or not a parameters section, add or not the ? separator.
	 * 
	 * @param url string
	 * @param params string
	 * @param hash string
	 * @return string
	 */
	this.buildUrl = function(url, params, hash){
		var urlDecomposed = url.split('?');
		var baseUrl = urlDecomposed[0];
		var searchUrl = urlDecomposed[1];

		if(searchUrl == undefined || searchUrl == ""){
			url = url + '?'+params;
		}
		else{
			url = url + '&'+params;
		}

		if(hash){
			url = url + hash;
		}

		return url;
	};

	/**
	 * build a url from url and params.
	 * params is a string of the parameters of the url that must be combined with url.
	 * if url has or not a parameters section, add or not the ? separator.
	 * 
	 * @param url string
	 * @param params string
	 * @param hash string
	 * @return string
	 */
	this.buildUrlFromRoute = function(url, params, hash){
		var urlDecomposed = url.split('?');
		var baseUrl = urlDecomposed[0];
		var searchUrl = urlDecomposed[1];

		if(searchUrl == undefined || searchUrl == ""){
			url = url + '?'+params;
		}
		else{
			url = url + '&'+params;
		}

		if(hash){
			url = url + hash;
		}

		return url;
	};

	/**
	 * Remove all ui- key from datas
	 */
	this.filterSubmitData = function(datas){
		filtered = {};
		$.each(datas, function(name, value){
			if(name.substring(0,3) != "ui-"){
				filtered[name] = value;
			}
		})
		return filtered;
	};

	/** 
	 * Search in fromJqElement or in his ancestor a element with class .rb-submit-data.
	 * If found, extract data to submit from data-* attributes
	 */
	this.getSubmitData = function(fromJqElement){
		if(fromJqElement.hasClass("rb-submit-data")){
			var submitDatas = fromJqElement;
		}
		else{
			var submitDatas = fromJqElement.parents(".rb-submit-data").first();
		}
		if(submitDatas == undefined){
			throw "the element or his ancestor must be a .rb-submit-data with data-* setted";
		}

		return submitDatas;
	};
};  /* End of object */

/**
 * 
 */
function RbButtons(jqParentElement)
{
	this.jqElement = jqParentElement;

	/**
	 * Return the Jquery element
	 */
	this.getJqueryElement = function()
	{
		return this.jqElement;
	}

	/**/
	this.reload = function(){
		$("#content").load("#", function(){});
		return false;
	};
};

/**
 * 
 */
function RbAjax()
{

	/**
	 * @param e Event
	 * @param domElemt Button or Ahref
	 * @param url string url
	 * @param options array
	 */
	this.serviceRequest = function(e, domElemt, url, options)
	{
		var myAjax = this;
		$.ajax({
			async: true,
			type : options.type,
			url : url,
			data : options.queryData,
			dataType: options.dataType,
			success : function(data, textStatus, jqXHR){
				var respons = data;

				if(options.successCallback){
					e.respons = respons;
					e.jqXHR = jqXHR;
					e.textStatus = textStatus;
					options.successCallback(e,domElemt);
				}

				/* display to user */
				if(options.dataType=='json' && options.messageDisplay==true){
					myAjax.messageDisplay(respons.feedbacks);
				}

				return false;
			},
			error : function(data, textStatus, jqXHR){
				var respons = data.responseJSON;

				/* display errors to user */
				if(respons){
					if(respons.errors){
						myAjax.errorDisplay(respons.errors);
					}
					if(respons.exception){
						myAjax.exceptionDisplay(respons.exception);
					}
					if(options.failedCallback){
						e.respons = respons;
						options.failedCallback(e,domElemt);
					}
				}
				/* assume that data is a html page returned */
				else{
					/* @var RbAction e.rbAction, must be setted in the caller */
					if(e.rbAction){
						e.rbAction.redisplay(data.responseText);
						if(options.failedCallback){
							e.respons = data.responseText;
							options.failedCallback(e,domElemt);
						}
					}
					else{
						error={
								message: data.responseText,
								code: "",
								file: "",
								line: "",
								trace: "",
						};
						myAjax.errorDisplay({error1:error});
					}
				}
				return false;
			}
		});
		return false;
	};

	/**
	 */
	this.errorDisplay = function(errors, debug=false)
	{
		$.each(errors, function(index,item){
			var boxPrototype = $("#rb-ajax-error");
			var box = boxPrototype.clone(true); /* with events */
			box.attr("id", "rb-ajax-error-"+index);
			box.addClass("rb-ajax-error");
			if(typeof(item) == 'string'){
				box.find("#message").html(item);
			}
			else{
				box.find("#message").html(item.message);
				box.find("#code").html(item.code);
				box.find("#file").html(item.file);
				box.find("#line").html(item.line);
				box.find("#trace").html(item.trace);
			}

			if(debug==false){
				box.find(".rb-debugdata").hide();
			}

			boxPrototype.parent().append(box);
			box.show();
		});
		return false;
	};

	/**
	 */
	this.exceptionDisplay = function(exception)
	{
		var boxPrototype = $("#rb-ajax-error");
		var box = boxPrototype.clone(true); /* with events */
		box.attr("id", "rb-ajax-error-exception");
		box.addClass("rb-ajax-error");
		box.find("#message").html(exception.message);
		box.find("#code").html(exception.code);
		box.find("#file").html(exception.file);
		box.find("#line").html(exception.line);
		box.find("#trace").html(exception.trace);
		boxPrototype.parent().append(box);
		box.show();
		return false;
	};

	/**
	 */
	this.messageDisplay = function(feedbacks)
	{
		var msg = '';
		$.each(feedbacks, function(index,item){
			if(typeof(item) == 'string'){
				msg = msg + '<div>' + item + '</div>';
			}
			else{
				if(item.message==""){
					return;
				}
				msg = msg + '<div>' + item.message + '</div>';
			}
		});

		var boxPrototype = $("#rb-ajax-feedback");
		var box = boxPrototype.clone(true); /* with events */
		box.attr("id", "rb-ajax-feedback-1");
		box.addClass("rb-ajax-feedback");
		box.find("#message").html(msg);
		boxPrototype.parent().append(box);
		box.show().delay( 2000 ).fadeOut( 1000 );

		return false;
	};

}; /* End of object */

/**
 * 
 */
function RbCookie()
{

	/** 
	 * Set a cookie var
	 * 
	 * @var string cname
	 * @var string cvalue
	 * @var integer exdays Num of day before expiration
	 */
	this.set = function(cname, cvalue, exdays) {
		var d = new Date();
		/* set time in 1/1000eme of seconds */
		d.setTime(d.getTime() + (exdays * 24 * 3600 * 1000));
		var expires = "expires="+d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	};

	/**
	 * Get a cookie var
	 * 
	 * @var string cname
	 */
	this.get = function(cname)
	{
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	};
}; /* End of object */


//# sourceURL=/ranchbe/dynamic/rb/model/rb.model.js

