/**
 * 
 */
function RbMetadataManager(jqElement)
{
	this.service = new RbMetadataService();
	this.jqElement = jqElement;

	/* @var RbMetadataButtons */
	this.rbButtons = null;
	
	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbMetadataButtons(jqParentElement);
		return rbButtons;
	};
}
RbMetadataManager.prototype = new RbManager();

/**
 * 
 */
function RbMetadataButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * @param array datas input datas as extracted from element
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			switch(item.name){
				case 'checked[]':
				case 'id':
					if(url.search('%id%') > 0){
						url = url.replace('%id%', item.value);
					}
					else{
						item.name = 'checked[]';
						queryData.push(item);
					}
					break;
				case 'spacename':
					url = url.replace('%spacename%', item.value);
					break;
				default:
					if(item.name.match('^spacenames(\[[0-9\'\"]+\])')){
						url = url.replace('%spacename%', item.value);
					}
					else{
						queryData.push(item);
					}
				break;
			}
		});
		url = url.replace('/%id%', '');
		url = url.replace('/%spacename%', '');
		return url;
	}
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;

		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/ged/metadata/%spacename%/create';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getModal(e, $(this), url, {
				queryData: queryData,
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#metadataEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/ged/metadata/%spacename%/edit';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getModal(e, $(this), url, {
				queryData: queryData,
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#metadataEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".delete-btn").click(function(e){
			var url = document.baseurl+'/ged/metadata/%spacename%/delete';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			if(confirm('Do you want really suppress this metadata?')){
				actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			}
			return false;
		});
	};
}
RbMetadataButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbMetadataService()
{

	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
			if(item.name.substring(0,11) == "spacenames["){
				eval(item.name + "='" + item.value + "'");
			}
		});
		
		var i=1;
		var serviceData = {'metadatas':{}};
		$.each(checked, function(index, id){
			serviceData.metadatas['metadata'+i] = {
				id: id,
				spacename: spacenames[id]
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {metadatas:{
			metadata1:{
				id:elemt.data('id'),
				spacename:elemt.data('spacename')
			}
		}};
		return queryData;
	};
}
RbMetadataService.prototype = new RbService();
