/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbBatchEditor()
{
	/**
	 * 
	 */
	this.options = {
			baseurl: document.baseurl,
			url:"import/batch",
			spacename: ""
	};

	/**
	 * 
	 */
	this.setOption = function(name,value)
	{
		eval("this.options."+name+"='"+value+"'");
		return this;
	};

	/**
	 * 
	 */
	this.setOptions = function(options)
	{
		this.options = options;
		return this;
	};

	/**
	 * 
	 */
	this.setForm = function(form)
	{
		this.form = form;
		return this;
	};

	/**
	 * 
	 */
	this.ping = function()
	{
		console.log("hello!");
	};

	/**
	 * docfile/service/delete
	 */
	this.buttonTrigger = function(e,button)
	{
		var actionName = button.attr('name');
		return eval("this." + actionName + "Action(e,button)");
	};
	
	/**
	 * 
	 */
	this.initPackagelist = function(selector)
	{
		this.packagelist = $(selector);
		this.packagelist.sortable();
		this.packagelist.disableSelection();
		var batchEditor = this;
		
		this.packagelist.children("li p.tid").bind('click', null, function(e){
			var li = this.parentNode;
			batchEditor.mockupSelectWin.dialog({title:'Select a target', width:1000, buttons: [{
				text: "Ok",
				click: function(e) {
					//get the selected line id of grid
					var table = batchEditor.mockupSelectWin.children("table").first();
					var selrow = table.jqGrid('getGridParam', 'selrow');
					var cbAllSame = batchEditor.mockupSelectWin.children("#selectallsame").first();
					//get name of the target
					targetName = table.getCell(selrow, 1);
					targetId = table.getCell(selrow, 0);
					if(targetName != false){
						if(cbAllSame.checked == false){
							targetElemt = li.children.item(4);
							targetElemt.innerHTML = '<b>' + targetName + '</b>';
							targetElemt.attributes.getNamedItem('targetId').value = targetId;
							targetElemt.attributes.getNamedItem('targetName').value = targetName;
						}
						else{
							$.each( batchEditor.packagelist.children('li'), function(index, item){
								targetElemt = item.children.item(4);
								targetElemt.innerHTML = '<b>' + targetName + '</b>';
								targetElemt.attributes.getNamedItem('targetId').value = targetId;
								targetElemt.attributes.getNamedItem('targetName').value = targetName;
							})
						}
					}
					$(this).dialog("close");
					}}]});
		});
		
		return this;
	}
	
	/**
	 * 
	 */
	this.initMockupSelectWin = function(selector, mydata)
	{
		this.mockupSelectWin = $(selector);
		this.mockupSelectWin.hide();
		this.mockupListTable = this.mockupSelectWin.children("table").first();
		
		/* set table of mockups list*/
		this.mockupListTable.jqGrid({
			datatype: 'local',
			data: mydata,
			colNames:['Id','Number', 'Description'],
			colModel:[
					{name:'id',index:'id', width:20},
					{name:'number',index:'number', width:300},
					{name:'description',index:'description', width:600}
					],
			sortname: 'number',
			sortorder: 'desc',
			viewrecords: true,
			caption:'Mockups',
			multiselect: false,
			autowidth: false,
			rowNum: 1000,
			afterInsertRow: function(rowid, rowdata, rowelem){
			},
			ondblClickRow: function(ids) {
			},
	        localReader: {
	            repeatitems: true,
	            cell: "",
	            id: 0
	        }
			});
		
		return this;
	}
	
	/**
	 * 
	 */
	this.initSavebutton = function(selector, feedbackWinSelector)
	{
		this.saveButton = $(selector);
		this.feedbackWin = $(feedbackWinSelector);
		var batchEditor = this;
		
		this.saveButton.bind('click', null, function(e){
			myArray = new Array();
			$.each(batchEditor.packagelist.children('li'), function(index, li){
				packageId = li.children.item(1).attributes.getNamedItem('value').value;
				targetId = li.children.item(4).attributes.getNamedItem('targetId').value;
				targetName = li.children.item(4).attributes.getNamedItem('targetName').value;
				myArray.push( new Array(packageId, targetId) );
			});

			batchEditor.feedbackWin.load(
				batchEditor.options.baseurl+'/'+batchEditor.options.url+'/create',
				'data-jsonencoded=' + $.JSON.encode(myArray),
				function(data, textStatus, jqXHR){
				}
			);
		});
		
		return this;
	}

	/**
	 * 
	 */
	this.action = function(e, button, url, successMsg, confirmMsg, successcallback)
	{
		if(confirmMsg != ""){
			if(!confirm(confirmMsg)){
				return false;
			}
		}

		if(button.is("button")){
			/* retrieve the parent form */
			var querydata = this.preventForm($(button).parents("form")).serializeArray();
		}
		else if(button.is("a")){
			/* get data from form and div */
			var data = button.parents("#docfile-context-menu").first().data('menuItemData');
			var formdata = this.preventForm($(button).parents("form")).serializeArray();
			var querydata = {};
			/* filter checkboxes */
			$.each(formdata, function(i,item) {
				if(item.name != "checked[]"){
					querydata[item.name] = item.value;
				}
			});
			querydata.checked = [data.id];
		}

		$.ajax({
			type: "POST",
			async: false, // Mode synchrone
			url: url,
			data: querydata,
			dataType: "json",
			success: function(data){
				if(successMsg){
					alert(successMsg);
				}
				console.log(data);
			},
			error: function(data){
				return ajaxErrorToMsg(data);
			}
		});

		return false;
	};

	/**
	 * To prevent the default submit action on the form
	 */
	this.preventForm = function(form)
	{
		console.log( "refine prevent default submit on "+form.id );
		form.unbind("submit");
		form.bind("submit", function(e){
			e.defaultPrevented();
			e.stopPropagation();
			console.log( "submit "+form.id );
			return false;
		});
		return form;
	};
};

