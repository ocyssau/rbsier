
/**
 * 
 */
function RbContainerManager(spacename, jqElement)
{
	this.spacename = spacename;
	this.service = new RbContainerService();
	this.jqElement = jqElement;
	
	/* @var RbWildspaceButtons */
	this.rbButtons = null;

	/**/
	this.init = function(refreshCallback)
	{
		RbContainerManager.prototype.init.call(this,refreshCallback);
		return this;
	};
	
	/**
	 * INIT TABLE AND CONTEXTUAL MENU
	 * @var string selectorForUl css selector for select parent element where is define the context menu
	 */
	this.initTableAliasCm = function(selectorForUl, trSelector)
	{
		if(!this.table.aliasContextMenu){
			ulElement = this.jqElement.find(selectorForUl).first();
			rbButtons = new RbContainerAliasButtons(ulElement, this.spacename);
			this.table.initContextMenu(rbButtons, this.service, trSelector);
		}
		return this;
	};
	
	/** 
	 * ALIAS BUTTONS BOX
	 * @var string boxSelector css selector for box elements
	 * @var string cmSelector css selector for context menu ul element
	 */
	this.initAliasButtonBox = function(boxSelector, cmSelector)
	{
		if(!this.aliasButtonbox){
			box = new RbObjectBox(this.jqElement.find(boxSelector));
			box.init();
			box.initButtons(new RbContainerAliasButtons(box.jqElement, this.spacename));
			this.aliasButtonbox = box;
		}
		return this;
	};
	
	/**
	 * Get context menu
	 * @var jqParentElement jquery element with data definition for actions. Must have class rb-submit-data.
	 * @return RbWildspaceButtons
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbContainerButtons(jqParentElement, this.spacename);
		return rbButtons;
	};
	
	/** 
	 * OBJECT BOX init some functionnalities and buttons
	 * 
	 * @var string boxSelector css selector for box elements
	 */
	this.initAliasObjectBox = function(boxSelector)
	{
		if(!this.aliasbox){
			box = new RbObjectBox(this.jqElement.find(boxSelector));
			box.init();
			box.initButtons(new RbContainerAliasButtons(box.jqElement, this.spacename));
			this.aliasbox = box;
		}
		return this;
	};
}
RbContainerManager.prototype = new RbManager();

/**
 * 
 */
function RbContainerButtons(jqParentElement, spacename)
{
	this.base = RbButtons;
	this.base(jqParentElement);

	/**
	 * @param array datas input datas as extracted from element
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			switch(item.name){
				case 'checked[]':
				case 'containerid':
				case 'containerId':
				case 'id':
					if(url.search('%id%') > 0){
						url = url.replace('%id%', item.value);
					}
					else{
						item.name = 'checked[]';
						queryData.push(item);
					}
					break;
				case 'spacename':
					url = url.replace('%spacename%', item.value);
					break;
				default:
					if(item.name.match('^spacenames(\[[0-9\'\"]+\])')){
						url = url.replace('%spacename%', item.value);
					}
					else{
						queryData.push(item);
					}
				break;
			}
		});
		url = url.replace('/%id%', '');
		url = url.replace('/%spacename%', '');
		return url;
	}

	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;

		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+"/ged/"+spacename+"/create";
			//actionProcessor.getRequest(e, $(this), url);
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#containerEdit"));
				}
			});
			return false;
		});

		/**/
		parent.find(".activate-btn").click(function(e){
			var url = document.baseurl+"/ged/session/activate/%spacename%/%id%";
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			return false;
		});

		/**/
		parent.find(".edit-btn").click(function(e){
			var submitData = $(this).parents(".rb-submit-data");
			var spacename = submitData.data("spacename");
			var url = document.baseurl+"/ged/"+spacename+"/edit";
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#containerEdit"));
				}
			});
			return false;
		});

		/**/
		parent.find(".rename-btn").click(function(e){
			var url = document.baseurl+"/ged/"+spacename+"/rename";
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".delete-btn").click(function(e){
			if(confirm('The Container must be empty to be deleted, Do you want to continue?')){
				var url = document.baseurl+"/ged/"+spacename+"/delete";
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});

		/**/
		parent.find(".link-property-btn").click(function(e){
			var url = document.baseurl+"/ged/container/metadatas";
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".link-doctype-btn").click(function(e){
			var url = document.baseurl+"/ged/container/doctypes";
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".link-categorie-btn").click(function(e){
			var url = document.baseurl+"/ged/container/categories";
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".permission-btn").click(function(e){
			var url = document.baseurl+"/ged/container/permission/assign";
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#assignPermissionForm"));
				}
			});
			return false;
		});

		/**/
		parent.find(".getdetail-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+"/ged/container/detail/%spacename%/%id%";
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			var dataDiv = $(this).parents(".rb-submit-data").first();
			var options = {
					queryData:queryData,
					popup:{
						target:"container-"+dataDiv.data("id"),
						ysize:600,
						xsize:1200
					},
			};
			actionProcessor.getRequest(e, $(this), url, options);
			return false;
		});

		/* other buttons */
		parent.find(".archive-btn").click(function(e){
			var url = document.baseurl+"/ged/document/archiver/container";
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".addprefered-btn").click(function(e){
			var url = document.baseurl+"/service/container/prefered/add";
			actionProcessor.toService(e,$(this),url);
			rbButtons.reload();
			return false;
		});

		/**/
		parent.find(".removeprefered-btn").click(function(e){
			var url = document.baseurl+"/service/container/prefered/remove";
			actionProcessor.toService(e,$(this),url);
			rbButtons.reload();
			return false;
		});

		/**/
		parent.find(".notification-btn").click(function(e){
			var url = document.baseurl+"/ged/container/notification/edit";
			actionProcessor.getModal(e, $(this), url, {
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#notificationEdit"));
				}
			});
			return false;
		});

		/**/
		parent.find(".gethistory-btn").click(function(e){
			var url = document.baseurl+"/ged/container/history";
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".getcategories-btn").click(function(e){
			var url = document.baseurl+"/ged/category/index";
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".getproperties-btn").click(function(e){
			var url = document.baseurl+"/ged/metadata/%spacename%/index";
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			return false;
		});

		/**/
		parent.find(".getbatch-wi-btn").click(function(e){
			var url = document.baseurl+'/ged/document/history/batch/ofcontainer/%spacename%/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			return false;
		});

		/**/
		parent.find(".gethistory-wi-btn").click(function(e){
			var url = document.baseurl+'/ged/document/history/ofcontainer/%spacename%/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
			return false;
		});

		/**/
		parent.find(".add-document-btn").click(function(e){
			var url = document.baseurl+'/ged/document/manager/%spacename%/create';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getModal(e, $(this), url, {
				queryData: queryData,
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#documentEdit"));
				}
			});
			return false;
		});

		/**/
		parent.find(".getdfmanager-btn").click(function(e){
			var url = document.baseurl+'/ged/docfile/manager/%spacename%/index/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {
				type: "GET",
				queryData: queryData
			});
			return false;
		});

		/**/
		parent.find(".getdocmanager-btn").click(function(e){
			var url = document.baseurl+"/session/manager/activate?id=%id%&spacename=%spacename%";
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {
				type: "GET",
				queryData: queryData
			});
			return false;
		});

		/**/
		parent.find(".downloadlist-btn").click(function(e){
			var url = document.baseurl+'/ged/document/manager/%spacename%/excel/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {
				type: "GET",
				queryData: queryData
			});
			return false;
		});

		/**/
		parent.find(".getimport-btn").click(function(e){
			var url = document.baseurl+'/import/index';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
	};
}
RbContainerButtons.prototype = new RbButtons();

/**
 * 
 */
function RbContainerAliasButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);

	/**
	 * @param array datas input datas as extracted from element
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			switch(item.name){
				case 'checked[]':
				case 'containerid':
				case 'containerId':
				case 'id':
					if(url.search('%id%') > 0){
						url = url.replace('%id%', item.value);
					}
					else{
						item.name = 'checked[]';
						queryData.push(item);
					}
					break;
				case 'spacename':
					url = url.replace('%spacename%', item.value);
					break;
				default:
					if(item.name.match('^spacenames(\[[0-9\'\"]+\])')){
						url = url.replace('%spacename%', item.value);
					}
					else{
						queryData.push(item);
					}
				break;
			}
		});
		url = url.replace('/%id%', '');
		url = url.replace('/%spacename%', '');
		return url;
	}
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;

		/* ALIAS BUTTONS DEFINITIONS */
		parent.find(".activate-btn").click(function(e){
			var url = document.baseurl+"/ged/session/activate/%spacename%/%id%";
			var queryData = $(this).parents(".rb-submit-data").first().data();
			url = url.replace('%id%', queryData.targetid);
			url = url.replace('%spacename%', queryData.spacename);
			actionProcessor.getRequest(e, $(this), url, {queryData:[]});
			return false;
		});
		
		/**/
		parent.find(".alias-create-btn").click(function(e){
			var url = document.baseurl+"/ged/container/alias/create";
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#aliasEdit"));
				}
			});
			return false;
		});

		/**/
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+"/ged/container/alias/edit";
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#aliasEdit"));
				}
			});
			return false;
		});

		/**/
		parent.find(".delete-btn").click(function(e){
			var url = document.baseurl+"/ged/container/alias/delete";
			actionProcessor.getRequest(e, $(this), url);
			rbButtons.reload();
			return false;
		});
	}
}
RbContainerAliasButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbContainerService()
{

	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
			if(item.name.substring(0,11) == "spacenames["){
				eval(item.name + "='" + item.value + "'");
			}
		});
		
		var i=1;
		var serviceData = {'containers':{}};
		$.each(checked, function(index, id){
			serviceData.containers['container'+i] = {
				id: id,
				spacename: spacenames[id]
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {containers:{
			container1:{
				id:elemt.data('id'),
				spacename:elemt.data('spacename')
			}
		}};
		return queryData;
	};
}
RbContainerService.prototype = new RbService();


/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbContainerFavorite(jqElement)
{
	/* CONTEXT MENU */
	var cm = new RbObjectBoxContextMenu(jqElement.find("#favorite-context-menu").first());
	cm.init($("button.rb-favorite"));

	var itemTemplate = jqElement.find("span.rb-favorite-item-template").first();
	itemTemplate.hide();

	/* delete button */
	jqElement.find(".delete-btn").click(function(e){
		var url = document.baseurl+"/service/container/prefered/remove";
		var submitData = $(this).parents(".rb-submit-data");
		var id = submitData.data('id');
		var spacename = submitData.data('spacename');
		var queryData = {containers:{
			container1:{
				id: id,
				spacename: spacename
			}
		}};

		var actionProcessor = new RbAction();
		actionProcessor.serviceRequest(e, $(this), url, {
			queryData: queryData,
			type: "GET",
			successCallback: function(e,jqBtn){
				var favorite = jqElement.find("#"+id);
				favorite.remove();
			}
		});
		return false;
	});

	/* activate button */
	jqElement.find(".activate-btn").click(function(e){
		var url = document.baseurl+"/ged/session/activate/%spacename%/%id%";
		rbButtons = new RbContainerButtons(jqElement, "");
		actionProcessor = new RbObjectBoxAction();
		queryData = [];
		url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
		actionProcessor.getRequest(e, $(this), url, {queryData:queryData});
		return false;
	});

	/* load the prefered from the current user */
	this.load = function(datas){
		var url = document.baseurl+"/service/container/prefered/index";
		$.ajax({
			type : 'get',
			url : url,
			data : {},
			dataType: 'json',
			success : function(respons, textStatus, jqXHR){
				var datas = respons.data;
				$.each(datas, function(index,favorite){
					var item = itemTemplate.clone(true);
					item.attr('class', 'rb-submit-data rb-favorite-item');
					item.data('id', favorite.container_id);
					item.data('spacename', favorite.spacename);
					item.find("button.activate-btn").html(favorite.container_number);
					item.show();
					jqElement.append(item);
				});
				cm.init($("button.rb-favorite"));
				return false;
			},
			error : function(data, textStatus, jqXHR){
				var respons = data.responseJSON;
			}
		});
		return false;
	};
}
