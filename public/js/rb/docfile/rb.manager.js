
/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDocfileManager(jqElement)
{
	this.service = new RbDocfileService();
	this.jqElement = jqElement;

	/* @var RbDocfileButtons */
	this.rbButtons = null;
	
	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbDocfileButtons(jqParentElement);
		return rbButtons;
	};
}
RbDocfileManager.prototype = new RbManager();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDocfileButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * @param array datas input datas as extracted from element
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			switch(item.name){
				case 'checked[]':
				case 'containerid':
				case 'containerId':
				case 'id':
					if(url.search('%id%') > 0){
						url = url.replace('%id%', item.value);
					}
					else{
						item.name = 'checked[]';
						queryData.push(item);
					}
					break;
				case 'spacename':
					url = url.replace('%spacename%', item.value);
					break;
				default:
					if(item.name.match('^spacenames(\[[0-9\'\"]+\])')){
						url = url.replace('%spacename%', item.value);
					}
					else{
						queryData.push(item);
					}
				break;
			}
		});
		url = url.replace('/%id%', '');
		url = url.replace('/%spacename%', '');
		return url;
	}

	/**/
	this.initRoleSelector = function(jqSelect, actionProcessor)
	{
		actionProcessor = actionProcessor || this.actionProcessor;

		jqSelect.change(function(e){
			url = document.baseurl+"/service/docfile/manager/setrole";
			roleId = $(this).val();
			fileId = $(this).data('id');
			spacename = $(this).data('spacename');
			queryData={
					docfiles:{
						docfile1:{
							id:fileId,
							spacename:spacename,
							role:roleId
						}
					}
			};
			actionProcessor.toService(e, $(this), url, {
				queryData: queryData,
				type: "GET"
			});
			return false;
		});
	}

	/**/
	this.init = function(actionProcessor)
	{
		this.actionProcessor = actionProcessor;
		var parent = this.jqElement;
		var rbButtons = this;

		/**/
		this.reload = function(){
			$("#content").load("#files", function(){});
			return false;
		};

		/*BUTTONS DEFINITIONS*/
		/**/
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this file?')){
				var url = document.baseurl+"/service/docfile/manager/delete";
				actionProcessor.toService(e, $(this), url, {
					successCallback:function(){
						rbButtons.reload();
					}
				});
			}
			return false;
		});

		/**
		 * 
		 */
		parent.find(".putinws-btn").click(function(e){
			var url = document.baseurl+"/service/docfile/datamanager/putinws";
			actionProcessor.toService(e, $(this), url);
			return false;
		});

		/**
		 * 
		 */
		parent.find(".getiteration-btn").click(function(e){
			var url = document.baseurl+'/ged/docfile/iteration/%spacename%/%id%';
			var queryData = [];
			$.each(actionProcessor.getQueryData($(this)),function(k, item){
				switch(item.name){
				case 'checked[]':
				case 'fileId':
				case 'fileid':
				case 'id':
					url = url.replace('%id%', item.value);
					break;
				case 'spacename':
					url = url.replace('%spacename%', item.value);
					break;
				case 'layout':
					queryData.push(item);
					break;
				}
			});
			url = url.replace('/%id%', '');
			url = url.replace('/%spacename%', '');

			var dataDiv = $(this).parents(".rb-submit-data").first();
			var options = {
					popup:{
						target:"iteration of file " + dataDiv.data('name'),
						ysize:600,
						xsize:1200
					},
					hash:null,
					queryData:queryData
			};
			actionProcessor.getRequest(e, $(this), url, options);
			return false;
		});
		
		
		/**
		 * 
		 */
		parent.find(".replacefile-btn").click(function(e){
			var url = document.baseurl+'/ged/docfile/version/%spacename%/%id%/replace';
			var queryData = [];
			$.each(actionProcessor.getQueryData($(this)),function(k, item){
				switch(item.name){
				case 'checked[]':
				case 'fileId':
				case 'fileid':
				case 'id':
					url = url.replace('%id%', item.value);
					break;
				case 'spacename':
					url = url.replace('%spacename%', item.value);
					break;
				case 'layout':
					queryData.push(item);
					break;
				}
			});
			url = url.replace('/%id%', '');
			url = url.replace('/%spacename%', '');

			var dataDiv = $(this).parents(".rb-submit-data").first();
			var options = {
				hash:'docfiles',
				queryData:queryData
			};
			actionProcessor.getRequest(e, $(this), url, options);
			return false;
		});
		
		/**
		 * 
		 */
		parent.find(".checkinreset-btn").click(function(e){
			var url = document.baseurl+"/service/docfile/lockmanager/resetfile";
			actionProcessor.toService(e, $(this), url, {
				successCallback:function(){
					rbButtons.reload();
				}
			});
			return false;
		});

		/**
		 * 
		 */
		parent.find(".download-btn").click(function(e){
			var url = document.baseurl+'/service/docfile/viewer/download';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**
		 * 
		 */
		parent.find(".hidedfmanager-btn").click(function(e){
			var url = document.baseurl+'/ged/docfile/manager/%spacename%/hide';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {
				type: "GET",
				queryData:queryData
			});
			return false;
		});

		/**
		 * 
		 */
		parent.find(".downloadlist-btn").click(function(e){
			var url = document.baseurl+'/ged/docfile/manager/%spacename%/excel/%id%';
			var queryData = [];
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {
				type: "GET",
				queryData:queryData
			});
			return false;
		});
	}
};
RbDocfileButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDocfileService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
			if(item.name.substring(0,11) == "spacenames["){
				eval(item.name + "='" + item.value + "'");
			}
		});
		
		var i=1;
		var serviceData = {'docfiles':{}};
		$.each(checked, function(index, id){
			serviceData.docfiles['docfile'+i] = {
				id: id,
				spacename: spacenames[id]
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS docfile id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {docfiles:{
			docfile1:{
				id:elemt.data('id'),
				spacename:elemt.data('spacename')
			}
		}};
		return queryData;
	};
};
RbDocfileService.prototype = new RbService();

//# sourceURL=/ranchbe/dynamic/rb/docfile/rb.manager.js
