/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDocfileIterationManager(jqElement)
{
	this.service = new RbDocfileIterationService();
	this.jqElement = jqElement;

	/* @var RbDocfileIterationButtons */
	this.rbButtons = null;
	
	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbDocfileIterationButtons(jqParentElement);
		return rbButtons;
	};
};
RbDocfileIterationManager.prototype = new RbManager();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 * 
 * @param jqParentElement
 * @param spacename string
 */
function RbDocfileIterationButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);

	/**/
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/*BUTTONS DEFINITIONS*/
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this file?')){
				var url = document.baseurl+"/service/docfile/iteration/delete";
				actionProcessor.toService(e, $(this), url, {
					successCallback:function(){
						rbButtons.reload();
					}
				});
			}
			return false;
		});

		/**
		 * 
		 */
		parent.find(".putinws-btn").click(function(e){
			var url = document.baseurl+"/service/docfile/iteration/putinws";
			actionProcessor.toService(e, $(this), url);
			return false;
		});

		/**
		 * 
		 */
		parent.find(".download-btn").click(function(e){
			var url = document.baseurl+'/service/docfile/iteration/download';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
	}
};
RbDocfileIterationButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDocfileIterationService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
			if(item.name.substring(0,11) == "spacenames["){
				eval(item.name + "='" + item.value + "'");
			}
		});
		
		var i=1;
		var serviceData = {'docfiles':{}};
		$.each(checked, function(index, id){
			serviceData.documents['docfile'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS docfile id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt)
	{
		var queryData = {docfiles:{
			docfile1:{
				id:elemt.data('id'),
				spacename:elemt.data('spacename')
			}
		}};
		return queryData;
	};
};
RbDocfileIterationService.prototype = new RbService();


//# sourceURL=/ranchbe/dynamic/rb/docfile/rb.iteration.js
