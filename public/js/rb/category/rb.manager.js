
/**
 * 
 */
function RbCategoryManager(jqElement)
{
	this.service = new RbCategoryService();
	this.jqElement = jqElement;

	/* @var RbCategoryButtons */
	this.rbButtons = null;
	
	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbCategoryButtons(jqParentElement);
		return rbButtons;
	};
}
RbCategoryManager.prototype = new RbManager();

/**
 * 
 */
function RbCategoryButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);

	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;

		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/ged/category/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#categoryEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/ged/category/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#categoryEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this category?')){
				var url = document.baseurl+'/ged/category/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
	};
}
RbCategoryButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbCategoryService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'categories':{}};
		$.each(checked, function(index, id){
			serviceData.categories['category'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {categories:{
			category1:{
				id:elemt.data('id')
			}
		}};
		return queryData;
	};
	
}
RbCategoryService.prototype = new RbService();
