/**
 * 
 */
function RbProductInstanceManager()
{
}
RbProductInstanceManager.prototype = new RbManager();

/**
 * 
 */
function RbProductInstanceButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	
	/**
	 * @param array datas input datas as extracted from element 
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			switch(item.name){
				case 'checked[]':
				case 'productid':
				case 'productId':
				case 'id':
					url = url.replace('%id%', item.value);
					break;
				default:
					queryData.push(item);
					break;
			}
		});
		url = url.replace('/%id%', '');
		return url;
	}

	/**
	 * @param actionProcessor RbAction
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* init actions buttons */
		parent.find(".edit-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+'/pdm/productinstance/edit/%id%';
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getModal(e, $(this), url, {
				hash: "",
				queryData: queryData,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#productinstanceEdit"));
				}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/pdm/productinstance/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#productinstanceEdit"));
				}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".setreference-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+'/pdm/productinstance/setreference/%id%';
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				queryData: queryData,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#setreferenceForm"));
				}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".setposition-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+'/pdm/productinstance/setposition/%id%';
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				queryData: queryData,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#setpositionForm"));
				}
			});
			return false;
		});
		
		
		/* init actions buttons */
		parent.find(".getdetail-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+'/pdm/productinstance/detail/index/%id%';
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			var dataDiv = $(this).parents(".rb-submit-data").first();
			var options = {
					queryData:queryData,
					hash:"#instance",
					popup:{
						target:"detail of instance " + dataDiv.data("id"),
						ysize:600,
						xsize:1200
					},
			};
			actionProcessor.getRequest(e, $(this), url, options);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".reconciliate-btn").click(function(e){
			var url = document.baseurl+'/service/pdm/reconciliate/instanceToProduct';
			var options = {
					hash: null,
					successCallback: function(e){
						var respons = e.respons;
						return false;
					}
			};
			actionProcessor.toService(e, $(this), url, options);
			return false;
		});
	};
}
RbProductInstanceButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbProductInstanceService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'products':{}};
		$.each(checked, function(index, id){
			serviceData.products['instance'+i] = {
				id: id,
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {products:{
			product1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbProductInstanceService.prototype = new RbService();

