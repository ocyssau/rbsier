
/**
 * 
 */
function RbProductVersionManager(jqElement)
{
	this.service = new RbProductVersionService();
	this.jqElement = jqElement;

	/* @var RbProductVersionButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbProductVersionButtons(jqParentElement);
		return rbButtons;
	};
}
RbProductVersionManager.prototype = new RbManager();

/**
 * 
 */
function RbProductVersionButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * @param array datas input datas as extracted from element 
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			switch(item.name){
				case 'checked[]':
				case 'productid':
				case 'productId':
				case 'id':
					url = url.replace('%id%', item.value);
					break;
				default:
					queryData.push(item);
					break;
			}
		});
		url = url.replace('/%id%', '');
		return url;
	}

	/**
	 * @param actionProcessor RbAction
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* init actions buttons */
		parent.find(".edit-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+'/pdm/productversion/edit/%id%';
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getModal(e, $(this), url, {
				hash: "",
				queryData: queryData,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#productversionEdit"));
				}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/pdm/productversion/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#productversionEdit"));
				}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this product?')){
				var url = document.baseurl+'/pdm/productversion/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		/* init actions buttons */
		parent.find(".enter-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+'/pdm/explorer/index/%id%';
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			actionProcessor.getRequest(e, $(this), url, {
				queryData:queryData,
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#confirmation"));
				}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".getdetail-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+'/pdm/productversion/detail/index/%id%';
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			var dataDiv = $(this).parents(".rb-submit-data").first();
			var options = {
					queryData:queryData,
					hash:"#product",
					popup:{
						target:"detail of product " + dataDiv.data("id"),
						ysize:600,
						xsize:1200
					},
			};
			actionProcessor.getRequest(e, $(this), url, options);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".usedby-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+'/pdm/product/relation/usedby/%id%';
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			var dataDiv = $(this).parents(".rb-submit-data").first();
			var options = {
				queryData:queryData,
				hash:"#product",
			};
			actionProcessor.getRequest(e, $(this), url, options);
			return false;
		});
		
		/* init actions buttons */
		parent.find(".getchildren-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+'/pdm/product/relation/children/%id%';
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			var dataDiv = $(this).parents(".rb-submit-data").first();
			var options = {
				queryData:queryData,
				hash:"#product",
			};
			actionProcessor.getRequest(e, $(this), url, options);
			return false;
		});
		

		/* init actions buttons */
		parent.find(".reconciliate-btn").click(function(e){
			var url = document.baseurl+'/service/pdm/reconciliate/productToDocument';
			var options = {
					hash: null,
					successCallback: function(e){
						var respons = e.respons;
						return false;
					}
			};
			actionProcessor.toService(e, $(this), url, options);
			return false;
		});
		
	};
}
RbProductVersionButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbProductVersionService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'products':{}};
		$.each(checked, function(index, id){
			serviceData.products['instance'+i] = {
				id: id,
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {products:{
			product1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbProductVersionService.prototype = new RbService();
