/**
 * 
 */
function RbProjectManager(jqElement)
{
	this.service = new RbProjectService();
	this.jqElement = jqElement;

	/* @var RbProjectButtons */
	this.rbButtons = null;

	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbProjectButtons(jqParentElement);
		return rbButtons;
	};
}
RbProjectManager.prototype = new RbManager();

/**
 * 
 */
function RbProjectButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * @param array datas input datas as extracted from element 
	 * @param array queryData output queryData to send to service
	 * @param string url url with hooks to replace
	 * @return string Url with hooks replaced
	 */
	this.buildRoute = function(datas, queryData, url)
	{
		$.each(datas, function(k, item){
			switch(item.name){
				case 'checked[]':
				case 'projectid':
				case 'projectId':
				case 'id':
					if(url.search('%id%') > 0){
						url = url.replace('%id%', item.value);
					}
					else{
						item.name = 'checked[]';
						queryData.push(item);
					}
					break;
				default:
					queryData.push(item);
					break;
			}
		});
		url = url.replace('/%id%', '');
		return url;
	}

	/**
	 * @param actionProcessor RbAction
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;

		/**/
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+"/ged/project/create";
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#projectEdit"));
				}
			});
			return false;
		});

		/**/
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+"/ged/project/edit";
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#projectEdit"));
				}
			});
			return false;
		});

		/**/
		parent.find(".delete-btn").click(function(e){
			if(confirm('The Project must be empty to be deleted, Do you want to continue?')){
				var url = document.baseurl+"/ged/project/delete";
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});

		/**/
		parent.find(".permission-btn").click(function(e){
			var url = document.baseurl+"/ged/project/permission/assign";
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#assignPermissionForm"));
				}
			});
			return false;
		});

		/**/
		parent.find(".link-doctype-btn").click(function(e){
			var url = document.baseurl+"/ged/project/doctypes";
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});

		/**/
		parent.find(".link-mockup-btn").click(function(e){
			var url = document.baseurl+"/ged/project/container/mockup/addlink";
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#projectAddMockup"));
				}
			});
			return false;
		});

		/**/
		parent.find(".link-cadlib-btn").click(function(e){
			var url = document.baseurl+"/ged/project/container/cadlib/addlink";
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#projectAddCadlib"));
				}
			});
			return false;
		});

		/**/
		parent.find(".link-bookshop-btn").click(function(e){
			var url = document.baseurl+"/ged/project/container/bookshop/addlink";
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#projectAddBookshop"));
				}
			});
			return false;
		});

		/**/
		parent.find(".getdetail-btn").click(function(e){
			var queryData = [];
			var url = document.baseurl+"/ged/project/detail/%id%";
			url = rbButtons.buildRoute(actionProcessor.getQueryData($(this)), queryData, url);
			var dataDiv = $(this).parents(".rb-submit-data").first();
			var options = {
					queryData:queryData,
					popup:{
						target:"detail of project "+dataDiv.data("id"),
						ysize:600,
						xsize:1200
					},
			};
			actionProcessor.getRequest(e, $(this), url, options);
			return false;
		});
	};
}
RbProjectButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbProjectService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
		});
		
		var i=1;
		var serviceData = {'projects':{}};
		$.each(checked, function(index, id){
			serviceData.categories['project'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {projects:{
			project1:{
				id:elemt.data('id'),
			}
		}};
		return queryData;
	};
}
RbProjectService.prototype = new RbService();
