/**
 * 
 */
function RbDsManager(jqElement)
{
	this.service = new RbDsManagerService();
	this.jqElement = jqElement;

	/* @var RbDsManagerButtons */
	this.rbButtons = null;

	/**/
	this.init = function(){
		return this;
	};
	
	/**
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbDsManagerButtons(jqParentElement);
		return rbButtons;
	};
}
RbDsManager.prototype = new RbManager();

/**
 * 
 */
function RbDsManagerButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/docseeder/mask/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#dsmanagerEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".typemanager-btn").click(function(e){
			var url = document.baseurl+'/docseeder/type';
			actionProcessor.getRequest(e, $(this), url, {
				hash: "",
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#maskform"));
				}
			});
			return false;
		});
		
		
		/* init actions buttons */
		parent.find(".selectbatch-btn").click(function(e){
			var url = document.baseurl+'/docseeder/batch/select';
			actionProcessor.getRequest(e, $(this), url, {
				hash: "",
			});
			return false;
		});
		
	};
}
RbDsManagerButtons.prototype = new RbButtons();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDsManagerService()
{

	/**
	 * Serialize form as required by rb service
	 */
	this.serializeForm = function(form)
	{
		var formdata = form.serializeArray();
		var checked=[];
		var queryData = {dsmanagers:{}};
		var spaceName = "";
		var i=0;
		$.each(formdata, function(index,item){
			if(item.name=="checked[]"){
				checked[i] = item.value;
				i = i+1;
			}
			if(item.name=="spacename"){
				spaceName = item.value;
			}
		});
		i=1;
		$.each(checked, function(index,id){
			queryData['dsmanagers']['dsmanager'+i] = {
				id:id,
				spacename:spaceName
			}
			i = i+1;
		});
		return queryData;
	};

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {dsmanagers:{
			dsmanager1:{
				id:elemt.data('id'),
				spacename:elemt.data('spacename')
			}
		}};
		return queryData;
	};
}
RbDsManagerService.prototype = new RbService();
