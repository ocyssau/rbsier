/**
 * 
 */
function RbDstypeManager(jqElement)
{
	this.service = new RbDstypeService();
	this.jqElement = jqElement;

	/* @var RbDstypeButtons */
	this.rbButtons = null;
	
	/**
	 * Get context menu
	 * @var jqParentElement jquery element with data definition for actions. Must have class rb-submit-data.
	 * @return RbWildspaceButtons
	 */
	this.buttonsFactory = function(jqParentElement)
	{
		rbButtons = new RbDstypeButtons(jqParentElement);
		return rbButtons;
	};
}
RbDstypeManager.prototype = new RbManager();

/**
 * 
 */
function RbDstypeButtons(jqParentElement)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/docseeder/type/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#dstypeEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".edit-btn").click(function(e){
			var url = document.baseurl+'/docseeder/type/edit';
			actionProcessor.getModal(e, $(this), url, {
				hash: " .container",
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#dstypeEdit"));
				}
			});
			return false;
		});
		
		/* init actions buttons */
		parent.find(".edit-mask-btn").click(function(e){
			var maskId = $(this).parents(".rb-submit-data").data('maskid');
			var url = document.baseurl+'/docseeder/mask/edit/'+maskId;
			actionProcessor.getModal(e, $(this), url, {
				hash: "",
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#maskform"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this dstype?')){
				var url = document.baseurl+'/docseeder/type/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
		
		
		/* init actions buttons */
		parent.find(".reconcile-btn").click(function(e){
			var url = document.baseurl+'/docseeder/reconcile';
			actionProcessor.getRequest(e, $(this), url);
			return false;
		});
		
	};
}
RbDstypeButtons.prototype = new RbButtons();


/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDstypeService()
{

	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
			if(item.name.substring(0,11) == "spacenames["){
				eval(item.name + "='" + item.value + "'");
			}
		});
		
		var i=1;
		var serviceData = {'dstypes':{}};
		$.each(checked, function(index, id){
			serviceData.documents['dstypes'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};
	
	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {dstypes:{
			dstype1:{
				id:elemt.data('id'),
				spacename:elemt.data('spacename')
			}
		}};
		return queryData;
	};
}
RbDstypeService.prototype = new RbService();

