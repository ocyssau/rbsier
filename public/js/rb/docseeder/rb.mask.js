/**
 * 
 */
function RbDsmaskButtons(jqParentElement, spacename)
{
	this.base = RbButtons;
	this.base(jqParentElement);
	
	/**
	 * 
	 */
	this.init = function(actionProcessor)
	{
		var parent = this.jqElement;
		var rbButtons = this;
		this.actionProcessor = actionProcessor;
		
		/* init actions buttons */
		parent.find(".create-btn").click(function(e){
			var url = document.baseurl+'/docseeder/mask/create';
			actionProcessor.getModal(e, $(this), url, {
				hash: null,
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#dsmaskEdit"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".edit-btn").click(function(e){
			var maskId = $(this).parents(".rb-submit-data").data('id');
			var url = document.baseurl+'/docseeder/mask/edit/'+maskId;
			actionProcessor.getModal(e, $(this), url, {
				hash: "",
				callback: function(){
					rbForm = new RbForm();
					rbForm.reload = function(){
						rbButtons.reload();
					};
					rbForm.initInModal($("#maskform"));
				}
			});
			return false;
		});

		/* init actions buttons */
		parent.find(".delete-btn").click(function(e){
			if(confirm('Do you want really suppress this dsmask?')){
				var url = document.baseurl+'/docseeder/mask/delete';
				actionProcessor.getRequest(e, $(this), url);
			}
			return false;
		});
	};
}
RbDsmaskButtons.prototype = new RbButtons();

/**
 * 
 */
function RbDsmaskManager(spacename)
{
	this.spacename = spacename;
	this.service = new RbDsmaskService(spacename);
	
	/**/
	this.init = function(){
		var actionProcessor = new RbAction();
		ranchbe.initPaginator($("form#paginator"));

		/**/
		$(".rb-dsmask .refresh-btn").click(function(e){
			$("#content").load("#", function(){});
			return false;
		});
	};
	
	/* INIT TABLE AND CONTEXTUAL MENU */
	this.initTableCm = function()
	{
		var spaceName = this.spacename;
		var rbButtons = new RbDsmaskButtons($("#dsmask-context-menu"), spaceName);
		var table = new RbTable( $("table.rb-dsmask") );
		table.markrowsInit();
		table.initContextMenu(rbButtons, new RbDsmaskService(), "tbody tr.rb-dsmask");
		return this;
	};
	
	/* INIT BUTTONS BOX */
	this.initButtonBox = function()
	{
		var spaceName = this.spacename;
		var jqButtons = $(".rb-dsmask #page-bar");
		var rbButtons = new RbDsmaskButtons(jqButtons, spaceName);
		var rbBtnBox = new RbObjectBox( jqButtons );
		rbBtnBox.initButtons(rbButtons);
		return this;
	};
	
	/* OBJECT BOX */
	this.initObjectBox = function()
	{
		var box = new RbObjectBox($("table.rb-dsmask .object-box"));
		box.init();
		box.initButtons(new RbDsmaskButtons(box.jqElement));
		return this;
	};
}
RbDsmaskManager.prototype = new RbManager();

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function RbDsmaskService()
{
	/**
	 * 
	 */
	this.queryDataToServiceData = function(queryData)
	{
		var checked = [];
		var spacenames = {};
		$.each(queryData, function(index, item){
			if(item.name.substring(0,8) == "checked["){
				checked.push(item.value);
			}
			if(item.name.substring(0,11) == "spacenames["){
				eval(item.name + "='" + item.value + "'");
			}
		});
		
		var i=1;
		var serviceData = {'dsmasks':{}};
		$.each(checked, function(index, id){
			serviceData.documents['dsmask'+i] = {
				id: id
			}
			i = i+1;
		});
		
		return serviceData;
	};
	

	/**
	 * Compose a set of query data from data found in DOM elemt
	 * data-id AS document id,
	 * data-spacename,
	 */
	this.extractQueryFromData = function(elemt){
		var queryData = {dsmasks:{
			dsmask1:{
				id:elemt.data('id'),
				spacename:elemt.data('spacename')
			}
		}};
		return queryData;
	};
}
RbDsmaskService.prototype = new RbService();
