function workflow(baseUrl){
	this.baseUrl = baseUrl;
	this.instance=null;
	this.ajaxService = new RbAjax();
	
	/**
	 * JsPlumb Definition of connectors end points
	 * @var
	 */
	this.endpointSourceOption = {
		isSource : true,
		isTarget : false,
		maxConnections : 4,
		endpoint : "Dot",
		paintStyle : {
			fillStyle : 'rgba(200,0,0,0.5)',
			radius : 8,
			cssClass: "endPointSource",
			hoverClass: "endPointSourceHover"
		}
	};

	/**
	 * JsPlumb Definition of connectors end points
	 * @var
	 */
	this.endpointTargetOption = {
		isSource : false,
		isTarget : true,
		maxConnections : 4,
		endpoint : "Rectangle",
		paintStyle : {
			fillStyle : "#7AB02C",
			width: 10,
			height: 10,
			cssClass: "endPointTarget",
			hoverClass: "endPointTargetHover"
		},
	};
	
	/**
	 * Get JsPlumb instance
	 * @function
	 */
	this.getInstance = function(container){
	    this.instance = jsPlumb.getInstance({
	    	Endpoint: ["Dot", {radius: 5}],
	    	HoverPaintStyle: {
	        	strokeStyle: "#1e8151", 
	        	lineWidth: 2 
	        	},
			PaintStyle :{
				lineWidth:2,
				strokeStyle:'rgba(100,0,150,0.3)'
			},		
			DragOptions :{
				cursor : 'pointer',
				zIndex : 2000
			},
			ConnectionOverlays : [
				["Arrow", {
					location : 1,
					id: "arrow",
					length: 14,
	                foldback: 0.8
					}],
				["Label", {
					location : 0.5,
					id : "label",
					cssClass : "connection-label"
				}]
			],
			connectorStyle: {
				strokeStyle: "#5c96bc", 
				lineWidth: 2, 
				outlineColor: "transparent", 
				outlineWidth: 4 
				},
			Connector : [ 
			     		"Flowchart",
			     		{ cornerRadius: 10, stub:25, alwaysRespectStubs:true, gap:0, midpoint:0.5 } 
			     		],
			ConnectionsDetachable:true,
			Container : container
		});
	    
	    // bind a click listener to each connection; the connection is deleted. you could of course
	    // just do this: jsPlumb.bind("click", jsPlumb.detach), but I wanted to make it clear what was
	    // happening.
	    this.instance.bind("click", function (connection) {
			var modal = $('#edit-connector-modal');
			modal.data("connectorid", connection.id);
			modal.connector = connection;
			modal.find("input[name='status']").val(connection.getOverlay("label").getLabel());
			modal.modal({});
	    });
	    
	    this.containerElemt = $("#"+container);
	    
	    return this.instance;
	};
	
	/**
	 * Add a activity, used when load json
	 * @function
	 */
	this.addActivity = function(params){
		var id = params.name; //if is new
		var entityId = "";
		
		if(params.id || params.entityId){
			id = params.id; //if is saved
			entityId = id; //id of entity as recorded in DB
		}
		
		var name = params.name;
		var title = params.title;
		var type = params.type;
		var isAutomatic = parseInt(params.isAutomatic);
		var isComment = parseInt(params.isComment);
		var isInteractive = parseInt(params.isInteractive);
		var loaded = params.loaded;
		var roles = params.roles;
		var positionx = 150;
		var positiony = 150;
		var callbackclass = "";
		var callbackmethod = "";
		var comments = "";
		var progression = 0;
		if(params.progression){
			progression = params.progression;
		}

		if( params.attributes ){
			positionx = params.attributes.positionx;
			positiony = params.attributes.positiony;
			callbackclass = params.attributes.callbackclass;
			callbackmethod = params.attributes.callbackmethod;
			comments = params.attributes.comments;
		}
		
		var activity = $("#activity-template").clone().appendTo(this.containerElemt);
		
		activity.show();
		activity.css('top', positiony);
		activity.css('left', positionx);
		activity.removeClass("activity-template");
		activity.addClass("activity graphInstance");
		activity.attr("id", id);
		activity.data("name", name);
		activity.data("isautomatic", isAutomatic);
		activity.data("iscomment", isComment);
		activity.data("isinteractive", isInteractive);
		activity.data("type", type);
		activity.data("entityid", entityId);
		activity.data("loaded", loaded);
		activity.data("callbackclass", callbackclass);
		activity.data("callbackmethod", callbackmethod);
		activity.data("roles", roles);
		activity.data("comments", comments);
		activity.data("progression", progression);
		activity.find("div.activity-name").first().html(name).attr("id", name);
		activity.find("span.activity-title").first().html(title);
		
		//In place editor of title
		activity.find("span.activity-title").first().editInPlace({
			callback : function(unused, enteredText) {
				enteredText = $.trim(enteredText);
				return enteredText;
			},
			bg_over : "#cff",
			field_type : "text",
			textarea_rows : "3",
			textarea_cols : "15"
		});
		
		switch(type){
			case "activity":
				activity.addClass("activity-activity");
				this.instance.addEndpoint(id, this.endpointTargetOption, {anchor:'Top'});
				this.instance.addEndpoint(id, this.endpointSourceOption, {anchor:'BottomCenter'});
				break;
			case "split":
				activity.addClass("activity-split");
				activity.find("div.shape").first().addClass("shape-triangle-up");
				this.instance.addEndpoint(id, this.endpointTargetOption, {anchor:'Top'});
				this.instance.addEndpoint(id, this.endpointSourceOption, {anchor:'BottomLeft'});
				this.instance.addEndpoint(id, this.endpointSourceOption, {anchor:'BottomRight'});
				break;
			case "join":
				activity.addClass("activity-join");
				activity.find("div.shape").first().addClass("shape-triangle-down");
				this.instance.addEndpoint(id, this.endpointTargetOption, {anchor:'TopLeft'});
				this.instance.addEndpoint(id, this.endpointTargetOption, {anchor:'TopRight'});
				this.instance.addEndpoint(id, this.endpointSourceOption, {anchor:'BottomCenter'});
				break;
			case "switch":
				activity.addClass("activity-switch");
				activity.find("div.shape").first().addClass("shape-trapeze");
				this.instance.addEndpoint(id, this.endpointTargetOption, {anchor:'Top'});
				this.instance.addEndpoint(id, this.endpointSourceOption, {anchor:'BottomCenter'});
				this.instance.addEndpoint(id, this.endpointSourceOption, {anchor:'Left'});
				this.instance.addEndpoint(id, this.endpointSourceOption, {anchor:'Right'});
				break;
			case "start":
				activity.addClass("activity-start");
				this.instance.addEndpoint(id, this.endpointSourceOption, {anchor:'BottomCenter'});
				break;
			case "end":
				activity.addClass("activity-end");
				this.instance.addEndpoint(id, this.endpointTargetOption, {anchor:'Top'});
				break;
			case "standalone":
				activity.addClass("activity-standalone");
				break;
		}
		
		this.instance.draggable(activity, {
			grid : [ 10, 10 ]
		});
		
		this.initActivityButton(activity);
	};
	
	/**
	 * @function
	 * Bind event on buttons of activity
	 */
	this.initActivityButton = function(parent){
		var workflow = this;
		
		parent.find(".activity-delete-btn").click(function(e){
			var ok = confirm("Are you sure?");
			if(ok==false){
				return false;
			}
			activity = $(this).parents(".activity").first();
			if(activity.data("loaded") == true){
				workflow.deleteActivity(activity);
			}
			else{
				workflow.instance.remove(activity);
			}
		});

		parent.find(".activity-edit-btn").click(function(e){
			var activityId = $(this).parents(".activity").first().attr("id");
			var activity = $("#"+activityId);
			var modal = $('#edit-activity-modal');
			modal.find("form#edit-activity-form").data("activityid", activityId);
			modal.find("input[name='name']").val(activity.data("name"));
			modal.find("input[name='title']").val(activity.find("span.activity-title").first().html());
			modal.find("input[name='isAutomatic']").prop('checked', activity.data('isautomatic'));
			modal.find("input[name='isComment']").prop('checked', activity.data('iscomment'));
			modal.find("input[name='isInteractive']").prop('checked', activity.data('isinteractive'));
			modal.find("input[name='roles']").val(activity.data("roles"));
			modal.find("textarea[name='comments']").val(activity.data("comments"));
			modal.find("textarea[name='scriptsrc']").val("");

			modal.find("input[name='progression']").val(activity.data("progression"));
			var slider = modal.find("#add-activity-progression-slider");
			slider.slider({value:parseInt(activity.data("progression"))});

			modal.modal({});
		});

		//to show/hide buttons
		parent.find(".activity-btn-bar").first().hide();
		parent.hover(function(e){
			$(this).find(".activity-btn-bar").toggle();
		});
	};
	
	/**
	 * Delete a activity trigger
	 * @function
	 */
	this.deleteActivity = function(activityElement){
		var graphInstance = this.instance;
		var activityId = activityElement.data("entityid");
		var ajaxService = this.ajaxService;
		var url = this.baseUrl+'/workflow/activity/delete/'+activityId;
		event = new Event("deleteActivity", {});
		ajaxService.serviceRequest(event,activityElement, url, {
			type : "get",
			successCallback: function(e,domElemt){
				graphInstance.remove(activityElement)
			},
			failedCallback: function(e,domElemt){
				alert(textStatus + ':' + errorThrown);
			}
		});
		/*
		$.ajax({
			type: "get",
			url: url,
			data: {},
			error: function(jqXHR, textStatus, errorThrown){
				alert(textStatus + ':' + errorThrown);
			},
			success: function(data, textStatus, jqXHR){
				graphInstance.remove(activityElement);
			}
		});
		*/
	};
	
	/**
	 * Add a transition, used in load from json
	 * @function
	 */
	this.addTransition = function(params)
	{
		if (params.attributes == null) {
			params.attributes = {};
			params.attributes.fromAnchor = "BottomCenter";
			params.attributes.toAnchor = "Top";
		}		
		
		var label = params.name;
		var from = params.fromid;
		var to = params.toid;
		var anchoFrom = params.attributes.fromAnchor;
		var anchorTo = params.attributes.toAnchor;
		var loaded = params.loaded;
		
		var common = {
			anchors:[ anchoFrom, anchorTo ],
			endpoints:["Blank", "Blank" ]
		};
		
		var connection = this.instance.connect({ source:from, target:to }, common);
		if(connection){
			connection.setParameter("loaded", loaded);
			if(params.name){
				connection.getOverlay("label").setLabel(label);
			}
			return connection;
		}
	};
	
	/**
	 * Delete a transition from modal input
	 * @function
	 */
	this.deleteTransition = function(){
		var graphInstance = this.instance;
		var modal = $("div#edit-connector-modal");
		var connectorId = modal.data('connectorid');
		
		if(confirm("Are you sure?")==false){
			modal.modal('hide');
			return false;
		}
		
		graphInstance.select().each(function(connection){
			if(connection.id == connectorId){
				graphInstance.detach(connection);
				modal.modal('hide');
				return false;
			}
		});
	};
	
	/**
	 * @function
	 */
	this.saveTransition = function(modal){
		var connectorId = modal.data('connectorid');
		var status = modal.find("input[name=status]").first().val();

		this.instance.select().each(function(connection){
			if(connection.id == connectorId){
				connection.getOverlay("label").setLabel(status);
				modal.modal('hide');
				return false;
			}
		});
	};
	
	/**
	 * @function
	 */
	this.saveModelScript = function(scriptSrc, activityId){
		json = {};
		json.processid = this.containerElemt.data("processid");
		json.activityid = activityId;
		json.scriptsrc = scriptSrc;
		
		//send request to server
		$.ajax({
			type: "post",
			url:this.baseUrl+'/workflow/process/savescript',
			data: json,
			dataType: "json",
			success: function(jsonReponse, textStatus, jqXHR){
			},
			error: function(data, textStatus, errorThrown){
				alert(textStatus + ':' + errorThrown);
			}
		});
	};
	
	
	/**
	 * @function
	 */
	this.saveActivity = function(modal){
		var form = modal.find("form").first();
		var activityId = form.data('activityid');
		var activity = $("#"+activityId);
		
		var title = form.find("input[name='title']").val();
		activity.find("span.activity-title").first().html(title);
		
		activity.data('name', form.find("input[name='name']").val());
		activity.data('isautomatic', form.find("input[name='isAutomatic']").prop('checked'));
		activity.data('iscomment', form.find("input[name='isComment']").prop('checked'));
		activity.data('isinteractive', form.find("input[name='isInteractive']").prop('checked'));
		activity.data('callbackclass', form.find("input[name='callbackclass']").val());
		activity.data('callbackmethod', form.find("input[name='callbackmethod']").val());
		activity.data('roles', form.find("input[name='roles']").val());
		activity.data('comments', form.find("textarea[name='comments']").val());
		activity.data('progression', form.find("input[name='progression']").val());

		modal.modal('hide');
	};
	
	/**
	 * @function
	 */
	this.addNewActivity = function(modal){
		var name = $("#add-activity-name").val();
		var type = $("#add-activity-type").val();
		
		title = name;
		name = name.normalize(); //normalize name without accents and spaces
		
		if(name != null){
			if(name==""){
				alert("Name must be set!");
				return;
			}
			if( $("#"+name).length > 0 ){
				alert("There is a another activity with this name");
				return;
			}

			var params = {
				id:null,
				entityId:null,
				name:name,
				title:title,
				type:type,
				isAutomatic:0,
				isComment:0,
				isInteractive:0,
				loaded:0,
				attributes:{
					positionx:150,
					positiony:150,
					domId: name,
					callbackclass:null,
					callbackmethod:null
				}
			};
			
			this.addActivity(params);
			modal.modal('hide');
		}
		return false;
	};
	
	/**
	 * load graph definition from AJAX
	 * @function
	 * @returns {String}
	 */
	this.loadFromJson = function(jsonGraph){
		var workflow = this;
		
		if(jsonGraph.activities){
			var activities = jsonGraph.activities;
			$(activities).each(function (index, elem) {
		    	elem.loaded = true;
		    	workflow.addActivity(elem);
			});
		}
	    
		if(jsonGraph.transitions){
		    var transitions = jsonGraph.transitions;
		    $.each(transitions, function (index, elem) {
		    	elem.loaded = true;
		    	workflow.addTransition(elem);
		    });
		}
	};
	
	/**
	 * Save graph into JSON data
	 * @function
	 * @returns {String}
	 */
	this.saveToJson = function(){
		var activities = [];
		$(".activity").each(function (idx, elem) {
		    var $elem = $(elem);
		    activities.push({
			    id:$elem.data('entityid'),
		    	name:$elem.data('name'),
		        title: $elem.find("span.activity-title").first().html(),
		        type:$elem.data('type'),
		        processid:$elem.data('processid'),
		        isAutomatic:Number($elem.data('isautomatic')),
		        isAutorouted:Number($elem.data('isautorouted')),
		        isComment:Number($elem.data('iscomment')),
		        isInteractive:Number($elem.data('isinteractive')),
		        loaded:$elem.data('loaded'),
		        roles:$elem.data('roles'),
		        progression: $elem.data('progression'),
		        attributes:{
			        positionx: $elem.css("left"),
			        positiony: $elem.css("top"),
			        id: $elem.attr('id'),
					callbackclass: $elem.data('callbackclass'),
					callbackmethod: $elem.data('callbackmethod'),
					comments: $elem.data('comments')
				}
		    });
		});
	    
	    var transitions = [];
	    $.each(this.instance.getConnections(), function (idx, connection) {
	    	//console.log(connection);
	        transitions.push({
	            id: connection.id,
	            fromid: connection.sourceId,
	            toid: connection.targetId,
	            fromAnchor:connection.endpoints[0].anchor.type,
	            toAnchor:connection.endpoints[1].anchor.type,
	            status:connection.getOverlay("label").getLabel(),
		        loaded:connection.getParameter('loaded')
	        });
	    });
    	console.log(transitions);
	    
   	    var jsonResult = {};
   	    if(activities.length > 0){
   	    	jsonResult.activities = activities;
   	    }
   	    if(transitions.length > 0){
   	    	jsonResult.transitions = transitions;
   	    }
	    //console.log(jsonResult);
	    return jsonResult;
	};

	/**
	 * @function
	 */
	this.save = function(e){
		var json = this.saveToJson();
		if(!json.activities){
			console.log("Nothings to save");
			return;
		}
		console.log(json);

		json.processid = this.containerElemt.data("processid");
		var graphInstance = this.instance;
		
		var ajaxService = this.ajaxService;
		var url = this.baseUrl+'/workflow/process/savegraph';
		
		/* send request to server */
		$.ajax({
			type: "post",
			url:url,
			data: json,
			dataType: "json",
			error: function(data, textStatus, errorThrown){
				error={
					message: data.responseText,
					code: textStatus,
					file: "",
					line: "",
					trace: "",
				};
				ajaxService.errorDisplay({error1:error});
			},
			success: function(jsonReponse, textStatus, jqXHR){
				$(".alert-success-save").modal({});
				$(".alert-success-save").modal('show');
				/* if save assume that all activities are saved */
				
				$(".activity").data("loaded", 1);
				
				/* mark transitions as saved */
			    $.each(graphInstance.getConnections(), function (idx, connection) {
			        connection.setParameter('loaded', 1);
			    });
			    
			    /* set entityIds */
			    $.each(jsonReponse.activities, function(index, activity){
				    $("#"+activity.attributes.id).data("entityid", activity.id);
			    });

			    /* */
				$(".alert-success-save").modal('hide');
			}
		});
	};
}
