<?php
/**
 *
 */
use Sabre\DAV;

chdir(__DIR__ . '/../.');
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);
error_reporting(E_ALL);

require 'vendor/autoload.php';

// Now we're creating a whole bunch of objects
$rootDirectory = new DAV\FS\Directory('data/wildspace/admin');

// The server object is responsible for making sense out of the WebDAV protocol
$server = new DAV\Server($rootDirectory);

// This ensures that we get a pretty index in the browser, but it is
// optional.
$server->addPlugin(new DAV\Browser\Plugin());

// If your server is not on your webroot, make sure the following line has the
// correct information
$server->setBaseUri('/remote.php');


// All we need to do now, is to fire up the server
$server->exec();
