<?php
use Zend\Stdlib\ArrayUtils;

/*
 * Controller for ranchbe 0.6
 * respond to url like /<module>/<controller>/<action>
 *
 * <module> is a sub-directory of Controller directory
 *
 * <controller> is class name define in file <controller>.php.
 * This class must be set in namespace <controller>
 *
 * <action> is a method of <controller>
 */
chdir(__DIR__ . '/../.');
// var_dump(getcwd());die;
// var_dump(getenv('APPLICATION_ENV'));die;
$env = getenv('APPLICATION_ENV');
if ( $env == 'dev' ) {
	define('DEBUG', true);
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}
else {
	define('DEBUG', false);
	ini_set('display_errors', 1);
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);
}

require_once 'module/Ranchbe/Ranchbe.php';
require_once 'config/boot.php';

$ranchbe = Ranchbe::get();
$appConfig = include ('config/application.config.php');
$ranchbe->setAutoloader(rbinit_autoloader($appConfig));

/* Choose the application From Url */
/* "Service" name is reserved */
(isset($_SERVER['REDIRECT_URL'])) ? $redirectUrl = $_SERVER['REDIRECT_URL'] : $redirectUrl = '';
(isset($_SERVER['REDIRECT_BASE'])) ? $redirectBase = $_SERVER['REDIRECT_BASE'] : $redirectBase = '';

$redirectUrl = trim(str_replace($redirectBase, '', $redirectUrl), '/');

$r = explode('/', $redirectUrl);

$module = ucfirst($r[0]);

if ( $module == 'Service' ) {
	/* */
	$module = ucfirst($r[1]);
	$controllerName = ucfirst($r[2]);
	$action = strtolower($r[3]);

	($action == '') ? $action = '' : null;
	($controllerName == '') ? $controllerName = 'index' : null;

	$includeFile = 'module/Service/src/Service/Controller/' . $module . '/' . $controllerName . 'Service.php';
	if ( is_file($includeFile) ) {
		$local = include 'config/autoload/local.php';
		$global = include 'config/autoload/global.php';
		$config = ArrayUtils::merge($global, $local);
		$ranchbe->setConfig($config['rbp']);

		/* merge configs */
		foreach( $appConfig['modules'] as $m ) {
			$appConfig = ArrayUtils::merge($appConfig, include ('module/' . $m . '/config/module.config.php'));
		}

		$controller = null;
		$rbConfig = $config['rbp'];

		try {
			$rbServiceManager = $ranchbe->getServiceManager();
			$rbServiceManager->getDaoService();
			rbinit_rbservice($rbConfig);
			rbinit_web($rbConfig);
			$rbServiceManager->getSessionManager();
			$ranchbe->setErrorStack($rbServiceManager->getErrorStack());
			$ranchbe->setLogger($rbServiceManager->getLogger());
			$currentUser = $ranchbe->getServiceManager()->currentUserService();

			if ( DEBUG === true ) {
				rbinit_enableDebug(true);
				error_reporting(E_ALL);
			}
			else {
				rbinit_enableDebug(false);
			}

			rbinit_filesystem($ranchbe);

			$controllerClass = '\\Service\\Controller\\' . $module . '\\' . $controllerName . 'Service';
			$controller = new $controllerClass();
			$controller->init();
			$return = call_user_func([
				$controller,
				$action . 'Service'
			]);
			$controller->postDispatch();
			if ( $return && $return instanceof \Service\Controller\ServiceRespons ) {
				$jsonReturn = json_encode($return);
				header('Content-Type:application/json');
				if ( $jsonReturn == false ) {
					throw new \Exception('return can not be traduct to json');
				}

				$httpErrorCode = $return->getHttpErrorCode();
				if ( $httpErrorCode ) {
					http_response_code($httpErrorCode);
				}
				elseif ( $return->hasErrors() ) {
					http_response_code(500);
				}

				echo $jsonReturn;
			}
			if(is_array($return)){
				header('Content-Type:application/json');
				echo json_encode($return);
			}
		}
		catch( \Exception $e ) {
			$return = new \Service\Controller\ServiceRespons($controller);
			$return->setException($e);
			http_response_code(500);
			header('Content-Type:application/json');
			echo json_encode($return);
		}
	}
	else {
		header("HTTP/1.0 404 Not Found");
	}
}
else {
	/* init ZF2 Mvc Application */
	$application = Zend\Mvc\Application::init($appConfig);
	$application->loader = $ranchbe->getAutoloader();
	$ranchbe->setMvcApplication($application);
	$application->run();
}
