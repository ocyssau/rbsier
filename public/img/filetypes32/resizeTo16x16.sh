#!/bin/bash
MAXSIZE=16
for file in `ls`
do
    echo "$file"
    convert "$file" -resize "$MAXSIZE" -quality 75 "../filetype16/$file"
done
exit 0
