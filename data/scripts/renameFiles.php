<?php
/**
 * Temporary scripts, may be delete after use.
 */

/**
 *
 */
function renamefiles($vaultDirectory, $spacename)
{
	$directory = new \DirectoryIterator($vaultDirectory);
	foreach( $directory as $file ) {
		if ( $directory->isFile() ) {
			$filename1 = $file->getFilename();
			$matches = [];
			
			$matches = explode('.', $filename1);
			
			if(count($matches)>2){
				$filename2 = $matches[0] . '.' . $matches[2];
				echo(sprintf("Rename file %s to %s", $filename1, $filename2) . "\n\r");
				rename($filename1, $filename2);
			}
		}
	}
}

renamefiles(getCwd(), 'workitem');

