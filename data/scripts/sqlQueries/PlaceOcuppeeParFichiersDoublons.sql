SELECT SUM(`fullsize`) FROM (
	SELECT SUM(`size`) AS `fullsize` FROM `workitem_doc_files`
	UNION
	SELECT SUM(`size`) AS `fullsize` FROM `workitem_doc_files_versions`
    UNION
	SELECT SUM(`size`) AS `fullsize` FROM `bookshop_doc_files`
	UNION
	SELECT SUM(`size`) AS `fullsize` FROM `bookshop_doc_files_versions`
) AS `sub`;

SELECT SUM(`fullsize`) FROM (
	SELECT Count(`md5`) as `dbl_count`, SUM(`size`) as `fullsize` FROM `workitem_doc_files` GROUP BY `md5` HAVING Count(`md5`) > 1
    UNION
	SELECT Count(`md5`) as `dbl_count`, SUM(`size`) as `fullsize` FROM `workitem_doc_files_versions` GROUP BY `md5` HAVING Count(`md5`) > 1
    UNION
	SELECT Count(`md5`) as `dbl_count`, SUM(`size`) as `fullsize` FROM `bookshop_doc_files` GROUP BY `md5` HAVING Count(`md5`) > 1
    UNION
	SELECT Count(`md5`) as `dbl_count`, SUM(`size`) as `fullsize` FROM `bookshop_doc_files_versions` GROUP BY `md5` HAVING Count(`md5`) > 1
) AS `sub`;
