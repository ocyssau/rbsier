SELECT
document_number AS 'Doc Number',
designation AS 'Designation',
document_state AS 'STATE', 
document_access_code AS 'Access',
IND.indice_value AS 'Indice',
document_version AS 'Version',
CONT.workitem_number AS 'Container',
DOCTYP.doctype_number AS 'Doctype',
CAT.category_number AS 'Category',
CHKOUTU.handle AS 'Check out by',
FROM_UNIXTIME(DOC.check_out_date,'%d-%m-%Y %H:%i:%s') AS 'Check out date',
OPENU.handle 'Created by',
FROM_UNIXTIME(DOC.open_date,'%d-%m-%Y %H:%i:%s') AS 'Created date',
UPDATEU.handle 'Update by',
FROM_UNIXTIME(DOC.update_date,'%d-%m-%Y %H:%i:%s') AS 'Update date'

FROM workitem_documents AS DOC

INNER JOIN workitems AS CONT ON
CONT.workitem_id = DOC.workitem_id

INNER JOIN document_indice AS IND ON
IND.document_indice_id = DOC.document_indice_id

INNER JOIN doctypes AS DOCTYP ON
DOCTYP.doctype_id = DOC.doctype_id

LEFT JOIN workitem_categories AS CAT ON
CAT.category_id = DOC.category_id

LEFT JOIN liveuser_users AS OPENU ON
OPENU.auth_user_id = DOC.open_by

LEFT JOIN liveuser_users AS CHKOUTU ON
CHKOUTU.auth_user_id = DOC.check_out_by

LEFT JOIN liveuser_users AS UPDATEU ON
UPDATEU.auth_user_id = DOC.update_by
;