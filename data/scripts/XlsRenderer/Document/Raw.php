<?php
namespace xlsRenderer\Document;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 *
 * @author olivier
 *
 */
class Raw
{

	/**
	 *
	 * @param unknown_type $container
	 */
	public function render($list, $name)
	{
		/* Creating a workbook */
		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCreator('')
			->setLastModifiedBy('')
			->setTitle('PhpSpreadsheet Test Document')
			->setSubject('PhpSpreadsheet Test Document')
			->setDescription('Test document for PhpSpreadsheet, generated using PHP classes.')
			->setKeywords('office PhpSpreadsheet php')
			->setCategory('Test result file');

		$activeSheet = $spreadsheet->setActiveSheetIndex(0);
		$activeSheet->setTitle('RanchbeDatas');

		/* Headers */
		foreach( array_keys($list[0]) as $key ) {
			$activeSheet->setCellValue('A'.$col, $key);
			$col++;
		}
		$style = $activeSheet->getStyleByColumnAndRow(0, 1, $col, 1);
		$style->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
		$style->getFont()->setSize(10)->setBold(true);

		/* Datas */
		$i = 0;
		while( $row < count($list) ) {
			$col = 0;
			$row = $i + 1;
			foreach( $list[$i] as $key => $val ) {
				$cell = $activeSheet->setCellValueByColumnAndRow($col,$row,$val,true);
				$col++;
			}
			$i++;
		}

		/* Redirect output to a client’s web browser (Xlsx) */
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="01simple.xlsx"');
		header('Cache-Control: max-age=0');
		/* If you're serving to IE 9, then the following may be needed */
		header('Cache-Control: max-age=1');
		/* If you're serving to IE over SSL, then the following may be needed */
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}
}
