<?php
// declare(encoding='UTF-8');
// -*- coding:utf-8 -*-
/**
 * \brief this table associates language extension and language name in the current language and language name in the native language
 * CAUTION: it is utf-8 encoding used here too
 * PLEASE : translators, please, update this file with your language name in your own language
 * *
 */
return array(
	null => tra('default'),
	// 'ar' => tra("Arabic"),
	// 'ca' => tra("Catalan"),
	// 'cn' => tra("Simplified Chinese"),
	// 'zh' => tra("Chinese"),
	// 'cs' => tra("Czech"),
	// 'da' => tra("Danish"),
	// 'de' => tra("German"),
	'en' => tra("English"),
	// 'en-uk' => tra("English British"),
	// 'es' => tra("Spanish"),
	// 'el' => tra("Greek"),
	// 'fi' => tra("Finnish"),
	'fr' => tra("French"),
// 'he' => tra("Hebrew"),
// 'hr' => tra("Croatian"),
// 'it' => tra("Italian"),
// 'ja' => tra("Japanese"),
// 'ko' => tra("Korean"),
// 'hu' => tra("Hungarian"),
// 'nl' => tra("Dutch"),
// 'no' => tra("Norwegian"),
// 'pl' => tra("Polish"),
// 'pt' => tra("Português"),
// 'pt-br' => tra("Brazilian Portuguese"),
// 'ru' => tra("Russian"),
// 'sb' => tra("Pijin Solomon"),
// 'sk' => tra("Slovak"),
// 'sr' => tra("Serbian"),
// 'sr-latn' => tra("Serbian Latin"),
// 'sv' => tra("Swedish"),
// 'tv' => tra("Tuvaluan"),
// 'tw' => tra("Traditional Chinese"),
// 'uk' => tra("Ukrainian")
);
